<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
  <!-- <div class="app-sidebar__user">
        <img class="app-sidebar__user-avatar" src="{{ asset('img/avatar.jpg') }}" alt="User Image" />
        <div>
            <p class="app-sidebar__user-name">John Doe</p>
            <p class="app-sidebar__user-designation">Frontend Developer</p>
        </div>
    </div> -->
  <ul id="app-menu" class="app-menu">
    <li class="{{ request()->routeIs('dboard') ? 'dashboard-active' : '' }}">

      <a class="app-menu__item " href="{{ route('dboard') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a>
    </li><!-- end Dashboard -->

    @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' ||Auth::user()->role == 'hq_user')

    <li class="treeview {{ request()->routeIs('user') || request()->routeIs('user.*') ? 'is-expanded' : '' }}">
      <a class="app-menu__item " href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">User Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>
      <ul class="treeview-menu">
        <!-- dashboard -->
        <li>
          <a href="#" class="treeview-item"><i class="icon fa fa-circle-o"></i>Dashboard</a>
        </li>

        <!-- Role -->
        <li>
          <a href="#" class="treeview-item"><i class="icon fa fa-circle-o"></i>Role</a>
        </li>

        <!-- Users -->
        <li class="{{ (request()->routeIs('user') || request()->routeIs('user.*')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Users</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i></a>
          <ul class="child-treeview-menu">
            <li>
              <a class="{{ (request()->routeIs('user') || request()->routeIs('user.*')) ? 'child-sidebar-active' : '' }}" href="{{ route('user') }}" rel="noopener"> <span><i class="icon fa fa-circle-o"></i>
                  User Management</span></a>
            </li>
          </ul>
        </li>

      </ul>
    </li>
    @endif



    @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user' || Auth::user()->role == 'store_user' || Auth::user()->role == 'hq_user')

    <!-- accounts module -->
    <li class="treeview {{ request()->is('accounts/*') ? 'is-expanded' : '' }}">
      <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-money"></i><span class="app-menu__label">Accounts & Finance</span><i class="treeview-indicator fa fa-angle-right"></i></a>
      <ul class="treeview-menu">

      <!-- account manager -->
      <li class="{{ (request()->routeIs('account-setting') || request()->routeIs('accounts.*') || request()->routeIs('account-closings') || request()->routeIs('create-account-closing') || request()->routeIs('show-ledger')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Account Manager</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i></a>
          <ul class="child-treeview-menu">
            <li>
              <a class="{{ request()->routeIs('account-setting') ? 'child-sidebar-active' : '' }}" href="{{ route('account-setting') }}"><i class="icon fa fa-circle-o pr-1"></i>Accounts Settings</a>
            </li><!-- end Accounts Settings -->

            <li>
              <a class="{{ request()->routeIs('accounts.*') ? 'child-sidebar-active' : '' }}" href="{{ route('accounts.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Accounts</a>
            </li><!-- end Accounts -->

            <li>
              <a class="{{ request()->routeIs('account-closings') || request()->routeIs('create-account-closing') ? 'child-sidebar-active' : '' }}" href="{{ route('account-closings') }}"><i class="icon fa fa-circle-o pr-1"></i>Account Closing</a>
            </li><!-- end Accounts -->

            <li>
              <a class="{{ request()->routeIs('show-ledger') ? 'child-sidebar-active' : '' }}" href="{{ route('show-ledger', 1) }}"><i class="icon fa fa-circle-o pr-1"></i>Ledger</a>
            </li><!-- end Ledger -->
          </ul>
        </li>


        @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user' || Auth::user()->role == 'hq_user')

        <!-- invoice manager -->
        <li class="{{ (request()->routeIs('sales-invoice.index') || request()->routeIs('sales-invoice.*') || request()->routeIs('accounts-vendor-invoice.index') || request()->routeIs('vendor.purchase-order-lists') || request()->routeIs('vendor.purchase.order.details') || request()->routeIs('store.invoice.archive') || request()->routeIs('sales-invoice.show') || request()->routeIs('accounts-vendor-invoice.show') || request()->routeIs('vendor.invoice.archive') ) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview {{ request()->routeIs('user') || request()->routeIs('user.*') ? 'sidebar-active ' : '' }}" href="#" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Invoice Manager</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i></a>
          <ul class="child-treeview-menu">
            @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user' )
            <li>
              <a class="{{ request()->routeIs('vendor.purchase-order-lists') || request()->routeIs('vendor.purchase.order.details') ? 'child-sidebar-active' : '' }}" href="{{ route('vendor.purchase-order-lists') }}"><i class="icon fa fa-circle-o pr-1"></i>Purchase Order List</a>
            </li><!-- end Purchase Order List -->
            <li>
              <a class="{{ request()->routeIs('sales-invoice.index') || request()->routeIs('store.invoice.archive') || request()->routeIs('sales-invoice.show') ? 'child-sidebar-active' : '' }}" href="{{ route('sales-invoice.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Sales Invoice</a>
            </li><!-- end Sales Invoice -->
            @endif
            <li>
              <a class="{{ request()->routeIs('accounts-vendor-invoice.index') || request()->routeIs('accounts-vendor-invoice.show') || request()->routeIs('vendor.invoice.archive') ? 'child-sidebar-active' : '' }}" href="{{ route('accounts-vendor-invoice.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Vendor
                Invoice</a>
            </li><!-- end Vendor Invoice -->
          </ul>
        </li>

        <!-- payment -->
        <li class="{{ (request()->routeIs('vendor.payment.lists') || request()->routeIs('other-payment.*')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview {{ request()->routeIs('vendor.payment.lists') || request()->routeIs('other-payment.index') ? 'sidebar-active' : '' }}" href="{{ route('user') }}" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Payment</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu sidebar-active">
            <li>
              <a class="{{ request()->routeIs('vendor.payment.lists') ? 'child-sidebar-active' : '' }}" href="{{ route('vendor.payment.lists') }}"><i class="icon fa fa-circle-o pr-1"></i>Vendor
                Payment</a>
            </li><!-- end Vendor Payments -->

            @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user' )
            <li>
              <a class="{{ request()->routeIs('other-payment.*') ? 'child-sidebar-active' : '' }}" href="{{ route('other-payment.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Other
                Payment</a>
            </li><!-- end Other Payment -->

            @endif
          </ul>
        </li><!-- end Payment -->
        @endif

        @if(Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user' || Auth::user()->role == 'store_user')
        <!-- deposit -->
        <li class="{{ (request()->routeIs('store-deposit.*')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview {{ request()->routeIs('user') || request()->routeIs('user.*') ? 'sidebar-active ' : '' }}" href="#" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Deposit</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ request()->routeIs('store-deposit.*') ? 'child-sidebar-active' : '' }}" href="{{ route('store-deposit.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Store
                Deposit</a>
            </li><!-- end Store Deposit -->
          </ul>
        </li><!-- end Deposit -->
        @endif

        @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user')
        <!-- receive -->
        <li class="{{ (request()->routeIs('deposit-receive.index') || request()->routeIs('deposit-receive.edit')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview {{ request()->routeIs('user') || request()->routeIs('user.*') ? 'sidebar-active ' : '' }}" href="#" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Receive</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ request()->routeIs('deposit-receive.index') || request()->routeIs('deposit-receive.edit') ? 'child-sidebar-active' : '' }}" href="{{ route('deposit-receive.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Deposit
                Receive</a>
            </li>
          </ul>
        </li>
        @endif

        <!-- Expense -->
        <li class="{{ request()->routeIs('expense-head.index') || request()->routeIs('expenses.index') || request()->routeIs('expenses.create') ? 'expanded_child_item' : '' }}">
          <a class="child-treeview {{ request()->routeIs('expense-head.index') || request()->routeIs('expenses.index') || request()->routeIs('expenses.create') ? 'sidebar-active ' : '' }}" href="#" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Expense</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ request()->routeIs('expense-head.index') ? 'child-sidebar-active' : '' }}" href="{{ route('expense-head.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Expense
                Head</a>
            </li><!-- end Expense Head -->

            <li>
              <a class="{{ request()->routeIs('expenses.index') ? 'child-sidebar-active' : '' }}" href="{{ route('expenses.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Expenses</a>
            </li><!-- end Expenses -->

            <li>
              <a class="{{ request()->routeIs('expenses.create') ? 'child-sidebar-active' : '' }}" href="{{ route('expenses.create') }}"><i class="icon fa fa-circle-o pr-1"></i>Create
                Expense</a>
            </li><!-- end Create Expense -->
          </ul>
        </li><!-- end Expense -->

        <!-- Transaction -->
        <li class="{{ request()->routeIs('transactions.index') || request()->routeIs('transactions.show')? 'expanded_child_item' : '' }}">
          <a class="child-treeview {{ (request()->routeIs('transactions.index') || request()->routeIs('transactions.show')) ? 'sidebar-active ' : '' }}" href="#" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Transaction</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ request()->routeIs('transactions.index') || request()->routeIs('transactions.show') ? 'child-sidebar-active' : '' }}" href="{{ route('transactions.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Transaction
                List</a>
            </li><!-- end Transaction List -->
          </ul>
        </li><!-- end Transaction -->


        @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user' || Auth::user()->role == 'hq_user' )
        <!-- report -->
        <li class="{{ (request()->routeIs('vendor.payment.summery') || request()->routeIs('daily.payment.report')
                    || request()->routeIs('deposit-receive.index') || request()->routeIs('trial-balance') || request()->routeIs('acc-statement') || request()->routeIs('daily.payment.report.result') ) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview {{ request()->routeIs('user') || request()->routeIs('user.*') ? 'sidebar-active ' : '' }}" href="{{ route('user') }}" rel="noopener">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Report</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">

            <li>
              <a class="{{ request()->routeIs('vendor.payment.summery') ? 'child-sidebar-active' : '' }}" href="{{ route('vendor.payment.summery') }}"><i class="icon fa fa-circle-o pr-1"></i>Vendor Payment Summary</a>
            </li><!-- end Vendor Payment Summary -->

            @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'account_user' )
            <li>
              <a class="{{ request()->routeIs('daily.payment.report') ? 'child-sidebar-active' : '' }}" href="{{ route('daily.payment.report') }}"><i class="icon fa fa-circle-o pr-1"></i>Daily Payments</a>
            </li><!-- end Daily Payments -->

            <li>
              <a class="{{ request()->routeIs('daily.payment.report.result') ? 'child-sidebar-active' : '' }}" href="{{ route('daily.payment.report.result') }}"><i class="icon fa fa-circle-o pr-1"></i>Daily Receives</a>
            </li><!-- end Daily Receives -->

            <li>
              <a class="#" href="#"><i class="icon fa fa-circle-o pr-1"></i>Deposit Receive Summary</a>
            </li><!-- end Receive -->

            <li>
              <a class="{{ request()->routeIs('acc-statement') ? 'child-sidebar-active' : '' }}" href="{{ route('acc-statement') }}"><i class="icon fa fa-circle-o pr-1"></i>Account Statement</a>
            </li><!-- end Account Statement -->

            <li>
              <a class="{{ (request()->routeIs('trial-balance')) ? 'child-sidebar-active' : '' }}" href="{{ route('trial-balance') }}"><i class="icon fa fa-circle-o pr-1"></i>Trial Balance</a>
            </li><!-- end Trial Balance -->
            @endif

          </ul>
        </li>
        @endif

      </ul>
    </li>
    <!-- end acconts module -->
    @endif


    @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user')

    <li class="treeview {{ request()->is('store/*') ? 'is-expanded' : '' }}">
      <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-home"></i><span class="app-menu__label">Store Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>
      <ul class="treeview-menu">
        <!-- dashboard -->
        <li>
          <a class="treeview-item" href="#"><i class="icon fa fa-circle-o"></i>Dashboard</a>
        </li>

        <!-- master-setup -->
        <li class="{{ (request()->routeIs('storetype.index') || request()->routeIs('storetype.*') ) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#">
            <span class="app-menu__label">
              <i class="icon fa fa-circle-o pr-1"></i>Master Setup
            </span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ (request()->routeIs('storetype.index') || request()->routeIs('storetype.*')) ? 'child-sidebar-active ' : '' }}" href="{{ route('storetype.index') }}" rel="noopener">
                <span><i class="icon fa fa-circle-o pr-1"></i> Store Type</span>
              </a>
            </li>
          </ul>
        </li>

        <!-- store manager -->
        <li class="{{ (request()->routeIs('store.create') || request()->routeIs('storeconfiguration.index') ||
                    request()->routeIs('store.index') || request()->routeIs('store.*') || request()->routeIs('storeconfiguration.edit') || request()->routeIs('storeconfiguration.create')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Store Manager</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="treeview-item {{ (request()->routeIs('store.create') || request()->routeIs('store.edit')) ? 'child-sidebar-active ' : '' }}" href="{{ route('store.create') }}">
                <i class="icon fa fa-circle-o"></i> New Store</a>
            </li>

            <li>
              <a class="treeview-item {{ (request()->routeIs('storeconfiguration.index') || request()->routeIs('storeconfiguration.edit') || request()->routeIs('storeconfiguration.create')) ? 'child-sidebar-active' : '' }}" href="{{ route('storeconfiguration.index') }}"><i class="icon fa fa-circle-o"></i>
                Store
                Configuration</a>
            </li>

            <li>
              <a class="treeview-item {{ request()->routeIs('store.index') ? 'child-sidebar-active' : '' }}" href="{{ route('store.index') }}"><i class="icon fa fa-circle-o"></i> Store
                List</a>
            </li>
          </ul>
        </li>


        <!-- store user -->
        <li class="{{ (request()->routeIs('storeuser.index') ||
                        request()->routeIs('storeuser.create') || request()->routeIs('storeuser.edit')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#">
            <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Store User</span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="treeview-item {{ (request()->routeIs('storeuser.index') ||
                                request()->routeIs('storeuser.create') || request()->routeIs('storeuser.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('storeuser.index') }}"><i class="icon fa fa-circle-o"></i> Assign User</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>

    @endif

    @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'product_operator')

    <li class="treeview {{ (request()->routeIs('product.*') || request()->routeIs('types.index')|| request()->routeIs('types.create') || request()->routeIs('types.edit') || request()->routeIs('category.index')|| request()->routeIs('category.create') || request()->routeIs('category.edit') || request()->routeIs('units.index') || request()->routeIs('units.create') || request()->routeIs('units.edit') ||
             request()->routeIs('generics.index') || request()->routeIs('generics.create') || request()->routeIs('generics.edit') || request()->routeIs('manufacturers.index') || request()->routeIs('manufacturers.create') || request()->routeIs('manufacturers.edit') || request()->routeIs('uom.create') || request()->routeIs('uom.edit') ||
             request()->routeIs('uom.index') || request()->routeIs('products.index')|| request()->routeIs('products.create')|| request()->routeIs('products.edit') || request()->routeIs('products.show') || request()->routeIs('manage-store-product.index') || request()->routeIs('manage-store-product.*') || request()->routeIs('store.products.list') || request()->routeIs('active.products') || request()->routeIs('bulk-store-product.index') || request()->routeIs('import-product.index') ||
             request()->routeIs('uom.index') || request()->routeIs('products.index') || request()->routeIs('manage-store-product.index') || request()->routeIs('store.products.list') || request()->routeIs('bulk-store-product.index') ||
             request()->routeIs('variants.index') || request()->routeIs('variants.create') || request()->routeIs('variants.edit') || request()->routeIs('variant_properties.index') || request()->routeIs('variant_properties.create') || request()->routeIs('variant_properties.edit') || request()->routeIs('products.index') || request()->routeIs('manage-store-product.index') || request()->routeIs('store.products.list') || request()->routeIs('bulk-store-product.index') || request()->routeIs('regular.index') || request()->routeIs('regular.create') || request()->routeIs('regular.edit') || request()->routeIs('regular.show') || request()->routeIs('product.report')|| request()->routeIs('get.product.report')) ? 'is-expanded' : '' }}">
      <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-product-hunt"></i><span class="app-menu__label">Product
          Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>
      <ul class="treeview-menu">

        <!-- dashboard -->
        <li>
          <a class="treeview-item {{ request()->routeIs('product.dashboard') ? 'child-sidebar-active ' : '' }}" href="{{ route('product.dashboard') }}"><i class="icon fa fa-circle-o"></i>Dashboard</a>
        </li>

        <!-- master setup -->
        <li class=" {{ (request()->routeIs('types.index')|| request()->routeIs('types.create') ||
                        request()->routeIs('types.edit') || request()->routeIs('category.index') || request()->routeIs('category.create') || request()->routeIs('category.edit')
                    || request()->routeIs('units.index') || request()->routeIs('units.create') || request()->routeIs('units.edit') || request()->routeIs('generics.index') || request()->routeIs('generics.create') || request()->routeIs('generics.edit') || request()->routeIs('manufacturers.index') || request()->routeIs('manufacturers.create') || request()->routeIs('manufacturers.edit') || request()->routeIs('uom.index') || request()->routeIs('uom.create') || request()->routeIs('uom.edit')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#">
            <span class="app-menu__label">
              <i class="icon fa fa-circle-o pr-1"></i>Master Setup
            </span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ (request()->routeIs('types.index') || request()->routeIs('types.create') ||
                                request()->routeIs('types.edit')) ? 'child-sidebar-active ' : '' }}" href="{{ route('types.index') }}" rel="noopener">
                <span><i class="icon fa fa-circle-o pr-1"></i> Product Type</span>
              </a>
            </li>

            <li>
              <a class="{{ (request()->routeIs('category.index') || request()->routeIs('category.create') || request()->routeIs('category.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('category.index') }}">
                <span><i class="icon fa fa-circle-o pr-1"></i> Product Category</span></a>
            </li>

            <li>
              <a class="{{ (request()->routeIs('units.index') || request()->routeIs('units.create') || request()->routeIs('units.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('units.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
                Product Unit</a>
            </li>

            <li>
              <a class="{{ (request()->routeIs('generics.index') || request()->routeIs('generics.create') || request()->routeIs('generics.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('generics.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
                Product Generic</a>
            </li>

            <li>
              <a class="{{ (request()->routeIs('manufacturers.index') || request()->routeIs('manufacturers.create') || request()->routeIs('manufacturers.edit') ) ? 'child-sidebar-active' : '' }}" href="{{ route('manufacturers.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Manufacturer</a>
            </li>

            <li>
              <a class="{{ (request()->routeIs('uom.index') || request()->routeIs('uom.create') || request()->routeIs('uom.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('uom.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Unit of
                Measurement</a>
            </li>
          </ul>
        </li>


        <!-- property -->
        <li class=" {{ (request()->routeIs('variants.index') || request()->routeIs('variants.create') || request()->routeIs('variants.edit') || request()->routeIs('variant_properties.index') || request()->routeIs('variant_properties.create') || request()->routeIs('variant_properties.edit')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#">
            <span class="app-menu__label">
              <i class="icon fa fa-circle-o pr-1"></i>Property
            </span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ (request()->routeIs('variants.index') || request()->routeIs('variants.create') || request()->routeIs('variants.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('variants.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
                Property Name</a>
            </li>

            <li>
              <a class="{{ (request()->routeIs('variant_properties.index') || request()->routeIs('variant_properties.create') || request()->routeIs('variant_properties.edit'))? 'child-sidebar-active' : '' }}" href="{{ route('variant_properties.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Properties</a>
            </li>
          </ul>
        </li>

        <!-- product manager -->
        <li class=" {{ (request()->routeIs('products.index')|| request()->routeIs('products.create')|| request()->routeIs('products.edit') || request()->routeIs('products.show') || request()->routeIs('manage-store-product.index') || request()->routeIs('manage-store-product.*') || request()->routeIs('store.products.list') || request()->routeIs('active.products') || request()->routeIs('bulk-store-product.index') || request()->routeIs('import-product.index')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#">
            <span class="app-menu__label">
              <i class="icon fa fa-circle-o pr-1"></i>Product Manager
            </span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="{{ (request()->routeIs('products.index') || request()->routeIs('products.create')|| request()->routeIs('products.edit') || request()->routeIs('products.show')) ? 'child-sidebar-active' : '' }}" href="{{ route('products.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
                Product</a>
            </li>

            <li>
              <a class="{{ (request()->routeIs('manage-store-product.index') || request()->routeIs('manage-store-product.*') || request()->routeIs('active.products')) ? 'child-sidebar-active' : '' }}" href="{{ route('manage-store-product.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Manage Store Product
              </a>
            </li>

            <li>
              <a class="{{ request()->routeIs('store.products.list') ? 'child-sidebar-active' : '' }}" href="{{ route('store.products.list') }}"><i class="icon fa fa-circle-o pr-1"></i>Stock
                Alert
              </a>
            </li>

            <li>
              <a class="{{ request()->routeIs('bulk-store-product.index') ? 'child-sidebar-active' : '' }}" href="{{ route('bulk-store-product.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Bulk Store
                Product</a>
            </li>

            <li>
              <a class="{{ request()->routeIs('import-product.index') ? 'child-sidebar-active' : '' }}" href="{{ route('import-product.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Import
                Product</a>
            </li>
          </ul>
        </li>

        <!-- bundle product -->
        <li class="{{ (request()->routeIs('regular.index') || request()->routeIs('regular.create') || request()->routeIs('regular.edit') || request()->routeIs('regular.show')) ? 'expanded_child_item' : '' }}">
          <a class="child-treeview" href="#">
            <span class="app-menu__label">
              <i class="icon fa fa-circle-o pr-1"></i>Bundle Product
            </span>
            <i class="child-treeview-indicator fa fa-angle-right"></i>
          </a>

          <ul class="child-treeview-menu">
            <li>
              <a class="treeview-item {{ (request()->routeIs('regular.index') || request()->routeIs('regular.create') || request()->routeIs('regular.edit') || request()->routeIs('regular.show')) ? 'child-sidebar-active' : '' }}" href="{{ route('regular.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Regular
                Bundle</a>
            </li>

            {{-- <li>
                                            <a class="treeview-item {{ request()->is('product/advance') || request()->is('product/advance/*') ? 'sidebar-active' : '' }}"
            href="{{ route('advance.index') }}"><i class="icon fa fa-circle-o"></i>Advance Bundle
            Product</a>
        </li> --}}
      </ul>
    </li>


    @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer')
    <!-- report -->
    <li class="{{ (request()->routeIs('product.report') || request()->routeIs('get.product.report')) ? 'expanded_child_item' : '' }}">
      <a class="child-treeview" href="#">
        <span class="app-menu__label">
          <i class="icon fa fa-circle-o pr-1"></i>Report</span>
        <i class="child-treeview-indicator fa fa-angle-right"></i>
      </a>

      <ul class="child-treeview-menu">
        <li>
          <a class="{{ request()->routeIs('product.report') || request()->routeIs('get.product.report') ? 'child-sidebar-active' : '' }}" href="{{ route('product.report') }}"><i class="icon fa fa-circle-o pr-1"></i>Summary
            Report</a>
        </li>
        {{-- <li>
                                                            <a class="treeview-item {{ request()->routeIs('hub.requisition.pending.list') ? 'sidebar-active' : '' }}"
        href="{{ route('hub.requisition.pending.list') }}"><i class="icon fa fa-circle-o"></i>
        Hub</a>
    </li>
    <li>
      <a class="treeview-item {{ request()->is('procurement/procurehq') || request()->is('procurement/procurehq/*') ? 'sidebar-active' : '' }}" href="{{ route('procurehq.index') }}"><i class="icon fa fa-circle-o"></i>Headquarters</a>
    </li>
    <li>
      <a class="treeview-item {{ request()->is('procurement/purchase-order') || request()->is('procurement/purchase-order/*') ? 'sidebar-active' : '' }}" href="{{ route('purchase-orders.list') }}"><i class="icon fa fa-circle-o"></i>Purchase
        Order</a>
    </li> --}}



    {{-- <li class="treeview {{ request()->is('procurement/*') ? 'is-expanded' : '' }}"> --}}
    {{-- <a class="treeview-item {{ request()->is('procurement/purchase-order') || request()->is('procurement/purchase-order/*') ? 'sidebar-active' : '' }}" --}}
    {{-- href="{{ route('purchase-orders.list') }}"><i class="icon fa fa-circle-o"></i>Report</a> --}}

    {{-- <ul class="treeview-menu"> --}}
    {{-- <li> --}}
    {{-- <a class="treeview-item {{ request()->is('procurement/procurestore') || request()->is('procurement/procurestore/*') ? 'sidebar-active' : '' }}" --}}
    {{-- href="{{ route('procurestore.index') }}"><i class="icon fa fa-circle-o"></i>Report 1</a> --}}
    {{-- </li> --}}
    {{-- </ul> --}}
    {{-- </li> --}}

  </ul>
  </li>
  @endif
  </ul>
  </li>

  @endif

  @if( Auth::user()->role=="admin" || Auth::user()->role=="developer" || Auth::user()->role=="price_operator")
  <!-- price management start -->
  <li class="treeview {{ (request()->routeIs('price.index') || request()->routeIs('price.create') || request()->routeIs('price.*') || request()->routeIs('product.search') || request()->routeIs('product.store') || request()->routeIs('pricing.*')) ? 'is-expanded' : '' }}">
    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-usd"></i><span class="app-menu__label">Price
        Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>
    <ul class="treeview-menu">


      {{-- <li>
                                  <a class="treeview-item {{ request()->is('price/customer-group') || request()->is('price/customer-group/*') ? 'sidebar-active' : '' }}"
      href="{{ route('customer-group.index') }}"><i class="icon fa fa-circle-o"></i>Customer
      Group</a>
  </li> --}}

  <li>
    <a class="treeview-item {{ (request()->routeIs('price.index') || request()->routeIs('price.create') || request()->routeIs('price.*') || request()->routeIs('pricing.*')) ? 'child-sidebar-active' : '' }}" href="{{ route('price.index') }}"><i class="icon fa fa-circle-o"></i>Price</a>
  </li>
  </ul>
  </li>
  <!-- price management end -->
  @endif



  @if( Auth::user()->role=="admin" || Auth::user()->role=="developer" || Auth::user()->role=="vendor_user" ||Auth::user()->role=="hq_user")
  <li class="treeview {{ (request()->routeIs('vendortype.index') || request()->routeIs('vendortype.create') || request()->routeIs('vendortype.edit') || request()->routeIs('vendorgroup.index') || request()->routeIs('vendorgroup.create') || request()->routeIs('vendorgroup.edit') || request()->routeIs('vendors.index') || request()->routeIs('vendors.create') || request()->routeIs('vendors.edit') || request()->routeIs('vendor-user.index') || request()->routeIs('vendor-user.create') || request()->routeIs('vendor-user.edit')|| request()->routeIs('vendor.details') || request()->routeIs('dashboard.index')) ? 'is-expanded' : '' }}">
    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-industry"></i><span class="app-menu__label">Vendor Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>
    <ul class="treeview-menu">
      @if( Auth::user()->role=="admin" || Auth::user()->role=="developer" || Auth::user()->role=="hq_user")
      <!-- dashboard -->
      <li>
        <a class="treeview-item {{ request()->routeIs('dashboard.index') ? 'child-sidebar-active' : '' }}" href="{{ route('dashboard.index') }}"><i class="icon fa fa-circle-o"></i>Dashboard</a>
      </li>

      <!-- master setup -->
      <li class="{{ (request()->routeIs('vendortype.index') || request()->routeIs('vendortype.create') || request()->routeIs('vendortype.edit') || request()->routeIs('vendorgroup.index') || request()->routeIs('vendorgroup.create') || request()->routeIs('vendorgroup.edit')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label">
            <i class="icon fa fa-circle-o pr-1"></i>Master Setup
          </span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ (request()->routeIs('vendortype.index') || request()->routeIs('vendortype.create') || request()->routeIs('vendortype.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('vendortype.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Vendor Type</a>
          </li>

          <li>
            <a class="{{ (request()->routeIs('vendorgroup.index') || request()->routeIs('vendorgroup.create') || request()->routeIs('vendorgroup.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('vendorgroup.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Vendor Group</a>
          </li>
        </ul>
      </li>

      <!-- vendor manager -->
      <li class="{{ (request()->routeIs('vendors.index') || request()->routeIs('vendors.create') || request()->routeIs('vendors.edit') || request()->routeIs('vendor.details')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label">
            <i class="icon fa fa-circle-o pr-1"></i>Vendor Manager</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ (request()->routeIs('vendors.index') || request()->routeIs('vendors.create') || request()->routeIs('vendors.edit') || request()->routeIs('vendor.details')) ? 'child-sidebar-active' : '' }}" href="{{ route('vendors.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Vendor List</a>
          </li>
        </ul>
      </li>
      @endif

      @if(Auth::user()->role=="vendor_user")
      <!-- purchasing -->
      <li class="{{ (request()->routeIs('sellrequest.index') || request()->routeIs('invoice')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label">
            <i class="icon fa fa-circle-o pr-1"></i>Purchasing</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('sellrequest.index') ? 'child-sidebar-active' : '' }}" href="{{ route('sellrequest.index') }}"><i class="icon fa fa-circle-o pr-1 "></i>Purchas
              Request</a>
          </li>

          <li>
            <a class="{{ request()->routeIs('invoice') ? 'child-sidebar-active' : '' }}" href="{{ route('invoice') }}"><i class="icon fa fa-circle-o pr-1"></i>Invoices</a>
          </li>
        </ul>
      </li>
      @endif


      @if( Auth::user()->role=="admin" || Auth::user()->role=="developer" || Auth::user()->role=="hq_user")
      <li>
        <a class="treeview-item {{ (request()->routeIs('vendor-user.index') || request()->routeIs('vendor-user.edit') || request()->routeIs('vendor-user.create')) ? 'child-sidebar-active' : '' }}" href="{{ route('vendor-user.index') }}"><i class="icon fa fa-circle-o"></i> Vendor
          User</a>
      </li>
      @endif




      {{-- --}}{{-- <li> --}}
      {{-- <a class="treeview-item {{request()->is('vendor/profile') || request()->is('vendor/profile/*') ? 'sidebar-active':''}}" href="{{ route('profile.index') }}"><i class="icon fa fa-circle-o"></i>Profile</a> --}}
      {{-- </li> --}}

    </ul>
  </li>

  @endif


  @if( Auth::user()->role=="admin" || Auth::user()->role=="developer" || Auth::user()->role=="hq_user" || Auth::user()->role=="hub_user" || Auth::user()->role=="store_user")
  <!-- Procurement Management Start -->

  <li class="treeview {{ request()->is('procurement/*') ? 'is-expanded' : '' }}">
    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-industry"></i><span class="app-menu__label">Procurement</span><i class="treeview-indicator fa fa-angle-right"></i></a>
    <ul class="treeview-menu">
      <!-- dashboard -->

      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'store_user')

      <li>
        <a class="treeview-item {{ request()->routeIs('procurement.dashboard') ? 'child-sidebar-active' : '' }}" href="{{route('procurement.dashboard')}}"><i class="icon fa fa-circle-o"></i>Dashboard</a>
      </li>

      <!-- Store Requisition Manager -->
      <li class="{{ (request()->routeIs('procurestore.create') || request()->routeIs('procurestore.index') || request()->routeIs('procurestore.create') || request()->routeIs('procurestore.show') || request()->routeIs('procurestore.edit') || request()->routeIs('procurestore.destroy')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label">
            <i class="icon fa fa-circle-o pr-1"></i>{{Auth::user()->role == 'store_user' ? 'Requisition Manager' : 'S. Requisition Manager'}}
          </span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          @if (Auth::user()->role == 'store_user')
          <li>
            <a class="{{ request()->routeIs('procurestore.create') ? 'child-sidebar-active' : '' }}" href="{{ route('procurestore.create') }}"><i class="icon fa fa-circle-o pr-1"></i>New Requisition</a>
          </li><!-- end store-New Requisition -->
          @endif
          <li>
            <a class="{{ request()->routeIs('procurestore.index') || request()->routeIs('procurestore.show') || request()->routeIs('procurestore.edit') || request()->routeIs('procurestore.destroy') ? 'child-sidebar-active' : '' }}" href="{{ route('procurestore.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Manage
              Requisition</a>
          </li><!-- end store-Manage Requisition -->
        </ul>
      </li><!-- end Store Requisition Manager -->

      @endif


      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hub_user')
      <!-- Hub Requisition Manager -->
      <li class="{{ (request()->routeIs('procurehub.create') || request()->routeIs('hub.requisition.list') || request()->routeIs('hub.requisition.details')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label">
            <i class="icon fa fa-circle-o pr-1"></i>{{!Auth::user()->role=='hub_user' ? 'Hub Requisition Manager' : 'Requisition Manager'}}
          </span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          @if (Auth::user()->role == 'hub_user')
          <li>
            <a class="{{ request()->routeIs('procurehub.create') ? 'child-sidebar-active' : '' }}" href="{{ route('procurehub.create') }}"><i class="icon fa fa-circle-o pr-1"></i>New Requisition</a>
          </li><!-- end Hub-New Requisition -->
          @endif

<li>
            <a class="{{ request()->routeIs('hub.requisition.list') || request()->routeIs('hub.requisition.details') ? 'child-sidebar-active' : '' }}" href="{{ route('hub.requisition.list') }}"><i class="icon fa fa-circle-o pr-1"></i>Manage Requisition</a>
          </li><!-- end Hub-Manage Requisition -->
          
        </ul>
      </li><!-- end Hub Requisition -->
      @endif



      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user')
      <!-- Hq Requisition Manager -->
      {{-- <li>
                                      <a class="child-treeview" href="#">
                                          <span class="app-menu__label">
                                              <i class="icon fa fa-circle-o pr-1"></i>Hq Requisition Manager
                                          </span>
                                          <i class="child-treeview-indicator fa fa-angle-right"></i>
                                      </a>

                                      <ul class="child-treeview-menu">
                                          <li>
                                              <a class="{{ request()->is('procurement/procurehq') || request()->is('procurement/procurehq/*') ? 'sidebar-active' : '' }}"
      href="{{ route('procurehq.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Requisition</a>
  </li>
  </ul>
  </li> --}}
  @endif


  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hub_user')
  <!-- Store Requisition -->
  <li class="{{ (request()->routeIs('hub.requisition.pending.list') || request()->routeIs('hub.requisition.process.list') || request()->routeIs('hub.requisition.pending.details')|| request()->routeIs('hub.requisition.pending.modify') || request()->is('procurement/hub-requisition/process/addAll') || request()->routeIs('hub.requisition.process.confirm.view')) ? 'expanded_child_item' : '' }}">
    <a class="child-treeview" href="#">
      <span class="app-menu__label">
        <i class="icon fa fa-circle-o pr-1"></i>Store Requisitions
      </span>
      <i class="child-treeview-indicator fa fa-angle-right"></i>
    </a>

    <ul class="child-treeview-menu">
      <li>
        <a class="{{ request()->routeIs('hub.requisition.pending.list') || request()->routeIs('hub.requisition.pending.details')|| request()->routeIs('hub.requisition.pending.modify') ? 'child-sidebar-active' : '' }}" href="{{ route('hub.requisition.pending.list') }}"><i class="icon fa fa-circle-o pr-1"></i>Pending Requisition</a>
      </li>

      <li>
        <a class="{{ request()->routeIs('hub.requisition.process.list') || request()->is('procurement/hub-requisition/process/addAll') || request()->routeIs('hub.requisition.process.confirm.view') ? 'child-sidebar-active' : '' }}" href="{{ route('hub.requisition.process.list') }}"><i class="icon fa fa-circle-o pr-1"></i>Process Requisition</a>
      </li><!-- end Store-Process Requisition -->

      {{--  
      <li>
        <a class="{{ request()->routeIs('hub.requisition.list') || request()->route ? 'child-sidebar-active' : '' }}" href="{{ route('hub.requisition.list') }}"><i class="icon fa fa-circle-o pr-1"></i>Manage Requisition</a>
      </li><!-- end Store-Manage Requisition -->
      --}}
    </ul>
  </li>

  @endif

  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user')
  <!-- Hub Requisitions -->
  <li class="{{ (request()->routeIs('procurehq.index') || request()->routeIs('approved.list')|| request()->routeIs('procurehq.show') || request()->routeIs('procurehq.edit')|| request()->routeIs('approved.details')) ? 'expanded_child_item' : '' }}">
    <a class="child-treeview" href="#">
      <span class="app-menu__label">
        <i class="icon fa fa-circle-o pr-1"></i>Hub Requisitions
      </span>
      <i class="child-treeview-indicator fa fa-angle-right"></i>
    </a>

    <ul class="child-treeview-menu">
      <li>
        <a class="treeview-item {{ request()->routeIs('procurehq.index') || request()->routeIs('procurehq.show') || request()->routeIs('procurehq.edit') ? 'child-sidebar-active' : '' }}" href="{{ route('procurehq.index') }}"><i class="icon fa fa-circle-o"></i>
          Pending Requisition</a>
      </li><!-- end Hub Pending Requisition -->

      <li>
        <a class="treeview-item {{ request()->routeIs('approved.list') || request()->routeIs('approved.details') ? 'child-sidebar-active' : '' }}" href="{{ route('approved.list') }}"><i class="icon fa fa-circle-o"></i>
          Approved Requisition</a>
      </li><!-- end Hub Approved Requisition -->

      {{-- <li>
                                                      <a class="treeview-item {{ request()->routeIs('hub.requisition.pending.list') ? 'sidebar-active' : '' }}"
      href="{{ route('hub.requisition.pending.list') }}"><i class="icon fa fa-circle-o"></i>
      Process Requisition</a>
  </li>

  <li>
    <a class="treeview-item {{ request()->routeIs('hub.requisition.pending.list') ? 'sidebar-active' : '' }}" href="{{ route('hub.requisition.pending.list') }}"><i class="icon fa fa-circle-o"></i>
      Manage Requisition</a>
  </li> --}}
  </ul>
  </li><!-- end Hub Requisition -->


  <!-- Vendor Requisitions -->
  <li class="{{ (request()->routeIs('hq.requisitions') || request()->routeIs('hq.approved.requisition')|| request()->routeIs('hqReq.Details') || request()->routeIs('purchase-order.create')) ? 'expanded_child_item' : '' }}">
    <a class="child-treeview" href="#">
      <span class="app-menu__label">
        <i class="icon fa fa-circle-o pr-1"></i>Vendor Requisitions
      </span>
      <i class="child-treeview-indicator fa fa-angle-right"></i>
    </a>

    <ul class="child-treeview-menu">
      <li>
        <a class="treeview-item {{ request()->routeIs('hq.requisitions') || request()->routeIs('hqReq.Details') || request()->routeIs('purchase-order.create') ? 'child-sidebar-active' : '' }}" href="{{ route('hq.requisitions') }}"><i class="icon fa fa-circle-o"></i>
          Pending Requisition</a>
      </li><!-- end Vendor Pending Requisition -->

      <li>
        <a class="treeview-item {{ request()->routeIs('hq.approved.requisition') ? 'child-sidebar-active' : '' }}" href="{{ route('hq.approved.requisition') }}"><i class="icon fa fa-circle-o"></i>
          Approved Requisition</a>
      </li><!-- end Vendor Approved Requisition -->
    </ul>
  </li><!-- end.Vendor -->

  @endif


  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user')
  <!-- Hq Requisitions -->
  {{--<li>
                                          <a class="child-treeview" href="#">
                                              <span class="app-menu__label">
                                                  <i class="icon fa fa-circle-o pr-1"></i>Hq Requisition
                                              </span>
                                              <i class="child-treeview-indicator fa fa-angle-right"></i>
                                          </a>

                                          <ul class="child-treeview-menu">
                                              <li>
                                                  <a class="treeview-item {{ request()->routeIs('hub.requisition.pending.list') ? 'sidebar-active' : '' }}"
  href="{{ route('hub.requisition.pending.list') }}"><i class="icon fa fa-circle-o"></i>
  Pending Requisition</a>
  </li>
  </ul>
  </li>--}}
  @endif

  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user')
  <!-- Purchase Order -->
  <li class="{{ (request()->routeIs('purchase-orders.list') || request()->routeIs('purchase-orders.list')|| request()->routeIs('purchase.orders.details') || request()->routeIs('purchase.orders.edit')) ? 'expanded_child_item' : '' }}">
    <a class="child-treeview" href="#">
      <span class="app-menu__label">
        <i class="icon fa fa-circle-o pr-1"></i>Purchase Order
      </span>
      <i class="child-treeview-indicator fa fa-angle-right"></i>
    </a>

    <ul class="child-treeview-menu">
      <li>
        <a class="{{ request()->routeIs('purchase-orders.list') || request()->routeIs('purchase.orders.details') || request()->routeIs('purchase.orders.edit') ? 'child-sidebar-active' : '' }}" href="{{ route('purchase-orders.list') }}"><i class="icon fa fa-circle-o pr-1"></i>Purchase Order</a>
      </li><!-- end Purchase Order => Purchase Order -->

      {{-- <li>
                                                      <a class="{{ request()->is('procurement/purchase-order') || request()->is('procurement/purchase-order/*') ? 'child-sidebar-active' : '' }}"
      href="{{ route('purchase-orders.list') }}"><i class="icon fa fa-circle-o pr-1"></i>Purchase
      Order List</a>
  </li> --}}

  <li>
    <a class="" href="#"><i class="icon fa fa-circle-o pr-1"></i>Invoice</a>
  </li><!-- end Purchase Order => Invoice -->
  </ul>
  </li><!-- end Purchase Order -->
  @endif

  @if (Auth::user()->role == 'store_user')
  <!-- start local purchase -->
  <li class="{{ (request()->routeIs('local-purchase.create') || request()->routeIs('local-purchase.index') || request()->routeIs('local-purchase.edit') || request()->routeIs('manage-local-purchase.index')|| request()->routeIs('local-purchase.show') || request()->routeIs('manage-local-purchase.show') || request()->routeIs('manage-local-purchase.edit') || request()->routeIs('manage-local-purchase.destroy')) ? 'expanded_child_item' : '' }}">
    <a class="child-treeview" href="#">
      <span class="app-menu__label">
        <i class="icon fa fa-circle-o pr-1"></i>Local Purchase
      </span>
      <i class="child-treeview-indicator fa fa-angle-right"></i>
    </a>

    <ul class="child-treeview-menu">
      <li>
        <a class="{{ request()->routeIs('local-purchase.create') ? 'child-sidebar-active' : '' }}" href="{{ route('local-purchase.create') }}"><i class="icon fa fa-circle-o pr-1"></i>Create
          Purchase</a>
      </li>

      <li>
        <a class="{{ request()->routeIs('local-purchase.index') || request()->routeIs('local-purchase.show')  || request()->routeIs('local-purchase.edit') ? 'child-sidebar-active' : '' }}" href="{{ route('local-purchase.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Purchase
          List</a>
      </li>
      <li>
        <a class="{{ request()->routeIs('manage-local-purchase.index') || request()->routeIs('manage-local-purchase.show') || request()->routeIs('manage-local-purchase.edit') || request()->routeIs('manage-local-purchase.destroy') ? 'child-sidebar-active' : '' }}" href="{{ route('manage-local-purchase.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Manage Local Purchase</a>
      </li>
    </ul>
  </li><!-- end local purchase -->
  @endif

        @if (Auth::user()->role == 'store_user')
        <!-- Purchase Order -->
            <li class="{{ (request()->routeIs('store-summary-report') ) ? 'expanded_child_item' : '' }}">
                <a class="child-treeview" href="#">
                  <span class="app-menu__label">
                    <i class="icon fa fa-circle-o pr-1"></i>Report
                  </span>
                    <i class="child-treeview-indicator fa fa-angle-right"></i>
                </a>

                <ul class="child-treeview-menu">
                    <li>
                        <a class="{{ request()->routeIs('store-summary-report') ? 'child-sidebar-active' : '' }}" href="{{ route('store-summary-report') }}"><i class="icon fa fa-circle-o pr-1"></i>Store Summary Report</a>
                    </li>
                </ul>
            </li>
        @endif

  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hub_user')
  <!-- Reports -->
  <li class="{{ (request()->is('procurement/store-report') || request()->is('procurement/purchase-report')
                || request()->is('procurement/hub-stock-product-report') || request()->is('procurement/hub-product-expiry-report')
                || request()->is('procurement/hub-product-expiry-alert-report') ||request()->is('procurement/admin-summary-report') || request()->is('procurement/hub-summary-report') || request()->is('procurement/store-product-summary-report') || request()->is('procurement/store-stock-product-report')) ? 'expanded_child_item' : '' }}">
    <a class="child-treeview" href="#">
      <span class="app-menu__label">
        <i class="icon fa fa-circle-o pr-1"></i>Reports
      </span>
      <i class="child-treeview-indicator fa fa-angle-right"></i>
    </a>

    <ul class="child-treeview-menu">
      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hub_user')
      <li>
        <a class="{{ request()->is('procurement/store-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/store-report') }}"><i class="icon fa fa-circle-o pr-1"></i>Procurement Report</a>
      </li>
      @endif

      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user')
      <li>
        <a class="{{ request()->is('procurement/purchase-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/purchase-report') }}"><i class="icon fa fa-circle-o pr-1"></i>Purchase Report</a>
      </li>
      @endif
      <li>
        <a class="treeview-item {{ request()->is('procurement/hub-stock-product-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/hub-stock-product-report') }}"><i class="icon fa fa-circle-o"></i>Hub Stock Summary</a>
      </li>

      <li>
        <a class="treeview-item {{ request()->is('procurement/store-stock-product-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/store-stock-product-report') }}"><i class="icon fa fa-circle-o"></i>Store Stock Summary</a>
      </li>

      <li>
        <a class="treeview-item {{ request()->is('procurement/hub-product-expiry-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/hub-product-expiry-report') }}"><i class="icon fa fa-circle-o"></i> Expiry Report</a>
      </li>
      <li>
        <a class="treeview-item {{ request()->is('procurement/hub-product-expiry-alert-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/hub-product-expiry-alert-report') }}"><i class="icon fa fa-circle-o"></i> Expiry Alert</a>
      </li>
      <li>
        <a class="treeview-item {{ request()->is('procurement/store-product-summary-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/store-product-summary-report') }}"><i class="icon fa fa-circle-o"></i> Store Summary Report</a>
      </li>
        @if (Auth::user()->role == 'admin')
          <li>
              <a class="treeview-item {{ request()->is('procurement/admin-summary-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/admin-summary-report') }}"><i class="icon fa fa-circle-o"></i> Admin Summary Report</a>
          </li>
        @endif

        @if (Auth::user()->role == 'hub_user')
      <li>
        <a class="treeview-item {{ request()->is('procurement/hub-summary-report') ? 'child-sidebar-active' : '' }}" href="{{ url('procurement/hub-summary-report') }}"><i class="icon fa fa-circle-o"></i> Hub Summary Report</a>
      </li>
      @endif
    </ul>
  </li>
  @endif

  {{-- <li class="treeview {{ request()->is('procurement/*') ? 'is-expanded' : '' }}"> --}}
  {{-- <a class="treeview-item {{ request()->is('procurement/purchase-order') || request()->is('procurement/purchase-order/*') ? 'sidebar-active' : '' }}" --}}
  {{-- href="{{ route('purchase-orders.list') }}"><i class="icon fa fa-circle-o"></i>Report</a> --}}

  {{-- <ul class="treeview-menu"> --}}
  {{-- <li> --}}
  {{-- <a class="treeview-item {{ request()->is('procurement/procurestore') || request()->is('procurement/procurestore/*') ? 'sidebar-active' : '' }}" --}}
  {{-- href="{{ route('procurestore.index') }}"><i class="icon fa fa-circle-o"></i>Report 1</a> --}}
  {{-- </li> --}}
  {{-- </ul> --}}
  {{-- </li> --}}

  </ul>
  </li>
  <!-- Procurement Management End -->
  @endif



  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user' || Auth::user()->role == 'warehouse_user' || Auth::user()->role == 'hub_user' || Auth::user()->role == 'store_user')
  <!-- Inventory Management Start -->
  <li class="treeview {{ request()->is('inventory/*') || request()->routeIs('hubStockEntry.availablestock.details') ? 'is-expanded' : '' }}">
    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-industry"></i><span class="app-menu__label">Inventory
        Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>
    <ul class="treeview-menu">


      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hq_user')
      <!-- dashboard -->
      <li>
        <a class="treeview-item {{ request()->routeIs('inventory.dashboard') ? 'child-sidebar-active ' : '' }}" href="{{ route('inventory.dashboard') }}"><i class="icon fa fa-circle-o"></i>Dashboard</a>
      </li>
      <!-- master setup -->
      <li class="{{ (request()->routeIs('warehouse-type.*')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Master Setup</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('warehouse-type.*') ? 'child-sidebar-active ' : '' }}" href="{{ route('warehouse-type.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Warehouse
              Type</a>
          </li>
        </ul>
      </li>

      <!-- WareHouse -->
      <li class="{{ (request()->routeIs('warehouse.*') || request()->routeIs('warehouse-user.*')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> WareHouses</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="treeview-item {{ request()->routeIs('warehouse.*') ? 'child-sidebar-active' : '' }}" href="{{ route('warehouse.index') }}"><i class="icon fa fa-circle-o"></i>Warehouse
              Setup</a>
          </li>

          <li>
            <a class="treeview-item {{ request()->routeIs('warehouse-user.*') ? 'child-sidebar-active' : '' }}" href="{{ route('warehouse-user.index') }}"><i class="icon fa fa-circle-o"></i>Warehouse
              User</a>
          </li>
        </ul>
      </li>

      <!-- Inventory Location -->
      <li class="{{ (request()->routeIs('inventory-location.*')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Inventory Location</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="treeview-item {{ request()->routeIs('inventory-location.*') ? 'child-sidebar-active ' : '' }}" href="{{ route('inventory-location.index') }}"><i class="icon fa fa-circle-o"></i>
              Inventory
              Location</a>
          </li>
        </ul>
      </li>


      @endif

      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'warehouse_user' || Auth::user()->role == 'hub_user')
      <!-- Requisition -->
      <li class="{{ (request()->routeIs('requisition.*') || request()->routeIs('process')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Store Requisition</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('requisition.*') || request()->routeIs('process')? 'child-sidebar-active ' : '' }}" href="{{ route('requisition.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Pending Requisition</a>
          </li>

          <li>
            <a class="{{ request()->routeIs('') || request()->routeIs('') ? 'child-sidebar-active ' : '' }}" href="#"><i class="icon fa fa-circle-o pr-1"></i>
              Requisition List</a>
          </li>
        </ul>
      </li>

      <!-- Good Receive Note -->
      <li class="{{ (request()->routeIs('inventory-GRN.index')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Good Receive Note</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="treeview-item {{ request()->routeIs('inventory-GRN.index') ? 'child-sidebar-active ' : '' }}" href="{{ route('inventory-GRN.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Inventory
              GRN</a>
          </li>
        </ul>
      </li>

      <!-- Hub Stock Entry -->
      <li class="{{ (request()->routeIs('hubStockEntry.list') || request()->routeIs('hubStockEntry.create')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Stock Entry</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('hubStockEntry.list') || request()->routeIs('hubStockEntry.create') ? 'child-sidebar-active ' : '' }}" href="{{ route('hubStockEntry.list') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Ready For Stock</a>
          </li>
        </ul>
      </li>




      @if(Auth::user()->role == 'hub_user')
      <!-- Independent Stock Entry -->
      <li>
        <a class="treeview-item {{ request()->routeIs('independent.stock') ? 'child-sidebar-active ' : '' }}" href="{{ route('independent.stock') }}"><i class="icon fa fa-circle-o"></i>Independent Stock Entry</a>
      </li>
      @endif

      <!-- Barcode -->
      <li class="{{ (request()->routeIs('barcode.*')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Barcode</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('barcode.*') ? 'child-sidebar-active ' : '' }}" href="{{ route('barcode.index') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Barcode</a>
          </li>
        </ul>
      </li>

      <!-- Distribution -->
      <li class="{{ (request()->routeIs('distribution.*') || request()->routeIs('hold.*')
                    || request()->routeIs('purchase.*')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Distribution</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('distribution.*') ? 'child-sidebar-active ' : '' }}" href="{{ route('distribution.process') }}"><i class="icon fa fa-circle-o pr-1"></i> Regular Distribution</a>
          </li>

          <li>
            <a class="{{ request()->routeIs('hold.*') ? 'child-sidebar-active ' : '' }}" href="{{ route('hold.distribution') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Hold Distribution</a>
          </li>

          <li>
            <a class="{{ request()->routeIs('purchase.*') ? 'child-sidebar-active ' : '' }}" href="{{ route('purchase.distribution') }}"><i class="icon fa fa-circle-o pr-1"></i> Purchase Distribution</a>
          </li>
        </ul>
      </li>
      @endif

      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'hub_user' || Auth::user()->role == 'warehouse_user')
      <!-- Inventory -->
      <li class="{{ (request()->routeIs('hubStockEntry.availablestock')|| request()->routeIs('warehouse.return.products') || request()->routeIs('warehouse.damage.products') || request()->routeIs('hubStockEntry.availablestock.details')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Inventory</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('hubStockEntry.availablestock') || request()->routeIs('hubStockEntry.availablestock.details') ? 'child-sidebar-active ' : '' }}" href="{{ route('hubStockEntry.availablestock') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Available Stock</a>
          </li>
          <li>
            <a class="{{ request()->routeIs('warehouse.return.products') ? 'child-sidebar-active ' : '' }}" href="{{ route('warehouse.return.products') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Return Product</a>
          </li>
          <li>
            <a class="{{ request()->routeIs('warehouse.damage.products') ? 'child-sidebar-active ' : '' }}" href="{{ route('warehouse.damage.products') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Damage Product</a>
          </li>
        </ul>
      </li>
      @endif

      @if (Auth::user()->role == 'store_user' || Auth::user()->role == 'hub_user' || Auth::user()->role == 'admin')

          <!-- Stock Alert Report -->
              <li>
                  <a class="treeview-item {{ request()->routeIs('compare.stock.alert') ? 'child-sidebar-active ' : '' }}" href="{{ route('compare.stock.alert') }}"><i class="icon fa fa-circle-o"></i>Stock Alert</a>
              </li>
      @endif

      @if (Auth::user()->role == 'store_user')

      <!-- Store Inventory Report -->
      <li class="{{ (request()->is('store/store-inventory')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Store Inventory </span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="treeview-item {{ request()->is('store/store-inventory')  ? 'child-sidebar-active ' : '' }}" href="{{  url('store/store-inventory') }}"><i class="icon fa fa-circle-o"></i>
              Store Inventory</a>
          </li>
        </ul>
      </li>
      <!-- Store Stock Entry -->
      <li class="{{ (request()->routeIs('storeStockEntry.list')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Stock Entry</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ request()->routeIs('storeStockEntry.list') ? 'child-sidebar-active ' : '' }}" href="{{ route('storeStockEntry.list') }}"><i class="icon fa fa-circle-o pr-1"></i>
              Ready For Stock</a>
          </li>
        </ul>
      </li>
          <!-- Store Stock Entry -->
          <li class="{{ (request()->routeIs('store-barcode')) ? 'expanded_child_item' : '' }}">
              <a class="child-treeview" href="#">
                  <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Barcode</span>
                  <i class="child-treeview-indicator fa fa-angle-right"></i>
              </a>

              <ul class="child-treeview-menu">
                  <li>
                      <a class="{{ request()->routeIs('store-barcode') ? 'child-sidebar-active ' : '' }}" href="{{ route('store-barcode') }}"><i class="icon fa fa-circle-o pr-1"></i>
                          Barcode For Store</a>
                  </li>
              </ul>
          </li>
      @endif

      <!-- Expiry Alert -->
      {{-- <li>
                              <a class="treeview-item" href="#"><i class="icon fa fa-circle-o"></i>Expiry Alert</a>
                          </li> --}}

      <!-- Report -->
      {{-- <li>
                                  <a class="child-treeview" href="#">
                                      <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Report</span>
                                      <i class="child-treeview-indicator fa fa-angle-right"></i>
                                  </a>

                                  <ul class="child-treeview-menu">
                                      <li>
                                          <a class="{{ request()->routeIs('hubStockEntry.availablestock') || request()->routeIs('hubStockEntry.availablestock.*') ? 'sidebar-active ' : '' }}"
      href="#"><i class="icon fa fa-circle-o pr-1"></i>
      Stock Summnary Report</a>
  </li>

  <li>
    <a class="{{ request()->routeIs('hubStockEntry.availablestock') || request()->routeIs('hubStockEntry.availablestock.*') ? 'sidebar-active ' : '' }}" href="#"><i class="icon fa fa-circle-o pr-1"></i>
      Expiry Report</a>
  </li>
  </ul>
  </li> --}}
  </ul>
  </li>
  <!-- Inventory Management End -->
  @endif

  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'store_user' || Auth::user()->role == 'hub_user')
  {{-- Start Permission for admin and Developer --}}
  <!-- start POS -->
  <li class="treeview {{ (request()->routeIs('pos.create') || request()->routeIs('invoice.index')
        || request()->routeIs('pos.daily.report') || request()->routeIs('pos.sales.summary') ||
        request()->routeIs('pos.hub.sales.report') || request()->routeIs('sales-return.index') || request()->routeIs('invoice.show')|| request()->routeIs('invoice.create') || request()->routeIs('pos.create') || request()->routeIs('sales.batch_wise.report') || request()->routeIs('pos.hub.report.search')|| request()->routeIs('pos.product.filter') || request()->routeIs('get.product-search')|| request()->routeIs('pos.batch.filter')) ? 'is-expanded' : '' }}">
    <a class="app-menu__item" href="#" data-toggle="treeview">
      <i class="app-menu__icon fa fa-calculator"></i><span class="app-menu__label">Sales Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>
    <ul class="treeview-menu">
      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'store_user')
      <!-- pos -->
      @if (Auth::user()->role == 'store_user')
      <li class="{{ (request()->routeIs('pos.create')) ? 'expanded_child_item' : '' }}">
        <a class="treeview-item {{ request()->routeIs('pos.create') ? 'child-sidebar-active ' : '' }}" href="{{ route('pos.create') }}"><i class="icon fa fa-circle-o"></i>POS</a>
      </li>
      @endif


      <!-- sales invoice -->
      <li class="{{ (request()->routeIs('invoice.index') || request()->routeIs('invoice.show') || request()->routeIs('invoice.create')) ? 'expanded_child_item' : '' }}">
        <a class="treeview-item {{ (request()->routeIs('invoice.index') || request()->routeIs('invoice.show')|| request()->routeIs('invoice.create')) ? 'child-sidebar-active ' : '' }}" href="{{ route('invoice.index') }}"><i class="icon fa fa-circle-o"></i>Sales Invoices</a>
      </li><!-- end Sales Management-Sales Invoice -->

      <!-- pos return -->
      <li class="{{ (request()->routeIs('sales-return.index')) ? 'expanded_child_item' : '' }}">
        <a class="treeview-item {{ request()->routeIs('sales-return.index') ? 'child-sidebar-active ' : '' }}" href="{{ route('sales-return.index') }}"><i class="icon fa fa-circle-o"></i>Sales Return</a>
      </li><!-- end Sales Management-Sales Return -->

      <!-- return Invoice -->
      <li class="{{ (request()->routeIs('return-invoice.index')) ? 'expanded_child_item' : '' }}">
        <a class="treeview-item {{ request()->routeIs('return-invoice.index') ? 'child-sidebar-active ' : '' }}" href="{{ route('return-invoice.index') }}"><i class="icon fa fa-circle-o"></i>Return
          Invoice</a>
      </li><!-- end Sales Management-Return Invoice -->

      @endif


      <!-- Report -->
      <li class="{{ (request()->routeIs('pos.daily.report') || request()->routeIs('pos.sales.summary') ||
                request()->routeIs('pos.hub.sales.report') || request()->routeIs('hubStockEntry.availablestock')|| request()->routeIs('sales.batch_wise.report')|| request()->routeIs('pos.hub.report.search')|| request()->routeIs('pos.product.filter') || request()->routeIs('get.product-search') || request()->routeIs('pos.batch.filter')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Report</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          @if (Auth::user()->role == 'store_user')

          <li>
            <a class="treeview-item {{ request()->routeIs('pos.daily.report') ? 'child-sidebar-active ' : '' }}" href="{{ route('pos.daily.report') }}"><i class="icon fa fa-circle-o"></i>Daily
              Sales</a>
          </li>



          <li>
            <a class="treeview-item {{ request()->routeIs('pos.sales.summary') || request()->routeIs('pos.product.filter') || request()->routeIs('get.product-search') ? 'child-sidebar-active ' : '' }}" href="{{ route('pos.sales.summary') }}"><i class="icon fa fa-circle-o"></i>Sales
              Summary</a>
          </li>
          @endif

          @if (Auth::user()->role == 'hub_user')
          <li>
            <a class="treeview-item {{ request()->routeIs('pos.hub.sales.report')|| request()->routeIs('pos.hub.report.search') ? 'child-sidebar-active ' : '' }}" href="{{ route('pos.hub.sales.report') }}"><i class="icon fa fa-circle-o"></i>Hub
              Sales
              Summary</a>
          </li>

          <li>
            <a class="{{ request()->routeIs('') ? 'child-sidebar-active ' : '' }}" href="#"><i class="icon fa fa-circle-o pr-1"></i>
              Stock Report</a>
          </li>
          @endif
          <li>
            <a class="{{ request()->routeIs('sales.batch_wise.report') || request()->routeIs('pos.batch.filter') ? 'child-sidebar-active ' : '' }}" href="{{route('sales.batch_wise.report')}}"><i class="icon fa fa-circle-o pr-1"></i>
              Batch Wise Sales Report</a>
          </li>

          <li>
            <a class="{{ request()->routeIs('') ? 'child-sidebar-active ' : '' }}" href="#"><i class="icon fa fa-circle-o pr-1"></i>
              Product Wise Sales</a>
          </li>

        </ul>
        {{-- <li>
                                        <a class="treeview-item {{ request()->routeIs('customer.index') || request()->routeIs('customer.index.*') ? 'child-sidebar-active ' : '' }}"
        href="{{ route('customer.index') }}"><i class="icon fa fa-circle-o"></i>Customer</a>
      </li> --}}
  </li>
  </ul>
  </li>
  <!-- end POS -->
  {{-- End Permission for admin and Developer --}}
  @endif








  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer' || Auth::user()->role == 'store_user')
  <!-- start customer module -->
  <li class="treeview {{ (request()->routeIs('customer.index') || request()->routeIs('customer.create') ||request()->routeIs('customer.edit')||request()->routeIs('customer.show') || request()->routeIs('customer-group.index') || request()->routeIs('customer-group.create') || request()->routeIs('customer-group.edit')) ? 'is-expanded' : '' }}">
    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Customer Management</span><i class="treeview-indicator fa fa-angle-right"></i></a>

    <ul class="treeview-menu">

      <!-- Customer Manager -->
      <li class="{{ (request()->routeIs('customer.index') || request()->routeIs('customer.create') ||request()->routeIs('customer.edit') || request()->routeIs('customer.show')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Customer Manager</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ (request()->routeIs('customer.index') || request()->routeIs('customer.create') ||request()->routeIs('customer.edit') || request()->routeIs('customer.show')) ? 'child-sidebar-active ' : '' }}" href="{{ route('customer.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Customer
              Manager</a>
          </li>
        </ul>
      </li>

      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'developer')
      <!-- Customer Group -->
      <li class="{{ (request()->routeIs('customer-group.index') || request()->routeIs('customer-group.create') || request()->routeIs('customer-group.edit')) ? 'expanded_child_item' : '' }}">
        <a class="child-treeview" href="#">
          <span class="app-menu__label"> <i class="icon fa fa-circle-o"></i> Customer Group</span>
          <i class="child-treeview-indicator fa fa-angle-right"></i>
        </a>

        <ul class="child-treeview-menu">
          <li>
            <a class="{{ (request()->routeIs('customer-group.index') || request()->routeIs('customer-group.create') || request()->routeIs('customer-group.edit')) ? 'child-sidebar-active' : '' }}" href="{{ route('customer-group.index') }}"><i class="icon fa fa-circle-o pr-1"></i>Customer
              Group</a>
          </li>
        </ul>
      </li>
      @endif
  </li>
  <!-- end customer module -->
  @endif
  </ul>
</aside>

@push('post_scripts')

{{-- <script type="text/javascript">

            var lists = document.getElementById('app-menu').getElementsByTagName('a');

            console.log(lists);

            for (let i = 0; i <= lists.length - 1; i++) {
                console.log(lists[i].data('name'));
            }

            $(document).ready(function(){
                //
            });
        </script> --}}

@endpush
