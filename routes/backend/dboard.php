<?php

use App\Http\Controllers\Backend\DboardController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| All the administrative and dashboard routes base plate where all partial
| and particular route groups will be delivered
|
*/

Route::group(['prefix' => 'dboard', 'as' => 'dboard','middleware'=>'auth'], function () {
    Route::get('/', [DboardController::class,'dboard']);
    // template explorer routes
    // Route::get('/bootstrap-components', [DboardController::class,'bootstrap_components'])->name('.bootstrap-components');
    // Route::get('/cards', [DboardController::class,'cards'])->name('.cards');
    // Route::get('/form-components', [DboardController::class,'form_components'])->name('.form-components');
    // Route::get('/custom-form-components', [DboardController::class,'custom_form_components'])->name('.custom-form-components');
    // Route::get('/form-sample', [DboardController::class,'form_sample'])->name('.form-sample');
});