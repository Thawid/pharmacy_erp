<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Auth::routes(['register'=>false]);


Route::get('/', 'HomeController@index')->name('home');

require __DIR__.'/backend/dboard.php';

Route::get('json', function(){
    
$json = file_get_contents(public_path('medicine.json'));
$array = json_decode($json, true);
echo "<pre>";
print_r($array);
echo "</pre>";
});
