<?php

namespace App\Helpers;

use Storage;

class FileUploadHelper
{
    public function uploadFile($file, $storageName, $checkFile = null, $maxSize = null, $fileName = null)
    {
        if ($file) {
            // check file size
            $fileSize = $file->getSize();
            // 1 mb = 1048576 bytes in binary which is countable for the file size here
            if($maxSize !== 0 && $maxSize !== null){
                if ($fileSize > $maxSize) {
                    return 'MaxSizeErr';
                }
            }
            // check existing file
            if($checkFile != null) {
                if (Storage::disk($storageName)->has($checkFile)) {
                    Storage::disk($storageName)->delete($checkFile);
                }
            }
            // rename file
            if($fileName == null) {
                $fileName = date('Y_m_d_h_i_s') . '.' . $file->getClientOriginalExtension();
            }else{
                $fileName = str_replace(' ', '_', $fileName) . '.' . $file->getClientOriginalExtension();
            }
            // upload or store file in the particular storage directory
            $file->move($storageName, $fileName);
            //$uploadedFile = Storage::disk($storageName)->put($fileName, $file);

            return $fileName;

        }else{
            return 'No file found';
        }
    }
}