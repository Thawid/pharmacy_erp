<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Repositories\ProductGenericInterface;
use Modules\Product\Http\Requests\CreateProductGenericRequest;
use Modules\Product\Http\Requests\UpdateProductGenericRequest;
use Modules\Product\Entities\ProductType;

class ProductGenericController extends Controller
{
    private $productGenericRepository;

    public function __construct(ProductGenericInterface $productGenericRepository)
    {
        $this->productGenericRepository = $productGenericRepository;
    }

    public function index()
    {
        $product_generic = $this->productGenericRepository->index();
        return view('product::product-management.product-generic.index', compact('product_generic'));
    }

    public function create()
    {
        return view('product::product-management.product-generic.create');
    }

    public function store(CreateProductGenericRequest $request)
    {
        $this->productGenericRepository->store($request);
        return redirect()->route('generics.index')->with('success', 'Product Generic has been created successfully');
    }

    public function edit($id)
    {
        $product_generic = $this->productGenericRepository->edit($id);
        $product_types = ProductType::where('status',1)->get(['id','name']);
        return view('product::product-management.product-generic.edit', compact(['product_generic', 'product_types']));
    }

    public function update(UpdateProductGenericRequest $request, $id)
    {
        $this->productGenericRepository->update($request, $id);
        return redirect()->route('generics.index')->with('success', 'Product Generic has been updated successfully');
    }

    public function destroy($id)
    {
        $this->productGenericRepository->destroy($id);
        return redirect()->back()->with('success', 'Product Generic status has been changed successfully');
    }

    public function getProductGeneric($id)
    {
        $this->productGenericRepository->getProductGeneric($id);
    }
}
