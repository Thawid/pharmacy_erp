<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Product\Http\Requests\CreateUOMRequest;
use Modules\Product\Http\Requests\UpdateUOMRequest;
use Modules\Product\Repositories\UOMInterface;
use Modules\Product\Entities\UOM;
use Illuminate\Routing\Controller;

class UOMController extends Controller
{
    protected $uomInterface;

    public function __construct(UOMInterface $uomInterface)
    {
        $this->uomInterface = $uomInterface;
    }

    public function index()
    {
        $uoms = $this->uomInterface->index();
        return view('product::product-management.uom.index', compact('uoms'));
    }

    public function create()
    {
        return view('product::product-management.uom.create');
    }

    public function store(CreateUOMRequest $request)
    {
        $this->uomInterface->store($request);
        return redirect()->route('uom.index')->with('success', 'Units Of Measurement has been created successfully');
    }

    public function show($id)
    {
        return view('product::show');
    }

    public function edit($id)
    {
        $uom = $this->uomInterface->edit($id);
        return view('product::product-management.uom.edit', compact('uom'));
    }

    public function update(UpdateUOMRequest $request, $id)
    {
        $this->uomInterface->update($request, $id);
        return redirect()->route('uom.index')->with('success', 'Units Of Measurement has been updated successfully');
    }

    public function destroy($id)
    {
        $this->uomInterface->destroy($id);
        return redirect()->route('uom.index')->with('success', 'Units Of Measurements Status has been changed successfully');
    }
}
