<?php

namespace Modules\Product\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use Modules\Store\Entities\StoreType;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ManageStoreProduct;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\Product\Repositories\ManageStoreProductInterface;
use Modules\Product\Http\Requests\CreateManageStoreProductRequest;
use Modules\Product\Http\Requests\UpdateManageStoreProductRequest;

class ManageStoreProductController extends Controller
{
    private $manageStoreProduct;

    public function __construct(ManageStoreProductInterface $manageStoreProduct)
    {
        $this->manageStoreProduct = $manageStoreProduct;
    }

    public function index()
    {
        $store_types = StoreType::select('id', 'name')->where('status', 1)->get();
        $stores = Store::select('id', 'name', 'address', 'code')->where('status', 1)->where('store_type', '=', 'store')->get();
        $manageStoreProduct = $this->manageStoreProduct->index();
        return view('product::product-management.manage-store-products.index', compact('manageStoreProduct', 'store_types', 'stores'));
    }

    public function activeProducts($id)
    {
        $product_id = ManageStoreProduct::where('store_id', $id)->pluck('product_id');
        $products = Product::with('productDetails')->whereIn('id', $product_id)->get(['id', 'product_name']);
        return view('product::product-management.manage-store-products.active-products', compact('products'));

    }

    public function create(Request $request)
    {
        $store_types = StoreType::select('id', 'name')->where('id', $request->store_type_name)->first();
        $stores = Store::select('id', 'name', 'address', 'code', 'store_type_id')->where('id', $request->store_name)->get();
        $manageStoreProduct = $this->manageStoreProduct->index();
        return view('product::product-management.manage-store-products.view', compact('manageStoreProduct', 'store_types', 'stores'));
    }

    public function store(Request $request)
    {
        /*$qtys = array_filter($request->qty);
        $qty = array_values($qtys);*/
        $hub_id = Store::where('id', $request->store_id)->value('hub_id');

        try {
            $i = 0;
            foreach ($request->product_id as $product_id) {
                ManageStoreProduct::updateOrInsert(
                    [
                        'store_id' => $request->store_id,
                        'product_id' => $product_id
                    ],
                    [
                        'status' => 1,
                    ]
                );
                $i++;
            }

            if (HubAvailableProduct::where('hub_id', $hub_id)->exists()) {
                $HubAvailableProductId = HubAvailableProduct::where('hub_id', $hub_id)->value('id');
            } else {
                $HubAvailableProduct = new HubAvailableProduct();
                $HubAvailableProduct->hub_id = $hub_id;
                $HubAvailableProduct->status = 0;
                $HubAvailableProduct->save();
                $HubAvailableProductId = $HubAvailableProduct->id;
            }

            $i = 0;
            foreach ($request->product_id as $product_id) {
                HubAvailableProductDetails::updateOrInsert(
                    [
                        'hub_available_product_id' => $HubAvailableProductId,
                        'product_id' => $product_id,
                        'unit_id' => $request->unit_id,
                        'uom_id' => $request->purchase_uom_id,
                        'available_quantity' => 0,
                    ],
                    [
                        'status' => 1,
                    ]
                );
                $i++;
            }

            /*store availabe */
            if (StoreAvailableProduct::where('store_id', $request->store_id)->exists()) {
                $StoreAvailableProductId = StoreAvailableProduct::where('store_id', $request->store_id)->value('id');
            } else {
                $StoreAvailableProduct = new StoreAvailableProduct();
                $StoreAvailableProduct->store_id = $request->store_id;
                $StoreAvailableProduct->status = 0;
                $StoreAvailableProduct->save();
                $StoreAvailableProductId = $StoreAvailableProduct->id;
            }

            $i = 0;
            foreach ($request->product_id as $product_id) {
                StoreAvailableProductDetails::updateOrInsert(
                    [
                        'store_available_product_id' => $StoreAvailableProductId,
                        'product_id' => $product_id,
                        'unit_id' => $request->unit_id,
                        'uom_id' => $request->purchase_uom_id,
                        'available_quantity' => 0,
                    ],
                    [
                        'status' => 1,
                    ]
                );
                $i++;
            }
            /*endstore availabe */

            return redirect()->back()->with('success', "Product Store Successfully");
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors("Something went wrong " . $e->getMessage());
        }
    }

    public function show(CreateManageStoreProductRequest $request, $id)
    {
        $stores = Store::select('id', 'name', 'code')->where('id', $id)->first();
        $product_id = Product::select('id')->where('id', $request->name)->first(); // get request product id
        $products = Product::with(['ptype', 'productCategory', 'manufacturer', 'pgeneric'])->where('status', 1)->get();
        if ($request->name == "") {
            return redirect()->back()->withInput()->withErrors("Please Select Product");
        } else {
            return view('product::product-management.manage-store-products.show', compact('stores', 'products', 'product_id'));
        }
    }

    public function edit(CreateManageStoreProductRequest $request, $id)
    {
        $stores = Store::select('id', 'name', 'code')->where('id', $id)->first();
        $products = Product::with(['ptype', 'productCategory', 'manufacturer', 'pgeneric'])->where('status', 1)->get();

        return view('product::product-management.manage-store-products.edit', compact('stores', 'products'));
    }

    public function update(UpdateManageStoreProductRequest $request, $id)
    {
        return 'update';
    }

    public function destroy($id)
    {
        return 'destroy';
    }

    public function storeProductList()
    {
        $userId = auth()->user()->id;
        $store_id = StoreUser::where('store_type', '=', "store")->where('user_id', $userId)->value('store_id');
        $hub_id = StoreUser::where('store_type', '=', "hub")->where('user_id', $userId)->value('store_id');
        $stores = Store::select('id', 'name', 'code')->where('id', $store_id)->first();
        if ($store_id) {
            $product_id = ManageStoreProduct::where('store_id', $store_id)->pluck('product_id');
        } else {
            $product_id = ManageStoreProduct::where('store_id', $hub_id)->pluck('product_id');
        }
        $products = Product::with('productDetails')->whereIn('id', $product_id)->get(['id', 'product_name', 'unit_id', 'purchase_uom_id']);
        return view('product::product-management.manage-store-products.store-product', compact('products', 'stores'));
    }

    public function setupStockAlertSave(Request $request)
    {
        $userId = auth()->user()->id;
        $store_id = StoreUser::where('store_type', '=', "store")->where('user_id', $userId)->value('store_id');
        $hub_id = StoreUser::where('store_type', '=', "hub")->where('user_id', $userId)->value('store_id');
        $qtys = array_filter($request->qty);
        $qty = array_values($qtys);
        if ($store_id) {
            if (count($qty) > 0) {
                try {
                    if (StoreAvailableProduct::where('store_id', $request->store_id)->exists()) {
                        $StoreAvailableProductId = StoreAvailableProduct::where('store_id', $request->store_id)->value('id');
                    } else {
                        $StoreAvailableProduct = new StoreAvailableProduct();
                        $StoreAvailableProduct->store_id = $request->store_id;
                        $StoreAvailableProduct->status = 0;
                        $StoreAvailableProduct->save();
                        $StoreAvailableProductId = $StoreAvailableProduct->id;
                    }

                    $j = 0;
                    foreach ($request->product_id as $product_id) {
                        DB::table('store_available_product_details')
                            ->where('store_available_product_id', $StoreAvailableProductId)
                            ->where('product_id', $product_id)
                            ->delete();

                        StoreAvailableProductDetails::updateOrInsert([
                            'store_available_product_id' => $StoreAvailableProductId,
                            'product_id' => $product_id,
                            'unit_id' => $request->unit_id,
                            'uom_id' => $request->purchase_uom_id,
                            'available_quantity' => 0,
                            'stock_alert' => isset($qty[$j]) ? $qty[$j] : null,
                        ],
                            [
                                'status' => 1,
                            ]
                        );
                        $j++;
                    }
                    return redirect()->back()->with('success', "Store Stock Alert Set Successful");
                } catch (\Exception $e) {
                    return redirect()->back()->withInput()->withErrors("Something went wrong " . $e->getMessage());
                }
            } else {
                return redirect()->back()->withInput()->withErrors("Please fill up stock alert min qty");
            }
        }
        if ($hub_id) {
            if (count($qty) > 0) {
                $hub_id = Store::where('id', $request->store_id)->value('hub_id');
                try {
                    if (HubAvailableProduct::where('hub_id', $hub_id)->exists()) {
                        $HubAvailableProductId = HubAvailableProduct::where('hub_id', $hub_id)->value('id');
                    } else {
                        $HubAvailableProduct = new HubAvailableProduct();
                        $HubAvailableProduct->hub_id = $hub_id;
                        $HubAvailableProduct->status = 0;
                        $HubAvailableProduct->save();
                        $HubAvailableProductId = $HubAvailableProduct->id;
                    }

                    $i = 0;
                    foreach ($request->product_id as $product_id) {
                        DB::table('hub_available_product_details')
                            ->where('hub_available_product_id', $HubAvailableProductId)
                            ->where('product_id', $product_id)
                            ->delete();

                        HubAvailableProductDetails::updateOrInsert([
                            'hub_available_product_id' => $HubAvailableProductId,
                            'product_id' => $product_id,
                            'unit_id' => $request->unit_id,
                            'uom_id' => $request->purchase_uom_id,
                            'available_quantity' => 0,
                            'stock_alert' => isset($qty[$i]) ? $qty[$i] : null,
                        ],
                            [
                                'status' => 1,
                            ]
                        );
                        $i++;
                    }
                    return redirect()->back()->with('success', "Hub Stock Alert Set Successful");
                } catch (\Exception $e) {
                    return redirect()->back()->withInput()->withErrors("Something went wrong " . $e->getMessage());
                }
            } else {
                return redirect()->back()->withInput()->withErrors("Please fill up stock alert min qty");
            }
        }

    }

    public function getProductVariations($id)
    {
        $product = Product::with('productVariations')->where('id', $id)->first();
        $response = '';
        foreach ($product->productVariations as $key => $variations) {
            $response .= "<tr>";
            $response .= "<td>" . $variations->variant->name . "</td>";
            $response .= "<td>" . $variations->variantProperty->name . "</td>";
            $response .= "</tr>";
        }
        return $response;
    }
}
