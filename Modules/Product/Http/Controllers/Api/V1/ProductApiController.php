<?php

namespace Modules\Product\Http\Controllers\Api\V1;

use GuzzleHttp\Psr7\Message;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\Store\Entities\StoreUser;
use Illuminate\Support\Arr;
use Modules\POS\Entities\Customer;
use Modules\Price\Entities\CustomerGroup;

class ProductApiController extends Controller
{
    
    
    public function __construct()
    {
        $get_store_by_auth_user = StoreUser::where('user_id', '=', 1)->firstOrFail();
        $auth_user_store_id = $get_store_by_auth_user->store_id;
        $get_store_available_product = StoreAvailableProduct::where('store_id', '=', $auth_user_store_id)->firstOrFail();
        $this->get_store_available_product_id= $get_store_available_product->id;
        $this->get_auth_user_store_id = $auth_user_store_id ;
    }


    public function products()
    {


        $all_products_info = DB::table('products')
        ->join('product_generics','products.product_generic_id','product_generics.id')
        ->join('product_details','products.id','product_details.product_id')    
        ->select('product_name as name','brand_name as brand','product_generics.name as generic','product_details.feature_image','product_details.short_desc as short description','product_details.long_desc as large description',)
        ->get();

        return response()->json([
            'status' => 'success',
            'message' => 'All Products Data',
            'data' => [
                'all-products' => $all_products_info,
            ]

        ], 200);

        
    }

    


    public function searchProduct(Request $request)
    {


        $name = $request->product_name;

        $productData = DB::table('store_available_products')

            ->join('store_available_product_details', 'store_available_products.id', '=', 'store_available_product_details.store_available_product_id')
            ->join('products', 'store_available_product_details.product_id', '=', 'products.id')
            ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
            ->select('products.id as product_id', 'products.product_name', 'product_generics.id as generic_id', 'product_generics.name as generic_name')
            ->where('store_available_product_details.store_available_product_id', '=',      $this->get_store_available_product_id)
            ->Where('products.product_name', 'LIKE', "%{$name}%")
            ->limit(5)
            ->get();


        if (count($productData) == 0) {

            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'No Product Found',
                    'data' => [
                        'products' => $productData
                    ]

                ],
                200
            );
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Product list by name',
                'data' => [
                    'products' => $productData
                ]

            ], 200);
        }
    }


    public function productDetails(Request $request)
    {

        $product_id=$request->product_id;
        $customer_mobile_no = $request->phone_number;

        $store_available_product = StoreAvailableProductDetails::where('store_available_product_id',$this->get_store_available_product_id)->firstOrFail();
        $store_available_product_quantity= $store_available_product->available_quantity;
       
        $product_uoms  = DB::table('uom_prices')
        ->join('uoms','uoms.id','=','uom_prices.uom_id')
        ->Where('uom_prices.product_id', '=', $product_id)
        ->select('uoms.uom_name as name','uom_prices.uom_price as regular_price', 'uom_prices.capacity  as quantity')
        ->get();

        $store_prices_and_discount = DB::table('store_prices')
        ->join('uoms','uoms.id','=','store_prices.uom_id')
        ->select('uoms.uom_name as name','regular_price as store_regular_price','store_discount_value')
        ->Where('store_prices.store_id', '=', $this->get_auth_user_store_id )
        ->Where('store_prices.product_id', '=', $product_id)
        ->get();

        $vat_and_taxes = DB::table('price_vat_tax_details')
        ->join('uoms','uoms.id','=','price_vat_tax_details.uom_id')
        ->Where('price_vat_tax_details.product_id', '=', $product_id)
        ->select('uoms.uom_name as name','vat_tax_value')
        ->get();

        $store_vat_and_taxes = DB::table('store_vat_tax_prices')
        ->join('uoms','uoms.id','=','store_vat_tax_prices.uom_id')
        ->select('uoms.uom_name as name','store_vat_tax_value as store_vat')
        ->Where('store_vat_tax_prices.store_id', '=', $this->get_auth_user_store_id )
        ->Where('store_vat_tax_prices.product_id', '=', $product_id)
        ->get();


        $all_customer_get_discount = [];
        $group_customer_get_discount = [];

        $customer_total_expense = Customer::select('total_expense')->Where('customers.mobile_no', '=', $customer_mobile_no)->first();

        if($customer_total_expense == null){
             
         $all_customer_get_discount=  DB::table('price_customer_discount_details')
            ->join('uoms','uoms.id','=','price_customer_discount_details.uom_id')
            ->Where('price_customer_discount_details.product_id', '=', $product_id)
            ->select('uoms.uom_name as name','discount_value')
            ->get();

        }else{
            
            $customer_group_expense =CustomerGroup::Where('min_expense','<=',$customer_total_expense->total_expense)
            ->Where('max_expense','>=',$customer_total_expense->total_expense)
            ->select('id','group_name')->first();
            if($customer_group_expense){
                $group_customer_get_discount=  DB::table('customer_group_prices')
                ->join('uoms','uoms.id','=','customer_group_prices.uom_id')
                ->Where('customer_group_prices.product_id', '=', $product_id)
                ->select('uoms.uom_name as name','customer_discount_value')
                ->get();
            }else{
                $all_customer_get_discount=  DB::table('price_customer_discount_details')
                ->join('uoms','uoms.id','=','price_customer_discount_details.uom_id')
                ->Where('price_customer_discount_details.product_id', '=', $product_id)
                ->select('uoms.uom_name as name','discount_value')
                ->get();
            }

        }


       

      

       
        
        
        





        




        $data = [];
       
        foreach($product_uoms as $uoms){
            if($uoms->quantity){
                $uoms->quantity = floor($store_available_product_quantity / $uoms->quantity);   
            }  


            foreach ($all_customer_get_discount as $customer_discount) {
                if($customer_discount->name == $uoms->name){
                    $uoms->{'customer_discount'} = $customer_discount->discount_value;
                }
            }

            foreach ($group_customer_get_discount as $group_customer_discount) {
                if($group_customer_discount->name == $uoms->name){
                    $uoms->{'group_customer_discount'} = $group_customer_discount->customer_discount_value;
                }
            }


            
                foreach ($store_prices_and_discount as  $value) {
                  if($value->name == $uoms->name){
                    $uoms->{'store_price'} = $value->store_regular_price;
                    $uoms->{'store_discount'} = $value->store_discount_value;
                  }  
                }


                


                foreach($vat_and_taxes as $vat){
                    if($vat->name == $uoms->name){
                        $uoms->{'vat_tax'} = $vat->vat_tax_value;
                    }
                }

                foreach($store_vat_and_taxes as $store_vat){
                    if($store_vat->name == $uoms->name){
                        $uoms->{'store_vat_tax'} =  $store_vat->store_vat;
                    }
                }



                array_push($data, $uoms);       
            }

           



        if (count($data) == 0) {

            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'No Product Details Found',
                    'data' => [
                         $data
                    ]

                ],
                200
            );
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Product Details',
                'data' => [
                    'product'=>[
                        'id' => $product_id,
                        'uoms' => $data
                    ]
                   
                ]

            ], 200);
        }

       

       

        



        
       



        
    }
}
