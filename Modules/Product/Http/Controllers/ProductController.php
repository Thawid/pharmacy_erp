<?php

namespace Modules\Product\Http\Controllers;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Manufacturer;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductType;
use Modules\Product\Entities\Variant;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\UOM;
use Modules\Product\Entities\VariantProperty;
use Modules\Product\Repositories\ProductInterface;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
    private $productRepository;
    public function __construct(ProductInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $products = $this->productRepository->index();
        return view('product::product-management.product.index', compact('products'));
    }

    /*public function getProducts(Request $request)
    {
        return $this->productRepository->getProducts($request);
    }*/

    public function create()
    {
        $manufacturers = Manufacturer::where('status', 1)->get(['id', 'name']);
        $uoms = UOM::where('status', 1)->get(['id', 'uom_name']);
        return view('product::product-management.product.create', compact(['manufacturers', 'uoms']));
    }

    public function store(CreateProductRequest $request)
    {
        // return $request->all();
        $this->productRepository->store($request);
        return redirect()->route('products.index');
    }

    public function show($id)
    {
        $products = $this->productRepository->show($id);
        return view('product::product-management.product.show', compact('products'));
    }

    public function edit($id)
    {
        $product_types = ProductType::where('status', 1)->get(['id', 'name']);
        $product_categories = ProductCategory::where('status', 1)->where('parent_id', null)->get(['id', 'name']);
        $product_subcats = ProductCategory::where('status', 1)->where('parent_id', '!=', null)->get(['id', 'name']);
        $product_generics = ProductGeneric::where('status', 1)->get(['id', 'name']);
        $manufacturers = Manufacturer::where('status', 1)->get(['id', 'name']);
        $variants = Variant::where('status', 1)->get(['id', 'name']);
        $variant_properties = VariantProperty::where('status', 1)->get(['id', 'name']);
        $units = ProductUnit::where('status', 1)->get(['id', 'unit_name']);
        $uoms = UOM::where('status', 1)->get(['id', 'uom_name']);
        $products = $this->productRepository->edit($id);
        return view('product::product-management.product.edit', compact('products', 'product_types', 'product_categories', 'product_subcats', 'product_generics', 'manufacturers', 'variants', 'variant_properties', 'units', 'uoms'));
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $this->productRepository->update($request, $id);
        return redirect()->route('products.index');
    }

    public function destroy($id)
    {
        $this->productRepository->destroy($id);
        return redirect()->back()->with('success', 'Product has been deleted successfully');
    }

    public function productNameCheck()
    {
        if (Product::where('product_name', request()->product_name)->exists()) {
            return "Product Name already exists!";
        } else {
        }
    }
}
