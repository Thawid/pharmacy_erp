<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Product\Repositories\ManufacturerInterface;
use Modules\Product\Http\Requests\CreateManufacturerRequest;
use Modules\Product\Http\Requests\UpdateManufacturerRequest;
use Modules\Product\Entities\Manufacturer;

class ManufacturerController extends Controller
{
    private $manufacturerInterface;

    public function __construct(ManufacturerInterface $manufacturerInterface)
    {
        $this->manufacturerInterface = $manufacturerInterface;
    }

    public function index()
    {
        $manufacturers = $this->manufacturerInterface->index();
        return view('product::product-management.manufacturer.index', compact('manufacturers'));
    }

    public function create()
    {
        return view('product::product-management.manufacturer.create');
    }

    public function store(CreateManufacturerRequest $request)
    {
        $this->manufacturerInterface->store($request);
        return redirect()->route('manufacturers.index')->with('success', 'Manufacturers has been created successfully');
    }

    public function show($id)
    {
//
    }

    public function edit($id)
    {
        $manufacturer = $this->manufacturerInterface->edit($id);
        return view('product::product-management.manufacturer.edit', compact('manufacturer'));
    }

    public function update(UpdateManufacturerRequest $request, $id)
    {
        $this->manufacturerInterface->update($request, $id);
        return redirect()->route('manufacturers.index')->with('success', 'Manufacturers has been updated successfully');
    }

    public function destroy($id)
    {
        $this->manufacturerInterface->destroy($id);
        return redirect()->route('manufacturers.index')->with('success', 'Manufacturers has been updated successfully');
    }
}
