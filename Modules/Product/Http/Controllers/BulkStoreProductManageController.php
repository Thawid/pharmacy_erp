<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Store\Entities\Store;
use Modules\Product\Entities\CopyStore;
use Modules\Product\Entities\ManageStoreProduct;

class BulkStoreProductManageController extends Controller
{
    public function index()
    {
        $stores = Store::select('id', 'name', 'address', 'code')->where('status', 1)->get();
        return view('product::product-management.bulk-store-product.index', compact('stores'));
    }

    public function create()
    {
        return view('product::create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'copy_from_store_id' => 'required',
            'copy_to_store_id' => 'required'
        ]);
        DB::beginTransaction();
        try {
            $copyStore = new CopyStore();
            $copyStore->copy_from_store_id = $request->copy_from_store_id;
            $copyStore->copy_to_store_id = $request->copy_to_store_id;
            $copyStore->status = 1;
            $copyStore->save();

            if ($request->has('product_id')) {
                $i = 0;
                foreach ($request->product_id as $product_id) {
                    ManageStoreProduct::updateOrInsert(
                        [
                            'store_id' => $request->copy_to_store_id,
                            'product_id' => $product_id,
                            'qty' => $request->qty[$i],
                        ],
                        [
                            'status' => 1,
                        ]
                    );
                    $i++;
                }
            }
            DB::commit();
            return redirect()->back()->with('success', "Product Copy Successfully");


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function show($id)
    {
        return view('product::show');
    }

    public function edit($id)
    {
        return view('product::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function checkManageStore($id)
    {
        if (ManageStoreProduct::where('store_id', $id)->exists()) {
            $data = ManageStoreProduct::where('store_id', $id)->get();
        } else {
            $data = "No Product Assign";
        }
        return json_encode($data);

    }
}
