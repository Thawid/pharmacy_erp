<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\AdvanceBundle;
use Modules\Product\Entities\AdvanceBundleProduct;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Repositories\AdvanceBundleInterface;
use Modules\Product\Http\Requests\CreateAdvanceBundleRequest;
use Modules\Product\Http\Requests\UpdateAdvanceBundleRequest;
use Illuminate\Support\Facades\DB;

class AdvanceBundleController extends Controller
{
    private $advanceBundleInterface;
    public function __construct(AdvanceBundleInterface $advanceBundleInterface)
    {
        $this->advanceBundleInterface = $advanceBundleInterface;
    }

    public function index()
    {
        $advanceBundle = AdvanceBundle::with('advanceProducts')->get();
        return view('product::product-management.product-bundle.advance.index', compact('advanceBundle'));
    }

    public function create()
    {
        $uoms = DB::table('uoms')->get();
        $products = Product::where('status', 1)->get(['id', 'product_name']);
        return view('product::product-management.product-bundle.advance.create', compact('products', 'uoms'));
    }


    public function store(CreateAdvanceBundleRequest $request)
    {
        $this->advanceBundleInterface->store($request);
        return redirect()->route('advance.index')->with('success', 'Advance Product Bundle has been Created successfully');
    }


    public function show($id)
    {
        $advanceBundle = AdvanceBundle::with(['advanceProducts'])->where('id', $id)->first();
        return view('product::product-management.product-bundle.advance.show', compact('advanceBundle'));
    }

    public function edit($id)
    {
        $uoms = DB::table('uoms')->get();
        $products = Product::where('status', 1)->get(['id', 'product_name']);
        $advanceBundle = AdvanceBundle::with(['advanceProducts'])->where('id', $id)->first();

        return view('product::product-management.product-bundle.advance.edit', compact(['products', 'advanceBundle', 'uoms']));
    }

    public function update(UpdateAdvanceBundleRequest $request, $id)
    {
        $this->advanceBundleInterface->update($request, $id);
        return redirect()->route('advance.index');
    }

    public function destroy($id)
    {
        $this->advanceBundleInterface->destroy($id);
        return redirect()->back()->with('success', 'Advance Product Bundle has been changed successfully');
    }

    public function getProduct($id)
    {
        $this->advanceBundleInterface->getProduct($id);
    }
}
