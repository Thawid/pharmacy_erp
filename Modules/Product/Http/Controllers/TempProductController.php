<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Support\Renderable;
use Modules\Product\Entities\TempProduct;
use DB;

class TempProductController extends Controller
{
    public function storeDummyData(Request $request)
    {
        try {
            TempProduct::truncate();
            if ($request->has('sub_category_id')) {
                $i = 0;
                foreach ($request->sub_category_id as $sub_category) {
                    TempProduct::updateOrInsert(
                        [
                            'sub_category_id' => $sub_category,
                            'product_name' => $request->product_name[$i],
                            /*'variant_id' => $request->variant_id[$i],
                            'variant_property_id' => $request->variant_property_id[$i],*/
                        ],
                        ['unit_id' => $request->unit_id[$i],
                        ]
                    );
                    $i++;
                }
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getDummyData()
    {
        echo json_encode(TempProduct::where('sub_category_id', '!=', null)->get('product_name'));
        //echo json_encode(TempProduct::with(['variant','variantValues'])->get('product_name'));
    }
}
