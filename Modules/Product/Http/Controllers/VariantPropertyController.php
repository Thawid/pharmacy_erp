<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;

use Modules\Product\Entities\Variant;
use Modules\Product\Entities\VariantProperty;
use Illuminate\Routing\Controller;
use Modules\Product\Repositories\VariantPropertyInterface;
use Modules\Product\Http\Requests\CreateVariantPropertyRequest;
use Modules\Product\Http\Requests\UpdateVariantPropertyRequest;


class VariantPropertyController extends Controller
{
    private $variantPropertyRepository;

    public function __construct(VariantPropertyInterface $variantPropertyRepository)
    {
        $this->variantPropertyRepository = $variantPropertyRepository;
    }
    public function index()
    {
        $variant_properties = $this->variantPropertyRepository->index();
        //return $variant_properties;
        return view('product::product-management.variant-property.index', compact('variant_properties'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('product::product-management.variant-property.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateVariantPropertyRequest $request)
    {
        $this->variantPropertyRepository->store($request);
        return redirect()->route('variant_properties.index')->with('success', 'Variant Property has been created successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $variants = Variant::where('status',1)->get();
        $variant_property = $this->variantPropertyRepository->edit($id);
        return view('product::product-management.variant-property.edit',compact('variants','variant_property'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateVariantPropertyRequest $request, $id)
    {
        $this->variantPropertyRepository->update($request, $id);
        return redirect()->route('variant_properties.index')->with('success', 'Variant Property has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $this->variantPropertyRepository->destroy($id);
        return redirect()->back()->with('success','Variant Property status has been changed successfully');
    }
}
