<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Support\Renderable;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Repositories\ProductTypeInterface;
use Modules\Product\Repositories\ProductCategoryInterface;

class ProductCategoryController extends Controller
{
    private $productCategoryRepository;
    private $productTypeRepository;
    public function __construct(ProductCategoryInterface $productCategoryRepository, ProductTypeInterface $productTypeRepository)
    {
        $this->productCategoryRepository = $productCategoryRepository;
        $this->productTypeRepository = $productTypeRepository;
    }

    public function index()
    {
        $product_categories = $this->productCategoryRepository->index();
        //return $product_categories;
        return view('product::product-management.category.index', compact('product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('product::product-management.category.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {

        $this->productCategoryRepository->store($request);
        return redirect()->route('category.index')->with('success','Product Category has been created successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $product_types = $this->productTypeRepository->index();
        $product_categories = ProductCategory::whereNull('parent_id')->get(['id','name']);
        $product_category = $this->productCategoryRepository->edit($id);
        return view('product::product-management.category.edit', compact('product_category','product_categories','product_types'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $this->productCategoryRepository->update($request,$id);
        return redirect()->route('category.index')->with('success','Product Category has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $this->productCategoryRepository->destroy($id);
        return redirect()->back()->with('success','Product Category status has been changed successfully');
    }

    public function getProductCategory($id)
    {
        $this->productCategoryRepository->getProductCategory($id);
    }

    public function getProductSubCategory($id)
    {
        $this->productCategoryRepository->getProductSubCategory($id);
    }

    public function getVariantProperty($id)
    {
        $this->productCategoryRepository->getVariantProperty($id);
    }
}
