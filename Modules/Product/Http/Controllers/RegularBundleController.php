<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Store\Entities\StoreUser;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\RegularBundle;
use Modules\Product\Entities\ManageStoreProduct;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\RegularBundleProduct;
use Modules\Product\Repositories\RegularBundleInterface;
use Modules\Product\Http\Requests\CreateRegularBundleRequest;
use Modules\Product\Http\Requests\UpdateRegularBundleRequest;
use Illuminate\Support\Facades\DB;

class RegularBundleController extends Controller
{
    private $regularBundleInterface;
    public function __construct(RegularBundleInterface $regularBundleInterface)
    {
        $this->regularBundleInterface = $regularBundleInterface;
    }

    public function index()
    {
        $regularBundle = RegularBundle::with('bundleProducts')->get();
        return view('product::product-management.product-bundle.regular.index', compact('regularBundle'));
    }

    public function create()
    {
        $userId = auth()->user()->id;
        $store_id = StoreUser::where('user_id', $userId)->value('store_id');
        $product_id = ManageStoreProduct::where('store_id', $store_id)->pluck('product_id');
        $products = Product::whereIn('id', $product_id)->where('status', 1)->get(['id', 'product_name']);
        $uoms = DB::table('uoms')->get();
        return view('product::product-management.product-bundle.regular.create', compact('products', 'uoms'));
    }


    public function store(CreateRegularBundleRequest $request)
    {
        $this->regularBundleInterface->store($request);
        return redirect()->route('regular.index');
    }


    public function show($id)
    {
        $regularBundle = RegularBundle::with(['bundleProducts'])->where('id', $id)->first();
        return view('product::product-management.product-bundle.regular.show', compact('regularBundle'));
    }

    public function edit($id)
    {
        $uoms = DB::table('uoms')->get();
        $products = Product::where('status', 1)->get(['id', 'product_name']);
        $regularBundle = RegularBundle::with(['bundleProducts'])->where('id', $id)->first();
        return view('product::product-management.product-bundle.regular.edit', compact('regularBundle', 'products', 'uoms'));
    }


    public function update(UpdateRegularBundleRequest $request, $id)
    {
        $this->regularBundleInterface->update($request, $id);
        return redirect()->route('regular.index');
    }


    public function destroy($id)
    {
        $this->regularBundleInterface->destroy($id);
        return redirect()->back()->with('success', 'Product Regular Bundle has been changed successfully');
    }
}
