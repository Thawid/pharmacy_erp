<?php

namespace Modules\Product\Http\Controllers;

use App\DataTables\ProductsDataTable;
use App\Exports\ProductExport;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\Product;
use Carbon\Carbon;
use Modules\Product\Entities\ManageStoreProduct;
use Modules\Product\Entities\RegularBundle;
use Modules\Product\Entities\RegularBundleProduct;
use Maatwebsite\Excel\Facades\Excel;

class ProductReportController extends Controller
{
    public function index()
    {
        $product_types = DB::table('product_types')->orderBy('id', 'desc')->get(['id', 'name', 'status']);
        $product_categories = DB::table('product_categories')->where('parent_id', null)->orderBy('id', 'desc')->get(['id', 'name']);
        $product_generics = DB::table('product_generics')->orderBy('id', 'desc')->get(['id', 'name']);
        $manufacturers = DB::table('manufacturers')->orderBy('id', 'desc')->get(['id', 'name']);
        $stores = DB::table('stores')->orderBy('id', 'desc')->get(['id', 'name']);
        $products = Product::select('id', 'brand_name', 'product_name','unit_id')->with('ProductDetails','productUnit')->get();

        return view('product::product-management.product-reports.index', compact('product_types', 'product_categories', 'product_generics', 'manufacturers', 'stores', 'products'));
    }

    public function create()
    {
        return view('product::create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return view('product::show');
    }

    public function edit($id)
    {
        return view('product::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function getProductReport(Request $request)
    {
        $product_types = DB::table('product_types')->orderBy('id', 'desc')->get(['id', 'name', 'status']);
        $product_categories = DB::table('product_categories')->where('parent_id', null)->orderBy('id', 'desc')->get(['id', 'name']);
        $product_generics = DB::table('product_generics')->orderBy('id', 'desc')->get(['id', 'name']);
        $manufacturers = DB::table('manufacturers')->orderBy('id', 'desc')->get(['id', 'name']);
        $stores = DB::table('stores')->orderBy('id', 'desc')->get(['id', 'name']);
        // return $stores;

        // formatting date
        $from_date = new Carbon($request->create_date_form);
        $to_date = new Carbon($request->create_date_to);

        $formatted_from_date = $from_date->format('Y-m-d');
        $formatted_to_date = $to_date->format('Y-m-d');

        $products = Product::select('id', 'brand_name', 'product_name','unit_id')
            ->orWhereBetween('created_at', [$formatted_from_date, $formatted_to_date])
            ->orWhere('product_type_id', $request->product_type_id)
            ->orWhere('product_category_id', $request->product_category_id)
            ->orWhere('product_generic_id', $request->product_generic_id)
            ->orWhere('manufacturer_id', $request->manufacturer_id)
            ->with('ProductDetails','productUnit')
            ->get();

        if ($request->has('store_id') && $request->store_id != "") {
            $product_id = ManageStoreProduct::where('store_id', $request->store_id)->pluck('product_id');
            $products = Product::whereIn('id', $product_id)->with('ProductDetails','productUnit')->get();
        }

        if ($request->has('store_id') && $request->product_type_id != "") {
            $product_id = ManageStoreProduct::where('store_id', $request->store_id)->pluck('product_id');
            $products = Product::whereIn('id', $product_id)->where('product_type_id', $request->product_type_id)->with('ProductDetails','productUnit')->get();
        }

        if ($request->has('package_type_id') && $request->package_type_id == "2") {
            $product_id = RegularBundleProduct::where('status', 1)->pluck('product_id');
            $products = Product::whereIn('id', $product_id)->with('ProductDetails','productUnit')->get();
        } elseif ($request->has('package_type_id') && $request->package_type_id == "1") {
            $products = Product::where('status', 1)->with('ProductDetails','productUnit')->get();
        }
        return view('product::product-management.product-reports.index', compact('products', 'product_types', 'product_categories', 'product_generics', 'manufacturers', 'stores'));
    }

    public function export(Request $request)
    {
        // formatting date
        // $from_date = new Carbon($request->create_date_form);
        // $to_date = new Carbon($request->create_date_to);

        // $formatted_from_date = $from_date->format('Y-m-d');
        // $formatted_to_date = $to_date->format('Y-m-d');

        // return Excel::download(new ProductExport($formatted_from_date, $formatted_to_date, $request->package_type_id, $request->product_type_id, $request->product_category_id, $request->product_generic_id, $request->manufacturer_id, $request->store_id), 'products.xlsx');
    }

    // public function test(ProductsDataTable $productsDataTable)
    // {
    //     return $productsDataTable->render('/reports/test');
    // }
}
