<?php

namespace Modules\Product\Http\Controllers;

use DB;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;

class DashboardController extends Controller
{

    public function index()
    {
        $latest_products = Product::with('ProductDetails')->latest()->take(5)->get(['id', 'brand_name', 'product_name']);
        $total_product_type = DB::table('product_types')->where('status', 1)->count();
        $total_product_generic = DB::table('product_generics')->where('status', 1)->count();
        $total_products = DB::table('products')->where('status', 1)->count();
        $total_product_categories = DB::table('product_categories')->where('status', 1)->where('parent_id', null)->count();
        return view('product::product-management.dashboard',
            compact('total_products', 'total_product_type', 'total_product_generic', 'total_product_categories', 'latest_products')
        );
    }
}
