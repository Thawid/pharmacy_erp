<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Repositories\ProductUnitInterface;
use Modules\Product\Http\Requests\ProductUnitRequest;
use Modules\Product\Http\Requests\UpdateProductUnitRequest;

class ProductUnitController extends Controller
{

    protected $productUnit;

    public function __construct(ProductUnitInterface $productUnit){
        $this->productUnit = $productUnit;
    }

    public function index()
    {
        $product_units = $this->productUnit->index();
        return view('product::product-management.product-unit.index',compact('product_units'));
    }

    public function create()
    {
        return view('product::product-management.product-unit.create');
    }

    public function store(ProductUnitRequest $request)
    {
        $product_unit = $this->productUnit->store($request);
        return redirect()->route('units.index')->with('success','Product Units has been created successfully');
    }

    public function show($id)
    {
        return view('product::show');
    }

    public function edit($id)
    {
        $product_unit = $this->productUnit->edit($id);
        return view('product::product-management.product-unit.edit', compact('product_unit'));
    }

    public function update(UpdateProductUnitRequest $request, $id)
    {
        $this->productUnit->update($request,$id);
        return redirect()->route('units.index')->with('success','Product Unit has been updated successfully');
    }

    public function destroy($id)
    {
        $this->productUnit->destroy($id);
        return redirect()->back()->with('success','Product Unit status has been changed successfully');
    }
}
