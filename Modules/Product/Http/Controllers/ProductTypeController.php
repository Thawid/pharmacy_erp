<?php

namespace Modules\Product\Http\Controllers;

use Modules\Product\Repositories\ProductTypeInterface;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Product\Http\Requests\TypeRequest;
use Modules\Product\Http\Requests\UpdateProductTypeRequest;

class ProductTypeController extends Controller
{
    private $productTypeRepository;

    public function __construct(ProductTypeInterface $productTypeRepository)
    {
        $this->productTypeRepository = $productTypeRepository;
    }

    public function index()
    {
        $product_types = $this->productTypeRepository->index();
        return view('product::product-management.product-type.index',compact('product_types'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('product::product-management.product-type.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(TypeRequest $request)
    {
        $product_type = $this->productTypeRepository->store($request);
        return redirect()->route('types.index')->with('success','Product Type has been created successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $product_type = $this->productTypeRepository->edit($id);
        return view('product::product-management.product-type.edit', compact('product_type'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateProductTypeRequest $request, $id)
    {
        $this->productTypeRepository->update($request,$id);
        return redirect()->route('types.index')->with('success','Product Type has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $this->productTypeRepository->destroy($id);
        return redirect()->back()->with('success','Product type status has been changed successfully');
    }
}
