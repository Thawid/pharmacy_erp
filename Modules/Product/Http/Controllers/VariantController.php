<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Repositories\VariantInterface;
use Modules\Product\Http\Requests\CreateVariantRequest;
use Modules\Product\Http\Requests\UpdateVariantRequest;
use Modules\Product\Entities\ProductType;

class VariantController extends Controller
{
    private $variantRepository;

    public function __construct(VariantInterface $variantRepository)
    {
        $this->variantRepository = $variantRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $variants = $this->variantRepository->index();
        return view('product::product-management.variant.index', compact('variants'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('product::product-management.variant.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateVariantRequest $request)
    {
        $this->variantRepository->store($request);
        return redirect()->route('variants.index')->with('success','Variant has been created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $variant = $this->variantRepository->edit($id);
        $product_types = ProductType::where('status',1)->get(['id','name']);
        return view('product::product-management.variant.edit', compact(['variant', 'product_types']));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateVariantRequest $request, $id)
    {
        $this->variantRepository->update($request, $id);
        return redirect()->route('variants.index')->with('success', 'Variant has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $this->variantRepository->destroy($id);
        return redirect()->back()->with('success', 'Variant status has been changed successfully');
    }

    public function getProductVariant($id) {
        $this->variantRepository->getProductVariant($id);
    }
}
