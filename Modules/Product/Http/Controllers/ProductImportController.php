<?php

namespace Modules\Product\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Imports\ProductsImport;
use App\Imports\ProductsExport;
use Exception;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Product\Entities\Product;

class ProductImportController extends Controller
{
    /**
     * Display import product options.
     *
     * @return View
     */
    public function index()
    {
        return view("product::product-management.bulk-store-product.import_product");
    }

    /**
     * Display import product options.
     * @param Request $request
     * @return null
     */
    public function create (Request $request)
    {
        $products = null;
        $collection_data = Product::with('ProductDetails', 'productVariations', 'Productuom')->get();
        $product = [
            'brand_name'          => 'brand_name',
            'product_name'        => 'product_name',
            'product_type_id'     => 'product_type_id',
            'product_category_id' => 'product_category_id',
            'sub_category_id'     => 'sub_category_id',
            'product_generic_id'  => 'product_generic_id',
            'manufacturer_id'     => 'manufacturer_id',
            'unit_id'             => 'unit_id',
            'purchase_uom_id'     => 'purchase_uom_id',
            'purchase_price'      => 'purchase_price',
            'uom_id'              => 'uom_id',
            'quantity_per_uom'    => 'quantity_per_uom',
        ];

        $products[] = $product;

        foreach ($collection_data as $collection){
            $uom_ids = [];
            $quantity_per_uom = [];
            $uom_id = "";
            $quantity_per_uom_data = "";

            foreach ($collection->Productuom as $product_uom) {
                $uom_ids[] = $product_uom->uom_id;
                $quantity_per_uom[] = $product_uom->quantity_per_uom;
            }

            $uom_id = implode(",", $uom_ids);
            $quantity_per_uom_data = implode(",", $quantity_per_uom);

            $product = [
                'brand_name'          => $collection['brand_name'],
                'product_name'        => $collection['product_name'],
                'product_type_id'     => $collection['product_type_id'],
                'product_category_id' => $collection['product_category_id'],
                'sub_category_id'     => $collection['sub_category_id'],
                'product_generic_id'  => $collection['product_generic_id'],
                'manufacturer_id'     => $collection['manufacturer_id'],
                'unit_id'             => $collection['unit_id'],
                'purchase_uom_id'     => $collection['purchase_uom_id'],
                'purchase_price'      => $collection['purchase_price'],
                'uom_id'              => $uom_id,
                'quantity_per_uom'    => $quantity_per_uom_data,
            ];
            $products[] = $product;
        }
        return Excel::download(new ProductsExport($products), 'products.xlsx');
    }

    /**
     * Store imported products.
     * @param Request $request
     * @return null
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_excel' => 'required|mimes:xls,xlsx'
        ]);

        $file = $request->file('product_excel');

        try{
            Excel::import(new ProductsImport, $file);
        } catch (Exception $e){
            return back()->with('message_success', 'Import Failed');
        }

        return redirect()->route('products.index')->with('success', 'Products Imported Successfully Completed.');
    }
}
