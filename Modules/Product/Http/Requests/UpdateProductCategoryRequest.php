<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductCategoryRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'min:3', 'max:30', 'unique:product_categories'],
        ];
    }

    public function authorize()
    {
        return true;
    }
}
