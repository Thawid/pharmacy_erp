<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUOMRequest extends FormRequest
{
    public function rules()
    {
        return [
            'uom_name' => 'required|alpha|min:2|max:30|unique:uoms',
            'status'=>'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
