<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateManufacturerRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required', 'regex:/^[a-zA-Z\s]*$/', 'min:2',
            'status' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
