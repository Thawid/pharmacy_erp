<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    public function rules()
    {
        return [
            'product_type_id' => 'required',
            'name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'min:3', 'max:30', 'unique:product_categories'],
            'status'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
