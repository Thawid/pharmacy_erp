<?php

namespace Modules\Product\Http\View\Composers;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductType;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\Variant;
use Modules\Product\Entities\Manufacturer;
use Modules\Product\Entities\Product;
use Illuminate\View\View;

class ProductComposer
{


    public function compose(View $view)
    {
        $view->with('product_types', ProductType::where('status','=',1)->get(['id','name']));
        $view->with('product_categories', ProductCategory::whereNull('parent_id')->where('status','=',1)->get(['id','name']));
        $view->with('product_units', ProductUnit::where('status','=',1)->get(['id','unit_name']));
        $view->with('product_generics', ProductGeneric::where('status','=',1)->get(['id','name']));
        $view->with('variants', Variant::where('status','=',1)->get(['id','name']));
        $view->with('manufacturers', Manufacturer::where('status','=',1)->get(['id','name']));
        $view->with('products', Product::where('status','=',1)->get(['id','brand_name','product_name','product_type_id','product_category_id','product_generic_id','manufacturer_id','slug'])); // ,'image'
    }

}
