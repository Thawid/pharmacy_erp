<?php


namespace Modules\Product\Repositories;

use \Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Product\Http\Requests\CreateUOMRequest;
use Modules\Product\Http\Requests\UpdateUOMRequest;
use Modules\Product\Entities\UOM;

class UOMRepository implements UOMInterface
{
    public function index()
    {
        return DB::table('uoms')->get();
    }

    public function store(CreateUOMRequest $request)
    {
        $validator = $request->validated();
        $uom = new UOM();
        $uom->uom_name = $request->uom_name;
        $uom->status = $request->status ?? true;
        $uom->save();
    }

    public function edit($id)
    {
        return UOM::where('id', $id)->firstOrFail();
    }

    public function update(UpdateUOMRequest $request, $id)
    {
        $validator = $request->validated();

        $uom = UOM::where('id', $id)->firstOrFail();

        $uom->update([
            'uom_name' => $request->uom_name,
            'status' => $request->status
        ]);
    }

    public function destroy($id) {
        $uom = UOM::findOrFail($id);
        $uom->status = !$uom->status;
        $uom->save();
    }
}
