<?php


namespace Modules\Product\Repositories;

use Illuminate\Support\Str;
use Modules\Product\Entities\VariantProperty;
use Modules\Product\Entities\Variant;
use Modules\Product\Http\Requests\CreateVariantPropertyRequest;
use Modules\Product\Http\Requests\UpdateVariantPropertyRequest;

class VariantPropertyRepository implements VariantPropertyInterface
{
    public function index()
    {
        return VariantProperty::with('variant')->latest()->get();
    }

    public function create()
    {

    }

    public function store(CreateVariantPropertyRequest $request)
    {
        $variant_property = new VariantProperty();
        $variant_property->name = $request->name;
        $variant_property->variant_id = $request->variant_id;
        $variant_property->status = $request->status ?? true;
        $variant_property->save();

    }

    public function edit($id)
    {
        return VariantProperty::where('id',$id)->firstOrFail();
    }

    public function update(UpdateVariantPropertyRequest $request, $id)
    {
        $request->validated();
        return VariantProperty::where('id', $id)->update([
            'name' => $request->name,
            'variant_id' => $request->variant_id,
            'status' => $request->status,
        ]);
    }

    public function destroy($id)
    {
        $variant_property = VariantProperty::findOrFail($id);
        $variant_property->status = !$variant_property->status;
        $variant_property->save();
    }
}
