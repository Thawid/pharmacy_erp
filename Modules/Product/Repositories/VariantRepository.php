<?php


namespace Modules\Product\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Modules\Product\Entities\Variant;
use Modules\Product\Http\Requests\CreateVariantRequest;
use Modules\Product\Http\Requests\UpdateVariantRequest;

class VariantRepository implements VariantInterface
{
    public function index()
    {
        return Variant::orderBy('id', 'desc')->get(['id', 'product_type_id', 'name', 'slug', 'status']);
    }

    public function create()
    {

    }

    public function store(CreateVariantRequest $request)
    {
        $variant = new Variant();
        $variant->product_type_id = $request->product_type_id;
        $variant->name = $request->name;
        $variant->status = $request->status ?? true;
        $variant->slug = Str::slug($request->name, '-');
        $variant->save();

    }

    public function edit($id)
    {
        return Variant::where('id',$id)->firstOrFail();
    }

    public function update(UpdateVariantRequest $request, $id)
    {
        $request->validated();
        $variant = Variant::where('id', $id);
        $variant->update([
            'product_type_id' => $request->product_type_id,
            'name' => $request->name,
            'status' => $request->status,
            'slug' => Str::slug($request->name, '-')
        ]);
    }

    public function destroy($id)
    {
        $variant = Variant::findOrFail($id);
        $variant->status = !$variant->status;
        $variant->save();
    }

    public function getProductVariant($id)
    {
        echo json_encode(DB::table('variants')->where('product_type_id', $id)->where('status',1)->get());
    }
}
