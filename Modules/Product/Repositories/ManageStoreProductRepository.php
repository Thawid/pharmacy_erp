<?php


namespace Modules\Product\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\ManageStoreProduct;
use Modules\Product\Http\Requests\CreateManageStoreProductRequest;
use Modules\Product\Http\Requests\UpdateManageStoreProductRequest;

class ManageStoreProductRepository implements ManageStoreProductInterface
{
    public function index(){
        return ManageStoreProduct::all();
    }

    public function activeProducts($id){}
    public function create(){}

    public function store(CreateManageStoreProductRequest $request){
    }

    public function show(CreateManageStoreProductRequest $request,$id){
    }

    public function edit(CreateManageStoreProductRequest $request,$id){

    }

    public function update(UpdateManageStoreProductRequest $request, $id){

    }

    public function destroy($id){

    }

    public function getProductVariations($id) {
        $product = Product::with('productVariations')->where('id', $id)->first();
        $response = '';
        foreach ($product->productVariations as $key => $variations) {
            $response .= "<tr>";
            $response .= "<td>" . $variations->id . "</td>";
            $response .= "<td>" . $variations->product_variation_name . "</td>";
            $response .= "<td>" . $variations->variant[0]->name . "</td>";
            $response .= "<td>" . $variations->variantProperty[0]->name . "</td>";
            $response .= "</tr>";
        }
        return $response;
    }

}
