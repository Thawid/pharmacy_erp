<?php

namespace Modules\Product\Repositories;

use Modules\Product\Http\Requests\TypeRequest;
use Modules\Product\Http\Requests\UpdateProductTypeRequest;

interface ProductTypeInterface
{
	public function index();

	public function create();

	public function store(TypeRequest $request);

	public function edit($id);

	public function update(UpdateProductTypeRequest $request, $id);

	public function destroy($id);
}
