<?php


namespace Modules\Product\Repositories;

use Modules\Product\Http\Requests\CreateVariantPropertyRequest;
use Modules\Product\Http\Requests\UpdateVariantPropertyRequest;

interface VariantPropertyInterface
{
    public function index();

    public function create();

    public function store(CreateVariantPropertyRequest $request);

    public function edit($id);

    public function update(UpdateVariantPropertyRequest $request, $id);

    public function destroy($id);
}
