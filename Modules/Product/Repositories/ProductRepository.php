<?php


namespace Modules\Product\Repositories;

use DB;
use DataTables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductUom;
use Modules\Product\Entities\TempProduct;
use Modules\Product\Entities\ProductDetails;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Http\Controllers\Traits\FileUploadTrait;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;

class ProductRepository implements ProductInterface
{
    use FileUploadTrait;

    /*use traits for file and image upload*/

    public function index()
    {
        return Product::with('ProductDetails')->latest()->get(['id', 'brand_name', 'product_name']);
        // return ProductVariation::latest()->get(['id', 'product_id', 'product_variation_name', 'image']);
    }

    /*public function getProducts(Request $request)
    {
        $product_type = $request->product_type;
        $pcategory = $request->pcategory;
        $manufacture = $request->manufacture;
        $product_generic_id = $request->product_generic_id;
        if ($product_type != '' || $pcategory != '' || $manufacture != '' || $product_generic_id) {
            $products = Product::where('product_type_id', $product_type)
                ->orWhere('product_category_id', $pcategory)
                ->orWhere('manufacturer_id', $manufacture)
                ->orWhere('product_generic_id', $product_generic_id)
                ->with(['ptype','productVariations']);
        } else {
            $products = ProductVariation::where('product_id','!=',NULL)->get();
        }
        return datatables()->of($products)
            ->addColumn('image', function ($row) {
                   return '<img src="' . asset('products' . "/" . $row->image) . '" alt="Product images" height="60px" width="60px">';
            })
            ->addColumn('product_variation_name', function ($row) {
                foreach ($row as $variation_name){
                    return $row->product_variation_name;
                }

            })
            ->addColumn('action', function ($row) {
                $btn = '';
                foreach ($row as $pid) {
                   return '<a class="edit" href="' . route('products.edit', $row->product_id) . '">
                            <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i>
                        </a> &nbsp
                        <a class="" href="' . route('products.show', $row->product_id) . '">
                        <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"></i>
                        </a>';
                }
                return $btn;
            })
            ->rawColumns(['action', 'product_variation_name', 'image'])
            ->make(true);
    }*/

    public function create()
    {
    }

    public function store(CreateProductRequest $request)
    {
        DB::beginTransaction();
        $request->validated();

        try {

            if ($request->hasFile('feature_image')) {
                $image = $request->file('feature_image');
                $file_name = time() . '.' . $image->extension();
                if (!file_exists(public_path('feature-image'))) {
                    mkdir(public_path('feature-image'), 0777);
                }
                $request->feature_image->move(public_path('feature-image'), $file_name);
                $feature_image = $file_name;
            }


            $images = [];
            if ($request->hasfile('images')) {
                foreach ($request->file('images') as $file) {
                    $images_files_name = time() . rand(1, 100) . '.' . $file->extension();
                    if (!file_exists(public_path('images'))) {
                        mkdir(public_path('images'), 0777);
                    }
                    $file->move(public_path('images'), $images_files_name);
                    $images[] = $images_files_name;
                }
            }

            /*$image_name = $this->saveFiles($request);*/
            $product = new Product();
            $product->brand_name = $request->brand_name;
            $product->product_name = $request->product_name;
            $product->product_type_id = $request->product_type_id;
            $product->product_category_id = $request->parent_id;
            $product->sub_category_id = $request->sub_category_id;
            $product->manufacturer_id = $request->manufacturer_id;
            $product->product_generic_id = $request->product_generic_id;
            $product->unit_id = $request->unit_id;
            $product->purchase_uom_id = $request->purchase_uom;
            $product->purchase_price = $request->purchase_price;
            $product->inventory_type = $request->inventory_type;
            $product->inventory_state = $request->inventory_state;
            $product->status = 1;
            $product->is_returnable = $request->is_returnable;
            $product->is_emergency = $request->is_emergency;
            $product->is_exchangeable = $request->is_exchangeable;
            $product->slug = Str::slug($request->brand_name, '-').'-'. $request->product_name;
            $product->save();

            $product_details = new ProductDetails();
            $product_details->product_id = $product->id;
            $product_details->short_desc = $request->short_description;
            $product_details->long_desc = $request->long_description;
            $product_details->specifications = $request->specifications;
            $product_details->feature_image = $feature_image;
            $product_details->images = implode('|', $images);
            $product_details->save();


            $product_variations = [];
            $variants_id = $request->variant_id;
            $vproperties_id = $request->variant_property_id;

            if ($variants_id) {
                foreach ($variants_id as $key => $variant_id) {
                    $product_variations[] = [
                        'product_id' => $product->id,
                        'variant_id' => $variant_id,
                        'variant_property_id' => isset($vproperties_id[$key]) ? $vproperties_id[$key] : NULL,
                        'status' => 1
                    ];
                }
            }
            ProductVariation::insert($product_variations);

            $product_uoms = [];
            $uom_ids = $request->uom_id;
            $quantity_per_uom = $request->quantity_per_uom;
            if ($uom_ids) {
                foreach ($uom_ids as $key => $uom_id) {
                    $product_uoms[] = [
                        'product_id' => $product->id,
                        'uom_id' => $uom_id,
                        'quantity_per_uom' => isset($quantity_per_uom[$key]) ? $quantity_per_uom[$key] : NULL,
                        'status' => 1
                    ];
                }
            }

            ProductUom::insert($product_uoms);

            DB::commit();
            return redirect()->route('products.index')->with('success', 'Product has been Created successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error below" . $e->getMessage());
        }
    }

    public function show($id)
    {
        return Product::with(['ptype', 'productCategory', 'productSubCategory', 'manufacturer', 'pgeneric', 'productDetails', 'productVariations', 'productUnit','Productuom'])
            ->where('id', $id)
            ->first();
    }

    public function edit($id)
    {
        return Product::with(['ptype', 'productCategory', 'productSubCategory', 'manufacturer', 'pgeneric', 'productDetails', 'productVariations', 'Productuom', 'productUnit', 'purchaseUom'])->where('id', $id)->first();
    }

    public function update(UpdateProductRequest $request, $id)
    {

        DB::beginTransaction();
        try {
            $product = Product::findOrFail($id);
            $product->brand_name = $request->brand_name;
            $product->product_name = $request->product_name;
            $product->product_type_id = $request->product_type_id;
            $product->product_category_id = $request->parent_id;
            $product->sub_category_id = $request->sub_category_id;
            $product->manufacturer_id = $request->manufacturer_id;
            $product->product_generic_id = $request->product_generic_id;
            $product->unit_id = $request->unit_id;
            $product->purchase_uom_id = $request->purchase_uom;
            $product->purchase_price = $request->purchase_price;
            $product->inventory_type = $request->inventory_type;
            $product->inventory_state = $request->inventory_state;
            $product->status = 1;
            $product->is_returnable = $request->is_returnable;
            $product->is_emergency = $request->is_emergency;
            $product->is_exchangeable = $request->is_exchangeable;
            $product->slug = Str::slug($request->brand_name, '-') .'-'. $request->product_name;
            $product->save();

            /*product details upate*/
            $product_details = ProductDetails::where('product_id', $id)->first();
            $exist_feature_image = "";
            if ($request->hasFile('feature_image') && $request->file('feature_image') != null) {
                $image = $request->file('feature_image');
                $file_name = time() . '.' . $image->extension();
                if (!file_exists(public_path('feature-image'))) {
                    mkdir(public_path('feature-image'), 0777);
                }
                $request->feature_image->move(public_path('feature-image'), $file_name);
                $product_details->feature_image = $file_name;
                $product_details->save();
            }

            $images = [];
            if ($request->hasFile('images') && $request->file('images') != null) {
                foreach ($request->file('images') as $file) {
                    $images_files_name = time() . rand(1, 100) . '.' . $file->extension();
                    if (!file_exists(public_path('images'))) {
                        mkdir(public_path('images'), 0777);
                    }
                    $file->move(public_path('images'), $images_files_name);
                    $images[] = $images_files_name;
                    $product_details->images = implode("|",$images);
                    $product_details->save();
                }
            }
            $product_details->update([
                'product_id' => $product->id,
                'short_desc' => $request->short_description,
                'long_desc' => $request->long_description,
                'specifications' => $request->specifications,
                'status' => 1
            ]);

            $product_variations = [];
            $variants_id = $request->variant_id;
            $vproperties_id = $request->variant_property_id;

            if ($variants_id) {
                foreach ($variants_id as $key => $variant_id) {
                    DB::table('product_variations')->where('product_id', $id)->delete();
                    $product_variations[] = [
                        'product_id' => $product->id,
                        'variant_id' => $variant_id,
                        'variant_property_id' => isset($vproperties_id[$key]) ? $vproperties_id[$key] : NULL,
                        'status' => 1
                    ];
                }
            }
            ProductVariation::insert($product_variations);

            $product_uoms = [];
            $uom_ids = $request->uom_id;
            $quantity_per_uom = $request->quantity_per_uom;
            if ($uom_ids) {
                foreach ($uom_ids as $key => $uom_id) {
                    DB::table('product_uoms')->where('product_id', $id)->delete();
                    $product_uoms[] = [
                        'product_id' => $product->id,
                        'uom_id' => $uom_id,
                        'quantity_per_uom' => isset($quantity_per_uom[$key]) ? $quantity_per_uom[$key] : NULL,
                        'status' => 1
                    ];
                }
            }

            ProductUom::insert($product_uoms);
            DB::commit();
            return redirect()->route('products.index')->with('success', 'Product has been Updated successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->status = !$product->status;
        $product->save();
    }
}
