<?php


namespace Modules\Product\Repositories;


use DB;
use Illuminate\Http\Request;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\RegularBundle;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\RegularBundleProduct;
use Modules\Product\Http\Requests\CreateRegularBundleRequest;
use Modules\Product\Http\Requests\UpdateRegularBundleRequest;


class RegularBundleRepository implements RegularBundleInterface
{
    public function index()
    {
    }

    public function create()
    {
    }

    public function store(CreateRegularBundleRequest $request)
    {
        $validator = $request->validated();
        DB::beginTransaction();
        try {
            $regular_bundle = new RegularBundle();
            $regular_bundle->bundle_name = $request->bundle_name;
            $regular_bundle->status = 1;
            $regular_bundle->save();

            if ($request->has('product_id')) {
                $i = 0;
                foreach ($request->product_id as $pid) {
                    RegularBundleProduct::updateOrInsert(
                        [
                            'regular_bundle_id' => $regular_bundle->id,
                            'product_id' => $pid,
                            'product_name' => $request->product_name[$i],
                            'uom_id' => $request->uom_id[$i],
                            'status' => 1
                        ]
                    );
                    $i++;
                }
            }
            DB::commit();
            return redirect()->route('regular.index')->with('success', 'Product Bundle has been Created successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors("error " . $e->getMessage());
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(UpdateRegularBundleRequest $request, $id)
    {
        $validator = $request->validated();
        DB::beginTransaction();
        try {
            $regular_bundle = RegularBundle::findOrFail($id);
            $regular_bundle->bundle_name = $request->bundle_name;
            $regular_bundle->status = 1;
            $regular_bundle->save();

            $productBundle = [];
            if ($request->has('product_id')) {
                DB::table('regular_bundle_products')->where('regular_bundle_id', $id)->delete();
                $i = 0;
                foreach ($request->product_id as $pid) {
                    $productBundle[] = [
                        'regular_bundle_id' => $regular_bundle->id,
                        'product_id' => $pid,
                        'product_name' => $request->product_name[$i],
                        'uom_id' => $request->uom_id[$i],
                        'status' => 1,
                    ];
                    $i++;
                }
            }
            RegularBundleProduct::insert($productBundle);
            DB::commit();
            return redirect()->route('regular.index')->with('success', 'Product Bundle has been Updated successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $regularBundle = RegularBundle::findOrFail($id);
        $regularBundle->status = !$regularBundle->status;
        $regularBundle->save();
        $regularBundleProduct = RegularBundleProduct::where('regular_bundle_id', $id)->first();
        $regularBundleProduct->status = !$regularBundleProduct->status;
        $regularBundleProduct->save();
    }

}
