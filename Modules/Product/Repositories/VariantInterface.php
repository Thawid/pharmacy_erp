<?php


namespace Modules\Product\Repositories;

use Modules\Product\Http\Requests\CreateVariantRequest;
use Modules\Product\Http\Requests\UpdateVariantRequest;

interface VariantInterface
{
    public function index();

    public function create();

    public function store(CreateVariantRequest $request);

    public function edit($id);

    public function update(UpdateVariantRequest $request, $id);

    public function destroy($id);
}
