<?php

namespace Modules\Product\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\ProductCategory;

class ProductCategoryRepository implements ProductCategoryInterface
{
    public function index()
    {
        /* Eluquent join with product type and self join using parent id */
        return ProductCategory::with(['ptype','children'])->get();
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        if($request->has('have_parent')){
              $request->validate([
                'product_type_id' => 'required',
                'parent_id' => 'required',
                  'name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'min:3', 'max:30', 'unique:product_categories'],
                'status'=>'required'
            ]);
        }else{
              $request->validate([
                'product_type_id' => 'required',
                  'name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'min:3', 'max:30', 'unique:product_categories'],
                'status'=>'required'
            ]);
        }

        $product_category = new ProductCategory();
        $product_category->name = $request->name;
        $product_category->product_type_id = $request->product_type_id;
        $product_category->parent_id = $request->parent_id ?? NULL;
        $product_category->status = $request->status?? true;
        $product_category->save();
    }

    public function edit($id)
    {
        return ProductCategory::where('id',$id)->firstOrFail();

    }

    public function update(Request $request, $id)
    {
        if($request->has('have_parent') && $request->have_parent == '1'){
            $request->validate([
                'product_type_id' => 'required',
                'parent_id' => 'required',
                'name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'min:2', 'max:30'],
                'status'=>'required'
            ]);
        }else{
            $request->validate([
                'product_type_id' => 'required',
                'name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'min:2', 'max:30'],
                'status'=>'required'
            ]);
        }

        $product_category = ProductCategory::where('id',$id)->firstOrFail();
        if($request->have_parent == ''){
            $product_category->update([
                'name' => $request->name,
                'product_type_id' => $request->product_type_id,
                'parent_id' => NULL,
                'status' => $request->status,
            ]);
        }else{
            $product_category->update([
                'name' => $request->name,
                'product_type_id' => $request->product_type_id,
                'parent_id' => $request->parent_id,
                'status' => $request->status,
            ]);
        }


    }

    public function destroy($id)
    {
        $product_category = ProductCategory::findOrFail($id);
        $product_category->status = !$product_category->status;
        $product_category->save();
    }

    public function getProductCategory($id)
    {
        echo json_encode(DB::table('product_categories')->where('product_type_id', $id)->whereNull('parent_id')->where('status',1)->get());
    }

    public function getProductSubCategory($id)
    {
        echo json_encode(DB::table('product_categories')->where('parent_id', $id)->where('status',1)->get());
    }

    public function getVariantProperty($id)
    {
        echo json_encode(DB::table('variant_properties')->where('variant_id', $id)->where('status',1)->get());
    }

}
