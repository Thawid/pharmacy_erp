<?php


namespace Modules\Product\Repositories;


use Modules\Product\Http\Requests\CreateAdvanceBundleRequest;
use Modules\Product\Http\Requests\UpdateAdvanceBundleRequest;

interface AdvanceBundleInterface
{
    public function index();

    public function create();

    public function store(CreateAdvanceBundleRequest $request);

    public function show($id);

    public function edit($id);

    public function update(UpdateAdvanceBundleRequest $request, $id);

    public function destroy($id);
}
