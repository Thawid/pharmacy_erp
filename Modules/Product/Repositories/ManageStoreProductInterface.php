<?php


namespace Modules\Product\Repositories;


use Modules\Product\Http\Requests\CreateManageStoreProductRequest;
use Modules\Product\Http\Requests\UpdateManageStoreProductRequest;

interface ManageStoreProductInterface
{
    public function index();
    public function activeProducts($id);

    public function create();

    public function store(CreateManageStoreProductRequest $request);

    public function show(CreateManageStoreProductRequest $request,$id);

    public function edit(CreateManageStoreProductRequest $request,$id);

    public function update(UpdateManageStoreProductRequest $request, $id);

    public function destroy($id);

    public function getProductVariations($id);
}
