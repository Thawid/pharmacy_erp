<?php


namespace Modules\Product\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Http\Requests\CreateProductGenericRequest;
use Modules\Product\Http\Requests\UpdateProductGenericRequest;

class ProductGenericRepository implements ProductGenericInterface
{
    public function index()
    {
        return ProductGeneric::orderBy('id', 'desc')->get(['id', 'product_type_id', 'name', 'status']);
    }

    public function create()
    {

    }

    public function edit($id)
    {
        return ProductGeneric::where('id', $id)->firstOrFail();
    }

    public function store(CreateProductGenericRequest $request)
    {
        $validator = $request->validated();
        $product_generic = new ProductGeneric();
        $product_generic->name = $request->name;
        $product_generic->product_type_id = $request->product_type_id;
        $product_generic->status = $request->status ?? true;
        $product_generic->save();
    }

    public function update(UpdateProductGenericRequest $request, $id)
    {
        $request->validated();
        $product_generic = ProductGeneric::where('id', $id)->firstOrFail();
        $product_generic->update([
            'name' => $request->name,
            'product_type_id' => $request->product_type_id,
            'status' => $request->status
        ]);
    }

    public function destroy($id)
    {
        $product_generic = ProductGeneric::findOrFail($id);
        $product_generic->status = !$product_generic->status;
        $product_generic->save();
    }

    public function getProductGeneric($id)
    {
        echo json_encode(DB::table('product_generics')->where('product_type_id', $id)->get());
    }

}
