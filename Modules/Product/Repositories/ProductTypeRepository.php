<?php

namespace Modules\Product\Repositories;
use Modules\Product\Entities\ProductType;
use Modules\Product\Http\Requests\TypeRequest;
use Modules\Product\Http\Requests\UpdateProductTypeRequest;
use DataTables;
use Illuminate\Support\Facades\DB;


class ProductTypeRepository implements ProductTypeInterface
{

    public function index()
    {
        return ProductType::latest()->get(['id','name','status']);

    }

    public function create()
    {

    }

    public function store(TypeRequest $request)
    {
        $validator = $request->validated();

        $product_type = new ProductType();
        $product_type->name = $request->name;
        $product_type->status = $request->status??true;
        $product_type->save();
    }

    public function edit($id)
    {
        return ProductType::where('id',$id)->firstOrFail();

    }

    public function update(UpdateProductTypeRequest $request, $productTypeId)
    {
        $request->validated();

        $product_type = ProductType::where('id',$productTypeId)->firstOrFail();

        $product_type->update([
            'name' => $request->name,
            'status' => $request->status
        ]);

    }

    public function destroy($id)
    {
        $product_type = ProductType::findOrFail($id);
        $product_type->status = !$product_type->status;
        $product_type->save();
    }

}
