<?php


namespace Modules\Product\Repositories;

use Modules\Product\Http\Requests\CreateRegularBundleRequest;
use Modules\Product\Http\Requests\UpdateRegularBundleRequest;

interface RegularBundleInterface
{
    public function index();

    public function create();

    public function store(CreateRegularBundleRequest $request);

    public function show($id);

    public function edit($id);

    public function update(UpdateRegularBundleRequest $request, $id);

    public function destroy($id);
}
