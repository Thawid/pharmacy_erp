<?php


namespace Modules\Product\Repositories;
use Modules\Product\Entities\Manufacturer;
use Modules\Product\Http\Requests\CreateManufacturerRequest;
use Modules\Product\Http\Requests\UpdateManufacturerRequest;
use DataTables;
use Illuminate\Support\Facades\DB;

class ManufacturerRepository implements ManufacturerInterface
{
    public function index() {
        return Manufacturer::latest()->get(['id', 'name', 'status']);
    }

    public function create() {}

    public function store(CreateManufacturerRequest $request)
    {
        $validator = $request->validated();
        $manufacturer = new Manufacturer();
        $manufacturer->name = $request->name;
        $manufacturer->status = $request->status;
        $manufacturer->save();
    }

    public function edit($id)
    {
        return Manufacturer::where('id',$id)->firstOrFail();
    }

    public function update(UpdateManufacturerRequest $request, $id)
    {
        $request->validated();

        $manufacturer = Manufacturer::where('id', $id)->firstOrFail();

        $manufacturer->update([
            'name' => $request->name,
            'status' => $request->status
        ]);

    }

    public function destroy($id)
    {
        $manufacturer = Manufacturer::findOrFail($id);
        $manufacturer->status = !$manufacturer->status;
        $manufacturer->save();
    }

}
