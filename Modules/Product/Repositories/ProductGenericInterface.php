<?php


namespace Modules\Product\Repositories;

use Modules\Product\Http\Requests\CreateProductGenericRequest;
use Modules\Product\Http\Requests\UpdateProductGenericRequest;

interface ProductGenericInterface
{
    public function index();

    public function create();

    public function store(CreateProductGenericRequest $request);

    public function edit($id);

    public function update(UpdateProductGenericRequest $request, $id);

    public function destroy($id);
}
