<?php

namespace Modules\Product\Repositories;

use Illuminate\Http\Request;

interface ProductCategoryInterface
{
	public function index();

	public function create();

	public function store(Request $request);

	public function edit($id);

	public function update(Request $request, $id);

	public function destroy($id);

	public function getProductCategory($id);
	public function getProductSubCategory($id);
	public function getVariantProperty($id);
}
