<?php


namespace Modules\Product\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\AdvanceBundle;
use Modules\Product\Entities\AdvanceBundleProduct;
use Modules\Product\Http\Requests\CreateAdvanceBundleRequest;
use Modules\Product\Http\Requests\UpdateAdvanceBundleRequest;

class AdvanceBundleRepository implements AdvanceBundleInterface
{
    public function index()
    {
    }

    public function create()
    {
    }

    public function store(CreateAdvanceBundleRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();
        try {
            $advance_bundle = new AdvanceBundle();
            $advance_bundle->bundle_name = $request->bundle_name;
            $advance_bundle->status = 1;
            $advance_bundle->save();

            $productBundle = [];
            if ($request->has('product_id_ag')) {
                $i = 0;
                foreach ($request->product_id_ag as $pid) {
                    $productBundle[] = [
                        'advance_bundle_id' => $advance_bundle->id,
                        'product_id' => $pid,
                        'lot_id' => $request->lot_id_ag[$i],
                        'uom_id' => $request->uom_id_ag[$i],
                        'product_name' => $request->product_name_ag[$i],
                        'status' => 1,
                    ];
                    $i++;
                }
            }
            AdvanceBundleProduct::insert($productBundle);
            DB::commit();
            return redirect()->route('advance.index')->with('success', 'Advance Product Bundle has been Created successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(UpdateAdvanceBundleRequest $request, $id)
    {
        $validator = $request->validated();
        DB::beginTransaction();
        try {
            $advance_bundle = AdvanceBundle::findOrFail($id);
            $advance_bundle->bundle_name = $request->bundle_name;
            $advance_bundle->status = 1;
            $advance_bundle->save();

            $productBundle = [];
            if ($request->has('product_id_ag')) {
                DB::table('advance_bundle_products')->where('advance_bundle_id', $id)->delete();
                $i = 0;
                foreach ($request->product_id_ag as $pid) {
                    $productBundle[] = [
                        'advance_bundle_id' => $advance_bundle->id,
                        'product_id' => $pid,
                        'lot_id' => $request->lot_id_ag[$i],
                        'uom_id' => $request->uom_id_ag[$i],
                        'product_name' => $request->product_name_ag[$i],
                        'status' => 1,
                    ];
                    $i++;
                }
            }
            AdvanceBundleProduct::insert($productBundle);
            DB::commit();
            return redirect()->route('advance.index')->with('success', ' Advance Product Bundle has been Updated successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $advanceBundle = AdvanceBundle::findOrFail($id);
        $advanceBundle->status = !$advanceBundle->status;
        $advanceBundle->save();
        $advanceBundleProduct = AdvanceBundleProduct::where('advance_bundle_id', $id)->first();
        $advanceBundleProduct->status = !$advanceBundleProduct->status;
        $advanceBundleProduct->save();
        return redirect()->route('advance.index')->with('success', ' Advance Product Bundle has been Changed successfully');
    }
}
