<?php


namespace Modules\Product\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Http\Requests\ProductUnitRequest;
use Modules\Product\Http\Requests\UpdateProductUnitRequest;
use DataTables;

class ProductUnitRepository implements ProductUnitInterface
{
    public function index()
    {
        return ProductUnit::latest()->get(['id', 'unit_name', 'status']);
//        return ProductUnit::all();
    }

    public function create()
    {

    }

    public function store(ProductUnitRequest $request)
    {
        $validator = $request->validated();
        $product_unit = new ProductUnit();
        $product_unit->unit_name = $request->unit_name;
        $product_unit->status = $request->status ?? true;
        $product_unit->save();
    }

    public function edit($id)
    {
        return ProductUnit::where('id', $id)->firstOrFail();

    }

    public function update(UpdateProductUnitRequest $request, $product_unit_id)
    {
        $validator = $request->validated();

        $product_unit = ProductUnit::where('id', $product_unit_id)->firstOrFail();

        $product_unit->update([
            'unit_name' => $request->unit_name,
            'status' => $request->status
        ]);

    }

    public function destroy($id)
    {
        $product_unit = ProductUnit::findOrFail($id);
        $product_unit->status = !$product_unit->status;
        $product_unit->save();
    }
}
