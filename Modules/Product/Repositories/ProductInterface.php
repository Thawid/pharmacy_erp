<?php

namespace Modules\Product\Repositories;

use Illuminate\Http\Request;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;

interface ProductInterface
{
	public function index();

	public function create();

	public function store(CreateProductRequest $request);

	public function show($id);
	public function edit($id);

	public function update(UpdateProductRequest $request, $id);

	public function destroy($id);
}
