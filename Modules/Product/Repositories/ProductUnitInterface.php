<?php

namespace Modules\Product\Repositories;

use Illuminate\Http\Request;
use Modules\Product\Http\Requests\ProductUnitRequest;
use Modules\Product\Http\Requests\UpdateProductUnitRequest;

interface ProductUnitInterface
{
    public function index();

    public function create();

    public function store(ProductUnitRequest $request);

    public function edit($id);

    public function update(UpdateProductUnitRequest $request, $id);

    public function destroy($id);

}
