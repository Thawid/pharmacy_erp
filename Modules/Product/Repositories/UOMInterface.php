<?php


namespace Modules\Product\Repositories;

use Illuminate\Http\Request;
use Modules\Product\Http\Requests\CreateUOMRequest;
use Modules\Product\Http\Requests\UpdateUOMRequest;

interface UOMInterface
{
    public function index();

    public function store(CreateUOMRequest $request);

    public function edit($id);

    public function update(UpdateUOMRequest $request, $id);

    public function destroy($id);

}
