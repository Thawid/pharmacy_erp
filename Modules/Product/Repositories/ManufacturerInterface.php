<?php


namespace Modules\Product\Repositories;

use Modules\Product\Http\Requests\CreateManufacturerRequest;
use Modules\Product\Http\Requests\UpdateManufacturerRequest;

interface ManufacturerInterface
{
    public function index();

    public function create();

    public function store(CreateManufacturerRequest $request);

    public function edit($id);

    public function update(UpdateManufacturerRequest $request, $id);

    public function destroy($id);
}
