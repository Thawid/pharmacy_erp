<?php


namespace Modules\Product\Repositories;
use Modules\Product\Entities\ProductVariation;
use DataTables;

class ProductVariationRepository implements ProductVariationInterface
{
    public function index()
    {
        return ProductVariation::all();
//        return ProductVariation::with('pcat')->get();
    }
}
