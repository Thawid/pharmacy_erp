<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Store\Entities\StoreType;

class ManageStoreProduct extends Model
{
    protected $guarded = [];

}
