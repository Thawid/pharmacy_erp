<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TempProduct extends Model
{

    protected $guarded = [];

    public function variant()
    {
        return $this->belongsTo(Variant::class,'variant_id');

    }

    public function variantValues()
    {
        return $this->belongsTo(VariantProperty::class,'variant_property_id');

    }

}
