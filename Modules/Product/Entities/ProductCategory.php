<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    protected $guarded = [];

    public function children()
    {
        return $this->hasMany(ProductCategory::class, 'parent_id')->select('parent_id','name');
    }

    public function ptype()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}
