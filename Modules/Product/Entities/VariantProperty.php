<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class VariantProperty extends Model
{


    protected $guarded = [];

    public function variant()
    {
        return $this->belongsTo(Variant::class,'variant_id');
    }

    public function temp()
    {
        return $this->hasMany(TempProduct::class);
    }

    public function pVariation()
    {
        return $this->hasMany(ProductVariation::class);
    }

}
