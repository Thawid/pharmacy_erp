<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{

    protected $guarded = [];

    public function variantProperties()
    {
        return $this->hasMany(VariantProperty::class);

    }
    public function temp()
    {
        return $this->hasMany(TempProduct::class);
    }

    public function pVariation()
    {
        return $this->hasMany(ProductVariation::class);
    }

    public function ptype()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }
}
