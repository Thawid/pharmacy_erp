<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdvanceBundle extends Model
{
    protected $guarded = [];

    public function advanceProducts()
    {
        return $this->hasMany(AdvanceBundleProduct::class)->with(['manufacturer','pgeneric']);
    }
}
