<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class AdvanceBundleProduct extends Model
{
    protected $guarded = [];

    public function advanceBundle()
    {
        return$this->belongsTo(AdvanceBundle::class,'advance_bundle_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id');
    }

    public function pgeneric()
    {
        return $this->belongsTo(ProductGeneric::class, 'product_generic_id');
    }
}
