<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductUom extends Model
{
    protected $guarded= [];

    public function uom(){
        return $this->hasOne(UOM::class, 'id','uom_id')->select('id','uom_name');
    }

}
