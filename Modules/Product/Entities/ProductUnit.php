<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{

    protected $table = 'product_unit';

    protected $fillable = [
        'unit_name',
        'status'
    ];
    protected $guarded = [];

    public function pVariation()
    {
        return $this->belongsTo(ProductVariation::class, 'product_unit_id');
    }
}
