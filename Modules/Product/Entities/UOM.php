<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Price\Entities\StorePrice;

class UOM extends Model
{
    protected $table = 'uoms';

    protected $guarded = [];

    public function Productuom()
    {
        return $this->belongsTo(ProductUom::class, 'uom_id');
    }


}
