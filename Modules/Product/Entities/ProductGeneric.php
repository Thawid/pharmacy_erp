<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductGeneric extends Model
{

    protected $guarded = [];

    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function ptype()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }
}
