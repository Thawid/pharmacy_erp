<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{

    protected $guarded = [];

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }

}
