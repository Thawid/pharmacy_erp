<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class RegularBundleProduct extends Model
{
    protected $guarded = [];

    public function regularBundle()
    {
        return $this->belongsTo(RegularBundle::class, 'regular_bundle_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id');
    }

    public function pgeneric()
    {
        return $this->belongsTo(ProductGeneric::class, 'product_generic_id');
    }

    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id');
    }
}
