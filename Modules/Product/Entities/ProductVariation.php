<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Price\Entities\Price;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Vendor\Entities\VendorDetails;

class ProductVariation extends Model
{

    protected $guarded = [];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


    public function variant()
    {
        return $this->hasOne(Variant::class, 'id', 'variant_id')->select(['id', 'name']);
    }

    public function variantProperty()
    {
        return $this->hasOne(VariantProperty::class, 'id', 'variant_property_id')->select(['id', 'name']);
    }

    public function cat()
    {
        return $this->hasMany(ProductCategory::class, 'id', 'sub_category_id')->select(['id', 'name']);
    }

    public function punit()
    {
        return $this->hasMany(ProductUnit::class, 'id', 'product_unit_id')->select(['id', 'unit_name']);
    }

    public function unit()
    {
        return $this->belongsTo(ProductUnit::class, 'product_unit_id')->withDefault();
    }

    public function vendordetails()
    {
        return $this->hasMany(VendorDetails::class, 'product_id', 'id');
    }

    public function scopeWithRelationTo($query)
    {
        return $query->with(['product.productGenericName' => function ($q) {
            $q->select('id', 'name');
        }, 'unit' => function ($q) {
            $q->select('id', 'unit_name');
        }, 'vendordetails.vendorName' => function ($q) {
            $q->select('id', 'name');
        }, 'product_uom']);
    }

    public function pvariant()
    {
        return $this->belongsTo(Variant::class, 'variant_id');
    }

    public function pvariantproperty()
    {
        return $this->belongsTo(VariantProperty::class, 'variant_property_id');
    }

    public function uom()
    {
        return $this->hasMany(UOM::class, 'id', 'uom_id')->select(['id', 'uom_name']);
    }

    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'id','uom_id')->withDefault();
    }

    public function purchase_order()
    {
        return $this->belongsTo(PurchaseOrderDetails::class,'id','product_id')->withDefault();
    }
    public function product_price()
    {
        return $this->hasOne(Price::class,'product_variation_id','id')->latest();
    }

}
