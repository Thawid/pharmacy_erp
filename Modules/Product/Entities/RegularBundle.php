<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class RegularBundle extends Model
{

    protected $guarded = [];

    public function bundleProducts()
    {
        return $this->hasMany(RegularBundleProduct::class)->with(['manufacturer', 'pgeneric', 'productUom']);
    }
}
