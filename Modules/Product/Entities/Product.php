<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Price\Entities\UomPrice;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Procurement\Entities\StoreRequisitionDetails;
use Modules\Vendor\Entities\VendorDetails;

class Product extends Model
{
    protected $guarded = [];

    public function ProductDetails()
    {
        return $this->hasOne(ProductDetails::class);
    }

    public function Productuom()
    {
        return $this->hasMany(ProductUom::class)->with('uom');
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id');
    }

    public function pgeneric()
    {
        return $this->belongsTo(ProductGeneric::class, 'product_generic_id');
    }

    public function productVariations()
    {
        return $this->hasMany(ProductVariation::class)->with(['variant', 'variantProperty']);
    }

    public function ptype()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function productSubCategory()
    {
        return $this->hasOne(ProductCategory::class, 'id', 'sub_category_id');
    }

    public function productUnit()
    {
        return $this->hasOne(ProductUnit::class, 'id', 'unit_id')->select('unit_name', 'id');
    }

    public function purchaseUom()
    {
        return $this->hasOne(UOM::class, 'id', 'purchase_uom_id')->select('uom_name', 'id');
    }

    public function scopeWithRelationTo($query)
    {
        return $query->with(['product.productGenericName' => function ($q) {
            $q->select('id', 'name');
        }, 'unit' => function ($q) {
            $q->select('id', 'unit_name');
        }, 'vendordetails.vendorName' => function ($q) {
            $q->select('id', 'name');
        }, 'product_uom']);
    }

    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'purchase_uom_id','id')->withDefault();
    }

    public function vendors(){
        return $this->hasMany(VendorDetails::class,'product_id','id');
    }

    public function purchase_order()
    {
        return $this->belongsTo(PurchaseOrderDetails::class,'id','product_id')->latest();
    }

    public function UomPrice()
    {
        return $this->belongsTo(UomPrice::class,'id','product_id')->latest();
    }
}
