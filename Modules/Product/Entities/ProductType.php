<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{

    protected $guarded = [];

    public function productCategory()
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function pgeneric()
    {
        return $this->hasMany(ProductGeneric::class);
    }
}
