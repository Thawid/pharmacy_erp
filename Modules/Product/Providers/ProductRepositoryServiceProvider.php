<?php

namespace Modules\Product\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Product\Repositories\ProductCategoryInterface;
use Modules\Product\Repositories\ProductTypeInterface;
use Modules\Product\Repositories\ProductUnitInterface;
use Modules\Product\Repositories\ProductInterface;
use Modules\Product\Repositories\ProductGenericInterface;
use Modules\Product\Repositories\VariantPropertyInterface;
use Modules\Product\Repositories\VariantInterface;
use Modules\Product\Repositories\ManufacturerInterface;
use Modules\Product\Repositories\ProductVariationInterface;
use Modules\Product\Repositories\ManageStoreProductInterface;
use Modules\Product\Repositories\UOMInterface;
use Modules\Product\Repositories\RegularBundleInterface;
use Modules\Product\Repositories\AdvanceBundleInterface;
use Modules\Product\Repositories\ProductCategoryRepository;
use Modules\Product\Repositories\ProductTypeRepository;
use Modules\Product\Repositories\ProductUnitRepository;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Repositories\ProductGenericRepository;
use Modules\Product\Repositories\VariantRepository;
use Modules\Product\Repositories\VariantPropertyRepository;
use Modules\Product\Repositories\ManufacturerRepository;
use Modules\Product\Repositories\ProductVariationRepository;
use Modules\Product\Repositories\ManageStoreProductRepository;
use Modules\Product\Repositories\UOMRepository;
use Modules\Product\Repositories\RegularBundleRepository;
use Modules\Product\Repositories\AdvanceBundleRepository;

class ProductRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductCategoryInterface::class, ProductCategoryRepository::class);
        $this->app->bind(ProductTypeInterface::class, ProductTypeRepository::class);
        $this->app->bind(ProductUnitInterface::class,ProductUnitRepository::class);
        $this->app->bind(ProductInterface::class,ProductRepository::class);
        $this->app->bind(ProductGenericInterface::class,ProductGenericRepository::class);
        $this->app->bind(VariantInterface::class,VariantRepository::class);
        $this->app->bind(VariantPropertyInterface::class,VariantPropertyRepository::class);
        $this->app->bind(ManufacturerInterface::class,ManufacturerRepository::class);
        $this->app->bind(ProductVariationInterface::class,ProductVariationRepository::class);
        $this->app->bind(ManageStoreProductInterface::class,ManageStoreProductRepository::class);
        $this->app->bind(UOMInterface::class, UOMRepository::class);
        $this->app->bind(RegularBundleInterface::class, RegularBundleRepository::class);
        $this->app->bind(AdvanceBundleInterface::class, AdvanceBundleRepository::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */

//    public function boot()
//    {
//        $this->app->bind(ProductInterface::class, ProductCategory::class);
//    }


    public function provides()
    {
        return [];
    }
}
