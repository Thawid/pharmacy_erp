@extends('dboard.index')

@section('title', 'Product Create')

@push('styles')

    <style>
        nav>.nav-tabs>a.nav-item {
            font-size: 18px;
        }

        .row.tile {
          border-radius: 0 0 8px 8px;

        }


        .c-ml-1 {
            margin-left: 1px;
        }

        .c-mr-1 {
            margin-right: -1px;
        }

        .next-button {
            margin-top: -47px;
            z-index: 3;
            position: absolute;
            right: 49px;
            bottom: 40px;
            /* color: #006ABD !important; */
        }

        .next-btn {
            background: rgba(0, 136, 255, 0.1);
            border: 1px solid #0088FF;
            box-sizing: border-box;
            color: #0088FF !important;
        }

        .next-btn:hover {
            background: rgba(0, 136, 255, 0.1);
            color: #0088FF;
        }

         a#add_box_pack_row_btn {
            color: #00C880 !important;
            margin-top: 28px;
        }

        a#add_box_pack_row_btn:hover {
            color: #00C880 !important;
        }

        a#add_inventory_row_button {
            color: #006ABD !important;
        }

        a#add_inventory_row_button:hover {
            color: white !important;
        }

        .description-submit-button {
            margin-top: -47px;
            z-index: 3;
            position: absolute;
            right: 16px;
            bottom: 0;
            color: #0088FF !important;
        }

        /* .variation-next-button {
            bottom: 12px;
            right: 16px;
        } */

        /* .pack-next-button {
            bottom: 10px;
            right: 20px;
            margin-bottom: -20px;
        } */

        .inventory-next-button {
            bottom: -10px;
            right: 25px;
        }

        .general_info_table {
            padding-top: 30px;
        }

        .thumb {
            margin: 10px 5px 0 0;
            height: 100px;
            width: 120px;
        }

        .thumb2 {
            margin: 10px 5px 0 0;
            height: 100px;
            width: 120px;
        }


        #add_box_pack_row_btn {
            margin-right: 18px;
            width: 83px;
        }

        i {
            font-size: 18px;
        }

        #nav-variation .input-area {
            background: white;
        }

        .add-btn-area {
            margin-top: 26px;
        }

        .pt-40 {
            padding-top: 40px;
        }

        .select2-container--default .select2-selection--single .select2-selection__placeholder {
            color: #4c4848;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            /* display: none; <- Crashes Chrome on hover */
            -webkit-appearance: none;
            margin: 0;
            /* <-- Apparently some margin are still there even though it's hidden */
        }

        input[type=number] {
            -moz-appearance: textfield;
            /* Firefox */
        }

        .var_spec_add_btn {
            margin-top: 27px;
        }

        .product-create-title>h2 {
            font-style: normal;
            font-weight: 500;
            font-size: 20px;
            line-height: 30px;
            color: #294A65;
        }

        /* .title-tile {
            height: 60px;
            line-height: 60px;
        } */
        .col-xl, .col-xl-auto, .col-xl-12, .col-xl-11, .col-xl-10, .col-xl-9, .col-xl-8, .col-xl-7, .col-xl-6, .col-xl-5, .col-xl-4, .col-xl-3, .col-xl-2, .col-xl-1, .col-lg, .col-lg-auto, .col-lg-12, .col-lg-11, .col-lg-10, .col-lg-9, .col-lg-8, .col-lg-7, .col-lg-6, .col-lg-5, .col-lg-4, .col-lg-3, .col-lg-2, .col-lg-1, .col-md, .col-md-auto, .col-md-12, .col-md-11, .col-md-10, .col-md-9, .col-md-8, .col-md-7, .col-md-6, .col-md-5, .col-md-4, .col-md-3, .col-md-2, .col-md-1, .col-sm, .col-sm-auto, .col-sm-12, .col-sm-11, .col-sm-10, .col-sm-9, .col-sm-8, .col-sm-7, .col-sm-6, .col-sm-5, .col-sm-4, .col-sm-3, .col-sm-2, .col-sm-1, .col, .col-auto, .col-12, .col-11, .col-10, .col-9, .col-8, .col-7, .col-6, .col-5, .col-4, .col-3, .col-2, .col-1 {
            padding-right: 10px;
            padding-left: 10px;
          }  

        

        @media (min-width: 992px) and (max-width: 1199px) {
          nav>.nav-tabs>a.nav-item {
            font-size: 14px;
            font-weight: bolder;
          }

          .tile {
            padding: 15px;
          }

          .row.tile {
              border-radius: 0 0 8px 8px;
              padding: 10px;
          }

          label {
            display: inline-block;
            margin-bottom: 0.5rem;
            font-size: 13px !important;
          }

          input::-webkit-input-placeholder {
            font-size: 14px;
          }

          .col-xl, .col-xl-auto, .col-xl-12, .col-xl-11, .col-xl-10, .col-xl-9, .col-xl-8, .col-xl-7, .col-xl-6, .col-xl-5, .col-xl-4, .col-xl-3, .col-xl-2, .col-xl-1, .col-lg, .col-lg-auto, .col-lg-12, .col-lg-11, .col-lg-10, .col-lg-9, .col-lg-8, .col-lg-7, .col-lg-6, .col-lg-5, .col-lg-4, .col-lg-3, .col-lg-2, .col-lg-1, .col-md, .col-md-auto, .col-md-12, .col-md-11, .col-md-10, .col-md-9, .col-md-8, .col-md-7, .col-md-6, .col-md-5, .col-md-4, .col-md-3, .col-md-2, .col-md-1, .col-sm, .col-sm-auto, .col-sm-12, .col-sm-11, .col-sm-10, .col-sm-9, .col-sm-8, .col-sm-7, .col-sm-6, .col-sm-5, .col-sm-4, .col-sm-3, .col-sm-2, .col-sm-1, .col, .col-auto, .col-12, .col-11, .col-10, .col-9, .col-8, .col-7, .col-6, .col-5, .col-4, .col-3, .col-2, .col-1 {
            padding-right: 10px;
            padding-left: 10px;
          }  
          
          .fg-mb-0 {
            margin-bottom: 0;
          }

          thead th {
              font-style: normal;
              font-weight: 500;
              font-size: 14px;
          }
        }

    </style>
@endpush

@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile title-tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-9 text-left product-create-title">
                            <h2 class="title-heading">Create Product</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn index-btn" href="{{ route('products.index') }}">Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    <!-- data-area -->
    <section id="tabs">
        <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data" id="product_submit_form">
            @csrf
            <div class="row">
                <div class="col-md-12 ">
                    <nav>
                        <!-- tab title -->
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">

                            <a class="nav-item nav-link active" id="nav-general-info-tab" data-toggle="tab"
                                href="#nav-general-info" role="tab" aria-controls="nav-general-info"
                                aria-selected="true">General Info</a>

                            <a class="nav-item nav-link" id="nav-variation-tab" data-toggle="tab" href="#nav-variation"
                                role="tab" aria-controls="nav-variation" aria-selected="false">Properties &
                                Specification</a>

                            <a class="nav-item nav-link" id="nav-box-pack-tab" data-toggle="tab" href="#nav-box-pack"
                                role="tab" aria-controls="nav-box-pack" aria-selected="false">Box & Packaging</a>

                            <a class="nav-item nav-link" id="nav-inventory-setup-tab" data-toggle="tab"
                                href="#nav-inventory-setup" role="tab" aria-controls="nav-inventory-setup"
                                aria-selected="false">Inventory Setup</a>

                            <a class="nav-item nav-link" id="nav-description-tab" data-toggle="tab" href="#nav-description"
                                role="tab" aria-controls="nav-description" aria-selected="false"
                                onclick="desRowAddValidation()">Description</a>
                        </div>
                    </nav>

                    <!-- tab content -->
                    <div class="tab-content px-3 px-sm-0" id="nav-tabContent">

                        <div class="tab-pane fade show active" id="nav-general-info" role="tabpanel"
                            aria-labelledby="nav-general-info-tab">
                            <div class="row tile" style="margin-left: 1px; margin-right: 1px;">
                                <div class="col-md-12 pt-3">


                                    <!-- row for first column -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="brand_name" class="forget-form">Brand Name</label>
                                                <input type="text" class="form-control" id="brand_name" name="brand_name"
                                                    value="{{ old('brand_name') }}" placeholder="write brand name" autocomplete="off">
                                            </div><!-- form-group -->
                                        </div><!-- end .col-md-3 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="product_name" class="forget-form">Product
                                                    Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="product_name" value="{{ old('product_name') }}"
                                                    id="product_name" placeholder="write product name"
                                                    onkeyup="generalInfo(); productName();" autocomplete="off">
                                                <span id="pname" style="color: red;"></span>
                                            </div>
                                        </div><!-- end .col-md-3 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    @include('product::shares.product-types')
                                                </div><!-- end .form-group -->
                                            </div>
                                        </div><!-- end .col-md-3 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="forget-form">Product Category <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" name="parent_id" id="pcategory"
                                                    onchange="generalInfo()">
                                                </select>
                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-3 -->
                                    </div><!-- end .row -->


                                    <!-- row for second column -->
                                    <div class="row mt-5">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="sub_category" class="forget-form">Product Sub Category<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" id="sub_cat"
                                                    name="sub_category_id" onchange="generalInfo()">
                                                </select>
                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-4 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="forget-form">Product Generic<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" name="product_generic_id"
                                                    id="product_generic_id" onchange="generalInfo()">

                                                </select>
                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-4 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="manufacturer" class="forget-form">Manufacturer<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select"
                                                    name="manufacturer_id" id="manufacturer" onchange="generalInfo()">
                                                    <option disabled selected>Select Manufacturer</option>
                                                    @foreach ($manufacturers as $man)
                                                        <option value="{{ $man->id }}" @if(old('manufacturer_id') == $man->id) selected="selected" @endif>{{ $man->name }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-4 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                @include('product::shares.product-units')
                                            </div>
                                        </div>
                                    </div><!-- end .row -->

                                    <!-- 3rd row -->
                                    <div class="row mt-5 align-items-end">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="purchase_uom" class="form-label">Purchase Uom<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" name="purchase_uom"
                                                    id="purchase_uom" onchange="generalInfo(); bpAddValidation();">
                                                    <option disabled selected>Select UOM</option>
                                                    @foreach ($uoms as $uom)
                                                        <option value="{{ $uom->id }}" @if(old('purchase_uom') == $uom->id) selected="selected" @endif>
                                                            {{ $uom->uom_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="purchase_price" class="form-label">Purchase Price</label>
                                                <input type="number" name="purchase_price" class="form-control" value="{{old('purchase_price')}}">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group fg-mb-0">
                                                <label class="checkbox-inline pr-2">
                                                    <input name="is_returnable" type="checkbox" value="1"> IsReturnable
                                                </label>
                                                <label class="checkbox-inline pr-2">
                                                    <input name="is_emergency" type="checkbox" value="1"> IsEmergency
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input name="is_exchangeable" type="checkbox" value="1"> IsExchangeable
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <!-- button -->
                                    <div class="row text-right mt-0">
                                        <div class="col-md-12">
                                            <div id="gInfo">
                                                <a class="btn next-btn info-next-button"
                                                        id="info-next-button">Next</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-general-info -->

                        <div class="tab-pane fade" id="nav-variation" role="tabpanel" aria-labelledby="nav-variation-tab">
                            <div class="row tile mr-0 ml-0">
                                <div class="col-md-12">
                                    <!-- row for input field -->
                                    <div class="row pt-4" style="">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="product_variant">Property Name </label>
                                                <select class="form-control single-select" id="variant"
                                                    onchange="varSpecAddValidation()">

                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="variant_property_id" class="forget-form">Properties
                                                </label>
                                                <select class="form-control single-select" id="variant_property"
                                                    onchange="varSpecAddValidation()">

                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="add-btn-area">
                                                <a href="#" class="btn add-btn w-100" id="add_item">Add</a>
                                            </div>
                                        </div>
                                    </div><!-- end .row for input fields -->

                                    <!-- row for table -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- generated table area -->
                                            <div class="adding-items-area" id="nav_generated_data_table">
                                                <table
                                                    class="table table-striped table-bordered table-hover bg-white table-sm">
                                                    <thead>
                                                        <tr class="">
                                                            <th width="47.5%">Property Name</th>
                                                            <th width="47.5%">Properties</th>
                                                            <th width="3%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="item_add_area">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- end .col-md-12 -->
                                    </div><!-- end .row -->
                                    <hr>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="specification_text_area" class="forget-form"
                                                    style="font-size: 20px">
                                                    Specification
                                                </label>
                                                <textarea class="ckeditor form-control" name="specifications">{{old('specifications')}}</textarea>
                                            </div>
                                        </div><!-- end.col-md-12 -->
                                    </div><!-- end.row -->
                                    <hr>

                                    <div class="row text-right">
                                      <div class="col-md-12">
                                      <div class="form-group" id="vSpec">
                                        <a id="variation-next-button"
                                            class="btn next-btn variation-next-button">Next</a>
                                    </div>
                                      </div>
                                    </div>
                                </div> <!-- end .col-md-12 -->
                            </div> <!-- end .row -->
                        </div> <!-- end nav-variation -->

                        <div class="tab-pane fade" id="nav-box-pack" role="tabpanel" aria-labelledby="nav-box-pack-tab">
                            <div class="row tile mr-0 ml-0 pt-5">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="uom" class="form-label">UOM <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" name="uom" id="uom"
                                                    onchange="bpAddValidation()">
                                                    <option value="" selected disabled>Select UOM</option>
                                                    @foreach ($uoms as $uom)
                                                        <option value="{{ $uom->id }}">
                                                            {{ $uom->uom_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="quantity_per_uom" class="form-label">Quantity Per
                                                    UOM <span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="quantity_puom"
                                                    name="quantity_per_uom" autocomplete="off" onkeyup="bpAddValidation()"
                                                    placeholder="Enter quantity per uom">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <a class="btn add-btn add_box_pack_row_button w-100"
                                                id="add_box_pack_row_btn">Add</a>
                                        </div>
                                    </div><!-- end .row -->

                                    {{-- <!-- generated table area --> --}}
                                   <div class="row">
                                     <div class="col-md-12">
                                     <div class="adding-items-area" id="pac_generated_data_table">
                                        <table class="table table-striped table-bordered bg-white table-hover table-sm"
                                            style="margin-top: 10px;">
                                            <thead>
                                                <tr class="">
                                                    <th style="width: 47.5%;">UOM</th>
                                                    <th style="width: 47.5%;">Quantity Per UOM</th>
                                                    <th width="5%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="item_add_area_pb">

                                            </tbody>
                                        </table><!-- end generated-table-data -->
                                    </div>
                                     </div><!-- end.col-md-12 -->
                                   </div><!-- end.row -->
                                   <hr>

                                    <div class="row text-right">
                                      <div class="col-md-12">
                                      <div class="text-right" id="bPack">
                                        <a class="btn next-btn pack-next-button">Next</a>
                                    </div>
                                      </div>
                                    </div>
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-box-pack -->

                        <div class="tab-pane fade" id="nav-inventory-setup" role="tabpanel"
                            aria-labelledby="nav-inventory-setup-tab">
                            <div class="row tile ml-0 mr-0">
                                <div class="col-md-12">
                                    <table id="dynamic_field_inventory_setup" class="table">
                                        <tbody>
                                            <tr>
                                                <td width="50%" style="padding-top: 40px; border: none;">
                                                    <div class="form-group">
                                                        <label for="inventory_type" class="forget-form">Inventory
                                                            Type <span class="text-danger">*</span></label>
                                                        <select class="form-control single-select" id="inventory_type"
                                                            name="inventory_type" onchange="invSetup()">
                                                            <option value="" selected disabled>Select Inventory Type</option>
                                                            <option value="1" @if (old('inventory_type') == '1') selected="selected" @endif>Work-In-Process</option>
                                                            <option value="2" @if (old('inventory_type') == '2') selected="selected" @endif>Cycle Stock</option>
                                                            <option value="3" @if (old('inventory_type') == '3') selected="selected" @endif>Pipeline Stock</option>
                                                            <option value="4" @if (old('inventory_type') == '4') selected="selected" @endif>Hedge Inventory</option>
                                                        </select>
                                                    </div>
                                                </td>

                                                <td width="50%" style="padding-top: 40px; border: none;">
                                                    <div class="form-group">
                                                        <label for="inventory_state" class="forget-form">Inventory
                                                            State <span class="text-danger">*</span></label>
                                                        <select class="form-control single-select" id="inventory_state"
                                                            name="inventory_state" onchange="invSetup()">
                                                            <option value="" selected disabled>Select Inventory State</option>
                                                            <option value="1" @if (old('inventory_state') == '1') selected="selected" @endif>Normal</option>
                                                            <option value="2" @if (old('inventory_state') == '2') selected="selected" @endif>Cold</option>
                                                            <option value="3" @if (old('inventory_state') == '3') selected="selected" @endif>Cool</option>
                                                            <option value="4" @if (old('inventory_state') == '4') selected="selected" @endif>Warm</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <div id="iSet" class="text-right" style="margin-bottom: -30px;">
                                                        <a
                                                            class="btn next-btn inventory-next-button">Next
                                                            </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-inventory-setup -->

                        <div class="tab-pane fade" id="nav-description" role="tabpanel"
                            aria-labelledby="nav-description-tab">
                            <div class="row tile mr-0 ml-0">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="dynamic_field_description" class="table c-mr-1">
                                                <thead>
                                                    <tr>
                                                        <td class="border-0">
                                                            Feature Image<span class="text-danger">*</span> (Max One)
                                                        </td>

                                                        <td class="border-0">
                                                            Images (Multiple)
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <div class="form-group">
                                                                <input type="file" id="feature_image" class="form-contorl"
                                                                    name="feature_image" onchange="return fileValidation()" />
                                                                <div id="thumb-output"></div>
                                                            </div>
                                                        </td>

                                                        <td style="width: 50%;">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control" id="images"
                                                                    name="images[]" multiple>
                                                                <div id="images-output"></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <hr>

                                        <div class="col-md-12">
                                            <!-- short-description -->
                                            <div class="form-group mb-5">
                                                <label for="short_description_text_area" class="forget-form"
                                                    style="font-size: 20px">Short Description
                                                </label>
                                                <textarea class="ckeditor form-control" name="short_description">{{old('short_description')}}</textarea>
                                            </div>

                                            <!-- long description -->
                                            <div class="form-group mb-5">
                                                <label for="long_description_text_area" class="forget-form"
                                                    style="font-size: 20px">
                                                    Long Description
                                                </label>
                                                <textarea class="ckeditor form-control" name="long_description">{{old('long_description')}}</textarea>
                                            </div>
                                        </div> <!-- end .col-md-12 -->
                                    </div> <!-- end .row -->

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div id="submitButton">
                                                <button class="btn next-btn description-submit-button" type="submit">
                                                    Submit</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- end nav-description -->

                    </div> <!-- end .tab-content -->
                </div> <!-- end .col-md-12 -->
            </div><!-- end .row -->
        </form><!-- end form -->
    </section>


@endsection

@push('post_scripts')

    {{-- <!-- Page specific javascripts--> --}}
    {{-- <!-- Data table plugin --> --}}
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <!-- validation library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
    <!-- ck-editor-plugin -->
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <!-- validate js -->
    <script src="{{ asset('js/plugins/jquery.validate.js') }}"></script>

    <!-- validation library -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#gInfo').hide();
            $("#add_item").hide();
            //$('#vSpec').hide();
            $("#add_box_pack_row_btn").hide();
            $('#bPack').hide();
            $("#add_inventory_row_button").hide();
            $('#iSet').hide();
            $("#add_pv_image_button").hide();
            $("#nav_generated_data_table").hide();
            $("#pac_generated_data_table").hide();
            $("#inv_generated_data_table").hide();
            // $("#submitButton").hide();
            $(".multiple-select").select2({
                width: '100%',
                selectedTop: false,
            });
            $(".single-select").select2({
                width: '100%'
            });
            $('.ckeditor').ckeditor();
            generalInfo();
            desRowAddValidation();
            // desSub();

        });
    </script>

    <!-- all show hide functions -->
    <script>
        function generalInfo() {
            let productName = $("#product_name").val();
            let productNameValue = productName.length;
            let selectedProductType = $("#product_type option:selected").val();
            let selectedProductCategory = $("#pcategory option:selected").val();
            let selectedSubCategoryValue = $("#sub_cat option:selected").val();
            let selectedProductUnitValue = $("#unit_type option:selected").val();
            let selectedProductGenericValue = $("#product_generic_id option:selected").val();
            let selectedPurchaseUomValue = $("#purchase_uom option:selected").val();
            let selectedManufacturerValue = $("#manufacturer option:selected").val();

            if (productNameValue >= 2 &&
                selectedProductType > 0 &&
                selectedProductCategory > 0 &&
                selectedSubCategoryValue > 0 &&
                selectedProductUnitValue > 0 &&
                selectedProductGenericValue > 0 &&
                selectedPurchaseUomValue > 0 &&
                selectedManufacturerValue > 0)
            {
                $("#gInfo").show();
            } else {
                $("#gInfo").hide();
            }
        }

        /*function varSpec() {
            if ($("#ag_var_name").val().length > 0 && $("#ag_var_pro").val().length > 0) {
                $("#vSpec").show();
            } else {
                $("#vSpec").hide();
            }
        }*/

        function boxPack() {
            if ($(".uom_id_ag").val().length > 0 && $(".quantity_per_uom").val().length > 0) {
                $("#bPack").show();
            } else {
                $("#bPack").hide();
            }
        }

        function invSetup() {
            if ($("#inventory_type option:selected").val() > 0 &&
                $("#inventory_state option:selected").val() > 0) {
                $("#iSet").show()
            } else {
                $("#iSet").hide();
            }
        }

        function navGenDataTableShow() {
            $("#nav_generated_data_table").show();
        }

        function boxGenPakDataTableShow() {
            $("#pac_generated_data_table").show();
        }

        function invGenDataTableShow() {
            $("#inv_generated_data_table").show();
        }
    </script>

    <!-- validation for adding new row -->
    <script>
        function varSpecAddValidation() {
            let selected_variant_value = $("#variant").val();
            let selected_variant_type = $("#variant_property").val();
            if (selected_variant_value > 0 && selected_variant_type > 0) {
                $("#add_item").show();
            } else {
                $("#add_item").hide();
            }
        }

        function bpAddValidation() {
            if ($("#uom").val() > 0 && $("#quantity_puom").val() > 0) {
                $("#add_box_pack_row_btn").show();
            } else {
                $("#add_box_pack_row_btn").hide();
            }
        }

        // function invAddValidation() {
        //     if ($("#product_variation_inventory").text().length > 0 &&
        //         $("#product_variation_inventory").val() != "" &&
        //         $("#inventory_type").val() > 0 &&
        //         $("#inventory_state").val() > 0) {
        //         $("#add_inventory_row_button").show();
        //     } else {
        //         $("#add_inventory_row_button").hide();
        //     }
        // }

        function desRowAddValidation() {
            console.log("c");
            if ($("#product_variation_description").text().length > 0 &&
                $("#product_variation_description").val() != "") {
                $("#add_pv_image_button").show();
            } else {
                $("#add_pv_image_button").hide();
            }
        }
    </script>

    <!-- single image preview and validation check-->
    <script>
        function fileValidation() {
            var fileInput = document.getElementById('feature_image');

            var filePath = fileInput.value;

            // Allowing file type
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i; /*/(\.doc|\.docx|\.odt|\.pdf|\.tex|\.txt|\.rtf|\.wps|\.wks|\.wpd)$/i;*/

            if (!allowedExtensions.exec(filePath)) {
                alert('Invalid file type');
                fileInput.value = '';
                return false;
            }
            else
            {
                // Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById(
                            'thumb-output').innerHTML =
                            '<img src="' + e.target.result
                            + '" class="thumb"/>';
                    };

                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }
    </script>

    <!-- multiple image preview and validation check-->
    <script>
        $(document).ready(function() {
            $('#images').on('change', function() { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {
                    var fileInput = document.getElementById('images');

                    var filePath = fileInput.value;

                    // Allowing file type
                    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;

                    if (!allowedExtensions.exec(filePath)) {
                        alert('Invalid file type');
                        fileInput.value = '';
                        return false;
                    }

                    var data = $(this)[0].files; //this file data

                    $.each(data, function(index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                                .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function(file) { //trigger function on successful read
                                return function(e) {
                                    var img = $('<img/>').addClass('thumb2').attr('src',
                                        e.target.result); //create image element
                                    $('#images-output').append(
                                        img); //append image to output element
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });

                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>

    <!-- next-button & previous-button -->
    <script>
        var generalInfoTab = $("a#nav-general-info-tab");
        var variationSetupTab = $("a#nav-variation-tab");
        var boxPackTab = $("a#nav-box-pack-tab");
        var inventorySetupTab = $("a#nav-inventory-setup-tab");
        var descriptionTab = $("a#nav-description-tab");

        var generalInfoTabContent = $("#nav-general-info");
        var variationSetupTabContent = $("#nav-variation");
        var boxPackTabContent = $("#nav-box-pack");
        var inventorySetupTabContent = $("#nav-inventory-setup");
        var descriptionTabContent = $("#nav-description");


        // goto "general-info" to "brand & manufacturer"
        $(".info-next-button").click(function() {
            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.addClass(['active', 'show']).attr('aria-labelledby', 'nav-variation-tab');
        });

        // goto "variation-setup" to "pack size"
        $(".variation-next-button").click(function() {

            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.addClass(['active', 'show']).attr('aria-labelledby', 'nav-box-pack-tab');

        });

        // goto "pack size" to "inventory-setup"
        $(".pack-next-button").click(function() {

            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.addClass(['active', 'show']).attr('aria-labelledby',
                'nav-inventory-setup-tab');

        });

        // goto "inventory-setup" to "description"
        $(".inventory-next-button").click(function() {

            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.addClass(['active', 'show']).attr('aria-labelledby', 'nav-description-tab');

            desRowAddValidation();
            // desSub()
        });
    </script>

    <!-- catch selected option and add items on table for variation  -->
    <script>
        // Item Add function goes here
        $(document).on('click', '#add_item', function() {
            navGenDataTableShow();
            let variant_val = $("#variant option:selected").val();
            var variant_name = $("#variant option:selected").text();
            let variant_property_val = $("#variant_property option:selected").val();
            var variant_property_name = $("#variant_property option:selected").text();
            let unit_val = $("#unit_type option:selected").val();
            var unit_type_name = $("#unit_type option:selected").text();

            $('.item_add_area').append(
                `<tr>
                    <td>
                        <input class="form-control" value="${variant_name}" readonly>
                        <input type="hidden" class="form-control" name="variant_id[]" id="ag_var_name" value="${variant_val}" readonly>
                    </td>

                    <td>
                        <input class="form-control" value="${variant_property_name}" readonly>
                        <input type="hidden" class="form-control" name="variant_property_id[]" id="ag_var_pro" value="${variant_property_val} " readonly>
                    </td>

                    <td>
                        <a href="#" class="btn btn-outline-danger item_remove btn-xs w-100">X</a>
                    </td>
                </tr>`);

                // check next button validation
                if ($("#ag_var_name").val() > 0) {
                    $("#variation-next-button").show();
                } else {
                    $("#variation-next-button").hide();
                }

            // calling varSpec function for validation based upon auto generated table data
            varSpec();

            $('#variant').select2("val", " ");
            $('#variant').select2({
                placeholder: "Select Property Name"
            });

        });

        // Remove Item
        $(document).on('click', '.item_remove', function() {
            $(this).parent().parent().remove();

            // check next button validation
            if ($("#ag_var_name").val() > 0) {
                $("#variation-next-button").show();
            } else {
                $("#variation-next-button").hide();
            }
        });
    </script>

    <!-- add new box and pack for uom -->
    <script>
        // Item Add function goes here
        $(document).on('click', '#add_box_pack_row_btn', function() {

            boxGenPakDataTableShow();
            let uom_id_val = $("#uom option:selected").val();
            let uom_id_text = $("#uom option:selected").text();
            let quantity_per_uom_val = $("#quantity_puom").val();

            $('.item_add_area_pb').append(
                `<tr>
                    <td>
                        <input class="form-control" value="${uom_id_text}" readonly>
                        <input type="hidden" name="uom_id[]" id="selected_uom_val_ag_id" value="${uom_id_val}">
                    </td>

                    <td>
                        <input class="form-control" value="${quantity_per_uom_val}" readonly>
                        <input type="hidden" name="quantity_per_uom[]" value="${quantity_per_uom_val}">
                    </td>

                    <td>
                        <a href="#" class="btn btn btn-outline-danger w-100 item_remove btn-xs">X</a>
                    </td>
                </tr>`);

                // check next button validation
                if ($("#selected_uom_val_ag_id").val() > 0 ) {
                    $(".pack-next-button").show();
                } else {
                    $(".pack-next-button").hide();
                }

            // calling boxPack function for validation based upon auto generated table data
            $('#bPack').show();

            $('#uom').select2("val", " ");
            $('#uom').select2({
                placeholder: "Select UOM"
            });

            $("#quantity_puom").val('').change();

        });

        // Remove Item
        $(document).on('click', '.item_remove', function() {
            $(this).parent().parent().remove();

            // check next button validation
            if ($("#selected_uom_val_ag_id").val() > 0 ) {
                $(".pack-next-button").show();
            } else {
                $(".pack-next-button").hide();
            }

        });
    </script>

    <script>
        function productName() {
            let product_name = $("#product_name").val();
            var url = "{{ url('product/check-product-name') }}";
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    product_name: product_name
                },
                success: function(res) {
                    if (res) {
                        $("#pname").html(res)
                    } else {
                        $("#pname").html("")
                    }
                },
            });
        }
    </script>

    {{-- dependency script --}}
    <script>
        $(document).ready(function() {
            $('#product_type').on('change', function() {
                let id = $(this).val();
                $('#pcategory').empty();
                $('#pcategory').append(`<option value="" disabled selected></option>`);


                var url = "{{ url('product/get-product-category') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function(response) {
                        var response = JSON.parse(response);

                        $('#pcategory').empty();
                        $('#pcategory').append(
                            `<option value="" disabled selected>Select Product Category</option>`);

                        response.forEach(element => {
                            $('#pcategory').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    }
                });


                // for product generic
                $('#product_generic_id').empty();
                $('#product_generic_id').append(`<option value="" disabled selected></option>`);

                var url = "{{ url('product/get-product-generic') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function(response) {
                        var response = JSON.parse(response);
                        $('#product_generic_id').empty();
                        $('#product_generic_id').append(
                            `<option value="" disabled selected>Select Product Generic</option>`);

                        response.forEach(element => {
                            $('#product_generic_id').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    },
                    error: function(error) {}
                });

                // for product variant
                $('#variant').empty();
                $('#variant').append(`<option value="" selected></option>`);

                var url = "{{ url('product/get-product-variant') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function(response) {
                        var response = JSON.parse(response);
                        $('#variant').empty();
                        $('#variant').append(
                            `<option value="" selected>Select Property Name</option>`);

                        response.forEach(element => {
                            $('#variant').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    },
                    error: function(error) {}
                });
            });
        });
    </script>

    <!-- calling sub-category -->
    <script>
        $(document).ready(function() {
            $('#pcategory').on('change', function() {
                let id = $(this).val();
                $('#sub_cat').empty();
                $('#sub_cat').append(`<option value="0" selected>Processing...</option>`);

                /*$('#product_variation').empty();
                $('#product_variation').append(`<option value="" selected>Processing...</option>`);

                $('#product_variation_inventory').empty();
                $('#product_variation_inventory').append(
                    `<option value="" selected>Processing...</option>`);

                $('#product_variation_description').empty();
                $('#product_variation_description').append(
                    `<option value="" selected>Processing...</option>`);*/
                var url = "{{ url('product/get-product-sub-category') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function(response) {
                        var response = JSON.parse(response);
                        let bname = localStorage.getItem('brand_name');
                        $('#sub_cat').empty();
                        $('#sub_cat').append(
                            `<option value="0" disabled selected>Select Product Sub Category</option>`
                        );

                        /*$('#product_variation').empty();
                        $('#product_variation').append(
                            `<option value="" selected>Select Property Name</option>`);

                        $('#product_variation_inventory').empty();
                        $('#product_variation_inventory').append(
                            `<option value="" selected>Select Property Name</option>`);

                        $('#product_variation_description').empty();
                        $('#product_variation_description').append(
                            `<option value="">Select Property Name</option>`);*/
                        response.forEach(element => {
                            $('#sub_cat').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                            /*$('#product_variation').append(
                                `<option value="${element['id']}">${bname}</option>`
                            );
                            $('#product_variation_inventory').append(
                                `<option value="${element['id']}">${bname}</option>`
                            );
                            $('#product_variation_description').append(
                                `<option value="${element['id']}">${bname}</option>`
                            );*/
                        });
                    }
                });
            });
        });
    </script>

    <!-- calling variant -->
    <script>
        $(document).ready(function() {
            $('#variant').on('change', function() {
                let id = $(this).val();
                $('#variant_property').empty();
                $('#variant_property').append(`<option value="" selected>Processing...</option>`);
                var url = "{{ url('product/get-variant-property') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function(response) {
                        var response = JSON.parse(response);
                        $('#variant_property').empty();
                        $('#variant_property').append(
                            `<option value="" selected>Select Properties</option>`);
                        response.forEach(element => {
                            $('#variant_property').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    }
                });
            });
        });
    </script>

    <!-- catch variation data and save into dummy table -->
    <script>
        $(document).ready(function() {
            $("#variation-next-button").on('click', function(e) {
                e.preventDefault();

                const sub_category_id = [];
                const product_name_ag = [];
                /*const variant_id = [];
                const variant_property_id = [];*/
                const unit_id = [];

                $('input[name^="sub_category_id"]').each(function() {
                    sub_category_id.push($(this).val());
                });

                $('input[name^="product_name_ag"]').each(function() {
                    product_name_ag.push($(this).val());
                });

                /*$('input[name^="variant_id"]').each(function() {
                    variant_id.push($(this).val());
                });
                $('input[name^="variant_property_id"]').each(function() {
                    variant_property_id.push($(this).val());
                });*/
                $('input[name^="unit_id"]').each(function() {
                    unit_id.push($(this).val());
                });
                $.ajax({
                    url: "{{ route('save_temp_data') }}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        sub_category_id: sub_category_id,
                        product_name: product_name_ag,
                        /*variant_id: variant_id,
                        variant_property_id: variant_property_id,*/
                        unit_id: unit_id,
                    },
                    success: function(res) {},
                });

            })
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#variation-next-button").on('click', function(e) {
                e.preventDefault();
                var url = "{{ url('product/get-temp-data') }}";
                var dltUrl = url;
                $.ajax({
                    url: dltUrl,
                    type: "GET",
                    success: function(response) {
                        var response = JSON.parse(response);
                        $('#product_variation').empty();
                        $('#product_variation').append(
                            `<option value="">Select Product Variation Name</option>`);

                        /*$('#variantsid').empty();
                        $('#variantsid').append(
                            `<option value="">Select Variant Name</option>`);

                        $('#vproperties').empty();
                        $('#vproperties').append(
                            `<option value="">Select Variant Name</option>`);*/

                        $('#product_variation_inventory').empty();
                        $('#product_variation_inventory').append(
                            `<option value="">Select Product Variation Name</option>`);

                        $('#product_variation_description').empty();
                        $('#product_variation_description').append(
                            `<option value="">Select Product Variation Name</option>`);
                        response.forEach(element => {
                            $('#product_variation').append(
                                `<option value="${element.product_name}">${element.product_name}</option>`
                            );
                            /*$('#variantsid').append(
                                `<option value="${element.variant['id']}">${element.variant['name']}</option>`
                            );
                            $('#vproperties').append(
                                `<option value="${element.variant_values['id']}">${element.variant_values['name']}</option>`
                            );*/
                            $('#product_variation_inventory').append(
                                `<option value="${element.product_name}">${element.product_name}</option>`
                            );
                            $('#product_variation_description').append(
                                `<option value="${element.product_name}">${element.product_name}</option>`
                            );
                        });

                    },
                });

            })
        });
    </script>
    {{-- end dependency script --}}
@endpush
