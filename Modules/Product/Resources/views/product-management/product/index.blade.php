@extends('dboard.index')

@section('title', 'Products')

@push('styles')
    <style>


    </style>
@endpush

@section('dboard_content')

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="title-heading">Product List</h2>
                                </div><!-- end col-md-6 -->

                                <div class="col-md-6 text-right">
                                    <a class="btn create-btn" href="{{ route('products.create') }}">Add Product</a>
                                </div><!-- ene .col-md-6 -->
                            </div>
                            <hr>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th  width="15%">Product Image</th>
                                            <th>Product Brand Name</th>
                                            <th>Product Name</th>
                                            <th width="8%">Action</th>
                                        </tr><!-- end tr -->
                                    </thead><!-- end thead -->

                                    <tbody class="align-items-center">
                                        @if (isset($products))
                                            @foreach ($products as $key => $row)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>
                                                        <img src="{{ !empty($row->ProductDetails) ? asset('feature-image' . '/' . $row->ProductDetails->feature_image) : asset('img/default-image.jpg') }}"
                                                            alt="Product images" height="60px" width="60px">
                                                    </td>
                                                    <td>{{ $row->brand_name ?? ''}}</td>
                                                    <td>{{ $row->product_name ?? ''}}</td>

                                                    <td>
                                                       <div class="d-flex justify-content-between align-items-center">
                                                           <a href="{{ route('products.edit', $row->id) }}"
                                                            class="btn edit-btn">
                                                            <i class="fa fa-lg fa-pencil"></i></a>

                                                        <a href="{{ route('products.show', $row->id) }}"
                                                            class="btn details-btn">
                                                            <i class="fa fa-eye" aria-hidden="true"
                                                                data-toggle="tooltip" title="Details"></i></a>
                                                       </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody><!-- end tbody -->
                                </table><!-- end table -->
                            </div><!-- end .table-responsive -->
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->

                    <div class="col-md-12"></div>
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    <!-- data-area -->
    <div class="row">

    </div><!-- end .row -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
@endpush
