@extends('dboard.index')

@section('title', 'Product Show')

@push('styles')


    <style>
        body {
            font-size: 16px;
            box-sizing: border-box;
        }

        ul {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        .description-multiple-image-area {
            height: 100px;
            width: 100px;
            border-radius: 15px;
        }

        .description-multiple-image-area img {
            height: 100px;
            width: 100px;
            border-radius: 15px;
            margin-right: 10px;
        }

    </style>
@endpush

@section('dboard_content')
    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading">Show Product</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn index-btn"
                            href="{{ route('products.index') }}">Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->


    <!-- data-area -->
    <!-- general info row -->
    <div class="row">
        <div class="col-md-12">
            <div class="general-info-data-area">
                <div class="card">
                    <div class="card-header">
                        <h2 class="title-heading">General Information</h2>
                    </div><!-- end .card-header -->
                    <div class="card-body">
                        <ul class="d-md-flex">
                            <li class="pr-5">
                                <p><strong>Brand Name:</strong> {{ $products->brand_name }}</p>
                            </li>

                            <li class="pr-5">
                                <p><strong>Product Name:</strong> {{ $products->product_name }}</p>
                            </li>
                            <li class="pr-5">
                                <p><strong>Product Type:</strong> {{ $products->ptype->name }}</p>
                            </li>
                            <li class="pr-5">
                                <p><strong>Product
                                        Category:</strong> {{ $products->productCategory->name }}</p>
                            </li>
                            <li class="pr-5">
                                <p><strong>Product Sub
                                        Category:</strong> {{ $products->productSubCategory->name }}</p>
                            </li>
                            <li class="pr-5">
                                <p>
                                    <strong>Manufacturer:</strong>
                                    @if (isset($products->manufacturer))
                                        {{ $products->manufacturer->name }}
                                    @endif
                                </p>
                            </li>
                            {{--<li>
                                <p><strong>Product
                                        Generic:</strong>
                                    @if (isset($products->pgeneric))
                                        {{ $products->pgeneric->name }}
                                    @endif
                                </p>
                            </li>--}}
                        </ul>
                    </div><!-- end card-body -->
                </div><!-- end .card -->
            </div><!-- end .general-info-data-area -->
        </div><!-- end .col-md-12 -->
    </div><!-- end general info .row -->

    <!-- variation & specification row -->
    <div class="row">
        <div class="col-md-12">
            <div class="variation-specification-data-area mt-5">
                <div class="card">
                    <div class="card-header">
                        <h2 class="title-heading">Variation & Specification</h2>
                    </div><!-- end .card-header -->
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm">
                            <thead>
                            <tr class="">
                                <th style="width: 50%;">Property</th>
                                <th style="width: 50%;">Properties</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products->productVariations as $key => $variations)
                                <tr>
                                    <td>{{ $variations->variant->name }}</td>
                                    <td>{{ $variations->variantProperty->name }}</td>

                                </tr>
                            @endforeach
                            </tbody><!-- end tbody -->
                        </table><!-- end table -->
                    </div><!-- end .card-body -->
                </div><!-- end .card -->
            </div><!-- end .variation-specification-data-area -->
        </div><!-- end col-md-12 -->
    </div><!-- end variation & specification .row -->

    <!-- box & pack row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box-pack-data-area mt-5">
                <div class="card">
                    <div class="card-header">
                        <h2 class="title-heading">Box & Packaging</h2>
                    </div><!-- end .card-header -->
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm">
                            <thead>
                            <tr class="">
                                <th style="width: 20%;">UOM</th>
                                <th style="width: 20%;">Quantity per uom</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products->Productuom as $key => $uom)
                                <tr>
                                    <td>{{ $uom->uom->uom_name }} </td>
                                    <td>{{ $uom->quantity_per_uom }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table><!-- end table -->

                        <div class="specification-text-area">
                            <h4>Specification</h4>
                            @if (isset($products->productDetails))
                                <p>{!! $products->productDetails->specifications !!}</p>
                            @endif
                        </div>
                    </div><!-- end .card-body -->
                </div><!-- end .card -->
            </div><!-- end box-pack-data-area -->
        </div><!-- end .col-md-12 -->
    </div><!-- end box & pack .row -->

    <!-- inventory setup row -->
    <div class="row">
        <div class="col-md-12">
            <div class="inventory-setup-data-area mt-5">
                <div class="card">
                    <div class="card-header">
                        <h2 class="title-heading">Inventory Setup</h2>
                    </div><!-- end .card-header -->
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm">
                            <thead>
                            <tr class="">
                                <th width="50%">Inventory Type</th>
                                <th width="50%">Inventory State</th>
                            </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>
                                        @if ($products->inventory_type == 1)
                                            {{ 'Work-In-Process' }}
                                        @endif

                                        @if ($products->inventory_type == 2)
                                            {{ 'Cycle Stock' }}
                                        @endif

                                        @if ($products->inventory_type == 3)
                                            {{ 'Pipeline Stock' }}
                                        @endif

                                        @if ($products->inventory_type == 4)
                                            {{ 'Hedge Inventory' }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($products->inventory_state == 1)
                                            {{ 'Normal' }}
                                        @endif
                                        @if ($products->inventory_state == 2)
                                            {{ 'Cold' }}
                                        @endif

                                        @if ($products->inventory_state == 3)
                                            {{ 'Cool' }}
                                        @endif

                                        @if ($products->inventory_state == 4)
                                            {{ 'Warm' }}
                                        @endif
                                    </td>
                                </tr>
                            </tbody><!-- end tbody -->
                        </table><!-- end table -->
                    </div><!-- end .card-body -->
                </div><!-- end .card -->
            </div><!-- end .inventory-setup-data-area -->
        </div><!-- end col-md-12 -->
    </div><!-- end inventory setup .row -->

    <!-- description row -->
    <div class="row">
        <div class="col-md-12">
            <div class="inventory-setup-data-area mt-5">
                <div class="card">
                    <div class="card-header">
                        <h2 class="title-heading">Description</h2>
                    </div><!-- end .card-header -->
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm">
                            <thead>
                            <tr class="">
                                <th style="width: 33.33%;">Product Feature Image</th>
                                <th style="width: 33.33%;">Images</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <img src="{{ asset('feature-image' . '/' . $products->productDetails->feature_image) }}" alt="Product Image" width="90px" height="100px">

                                </td>
                                <td>
                                    <div class="description-multiple-image-area d-md-flex">
                                        @php
                                        $images = explode("|", $products->productDetails->images);
                                        @endphp
                                        @foreach ($images as $key => $img)
                                            <img src="{{ asset('images' . '/' . $img) }}"
                                                 alt="Product Image">
                                        @endforeach

                                    </div>
                                </td>
                            </tr>
                            </tbody><!-- end tbody -->
                        </table><!-- end table -->

                        <div class="product-short-description-text-area">
                            <h4>Product Short Description</h4>
                            @if (isset($products->productDetails))
                                <p>{!! $products->productDetails->short_desc !!}</p>
                            @endif
                        </div>

                        <div class="product-long-description-text-area">
                            <h4>Product Long Description</h4>
                            @if (isset($products->productDetails))
                                <p>{!! $products->productDetails->long_desc !!}</p>
                            @endif
                        </div>
                    </div><!-- end .card-body -->
                </div><!-- end .card -->
            </div><!-- end .inventory-setup-data-area -->
        </div><!-- end col-md-12 -->
    </div><!-- end description .row -->

@endsection

@push('post_scripts')

@endpush
