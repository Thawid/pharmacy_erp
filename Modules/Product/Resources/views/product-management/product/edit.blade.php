@extends('dboard.index')

@section('title', 'Product Edit')

@push('styles')

    <style>
        nav > .nav-tabs > a.nav-item {
            font-size: 18px;
        }

        .row.tile {
          border-radius: 0 0 8px 8px;

        }

        .next-button {
            margin-top: -47px;
            z-index: 3;
            position: absolute;
            right: 49px;
            bottom: 40px;
            color: #0088FF !important;
        }

        .next-button:hover {
            color: #0088FF !important;
        }

        a#add_box_pack_row_btn {
            color: #006ABD !important;
            margin-top: 28px;
        }

        a#add_box_pack_row_btn:hover {
            color: white !important;
        }

        a#add_inventory_row_button {
            color: #006ABD !important;
        }

        a#add_inventory_row_button:hover {
            color: white !important;
        }

        .description-submit-button {
            /* right: 0; */
             color: #0088FF !important;
        }

        /* #des */

        .info-next-button {
            color: #0088FF !important;
        }


        .variation-next-button {
            bottom: -5px;
            right: 16px;
        }

        .pack-next-button {
            left: 0;
            color: #006ABD !important;

        }

        .inventory-next-button {
            margin-top: 28px;
            color: #006ABD !important;
        }

        .general_info_table {
            padding-top: 30px;
        }

        .thumb {
            margin: 10px 5px 0 0;
            height: 100px;
            width: 120px;
        }

        i {
            font-size: 18px;
        }

        #nav-variation .input-area {
            padding-top: 40px;
            padding-bottom: 60px;
            background: white;
        }

        .add-btn-area {
            margin-top: 26px;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            /* display: none; <- Crashes Chrome on hover */
            -webkit-appearance: none;
            margin: 0;
            /* <-- Apparently some margin are still there even though it's hidden */
        }

        input[type=number] {
            -moz-appearance: textfield;
            /* Firefox */
        }

        .thumb {
            margin: 10px 5px 0 0;
            height: 100px;
            width: 120px;
        }

        .thumb2 {
            margin: 10px 5px 0 0;
            height: 100px;
            width: 120px;
        }

        .show-feature-image {
            height: 150px;
            width: 150px;
            border-radius: 15px;
        }

        .show-images {
            height: 150px;
            width: 150px;
            border-radius: 15px;
        }

        .col-xl, .col-xl-auto, .col-xl-12, .col-xl-11, .col-xl-10, .col-xl-9, .col-xl-8, .col-xl-7, .col-xl-6, .col-xl-5, .col-xl-4, .col-xl-3, .col-xl-2, .col-xl-1, .col-lg, .col-lg-auto, .col-lg-12, .col-lg-11, .col-lg-10, .col-lg-9, .col-lg-8, .col-lg-7, .col-lg-6, .col-lg-5, .col-lg-4, .col-lg-3, .col-lg-2, .col-lg-1, .col-md, .col-md-auto, .col-md-12, .col-md-11, .col-md-10, .col-md-9, .col-md-8, .col-md-7, .col-md-6, .col-md-5, .col-md-4, .col-md-3, .col-md-2, .col-md-1, .col-sm, .col-sm-auto, .col-sm-12, .col-sm-11, .col-sm-10, .col-sm-9, .col-sm-8, .col-sm-7, .col-sm-6, .col-sm-5, .col-sm-4, .col-sm-3, .col-sm-2, .col-sm-1, .col, .col-auto, .col-12, .col-11, .col-10, .col-9, .col-8, .col-7, .col-6, .col-5, .col-4, .col-3, .col-2, .col-1 {
            padding-right: 10px;
            padding-left: 10px;
          }  

        

        @media (min-width: 992px) and (max-width: 1199px) {
          nav>.nav-tabs>a.nav-item {
            font-size: 14px;
            font-weight: bolder;
          }

          .tile {
            padding: 15px;
          }

          .row.tile {
              border-radius: 0 0 8px 8px;
              padding: 10px;
          }

          label {
            display: inline-block;
            margin-bottom: 0.5rem;
            font-size: 13px !important;
          }

          input::-webkit-input-placeholder {
            font-size: 14px;
          }

          .col-xl, .col-xl-auto, .col-xl-12, .col-xl-11, .col-xl-10, .col-xl-9, .col-xl-8, .col-xl-7, .col-xl-6, .col-xl-5, .col-xl-4, .col-xl-3, .col-xl-2, .col-xl-1, .col-lg, .col-lg-auto, .col-lg-12, .col-lg-11, .col-lg-10, .col-lg-9, .col-lg-8, .col-lg-7, .col-lg-6, .col-lg-5, .col-lg-4, .col-lg-3, .col-lg-2, .col-lg-1, .col-md, .col-md-auto, .col-md-12, .col-md-11, .col-md-10, .col-md-9, .col-md-8, .col-md-7, .col-md-6, .col-md-5, .col-md-4, .col-md-3, .col-md-2, .col-md-1, .col-sm, .col-sm-auto, .col-sm-12, .col-sm-11, .col-sm-10, .col-sm-9, .col-sm-8, .col-sm-7, .col-sm-6, .col-sm-5, .col-sm-4, .col-sm-3, .col-sm-2, .col-sm-1, .col, .col-auto, .col-12, .col-11, .col-10, .col-9, .col-8, .col-7, .col-6, .col-5, .col-4, .col-3, .col-2, .col-1 {
            padding-right: 10px;
            padding-left: 10px;
          }  
          
          .fg-mb-0 {
            margin-bottom: 0;
          }

          thead th {
              font-style: normal;
              font-weight: 500;
              font-size: 14px;
          }
        }

    </style>
@endpush

@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Edit Product</h2>
                        </div><!-- end .col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('products.index') }}">Back</a>
                        </div><!-- end .col-md-6 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    <!-- data-area -->
    <section id="tabs">
        <form action="{{ route('products.update', $products->id) }}" method="POST" enctype="multipart/form-data"
              id="product_submit_form">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-12 ">
                    <nav>
                        <!-- tab title -->
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">

                            <a class="nav-item nav-link active" id="nav-general-info-tab" data-toggle="tab"
                               href="#nav-general-info" role="tab" aria-controls="nav-general-info"
                               aria-selected="true">General Info</a>

                            <a class="nav-item nav-link" id="nav-variation-tab" data-toggle="tab" href="#nav-variation"
                               role="tab" aria-controls="nav-variation" aria-selected="false"
                               onclick="varSpecAddValidation()">Variation &
                                Specification</a>

                            <a class="nav-item nav-link" id="nav-box-pack-tab" data-toggle="tab" href="#nav-box-pack"
                               role="tab" aria-controls="nav-box-pack" aria-selected="false"
                               onclick="bpAddValidation()">Box & Packaging</a>

                            <a class="nav-item nav-link" id="nav-inventory-setup-tab" data-toggle="tab"
                               href="#nav-inventory-setup" role="tab" aria-controls="nav-inventory-setup"
                               aria-selected="false" onclick="invAddValidation()">Inventory Setup</a>

                            <a class="nav-item nav-link" id="nav-description-tab" data-toggle="tab"
                               href="#nav-description"
                               role="tab" aria-controls="nav-description" aria-selected="false"
                               onclick="">Description</a>
                        </div>
                    </nav>

                    <!-- tab content -->
                    <div class="tab-content px-3 px-sm-0" id="nav-tabContent">

                        <div class="tab-pane fade show active" id="nav-general-info" role="tabpanel"
                             aria-labelledby="nav-general-info-tab">
                            <div class="row tile" style="margin-left: 1px; margin-right: 1px;">
                                <div class="col-md-12 pt-3">

                                    <!-- row for first column -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="brand_name" class="forget-form">Brand Name</label>
                                                <input type="text" class="form-control" id="brand_name"
                                                       name="brand_name"
                                                       value="{{ $products->brand_name }}" autocomplete="off">
                                            </div><!-- form-group -->
                                        </div><!-- end .col-md-3 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="product_name" class="forget-form">Product
                                                    Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="product_name"
                                                       id="product_name" placeholder="enter product name"
                                                       onkeyup="generalInfo();" value="{{ $products->product_name }}"
                                                       autocomplete="off">
                                            </div>
                                        </div><!-- end .col-md-3 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="product_type" class="forget-form">Product Type <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control select2 product-type single-select"
                                                        name="product_type_id" id="product_type"
                                                        onchange="generalInfo()">
                                                    @foreach ($product_types as $type)
                                                        <option value="{{ $type->id }}"
                                                            {{ $type->id == $products->ptype->id ? 'selected' : '' }}>
                                                            {{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div><!-- end .col-md-3 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="forget-form">Product Category <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" name="parent_id"
                                                        id="pcategory"
                                                        onchange="generalInfo()">
                                                    @foreach ($product_categories as $category)
                                                        <option value="{{ $category->id }}"
                                                            {{ $category->id == $products->productCategory->id ? 'selected' : '' }}>
                                                            {{ $category->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-3 -->
                                    </div><!-- end .row -->

                                    <!-- row for second column -->
                                    <div class="row mt-5">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="sub_category" class="forget-form">Sub Category <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" id="sub_cat"
                                                        name="sub_category_id" onchange="generalInfo()">
                                                    <option value="">Select Sub Category</option>
                                                    @foreach ($product_subcats as $subcategory)
                                                        <option value="{{ $subcategory->id }}"
                                                            {{ $subcategory->id == $products->productSubCategory->id ? 'selected' : '' }}>
                                                            {{ $subcategory->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-4 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="product_type" class="forget-form">Select Product
                                                    Generic</label>
                                                <select class="form-control single-select" name="product_generic_id">
                                                    <option disabled selected>Select Product Generic</option>
                                                    @if ($products->pgeneric)
                                                        @foreach ($product_generics as $generic)
                                                            <option value="{{ $generic->id }}"
                                                                {{ $generic->id == $products->pgeneric->id ? 'selected' : '' }}>
                                                                {{ $generic->name }}
                                                            </option>
                                                        @endforeach
                                                    @else
                                                        @foreach ($product_generics as $generic)
                                                            <option
                                                                value="{{ $generic->id }}">{{ $generic->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-4 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="manufacturer" class="forget-form">Select
                                                    Manufacturer</label>
                                                <select class="form-control single-select" name="manufacturer_id"
                                                        id="manufacturer">
                                                    @if (isset($products->manufacturer))
                                                        @foreach ($manufacturers as $manufacturer)
                                                            <option value="{{ $manufacturer->id }}"
                                                                {{ $manufacturer->id == $products->manufacturer->id ? 'selected' : '' }}>
                                                                {{ $manufacturer->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div><!-- end .form-group -->
                                        </div><!-- end .col-md-4 -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {{-- @include('product::shares.product-units') --}}
                                                {{-- @include('product::shares.product-units',['field'=>'unit_id']) --}}
                                                <label for="unit_type" class="forget-form">Select Unit Type <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control select2 single-select"
                                                        name="{{ $field ?? 'unit_id' }}" id="unit_type"
                                                        onchange="varSpecAddValidation()">
                                                    <option value="">Select Unit Type</option>
                                                    @foreach ($units as $unit)
                                                        <option value="{{ $unit->id }}"
                                                            {{ $unit->id == $products->productUnit->id ? 'selected' : '' }}>
                                                            {{ $unit->unit_name }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div><!-- end .row -->

                                    <!-- 3rd row -->
                                    <div class="row mt-5 align-items-end">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="purchase_uom" class="form-label">Purchase Uom</label>
                                                <select class="form-control single-select" name="purchase_uom"
                                                        id="purchase_uom" onchange="bpAddValidation()">
                                                    <option disabled selected>Select UOM</option>
                                                    @foreach ($uoms as $uom)
                                                        <option value="{{ $uom->id }}"
                                                            {{ $uom->id == isset($products->purchaseUom->id) ? 'selected' : '' }}>
                                                            {{ $uom->uom_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div><!-- end .col-md-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="purchase_price" class="form-label">Purchase Price</label>
                                                <input type="number" name="purchase_price"
                                                       value="{{$products->purchase_price}}" class="form-control">
                                            </div>
                                        </div><!-- end .col-md-4 -->

                                        <div class="col-md-4">
                                            <div class="form-group fg-mb-0">
                                                <label class="checkbox-inline pr-2">
                                                    <input name="is_returnable" type="checkbox" value="1" {{ $products->is_returnable == 1 ? "checked" : "" }}> IsReturnable
                                                </label>
                                                <label class="checkbox-inline pr-2">
                                                    <input name="is_emergency" type="checkbox" value="1" {{ $products->is_emergency == 1 ? "checked" : "" }}> IsEmergency
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input name="is_exchangeable" type="checkbox" value="1" {{ $products->is_exchangeable == 1 ? "checked" : "" }}> IsExchangeable
                                                </label>
                                            </div>
                                        </div>
                                    </div><!-- end .row -->
                                    <hr>

                                    <div class="row text-right">
                                        <div class="col-md-12">
                                            <div id="gInfo">
                                                <a class="btn index-btn info-next-button"
                                                   id="info-next-button">Next</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-general-info -->

                        <div class="tab-pane fade" id="nav-variation" role="tabpanel"
                             aria-labelledby="nav-variation-tab">
                            <div class="row tile mr-0 ml-0">
                                <div class="col-md-12">
                                    <!-- row for input field -->
                                    <div class="row pt-4" style="">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @include('product::shares.variant',['field' => 'product_variant_id[]'])
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="variant_property_id[]" class="forget-form">Variation
                                                    Values</label>
                                                <select class="form-control single-select" id="variant_property"
                                                        onchange="varSpecAddValidation()">
                                                    <option value="">Select Variant Property</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="add-btn-area">
                                                <a href="#" class="btn btn-outline-primary w-100" id="add_item">Add</a>
                                            </div>
                                        </div>
                                    </div><!-- end .row for input fields -->

                                    <!-- row for table -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- generated table area -->
                                            <div class="adding-items-area" id="nav_generated_data_table">
                                                <table
                                                    class="table table-striped table-bordered table-hover bg-white table-sm">
                                                    <thead>
                                                    <tr class="">
                                                        <th width="47.5%">Variant Name</th>
                                                        <th width="47.5%">Variant Property</th>
                                                        <th width="3%">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="item_add_area">
                                                    {{-- @foreach ($products->productVariations as $key => $variations)
                                                        <tr>
                                                            <td>
                                                                @if (isset($variations->variant[0]))
                                                                    <input class="form-control"
                                                                        value="{{ $variations->variant[0]->name }}"
                                                                        readonly>
                                                                    <input type="hidden"
                                                                        class="form-control product_type"
                                                                        name="variant_id[]" id="ag_vs_var_id_up"
                                                                        value="{{ $variations->variant[0]->id }}">
                                                                @endif
                                                            </td>

                                                            <td>
                                                                @if (isset($variations->variantProperty[0]))
                                                                    <input class="form-control"
                                                                        value="{{ $variations->variantProperty[0]->name }}"
                                                                        readonly>
                                                                    <input type="hidden"
                                                                        class="form-control product_type"
                                                                        name="variant_property_id[]"
                                                                        id="ag_vs_var_ppt_up"
                                                                        value="{{ $variations->variantProperty[0]->id }}">
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <a href="#"
                                                                    class="btn btn btn-outline-danger item_remove btn-xs">x</a>
                                                            </td>

                                                        </tr>
                                                    @endforeach --}}
                                                    @foreach ($products->productVariations as $key => $variations)
                                                        <tr>
                                                            <td>
                                                                @if (isset($variations->variant))
                                                                    <input class="form-control"
                                                                           value="{{ $variations->variant->name }}"
                                                                           readonly>
                                                                    <input type="hidden" name="variant_id[]"
                                                                           value="{{ $variations->variant->id }}">
                                                                @endif
                                                            </td>

                                                            <td>
                                                                @if (isset($variations->variantProperty))
                                                                    <input class="form-control"
                                                                           value="{{ $variations->variantProperty->name }}"
                                                                           readonly>
                                                                    <input type="hidden" name="variant_property_id[]"
                                                                           value="{{ $variations->variantProperty->id }}">
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <a href="#"
                                                                   class="btn btn btn-outline-danger item_remove btn-xs">x</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div><!-- end .col-md-12 -->
                                    </div><!-- end .row -->


                                    <div class="form-group mb-5" style="margin-top: 30px">
                                        <label for="specification_text_area" class="forget-form"
                                               style="font-size: 20px">
                                            Specification
                                        </label>
                                        @if (isset($products->productDetails))
                                            <textarea class="ckeditor form-control"
                                                      name="specifications">{{ $products->productDetails->specifications }}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </textarea>
                                        @endif
                                    </div>
                                    <div class="form-group" id="vSpec">
                                        <a id="variation-next-button"
                                           class="btn create-btn variation-next-button next-button">Next</a>
                                    </div>
                                </div> <!-- end .col-md-12 -->
                            </div> <!-- end .row -->
                        </div> <!-- end nav-variation -->

                        <div class="tab-pane fade" id="nav-box-pack" role="tabpanel" aria-labelledby="nav-box-pack-tab">
                            <div class="row tile mr-0 ml-0 pt-5">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="uom" class="form-label">UOM <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control single-select" name="uom" id="uom"
                                                        onchange="bpAddValidation()">
                                                    <option value="">Select UOM</option>
                                                    @foreach ($uoms as $uom)
                                                        <option value="{{ $uom->id }}">
                                                            {{ $uom->uom_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="quantity_per_uom" class="form-label">Quantity Per
                                                    UOM <span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="quantity_puom"
                                                       name="quantity_per_uom" autocomplete="off"
                                                       onkeyup="bpAddValidation()"
                                                       placeholder="Enter quantity per uom">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <a class="btn btn-outline-primary add_box_pack_row_button w-100"
                                               id="add_box_pack_row_btn">Add</a>
                                        </div>
                                    </div><!-- end .row -->

                                    <!-- generated table area -->
                                    <div class="adding-items-area" id="pac_generated_data_table">
                                        <table class="table table-striped table-bordered bg-white table-hover table-sm">
                                            <thead>
                                            <tr class="">
                                                <th style="width: 47.5%;">UOM</th>
                                                <th style="width: 47.5%;">Quantity Per UOM</th>
                                                <th width="5%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody class="item_add_area_pb">
                                            @foreach ($products->Productuom as $key => $product_uom)
                                                <tr>
                                                    <td>
                                                        @if (isset($product_uom->uom))
                                                            <input class="form-control"
                                                                   value="{{ $product_uom->uom->uom_name }}" readonly>

                                                            <input type="hidden" name="uom_id[]"
                                                                   value="{{ $product_uom->uom->id }}">
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if (isset($product_uom->quantity_per_uom))
                                                            <input class="form-control"
                                                                   value="{{ $product_uom->quantity_per_uom }}"
                                                                   readonly>

                                                            <input type="hidden" name="quantity_per_uom[]"
                                                                   value="{{ $product_uom->quantity_per_uom }}">
                                                        @endif
                                                    </td>

                                                    <td><a href="#"
                                                           class="btn btn btn-outline-danger item_remove btn-xs">x</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table><!-- end generated-table-data -->
                                    </div>
                                    <hr>

                                    <div class="text-right" id="bPack">
                                        <a class="btn create-btn pack-next-button ">Next</a>
                                    </div>
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-box-pack -->

                        <div class="tab-pane fade" id="nav-inventory-setup" role="tabpanel"
                             aria-labelledby="nav-inventory-setup-tab">
                            <div class="row tile mr-0 ml-0 pt-5">
                                <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="inventory_type" class="forget-form">Inventory
                                                    Type</label>
                                                <select class="form-control single-select" id="inventory_type"
                                                        name="inventory_type" onchange="invAddValidation()">
                                                    <option value="">Select Type</option>
                                                    <option value="1"
                                                        {{ $products->inventory_type == 1 ? 'selected' : '' }}>
                                                        Work-In-Process
                                                    </option>
                                                    <option value="2"
                                                        {{ $products->inventory_type == 2 ? 'selected' : '' }}>Cycle
                                                        Stock
                                                    </option>
                                                    <option value="3"
                                                        {{ $products->inventory_type == 3 ? 'selected' : '' }}>Pipeline
                                                        Stock
                                                    </option>
                                                    <option value="4"
                                                        {{ $products->inventory_type == 4 ? 'selected' : '' }}>Hedge
                                                        Inventory
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="inventory_state" class="forget-form">Inventory
                                                    State</label>
                                                <select class="form-control single-select" id="inventory_state"
                                                        name="inventory_state" onchange="invAddValidation()">
                                                    <option value="">Select Inventory State</option>
                                                    <option value="1"
                                                        {{ $products->inventory_state == 1 ? 'selected' : '' }}>Normal
                                                    </option>
                                                    <option value="2"
                                                        {{ $products->inventory_state == 2 ? 'selected' : '' }}>Cold
                                                    </option>
                                                    <option value="3"
                                                        {{ $products->inventory_state == 3 ? 'selected' : '' }}>Cool
                                                    </option>
                                                    <option value="4"
                                                        {{ $products->inventory_state == 4 ? 'selected' : '' }}>Warm
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div id="iSet">
                                                <a class="btn index-btn inventory-next-button">Next</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-inventory-setup -->

                        <div class="tab-pane fade" id="nav-description" role="tabpanel"
                             aria-labelledby="nav-description-tab">
                            <div class="row tile ml-0" style="margin-right: 1px">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="dynamic_field_description" class="table c-mr-1">
                                                <thead>
                                                <tr>
                                                    <td class="border-0">
                                                        Featur Image
                                                    </td>

                                                    <td class="border-0">
                                                        Images
                                                    </td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td style="width: 50%">
                                                        <div class="form-group">
                                                            <input type="file" id="feature_image" class="form-contorl"
                                                                   name="feature_image">
                                                            <div id="thumb-output"></div>
                                                            <div class="stored-image p-3" id="fImage">
                                                                <img class="show-feature-image"
                                                                     src="{{ !empty($products->productDetails) ? asset('feature-image' . '/' . $products->productDetails->feature_image) : asset('img/default-image.jpg') }}"
                                                                     alt="features image">
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td style="width: 50%;">
                                                        <div class="form-group">
                                                            <input type="file" class="form-control" id="images"
                                                                   name="images[]" multiple>
                                                            <div id="images-output"></div>
                                                            <div class="stored-images d-inline-flex p-2" id="multiple_image">
                                                                @foreach (explode('|', $products->productDetails->images) as $image)
                                                                    <img class="show-images" src="{{ !empty($image) ? asset('images' . '/' . $image) : asset('img/default-image.jpg') }}" alt="images">
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tfoot>
                                            </table><!-- end #dynamic_field_description -->
                                        </div><!-- end .col-md-12 -->
                                        <hr>
                                        <div class="col-md-12">
                                            <!-- short-description -->
                                            <div class="form-group mb-5">
                                                <label for="short_description_text_area" class="forget-form"
                                                       style="font-size: 20px">Short Description
                                                </label>
                                                <textarea class="ckeditor form-control" name="short_description">
                                                                                                                                                                                @if (isset($products->productDetails->short_desc))
                                                        {{ $products->productDetails->short_desc }}
                                                    @endif
                                                                                                                                                                                    </textarea>
                                            </div><!-- end .form-group -->

                                            <!-- long description -->
                                            <div class="form-group mb-5">
                                                <label for="long_description_text_area" class="forget-form"
                                                       style="font-size: 20px">
                                                    Long Description
                                                </label>
                                                <textarea class="ckeditor form-control" name="long_description">
                                                                                                                                                                            @if (isset($products->productDetails->long_desc))
                                                        {{ $products->productDetails->long_desc }}
                                                    @endif
                                                                                                                                                                        </textarea>
                                            </div><!-- end .form-group -->
                                        </div> <!-- end .col-md-12 -->


                                        <div class="text-right ml-0 mr-0  col-md-12 text-right">
                                            <div id="submitButton">
                                                <button class="btn index-btn next-btn description-submit-button" type="submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </div><!-- end .col-md-12 -->
                                    </div> <!-- end .row -->
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-description -->
                    </div> <!-- end .tab-content -->
                </div> <!-- end .col-md-12 -->
            </div><!-- end .row -->
        </form><!-- end form -->
    </section>


@endsection

@push('post_scripts')

    {{-- <!-- Page specific javascripts--> --}}
    {{-- <!-- Data table plugin --> --}}
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function () {
            $(".multiple-select").select2({
                width: '100%'
            });
            $(".single-select").select2({
                width: '100%'
            });
        });
    </script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
    <!-- ck-editor-plugin -->
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

    <script>
        $(document).ready(function () {
            generalInfo();

            $('.ckeditor').ckeditor();
            $('#gInfo').hide();
            $('#info-next-button').hide();
            $("#add_item").hide();
            $('#vSpec').hide();
            $("#add_box_pack_row_btn").hide();
            $('#bPack').hide();
            // $('#add_new_pvariation_des').hide();
            $(".multiple-select").select2({
                width: '100%',
                selectedTop: false,
            });
            $(".single-select").select2({
                width: '100%'
            });
            varSpecAddValidation();
            bpAddValidation();
            invAddValidation();
        });
    </script>

    <!-- all show hide functions -->
    <script>
        function generalInfo() {
            let product_name = $("#product_name").val().length;
            let product_type = $("#product_type option:selected").val();
            let product_category = $("#pcategory option:selected").val();
            let product_sub_category = $("#sub_cat option:selected").val();

            if (product_name >= 2 && product_type > 0 && product_category > 0 && product_sub_category > 0) {
                // $("#gInfo").show();
                $('#info-next-button').show();
            } else {
                // $("#gInfo").hide();
                $('#info-next-button').hide();
            }
        }

        function varSpecValidation() {
            if ($("#ag_vs_sub_cat_up").val() > 0 && $("#ag_vs_var_id_up").val() > 0 && $("#ag_vs_var_ppt_up").val() > 0 &&
                $("#ag_vs_un_up").val() > 0) {
                $("#vSpec").show();
            } else {
                $("#vSpec").hide();
            }
        }

        function varSpecValidation2() {
            let auto_variant_name = $("#ag_var_name").val();
            let auto_variant_property = $("#ag_var_pro").val();

            if (auto_variant_name > 0 && auto_variant_property > 0) {
                $("#vSpec").show();
            } else {
                $("#vSpec").hide();
            }
        }

        function desSub() {
            if ($("#product_image").val().length > 1) {
                $("#submitButton").show()
            } else {
                $("#submitButton").hide();
            }
        }
    </script>

    <!-- validation for adding new row -->
    <script>
        function varSpecAddValidation() {
            let variant_name = $("#variant option:selected").val();
            let variant_property = $("#variant_property option:selected").val();

            if (variant_name > 0 && variant_property > 0) {
                $("#add_item").show();
            } else {
                $("#add_item").hide();
            }
        }

        function bpAddValidation() {
            if ($("#uom option:selected").val() > 0 && $("#quantity_puom").val() > 0) {
                $("#add_box_pack_row_btn").show();
            } else {
                $("#add_box_pack_row_btn").hide();
            }
        }

        function invAddValidation() {
            if ($("#product_variation_inventory").text().length > 0 &&
                $("#product_variation_inventory").val() != "" &&
                $("#inventory_type").val() > 0 &&
                $("#inventory_state").val() > 0) {
                $("#add_inventory_row_button").show();
            } else {
                $("#add_inventory_row_button").hide();
            }
        }

        function desRowAddValidation() {
            console.log('des calling');
            if ($("#product_variation_description").text().length > 0 && $("#product_variation_description").val() != "") {
                $("#add_new_pvariation_des").show();
            } else {
                $("#add_new_pvariation_des").hide();
            }
        }
    </script>

    <!-- single image preview -->
    <script>
        $(document).ready(function () {
            $('#feature_image').on('change', function () { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {

                    var data = $(this)[0].files; //this file data

                    $.each(data, function (index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                            .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function (file) { //trigger function on successful read
                                return function (e) {
                                    var img = $('<img/>').addClass('thumb').attr('src',
                                        e.target.result); //create image element
                                    $('#thumb-output').append(
                                        img); //append image to output element
                                    $("#fImage").fadeOut();
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });

                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>

    <!-- multiple image preview -->
    <script>
        $(document).ready(function () {
            $('#images').on('change', function () { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {

                    var data = $(this)[0].files; //this file data

                    $.each(data, function (index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                            .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function (file) { //trigger function on successful read
                                return function (e) {
                                    var img = $('<img/>').addClass('thumb2').attr('src',
                                        e.target.result); //create image element
                                    $('#images-output').append(
                                        img); //append image to output element
                                    $("#multiple_image").delay(2000).fadeOut();
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });

                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>

    <script>
        // value for check
        let brand_name_value = $("#brand_name").val();
        let product_type_value = $("#product_type option:selected").val();
        let product_category_value = $("#pcategory option:selected").val();
        let manufacturer_value = $(".manufacturer option:selected").val();
        let product_generic_value = $("#product_generic_id option:selected").val();
    </script>

    <!-- next-button & previous-button -->
    <script>
        var generalInfoTab = $("a#nav-general-info-tab");
        var variationSetupTab = $("a#nav-variation-tab");
        var boxPackTab = $("a#nav-box-pack-tab");
        var inventorySetupTab = $("a#nav-inventory-setup-tab");
        var descriptionTab = $("a#nav-description-tab");

        var generalInfoTabContent = $("#nav-general-info");
        var variationSetupTabContent = $("#nav-variation");
        var boxPackTabContent = $("#nav-box-pack");
        var inventorySetupTabContent = $("#nav-inventory-setup");
        var descriptionTabContent = $("#nav-description");

        // goto "general-info" to "brand & manufacturer"
        $(".info-next-button").click(function () {

            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.addClass(['active', 'show']).attr('aria-labelledby', 'nav-variation-tab');

            varSpecAddValidation();
        });

        // goto "variation-setup" to "pack size"
        $(".variation-next-button").click(function () {

            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.addClass(['active', 'show']).attr('aria-labelledby', 'nav-box-pack-tab');

            bpAddValidation();
        });

        // goto "pack size" to "inventory-setup"
        $(".pack-next-button").click(function () {

            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.addClass(['active', 'show']).attr('aria-labelledby',
                'nav-inventory-setup-tab');

            invAddValidation();
        });

        // goto "inventory-setup" to "description"
        $(".inventory-next-button").click(function () {

            generalInfoTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            variationSetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            boxPackTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            inventorySetupTab.removeClass(['active', 'show']).attr('aria-selected', 'false');
            descriptionTab.addClass(['active', 'show']).attr('aria-selected', 'true');

            generalInfoTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            variationSetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            boxPackTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            inventorySetupTabContent.removeClass(['active', 'show']).attr('aria-labelledby', '');
            descriptionTabContent.addClass(['active', 'show']).attr('aria-labelledby', 'nav-description-tab');

            // desRowAddValidation();
        });
    </script>

    <!-- jquery validate js validation -->
    <script>
        $(document).ready(function () {
            $("#product_submit_form").validate({
                rules: {
                    brand_name: {
                        required: true,
                        minlength: 2
                    },
                },
                messages: {
                    brand_name: {
                        required: "Please enter a brand name",
                        minLength: "Please enter at least 2 character"
                    }
                }
            });
        });
    </script>

    <!-- create new row for variation & spacification -->
    <!-- catch selected option and add items on table -->
    <script>
        // Item Add function goes here
        $(document).on('click', '#add_item', function () {
            let variant_val = $("#variant option:selected").val();
            var variant_name = $("#variant option:selected").text();
            let variant_property_val = $("#variant_property option:selected").val();
            var variant_property_name = $("#variant_property option:selected").text();

            $('.item_add_area').append(
                `<tr>
                    <td>
                        <input class="form-control" value="${variant_name}" readonly>
                        <input type="hidden" class="form-control" name="variant_id[]" id="ag_var_name" value="${variant_val}">
                    </td>

                    <td>
                        <input class="form-control" value="${variant_property_name}" readonly>
                        <input type="hidden" class="form-control" name="variant_property_id[]" id="ag_var_pro" value="${variant_property_val}">
                    </td>

                    <td>
                        <a href="#" class="btn btn btn-outline-danger item_remove btn-xs w-100">X</a>
                    </td>
                </tr>`);

            // check next button validation
            if ($("#ag_var_name").val() > 0) {
                $("#variation-next-button").show();
            } else {
                $("#variation-next-button").hide();
            }

            $('#variant').select2("val", " ");
            $('#variant').select2({
                placeholder: "Select Variant"
            });

            $("#variant_property").select2("val", " ");
            $('#variant_property').select2({
                placeholder: "Select Variant Property"
            });
        });

        // Remove Item
        $(document).on('click', '.item_remove', function () {
            $(this).parent().parent().remove();

            // check next button validation
            if ($("#ag_var_name").val() > 0) {
                $("#variation-next-button").show();
            } else {
                $("#variation-next-button").hide();
            }
        });
    </script>

    <!-- add new box and pack for uom -->
    <script>
        // Item Add function goes here
        $(document).on('click', '#add_box_pack_row_btn', function () {

            let uom_id_val = $("#uom option:selected").val();
            let uom_id_text = $("#uom option:selected").text();
            let quantity_per_uom_val = $("#quantity_puom").val();

            $('.item_add_area_pb').append(
                `<tr>
                    <td>
                        <input class="form-control" value="${uom_id_text}" readonly>
                        <input type="hidden" class="form-control" name="uom_id[]" id="selected_uom_val_id_ag" value="${uom_id_val}">
                    </td>

                    <td>
                        <input class="form-control" value="${quantity_per_uom_val}" readonly>
                        <input type="hidden" class="form-control" name="quantity_per_uom[]" value="${quantity_per_uom_val}">
                    </td>

                    <td><a href="#" class="btn btn btn-outline-danger item_remove w btn-xs">x</a></td>
                </tr>`);

            // check next button validation
            if ($("#selected_uom_val_id_ag").val() > 0) {
                $(".pack-next-button").show();
            } else {
                $(".pack-next-button").hide();
            }

            // calling boxPack function for validation based upon auto generated table data
            $('#bPack').show();

            $('#uom').select2("val", " ");
            $('#uom').select2({
                placeholder: "Select UOM"
            });

            $("#quantity_puom").val('').change();

        });

        // Remove Item
        $(document).on('click', '.item_remove', function () {
            $(this).parent().parent().remove();

            // check next button validation
            if ($("#selected_uom_val_id_ag").val() > 0) {
                $(".pack-next-button").show();
            } else {
                $(".pack-next-button").hide();
            }
        });
    </script>

    <script>
        function productName() {
            let product_name = $("#product_name").val();
            localStorage.setItem('brand_name', product_name);
            // $("#pcategory").trigger('change');
        }
    </script>

    {{-- dependency script --}}
    <script>
        $(document).ready(function () {
            $('#product_type').on('change', function () {
                let id = $(this).val();
                $('#pcategory').empty();
                $('#pcategory').append(`<option value="" selected></option>`);


                var url = "{{ url('product/get-product-category') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function (response) {
                        var response = JSON.parse(response);

                        $('#pcategory').empty();
                        $('#pcategory').append(
                            `<option value="" selected>Select Product Category*</option>`);

                        response.forEach(element => {
                            $('#pcategory').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    }
                });


                // for product generic
                $('#product_generic_id').empty();
                $('#product_generic_id').append(`<option value="" selected></option>`);

                var url = "{{ url('product/get-product-generic') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log("Product Generic Ajax Calling");
                        console.log(response)
                        $('#product_generic_id').empty();
                        $('#product_generic_id').append(
                            `<option value="" selected>Select Product Generic</option>`);

                        response.forEach(element => {
                            $('#product_generic_id').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    },
                    error: function (error) {
                        console.log("Product Generic Error: ")
                        console.log(error);
                    }
                });

                // for product variant
                $('#variant').empty();
                $('#variant').append(`<option value="" selected></option>`);

                var url = "{{ url('product/get-product-variant') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log("Product Variation Ajax Calling");
                        console.log(response)
                        $('#variant').empty();
                        $('#variant').append(
                            `<option value="" selected>Select Product Variation</option>`);

                        response.forEach(element => {
                            $('#variant').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    },
                    error: function (error) {
                        console.log("Product Variation Error: ")
                        console.log(error);
                    }
                });
            });
        });
    </script>

    <!-- calling sub-category -->
    <script>
        $(document).ready(function () {
            $('#pcategory').on('change', function () {
                let id = $(this).val();
                $('#sub_cat').empty();
                $('#sub_cat').append(`<option value="0" selected>Processing...</option>`);

                $('#product_variation').empty();
                $('#product_variation').append(`<option value="" selected>Processing...</option>`);

                $('#product_variation_inventory').empty();
                $('#product_variation_inventory').append(
                    `<option value="" selected>Processing...</option>`);

                $('#product_variation_description').empty();
                $('#product_variation_description').append(
                    `<option value="" selected>Processing...</option>`);
                var url = "{{ url('product/get-product-sub-category') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function (response) {
                        var response = JSON.parse(response);
                        let bname = localStorage.getItem('brand_name');
                        $('#sub_cat').empty();
                        $('#sub_cat').append(
                            `<option value="0" selected>Select Product Sub Category</option>`
                        );

                        $('#product_variation').empty();
                        $('#product_variation').append(
                            `<option value="" selected>Select Product Variation</option>`);

                        $('#product_variation_inventory').empty();
                        $('#product_variation_inventory').append(
                            `<option value="" selected>Select Product Variation</option>`);

                        $('#product_variation_description').empty();
                        $('#product_variation_description').append(
                            `<option value="">Select Product Variation</option>`);
                        response.forEach(element => {
                            $('#sub_cat').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                            $('#product_variation').append(
                                `<option value="${element['id']}">${bname}</option>`
                            );
                            $('#product_variation_inventory').append(
                                `<option value="${element['id']}">${bname}</option>`
                            );
                            $('#product_variation_description').append(
                                `<option value="${element['id']}">${bname}</option>`
                            );
                        });
                    }
                });
            });
        });
    </script>

    <!-- calling variant -->
    <script>
        $(document).ready(function () {
            $('#variant').on('change', function () {
                let id = $(this).val();
                $('#variant_property').empty();
                $('#variant_property').append(`<option value="0" disabled selected>Processing...</option>`);
                var url = "{{ url('product/get-variant-property') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function (response) {
                        var response = JSON.parse(response);
                        $('#variant_property').empty();
                        $('#variant_property').append(
                            `<option value="0" disabled selected>Select Variant Property</option>`
                        );
                        response.forEach(element => {
                            $('#variant_property').append(
                                `<option value="${element['id']}">${element['name']}</option>`
                            );
                        });
                    }
                });
            });
        });
    </script>

    <!-- catch variation data and save into dummy table -->
    <script>
        $(document).ready(function () {
            $("#variation-next-button").on('click', function (e) {
                e.preventDefault();

                const sub_category_id = [];
                const product_name_ag = [];
                /*const variant_id = [];
                const variant_property_id = [];*/
                const unit_id = [];

                $('input[name^="sub_category_id"]').each(function () {
                    sub_category_id.push($(this).val());
                });

                $('input[name^="product_name_ag"]').each(function () {
                    product_name_ag.push($(this).val());
                });

                $('input[name^="variant_id"]').each(function () {
                    variant_id.push($(this).val());
                });
                $('input[name^="variant_property_id"]').each(function () {
                    variant_property_id.push($(this).val());
                });
                $('input[name^="unit_id"]').each(function () {
                    unit_id.push($(this).val());
                });
                $.ajax({
                    url: "{{ route('save_temp_data') }}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        sub_category_id: sub_category_id,
                        product_name: product_name_ag,
                        /*variant_id: variant_id,
                        variant_property_id: variant_property_id,*/
                        unit_id: unit_id,
                    },
                    success: function (res) {
                    },
                });

            })
        });
    </script>

    <script>
        $(document).ready(function () {
            $("#variation-next-button").on('click', function (e) {
                e.preventDefault();
                var url = "{{ url('product/get-temp-data') }}";
                var dltUrl = url;
                $.ajax({
                    url: dltUrl,
                    type: "GET",
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log(response);
                        $('#product_variation').empty();
                        $('#product_variation').append(
                            `<option value="">Select Product Variation Name</option>`);

                        /*$('#variantsid').empty();
                        $('#variantsid').append(
                            `<option value="">Select Variant Name</option>`);

                        $('#vproperties').empty();
                        $('#vproperties').append(
                            `<option value="">Select Variant Name</option>`);*/

                        $('#product_variation_inventory').empty();
                        $('#product_variation_inventory').append(
                            `<option value="">Select Product Variation Name</option>`);

                        $('#product_variation_description').empty();
                        $('#product_variation_description').append(
                            `<option value="">Select Product Variation Name</option>`);
                        response.forEach(element => {
                            $('#product_variation').append(
                                `<option value="${element.product_name}">${element.product_name}</option>`
                            );
                            /*$('#variantsid').append(
                                `<option value="${element.variant['id']}">${element.variant['name']}</option>`
                            );
                            $('#vproperties').append(
                                `<option value="${element.variant_values['id']}">${element.variant_values['name']}</option>`
                            );*/
                            $('#product_variation_inventory').append(
                                `<option value="${element.product_name}">${element.product_name}</option>`
                            );
                            $('#product_variation_description').append(
                                `<option value="${element.product_name}">${element.product_name}</option>`
                            );
                        });

                    },
                });

            })
        });
    </script>
    {{-- end dependency script --}}

    <!-- other scripts -->
    <script>
        $(document).ready(function () {
            // remove product variation on button click on description tab
            $(".remove_pv_edit").click(function () {
                $(this).parent().remove();
            })
        });
    </script>

@endpush
