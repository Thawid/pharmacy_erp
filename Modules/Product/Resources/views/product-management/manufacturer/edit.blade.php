@extends('dboard.index')
@section('title', 'Update Manufacturer')
@section('dboard_content')


    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title-area -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Update Manufacturer</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('manufacturers.index') }}">Back </a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('manufacturers.update', $manufacturer->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="manufacturer_name" class="forget-form">Manufacturer Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $manufacturer->name }}">
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status" class="forget-form">Status</label>
                                    <select class="single-select form-control" name="status">
                                        <option>Select Status</option>
                                        <option value="1" {{ $manufacturer->status == 1 ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ $manufacturer->status == 0 ? 'selected' : '' }}>InActive</option>
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection


@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function() {
            $(".single-select").select2({
                width: '100%'
            });
        });
    </script>
@endpush
