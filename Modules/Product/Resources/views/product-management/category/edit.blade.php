@extends('dboard.index')
@section('title', 'Update Category')
@section('dboard_content')

    <!-- title -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-9">
                            <h1 class="title-heading">Update Category</h1>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn index-btn" href="{{ route('category.index') }}">Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <form action="{{ route('category.update', $product_category->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="product_type" class="forget-form">Product Type*</label>
                                        <select class="js-example-basic-single form-control" name="product_type_id">
                                            @foreach ($product_types as $type)
                                                <option value="{{ $type->id }}"
                                                    {{ $type->id == $product_category->product_type_id ? 'selected' : '' }}>
                                                    {{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                    </div><!-- end .form-group -->
                
                                    <div class="form-group ">
                                        <label for="category_name" class="forget-form">Name*</label>
                                        <input type="text" class="form-control" name="name" value="{{ $product_category->name }}">
                                    </div><!-- end .category-name -->
                            </div><!-- end .col-md-6 -->
                
                            <div class="col-md-6">
                                    <div class="d-md-flex justify-content-between">
                                        <div class="form-group d-md-inline-flex mr-2" style="margin-top: 35px">
                                            <input class="" type="checkbox" value="1" id="have_parent" name="have_parent"
                                                style="width: 20px; height: 20px" {{ $product_category->parent_id > 0 ? 'checked' : '' }}>
                                            <label class="form-check-label" for="have_parent" style="margin-top: -5px; margin-left: 10px">
                                                Have Parent Category
                                            </label>
                                        </div><!-- end .form-group -->
                
                                        <div class="form-group w-50 have_parent_show">
                                            <label class="forget-form">Product Category*</label>
                                            <select class="js-example-basic-single form-control" name="parent_id">
                                                <option value="NULL" selected disabled>Select Product Category</option>
                                                @if ($product_category->parent_id)
                                                    @foreach ($product_categories as $category)
                                                        <option value="{{ $category->id }}"
                                                            {{ $category->id == $product_category->parent_id ? 'selected' : '' }}>
                                                            {{ $category->name }}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($product_categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div><!-- end parent category -->
                                    </div>
                
                                    <div class="form-group mt-2">
                                        <label for="status" class="forget-form">Status</label>
                                        <select class="js-example-basic-single form-control" name="status">
                                            <option value="1" {{ $product_category->status == 1 ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ $product_category->status == 0 ? 'selected' : '' }}>InActive</option>
                                        </select>
                
                                    </div><!-- end parent category -->
                            </div><!-- end .col-md-6 -->
                        </div><!-- end .row -->
                        <hr>
                
                        <!-- buttons-area -->
                        <div class="row text-right">
                            <div class="col-md-12">
                                    <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end from -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection

@push('post_scripts')

<script>
    function generalInfo(){
        console.log('generalInfo is calling...');
    }
</script>

    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
            haveParentShow();
        });

        function haveParentShow() {
            var have_parent = $('[name="have_parent"]:checked').val();

            if (have_parent == 1) {
                $('.have_parent_show').fadeIn();
            } else {
                $('.have_parent_show').fadeOut();
            }

        }

        $('[name="have_parent"]').change(function() {
            haveParentShow();
        });
    </script>


@endpush
