@extends('dboard.index')
@section('title', 'Create Category')
@section('dboard_content')

    <form action="{{ route('category.store') }}" method="POST">
        
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <!-- title -->
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-6">
                                <h1 class="title-heading">Create Category</h1>
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-6 text-right">
                                <a class="btn index-btn" href="{{ route('category.index') }}">Back</a>
                            </div><!-- end .col-md-6 -->
                        </div><!-- end. row -->
                        <hr>

                        <!-- data -->
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        @include('product::shares.product-types')
                                    </div><!-- end .form-group -->
                
                                    <div class="form-group ">
                                        <label for="category_name" class="forget-form">Name<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Type product category name">
                                    </div><!-- end .category-name -->
                            </div><!-- end .col-md-6 -->
                
                            <div class="col-md-6">
                                    <div class="d-md-flex justify-content-between">
                                        <div class="form-group d-md-inline-flex mr-2" style="margin-top: 35px">
                                            <input class="" type="checkbox" value="1" id="have_parent" name="have_parent"
                                                style="width: 20px; height: 20px" {{ old('have_parent') == '1' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="have_parent" style="margin-top: -5px; margin-left: 10px">
                                                Have Parent Category ?
                                            </label>
                                        </div><!-- end .form-group -->
                
                                        <div class="form-group w-50 have_parent_show align-items-end">
                                            @include('product::shares.product-categories')
                                        </div><!-- end parent category -->
                                    </div>
                
                                    <div class="form-group mt-2">
                                        @include('product::shares.status')
                                    </div><!-- end parent category -->
                            </div><!-- end .col-md-6 -->
                        </div><!-- end .row -->
                        <hr>

                        <!-- button -->
                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Submit</button>
                            </div>
                        </div> 
                    </div><!-- end .tile-body -->
                </div><!-- tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form><!-- end from -->
@endsection

@push('post_scripts')
<script>
    function generalInfo(){
        console.log('generalInfo is calling...');
    }
</script>

    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                width: '100%'
            });
            haveParentShow();
        });

        function haveParentShow() {
            var have_parent = $('[name="have_parent"]:checked').val();

            if (have_parent == 1) {
                $('.have_parent_show').fadeIn();
            } else {
                $('.have_parent_show').fadeOut();
            }

        }

        $('[name="have_parent"]').change(function() {
            haveParentShow();
        });
    </script>


@endpush
