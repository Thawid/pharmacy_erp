@extends('dboard.index')

@section('title', 'Store Product & Stock Alert')

@section('dboard_content')

<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-12">
            <h2 class="title-heading">Products List and Set Stock Alert</h2>
          </div><!-- end col-md-6 -->
        </div><!-- end .row -->
        <hr>
        <form action="{{ route('setup.stock.alert.save') }}" method="POST">
          @csrf
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-hover bg-white table-sm" id="bootstrapDataTable">
                <thead>
                  <tr class="">
                    <th width=" 3%">
                      <input type="checkbox" id="select-all" name="select-all">
                    </th>
                    <th width="3%">ID</th>
                    <th width="50%">Product Name</th>
                    <th width="44%">Stock Alert Min Qty<span class="text-danger">*</span></th>
                  </tr>
                </thead>
                <tbody class="item_add_area_bundle_product" id="auto_generated_tr">
                  @if (isset($products))
                  @foreach ($products as $key => $product)
                  <tr>
                    <td><input type="checkbox" id="" name="product_id[]" value="{{ $product->id }}">
                      <input type="hidden" name="store_id" value="{{ $stores->id }}">
                      <input type="hidden" name="unit_id" value="{{ $product->unit_id }}">
                      <input type="hidden" name="purchase_uom_id" value="{{ $product->purchase_uom_id }}">
                    </td>
                    <td>{{ $key + 1 }}</td>
                    <td>
                      <div>
                        <h3> {{ $product->product_name }} <input type="hidden" name="product_name[]" value="{{ $product->product_name }}">
                        </h3>
                      </div>
                    </td>

                    <td>
                      <input type="number" class="form-control" name="qty[]" placeholder="Write min qty">
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!-- end.col-md-12 -->
          </div><!-- end.row -->
          <hr>

          <div class="submit-button text-right">
            <button class="btn index-btn text-right" type="submit">Active</button>
          </div>
        </form>
      </div><!-- end .title-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->
@endsection

@push('post_scripts')
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  // bootstrap data table configuration
  $('#bootstrapDataTable').DataTable();
</script>
<script>
  $(document).ready(function() {
    // select2 configuration
    $('.single-select').select2({
      width: '100%'
    });
  });
</script>

<script>
  $('#select-all').click(function(event) {
    if (this.checked) {
      // Iterate each checkbox
      $(':checkbox').each(function() {
        this.checked = true;
      });
    } else {
      $(':checkbox').each(function() {
        this.checked = false;
      });
    }
  });

  $('#select-all-a').click(function(event) {
    if (this.checked) {
      // Iterate each checkbox
      $("input.modal-check:checkbox").each(function() {
        this.checked = true;
      });
    } else {
      $("input.modal-check:checkbox").each(function() {
        this.checked = false;
      });
    }
  });
</script>
@endpush