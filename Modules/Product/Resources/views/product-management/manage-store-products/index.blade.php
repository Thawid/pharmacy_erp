@extends('dboard.index')

@section('title', 'Manage Store Products')

@push('styles')


<style>
  i {
    font-size: 18px;
  }

  #submit_button>button {
    margin-top: 28px;
  }
</style>
@endpush

@section('dboard_content')

<div class="tile">
  <div class="tile-body">

    <!-- title area -->
    <div class="row align-items-center">
      <div class="col-md-6">
        <h2 class="title-heading">Manage Store Products</h2>
      </div>
      <div class="col-md-6 text-right">
        {{-- --}}{{-- <a class="btn btn-outline-primary icon-btn" href="{{ route('products.create') }}"><i--}} {{--                            class="pr-2 fas fa-plus-circle"></i>Add Store Product</a>--}} </div>
      </div><!-- end .row -->
      <hr>

      <form action="{{ route('manage-store-product.create') }}" method="GET">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-4">
                <div class="from-group">
                  <label for="status" class="forget-form">Select Type</label>
                  <select name="store_type_name" id="" class="form-control">
                    <option value="" disabled selected> Select Type</option>
                    <option value="store">Store</option>
                    <option value="hub">Hub</option>
                  </select>
                </div>
              </div><!-- end store type -->
              <div class="col-md-4">
                <div class="from-group">
                  @include('store::shares.store')
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <div id="submit_button">
                    <button class="btn index-btn">Search</button>
                  </div>
                </div>
              </div><!-- end submit button -->
            </div>
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
      </form><!-- form end -->
      <hr>

      <!-- start .manage-store-product table -->
      <div class="row manage-store-product">
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover bg-white table-sm" id="bootstrapDataTable">
            <thead>
              <tr class="">
                <th width="10%">ID</th>
                <th width="50%">Store Name</th>
                <th width="10%">Status</th>
                <th width="8%">Action</th>
              </tr>
            </thead>
            <tbody class="item_add_area_bundle_product" id="auto_generated_tr">
              @if(isset($stores))
              @foreach($stores as $key => $store)
              <tr>
                <td>{{ $key + 1 }}</td>
                <td>
                  <div>
                    <p><strong>{{ $store->name }}</strong></h3>
                    <p><strong>ID: </strong>{{ $store->code }}</p>
                    <p><strong>Address: </strong>{{ $store->address }}</p>
                  </div>
                </td>
                <td>
                  <span class="badge badge-success m-1 p-2">Active</span>
                </td>
                <td>
                  <div class="d-flex justify-content-around align-content-center">
                    <a href="{{ route('active.products',$store->id) }}" class="btn details-btn" title="Show Active Product">
                      <i class="fa fa-lg fa-eye"></i> 
                    </a>

                    <a href="{{route('manage-store-product.edit', $store->id )}}" class="btn edit-btn" title="Add Active Product">
                    <i class="fa fa-plus" aria-hidden="true"></i></a>
                  </div>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div><!-- end .manage-store-product table -->
    </div><!-- end .tile-body -->
  </div><!-- end .tile -->

  <!-- content-area -->

  <!-- form start -->
  {{-- <form action="#">--}}



  @endsection

  @push('post_scripts')
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript">
    // bootstrap data table configuration
    $('#bootstrapDataTable').DataTable();
  </script>
  <script>
    $(document).ready(function() {
      // select2 configuration
      $('.single-select').select2({
        width: '100%'
      });
    });
  </script>
  @endpush