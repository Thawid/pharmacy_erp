@extends('dboard.index')

@section('title', 'Product Report')

@section('dboard_content')

    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Active Product List</h2>
                        </div><!-- end col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('manage-store-product.index') }}">Back</a>
                        </div><!-- ene .col-md-6 -->
                    </div><!-- end .row -->
                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Product Name</th>
                                            <th>Product Image</th>
                                        </tr><!-- end tr -->
                                    </thead><!-- end thead -->
        
                                    <tbody>
                                        @if (isset($products))
                                            @foreach ($products as $key => $product)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $product->product_name }}</td>
                                                    <td>
                                                        <img src="{{ asset('feature-image' . '/' . $product->productDetails->feature_image) }}"
                                                            alt="Product images" height="60px" width="60px">
                                                    </td>
                                                    {{-- <td class="d-md-flex">
                                                    <a href="{{ route('products.edit', $product->id) }}"
                                                       class="btn btn-outline-primary mr-3">
                                                        <i class="fa fa-lg fa-edit"></i>Edit</a>
        
                                                    <a href="{{ route('products.show', $product->id) }}"
                                                       class="btn btn-outline-primary mr-3">
                                                        <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"></i>Show</a>
        
                                                </td> --}}
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody><!-- end tbody -->
                                </table><!-- end table -->
                            </div><!-- end .table-responsive -->
                        </div>
                    </div>
                </div><!-- end .title-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
@endpush
