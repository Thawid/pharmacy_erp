@extends('dboard.index')

@section('title', 'Manage Store Product')

@push('styles')


    <style>
        i {
            font-size: 18px;
        }

        #submit_button>button {
            margin-top: 28px;
        }

        .reset_btn {
            margin-top: 28px;
            margin-left: 10px;
        }

    </style>
@endpush

@section('dboard_content')
    <!-- title area -->
    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-end">
                <div class="col-md-9">
                    <h2 class="title-heading mb-1">Manage Store Product</h2>
                    <h3 class="title-heading mb-1">{{ $stores->name }}</h3>
                    <p><strong>Store ID: </strong>{{ $stores->code }}</p>
                </div>

                <div class="col-md-3 text-right">
                    <a class="btn index-btn" href="{{ route('manage-store-product.index') }}">Back</a>
                </div>
            </div>
            <hr>

            <form action="{{ route('manage-store-product.show', $stores->id) }}" method="GET">
                @csrf
                {{-- @method('HEAD') --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
                                    @include('product::shares.product')
                                </div>
                            </div><!-- end store type -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div id="submit_button">
                                        <button class="btn index-btn">Search</button>

                                        <a href="{{ route('manage-store-product.edit', $stores->id) }}">
                                            <button type="button" name="reset" id="reset"
                                                class="btn index-btn reset_btn">
                                                Reset
                                        </button>
                                        </a>
                                    </div>
                                </div>
                            </div><!-- end submit button -->
                        </div>
                    </div>
                </div><!-- end .row -->
            </form><!-- form end -->
            <hr>

            <!-- start .manage-store-product table -->
            <form action="{{ route('manage-store-product.store') }}" method="POST">
                @csrf
                <div class="manage-store-product">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm" id="bootstrapDataTable">
                                <thead>
                                    <tr class="">
                                    <th width=" 3%"><input type="checkbox" id="select-all"
                                            name="select-all"></th>
                                        <th width="7%">#</th>
                                        <th width="20%">Product Name</th>
                                        <th width="10%">Generic</th>
                                        <th width="10%">Product Type</th>
                                        <th width="10%">Manufacturer</th>
                                        {{--<th width="20%">Auto Requisition Min Qty</th>--}}
                                        <th width="8%">Product Property</th>
                                    </tr>
                                </thead>
                                <tbody class="item_add_area_bundle_product" id="auto_generated_tr">
                                    @if (isset($products))
                                        @foreach ($products as $key => $product)
                                            @if ($product_id->id == $product->id)
                                                <tr>
                                                    <td><input type="checkbox" id="" name="product_id[]" value="{{ $product->id }}">
                                                        <input type="hidden" name="store_id" value="{{ $stores->id }}">
                                                    </td>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>
                                                        <div>
                                                            <strong> {{ $product->product_name }} <input type="hidden" name="product_name[]"
                                                                    value="{{ $product->product_name }}"></st>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p>{{ $product->pgeneric->name }}
                                                            <input type="hidden" name="product_generic_id[]"
                                                                value="{{ $product->pgeneric->id }}">
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p>{{ $product->ptype->name }}
                                                            <input type="hidden" name="product_type_id[]"
                                                                value="{{ $product->ptype->id }}">
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p>{{ $product->manufacturer->name }}
                                                            <input type="hidden" name="product_manufacture_id[]"
                                                                value="{{ $product->manufacturer->id }}">
                                                            <input type="hidden" name="status[]" value="{{ $product->status }}">
                                                        </p>
                                                    </td>

                                                    {{--<td>
                                                        <input type="number" name="qty[]" value="" placeholder="0.00">
                                                    </td>--}}

                                                    <td>
                                                        <div class="d-flex align-items-center justify-content-center">
                                                            <button class="btn details-btn" data-toggle="modal"
                                                            data-target="#myModal" data-id="{{ $product->id }}">
                                                            <i class="fa fa-eye"></i>
                                                        </button>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                    <hr>

                    <div class="submit-button text-right">
                        <button class="btn index-btn text-right" type="submit">Active</button>
                    </div>
                </div><!-- end .manage-store-product table -->
            </form>

            <!-- Modal -->
            <div class="modal" tabindex="-1" id="myModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">View Product Property</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <!-- table -->
                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                <thead>
                                    <tr class="">
                                    <th width=" 10%">Id</th>
                                        <th width="30%">Product Variation</th>
                                        <th width="30%">Property Name</th>
                                        <th width="30%">Propertes</th>
                                    </tr>
                                </thead>
                                <tbody class="product_variations">

                                </tbody>
                            </table><!-- end table -->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content-area -->

    <!-- form start -->
    {{-- <form action="#"> --}}
    {{-- <form action="{{ route('manage-store-product',$product_id->id) }}" method="GET"> --}}



@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        // bootstrap data table configuration
        $('#bootstrapDataTable').DataTable();
    </script>
    <script>
        $(document).ready(function() {
            // select2 configuration
            $('.single-select').select2({
                width: '100%'
            });
        });
    </script>

    <script>
        $('#select-all').click(function(event) {
            if (this.checked) {
                // Iterate each checkbox
                $(':checkbox').each(function() {
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

        $('#select-all-a').click(function(event) {
            if (this.checked) {
                // Iterate each checkbox
                $("input.modal-check:checkbox").each(function() {
                    this.checked = true;
                });
            } else {
                $("input.modal-check:checkbox").each(function() {
                    this.checked = false;
                });
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.details-btn').click(function() {
                const id = $(this).attr('data-id');
                var url = "{{ url('product/get-product-vp') }}";
                var dltUrl = url + "/" + id;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function(response) {
                        // var response = JSON.parse(data);
                        // console.log(response);


                        $(".product_variations").html(response);

                        $('#myModal').modal('show');

                    }
                })
            });
        });
    </script>
@endpush
