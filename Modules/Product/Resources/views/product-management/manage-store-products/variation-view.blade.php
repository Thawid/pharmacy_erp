@extends('dboard.index')

@section('title', 'Manage Store Products')

@push('styles')
    

    <style>
        i {
            font-size: 18px;
        }

        #submit_button > button {
            margin-top: 28px;
        }

        p {
            margin: 0;
            padding: 0;
        }
    </style>
@endpush

@section('dboard_content')
    <!-- title area -->
    <div class="tile">
        <div class="tile-body">

            <div class="row align-items-center">
                <div class="col-md-6">
                    <h2 class="title-heading">Manage Store Products</h2>
                </div>

                <div class="col-md-6 text-md-right">
                    <a class="btn index-btn" href="{{ route('products.create') }}">Add Store Product</a>
                </div>
            </div><!-- end .row -->
            <hr>

            <!-- start .manage-store-product table -->
            <div class="manage-store-product">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm" id="bootstrapDataTable">
                            <thead>
                            <tr class="">
                                <th width="10%">#</th>
                                <th width="50%">Store Name</th>
                                <th width="10%">Status</th>
                                <th width="30%">Action</th>
                            </tr>
                            </thead>
                            <tbody class="item_add_area_bundle_product" id="auto_generated_tr">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- end .manage-store-product table -->
            <hr>
        </div>
    </div>

    <!-- content-area -->


    
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        // bootstrap data table configuration
        $('#bootstrapDataTable').DataTable();
    </script>
    <script>
        $(document).ready(function () {
            // select2 configuration
            $('.single-select').select2({
                width: '100%'
            });
        });
    </script>
@endpush
