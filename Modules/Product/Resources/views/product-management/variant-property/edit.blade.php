@extends('dboard.index')
@section('title', 'Update Properties')
@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Update Properties</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('variant_properties.index') }}">Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('variant_properties.update', $variant_property->id) }}" method="POST">
                        @csrf
                        <input name="_method" type="hidden" value="PUT">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="variant" class="forget-form">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $variant_property->name }}">
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="variant" class="forget-form">Select Property</label>
                                    <select class="js-example-basic-single form-control" name="variant_id">
                                        @foreach ($variants as $variant)
                                            <option value="{{ $variant->id }}"
                                                {{ $variant->id == $variant_property->variant_id ? 'selected' : '' }}>
                                                {{ $variant->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status" class="forget-form">Select Status</label>
                                    <select class="js-example-basic-single form-control" name="status">
                                        <option value="1" {{ $variant->status == 1 ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ $variant->status == 0 ? 'selected' : '' }}>InActive</option>
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection

@push('post_scripts')
<script>
    function varSpecAddValidation(){
        console.log('varSpecAddValidation is calling...');
    }
</script>
@endpush
