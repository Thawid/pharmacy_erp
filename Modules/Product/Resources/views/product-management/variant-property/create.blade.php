@extends('dboard.index')
@section('title', 'Create Properties')
@section('dboard_content')


    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">


                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Create Properties</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('variant_properties.index') }}">Back </a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('variant_properties.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="variant" class="forget-form">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Type variant property name here">
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    @include('product::shares.variant',['field'=>'variant_id'])
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    @include('product::shares.status')
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection

@push('post_scripts')
    <script>
        function varSpecAddValidation(){
            console.log('varSpecAddValidation is calling...');
        }
    </script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function() {
            $(".single-select").select2({
                width: '100%'
            });
        });
    </script>
@endpush
