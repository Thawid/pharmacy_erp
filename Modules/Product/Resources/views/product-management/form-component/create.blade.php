@extends('dboard.index')

@section('title','Dashboard')

@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Add Component</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-center">
                            <a class="btn btn-primary icon-btn" href="{{ route('form-components.index') }}"><i
                                    class="fa fa-arrow-left"></i>Back </a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    <!-- data-area -->
    <form action="#" method="POST">
        @csrf
        <div class="tile">
            <div class="row">

                <!-- select header group -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="select_header" class="forget-form">Select Header</label>
                            <select class="form-control select2" name="select_header">
                                <option value="">Select Header*</option>
                                <option value="0">General Info</option>
                                <option value="1">Brand & Manufacturer</option>
                                <option value="1">Variation Setup</option>
                                <option value="1">Pack Size</option>
                                <option value="1">Inventory Setup</option>
                                <option value="1">Description</option>
                            </select>
                        </div>
                    </div>
                <!-- end select header group -->

                <!-- component name group -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="component_name" class="forget-form">Component Name*</label>
                        <input type="text" class="form-control" name="component_name" value="{{ old('component_name') }}">
                    </div><!-- end form-group -->
                </div><!-- end component name group -->

                <!-- select data-type group -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="select_header" class="forget-form">Select Data Type</label>
                        <select class="form-control select2" name="select_header">
                            <option value="">Select Data Type*</option>
                            <option value="0">Text Field</option>
                            <option value="1">Text Area</option>
                            <option value="2">Check Box</option>
                            <option value="3">Drop Down Menu</option>
                            <option value="4">Button</option>
                            <option value="5">Radio Button</option>
                        </select>
                    </div>
                </div>
                <!-- end select data-type group -->

                <!-- value input group -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="value_input" class="forget-form">Value</label>
                        <input type="text" class="form-control" name="value_input" value="{{ old('value_input') }}">
                    </div><!-- end form-group -->
                </div><!-- end value input group -->
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="tile-footer">
                    <button class="btn btn-primary btn-block" type="submit">Create Form Component</button>
                </div><!-- end .tile-footer -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form><!-- end .form -->
    <!-- end data-area -->
@endsection
