@extends('dboard.index')

@section('title','Dashboard')

@push('styles')
    <style>
        .nav-tabs > a.nav-item {
            font-size: 15px;
        }

        .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
            color: white;
            background-color: #FFF;
            border-color: #dee2e6 #dee2e6 #FFF;
        }

    </style>
@endpush

@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <h2>Form Components</h2>
                        </div><!-- end col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn btn-primary icon-btn" href="{{ route('form-components.create') }}"><i
                                    class="fa fa-plus"></i>Add Product type</a>
                        </div><!-- ene .col-md-6 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile d-md-flex mb-0">
                <div class="col-md-2 text-center">
                    <!-- tab head -->
                    <nav>
                        <!-- tab title -->
                        <div class="nav nav-tabs nav-fill d-inline-block" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link text-right active" id="nav-general-info-tab" data-toggle="tab"
                               href="#nav-general-info"
                               role="tab" aria-controls="nav-general-info" aria-selected="true">General Info</a>

                            <a class="nav-item nav-link text-right" id="nav-specifications-tab" data-toggle="tab"
                               href="#nav-specifications"
                               role="tab" aria-controls="nav-specifications" aria-selected="false">Specification</a>

                            <a class="nav-item nav-link text-right" id="nav-variation-tab" data-toggle="tab"
                               href="#nav-variation"
                               role="tab" aria-controls="nav-variation" aria-selected="false">Variation</a>


                            <a class="nav-item nav-link text-right" id="nav-inventory-tab" data-toggle="tab"
                               href="#nav-inventory"
                               role="tab" aria-controls="nav-inventory" aria-selected="false">Inventory</a>

                            <a class="nav-item nav-link text-right" id="nav-description-tab" data-toggle="tab"
                               href="#nav-description"
                               role="tab"
                               aria-controls="nav-description" aria-selected="false">Description</a>
                        </div>
                    </nav>
                    <!-- end tab head -->
                </div>
                <div class="col-md-10">
                    <!-- tab content -->
                    <div class="tab-content px-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-general-info" role="tabpanel"
                             aria-labelledby="nav-general-info-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tile d-md-flex mb-0">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Components</th>
                                                    <th>Data Types</th>
                                                    <th>Values</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead><!-- end thead -->

                                                <tbody>
                                                <tr class="align-items-center">
                                                    <td>1</td>
                                                    <td>Brand Name</td>
                                                    <td>Text Field</td>
                                                    <td>Null</td>
                                                    <td class="d-md-flex ">
                                                        <form action="#">
                                                            <a href="#" class="btn btn-primary">Edit</a>
                                                            <a href="#" class="btn btn-primary">Delete</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                </tbody><!-- end tbody -->
                                            </table>
                                        </div>
                                    </div><!-- end .tile -->
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-general-info -->

                        <div class="tab-pane fade" id="nav-specifications" role="tabpanel"
                             aria-labelledby="nav-specifications-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tile d-md-flex mb-0">
                                        specifications
                                    </div><!-- end .tile -->
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-specifications -->

                        <div class="tab-pane fade" id="nav-variation" role="tabpanel"
                             aria-labelledby="nav-variation-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tile d-md-flex mb-0">
                                        variation
                                    </div><!-- end .tile -->
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-variation -->

                        <div class="tab-pane fade" id="nav-inventory" role="tabpanel"
                             aria-labelledby="nav-inventory-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tile d-md-flex mb-0">
                                        inventory
                                    </div><!-- end .tile -->
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-inventory -->

                        <div class="tab-pane fade" id="nav-description" role="tabpanel"
                             aria-labelledby="nav-description-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tile d-md-flex mb-0">
                                        description
                                    </div><!-- end .tile -->
                                </div><!-- end .col-md-12 -->
                            </div><!-- end .row -->
                        </div><!-- end nav-description -->
                    </div><!-- end .tab-content -->
                    <!-- end tab-content -->
                </div>
            </div>
        </div>
    </div>
    <!-- end data-area -->


@endsection
