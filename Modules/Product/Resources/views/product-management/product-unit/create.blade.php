@extends('dboard.index')
@section('title', 'Create Product Unit')
@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                <div class="row align-items-center">
                    <div class="col-md-9">
                        <h2 class="title-heading">Create Product Unit</h2>
                    </div><!-- end .col-md-9 -->

                    <div class="col-md-3 text-right">
                        <a class="btn index-btn" href="{{ route('units.index') }}">Back</a>
                    </div><!-- end .col-md-3 -->
                </div><!-- end .row -->
                <hr>

                <!-- data-area -->
                <form action="{{ route('units.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label for="unit_name" class="forget-form">Unit Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="unit_name" value="{{ old('unit_name') }}" placeholder="Type product unit name">
                                </div><!-- end form-group -->
                        </div><!-- end .col-md-6 -->

                        <div class="col-md-6">
                                <div class="form-group">
                                    @include('product::shares.status')
                                </div><!-- end form-group -->
                        </div><!-- end .col-md-6 -->
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="tile-footer">
                                <button class="btn create-btn" type="submit">Submit</button>
                            </div><!-- end .tile-footer -->
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                </form><!-- end form -->

                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection


@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function() {
            $(".single-select").select2({
                width: '100%'
            });
        });
    </script>
@endpush
