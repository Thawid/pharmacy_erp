@extends('dboard.index')
@section('title', 'Update Product Unit')
@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-9">
                            <h2 class="title-heading">Update Product Unit</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn index-btn" href="{{ route('units.index') }}">Back </a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('units.update', $product_unit->id) }}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="unit_name" class="forget-form">Unit Name</label>
                                        <input type="text" class="form-control" name="unit_name" value="{{ $product_unit->unit_name }}">
                                    </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="status" class="forget-form">Status</label>
                                        <select class="js-example-basic-single form-control" name="status">
                                            <option>Select Status</option>
                                            <option value="1" {{ $product_unit->status == 1 ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ $product_unit->status == 0 ? 'selected' : '' }}>InActive</option>
                                        </select>
                                    </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->
                        </div><!-- .row -->

                        <div class="row text-right">
                            <div class="col-md-12">
                                    <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection
