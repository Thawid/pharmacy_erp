@extends('dboard.index')

@section('title', 'Bulk Store Products')

@push('styles')
    

    <style>
        i {
            font-size: 18px;
        }

        #submit_button > button {
            margin-top: 28px;
            margin-left: 10px;
        }


        p {
            margin: 0;
            padding: 0;
        }
    </style>
@endpush

@section('dboard_content')

    <!-- start form  -->
    <form action="{{ route('bulk-store-product.store') }}" method="POST"> {{-- onsubmit="event.preventDefault();" --}}
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">

                        <!-- title area -->
                        <div class="row align-items-center">
                            <div class="col-md-6 text-left">
                                <h2 class="title-heading">Bulk Store Product Manager</h2>
                            </div>
                        </div><!-- end .row -->
                        <hr>

                        <div id="message">

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="from-group">
                                    <label for="">Select Store (Copy From)</label>
                                    <select class="form-control select2 store-type single-select"
                                            name="copy_from_store_id"
                                            id="copy_from_store_id">
                                        <option value="">Select Store To Copy</option>
                                        @foreach($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name }}</option>
                                        @endforeach
                                    </select>
                                    <span id="check_data"></span>
                                </div>
                            </div><!-- end select store (copy from) -->

                            <div class="col-md-4">
                                <div class="from-group">
                                    <label for="">Select Store (To Copy)</label>
                                    <select class="form-control select2 search-store single-select"
                                            name="copy_to_store_id"
                                            id="copy_to_store_id">
                                        <option value="">Select Store To Paste</option>
                                        @foreach($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div id="submit_button">
                                        <button id="subBtn" class="btn index-btn">Copy Products</button>
                                    </div>
                                </div>
                            </div><!-- end submit button -->
                        </div><!-- end .row -->
                        <hr>

                         <!-- start .manage-store-product table -->
                        <div class="row manage-store-product">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                    <thead>
                                    <tr class="">
                                        <th>Manage Store Id</th>
                                        <th>Store Id</th>
                                        <th>Product Id</th>
                                        <th>Quantity</th>
                                        <th>status</th>
                                    </tr>
                                    </thead>
                                    <tbody class="" id="loadDataHere">

                                    </tbody>
                                </table>
                            </div><!-- ene .col-md-12 -->
                        </div><!-- end .row .manage-store-product table -->
                    </div><!-- tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            // select2 configuration
            $('.single-select').select2({
                width: '100%'
            });
        });
    </script>

    <!-- catch data from store dropdown -->
    <script>
        $(document).ready(function () {
            $('#copy_from_store_id').on('change', function () {

                console.log("clicked!");
                let cfId = $("#copy_from_store_id").val();
                let ctId = $("#copy_to_store_id").val();
                console.log(cfId);
                console.log(ctId);

                $("#loadDataHere").empty();

                var url = "{{ url('product/check-manage-store') }}";
                var dltUrl = url + "/" + cfId;
                $.ajax({
                    url: dltUrl,
                    type: 'GET',
                    success: function (response) {
                        var response = JSON.parse(response);

                        response.forEach(elements => {
                            $("#loadDataHere").append(`
                                <tr>
                                    <td> <input type="" class="form-control" name="manage_store_id[]" value="${elements['id']}" readonly></td>
                                    <td> <input type="" class="form-control" name="store_id[]" value="${elements['store_id']}" readonly></td>
                                    <td> <input type="" class="form-control" name="product_id[]" value="${elements['product_id']}" readonly></td>
                                    <td> <input type="" class="form-control" name="qty[]" value="${elements['qty']}" readonly></td>
                                    <td> <input type="" class="form-control" name="status[]" value="${elements['status']}" readonly></td>
                                </tr>
                            `);
                        });


                    },
                    error: function (error) {
                        console.log("error: ");
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endpush
