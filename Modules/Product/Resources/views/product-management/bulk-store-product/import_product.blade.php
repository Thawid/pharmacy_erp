@extends('dboard.index')

@section('title','Product Import')

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="#">Product Import</a></li>

@endsection

@section('dboard_content')

<!-- start form  -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title area -->
        <div class="row align-items-center">
          <div class="col-md-6 text-left">
            <h2 class="title-heading">Import Product From Excel</h2>
          </div>
        </div><!-- end .row -->
        <hr>
        <div id="message">
        </div>
        <div class="row align-items-end">
          <div class="col-md-6">
            <form method="POST" action="{{ route('import-product.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="card">
                <div class="card-header">
                  <p><strong>Import Product Excel File</strong></p>
                </div>
                <div class="card-body">
                  {{-- import excel file --}}
                  <input type="file" name="product_excel" class="btn form-control form-control-file">
                  @if ($errors->has('product_excel'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('product_excel') }}</strong>
                  </span>
                  @endif
                </div><!-- end.card-body -->
                <div class="card-footer text-right">
                  <button type="submit" class="btn index-btn">Import Data</button>
                </div><!-- end.card-footer -->
              </div><!-- end.card -->
            </form>
          </div><!-- end select store (copy from) -->

          <div class="col-md-6 text-right">
            <div class="card">
              <div class="card-header">
                <p><strong>Demo Excel Download</strong></p>
              </div><!-- card-header -->
              <div class="card-body">
                {{-- Excel Download Button --}}
                <a href="{{ route('import-product.create') }}">
                  <button type="button" class="btn index-btn">Download Excel</button>
                </a>
              </div><!-- end.card-body -->
            </div><!-- end.card -->
          </div><!-- end.col-md-6 -->
        </div><!-- end .row -->

      </div><!-- tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->
@endsection

@push('scripts')

<script type="text/javascript">
  $(document).ready(function() {
    //
  });
</script>

@endpush