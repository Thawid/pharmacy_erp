@extends('dboard.index')
@section('title', 'Update Property Name')
@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Update Property Name</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('variants.index') }}">Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('variants.update', $variant->id) }}" method="POST">
                        @csrf
                        <input name="_method" type="hidden" value="PUT">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="product_type" class="forget-form">Product Type*</label>
                                    <select class="form-control select2 product-type single-select" name="product_type_id"
                                            id="product_type">
                                        @foreach($product_types as $ptype)
                                            <option value="{{ $ptype->id }}" {{ $ptype->id ==  $variant->product_type_id ? 'selected' : '' }}>
                                                {{ $ptype->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="variant" class="forget-form">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $variant->name }}" placeholder="enter property name">
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status" class="forget-form">Select Status</label>
                                    <select class="single-select form-control" name="status">
                                        <option value="1" {{ $variant->status == 1 ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ $variant->status == 0 ? 'selected' : '' }}>InActive</option>
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection

@push('post_scripts')
    <script>
        function generalInfo(){
            console.log('generalInfo is calling...');
        }
    </script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function() {
            $(".single-select").select2({
                width: '100%'
            });
        });
    </script>
@endpush
