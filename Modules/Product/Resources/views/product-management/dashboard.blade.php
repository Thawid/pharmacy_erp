@extends('dboard.index')

@section('title','Dashboard')

@push('styles')
  <style>
    .widget-small.coloured-icon {
        background-color: #F4F4F4 !important;
        color: #2a2a2a;
        box-shadow: rgb(38, 57, 77) 0px 20px 15px -20px;
      }
  </style>
@endpush

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <!-- title -->

        <div class="row">
          <div class="col-md-6">
            <h2 class="title-heading">Recent 5 Products</h2>
          </div><!-- end col-md-6 -->
        </div><!-- end.row -->
        <hr>

        <!-- info -->
        <div class="row mt-4">
          <div class="col-md-6 col-lg-3 col-xl-3">
            <div class="widget-small primary coloured-icon">
              <i class="icon fa fa-users fa-3x"></i>
              <div class="info">
                <h4>Product Type</h4>
                <p><b>{{$total_product_type}}</b></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xl-3">
            <div class="widget-small info coloured-icon">
              <!-- <i class="icon fa fa-thumbs-o-up fa-3x"></i> -->
              <i class="icon fa fa-home fa-3x"></i>
              <div class="info">
                <h4>Generic</h4>
                <p><b>{{$total_product_generic}}</b></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xl-3">
            <div class="widget-small warning coloured-icon">
              <i class="icon fa fa-files-o fa-3x"></i>
              <div class="info">
                <h4>Products</h4>
                <p><b>{{$total_products}}</b></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xl-3">
            <div class="widget-small danger coloured-icon">
              <i class="icon fa fa-star fa-3x"></i>
              <div class="info">
                <h4>Category</h4>
                <p><b>{{$total_product_categories}}</b></p>
              </div>
            </div>
          </div>
        </div><!-- end.row -->

        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th width="15%">Product Image</th>
                    <th>Product Brand Name</th>
                    <th>Product Name</th>
                  </tr><!-- end tr -->
                </thead><!-- end thead -->

                <tbody class="align-items-center">
                  @if (count($latest_products) > 0)
                  @foreach ($latest_products as $key => $row)
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>
                      <img src="{{ asset('feature-image' . '/' . $row->ProductDetails->feature_image) }}" alt="Product images" height="60px" width="60px">
                    </td>
                    <td>{{ $row->brand_name }}</td>
                    <td>{{ $row->product_name }}</td>
                  </tr>
                  @endforeach
                  @endif
                </tbody><!-- end tbody -->
              </table><!-- end table -->
            </div><!-- end .table-responsive -->
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->

@endsection

@push('post_scripts')

@endpush()