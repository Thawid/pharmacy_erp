@extends('dboard.index')

@section('title', 'Product-Generic')

@section('dboard_content')

    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Product Generic List</h2>
                        </div><!-- end col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn create-btn" href="{{ route('generics.create') }}">Add Product Generic</a>
                        </div><!-- ene .col-md-6 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Product Type</th>
                                            <th>Product Generic Name</th>
                                            <th>Status</th>
                                            <th width="8%">Action</th>
                                        </tr><!-- end tr -->
                                    </thead><!-- end thead -->

                                    <tbody>
                                        @if (isset($product_generic))
                                            @foreach ($product_generic as $key => $row)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $row->ptype->name }}</td>
                                                    <td>{{ $row->name }}</td>
                                                    <td>
                                                        @if ($row->status == 1)
                                                            <span class="badge badge-success m-1 p-2">Active</span>
                                                        @else
                                                            <span class="badge badge-danger m-1 p-2">Inactive</span>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <a href="{{ route('generics.edit', $row->id) }}"
                                                                class="btn edit-btn"><i
                                                                    class="fa fa-lg fa-pencil"></i></a>
                                                            {{-- <a href="#" class="btn btn-outline-primary mr-3"><i class="fa fa-lg fa-trash"></i>Disable</a> --}}
                                                            <form id="delete_form{{ $row->id }}" method="POST"
                                                                action="{{ route('generics.destroy', $row->id) }}"
                                                                onclick="return confirm('Are you sure?')">
                                                                @csrf
                                                                @method("DELETE")
                                                                <button class="btn edit-btn" type="submit"><i
                                                                    class="fa fa-lg fa-refresh"></i></button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody><!-- end tbody -->
                                </table><!-- end table -->
                            </div><!-- end .table-responsive -->
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    



@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
@endpush
