@extends('dboard.index')
@section('title', 'Create Generic')
@section('dboard_content')

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title-area -->
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Create Generic</h2>
                        </div><!-- end .col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('generics.index') }}">Back</a>
                        </div><!-- end .col-md-6 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('generics.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                    <div class="form-group">
                                        @include('product::shares.product-types')
                                    </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="category_name" class="forget-form">Name<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Type product generic name here">
                                    </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                    <div class="form-group">
                                        @include('product::shares.status')
                                    </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                    <button class="btn create-btn" type="submit">Create</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection

@push('post_scripts')
<script>
    function generalInfo(){
        console.log('generalInfo is calling...');
    }
</script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function() {
            $(".single-select").select2({
                width: '100%'
            });
        });
    </script>
@endpush
