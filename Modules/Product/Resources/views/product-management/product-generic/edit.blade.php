@extends('dboard.index')
@section('title', 'Update Generic')
@section('dboard_content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <!-- title-area -->
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-center">Update Generic</h2>
                        </div><!-- end .col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('generics.index') }}">Back</a>
                        </div><!-- end .col-md-6 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('generics.update', $product_generic->id) }}" method="POST">
                        @csrf
                        <input name="_method" type="hidden" value="PUT">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="product_type" class="forget-form">Product Type*</label>
                                    <select class="form-control select2 product-type single-select" name="product_type_id"
                                            id="product_type">
                                        @foreach($product_types as $ptype)
                                            <option
                                                value="{{ $ptype->id }}" {{ $ptype->id ==  $product_generic->product_type_id ? 'selected' : '' }}>
                                                {{ $ptype->name }}</option>
                                        @endforeach
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="category_name" class="forget-form">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $product_generic->name }}">
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status" class="forget-form">Select Status</label>
                                    <select class="js-example-basic-single form-control" name="status">
                                        <option value="1" {{ $product_generic->status == 1 ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ $product_generic->status == 0 ? 'selected' : '' }}>InActive</option>
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-4 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                    <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection

@push('post_scripts')
    <script>
        function generalInfo(){
            console.log('generalInfo is calling...');
        }
    </script>
    <!-- select2 -->
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <!-- select2 configuration -->
    <script>
        $(document).ready(function () {
            $(".multiple-select").select2({width: '100%'});
            $(".single-select").select2({width: '100%'});
        });
    </script>
@endpush
