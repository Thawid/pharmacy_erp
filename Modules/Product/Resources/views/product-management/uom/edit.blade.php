@extends('dboard.index')
@section('title', 'Update Unit Of Measurement')
@section('dboard_content')

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title-area -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Update Unit Of Measurement</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('uom.index') }}">Back </a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('uom.update', $uom->id) }}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="unit_name" class="forget-form">UOM Name</label>
                                    <input type="text" class="form-control" name="uom_name" value="{{ $uom->uom_name }}">
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status" class="forget-form">Status</label>
                                    <select class="js-example-basic-single form-control" name="status">
                                        <option>Select Status</option>
                                        <option value="1" {{ $uom->status == 1 ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ $uom->status == 0 ? 'selected' : '' }}>InActive</option>
                                    </select>
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection
