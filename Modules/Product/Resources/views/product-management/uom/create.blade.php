@extends('dboard.index')
@section('title', 'UOM Create')
@section('dboard_content')

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    
                    <!-- title-area -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Create Unit Of Measurement</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{ route('uom.index') }}">Back </a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- data-area -->
                    <form action="{{ route('uom.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="unit_name" class="forget-form">UOM Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="uom_name" value="{{ old('uom_name') }}" placeholder="Type UOM name here">
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-6">
                                <div class="form-group">
                                    @include('product::shares.status')
                                </div><!-- end form-group -->
                            </div><!-- end .col-md-6 -->
                        </div><!-- .row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Submit</button>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->
                    </form><!-- end form -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    
@endsection


@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function() {
            $(".single-select").select2({
                width: '100%'
            });
        });
    </script>
@endpush
