@extends('dboard.index')

@section('title', 'Product Report')

@push('styles')
    <style>
        #button-area i {
            font-size: 18px;
        }

    </style>
@endpush

@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('get.product.report') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">

                        <!-- title -->
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="title-heading">Product Summary Report</h2>
                            </div><!-- end col-md-6 -->
                        </div><!-- end .row -->
                        <hr>

                        <!-- filtering area -->
                        <div class="filtering-area">
                            <!-- first row -->
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="create_date_form">Create Date From</label>
                                        <input type="date" class="form-control" name="create_date_form"
                                            id="create_date_form">
                                    </div>
                                </div><!-- end .col-md-3 -->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="create_date_to">Create Date To</label>
                                        <input type="date" class="form-control" name="create_date_to" id="create_date_to">
                                    </div>
                                </div><!-- end .col-md-3 -->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="package_type">Package Type</label>
                                        <select class="form-control" name="package_type_id" id="package_type">
                                            <option value="" selected disabled> Select Package Type </option>
                                            <option value="1">Individual Product</option>
                                            <option value="2">Regular Bundle Product</option>
                                        </select>
                                    </div>
                                </div><!-- end .col-md-3 -->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="product_type">Product Type</label>
                                        <select class="form-control" name="product_type_id" id="product_type">
                                            <option value="" selected disabled>
                                              Select Product Type</option>
                                            @if (isset($product_types))
                                                @foreach ($product_types as $product_type)
                                                    <option value="{{ $product_type->id }}">{{ $product_type->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div><!-- end .col-md-3 -->
                            </div><!-- end .row -->

                            <!-- second row -->
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="product_category">Product Category</label>
                                        <select class="form-control" name="product_category_id" id="product_category">
                                            <option value="" selected disabled>Select Product Category</option>
                                            @if (isset($product_categories))
                                                @foreach ($product_categories as $product_category)
                                                    <option value="{{ $product_category->id }}">
                                                        {{ $product_category->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div><!-- end .col-md-3 -->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="product_generic">Product Generic</label>
                                        <select class="form-control" name="product_generic_id" id="product_generic">
                                            <option value="" selected disabled>Select Product Generic</option>
                                            @if (isset($product_generics))
                                                @foreach ($product_generics as $product_generic)
                                                    <option value="{{ $product_generic->id }}">
                                                        {{ $product_generic->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div><!-- end .col-md-3 -->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="manufacturer">Manufacturer</label>
                                        <select class="form-control" name="manufacturer_id" id="manufacturer">
                                            <option value="" selected disabled>Select Manufacturer</option>
                                            @if (isset($manufacturers))
                                                @foreach ($manufacturers as $manufacturer)
                                                    <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div><!-- end .col-md-3 -->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="store">Store</label>
                                        <select class="form-control" name="store_id" id="store">
                                            <option value="" selected disabled>Select Store</option>
                                            @if (isset($stores))
                                                @foreach ($stores as $store)
                                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div><!-- end .col-md-3 -->
                            </div><!-- end .row -->
                            <hr>

                            <!-- filter button -->
                            <div class="row mt-4 mb-4 text-right">

                                <div class="col-md-12">
                                    <a href="{{ route('product.report') }}" class="btn index-btn"
                                        type="submit" title="Reset Button">Reset <i class="pl-2 fa fa-refresh" style="font-size: 20px;"></i></a>

                                    <button class="btn index-btn" type="submit" title="filter button">Filter <i class="pl-2 fa fa-filter"
                                            style="font-size: 20px;"></i>
                                    </button>
                                </div><!-- end .col-md-6 -->
                            </div>
                        </div>
                        <hr>

                        <div class="table-responsive printable-area">
                            <table class="table table-striped table-hover table-bordered " id="table2excel">
                                <thead>
                                    <tr>
                                        <th width="10%">Id</th>
                                        <th width="40%">Product Brand Name</th>
                                        <th width="40%">Product Name</th>
                                        <th width="10%">Product Image</th>
                                    </tr><!-- end tr -->
                                </thead><!-- end thead -->

                                <tbody>
                                    @if (isset($products))
                                        @foreach ($products as $product)
                                            <tr>
                                                <td>{{ $product->id }}</td>

                                                <td>{{ $product->brand_name }}</td>

                                                <td>{{ $product->product_name }}</td>

                                                <td>
                                                    <img src="{{ asset('feature-image' . '/' . $product->ProductDetails->feature_image) }}"
                                                        alt="Product images" height="50px" width="80px">
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody><!-- end tbody -->
                            </table><!-- end table -->
                        </div><!-- end .table-responsive -->
                        <hr>
                        <div id="button-area">
                            <div class="row text-right">
                                <div class="col-md-12">
                                    <a href="#" class="btn create-btn" type="button" id="pdf_btn" title="Print Button">Print <i
                                            class="pl-2 fa fa-print"></i>
                                    </a>
                                    <a href="#" class="btn create-btn" title="Show Button">Show <i class="pl-2 fa fa-eye"
                                            aria-hidden="true"></i></a>
                                    <a href="#" class="btn create-btn" type="button" id="print_btn" title="PDF Button">PDF <i
                                            class="pl-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                    <button class="btn create-btn" type="button" title="Excel Button">Excel <i
                                            class="pl-2 fa fa-file-excel-o" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div><!-- end .title-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>
@endsection

@push('post_scripts')
    <script
        src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
    </script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <!-- print area plugin -->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.printarea.js') }}"></script>
    <script type="text/javascript">
        $('#table2excel').DataTable();
    </script>

    <script>
        $(function() {
            $("button").click(function() {
                $("#table2excel").table2excel({
                    exclude: ".xls",
                    name: "Product Report"
                });
            });
        });
    </script>

    {{-- script to print a specific area --}}
    <script>
        $(function() {
            $("#print_btn").on('click', function() {

                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printable-area").printArea(options);
            });
        });
    </script>

    {{-- script to print a specific area --}}
    <script>
        $(function() {
            $("#pdf_btn").on('click', function() {

                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printable-area").printArea(options);
            });
        });
    </script>
@endpush
