@extends('dboard.index')

@section('title','Regular Bundle List')

@push('styles')
    
    <style>
        p {
            padding: 0;
            margin: 0;
        }

        i {
            font-size: 18px;
        }
    </style>
@endpush

@section('dboard_content')
    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Show Advance Bundle Product</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn btn-outline-primary icon-btn" href="{{route('advance.index')}}"><i
                                    class="fas fa-arrow-circle-left pr-2"></i> Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="general-info-data-area">
                <div class="card">
                    <div class="card-header">
                        <h2>Advance Bundle Product</h2>
                    </div><!-- end .card-header -->
                    <div class="card-body">
                        <h4 class="font-weight-bold"><strong class="pr-2">Bundle Name:</strong>{{ $advanceBundle->bundle_name }}</h4>
                        <ul class="d-md-block mb-3 bg-light pt-2 pb-2">
                            @if(isset($advanceBundle))
                                @foreach($advanceBundle->advanceProducts as $child)
                                    <ul class="d-md-block mb-3 bg-light pt-2 pb-2">
                                        <h5>Product Name: {{$child->product_name}}</h5>
                                        </li>
                                        <li>
                                            <p><strong>Status: </strong>
                                                @if($child->status){{ "Active" }}
                                                @else
                                                    {{ "InActive" }}
                                                @endif</p>
                                        </li>
                                    </ul>
                                @endforeach
                            @endif
                        </ul>
                    </div><!-- end card-body -->
                </div><!-- end .card -->
            </div><!-- end .general-info-data-area -->
        </div><!-- end .col-md-12 -->
    </div><!-- .row -->
@endsection
