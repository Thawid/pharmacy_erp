@extends('dboard.index')

@section('title', 'Advance Bundle Product')

@push('styles')
    

    <style>
        i {
            font-size: 18px;
        }

    </style>
@endpush


@section('dboard_content')

    <div class="tile">
        <div class="tile-body">
            <div class="row">
                <div class="col-md-6 text-left">
                    <h2>Advance Bundle Product List </h2>
                </div>
                <div class="col-md-6 text-md-right">
                    <a class="btn btn-outline-primary icon-btn" href="{{ route('advance.create') }}"><i
                            class="pr-2 fas fa-plus-circle"></i>Add
                        Advance Bundle Product</a>
                </div>
            </div>

        </div>
    </div>

    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-hover bg-white table-bordered" id="sampleTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Lot</th>
                                    <th>Bundle Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr><!-- end tr -->
                            </thead><!-- end thead -->

                            <tbody>
                                @if (isset($advanceBundle))
                                    @foreach ($advanceBundle as $key => $row)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>
                                                @foreach ($row->advanceProducts as $r)
                                                    <span class="badge badge-info p-2">
                                                        @if ($r->lot_id === 1)
                                                            {{ '12-1232-12' }}
                                                        @elseif($r->lot_id === 2)
                                                            {{ '64-1745-12' }}
                                                        @else
                                                            {{ '84-10932-3' }}
                                                        @endif
                                                    </span>
                                                @endforeach
                                            </td>
                                            <td>{{ $row->bundle_name }}</td>
                                            <td>
                                                @if ($row->status == 1)
                                                    <span class="badge badge-success m-1 p-2">Active</span>
                                                @else
                                                    <span class="badge badge-danger m-1 p-2">Inactive</span>
                                                @endif
                                            </td>

                                            <td class="d-md-flex">
                                                <a href="{{ route('advance.edit', $row->id) }}"
                                                    class="btn btn-outline-primary mr-3"><i
                                                        class="fa fa-lg fa-edit"></i>Edit</a>
                                                <a href="{{ route('advance.show', $row->id) }}"
                                                    class="btn btn-outline-primary mr-3"><i
                                                        class="fas fa-eye pr-2"></i>Show</a>
                                                <form id="delete_form{{ $row->id }}" method="POST"
                                                    action="{{ route('advance.destroy', $row->id) }}"
                                                    onclick="return confirm('Are you sure?')">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-outline-primary" type="submit">Change
                                                        Status
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody><!-- end tbody -->
                        </table><!-- end table -->
                    </div><!-- end .table-responsive -->
                </div><!-- end .title-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
@endpush
