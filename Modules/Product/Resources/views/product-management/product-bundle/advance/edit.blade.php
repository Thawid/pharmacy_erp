@extends('dboard.index')
@section('title', 'Update Advance Bundle')

@push('styles')
    
    <style>
        p {
            padding: 0;
            margin: 0;
        }

        .add-button-area>a {
            margin-top: 25px;
        }

        #submitButton>button {
            margin-top: 30px;
        }

    </style>
@endpush

@section('dboard_content')

    <!-- title -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Update Advance Bundle Product</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn btn-outline-primary icon-btn" href="{{ route('advance.index') }}"><i
                                    class="fa fa-arrow-left"></i>Back </a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

    <form action="{{ route('advance.update', $advanceBundle->id) }}" method="POST">
        @csrf
        @method('PUT')
        <!-- select & and product area -->
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="select-lot-area rounded">
                                    <label for="lot_select">Select Lot</label>
                                    <select class="form-control single-select" id="lot_id"
                                        onchange="addNewProductValidation()">
                                        <option value="" selected>Select Lot</option>
                                        <option value="1">12-1232-12</option>
                                        <option value="2">64-1745-12</option>
                                        <option value="3">84-10932-3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="select-product-area rounded">
                                    <label for="product_select">Select Product</label>
                                    <select class="form-control single-select" id="product_id"
                                        onchange="addNewProductValidation()">
                                        <option value="" selected>Select Product</option>
                                        @foreach ($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="select-product-area rounded">
                                    <label for="uom_select">Select Uom</label>
                                    <select class="form-control single-select" id="uom_id"
                                        onchange="addNewProductValidation()">
                                        <option value="" selected>--- Select Uom ---</option>
                                        @foreach ($uoms as $uom)
                                            <option value="{{ $uom->id }}">{{ $uom->uom_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="add-button-area">
                                    <a href="#" class="btn btn-outline-primary" id="add_bundle_product">Add<i
                                            class="pl-2 fas fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </div><!-- end .row -->

                        <div class="row mt-5">
                            <div class="col-md-12">
                                <!-- bundle product table -->
                                <div class="bundle-product-table mt-3">
                                    <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                        <thead>
                                            <tr class="">
                                                <th width="20%">Lot No</th>
                                                <th width="40%">Product Name</th>
                                                <th width="35%">Uom Name</th>
                                                <th width="5%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody class="item_add_area_bundle_product" id="auto_generated_tr">
                                            @if (isset($advanceBundle))
                                                @foreach ($advanceBundle->advanceProducts as $child)
                                                    <tr>
                                                        <td>{{ $child->product_variation_id }}
                                                            <input type="hidden" class="form-control" name="product_id[]"
                                                                id="rproduct_id" value="{{ $child->product_id }}"
                                                                readonly>
                                                            <input type="hidden" class="form-control"
                                                                name="product_variation_id[]" id="rproductv_id"
                                                                value="{{ $child->product_variation_id }}" readonly>
                                                        </td>
                                                        <td>
                                                            @if ($child->lot_id === 1)
                                                                {{ '12-1232-12' }}
                                                            @elseif($child->lot_id === 2)
                                                                {{ '64-1745-12' }}
                                                            @else
                                                                {{ '84-10932-3' }}
                                                            @endif
                                                            <input type="hidden" class="form-control" name="lot_id[]"
                                                                value="{{ $child->lot_id }}" readonly>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                <h3>{{ $child->product_name }}
                                                                    <input type="hidden" class="form-control"
                                                                        name="product_name[]"
                                                                        value="{{ $child->product_name }}" readonly>
                                                                </h3>
                                                            </div>
                                                        </td>
                                                        <td><a href="#" class="btn btn btn-danger item_remove btn-xs">x</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                </div><!-- end .bundle-product-table -->
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <!-- select & and product area -->
                        <div class="row mt-5">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="bundle_name">Bundle Name</label>
                                    <input type="text" name="bundle_name" id="bndl_name" class="form-control"
                                        placeholder="Enter bundle name." value="{{ $advanceBundle->bundle_name }}"
                                        onkeyup="submitButtonValidation()">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <!-- buttons-area -->
                                <div id="submitButton">
                                    <button class="btn btn-primary" type="submit" id="advanceSubmitButton">
                                        Submit <i class="pl-2 fas fa-paper-plane"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row mt-3">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row">

                            <div class="col-md-3">
                                <div class="select-product-area rounded">
                                    <label for="product_select">Select Product</label>
                                    <select class="form-control single-select" name="product_select" id="product_id"
                                            onchange="addNewProductValidation()">
                                        <option value="" selected>Select Product</option>
                                        @foreach ($products as $product)
                                            <option
                                                value="{{ $product->id }}">{{ $product->product_variation_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="add-button-area">
                                    <a href="#" class="btn btn-outline-primary" id="add_bundle_product">Add<i
                                            class="pl-2 fas fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- bundle product table -->
        <div class="bundle-product-table mt-3">
            <div class="tile">
                <table
                    class="table table-striped table-bordered table-hover bg-white table-sm">
                    <thead>
                    <tr class="">
                        <th width="10%">id</th>
                        <th width="10%">Lot ID</th>
                        <th width="70%">Product Name</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody class="item_add_area_bundle_product" id="auto_generated_tr">
                    @if (isset($advanceBundle))
                        @foreach ($advanceBundle->advanceProducts as $child)
                            <tr>
                                <td>{{ $child->product_variation_id }}
                                    <input type="hidden" class="form-control"
                                           name="product_id[]" id="rproduct_id"
                                           value="{{ $child->product_id }}" readonly>
                                    <input type="hidden" class="form-control"
                                           name="product_variation_id[]" id="rproductv_id"
                                           value="{{ $child->product_variation_id }}" readonly>
                                </td>
                                <td>
                                    @if ($child->lot_id === 1)
                                        {{ "12-1232-12" }}
                                    @elseif($child->lot_id === 2)
                                        {{ "64-1745-12" }}
                                    @else
                                        {{"84-10932-3"}}
                                    @endif
                                    <input type="hidden" class="form-control"
                                           name="lot_id[]" value="{{ $child->lot_id }}" readonly>
                                </td>
                                <td>
                                    <div>
                                        <h3>{{ $child->product_name }}
                                            <input type="hidden" class="form-control" name="product_name[]"
                                                   value="{{ $child->product_name }}"
                                                   readonly>
                                        </h3>
                                    </div>
                                </td>
                                <td><a href="#" class="btn btn btn-danger item_remove btn-xs">x</a></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div><!-- end .bundle-product-table -->

        <!-- select & and product area -->
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="bundle_name">Bundle Name</label>
                                    <input type="text" name="bundle_name" id="bndl_name" class="form-control"
                                           placeholder="Enter bundle name." value="{{ $advanceBundle->bundle_name }}"
                                           onkeyup="submitButtonValidation()">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <!-- buttons-area -->
                                <div id="submitButton">
                                    <button class="btn btn-primary" type="submit" id="advanceSubmitButton">
                                        Update <i class="pl-2 fas fa-paper-plane"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}


    </form><!-- end from -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            // select2 configuration
            $('.single-select').select2({
                width: '100%'
            });
        });
    </script>

    <!-- startup script -->
    <script>
        $("#add_bundle_product").hide();
        submitButtonValidation();
    </script>

    <!-- helper functions -->
    <script>
        function addNewProductValidation() {
            if ($("#product_id option:selected").val() > 0 && $("#lot_id option:selected").val() > 0 && $(
                    "#uom_id option:selected").val() > 0) {
                $("#add_bundle_product").show();
            } else {
                $("#add_bundle_product").hide();
            }
        }

        function submitButtonValidation() {
            if ($("#bndl_name").val().length >= 2) {
                $("#advanceSubmitButton").show();
            } else {
                $("#advanceSubmitButton").hide();
            }
        }
    </script>

    <!-- adding bundle tr on button click -->
    <script>
        $(document).ready(function() {
            $('#add_bundle_product').on('click', function() {
                let lotValue = $('#lot_id option:selected').val();
                let lotText = $('#lot_id option:selected').text();

                let product_value = $("#product_id option:selected").val();
                let product_name = $("#product_id option:selected").text();

                let uom_value = $("#uom_id option:selected").val();
                let uom_name = $("#uom_id option:selected").text();


                $("#auto_generated_tr").append(
                    `<tr>
                        <td>
                            <input class="form-control" value="${lotText}" readonly>
                            <input type="hidden" name="lot_id_ag[]" value="${lotValue}">
                        </td>

                        <td>
                            <input class="form-control" name="product_name_ag[]" value="${product_name}" readonly>
                            <input type="hidden" name="product_id_ag[]" value="${product_value}">
                        </td>

                        <td>
                            <input class="form-control" value="${uom_name}" readonly>
                            <input type="hidden" name="uom_id_ag[]" value="${uom_value}">
                        </td>

                        <td><a class="btn btn-outline-danger item-remove">x</a></td>
                    </tr>`
                );

                // Remove Item
                $(document).on('click', '.item_remove', function() {
                    $(this).parent().parent().remove();

                });

                // clear dropdown field
                $("#product_id").select2("val", " ");
                $('#product_id').select2({
                    placeholder: "Select Product"
                });

                $("#lot_id").select2("val", " ");
                $('#lot_id').select2({
                    placeholder: "Select Lot"
                });

                $("#uom_id").select2("val", " ");
                $('#uom_id').select2({
                    placeholder: "Select Lot"
                });

                // add button validation
                $("#add_bundle_product").hide();
            });
        });
    </script>

@endpush
