@extends('dboard.index')

@section('title','Regular Bundle List')

@push('styles')

<style>
  i {
    font-size: 18px;
  }
</style>
@endpush

@section('dboard_content')
<!-- title-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Regular Bundle Product</h2>
          </div><!-- end .col-md-6 -->

          <div class="col-md-6 text-right">
            <a class="btn index-btn" href="{{route('regular.index')}}">Back</a>
          </div><!-- end .col-md-6 -->
        </div><!-- end .row -->
        <hr>

        <!-- data-area -->
        <div class="row">
          <div class="col-md-12">
            <div class="general-info-data-area">
              <div class="card">
                <div class="card-body">
                  <h5 class="font-weight-bold">Bundle
                      Name:{{ $regularBundle->bundle_name }}</h4>
                  <hr>
                  @if(isset($regularBundle))
                  @foreach($regularBundle->bundleProducts as $child)
                  <ul class="d-md-block mb-3 bg-light pt-2 pb-2">
                    <li>
                      <p><strong>Product Name: {{$child->product_name}}</strong></p>
                    </li>
                    <li>
                      <p><strong>Status: </strong>
                        @if($child->status)
                        {{ "Active" }}
                        @else
                        {{ "InActive" }}
                        @endif
                      </p>
                    </li>
                  </ul>
                  @endforeach
                  @endif

                </div><!-- end card-body -->
              </div><!-- end .card -->
            </div><!-- end .general-info-data-area -->
          </div><!-- end .col-md-12 -->
        </div><!-- .row -->

      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->


@endsection