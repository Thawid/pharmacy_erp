@extends('dboard.index')

@section('title', 'Regular Bundle List')

@push('styles')
    

    <style>
        i {
            font-size: 18px;
        }

    </style>
@endpush

@section('dboard_content')

    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Regular Bundle Product List</h2>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a class="btn create-btn" href="{{ route('regular.create') }}">Add Regular Bundle</a>
                        </div>
                    </div><!-- end .row -->
                    <hr>

                    <!-- data -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Bundle Name</th>
                                            <th>Status</th>
                                            <th width="12%">Action</th>
                                        </tr><!-- end tr -->
                                    </thead><!-- end thead -->
        
                                    <tbody>
                                        @if (isset($regularBundle))
                                            @foreach ($regularBundle as $key => $row)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $row->bundle_name }}</td>
                                                    <td>
                                                        @if ($row->status == 1)
                                                            <span class="badge badge-success m-1 p-2">Active</span>
                                                        @else
                                                            <span class="badge badge-danger m-1 p-2">Inactive</span>
                                                        @endif
                                                    </td>
        
        
                                                    <td>
                                                        <div class="d-flex justify-content-around align-items-center">
                                                            <a href="{{ route('regular.edit', $row->id) }}"
                                                                class="btn edit-btn"><i
                                                                    class="fa fa-lg fa-pencil"></i></a>
                                                            <a href="{{ route('regular.show', $row->id) }}"
                                                                class="btn details-btn"><i
                                                                    class="fa fa-eye"></i></a>
                                                            <form id="delete_form{{ $row->id }}" method="POST"
                                                                action="{{ route('regular.destroy', $row->id) }}"
                                                                onclick="return confirm('Are you sure?')">
                                                                @csrf
                                                                <input name="_method" type="hidden" value="DELETE">
                                                                <button class="btn edit-btn" type="submit">
                                                                    <i class="fa fa-refresh"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody><!-- end tbody -->
                                </table><!-- end table -->
                            </div><!-- end .table-responsive -->
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row --> 
                </div><!-- end .title-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
@endpush
