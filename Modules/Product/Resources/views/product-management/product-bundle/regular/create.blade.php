@extends('dboard.index')

@section('title', 'Regular Bundle')

@push('styles')
    
    <style>
        p {
            padding: 0;
            margin: 0;
        }

        .add-button-area>a {
            margin-top: 27px;
            border: 2px solid #006ABD;
            color: #006ABD;
        }

        .add-button-area>a:hover {
            color: white;
            background: #006abd;
        }

        #submitButton>button {
            margin-top: 28px;
        }

        i {
            font-size: 18px;
        }

    </style>
@endpush

@section('dboard_content')

    <form action="{{ route('regular.store') }}" method="POST">
        @csrf

        <!-- select & and product area -->
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <!-- title -->
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="title-heading">Regular Bundle Product</h2>
                            </div><!-- end .col-md-6 -->
    
                            <div class="col-md-6 text-right">
                                <a class="btn index-btn" href="{{ route('regular.index') }}">Back </a>
                            </div><!-- end .col-md-6 -->
                        </div><!-- end .row -->
                        <hr>

                        <!-- data -->
                        <div class="row">
                            <div class="col-lg-5 col-xl-4">
                                <div class="select-product-area">
                                    <label for="product_select">Select Product <span style="color: red">*</span></label>
                                    <select class="form-control single-select" name="product_select" id="product_id"
                                        onchange="isEmptyProductAndUom()">
                                        <option value="" selected disabled>Select Product</option>
                                        @foreach ($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->product_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!-- end .col-md-4 -->

                            <div class="col-lg-5 col-xl-4">
                                <div class="form-group">
                                    <label for="uom_select">Select Uom <span style="color: red">*</span></label>
                                    <select class="form-control single-select" name="uom_id" id="select_uom"
                                        onchange="isEmptyProductAndUom()">
                                        <option value="" selected disabled>Select Unit Of Measurement</option>
                                        @foreach ($uoms as $uom)
                                            <option value="{{ $uom->id }}">{{ $uom->uom_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!-- end .col-md-4 -->

                            <div class="col-lg-2 col-xl-4">
                                <div class="add-button-area">
                                    <a href="#" class="btn index-btn" id="add_bundle_product">Add</a>
                                </div>
                            </div><!-- end .col-md-4 -->
                        </div><!-- end .row -->

                        <!-- bundle product area -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="bundle-product-table table-responsive">
                                    <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                        <thead>
                                            <tr class="">
                                                <th>Product id</th>
                                                <th>Product Name</th>
                                                <th>Unit of Measurement</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody class="item_add_area_bundle_product" id="auto_generated_tr">

                                        </tbody>
                                    </table>
                                </div><!-- end .bundle-product-table -->
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <!-- create bundle-product area -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-5 col-xl-4">
                                        <div class="form-group">
                                            <label for="bundle_name">Bundle Name <span style="color: red">*</span></label>
                                            <input type="text" name="bundle_name" id="bndl_name" class="form-control"
                                                placeholder="Enter bundle name." onkeyup="bundleNameCheck()">
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <!-- buttons-area -->
                                        <div id="submitButton">
                                            <button id="subBtn" class="btn index-btn" type="submit">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form><!-- end from -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            // select2 configuration
            $('.single-select').select2({
                width: '100%'
            });

            //
            $("#submitButton").hide();
            $("#add_bundle_product").hide();
            bundleNameCheck();
            isEmptyProductAndUom();
        });
    </script>

    <!-- validation -->
    <script>
        function bundleNameCheck() {
            if ($("#bndl_name").val().length > 2) {
                $("#submitButton").show();
            } else {
                $("#submitButton").hide();
            }

        }
    </script>

    <!-- validation functions -->
    <script>
        function isEmptyProductAndUom() {
            if ($("#product_id").val() > 0 && $("#select_uom").val() > 0) {
                $("#add_bundle_product").show()
            } else {
                $("#add_bundle_product").hide();
            }
        }
    </script>

    <!-- adding bundle tr on button click -->
    <script>
        $(document).ready(function() {
            let count = 0;
            $('#add_bundle_product').on('click', function() {
                let uom_value = $("#select_uom option:selected").val();
                let uom_name = $("#select_uom option:selected").text();

                let product_id = $("#product_id").val();
                let product_name = $("#product_id option:selected").text();

                $(".item_add_area_bundle_product").append(
                    `<tr>
                        <td>
                            <input class="form-control" value="${product_id}" readonly>   
                            <input type="hidden" class="form-control" name="product_id[]" value="${product_id}"> 
                        </td>

                        <td>  
                            <input class="form-control" value="${product_name}" readonly>   
                            <input type="hidden" name="product_name[]" value="${product_name}"">  
                        </td> 

                        <td>
                            <input class="form-control" value="${uom_name}" readonly>   
                            <input type="hidden" name="uom_id[]" value="${uom_value}">
                        </td> 

                        <td><a href="#" class="btn btn btn-danger item_remove btn-xs">x</a></td> 
                    </tr>`
                );

                // Remove Item
                $(document).on('click', '.item_remove', function() {
                    $(this).parent().parent().remove();
                });

                // clear dropdown field
                $("#product_id").select2("val", " ");
                $('#product_id').select2({
                    placeholder: "Select Product"
                });

                $("#select_uom").select2("val", " ");
                $('#select_uom').select2({
                    placeholder: "Select UOM"
                });

            })

        });
    </script>


@endpush
