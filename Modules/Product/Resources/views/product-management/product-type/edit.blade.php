@extends('dboard.index')
@section('title','Update Product Type')
@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('types.update',$product_type->id) }}" method="POST">
        @csrf
        <input name="_method" type="hidden" value="PUT">

        <div class="tile">
            <div class="tile-body">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Update Product Type</h2>
                    </div><!-- end .col-md-6 -->

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('types.index')}}">Back </a>
                    </div><!-- end .col-md-6 -->
                </div><!-- end .row -->
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="category_name" class="forget-form">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $product_type->name }}">
                        </div><!-- end form-group -->
                    </div><!-- end .col-md-6 -->
        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status" class="forget-form">Select Status</label>
                            <select class="js-example-basic-single form-control" name="status">
                                <option value="1" {{ $product_type->status == 1 ? 'selected' : '' }}>Active</option>
                                <option value="0" {{ $product_type->status == 0 ? 'selected' : '' }}>InActive</option>
                            </select>
                        </div><!-- end form-group -->
                    </div><!-- end .col-md-6 -->
                </div><!-- .row -->
                <hr>
        
                <div class="row text-right">
                    <div class="col-md-12">
                        <button class="btn create-btn" type="submit">Update</button>
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
            </div>
        </div>
    </form><!-- end form -->
@endsection
