@extends('dboard.index')

@section('title','Product Type List')

@push('styles')

<style>

</style>
@endpush

@section('dboard_content')


<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <div class="row justify-content-center align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Product Type List</h2>
          </div>

          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{ route('types.create') }}">Add Product Type</a>
          </div>
        </div>
        <hr>

        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="sampleTable">
            <thead>
              <tr>
                <th width="10%">ID</th>
                <th width="50%">Product Type</th>
                <th width="30%">Status</th>
                <th width="10%">Action</th>
              </tr><!-- end tr -->
            </thead><!-- end thead -->

            <tbody>
              @if(isset($product_types))
              @foreach($product_types as $key=>$row)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $row->name }}</td>
                <td>
                  @if( $row->status == 1 )
                  <div class="badge badge-success m-1 p-2">
                    Active
                  </div>
                  @else
                  <span class="badge badge-danger m-1 p-2">Inactive</span>
                  @endif
                </td>

                <td>
                  <div class="d-md-flex">
                    <a href="{{route('types.edit', $row->id )}}" class="btn edit-btn mr-3"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <form id="delete_form{{$row->id}}" method="POST" action="{{ route('types.destroy',$row->id) }}" onclick="return confirm('Are you sure?')">
                      @csrf
                      <input name="_method" type="hidden" value="DELETE">
                      <button class="btn edit-btn" type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    </form>
                  </div>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody><!-- end tbody -->
          </table><!-- end table -->
        </div><!-- end .table-responsive -->
      </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
  </div><!-- end .row -->



  @endsection


  @push('post_scripts')
  <!-- Page specific javascripts-->
  <!-- Data table plugin-->
  <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript">
    $('#sampleTable').DataTable();
  </script>
  @endpush