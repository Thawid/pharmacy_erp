@extends('dboard.index')
@section('title','Create Product Type')



@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('types.store') }}" method="POST">
        @csrf
        <div class="tile">
            <div class="tile-body">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-6">
                        <h1 class="title-heading">Create Product Type</h1>
                    </div>

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('types.index')}}">Back</a>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                            <div class="form-group">
                                <label for="category_name" class="forget-form">Name*</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Write product type here">
                            </div><!-- end form-group -->
                    </div><!-- end .col-md-6 -->
        
                    <div class="col-md-6">
                        <div class="form-group">
                            @include('product::shares.status')
                        </div><!-- end form-group -->
                    </div><!-- end .col-md-6 -->
                </div><!-- .row -->
                <hr>

                <div class="row text-right">
                    <div class="col-md-12">
                            <button class="btn create-btn" type="submit">Submit</button>
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
            </div>
        </div>
    </form><!-- end form -->
@endsection

@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function (){
            $(".multiple-select").select2({ width: '100%' });
            $(".single-select").select2({ width: '100%' });
        });
    </script>
@endpush
