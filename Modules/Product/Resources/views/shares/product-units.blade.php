<label for="unit_type" class="forget-form">Select Unit Type <span class="text-danger">*</span></label>
<select class="form-control select2 single-select" name="{{ $field ?? 'unit_id'}}" id="unit_type" onchange="varSpecAddValidation(); generalInfo();">
    <option value="" disabled selected>Select Unit Type</option>
    @foreach($product_units as $product_unit)
        <option value="{{$product_unit->id}}" @if(old('unit_id') == $product_unit->id) selected="selected" @endif>{{$product_unit->unit_name}}</option>
    @endforeach
</select>
