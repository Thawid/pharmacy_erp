<label for="status" class="forget-form">Select Status</label>
<select class="form-control select2 single-select" name="status">
    <option value="1" @if (old('status') == '1') selected="selected" @endif>Active</option>
    <option value="0" @if (old('status') == '0') selected="selected" @endif>InActive</option>
</select>
