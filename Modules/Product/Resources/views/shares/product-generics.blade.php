<label for="product_generic" class="forget-form">Product Generic</label>
<select class="form-control select2 single-select" name="product_generic_id" id="product_generic_id" onchange="generalInfo();">
    <option value="">Select Product Generic</option>
    @foreach($product_generics as $product_generic)
        <option value="{{$product_generic->id}}" @if (old('product_generic_id') == $product_generic->id) selected="selected" @endif>{{$product_generic->name}}</option>
    @endforeach
</select>
