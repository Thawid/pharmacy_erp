<label for="product_category" class="forget-form"> Product Type*</label>
<select class="form-control select2 product-type single-select" name="{{ $field?? 'product_category_id' }}" id="product_category">
    <option value="">Select Product Type</option>
    @foreach($product_categories as $product_category)
        <option value="{{$product_category->id}}"
                @if (old('product_category_id') == $product_category->id) selected="selected" @endif>{{$product_category->name}}</option>
    @endforeach
</select>
