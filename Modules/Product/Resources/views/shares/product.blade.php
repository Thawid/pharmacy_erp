<label for="product" class="forget-form">Search By Product</label>
<select class="form-control select2 product-type single-select" name="name">
    <option value="">Select Product</option>
    @foreach ($products as $product)
        <option value="{{ $product->id }}">{{ $product->product_name }}</option>
    @endforeach
</select>
