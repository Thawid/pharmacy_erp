<label for="product_type" class="forget-form">Product Type <span class="text-danger">*</span></label>
<select class="form-control select2 product-type single-select" name="{{ $field ?? 'product_type_id' }}"
    id="product_type" onchange="generalInfo()" >
    <option value="" disabled selected>Select Product Type</option>
    @foreach ($product_types as $product_type)
        <option value="{{ $product_type->id }}"
            {{-- @if (old('product_type_id') == $product_type->id) selected="selected" @endif--}}
            >{{ $product_type->name }}</option>
    @endforeach
</select>
