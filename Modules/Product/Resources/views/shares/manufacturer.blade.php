<label for="manufacturers" class="forget-form">Product Type*</label>
<select class="form-control select2 product-type single-select" name="{{ $field?? 'manufacturer_id' }}" id="manufacturer_id">
    <option value selected>Select Product Type</option>
    @foreach($manufacturers as $manufacturer)
        <option value="{{$manufacturer->id}}"
                @if (old('manufacturer_id') == $manufacturer->id) selected="selected" @endif>{{$manufacturer->name}}</option>
    @endforeach
</select>
