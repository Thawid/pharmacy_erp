<label for="variant" class="forget-form">Select Property Name</label>
<select class="form-control single-select" name="{{ $field ?? 'variant_id'}}" id="variant" onchange="varSpecAddValidation()">
    <option value="">Select Property Name</option>
    @foreach($variants as $variant)
        <option value="{{$variant->id}}">{{$variant->name}}</option>
    @endforeach
</select>
