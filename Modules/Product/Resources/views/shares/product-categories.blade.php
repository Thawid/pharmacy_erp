<label class="forget-form">Product Category <span class="text-danger">*</span></label>
<select class="form-control select2 single-select" name="parent_id">
    <option value="" selected disabled>Select Product Category</option>
    @foreach($product_categories as $product_category)
      <option value="{{$product_category->id}}" @if (old('parent_id') == $product_category->id) selected="selected" @endif>{{$product_category->name}}</option>
    @endforeach
</select>
