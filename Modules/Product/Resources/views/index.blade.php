@extends('product::layouts.master')

@section('content')
    <h1>Hello World</h1>

    <p>
        This view is loaded from module: {!! config('product.name') !!}
    </p>

    <h1>View share files </h1>
    @include('product::shares.product-categories')
@endsection
