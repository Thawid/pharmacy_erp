<?php
use Illuminate\Support\Facades\Route;

Route::middleware('auth')->prefix('product')->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('product.dashboard');
    Route::resource('/category', 'ProductCategoryController')->middleware('product');
    Route::get('/reports', 'ProductReportController@index')->name('product.report');
    Route::post('/reports/getProductReport', 'ProductReportController@getProductReport')->name('get.product.report');
    Route::get('/reports/export', 'ProductReportController@export')->name('report.export');
    // Route::get('/reports/test', 'ProductReportController@test')->name('report.test');
    Route::get('/get-product-category/{id}', 'ProductCategoryController@getProductCategory')->middleware('product');
    Route::get('/get-product-generic/{id}', 'ProductGenericController@getProductGeneric')->middleware('product');
    Route::get('/get-product-variant/{id}', 'VariantController@getProductVariant')->middleware('product');
    Route::get('/get-product-sub-category/{id}', 'ProductCategoryController@getProductSubCategory')->middleware('product');
    Route::get('/get-variant-property/{id}', 'ProductCategoryController@getVariantProperty')->middleware('product');
    Route::post('/save-variant-temp-data', 'TempProductController@storeDummyData')->name('save_temp_data')->middleware('product');
    Route::get('/get-temp-data', 'TempProductController@getDummyData')->middleware('product');
    Route::resource('/types', 'ProductTypeController')->middleware('product');
    Route::resource('/units', 'ProductUnitController')->middleware('product');
    Route::resource('/products', 'ProductController')->middleware('product');
    Route::get('/check-product-name', 'ProductController@productNameCheck')->middleware('product');
    Route::get('product-reports', 'ProductController@getProducts')->name('product.reports');
    Route::resource('/generics', 'ProductGenericController')->middleware('product');
    Route::resource('/variants', 'VariantController')->middleware('product');
    Route::resource('/variant_properties', 'VariantPropertyController')->middleware('product');
    Route::resource('/manufacturers', 'ManufacturerController')->middleware('product');
    Route::resource('/regular', 'RegularBundleController')/*->middleware(['product','store'])*/;
    Route::resource('/advance', 'AdvanceBundleController')->middleware('product');
    Route::resource('/manage-store-product', 'ManageStoreProductController')->middleware('hq');
    Route::get('/store-products', 'ManageStoreProductController@storeProductList')->name('store.products.list')->middleware('hq');
    Route::post('/setup-stock-alert-save', 'ManageStoreProductController@setupStockAlertSave')->name('setup.stock.alert.save')->middleware('hq');
    Route::get('/active-products/{id}', 'ManageStoreProductController@activeProducts')->name('active.products')->middleware('hq');
    Route::get('/get-product-vp/{id}', 'ManageStoreProductController@getProductVariations')->middleware('hq');
    Route::resource('/bulk-store-product', 'BulkStoreProductManageController')->middleware('product');
    Route::get('/check-manage-store/{id}', 'BulkStoreProductManageController@checkManageStore')->middleware('hq');
    Route::resource('/uom', 'UOMController')->middleware('product');

    // Import and export product from Excel Route
    Route::resource('import-product', 'ProductImportController')->middleware('product');
});
