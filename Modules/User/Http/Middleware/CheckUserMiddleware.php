<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;


class CheckUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    

    public function handle(Request $request, Closure $next)
    {
        
        if(systemModuleAuth()){
            return $next($request);
        }
       return abort(403,'No Permission');
        
    }
}
