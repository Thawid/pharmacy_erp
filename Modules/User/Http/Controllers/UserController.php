<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Http\Requests\UpdateUserRequest;
use Modules\User\Repositories\UserRepositoryInterface;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;


class UserController extends Controller
{
    private $userRepository;
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function dboard()
    {
        return view('user::dboard');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

       $users = $this->userRepository->index();
       return view('user::index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');
        
        $departments= userDomain();
        $roles      = userRole();
        $priorities = userWeight();
       
       
        return view('user::create', compact('roles','departments','priorities'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateUserRequest $request)
    {
          Gate::authorize('hasCreatePermission');
          $this->userRepository->store($request);
          return redirect()->route('user')->with('success','User has been stored successfully');
  
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        Gate::authorize('hasReadPermission');
        $user=$this->userRepository->show($id);
        return view('user::show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $user=$this->userRepository->edit($id);
        $departments= userDomain();
        $roles      = userRole();
        $priorities = userWeight();
        //Separate User Access//
        $userAccess = explode('_',$user->access);
        return view('user::edit',compact('user','roles','userAccess','departments','priorities'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateUserRequest $request, $userID)
    {   
            Gate::authorize('hasEditPermission');
            $this->userRepository->update($request,$userID);
            return redirect()->route('user')->with('success','User has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $distroy= $this->userRepository->destroy($id);
        return redirect()->route('user')->with('success','User Status has been Changed successfully');
    }

    
}
