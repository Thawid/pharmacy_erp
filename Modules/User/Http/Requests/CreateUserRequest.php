<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;
use App\Rules\CheckWhiteSpace;

class CreateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:50',
            'email' => 'required|email:rfc,dns|unique:users',
            'domain' => 'required',
            'role' => 'required',
            'weight' => 'required',
            // 'access' => 'required',
            'status' => 'required',
            'password' => ['required','max:15',new CheckWhiteSpace,Password::min(6)->letters()->mixedCase()->numbers()->symbols()],
            'password_confirm' => ['required','same:password']
        ];
           
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
