<?php


namespace Modules\User\Repositories;

use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Http\Requests\UpdateUserRequest;
use App\Models\User;


class UserRepository implements UserRepositoryInterface
{
    public function index()
    {
        return User::all();
    }

    public function create()
    {

    }

    public function store(CreateUserRequest $request)
    {
        $validatedData=$request->validated();
        if($validatedData){
            $checked =implode('_',$request->access);
            $validatedData['access']=$checked;
            $passSystem=$request->password;
            $validatedData['password']=bcrypt($passSystem);
            User::create($validatedData);
          
         }
       
    }
    public function show($id)
    {
       return User::where('id',$id)->first();
    }
    public function edit($id)
    {
        return User::findOrFail($id);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $validatedData=$request->validated();
        if($validatedData){
            $checked =implode('_',$request->access);
            $validatedData['access']=$checked;
            
            User::where('id',$id)->update($validatedData);
         }
        
    }

    public function destroy($id)
    {
        $checkStatus = User::Where('id',$id)->first();
        User::Where('id',$id)->update(['status'=>!$checkStatus->status]);
    }
   

}
