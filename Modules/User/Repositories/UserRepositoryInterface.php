<?php

namespace Modules\User\Repositories;

use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Http\Requests\UpdateUserRequest;

interface UserRepositoryInterface
{
    public function index();

    public function create();

    public function store(CreateUserRequest $request);

    public function show($id);

    public function edit($id);

    public function update(UpdateUserRequest $request, $id);

    public function destroy($id);

}
