@extends('dboard.index')

@section('title','User Data')

@section('dboard_content')

<div class="row ">
    <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="tile">
            <div class="tile-body">
                <div class="card mb-2 text-white ">
                    <div class="card-header">
                        <h3 class="title-heading">{{$user->name}}'s Profile</h3>
                    </div>
                    <div class="card-body">
                      <blockquote class="card-blockquote">
                          <table class="table table-hover " >
                            
                            <tbody>
                              <tr>
                              
                                <th>Email</th>
                                <td>{{$user->email}}</td>
                               
                              </tr>
                              <tr>
                              
                                <th>Department</th>
                                <td class="text-uppercase">{{$user->domain}}</td>
                                
                              </tr>
                              <tr>
                                
                                <th>Role</th>
                                <td>{{$user->role}}</td>
                               
                              </tr>
                              <tr>
                                
                                <th>Priority</th>
                                <td>{{$user->weight}}</td>
                               
                              </tr>
                              <tr>
                                
                                <th>Access</th>
                                <td>
                                    @foreach ( explode("_",$user->access) as $value)
                          @if ($value == "C")
                          <span class="badge badge-primary m-1 p-2">Create</span>
                          @endif
                          @if ($value == "R")
                          <span class="badge badge-info m-1 p-2">Read</span>
                          @endif
                          @if ($value == "E")
                          <span class="badge badge-secondary m-1 p-2">Edit</span>
                          @endif
                          @if ($value == "D")
                          <span class="badge badge-danger m-1 p-2">Delete</span>
                          @endif
                          
                        @endforeach  
                        @if ($user->access == "N_N_N_N")
                          <span class="badge badge-warning m-1 p-2">No Permission</span>
                          @endif  
                                </td>
                               
                              </tr>
                              <tr>
                                
                                <th>Status</th>
                                <td>{{$user->status==1?"Active":"Inactive"}}</td>
                               
                              </tr>
                            </tbody>
                          </table>
                        
                      </blockquote>
                    </div>
                    <div class="card-footer text-right">
                        <a class="btn index-btn" href="{{route('user')}}">Back	</a>
                    </div>
                </div>



            </div>
          </div>
        </div>
        <div class="col-md-2"></div>
      </div>


@endsection
@push('post_scripts')
{{--<!-- Page specific javascripts-->--}}
{{--    <!-- Data table plugin-->--}}
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush

