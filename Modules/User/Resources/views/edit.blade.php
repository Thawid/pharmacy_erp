@extends('dboard.index')

@section('title','User Update Form')

@section('dboard_content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
          <div class="tile-body">
                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                      <h2 class="title-heading">Update User Account</h2>
                    </div>
                    <div class="col-md-6 text-right">
                      <a class="btn index-btn" href="{{route('user')}}">Back</a>
                    </div>
                </div><!-- end.row -->
                <hr>

                <!-- body -->
                <form action="{{route('user.update',$user->id)}}" method="POST">
                @csrf
                @method('PATCH')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                              <label class="control-label">Name</label>
                              <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" placeholder="Enter full name" value="{{ $user->name }}">
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div><!-- end.for-group -->

                        <div class="form-group">
                          <label for="exampleSelect1">Department</label>
                          <select class="form-control @error('domain') is-invalid @enderror" name="domain" id="exampleSelect1" value="{{ old('domain') }}">
                            <option selected="true" disabled="disabled">Select Department</option>
                            @foreach ($departments as $key => $department )
                            <option value="{{$department}}" @if ($department == $user->domain) selected @endif>{{$department}}</option>
                            @endforeach
                            
                          </select>
                          @error('domain')
                           <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div><!-- end.form-group -->

                        <div class="form-group">
                          <label for="exampleSelect1">Priority Level</label>
                          <select class="form-control @error('weight') is-invalid @enderror" name="weight" id="exampleSelect1" value="{{ old('weight') }}">
                            <option selected="true" disabled="disabled">Select Priority</option>
                            @foreach ($priorities as $key => $priority )
                            <option value="{{$priority}}" @if ($priority == $user->weight) selected @endif>{{$priority}}</option>
                            @endforeach
                            
                          </select>
                          @error('weight')
                           <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div><!-- end.form-group -->

                        <div class="form-group">
                              <label class="control-label">Status</label>
                              <div class="form-check">
                                <label class="form-check-label">
                                <div class="animated-radio-button">
                                   <label>
                                    <input type="radio" name="status" value=1 {{ ($user->status==true)? "checked" : "" }} ><span class="label-text">Active</span>
                                  </label>
                              </div>
                                </label>
                              </div>
                              <div class="form-check">
                                <label class="form-check-label">
                                <div class="animated-radio-button">
                                   <label>
                                    <input type="radio" name="status" value=0 {{ ($user->status==false)? "checked" : "" }} ><span class="label-text">Inactive</span>
                                  </label>
                                </div>
                                </label>
                              </div>
                              @error('status')
                              <div class="alert alert-danger">{{ $message }}</div>
                             @enderror
                        </div><!-- end.form-group -->
                      </div><!-- end.col-md-6 -->

                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Email</label>
                          <input class="form-control  @error('email') is-invalid @enderror" type="email" name="email" placeholder="Enter email address" value="{{ $user->email }}" >
                          @error('email')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div><!-- end.form-group -->
                        
                        <div class="form-group">
                          <label for="exampleSelect1">Role</label>
                          <select class="form-control @error('role') is-invalid @enderror" name="role" id="exampleSelect1" >
                            <option selected="true" disabled="disabled">Select Role</option>
                            @foreach ($roles as $key => $role )
                               <option value="{{$role}}" @if ($role == $user->role) selected @endif>{{$role}}</option>
                            @endforeach
                          </select>
                          @error('role')
                           <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div><!-- end.form-group -->
                 
                        <div class="form-group">
                            <label class="control-label">User Permission</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                <div class="animated-checkbox"> 
                                <label>
                                  <input type="hidden" name="access['c']" value="N">
                                  <input type="checkbox" name="access['c']" value="C" {{ ($userAccess[0]=="C")? "checked" : "" }}><span class="label-text">Create</span>
                                </label>
                              </div>
                              </label>
                            </div><!-- end.form-check -->

                            <div class="form-check">
                              <label class="form-check-label">
                              <div class="animated-checkbox">
                              <label>
                                <input type="hidden" name="access['r']" value="N">
                                <input type="checkbox" name="access['r']" value="R" {{ ($userAccess[1]=="R")? "checked" : "" }}><span class="label-text">Read</span>
                              </label>
                            </div>
                              </label>
                            </div><!-- end.form-check -->

                            <div class="form-check">
                              <label class="form-check-label">
                              <div class="animated-checkbox">
                              <label>
                                <input type="hidden" name="access['e']" value="N">
                                <input type="checkbox" name="access['e']" value="E" {{ ($userAccess[2]=="E")? "checked" : "" }}><span class="label-text">Edit</span>
                              </label>
                            </div>
                              </label>
                            </div><!-- end.form-check -->

                            <div class="form-check">
                              <label class="form-check-label">
                              <div class="animated-checkbox">
                              <label>
                                <input type="hidden" name="access['d']" value="N">
                                <input type="checkbox" name="access['d']" value="D" {{ ($userAccess[3]=="D")? "checked" : "" }}><span class="label-text">Delete</span>
                              </label>
                            </div>
                              </label>
                            </div>
                              @error('access')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div><!-- end.form-group -->
                        &nbsp;
                    </div><!-- end.col-md-6 -->
                </div><!-- end.row -->
                <hr>

                <div class="row text-right">
                  <div class="col-md-12">
                      <button class="btn create-btn" type="submit">Update Account</button>
                  </div>
                </div><!-- end.row -->
                </form>

            </div><!--end.tile-body -->
      </div><!-- end.tile -->
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->
@endsection