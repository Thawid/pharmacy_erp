@extends('dboard.index')

@section('title','User Data')

@section('dboard_content')

<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">User Details</h2>
          </div>
          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{route('user.create')}}">Add User</a>
          </div>
          @endcan
        </div><!-- end.row -->
        <hr>

        <!-- body -->
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive table-responsive-md table-responsive-lg table-responsive-sm table-responsive-xl">
              <table class="table table-hover table-bordered table-responsive-md table-responsive-lg table-responsive-sm table-responsive-xl" id="sampleTable">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Access</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($users as $user)
                  <tr>

                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->role}}</td>


                    <td>
                      <div class="d-flex justify-content-center align-items-center">
                        @foreach ( explode("_",$user->access) as $value)
                        @if ($value == "C")
                        <span class="badge badge-primary m-1 p-2">Create</span>
                        @endif
                        @if ($value == "R")
                        <span class="badge badge-info m-1 p-2">Read</span>
                        @endif
                        @if ($value == "E")
                        <span class="badge badge-secondary m-1 p-2">Edit</span>
                        @endif
                        @if ($value == "D")
                        <span class="badge badge-danger m-1 p-2">Delete</span>
                        @endif

                        @endforeach
                        @if ($user->access == "N_N_N_N")
                        <span class="badge badge-warning m-1 p-2">No Permission</span>
                        @endif
                      </div>
                    </td>
                    <td>
                      <div class="d-flex justify-content-center align-items-center">
                        @if ($user->status == 1)
                        <span class="badge badge-success m-1 p-2">Active</span>
                        @else
                        <span class="badge badge-danger m-1 p-2">Inactive</span>
                        @endif
                      </div>
                    </td>


                    <td>
                      <div class="d-flex align-items-center justify-content-around">
                        @can('hasReadPermission')
                        <a class="btn details-btn" href="{{route('user.show',$user->id)}}"><i class="fa fa-lg fa-eye"></i></a>
                        @endcan

                        @can('hasEditPermission')
                        <a class="btn edit-btn" onclick="return confirm('Are you sure?')" href="{{route('user.edit',$user->id)}}"><i class="fa fa-lg fa-pencil"></i></a>
                        @endcan
                        @can('hasDeletePermission')
                        <a class="btn edit-btn" onclick="return confirm('Are you sure?')" href="{{route('user.destroy',$user->id)}}"><i class="fa fa-refresh"></i></a>
                        @endcan
                      </div>
                    </td>
                  </tr>
                  @empty
                  <h2>No User Found</h2>
                  @endforelse


                </tbody>
              </table>
            </div> <!-- table-responsive -->
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->

      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->


@endsection
@push('post_scripts')
{{--<!-- Page specific javascripts-->--}}
{{-- <!-- Data table plugin-->--}}
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $('#sampleTable').DataTable();
</script>

@endpush