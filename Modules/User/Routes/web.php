<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth','hq'])->prefix('user')->group(function() { 
    Route::get('/', 'UserController@index')->name('user'); 
    Route::get('/create', 'UserController@create')->name('user.create'); 
    Route::post('/store', 'UserController@store')->name('user.store');
    Route::get('/show/{userID}', 'UserController@show')->name('user.show');
    Route::get('/edit/{userID}', 'UserController@edit')->name('user.edit');
    Route::patch('/update/{userID}', 'UserController@update')->name('user.update');
    Route::get('/destroy/{userID}', 'UserController@destroy')->name('user.destroy');
});

