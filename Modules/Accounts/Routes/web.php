<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth')->prefix('accounts')->group(function () {
  // Route::get('/', 'AccountsController@index');
  Route::resource('/sales-invoice', 'SalesInvoiceController')->middleware('accounts');
  Route::get('/archive-sales-invoice', 'SalesInvoiceController@storeInvoiceArchive')->name('store.invoice.archive')->middleware('accounts');
  Route::resource('/accounts-vendor-invoice', 'VendorInvoiceController')->middleware('hqoraccount');
  Route::get('/archive-vendor-invoice', 'VendorInvoiceController@vendorInvoiceArchive')->name('vendor.invoice.archive')->middleware('accounts');
  Route::get('/vendor-payment/{id}', 'VendorInvoiceController@payment')->name('vendor.payment')->middleware('accounts');
  Route::get('/vendor-payment-lists/', 'VendorInvoiceController@vendorPaymentList')->name('vendor.payment.lists')->middleware('hqoraccount');
  Route::post('/store/vendor-payment', 'VendorInvoiceController@storeVendorPayment')->name('store.vendor.payment')->middleware('accounts');
  Route::get('/vendor-invoice-pending', 'VendorInvoiceController@pendingVendorInvoice')->name('vendor-invoice.pending')->middleware('accounts');
  Route::get('/purchase-order-lists', 'VendorInvoiceController@purchaseOrderList')->name('vendor.purchase-order-lists')->middleware('accounts');
  Route::resource('/vendor-payment', 'VendorPaymentController')->middleware('accounts');
  Route::post('/store-invoice-search-results', 'SalesInvoiceController@searchResult')->name('sales.store.invoice')->middleware('accounts');
  Route::post('/vendor-invoice-search-results', 'VendorInvoiceController@searchResult')->name('vendor.search.invoice')->middleware('accounts');
  Route::resource('/other-payment', 'OtherPaymentController')->middleware('accounts');
  Route::resource('/store-deposit', 'StoreDepositController')->middleware('store');
  Route::resource('/deposit-receive', 'DepositReceiveController')->middleware('accounts');
  Route::get('/vendor-purchase-order-details/{id}', 'VendorInvoiceController@purchaseOrderDetails')->name('vendor.purchase.order.details')->middleware('vendor');

  /*vendor payment summary report*/
  Route::get('/report', 'ReportController@index')->name('search.payment.summery')->middleware('hqoraccount');
  Route::get('/vendor-payment-report', 'ReportController@vendorPaymentSummery')->name('vendor.payment.summery')->middleware('hqoraccount');

  /*Daily Payment report*/
  Route::get('/daily-payment-report', 'ReportController@dailyPaymentReport')->name('daily.payment.report')->middleware('accounts');
  Route::get('/daily-payment-report-result', 'ReportController@dailyPaymentReportResults')->name('daily.payment.report.result')->middleware('accounts');


  /*Expense and transaction*/
  Route::resource('/expense-head', 'ExpenseHeadController');
  Route::resource('/expenses', 'ExpenseController');
  Route::resource('/transactions', 'TransactionController');
  Route::get('/transactions-search', 'TransactionController@searchResult')->name('transaction.search');

  /*Store Deposit Receive Report*/
  Route::get('/store-deposit-report', 'ReportController@storeDepositReport')->name('store.deposit.report')->middleware('accounts');
  Route::get('/store-deposit-report-result', 'ReportController@storeDepositReportResults')->name('store.deposit.report.result')->middleware('accounts');

  /*new features*/
    Route::get('/account-setting','AccountsSettingController@index')->name('account-setting');
    Route::post('/account-setting','AccountsSettingController@update')->name('update-account-setting');
    Route::resource('/accounts', 'AccountController');
    Route::get('/types', 'AccountsTypeController@types')->name('account-type');
    Route::get('/types/{id}', 'AccountsTypeController@type')->name('types.get');

    Route::get('/account-closings','AccountClosingController@index')->name('account-closings');
    Route::get('/create-account-closing','AccountClosingController@create')->name('create-account-closing');
    Route::get('/check-account-closing/{year}','AccountClosingController@check')->name('check-account-closing');
    Route::post('/create-account-closing','AccountClosingController@store')->name('store-account-closing');

    Route::get('/account-statements','AccountStatementController@index')->name('account-statements');
    Route::post('/get/account-statements','AccountStatementController@getStatements')->name('get-account-statements');
    Route::get('/download-account-statements','AccountStatementController@downloadStatements')->name('download-account-statements');

    Route::get('/ledger/{id}','LedgerController@getLedger')->name('show-ledger');
    Route::get('/download-ledger/{id}','LedgerController@download')->name('download-ledger');

    // reports
    Route::get('report/account-statements', 'ReportController@accountStatement')->name('acc-statement');

    Route::get('report/trial-balance', 'ReportController@trialBalance')->name('trial-balance');
  /*end new features*/
});
