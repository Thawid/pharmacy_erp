<?php

namespace Modules\Accounts\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Vendor\Entities\Vendor;
use Modules\Accounts\Entities\VendorInvoice;
use Modules\Accounts\Entities\VendorPayment;

class VendorInvoiceController extends Controller
{
    public function index()
    {
        $vendors = Vendor::where('status', 1)->get(['id', 'name']);
        $vendor_invoices = DB::table('vendor_invoices')
            ->leftJoin('vendors', 'vendors.id', '=', 'vendor_invoices.vendor_id')
            ->leftJoin('purchase_orders', 'purchase_orders.id', '=', 'vendor_invoices.purchase_order_id')
            ->whereDate('vendor_invoices.created_at', Carbon::today())
            ->where('vendor_invoices.status', '=', 'Pending')
            ->select('vendor_invoices.id','vendor_invoices.vendor_id', 'vendor_invoices.invoice_no', 'vendor_invoices.status', 'vendor_invoices.attachment', 'vendor_invoices.created_at', 'vendors.name', 'vendor_invoices.purchase_order_no')
            ->orderBy('vendor_invoices.created_at', 'desc')
            ->get();

        return view('accounts::accounts-n-finance-management.vendor-invoice.index', compact('vendor_invoices', 'vendors'));
    }

    public function vendorInvoiceArchive()
    {
        $vendors = Vendor::where('status', 1)->get(['id', 'name']);
        $vendor_invoices = DB::table('vendor_invoices')
            ->leftJoin('vendors', 'vendors.id', '=', 'vendor_invoices.vendor_id')
            ->leftJoin('purchase_orders', 'purchase_orders.id', '=', 'vendor_invoices.purchase_order_id')
            ->where('vendor_invoices.status', '=', 'Pending')
            ->select('vendor_invoices.id','vendor_invoices.vendor_id', 'vendor_invoices.invoice_no', 'vendor_invoices.status', 'vendor_invoices.attachment', 'vendor_invoices.created_at', 'vendors.name', 'vendor_invoices.purchase_order_no')
            ->orderBy('vendor_invoices.created_at', 'desc')
            ->get();

        return view('accounts::accounts-n-finance-management.vendor-invoice.invoice-archive', compact('vendor_invoices', 'vendors'));
    }

    public function pendingVendorInvoice()
    {
        
        return view('accounts::accounts-n-finance-management.vendor-invoice.pending');
    }

    public function payment($id)
    {
        $vendorInvoice = VendorInvoice::where('id',$id)->where('status', '=', 'Pending')->first();

        return view('accounts::accounts-n-finance-management.vendor-invoice.payment',compact('vendorInvoice'));
    }


    public function storeVendorPayment(Request $request)
    {
        $vendor_payment = new VendorPayment();
        $vendor_payment->vendor_invoice_id = $request->vendor_invoice_id;
        $vendor_payment->invoice_date = $request->invoice_date;
        $vendor_payment->invoice_no = $request->invoice_number;
        $vendor_payment->payment_type = $request->payment_type;
        $vendor_payment->status = $request->payment_type;
        $vendor_payment->payment_no = rand(1000000,10000000);
        $vendor_payment->pay_amount = $request->pay_amount;
        $vendor_payment->payment_note = $request->payment_note;
        $vendor_payment->save();
        return redirect()->route('vendor.payment.lists')->with('success','Vendor payment has been created successfully');
    }

    public function vendorPaymentList()
    {
        $vendor_payment_lists = VendorPayment::where('payment_type','=', 'Partial')->get();
        return view('accounts::accounts-n-finance-management.vendor-invoice.vendor-payment-list',compact('vendor_payment_lists'));
    }

    public function show($id)
    {
        $vendor_invoices = DB::table('vendor_invoices')
            ->leftJoin('vendors', 'vendors.id', '=', 'vendor_invoices.vendor_id')
            ->leftJoin('purchase_orders', 'purchase_orders.id', '=', 'vendor_invoices.purchase_order_id')
            ->select('vendor_invoices.id', 'vendor_invoices.invoice_no', 'vendor_invoices.status', 'vendor_invoices.created_at', 'vendors.name', 'vendor_invoices.purchase_order_no')
            ->orderBy('vendor_invoices.created_at', 'desc')
            ->where('vendor_invoices.vendor_id',$id)
            ->where('vendor_invoices.status','=', 'Pending')
            ->get();
        return view('accounts::accounts-n-finance-management.vendor-invoice.show',compact('vendor_invoices'));
    }

    public function searchResult(Request $request)
    {
        $vendors = Vendor::where('status', 1)->get(['id', 'name']);
        $to_date = new Carbon($request->created_at);
        $req_date = $to_date->format('Y-m-d');
        $vendor_id = $request->vendor_id;
        $status = $request->status;
        $vendor_invoices = DB::table('vendor_invoices')
            ->leftJoin('vendors', 'vendors.id', '=', 'vendor_invoices.vendor_id')
            ->leftJoin('purchase_orders', 'purchase_orders.id', '=', 'vendor_invoices.purchase_order_id')
            ->orWhere('vendor_invoices.vendor_id', $vendor_id)
            ->orWhere('vendor_invoices.status', $status)
            ->orWhere('vendor_invoices.created_at', 'LIKE', "%{$req_date}%")
            ->select('vendor_invoices.id','vendor_invoices.vendor_id', 'vendor_invoices.invoice_no', 'vendor_invoices.status', 'vendor_invoices.attachment','vendor_invoices.purchase_order_no', 'vendor_invoices.created_at', 'vendors.name')
            ->orderBy('vendor_invoices.created_at','desc')
            ->get();
       return view('accounts::accounts-n-finance-management.vendor-invoice.index', compact('vendor_invoices', 'vendors'));
    }

    public function purchaseOrderList()
    {
        $purchase_order_list = PurchaseOrder::with(['hq_requisition','vendor'])->orderBy('id', 'DESC')->get();
        return view('accounts::accounts-n-finance-management.vendor-invoice.purchase-order-list', compact('purchase_order_list'));
    }

    public function purchaseOrderDetails($id)
    {
        $order_details = PurchaseOrder::with([
                    'purchase_order_details','vendor',
                    'purchase_order_details.product',
                    'purchase_order_details.unit',
                    'purchase_order_details.uom'])
            ->findOrFail($id);
        return view('accounts::accounts-n-finance-management.vendor-invoice.purchase-order-details', compact('order_details'));
    }
}
