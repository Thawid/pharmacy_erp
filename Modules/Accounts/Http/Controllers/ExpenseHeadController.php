<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Modules\Accounts\Entities\ExpenseHead;

class ExpenseHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $expense_heads = ExpenseHead::where('status', 1)->get(['id','name','created_at']);
        return view('accounts::accounts-n-finance-management.expense-head.index',compact('expense_heads'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('accounts::accounts-n-finance-management.expense-head.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        Gate::authorize('hasCreatePermission');
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|min:3|unique:expense_heads'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('expense-head.create')
                ->withErrors($validator)
                ->withInput();
        }
        $ehead = new ExpenseHead();
        $ehead->name = $request->name;
        $ehead->status = 1;
        $ehead->save();
        return redirect()->route('expense-head.index')->with('success', 'Expense Head has created successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('accounts::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $expense_head = ExpenseHead::findOrFail($id);
        return view('accounts::accounts-n-finance-management.expense-head.edit',compact('expense_head'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $ehead = ExpenseHead::findOrFail($id);
        $ehead->name = $request->name;
        $ehead->status = 1;
        $ehead->save();
        return redirect()->route('expense-head.index')->with('success', 'Expense Head has updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $expense_head = ExpenseHead::findOrFail($id);
        $expense_head->status = 0;
        $expense_head->save();

        return redirect()->route('expense-head.index')->with('success', 'Expense Head ststus has been changed successfully');
    }
}
