<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Accounts\Entities\Deposite;
use Modules\Accounts\Entities\Transaction;
use Modules\Store\Entities\StoreUser;

class DepositReceiveController extends Controller
{
    public function index()
    {
        $received_deposites = Deposite::with('store')->where('status', '=', "Received")->get();
        return view('accounts::accounts-n-finance-management.deposit-receive.index', compact('received_deposites'));
    }

    public function create()
    {
        return view('accounts::accounts-n-finance-management.deposit-receive.create');
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        return view('accounts::show');
    }

    public function edit($id)
    {
        $pending_deposite = Deposite::where('id', $id)->where('status', '=', "Pending")->first();
        return view('accounts::accounts-n-finance-management.deposit-receive.edit', compact('pending_deposite'));
    }

    public function update(Request $request, $id)
    {
        Deposite::where('id', $id)->update([
            'received_note' => $request->receive_note,
            'status' => "Received"
        ]);
        $deposite = Deposite::where('id', $id)->firstOrFail();

        $transaction = new Transaction();
        $transaction->deposite_id = $id;
        $transaction->amount = $deposite->amount;
        $transaction->transaction_type = 1; /* 1 means cash Out */
        $transaction->save();
        return redirect()->route('deposit-receive.index')->with('success', 'Store Deposit has been Received successfully');
    }

    public function destroy($id)
    {
        //
    }
}
