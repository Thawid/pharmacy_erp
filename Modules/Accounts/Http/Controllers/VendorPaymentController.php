<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class VendorPaymentController extends Controller
{
    public function index()
    {
        return view('accounts::accounts-n-finance-management.vendor-payment.index');
    }

    public function create()
    {
        return view('accounts::accounts-n-finance-management.vendor-payment.create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return view('accounts::accounts-n-finance-management.vendor-payment.show');
    }

    public function edit($id)
    {
        return view('accounts::accounts-n-finance-management.vendor-payment.edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
