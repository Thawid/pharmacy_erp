<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller;
use Modules\Accounts\Entities\Account;
use Modules\Accounts\Entities\AccountRelation;
use DB;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $accounts = Account::all();
        return view('accounts::accounts-n-finance-management.accounts.index',compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('accounts::accounts-n-finance-management.accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'account_type' => 'required',
            'balance_type' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('accounts.create')
                ->withErrors($validator)
                ->withInput();
        }
        try{
            DB::beginTransaction();
            $account = new Account();
            $account->company_id = auth()->user()->id;
            $account->company_type = "ta";
            $account->name = $request->name;
            $account->slug = Str::slug($request->name, '-').'-'. rand(1000,100000);
            $account->account_code = $request->account_code;
            $account->suffixed_code = rand(100000,1000000);
            $account->parent_id = $request->parent_account;
            $account->type = $request->account_type;
            $account->initial_balance = $request->initial_balance;
            $account->level = 1;
            $account->save();

            $account_relation = new AccountRelation();
            $account_relation->accounts_id = $account->id;
            $account_relation->company_type = "ta";
            $account_relation->company_id = auth()->user()->id;
            $account_relation->level = 1;
            $account_relation->parent_id = $request->parent_account;
            $account_relation->type = $request->account_type;
            $account_relation->save();
            DB::commit();
            return redirect()->route('accounts.index')->with('success', 'data Created successfully');
        }catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('accounts::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('accounts::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
