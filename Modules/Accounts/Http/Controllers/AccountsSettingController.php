<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Accounts\Entities\AccountsSetting;

class AccountsSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $setting = AccountsSetting::where('company_id', auth()->user()->id)->first();
        return view('accounts::accounts-n-finance-management.account-settings.index')->with([
            'start_month'   => $setting->start_month ?? null,
            'end_month'     => $setting->end_month ?? null,
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request){
        $start = (int)$request->start_month;
        $end = (int)$request->end_month;

        $difference = $start - $end;

        if($difference == 11 || $difference == 1 || $difference == -11){
            $setting = AccountsSetting::where('company_id', auth()->user()->id)->first();
            if($setting){
                $setting->update([
                    'start_month'   => $start,
                    'end_month'     => $end,
                ]);
            }
            else{
                AccountsSetting::create([
                    'start_month'   => $start,
                    'end_month'     => $end,
                    'company_id'     => auth()->user()->id,
                    'company_type' => "ta"
                ]);
            }
        }else{
            return redirect()->back()->with('error', "Please select valid month.");
        }
        return redirect()->back()->with('success', "Successfully updated the accounts settings.");
    }

}
