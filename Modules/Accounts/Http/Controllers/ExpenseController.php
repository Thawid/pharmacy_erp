<?php

namespace Modules\Accounts\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Accounts\Entities\Expense;
use Modules\Accounts\Entities\ExpenseHead;
use Modules\Accounts\Entities\Transaction;

class ExpenseController extends Controller
{
    public function index()
    {
        $expenses = DB::table('expenses')
            ->join('expense_heads', 'expense_heads.id', '=', 'expenses.expense_head_id')
            ->select('expenses.expense_date', 'expenses.amount', 'expenses.note', 'expense_heads.name')->get();
        return view('accounts::accounts-n-finance-management.expenses.index', compact('expenses'));
    }

    public function create()
    {
        $expense_heads = ExpenseHead::where('status', 1)->get(['id', 'name']);
        return view('accounts::accounts-n-finance-management.expenses.create', compact('expense_heads'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'expense_head_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('expenses.create')
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $expense = new Expense();
            $expense->expense_date = $request->date;
            $expense->expense_head_id = $request->expense_head_id;
            $expense->amount = $request->amount;
            $expense->note = $request->note;
            $expense->invoice_no = "expense-".rand(100000, 1000000000);
            $expense->save();

            $transaction = new Transaction();
            $transaction->expense_id = $expense->id;
            $transaction->amount = $request->amount;
            $transaction->transaction_type = 1; /* 1 means cash Out */
            $transaction->save();

            return redirect()->route('expenses.index')->with('success', 'Expense has created successfully');

        } catch (\Exception $e) {
            return redirect()->back()->with('danger', 'Something Went Wrong');

        }
    }

    public function show($id)
    {
        return view('accounts::show');
    }

    public function edit($id)
    {
        return view('accounts::accounts-n-finance-management.expenses.edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
