<?php

namespace Modules\Accounts\Http\Controllers;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TransactionController extends Controller
{
    public function index()
    {
        $cashIn = DB::table('transactions')
            ->select(DB::raw('sum(amount) as total_amount'))
            ->where('transaction_type', 2)
            ->get();
        $cashOut = DB::table('transactions')
            ->select(DB::raw('sum(amount) as total_amount'))
            ->where('transaction_type', 1)
            ->get();
        $transactions = DB::table('transactions')
            ->leftJoin('expenses', 'expenses.id', '=', 'transactions.expense_id')
            ->leftJoin('deposites', 'deposites.id', '=', 'transactions.deposite_id')
            ->leftJoin('store_invoices', 'store_invoices.store_id', '=', 'deposites.store_id')
            ->leftJoin('other_payments', 'other_payments.id', '=', 'transactions.other_payment_id')
           ->select('transactions.*','deposites.deposit_no','store_invoices.invoice_no as sales_invoice','store_invoices.store_id','other_payments.invoice_no as other_invoice','expenses.invoice_no as expense_invoice')
           ->get();
        return view('accounts::accounts-n-finance-management.transaction.index',
            compact('transactions','cashIn','cashOut'));
    }

    public function searchResult()
    {

        $cashIn = DB::table('transactions')
            ->select(DB::raw('sum(amount) as total_amount'))
            ->where('transaction_type', 2)
            ->get();
        $cashOut = DB::table('transactions')
            ->select(DB::raw('sum(amount) as total_amount'))
            ->where('transaction_type', 1)
            ->get();
        /*formatting date*/
        $from_date = new Carbon(request()->from_date);
        $to_date = new Carbon(request()->to_date);

        $formatted_from_date = $from_date->format('Y-m-d');
        $formatted_to_date = $to_date->format('Y-m-d');
        $transactions = DB::table('transactions')
            ->leftJoin('expenses', 'expenses.id', '=', 'transactions.expense_id')
            ->leftJoin('deposites', 'deposites.id', '=', 'transactions.deposite_id')
            ->leftJoin('store_invoices', 'store_invoices.store_id', '=', 'deposites.store_id')
            ->leftJoin('other_payments', 'other_payments.id', '=', 'transactions.other_payment_id')
            ->select('transactions.*','deposites.deposit_no','store_invoices.invoice_no as sales_invoice','store_invoices.store_id','other_payments.invoice_no as other_invoice','expenses.invoice_no as expense_invoice')
            ->whereBetween('transactions.created_at', [$formatted_from_date, $formatted_to_date])
            ->get();
        return view('accounts::accounts-n-finance-management.transaction.index', compact('transactions','cashIn','cashOut'));
    }

    public function create()
    {
        return view('accounts::create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return view('accounts::accounts-n-finance-management.transaction.show');
    }

    public function edit($id)
    {
        return view('accounts::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
