<?php

namespace Modules\Accounts\Http\Controllers;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Store\Entities\Store;
use Modules\Accounts\Entities\StoreInvoice;

class SalesInvoiceController extends Controller
{
    public function index()
    {
        $stores = Store::where('status', 1)->where('store_type', '=', "store")->where('hub_id', '!=', null)->get(['id', 'name']);
        $hubs = Store::where('status', 1)->where('store_type', '=', "hub")->where('hub_id', null)->get(['id', 'name']);
        $store_invoices = DB::table('store_invoices')
            ->leftJoin('stores as s1', 's1.id', '=', 'store_invoices.store_id')
            ->leftJoin('stores as h1', 'h1.id', '=', 'store_invoices.hub_id')
            ->select('store_invoices.status', 'store_invoices.store_id', 's1.id', 's1.hub_id', 's1.name as store_name', 's1.address', 'h1.name as hub_name',
                DB::raw('count(store_invoices.invoice_no) as total_invoice'),
                DB::raw('sum(store_invoices.amount) as total_amount'))
            ->groupBy('store_invoices.store_id')
            ->orderBy('store_invoices.created_at', 'desc')
            ->whereDate('store_invoices.created_at',Carbon::today())
            ->get();

        return view('accounts::accounts-n-finance-management.sales-invoice.index', compact('stores', 'hubs', 'store_invoices'));
    }

    public function storeInvoiceArchive()
    {
        $stores = Store::where('status', 1)->where('store_type', '=', "store")->where('hub_id', '!=', null)->get(['id', 'name']);
        $hubs = Store::where('status', 1)->where('store_type', '=', "hub")->where('hub_id', null)->get(['id', 'name']);
        $store_invoices = DB::table('store_invoices')
            ->leftJoin('stores as s1', 's1.id', '=', 'store_invoices.store_id')
            ->leftJoin('stores as h1', 'h1.id', '=', 'store_invoices.hub_id')
            ->select('store_invoices.status', 'store_invoices.store_id', 's1.id', 's1.hub_id', 's1.name as store_name', 's1.address', 'h1.name as hub_name',
                DB::raw('count(store_invoices.invoice_no) as total_invoice'),
                DB::raw('sum(store_invoices.amount) as total_amount'))
            ->groupBy('store_invoices.store_id')
            ->orderBy('store_invoices.created_at', 'desc')
            ->get();

        return view('accounts::accounts-n-finance-management.sales-invoice.invoice-archive', compact('stores', 'hubs', 'store_invoices'));


    }

    public function searchResult(Request $request)
    {
        $stores = Store::where('status', 1)->where('store_type', '=', "store")->where('hub_id', '!=', null)->get(['id', 'name']);
        $hubs = Store::where('status', 1)->where('store_type', '=', "hub")->where('hub_id', null)->get(['id', 'name']);

        $to_date = new Carbon($request->created_at);
        $req_date = $to_date->format('Y-m-d');
        $store_id = $request->store_id;
        $hub_id = $request->hub_id;
        $store_invoices = [];
        $store_invoice = DB::table('store_invoices')
            ->leftJoin('stores as s1', 's1.id', '=', 'store_invoices.store_id')
            ->leftJoin('stores as h1', 'h1.id', '=', 'store_invoices.hub_id')
            ->orWhere('store_invoices.store_id', $store_id)
            ->orWhere('store_invoices.hub_id', $hub_id)
            ->where('store_invoices.created_at', 'LIKE', "%{$req_date}%")
            ->select('store_invoices.status', 'store_invoices.store_id', 's1.id', 's1.hub_id', 's1.name as store_name', 's1.address', 'h1.name as hub_name',
                DB::raw('count(store_invoices.invoice_no) as total_invoice'),
                DB::raw('sum(store_invoices.amount) as total_amount'))
            ->orderBy('store_invoices.created_at', 'desc')
            ->get();
        if(count($store_invoice) > 0 && $store_invoice[0]->total_amount > 0){
            $store_invoices = $store_invoice;
        }else{
            $store_invoices;
        }

        return view('accounts::accounts-n-finance-management.sales-invoice.index', compact('stores', 'hubs', 'store_invoices'));

    }

    public function show($id)
    {
        $storeInformation = StoreInvoice::with(['store', 'hub'])->where('store_id', $id)->first();
        $today_invoice_amount = DB::table('store_invoices')
            ->select(DB::raw('sum(amount) as today_invoice_amount'))
            ->whereDate('created_at', Carbon::today())
            ->orderBy('created_at', 'desc')
            ->get();

        $total_invoice_amount = DB::table('store_invoices')
            ->select(DB::raw('sum(amount) as total_invoice_amount'))
            ->where('store_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();
        $storeInvoices = StoreInvoice::with(['store', 'hub'])->where('store_id', $id)->get();
        return view('accounts::accounts-n-finance-management.sales-invoice.show',compact('storeInvoices','today_invoice_amount','total_invoice_amount','storeInformation'));
    }
}
