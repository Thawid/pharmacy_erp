<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Accounts\Entities\OtherPayment;
use Modules\Accounts\Entities\Transaction;

class OtherPaymentController extends Controller
{
    public function index()
    {
        $other_payments = OtherPayment::all();
        return view('accounts::accounts-n-finance-management.other-payment.index', compact('other_payments'));
    }

    public function create()
    {
        Gate::authorize('hasCreatePermission');
        return view('accounts::accounts-n-finance-management.other-payment.create');
    }

    public function store(Request $request)
    {
        Gate::authorize('hasCreatePermission');
        $validator = Validator::make($request->all(), [
            'pay_amount' => 'required',
            'payment_type' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('other-payment.create')
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $other_payments = new OtherPayment();
            $other_payments->payment_date = $request->invoice_date;
            $other_payments->invoice_no = "other-".rand(1000000, 10000000);
            $other_payments->amount = $request->pay_amount;
            $other_payments->payment_type = $request->payment_type;
            $other_payments->status = $request->payment_type;
            $other_payments->received_by = $request->received_by;
            $other_payments->note = $request->payment_note;
            $other_payments->save();

            $transaction = new Transaction();
            $transaction->other_payment_id = $other_payments->id;
            $transaction->amount = $request->pay_amount;
            $transaction->transaction_type = 1; /* 1 means cash Out */
            $transaction->save();
            return redirect()->route('other-payment.index')->with('success', 'Other payment has created successfully');


        } catch (\Exception $e) {
            return redirect()->back()->with('danger', 'Something Went Wrong ');

        }
    }

    public function show($id)
    {
        return view('accounts::accounts-n-finance-management.other-payment.show');
    }

    public function edit($id)
    {
        return view('accounts::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
