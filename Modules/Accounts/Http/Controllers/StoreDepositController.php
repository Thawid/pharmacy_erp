<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Accounts\Entities\Deposite;
use Modules\Accounts\Entities\Transaction;
use Modules\Store\Entities\StoreUser;

class StoreDepositController extends Controller
{
    public function index()
    {
        $pending_deposites = Deposite::with('store')->where('status', '=', "Pending")->get();
        return view('accounts::accounts-n-finance-management.store-deposit.index', compact('pending_deposites'));
    }

    public function create()
    {
        Gate::authorize('hasCreatePermission');
        return view('accounts::accounts-n-finance-management.store-deposit.create');
    }

    public function store(Request $request)
    {
        Gate::authorize('hasCreatePermission');
        $validator = Validator::make($request->all(), [
            'deposit_amount' => 'required',
            'bank_name' => 'required',
            'bank_ac_no' => 'required|max:20',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('store-deposit.create')
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $userId = auth()->user()->id;
            $store_id = StoreUser::where('user_id', $userId)->value('store_id');
            $store_deposite = new Deposite();
            $store_deposite->store_id = $store_id;
            $store_deposite->deposit_date = $request->deposit_date;
            $store_deposite->amount = $request->deposit_amount;
            $store_deposite->bank_name = $request->bank_name;
            $store_deposite->account_number = $request->bank_ac_no;
            $store_deposite->deposit_note = $request->deposit_note;
            $store_deposite->deposit_no = "deposit-".rand(100000, 1000000000);
            $store_deposite->status = "Pending";
            $store_deposite->save();

            $transaction = new Transaction();
            $transaction->other_payment_id = $store_deposite->id;
            $transaction->amount = $request->deposit_amount;
            $transaction->transaction_type = 1; /* 1 means cash Out */
            $transaction->save();
            return redirect()->route('store-deposit.index')->with('success', 'Store Deposit has created successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('danger', 'Something Went Wrong ');
        }
    }

    public function show($id)
    {
        return view('accounts::show');
    }

    public function edit($id)
    {
        return view('accounts::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
