<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Accounts\Entities\Account;
use Modules\Accounts\Entities\AccountClosing;
use Modules\Accounts\Entities\AccountsSetting;
use Modules\Accounts\Entities\AccountStatement;
use Carbon\Carbon;

class AccountClosingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $account_closings = AccountClosing::where('company_id', auth()->user()->id)->get();
        return view('accounts::accounts-n-finance-management.account-closing.index')->with([
            'account_closings'  => $account_closings
        ]);
        return view('accounts::accounts-n-finance-management.account-closing.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $setting = AccountsSetting::where('company_id', auth()->user()->id)->first();
        $month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        $start_month = $month[$setting->start_month-1];
        $end_month = $month[$setting->end_month-1];
        return view('accounts::accounts-n-finance-management.account-closing.create')->with([
            'start' => $start_month,
            'end'   => $end_month
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {

        $account = Account::where('company_id', auth()->user()->id)->first();
        $setting = AccountsSetting::where('company_id', auth()->user()->id)->first();

        $year = $request->year;
        $closing_start_year = $year;
        if($setting->start_month == 1){
            $closing_starting_date = Carbon::createFromDate($year,$setting->start_month)->startOfMonth();
            $closing_ending_date = Carbon::createFromDate($year,$setting->end_month)->endOfMonth();
        }else{
            $closing_start_year = $year - 1;
            $closing_starting_date = Carbon::createFromDate($year-1,$setting->start_month)->startOfMonth();
            $closing_ending_date = Carbon::createFromDate($year,$setting->end_month)->endOfMonth();
        }

        $account_closing = AccountClosing::firstOrCreate([
            'company_id' => auth()->user()->id,
            'company_type' => "ta",
            'closing_start_year' => $closing_start_year,
            'closing_end_year'  => $year,
            'closing_start_from' => $closing_starting_date->format('Y-m-d'),
            'closing_ends_at'   => $closing_ending_date->format('Y-m-d'),
        ],[
            'created_by' => auth()->user()->id
        ]);
        $account_closing->fill(['created_by' => auth()->user()->id])->save();
        $accountStatement = AccountStatement::where('company_id', auth()->user()->id)->where([
            ['account_closing_id','=',$account_closing->id],
            ['account_type','=',$account->type],
        ])->first();
        if($accountStatement){
            $accountStatement->update([
                'company_id' => auth()->user()->id,
                'account_closing_id'    => $account_closing->id,
                'closing_year'          => $year,
                'account_type'          => $account->type,
                'company_type'          => "ta",
                'balance'               => 0
            ]);
        }
        else{
            AccountStatement::create([
                'company_id' => auth()->user()->id,
                'account_closing_id'    => $account_closing->id,
                'closing_year'          => $year,
                'account_type'          => $account->type,
                'company_type'          => "ta",
                'balance'               => 0
            ]);
        }
        return redirect()->route('account-closings')->with('success', "AccountClosing information has done");
    }

}
