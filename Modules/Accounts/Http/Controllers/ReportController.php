<?php

namespace Modules\Accounts\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Accounts\Entities\VendorInvoice;
use Modules\Accounts\Entities\VendorPayment;
use Modules\Store\Entities\Store;
use Modules\Vendor\Entities\Vendor;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $vendors = Vendor::where('status', 1)->get(['id', 'name']);
        $total_paid = 0;
        $results = [];
        return view('accounts::accounts-n-finance-management.report.index', compact('vendors', 'results', 'total_paid'));
    }

    public function vendorPaymentSummery()
    {
        /*formatting date*/
        $from_date = new Carbon(request()->from_date);
        $to_date = new Carbon(request()->to_date);

        $formatted_from_date = $from_date->format('Y-m-d');
        $formatted_to_date = $to_date->format('Y-m-d');
        $vendors = Vendor::where('status', 1)->get(['id', 'name']);
        $vendor_id = request()->vendor_id;
        $results = DB::table('vendor_invoices')
            ->join('vendor_payments', 'vendor_invoices.id', '=', 'vendor_payments.vendor_invoice_id')
            ->join('vendors', 'vendors.id', '=', 'vendor_invoices.vendor_id')
            ->select('vendor_invoices.id',
                'vendor_invoices.vendor_id',
                'vendor_payments.created_at',
                'vendor_payments.status',
                'vendor_payments.pay_amount',
                'vendors.id', 'vendors.name')
            ->where('vendor_invoices.vendor_id', $vendor_id)
            ->whereBetween('vendor_payments.created_at', [$formatted_from_date, $formatted_to_date])
            ->get();
        $total_paid = DB::table('vendor_invoices')
            ->join('vendor_payments', 'vendor_invoices.id', '=', 'vendor_payments.vendor_invoice_id')
            ->join('vendors', 'vendors.id', '=', 'vendor_invoices.vendor_id')
            ->select('vendor_invoices.id',
                'vendor_invoices.vendor_id',
                'vendor_payments.vendor_invoice_id',
                DB::raw('sum(vendor_payments.pay_amount) as total_amount'))
            ->where('vendor_invoices.vendor_id', $vendor_id)
            ->whereBetween('vendor_payments.created_at', [$formatted_from_date, $formatted_to_date])
            ->get();
        return view('accounts::accounts-n-finance-management.report.index', compact('vendors', 'results', 'total_paid'));
    }

    public function dailyPaymentReport()
    {
        $vendors = Vendor::where('status', 1)->get(['id', 'name']);
        $vendor_invoices = VendorInvoice::where('created_at', '!=', null)->get(['id', 'purchase_order_no', 'invoice_no']);
        $payments_no = VendorPayment::where('created_at', '!=', null)->get(['id', 'payment_no']);
        $results = [];
        return view('accounts::accounts-n-finance-management.report.daily-payment-report',
            compact('vendors', 'vendor_invoices', 'payments_no', 'results')
        );
    }

    public function dailyPaymentReportResults()
    {
        /*formatting date*/
        $from_date = new Carbon(request()->from_date);
        $to_date = new Carbon(request()->to_date);

        $formatted_from_date = $from_date->format('Y-m-d');
        $formatted_to_date = $to_date->format('Y-m-d');
        $vendors = Vendor::where('status', 1)->get(['id', 'name']);
        $vendor_invoices = VendorInvoice::where('created_at', '!=', null)->get(['id', 'purchase_order_no', 'invoice_no']);
        $payments_no = VendorPayment::where('created_at', '!=', null)->get(['id', 'payment_no']);
        $vendor_id = request()->vendor_id;
        $invoice_no = request()->invoice_no;
        $purchase_order_no = request()->purchase_order_no;
        $payment_no = request()->payment_no;
        $results = DB::table('vendor_invoices')
            ->join('vendor_payments', 'vendor_invoices.id', '=', 'vendor_payments.vendor_invoice_id')
            ->join('vendors', 'vendors.id', '=', 'vendor_invoices.vendor_id')
            ->select('vendor_invoices.id',
                'vendor_invoices.vendor_id',
                'vendor_invoices.purchase_order_no',
                'vendor_invoices.invoice_no',
                'vendor_payments.created_at',
                'vendor_payments.status',
                'vendor_payments.pay_amount',
                'vendor_payments.payment_no',
                'vendors.id', 'vendors.name')
            ->where('vendor_invoices.vendor_id', $vendor_id)
            ->whereBetween('vendor_payments.created_at', [$formatted_from_date, $formatted_to_date])
            ->orWhere('vendor_invoices.purchase_order_no', $purchase_order_no)
            ->orWhere('vendor_invoices.invoice_no', $invoice_no)
            ->orWhere('vendor_payments.payment_no', $payment_no)
            ->get();
        return view('accounts::accounts-n-finance-management.report.daily-payment-report',
            compact('vendors', 'vendor_invoices', 'payments_no', 'results')
        );
    }

    public function storeDepositReport()
    {
        $results = [];
        $stores = Store::where('status', 1)->where('store_type', '=', "store")->where('hub_id', '!=', null)->get(['id', 'name']);
        return view('accounts::accounts-n-finance-management.report.store-deposit-receive',
            compact('stores', 'results')
        );
    }

    public function storeDepositReportResults()
    {
        $stores = Store::where('status', 1)->where('store_type', '=', "store")->where('hub_id', '!=', null)->get(['id', 'name']);
        $store_id = request()->store_id;
        $from_date = new Carbon(request()->from_date);
        $to_date = new Carbon(request()->to_date);
        $formatted_from_date = $from_date->format('Y-m-d');
        $formatted_to_date = $to_date->format('Y-m-d');

        $results = DB::table('deposites')
            ->join('stores', 'stores.id', '=', 'deposites.store_id')
            ->select('deposites.*', 'stores.id', 'stores.name as store_name')
            ->where('deposites.store_id', $store_id)
            ->whereBetween('deposites.created_at', [$formatted_from_date, $formatted_to_date])
            ->where('deposites.status', '=', "Received")
            ->get();
        return view('accounts::accounts-n-finance-management.report.store-deposit-receive',
            compact('stores', 'results')
        );

    }

    public function accountStatement() {
      return view('accounts::accounts-n-finance-management.report.accounts-statement');
    }

    public function trialBalance() {
      return view('accounts::accounts-n-finance-management.report.trial-balance');
    }
}
