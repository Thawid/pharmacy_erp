<?php
namespace Modules\Accounts\Http\Controllers\Traits;
use Request;
trait AccountsCompany
{
    public function scopeCompany($query){
        $query->where(function ($query){
            $query->where('company_id',Request::get('_user_info')['company_id'])
                ->where('company_type',Request::get('_user_info')['company_type']);
        });
    }
    public static function bootAccountsCompany()
    {
        static::creating(function ($model) {
            $model->company_id =  \Request::get('_user_info')['company_id'];
            $model->company_type =  \Request::get('_user_info')['company_type'];
        });
    }
}
