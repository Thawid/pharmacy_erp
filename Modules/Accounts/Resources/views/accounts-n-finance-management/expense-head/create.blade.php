@extends('dboard.index')
@section('title', 'expense-head-create')

@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('expense-head.store') }}" method="POST">
        @csrf
        <div class="tile">
            <div class="tile-body">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-6">
                        <h1 class="title-heading">Create Expense Head</h1>
                    </div>

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{ route('expense-head.index') }}">Back</a>
                    </div>
                </div><!-- end.row -->
                <hr>

                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="category_name" class="forget-form">Expense Title</label>
                            <input type="text" class="form-control" name="name"
                                value="{{ old('name') }}" placeholder="Write expense title here">
                        </div><!-- end form-group -->
                    </div><!-- end .col-md-6 -->
                </div><!-- end.row -->
                <hr>

                <div class="row text-center">
                    <div class="col-md-12">
                        <button class="btn create-btn" type="submit">Submit</button>
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </form><!-- end form -->
@endsection

@push('post_scripts')
@endpush
