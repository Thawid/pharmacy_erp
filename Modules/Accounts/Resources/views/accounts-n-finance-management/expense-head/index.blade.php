@extends('dboard.index')

@section('title', 'Expense Head')

@push('styles')
@endpush

@section('dboard_content')


    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <div class="row justify-content-center align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Expense-Head List</h2>
                        </div>

                        <div class="col-md-6 text-right">
                            <a class="btn create-btn" href="{{ route('expense-head.create') }}">Add New Expense Head</a>
                        </div>
                    </div>
                    <hr>

                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Date</th>
                                            <th>Expense Head</th>
                                            <th>Action</th>
                                        </tr><!-- end tr -->
                                    </thead><!-- end thead -->

                                    <tbody>
                                    @foreach($expense_heads as $key=>$expense_head)
                                        <tr>
                                            <td>{{$key + 1 }}</td>
                                            <td>{{date('d-m-Y', strtotime($expense_head->created_at))}}</td>
                                            <td>{{$expense_head->name}}</td>
                                            <td>
                                                <div class="d-md-flex justify-content-around">
                                                    <a href="{{ route('expense-head.edit', $expense_head->id) }}"
                                                       class="btn edit-btn"><i class="fa fa-pencil"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody><!-- end tbody -->
                                </table><!-- end table -->
                            </div><!-- end .table-responsive -->
                        </div>
                    </div>
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->



    @endsection


    @push('post_scripts')
        <!-- Page specific javascripts-->
        <!-- Data table plugin-->
        <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">
            $('#sampleTable').DataTable();
        </script>
    @endpush
