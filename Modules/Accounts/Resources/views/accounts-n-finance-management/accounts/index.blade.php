@extends('dboard.index')
@section('title','Accounts List')

@push('styles')
    <style>
        .badge {
            font-size: 13px;
        }

        thead th {
            font-size: 16px;
            font-weight: bolder;
        }
    </style>

@endpush

@section('dboard_content')
    <div class="tile">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h1 class="title-heading">List of Accounts</h1>
            </div><!-- end.col-md-6 -->

            <div class="col-md-6 text-right">
                <a href="{{ route('accounts.create') }}" class="btn index-btn">Create New</a>
            </div><!-- end.col-md-6 -->
        </div><!-- end.row -->
        <hr>

        <div class="row">

            <!-- tables data -->
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Account Code</th>
                            <th>Account Title</th>
                            <th>Account Type</th>
                            <th>Parent Account</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(isset($accounts))
                            @foreach($accounts as $key=>$account)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$account->account_code}}</td>
                                    <td>{{$account->name}}</td>
                                    <td>{{$account->type}}</td>
                                    <td>
                                        @if($account->parent_id == 1)
                                            <p>A</p>
                                        @elseif($account->parent_id == 2)
                                            <p>B</p>
                                        @elseif($account->parent_id == 3)
                                            <p>C</p>
                                        @else
                                            <p>-</p>
                                        @endif

                                    </td>
                                    <td><a href="" class="badge badge-success p-2">Ledger</a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!-- end.tile -->
@endsection
@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
@endpush
