@extends('dboard.index')
@section('title','Account Create')

@section('dboard_content')
<div class="tile">
  <div class="row align-items-center">
    <div class="col-md-6">
      <h1 class="title-heading">Create Account</h1>
    </div><!-- end.col-md-6 -->

    <div class="col-md-6 text-right">
      <a href="{{ route('accounts.index') }}" class="btn index-btn">Accounts List</a>
    </div><!-- end.col-md-6 -->
  </div><!-- end.row -->
  <hr>

  <form action="{{ route('accounts.store') }}" method="post">
      @csrf
    <div class="row">
      <div class="col-xl-3 col-lg-4">
        <div class="form-group">
          <label for="account-head">Account Head</label>
          <input class="form-control" type="text" id="account-head" name="name">
        </div><!-- end.form-group -->
      </div><!-- end.col-xl-3 -->

      <div class="col-xl-3 col-lg-4">
        <div class="form-group">
          <label for="account-type">Account Type</label>
          <select class="form-control" name="account_type" id="account-type">
            <option value="" selected disabled>Select Account Type</option>
              <option value="Owners Equity">Owners Equity</option>
              <option value="Asset">Asset</option>
              <option value="Liability">Liability</option>
              <option value="Expense">Expense</option>
              <option value="Revenue">Revenue</option>
          </select>
        </div><!-- end.form-group -->
      </div><!-- end.col-xl-3 -->

      <div class="col-xl-3 col-lg-4">
        <div class="form-group">
          <label for="parent-account">Parent Account</label>
          <select class="form-control" name="parent_account" id="parent-account">
            <option value="" selected disabled>Select Parent Account</option>
              <option value="1">A</option>
              <option value="2">B</option>
              <option value="3">C</option>
          </select>
        </div><!-- end.form-group -->
      </div><!-- end.col-xl-3 -->

      <div class="col-xl-3 col-lg-4">
        <div class="form-group">
          <label for="account-code">Account Code</label>
          <input class="form-control" type="text" id="account-code" name="account_code">
        </div><!-- end.form-group -->
      </div><!-- end.col-xl-3 -->

      <div class="col-xl-3 col-lg-4">
        <div class="form-group">
          <label for="opening-balance">Opening Balance</label>
          <input class="form-control" type="text" id="opening-balance" name="initial_balance">
        </div><!-- end.form-group -->
      </div><!-- end.col-xl-3 -->

      <div class="col-xl-3 col-lg-4">
        <div class="form-group">
          <label for="balance-type">Balance Type</label>
          <select id="balance-type" name="balance_type" class="form-control">
            <option value="" selected disabled>Select Balance Type</option>
              <option value="1">Debit</option>
              <option value="0">Credit</option>
          </select>
        </div><!-- end.form-group -->
      </div><!-- end.col-xl-3 -->
    </div><!-- end.row -->
    <hr>

    <div class="row">
      <div class="col-md-12 text-right">
        <button class="btn index-btn" type="submit">Submit</button>
      </div>
    </div>
  </form>
</div><!-- end.tile -->
@endsection
