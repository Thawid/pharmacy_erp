@extends('dboard.index')
@section('title','Ledger For-Capital')

@push('styles')
<style>
  .table th {
    font-weight: bolder;
  }
</style>
@endpush

@section('dboard_content')
<div class="tile">
  <div class="row">
    <div class="col-md-12">
      <h1 class="title-heading">Ledger For - Capital [Owners Equity]</h1>
    </div><!-- end.col-md-6 -->
  </div><!-- end.row -->
  <hr>

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label for="year">Year</label>
        <select class="form-control" name="year" id="year">
          <option value="" selected disabled>Select Running Year</option>
        </select>
      </div><!-- end.form-group -->
    </div><!-- end.col-md-4 -->

    <div class="col-md-4">
      <div class="form-group">
        <label for="filter">Filter</label>
        <button class="btn index-btn form-control" type="submit">Filter</button>
      </div>
    </div><!-- end.col-md-4 -->
  </div><!-- end.row -->
  <hr>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered table-striped ">
        <thead>
          <tr>
            <th>Date</th>
            <th>Account</th>
            <th>Narration</th>
            <th>Debit(Dr)</th>
            <th>Credit(Cr)</th>
            <th>Balance</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>2021-05-15</td>
            <td>Cash</td>
            <td>Test</td>
            <td class="text-right">0.00</td>
            <td class="text-right">10000</td>
            <td class="text-right">70000</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div><!-- end.tile -->
@endsection