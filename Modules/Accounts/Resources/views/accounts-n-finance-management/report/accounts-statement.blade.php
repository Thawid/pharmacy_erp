@extends('dboard.index')
@section('title','Account Statement')

@push('styles')
<style>
  .table th {
    font-weight: bolder;
  }
</style>
@endpush

@section('dboard_content')
<div class="tile">
  <div class="row align-items-center">
    <div class="col-md-6">
      <h1 class="title-heading">Account Statements</h1>
    </div><!-- end.col-md-6 -->
    <div class="col-md-6 text-right">
      <button class="btn index-btn">Download</button>
    </div>
  </div><!-- end.row -->
  <hr>

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label for="account_type">Account Type</label>
        <select class="form-control" name="account_type" id="account_type">
          <option value="" selected disabled>Select Account Type</option>
        </select>
      </div><!-- end.form-group -->
    </div><!-- end.col-md-4 -->

    <div class="col-md-4">
      <div class="form-group">
        <label for="date_from">Date From</label>
        <input class="form-control" type="date" name="date_from" id="date_from">
      </div><!-- end.form-group -->
    </div><!-- end.col-md-4 -->

    <div class="col-md-4">
      <div class="form-group">
        <label for="date_to">Date To</label>
        <input class="form-control" type="date" name="date_to" id="date_to">
      </div><!-- end.form-group -->
    </div><!-- end.col-md-4 -->
  </div><!-- end.row -->
  <hr>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-hover table-bordered table-striped">
        <thead>
          <tr>
            <th>SL</th>
            <th>Date</th>
            <th>Account</th>
            <th>Debit(Dr)</th>
            <th>Credit(Cr)</th>
            <th>Balance</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>1</td>
            <td>2021</td>
            <td>Cash</td>
            <td class="text-right">10000</td>
            <td class="text-right">30000</td>
            <td class="text-right">50000</td>
          </tr>
        </tbody>
      </table>
    </div><!-- end.col-md-12 -->
  </div><!-- end.row -->
</div><!-- end.tile -->
@endsection