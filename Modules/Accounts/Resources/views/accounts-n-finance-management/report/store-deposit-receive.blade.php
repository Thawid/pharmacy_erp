@extends('dboard.index')
@section('title', 'Store Deposit Report')
@push('styles')
    <style>
        table.dataTable td:last-child i {
            font-size: 16px;
            margin: 0;
        }


        input::-webkit-input-placeholder {
            font-size: 16px;
        }

        div.dataTables_wrapper div.dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            font-size: 0;
        }

        div.dataTables_wrapper div.dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            font-size: 0;
        }


        div.dataTables_wrapper div.dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
            margin-left: 10px;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: -1px;
        }

        ul.title-heading {
            display: flex;
            padding: 0;
            margin: 0;
        }

        ul.title-heading li {
            list-style: none;
            margin-right: 15px;
        }

        ul.title-heading li a {
            text-decoration: none;
            position: relative;
        }

        ul.title-heading li a:hover {
            text-decoration: none;
        }

        ul.title-heading li a::before {
            position: absolute;
            content: "";
            height: 2px;
            width: 0;
            background: #0088FF;
            bottom: -5px;
            left: 0;
            right: 0;
            margin: auto;
            transition: .3s;
        }

        ul.title-heading li:hover a::before {
            width: 100%;
        }

        ul.title-heading li.active-tab a {
            color: #0088FF;
            transition: .3s;
        }

        ul.title-heading li.active-tab a::before {
            width: 100%;
        }

        #button-area i {
            font-size: 18px;
        }

    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="{{route('store.deposit.report.result')}}" method="GET">
        <div class="row">
            <div class="col-md-12">
                <div class="tile vendor-invoice-body-tile">
                    <div class="tile-body">
                        <!-- payment tabs -->
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-6">
                                <ul class="title-heading">
                                    <li>
                                        <a href="#">Store Deposit Report</a>
                                    </li>
                                </ul>
                            </div><!-- end .col-md-6 -->
                        </div><!-- end .row -->
                        <hr>

                        <div class="row">

                            <!-- select vendor -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Select Store<span class="text-danger">*</span></label>
                                    <select name="store_id" id="select_status" class="form-control">
                                        <option value=""> --Select Store--</option>
                                        @foreach($stores as $store)
                                            <option value="{{$store->id}}">{{$store->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- select store -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">From Date</label>
                                    <input type="date" class="form-control" name="from_date">
                                </div>
                            </div>

                            <!-- select date -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">To Date</label>
                                    <input type="date" class="form-control" name="to_date">
                                </div>
                            </div>

                            <!-- submit button -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">&nbsp;&nbsp;</label>
                                    <button type="submit" class="btn filter-btn">Search</button>
                                </div>
                            </div>
                        </div>

                        <!-- data table -->
                        @if(isset($results) && count($results) > 0)

                            <div class="col-md-12">
                                <div class="table-responsive printable-area">
                                    <table class="table table-striped table-bordered table-hover"
                                           id="table2excel">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Store Details</th>
                                            <th>Deposite ID</th>
                                            <th>Status</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        @foreach($results as $key=>$result)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{date('d-m-Y  h:i A', strtotime($result->deposit_date))}}</td>

                                                <td>{{$result->store_name}}</td>
                                                <td>{{$result->deposit_no}}</td>
                                                <td>{{$result->status}}</td>
                                                <td>{{$result->amount}} BDT</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div><!--  end .table -->
                            </div><!--  end .table -->
                        @endif
                        <hr>
                        <div id="button-area">
                            <div class="row text-right">
                                <div class="col-md-12">
                                    <a href="#" class="btn btn-outline-primary mr-3" type="button" id="pdf_btn">Print <i
                                            class="pl-2 fa fa-print"></i>
                                    </a>

                                    <a href="#" class="btn btn-outline-primary mr-3" type="button" id="print_btn">PDF <i
                                            class="pl-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                    <button class="btn btn-outline-primary mr-3" type="button">Excel <i
                                            class="pl-2 fa fa-file-excel-o" aria-hidden="true"></i></button>
                                </div>

                                <div class="col-md-12">
                                </div>
                            </div>
                        </div>
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </form>

@endsection

@push('post_scripts')
    <script
        src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
    </script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <!-- print area plugin -->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.printarea.js') }}"></script>

    <script>
        $(function () {
            $("button").click(function () {
                $("#table2excel").table2excel({
                    exclude: ".xls",
                    name: "Product Report"
                });
            });
        });
    </script>

    {{-- script to print a specific area --}}
    <script>
        $(function () {
            $("#print_btn").on('click', function () {

                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printable-area").printArea(options);
            });
        });
    </script>

    {{-- script to print a specific area --}}
    <script>
        $(function () {
            $("#pdf_btn").on('click', function () {

                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printable-area").printArea(options);
            });
        });
    </script>
@endpush
