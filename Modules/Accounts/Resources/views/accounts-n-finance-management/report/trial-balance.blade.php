@extends('dboard.index')
@section('title','Trial Balance')

@push('styles')
<style>
  .table th {
    font-weight: bolder;
  }
</style>
@endpush

@section('dboard_content')
<div class="tile">
  <div class="row">
    <div class="col-md-6">
      <h1 class="title-heading">Trial Balance</h1>
    </div><!-- end.col-md-6 -->
  </div><!-- end.row -->
  <hr>

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label for="year">Year</label>
        <select class="form-control" name="year" id="year">
          <option value="">Select Running Year</option>
        </select>
      </div><!-- end.form-group -->
    </div><!-- end.col-md-4 -->

    <div class="col-md-4">
      <div class="form-group">
        <label for="filter">Filter</label>
        <button class="btn index-btn form-control" type="submit">Filter</button>
      </div>
    </div><!-- end.col-md-4 -->
  </div><!-- end.row -->
  <hr>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-hover table-bordered table-striped">
        <thead>
          <tr>
            <th width="10%">SL</th>
            <th>Account</th>
            <th>Debit</th>
            <th>Credit</th>
            <th>Balance</th>
          </tr>
          <tr class="">
            <td class="border-0" colspan="2"><strong>Owners Equality</strong></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Capital</td>
            <td class="text-right">80000</td>
            <td class="text-right">10000</td>
            <td class="text-right">70000</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Capital</td>
            <td class="text-right">80000</td>
            <td class="text-right">10000</td>
            <td class="text-right">70000</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td class="text-right"><strong>Total: 80000</strong></td>
            <td class="text-right"><strong>Total: 10000</strong></td>
            <td class="text-right"><strong>Total: 70000</strong></td>
          </tr>

          <tr class="">
            <td colspan="5"><strong>Assets</strong></td>
          </tr>

          <tr>
            <td>1</td>
            <td>Cash</td>
            <td class="text-right">4900</td>
            <td class="text-right">11000</td>
            <td class="text-right">38000</td>
          </tr>

          <tr>
            <td></td>
            <td></td>
            <td class="text-right"><strong>Total: 80000</strong></td>
            <td class="text-right"><strong>Total: 10000</strong></td>
            <td class="text-right"><strong>Total: 70000</strong></td>
          </tr>
        </tbody>
      </table>
    </div><!-- end.col-m-12 -->
  </div><!-- end.row -->
</div><!-- end.tile -->
@endsection