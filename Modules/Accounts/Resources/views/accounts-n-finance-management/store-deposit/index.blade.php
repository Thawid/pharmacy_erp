@extends('dboard.index')
@section('title', 'Pending Store Deposite')
@push('styles')
    <style>

        .pending-store-deposite-receive {
            border-radius: 8px;
        }

        div.dataTables_wrapper div.dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            font-size: 0;
        }

        div.dataTables_wrapper div.dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            font-size: 0;
        }


        div.dataTables_wrapper div.dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
            margin-left: 10px;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: -1px;
        }

        ul.title-heading {
            display: flex;
            padding: 0;
            margin: 0;
        }

        ul.title-heading li {
            list-style: none;
            margin-right: 15px;
        }

        ul.title-heading li a {
            text-decoration: none;
            position: relative;
        }

        ul.title-heading li a:hover {
            text-decoration: none;
        }

        ul.title-heading li a::before {
            position: absolute;
            content: "";
            height: 2px;
            width: 0;
            background: #0088FF;
            bottom: -5px;
            left: 0;
            right: 0;
            margin: auto;
            transition: .3s;
        }

        ul.title-heading li:hover a::before {
            width: 100%;
        }

        ul.title-heading li.active a {
            color: #0088FF;
            transition: .3s;
        }

        ul.title-heading li.active a::before {
            width: 100%;
        }

    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="">
        <div class="row">
            <div class="col-md-12">
                <div class="tile pending-store-deposite-receive">
                    <div class="tile-body">
                        <!-- title-area -->
                        <div class="row align-items-center">

                        <div class="col-md-6 text-left">
                                        <h2 class="title-heading">Store Deposit</h2>
                                    </div><!-- end .col-md-6 -->

                            <div class="col-md-6 text-right">
                                <a href="{{ route('store-deposit.create') }}" type="button" class="btn index-btn">Create
                                    Store Deposit</a>
                            </div><!-- end .col-md-6 -->

                            


                        </div><!-- end .row title-area -->

                        <!-- break line -->
                        <hr>
                        <!-- break line -->

                        <div class="row">

                            <!-- data table -->
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover"
                                       id="store-deposit-table">
                                    <thead>
                                    <tr>
                                        <th width="5%" style="border-right-width: 4px;">#</th>
                                        <th width="33.5%" style="border-right-width: 4px;">Store Details</th>
                                        <th width="33.5%" style="border-right-width: 4px;">Deposit ID</th>
                                        <th width="20%" style="border-right-width: 4px;">Status</th>
                                        <th width="8%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(isset($pending_deposites))
                                        @foreach($pending_deposites as $key=>$pending_deposite)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if($pending_deposite->store)
                                                        <h3>{{$pending_deposite->store->name}}</h3>
                                                    @endif
                                                    <p>Create Date: {{$pending_deposite->created_at}}</p>
                                                </td>
                                                <td class="align-item-center">
                                                    <h3>Deposit ID: {{$pending_deposite->deposit_no}}</h3>
                                                </td>

                                                <td>{{$pending_deposite->status}}</td>
                                                <td>
                                                    <div class="d-flex justify-content-around align-items-center">

                                                        <a href="{{ route('deposit-receive.edit',$pending_deposite->id) }}" class="btn download-btn">
                                                            <i class="fa fa-check" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>
                            </div><!--  end .table -->
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>


@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#store-deposit-table').DataTable();
    </script>

    <script>
        $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
    </script>

    <script>
        $(document).on('click', 'ul li', function () {
            $(this).addClass('active').siblings().removeClass('active');
        });
    </script>
@endpush
