@extends('dboard.index')
@section('title', 'Create Store Deposite')
@push('styles')
    <style>
    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('store-deposit.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <!-- title-area -->
                        <div class="row align-items-center">
                            <div class="col-md-9 text-left">
                                <h2 class="title-heading">Create Deposit</h2>
                            </div><!-- end .col-md-9 -->

                            <div class="col-md-3 text-right">
                                <a class="btn index-btn" href="{{ route('store-deposit.index') }}">Deposit List</a>
                            </div><!-- end .col-md-3 -->
                        </div><!-- end .row title-area -->
                        <hr>
                        <!-- form-fields -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <!-- first 4 fields -->
                                <div class="row">
                                    <!-- date -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="date">Date</label>
                                            <input type="date" class="form-control" name="deposit_date" id="date" value="{{old('deposit_date')}}">
                                        </div>
                                    </div><!-- end invoice date -->

                                    <!-- amount -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amount">Amount</label>
                                            <input type="number" class="form-control" name="deposit_amount"
                                                id="amount" placeholder="amount" value="{{old('deposit_amount')}}">
                                        </div>
                                    </div><!-- end amount -->

                                    <!-- bank name -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="bank_name">Bank Name</label>
                                            <input type="text" class="form-control" name="bank_name"
                                                id="bank_name" placeholder="EBL Bank" value="{{old('bank_name')}}">
                                        </div>
                                    </div><!-- end bank name -->

                                    <!-- bank a/c no -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="bank_ac_no">Bank A/C No</label>
                                            <input type="number" class="form-control" name="bank_ac_no"
                                                id="bank_ac_no" placeholder="xxxxxxxxxxxx1234" value="{{old('bank_ac_no')}}">
                                        </div>
                                    </div><!-- end bank a/c no -->
                                </div><!-- end .row first 4 fields -->

                                <!-- last field -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="deposit_notes">Deposit Note</label>
                                            <textarea class="form-control" name="deposit_note"
                                                id="deposit_notes">{{old('deposit_note')}}</textarea>
                                        </div><!-- end .form-control -->
                                    </div><!-- end .col-md-12 -->
                                </div><!-- end .row last field -->
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <!-- submit buttons -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="btn-area d-flex justify-content-end">
                                    <button class="btn index-btn" type="submit">Submit</button>
                                </div>
                            </div>
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>
@endsection

@push('post_scripts')
@endpush
