@extends('dboard.index')
@section('title', 'Create Other Payments')
@push('styles')
    <style>
        h1,
        h2,
        h3,
        h4,
        h5 {
            padding: 0;
            margin: 0;
        }

        p {
            padding: 0;
            margin: 0;
        }

        .vendor-invoice-body-tile {
            border-radius: 8px;
        }



    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('other-payment.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <!-- title-area -->
                        <div class="row align-items-center">
                            <div class="col-md-9 text-left">
                                <h2 class="title-heading">Create Others Payment</h2>
                            </div><!-- end .col-md-9 -->

                            <div class="col-md-3 text-right">
                                <a class="btn index-btn" href="{{ route('other-payment.index') }}">Other
                                    Payment List</a>
                            </div><!-- end .col-md-3 -->
                        </div><!-- end .row title-area -->
                        <hr>
                        <!-- form-fields -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <!-- first 4 fields -->
                                <div class="row">
                                    <!-- invoice date -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="invoice_date">Invoice Date</label>
                                            <input type="date" class="form-control" name="invoice_date" value="{{old('invoice_date')}}" id="invoice_date">
                                        </div>
                                    </div><!-- end invoice date -->

                                    <!-- invoice number -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="invoice_number">Insert Invoice Number</label>
                                            <input type="text" class="form-control" name="invoice_no"
                                                id="invoice_number" placeholder="auto generate" readonly>
                                        </div>
                                    </div><!-- end invoice number -->

                                    <!-- select payment type -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="select_payment_type">Select Payment Type</label>
                                            <select class="form-control" name="payment_type"
                                                    id="select_payment_type">
                                                <option value>Select Payment Type</option>
                                                <option value="Partial" @if (old('payment_type') == "Partial") selected="selected" @endif>Partial</option>
                                                <option value="Full Payment" @if (old('payment_type') == "Full Payment") selected="selected" @endif>Full Payment</option>
                                            </select>
                                        </div>
                                    </div><!-- end select payment type -->

                                </div><!-- end .row first 4 fields -->

                                <!-- second 3 fields -->
                                <div class="row">

                                    <!-- pay amount -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="pay_amount">Pay Amount</label>
                                            <input type="text" class="form-control" name="pay_amount" id="pay_amount"
                                                placeholder="0.0 BDT" value="{{old('pay_amount')}}">
                                        </div>
                                    </div><!-- end pay amount -->

                                    <!-- Received By -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="received_by">Received By</label>
                                            <input type="text" class="form-control" name="received_by" id="received_by"
                                                placeholder="receiver name" value="{{old('received_by')}}">
                                        </div>
                                    </div><!-- end Received By -->
                                </div><!-- end .row second 3 fields -->

                                <!-- last field -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-contorl">
                                            <label for="payment_notes">Payment Note</label>
                                            <textarea class="form-control" name="payment_note" id="payment_notes">{{old('payment_note')}}</textarea>
                                        </div>
                                    </div>
                                </div><!-- end .row last field -->
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <!-- submit buttons -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="btn-area d-flex justify-content-end">
                                    <button class="btn index-btn" type="submit">Submit</button>
                                </div>
                            </div>
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile vendor-invoice-body-tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>
@endsection

@push('post_scripts')
@endpush
