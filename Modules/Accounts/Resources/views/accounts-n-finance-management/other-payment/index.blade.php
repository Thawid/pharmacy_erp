@extends('dboard.index')
@section('title', 'Vendor Payment List')
@push('styles')
    <style>

        h1,
        h2,
        h3,
        h4,
        h5 {
            padding: 0;
            margin: 0;
        }

        p {
            padding: 0;
            margin: 0;
        }

        .tile {
            border-radius: 8px;
        }

        .back-btn {
            /* width: 72px; */
            height: 36px;
            background: rgba(0, 200, 128, 0.1);
            border: 1px solid #00C880;
            box-sizing: border-box;
            color: #00C880;
            font-size: 14px;
        }

        .back-btn:hover {
            background: rgba(0, 200, 128, 0.1);
            border: 1px solid #00C880;
            box-sizing: border-box;
            color: #00C880;
        }

        .thead-color {
            background: #F4F4F4;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: 0;
        }


        input::-webkit-input-placeholder {
            font-size: 16px;
        }

        div.dataTables_wrapper div.dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            font-size: 0;
        }

        div.dataTables_wrapper div.dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            font-size: 0;
        }


        div.dataTables_wrapper div.dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
            margin-left: 10px;
        }

        tbody h3 {
            font-size: 16px;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: -1px;
        }


    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row">
                            <!-- payment tabs -->
                            <div class="col-md-12">
                                <div class="row other-payment-title justify-content-between align-items-center">
                                    <div class="col-md-6">
                                        <h1 class="title-heading">Other Payments</h1>
                                    </div><!-- end .col-md-6 -->

                                    <!-- create button -->
                                    <div class="col-md-6" style="text-align: end;">
                                        <a href="{{ route('other-payment.create') }}" class="btn create-btn "
                                           type="button">Create
                                            Other Payment</a>
                                    </div><!-- end .col-md-6 -->
                                </div><!-- end .row -->
                                <hr>
                            </div><!-- end .col-md-12 -->

                            <!-- data table -->
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover"
                                       id="vendor-payment-data-table">
                                    <thead>
                                    <tr>
                                        <th width="5%" style="border-right-width: 4px;">#</th>
                                        <th width="20%" style="border-right-width: 4px;">Payments Recipet</th>
                                        <th width="20%" style="border-right-width: 4px;">Invoice ID</th>
                                        <th width="10%" style="border-right-width: 4px;">Payment Type</th>
                                        <th width="35%" style="border-right-width: 4px;">Payment Notes</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(isset($other_payments))
                                        @foreach($other_payments as $key=>$other_payment)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>
                                                    <h3>Payment ID: {{$other_payment->id}}</h3>
                                                    <p>Create Date: {{$other_payment->payment_date}}</p>
                                                </td>
                                                <td class="align-item-center">
                                                    <h3>Inv ID: {{$other_payment->invoice_no}}</h3>
                                                    <p>Create Date: {{$other_payment->payment_date}}</p>
                                                </td>

                                                <td>{{$other_payment->payment_type}}</td>
                                                <td>{!! $other_payment->note !!}</td>
                                                <td>
                                                    <div class="d-flex justify-content-between">
                                                        <a href="#" class="btn details-btn"><i
                                                                class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="#" class="btn pdf-btn"><i
                                                                class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="#"
                                                           class="btn download-btn"><i class="fa fa-download"
                                                                                       aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>
                            </div><!--  end .table -->
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>


@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#vendor-payment-data-table').DataTable();
    </script>

    <script>
        $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
    </script>
@endpush
