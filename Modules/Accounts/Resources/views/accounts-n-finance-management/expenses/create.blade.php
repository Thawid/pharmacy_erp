@extends('dboard.index')
@section('title', 'expense-create')

@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('expenses.store') }}" method="POST">
        @csrf
        <div class="tile">
            <div class="tile-body">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-6">
                        <h1 class="title-heading">Create Expenses</h1>
                    </div>

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{ route('expenses.index') }}">Back</a>
                    </div>
                </div><!-- end.row -->
                <hr>

                <div class="row">
                    <div class="col-md-6 col-md-lg-6 col-md-xl-6">
                        <!-- date -->
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" class="form-control" name="date" value="{{old('date')}}">
                        </div><!-- end form-group -->

                        <!-- head -->
                        <div class="form-group">
                            <label for="expense-head-title">Expense Head</label>
                            <select class="form-control" name="expense_head_id" id="">
                                <option value="" disabled selected>Select Expense Head</option>
                                @foreach($expense_heads as $expense_head)
                                    <option value="{{$expense_head->id}}" @if (old('expense_head_id') == $expense_head->id) selected="selected" @endif>{{$expense_head->name}}</option>
                                @endforeach
                            </select>
                        </div><!-- end form-group -->
                    </div><!-- end .col-md-6 -->

                    <div class="col-md-6 col-md-lg-6 col-md-xl-6">
                        <!-- amount -->
                        <div class="form-group">
                            <label for="amount" class="">Amount</label>
                            <input type="number" class="form-control" name="amount" value="{{old('amonut')}}" placeholder="enter amount here">
                        </div><!-- end form-group -->

                        <!-- note -->
                        <div class="form-group">
                            <label for="note" class="">Note</label>
                            <textarea class="form-control" name="note" id="receive_notes">{{old('note')}}</textarea>
                        </div><!-- end form-group -->
                    </div><!-- end .col-md-6 -->
                </div><!-- end.row -->
                <hr>

                <div class="row text-right">
                    <div class="col-md-12">
                        <button class="btn create-btn" type="submit">Submit</button>
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </form><!-- end form -->
@endsection

@push('post_scripts')
@endpush
