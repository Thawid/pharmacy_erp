@extends('dboard.index')
@section('title', 'Purchase Order List')

@section('dboard_content')
<div class="table-responsive bg-light p-3 ">
    <!-- title-area -->
    <div class="row align-items-center">
        <div class="col-md-9 text-left">
            <h2 class="title-heading">Purchase Order List</h2>
        </div><!-- end .col-md-9 -->
    </div><!-- end .row title-area -->
    <hr>
    <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
        <thead>
        <tr>
            <th>Details</th>
            <th>Priority</th>
            <th>Status</th>
            <th width="8%">Action</th>
        </tr>
        </thead>
        <tbody>

        @if(!empty($purchase_order_list))
            @foreach($purchase_order_list as $row)
                <tr>
                    <td>
                        <address>
                            <strong>Vendor: {{$row->vendor->name}}</strong><br>
                            <strong>Purchase_No: {{$row->purchase_order_no}}</strong><br>
                            Created Date: {{$row->purchase_order_date}}<br>
                            Request Delivery Date: {{$row->requested_delivery_date}} <br>
                        </address>
                    </td>
                    @if($row->priority == 1)
                        <td><strong>High</strong></td>
                    @elseif($row->priority == 2)
                        <td><strong>Low</strong></td>
                    @endif
                    <td> <strong>{{ $row->status }}</strong></td>
                    <td>
                        <div class="d-flex justify-content-around align-items-center">
                            <a href="{{route('vendor.purchase.order.details',$row->id)}}" class="btn details-btn" type="button">
                                <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#requisitionList').DataTable();
    </script>
@endpush
