@extends('dboard.index')
@section('title', 'Vendor Invoice Payment List')
@push('styles')
    <style>

        .vendor-invoice-body-tile {
            border-radius: 8px;
        }

        .thead-color {
            background: #F4F4F4;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin-left: 0;
        }


        input::-webkit-input-placeholder {
            font-size: 16px;
        }

        div.dataTables_wrapper div.dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            font-size: 0;
        }

        div.dataTables_wrapper div.dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            font-size: 0;
        }


        div.dataTables_wrapper div.dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
            margin-left: 10px;
        }

        tbody h3 {
            font-size: 16px;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: -1px;
        }

        thead th {
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
        }

    </style>
@endpush
@section('dboard_content')
    <!-- data-area -->
    <form>

        <div class="row">
            <div class="col-md-12">
                <div class="tile vendor-invoice-body-tile">
                    <div class="tile-body">
                        <div class="row align-items-center">
                            <div class="col-md-9 text-left">
                                <h2 class="title-heading">Vendor Payment List</h2>
                            </div><!-- end .col-md-9 -->
                        </div><!-- end .row title-area -->
                        <hr>
                        <div class="row">
                            <!-- data table -->
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover"
                                       id="vendor-invoice-data-table">
                                    <thead>
                                    <tr>
                                        <th width="5%" style="border-right-width: 4px;">#</th>
                                        <th width="18%" style="border-right-width: 4px;">Payment Recipet</th>
                                        <th width="18%" style="border-right-width: 4px;">Invoice ID</th>
                                        <th width="18%" style="border-right-width: 4px;">Payment Type</th>
                                        <th width="5%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(isset($vendor_payment_lists))
                                        @foreach($vendor_payment_lists as $key=>$vendor_payment)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>
                                                    <h3>Inv ID: {{$vendor_payment->id}}</h3>
                                                    <p>Create Date: {{$vendor_payment->created_at}}</p>
                                                </td>
                                                <td>
                                                    <h3>Inv ID: {{$vendor_payment->invoice_no}}</h3>
                                                    <p>Create Date: {{$vendor_payment->created_at}}</p>
                                                </td>
                                                <td>
                                                    {{$vendor_payment->payment_type}}
                                                </td>
                                                <td>
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <button class="btn sumit-btn mr-2" type="button">Download</button>
                                                        <button class="btn sumit-btn" type="button">Prints</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!--  end .table -->
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>


@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#vendor-invoice-data-table').DataTable();
    </script>

    <script>
        $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
    </script>
@endpush
