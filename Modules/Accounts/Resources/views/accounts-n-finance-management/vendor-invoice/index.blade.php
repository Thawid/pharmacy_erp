@extends('dboard.index')
@section('title', 'Vendor Invoice List')
@push('styles')
<style>
  .vendor-invoice-body-tile {
    border-radius: 8px;
  }

  .thead-color {
    background: #F4F4F4;
  }

  table.dataTable td:last-child i {
    font-size: 16px;
    margin-left: 0;
  }


  input::-webkit-input-placeholder {
    font-size: 16px;
  }

  div.dataTables_wrapper div.dataTables_filter label {
    font-weight: normal;
    white-space: nowrap;
    text-align: left;
    font-size: 0;
  }

  div.dataTables_wrapper div.dataTables_length label {
    font-weight: normal;
    text-align: left;
    white-space: nowrap;
    font-size: 0;
  }


  div.dataTables_wrapper div.dataTables_info {
    padding-top: 0.85em;
    white-space: nowrap;
    margin-left: 10px;
  }

  tbody h3 {
    font-size: 16px;
  }

  table.dataTable td:last-child i {
    font-size: 16px;
    margin: -1px;
  }

  thead th {
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
  }
</style>
@endpush
@section('dboard_content')

<form action="{{ route('vendor.search.invoice') }}" method="POST">
  @csrf
  <div class="row">
    <div class="col-md-12">
      <div class="tile vendor-invoice-body-tile">
        <div class="tile-body">
          <!-- title-area -->
          <div class="row align-items-center">
            <div class="col-md-9 text-left">
              <h2 class="title-heading">Recent Stores Vendor Invoice</h2>
            </div><!-- end .col-md-9 -->

            <div class="col-md-3 text-right">
              <a class="btn index-btn" href="{{ route('vendor.invoice.archive') }}">Invoice Archive</a>
            </div><!-- end .col-md-3 -->
          </div><!-- end .row title-area -->
          <hr>

          <div class="row">
            <!-- filter-area -->
            <div class="col-md-12">
              <div class="row">
                <!-- select hub -->
                <div class="col-md-3">
                  <div class="form-group">
                    <select name="vendor_id" id="select_vendor" class="form-control">
                      <option value="" selected disabled>Select Vendor</option>
                      @foreach($vendors as $vendor)
                      <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <!-- select store -->
                <div class="col-md-3">
                  <div class="form-group">
                    <select name="status" id="select_status" class="form-control">
                      <option value="" selected disabled>Select Status</option>
                      <option value="Pending">Pending</option>
                      <option value="Approved">Approved</option>
                      <option value="Cancel">Cancel</option>
                      <option value="Partial">Partial</option>
                    </select>
                  </div>
                </div>

                <!-- select date -->
                <div class="col-md-3">
                  <div class="form-group">
                    <input type="date" class="form-control" name="created_at">
                  </div>
                </div>

                <!-- submit button -->
                <div class="col-md-3">
                  <div class="d-flex justify-content-end">
                    <button type="submit" class="btn filter-btn">Filter</button>
                  </div>
                </div>
              </div>
            </div><!-- end filter-area -->

            <!-- data table -->
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-hover" id="vendor-invoice-data-table">
                <thead>
                  <tr>
                    <th width="5%" style="border-right-width: 4px;">#</th>
                    <th width="18%" style="border-right-width: 4px;">Invoice Details</th>
                    <th width="18%" style="border-right-width: 4px;">Purchase Order</th>
                    <th width="18%" style="border-right-width: 4px;">Vendor Details</th>
                    <th width="18%" style="border-right-width: 4px;">Status</th>
                    <th width="18%" style="border-right-width: 4px;">Attachment</th>
                    <th width="5%">Action</th>
                  </tr>
                </thead>

                <tbody>
                  @if(isset($vendor_invoices))
                  @foreach($vendor_invoices as $key=>$vendor_invoice)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>
                      <h3>Inv ID: {{$vendor_invoices[$key]->invoice_no}}</h3>
                      <p>Create Date: {{$vendor_invoices[$key]->created_at}}</p>
                    </td>
                    <td class="align-item-center">
                      <h3>PO
                        ID: {{$vendor_invoices[$key]->purchase_order_no}}</h3>
                      <p>Create Date: {{$vendor_invoices[$key]->created_at}}</p>
                    </td>
                    <td>
                      {{$vendor_invoices[$key]->name}}
                    </td>

                    <td>{{$vendor_invoices[$key]->status}}</td>
                    <td>
                      @if($vendor_invoices[$key]->attachment)
                      <img class="show-images" src="{{ asset('uploads/vendor-invoice' . '/' . $vendor_invoices[$key]->attachment) }}" alt="images">
                      @else
                      <img src="https://via.placeholder.com/80" alt="Default Image">
                      @endif


                    </td>
                    <td>
                      <div class="d-flex justify-content-center align-items-center">
                        <a href="{{ route('accounts-vendor-invoice.show', $vendor_invoices[$key]->vendor_id) }}" class="btn details-btn"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!--  end .table -->
          </div><!-- end .row -->
        </div><!-- end .tile-body -->
      </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
  </div><!-- end .row -->
</form>

@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
  $('#vendor-invoice-data-table').DataTable();
</script>

<script>
  $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
</script>

@endpush