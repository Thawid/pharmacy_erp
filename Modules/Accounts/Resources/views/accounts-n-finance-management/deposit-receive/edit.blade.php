@extends('dboard.index')
@section('title', 'Create Store Deposite')
@push('styles')
    <style>
    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="{{ route('deposit-receive.update', $pending_deposite->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <!-- title-area -->
                        <div class="row align-items-center">
                            <div class="col-md-9 text-left">
                                <h2 class="title-heading">Create Deposit Receive</h2>
                            </div><!-- end .col-md-9 -->

                            <div class="col-md-3 text-right">
                                <a class="btn index-btn" href="{{ route('deposit-receive.index') }}">Deposit Receive List</a>
                            </div><!-- end .col-md-3 -->
                        </div><!-- end .row title-area -->
                        <hr>
                        <!-- form-fields -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <!-- first 4 fields -->
                                <div class="row">
                                    <!-- deposit date -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="date">Deposit Date</label>
                                            <input type="date" class="form-control" value="{{$pending_deposite->deposit_date}}" name="deposit_date" id="deposit_date" readonly>
                                        </div>
                                    </div><!-- end invoice date -->

                                    <!-- amount -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amount">Amount</label>
                                            <input type="text" class="form-control" name="amount" value="{{$pending_deposite->amount}}"
                                                   id="amount" readonly>
                                        </div>
                                    </div><!-- end amount -->

                                    <!-- bank name -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="bank_name">Bank Name</label>
                                            <input type="text" class="form-control" name="bank_name" value="{{$pending_deposite->bank_name}}"
                                                   id="bank_name" readonly>
                                        </div>
                                    </div><!-- end bank name -->

                                    <!-- bank a/c no -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="bank_ac_no">Bank A/C No</label>
                                            <input type="text" class="form-control" name="bank_ac_no" value="{{$pending_deposite->account_number}}"
                                                   id="bank_ac_no" readonly>
                                        </div>
                                    </div><!-- end bank a/c no -->
                                </div><!-- end .row first 4 fields -->

                                <!-- 2nd field -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-contorl">
                                            <label for="deposit_notes">Deposit Note</label>
                                            <textarea class="form-control" name="deposit_notes"
                                                      id="deposit_notes" readonly>{!! $pending_deposite->deposit_note !!}</textarea>
                                        </div><!-- end .form-control -->
                                    </div><!-- end .col-md-12 -->
                                </div><!-- end .row last field -->
                                <br>
                                <!-- 3rd field -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-contorl">
                                            <label for="receive_notes">Receive Note</label>
                                            <textarea class="form-control" name="receive_note" id="receive_notes">{{old('receive_note')}}</textarea>
                                        </div><!-- end .form-control -->
                                    </div><!-- end .col-md-12 -->
                                </div><!-- end .row last field -->
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <!-- submit buttons -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="btn-area d-flex justify-content-end">
                                    <button class="btn sumit-btn" type="submit">Submit</button>
                                </div>
                            </div>
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>
@endsection

@push('post_scripts')
@endpush
