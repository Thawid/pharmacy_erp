@extends('dboard.index')
@section('title', 'Pending Store Deposite Receive')
@push('styles')
<style>
  .pending-store-deposite-receive {
    border-radius: 8px;
  }

  div.dataTables_wrapper div.dataTables_filter label {
    font-weight: normal;
    white-space: nowrap;
    text-align: left;
    font-size: 0;
  }

  div.dataTables_wrapper div.dataTables_length label {
    font-weight: normal;
    text-align: left;
    white-space: nowrap;
    font-size: 0;
  }


  div.dataTables_wrapper div.dataTables_info {
    padding-top: 0.85em;
    white-space: nowrap;
    margin-left: 10px;
  }

  table.dataTable td:last-child i {
    font-size: 16px;
    margin: -1px;
  }

  ul.title-heading {
    display: flex;
    padding: 0;
    margin: 0;
  }

  ul.title-heading li {
    list-style: none;
    margin-right: 15px;
  }

  ul.title-heading li a {
    text-decoration: none;
    position: relative;
  }

  ul.title-heading li a:hover {
    text-decoration: none;
  }

  ul.title-heading li a::before {
    position: absolute;
    content: "";
    height: 2px;
    width: 0;
    background: #0088FF;
    bottom: -5px;
    left: 0;
    right: 0;
    margin: auto;
    transition: .3s;
  }

  ul.title-heading li:hover a::before {
    width: 100%;
  }

  ul.title-heading li.active a {
    color: #0088FF;
    transition: .3s;
  }

  ul.title-heading li.active a::before {
    width: 100%;
  }
</style>
@endpush

@section('dboard_content')

<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile pending-store-deposite-receive">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-9 text-left">
            <h2 class="title-heading">Deposit Receive</h2>
          </div><!-- end .col-md-9 -->
        </div><!-- end .row title-area -->
        <hr>
        <!-- title-area -->
        <div class="row align-items-center">
          <div class="col-md-6 text-left">
            <ul class="title-heading">
              <li class="">
                <a href="{{route('store-deposit.index')}}" class="btn index-btn">Pending Deposit
                </a>
              </li>

            </ul>
          </div><!-- end .col-md-6 -->
        </div><!-- end .row title-area -->
        <!-- break line -->
        <hr>
        <!-- break line -->
        <div class="row">
          {{--<!-- filter-area -->
                    <div class="col-md-12">
                        <div class="row">
                            <!-- search by store -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="search_by_store" id="search_by_store"
                                           class="form-control" placeholder="Search By Store">
                                </div>
                            </div><!-- end search invoice -->

                            <!-- search by deposit id -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="search_by_deposit" id="search_by_deposit"
                                           class="form-control" placeholder="Search By Deposit">
                                </div>
                            </div><!-- end search by deposit id -->

                            <!-- to date -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="date" id="date" name="date" class="form-control">
                                </div>
                            </div><!-- end to date -->

                            <!-- submit button -->
                            <div class="col-md-3">
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn filter-btn">Filter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end filter-area -->--}}

          <!-- data table -->
          <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover" id="deposit-receive-table">
              <thead>
                <tr>
                  <th width="5%" style="border-right-width: 4px;">#</th>
                  <th width="38.5%" style="border-right-width: 4px;">Store Details</th>
                  <th width="33.5%" style="border-right-width: 4px;">Deposit ID</th>
                  <th width="20%" style="border-right-width: 4px;">Status</th>
                  <th width="5%">Action</th>
                </tr>
              </thead>

              <tbody>
                @if(isset($received_deposites))
                @foreach($received_deposites as $key=>$received_deposite)
                <tr>
                  <td>{{ $key + 1 }}</td>
                  <td>
                    @if($received_deposite->store)
                    <h3>{{$received_deposite->store->name}}</h3>
                    @endif
                    <p>Create Date: {{$received_deposite->created_at}}</p>
                  </td>
                  <td class="align-item-center">
                    <h3>Deposit ID: {{$received_deposite->deposit_no}}</h3>
                  </td>

                  <td>{{$received_deposite->status}}</td>
                  <td>
                    {{--<div class="d-flex justify-content-around align-items-center">
                                                    <a href="{{ route('deposit-receive.edit',$received_deposite->id) }}"
                    class="btn details-btn mr-2">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>

                    <a href="#" class="btn download-btn">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </a>
          </div>--}}
          </td>
          </tr>
          @endforeach
          @endif
          </tbody>
          </table>
        </div><!--  end .table -->
      </div><!-- end .row -->
    </div><!-- end .tile-body -->
  </div><!-- end .tile -->
</div><!-- end .col-md-12 -->
</div><!-- end .row -->

@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  $('#deposit-receive-table').DataTable();
</script>

<script>
  $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
</script>

<script>
  $(document).on('click', 'ul li', function() {
    $(this).addClass('active').siblings().removeClass('active');
  });
</script>
@endpush