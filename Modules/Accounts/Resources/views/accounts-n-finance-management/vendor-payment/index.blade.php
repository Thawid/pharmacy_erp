@extends('dboard.index')
@section('title', 'Vendor Payment List')
@push('styles')
    <style>
        table.dataTable td:last-child i {
            font-size: 16px;
            margin: 0;
        }


        input::-webkit-input-placeholder {
            font-size: 16px;
        }

        div.dataTables_wrapper div.dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            font-size: 0;
        }

        div.dataTables_wrapper div.dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            font-size: 0;
        }


        div.dataTables_wrapper div.dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
            margin-left: 10px;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: -1px;
        }

        ul.title-heading {
            display: flex;
            padding: 0;
            margin: 0;
        }

        ul.title-heading li {
            list-style: none;
            margin-right: 15px;
        }

        ul.title-heading li a {
            text-decoration: none;
            position: relative;
        }

        ul.title-heading li a:hover {
            text-decoration: none;
        }

        ul.title-heading li a::before {
            position: absolute;
            content: "";
            height: 2px;
            width: 0;
            background: #0088FF;
            bottom: -5px;
            left: 0;
            right: 0;
            margin: auto;
            transition: .3s;
        }

        ul.title-heading li:hover a::before {
            width: 100%;
        }

        ul.title-heading li.active-tab a {
            color: #0088FF;
            transition: .3s;
        }

        ul.title-heading li.active-tab a::before {
            width: 100%;
        }

    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="">
        <div class="row">
            <div class="col-md-12">
                <div class="tile vendor-invoice-body-tile">
                    <div class="tile-body">
                        <!-- payment tabs -->
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-6">
                                <ul class="title-heading">
                                    <li>
                                        <a href="#">Vendor Payment List</a>
                                    </li>
                                </ul>
                            </div><!-- end .col-md-6 -->
                        </div><!-- end .row -->

                        <!-- break line -->
                        <hr>
                        <!-- break line -->

                        <!-- filter-area -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <!-- select hub -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="search_by_payment_id"
                                                id="search_by_payment_id" placeholder="Search By Product Id">
                                        </div>
                                    </div>

                                    <!-- select store -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select name="select_status" id="select_status" class="form-control">
                                                <option value=""> --Select Status-- </option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- select date -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="date" class="form-control">
                                        </div>
                                    </div>

                                    <!-- submit button -->
                                    <div class="col-md-3">
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn filter-btn">Filter</button>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end filter-area -->

                            <!-- data table -->
                            <div class="col-md-12">
                                <div class="row align-items-center">
                                    <div class="col-md-9 text-left">
                                        <h2 class="title-heading">Vendor Payment List</h2>
                                    </div><!-- end .col-md-9 -->
                                </div><!-- end .row title-area -->
                                <hr>
                                <table class="table table-striped table-bordered table-hover"
                                    id="vendor-payment-data-table">
                                    <thead>
                                        <tr>
                                            <th width="5%" style="border-right-width: 4px;">#</th>
                                            <th width="25%" style="border-right-width: 4px;">Payments Recipet</th>
                                            <th width="25%" style="border-right-width: 4px;">Invoice ID</th>
                                            <th width="20%" style="border-right-width: 4px;">PO Reference</th>
                                            <th width="13%" style="border-right-width: 4px;">Payment Type</th>
                                            <th width="12%">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <h3>Payment ID: 09212</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td class="align-item-center">
                                                <h3>Inv ID: 464684</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td>
                                                PO ID: 732832
                                            </td>

                                            <td>Partial Payment</td>
                                            <td>
                                                <div class="d-flex justify-content-between">
                                                    <a href="#" class="btn details-btn"><i class="fa fa-eye"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn pdf-btn"><i class="fa fa-file-pdf-o"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn download-btn"><i class="fa fa-download"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <h3>Payment ID: 09212</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td class="align-item-center">
                                                <h3>Inv ID: 464684</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td>
                                                PO ID: 732832
                                            </td>

                                            <td>Partial Payment</td>
                                            <td>
                                                <div class="d-flex justify-content-between">
                                                    <a href="#" class="btn details-btn"><i class="fa fa-eye"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn pdf-btn"><i class="fa fa-file-pdf-o"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn download-btn"><i class="fa fa-download"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <h3>Payment ID: 09212</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td class="align-item-center">
                                                <h3>Inv ID: 464684</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td>
                                                PO ID: 732832
                                            </td>

                                            <td>Partial Payment</td>
                                            <td>
                                                <div class="d-flex justify-content-between">
                                                    <a href="#" class="btn details-btn"><i class="fa fa-eye"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn pdf-btn"><i class="fa fa-file-pdf-o"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn download-btn"><i class="fa fa-download"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!--  end .table -->
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>


@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#vendor-payment-data-table').DataTable();
    </script>

    <script>
        $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
        // $(document).on('click', 'vendor-payment-tabs a', function() {
        //     $(this).addClass('acive-vendor-payment-btn').siblings().removeClass('acive-vendor-payment-btn');
        // });

        $(document).on('click', 'ul li', function() {
            $(this).addClass('active-tab').siblings().removeClass('active-tab');
        });
    </script>
@endpush
