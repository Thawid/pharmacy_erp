@extends('dboard.index')
@section('title', 'Create Vendor Payments')
@push('styles')
    <style>
        h1,
        h2,
        h3,
        h4,
        h5 {
            padding: 0;
            margin: 0;
        }

        p {
            padding: 0;
            margin: 0;
        }

        .vendor-invoice-body-tile {
            border-radius: 8px;
        }

        .sumit-btn {
            background: rgba(0, 200, 128, 0.1);
            border: 1px solid #00C880;
            box-sizing: border-box;
            color: #00C880;
            font-size: 14px;
        }

        .sumit-btn:hover {
            color: #00C880
        }

    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="">
        <div class="row">
            <div class="col-md-12">
                <div class="tile vendor-invoice-body-tile">
                    <div class="tile-body">
                        <!-- title-area -->
                        <div class="row align-items-center">
                            <div class="col-md-9 text-left">
                                <h1 class="title-heading">Create Other Payments</h1>
                            </div><!-- end .col-md-9 -->

                            <div class="col-md-3 text-right">
                                <a class="btn index-btn" href="{{ route('vendor-payment.index') }}">Vendor
                                    Payment List</a>
                            </div><!-- end .col-md-3 -->
                        </div><!-- end .row title-area -->
                        <hr>
                        <!-- form-fields -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <!-- first 3 fields -->
                                <div class="row">
                                    <!-- invoice date -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="invoice_date">Invoice Date</label>
                                            <input type="date" class="form-control" name="invoice_date" id="invoice_date">
                                        </div>
                                    </div><!-- end invoice date -->

                                    <!-- invoice number -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="invoice_number">Insert Invoice Number</label>
                                            <input type="text" class="form-control" name="invoice_number"
                                                id="invoice_number">
                                        </div>
                                    </div><!-- end invoice number -->

                                    <!-- invoiced amount -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="invoiced_amount">Invoiced Amount</label>
                                            <input type="text" class="form-control" name="invoiced_amount"
                                                id="invoiced_amount" readonly>
                                        </div>
                                    </div><!-- end invoiced amount -->
                                </div><!-- end .row first 3 fields -->

                                <!-- second 3 fields -->
                                <div class="row">
                                    <!-- due amount -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="due_amount">Due Amount</label>
                                            <input type="text" class="form-control" name="due_amount" id="due_amount"
                                                readonly>
                                        </div>
                                    </div><!-- end due amount -->

                                    <!-- select payment type -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="select_payment_type">Select Payment Type</label>
                                            <select class="form-control" name="select_payment_type"
                                                id="select_payment_type">
                                                <option value="partial">partial</option>
                                            </select>
                                        </div>
                                    </div><!-- end select payment type -->

                                    <!-- pay amount -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pay_amount">Pay Amount</label>
                                            <input type="text" class="form-control" name="pay_amount" id="pay_amount"
                                                placeholder="0.0 BDT">
                                        </div>
                                    </div><!-- end pay amount -->
                                </div><!-- end .row second 3 fields -->

                                <!-- last field -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-contorl">
                                            <label for="payment_notes">Payment Note</label>
                                            <textarea class="form-control" name="payment_notes"
                                                id="payment_notes"></textarea>
                                        </div>
                                    </div>
                                </div><!-- end .row last field -->
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <!-- submit buttons -->
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="btn-area d-flex justify-content-end">
                                    <button class="btn sumit-btn mr-2" type="button">Submit</button>
                                    <button class="btn sumit-btn" type="button">Submit & Prints</button>
                                </div>
                            </div>
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile vendor-invoice-body-tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>
@endsection

@push('post_scripts')
@endpush
