@extends('dboard.index')
@section('title','Update Accounts Settings')

@section('dboard_content')
<div class="tile">
  <div class="row align-items-center">
    <div class="col-md-6">
      <h1 class="title-heading">Accounts Settings</h1>
    </div><!-- end.col-md-6 -->
  </div><!-- end.row -->
  <hr>

  <form action="">
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-6">
        <div class="form-group">
          <label for="accounts-closing-start-from">Accounts Closing Start From</label>
          <input type="date" class="form-control" name="accounts-closing-start-from" id="accounts-closing-start-from">
        </div><!-- end.form-control -->
      </div><!-- end.col-xl-6 -->

      <div class="col-xl-6 col-lg-6 col-md-6">
        <div class="form-group">
          <label for="accounts-closing-ending-month">Accounts Closing Ending Month</label>
          <input type="date" class="form-control" name="accounts-closing-ending-month" id="accounts-closing-ending-month">
        </div><!-- end.form-control -->
      </div><!-- enc.col-xl-6 -->
    </div><!-- end.row -->
    <hr>

    <div class="row">
      <div class="col-md-12 text-right">
        <button type="submit" class="btn index-btn">Update</button>
      </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
  </form><!-- end.form -->
</div><!-- end.tile -->
@endsection