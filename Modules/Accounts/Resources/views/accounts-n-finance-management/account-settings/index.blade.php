@extends('dboard.index')
@section('title','Update Accounts Settings')

@section('dboard_content')
    <div class="tile">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h1 class="title-heading">Accounts Settings</h1>
            </div><!-- end.col-md-6 -->
        </div><!-- end.row -->
        <hr>

        <form action="{{ route('update-account-setting') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="form-group">
                        <label for="accounts-closing-start-from">Accounts Closing Start From</label>
                        <select name="start_month" class="form-control" required>
                            <option value="">-- Select Month --</option>
                            <option value="01"  {{ $start_month == '01' ? 'selected="seledted"' : '' }}>January</option>
                            <option value="02"  {{ $start_month == '02' ? 'selected="seledted"' : '' }}>February</option>
                            <option value="03"  {{ $start_month == '03' ? 'selected="seledted"' : '' }}>March</option>
                            <option value="04"  {{ $start_month == '04' ? 'selected="seledted"' : '' }}>April</option>
                            <option value="05"  {{ $start_month == '05' ? 'selected="seledted"' : '' }}>May</option>
                            <option value="06"  {{ $start_month == '06' ? 'selected="seledted"' : '' }}>June</option>
                            <option value="07"  {{ $start_month == '07' ? 'selected="seledted"' : '' }}>July</option>
                            <option value="08"  {{ $start_month == '08' ? 'selected="seledted"' : '' }}>August</option>
                            <option value="09"  {{ $start_month == '09' ? 'selected="seledted"' : '' }}>September</option>
                            <option value="10"  {{ $start_month == '10' ? 'selected="seledted"' : '' }}>October</option>
                            <option value="11"  {{ $start_month == '11' ? 'selected="seledted"' : '' }}>November</option>
                            <option value="12"  {{ $start_month == '12' ? 'selected="seledted"' : '' }}>December</option>
                        </select>
                    </div><!-- end.form-control -->
                </div><!-- end.col-xl-6 -->

                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="form-group">
                        <label for="accounts-closing-ending-month">Accounts Closing Ending Month</label>
                        <select name="end_month" class="form-control" required>
                            <option value="">-- Select Month --</option>
                            <option value="01"  {{ $end_month == '01' ? 'selected="seledted"' : '' }}>January</option>
                            <option value="02"  {{ $end_month == '02' ? 'selected="seledted"' : '' }}>February</option>
                            <option value="03"  {{ $end_month == '03' ? 'selected="seledted"' : '' }}>March</option>
                            <option value="04"  {{ $end_month == '04' ? 'selected="seledted"' : '' }}>April</option>
                            <option value="05"  {{ $end_month == '05' ? 'selected="seledted"' : '' }}>May</option>
                            <option value="06"  {{ $end_month == '06' ? 'selected="seledted"' : '' }}>June</option>
                            <option value="07"  {{ $end_month == '07' ? 'selected="seledted"' : '' }}>July</option>
                            <option value="08"  {{ $end_month == '08' ? 'selected="seledted"' : '' }}>August</option>
                            <option value="09"  {{ $end_month == '09' ? 'selected="seledted"' : '' }}>September</option>
                            <option value="10"  {{ $end_month == '10' ? 'selected="seledted"' : '' }}>October</option>
                            <option value="11"  {{ $end_month == '11' ? 'selected="seledted"' : '' }}>November</option>
                            <option value="12"  {{ $end_month == '12' ? 'selected="seledted"' : '' }}>December</option>
                        </select>
                    </div><!-- end.form-control -->
                </div><!-- enc.col-xl-6 -->
            </div><!-- end.row -->
            <hr>

            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn index-btn">Update</button>
                </div><!-- end.col-md-12 -->
            </div><!-- end.row -->
        </form><!-- end.form -->
    </div><!-- end.tile -->
@endsection
