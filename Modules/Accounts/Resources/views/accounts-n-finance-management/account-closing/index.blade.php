@extends('dboard.index')
@section('title','Closing Accounts List')

@push('styles')
<style>
  thead th{
    font-size: 16px;
    font-weight: bolder;
  }
</style>

@endpush

@section('dboard_content')
  <div class="tile">
    <div class="row align-items-center">
      <div class="col-md-6">
        <h1 class="title-heading">List  of Account Closings</h1>
      </div><!-- end.col-md-6 -->

      <div class="col-md-6 text-right">
        <a href="{{ route('create-account-closing') }}" class="btn index-btn">Create Account Closings</a>
      </div><!-- end.col-md-6 -->
    </div><!-- end.row -->
    <hr>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <th>SL</th>
            <th>Closing Months</th>
            <th>Closing Year</th>
            <th>Closed By</th>
            <th>Closed At</th>
          </thead>
          <tbody>
          @foreach($account_closings as $k => $account_closing)
              <tr>
                  <td>{{ $k+1 }}</td>
                  <td>{{ $account_closing->closing_start_from .'-'.$account_closing->closing_ends_at }}</td>
                  <td>{{ $account_closing->closing_start_year == $account_closing->closing_end_year ? $account_closing->closing_end_year : $account_closing->closing_start_year .'-'.$account_closing->closing_end_year }}</td>
                  <td>{{ $account_closing->user->name ?? 'Unknown' }}</td>
                  <td>{{ $account_closing->created_at }}</td>
              </tr>
          @endforeach
          </tbody>
        </table><!-- end table -->
      </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
  </div><!-- end.tile -->
@endsection
