@extends('dboard.index')
@section('title','Create Closing Accounts')

@section('dboard_content')
<div class="tile">
  <div class="row align-items-center">
    <div class="col-md-6">
      <h1 class="title-heading">Create Closing Accounts</h1>
    </div><!-- end.col-md-6 -->

    <div class="col-md-6 text-right">
      <a href="{{ route('account-closings') }}" class="btn index-btn">Closed Accounts List</a>
    </div><!-- end.col-md-6 -->
  </div><!-- end.row -->
  <hr>
    <form action="{{ route('store-account-closing') }}" method="post">
        @csrf
        <div class="form">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label for="closing-starting-month">Closing Starting Month</label>
                        <input type="text" class="form-control" name="closing_starting_month" value="{{$start}}" readonly>
                    </div><!-- end.form-group -->
                </div><!-- end.col-md-4 -->

                <div class="col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label for="closing-ending-month">Closing Ending Month</label>
                        <input type="text" class="form-control" name="closing_ending_month" value="{{$end}}" readonly>
                    </div><!-- end.form-group -->
                </div><!-- end.col-md-4 -->

                <div class="col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group">
                        <label for="year">Year</label>
                        <input type="number" class="form-control" name="year" id="year" maxlength="4" minlength="4">
                    </div><!-- end.form-group -->
                </div><!-- end.col-md-4 -->
            </div><!-- end.row -->
            <hr>

            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn index-btn">Submit</button>
                </div><!-- end.col-md-12 -->
            </div><!-- end.row -->
        </div>
    </form>

</div><!-- end.tile -->
@endsection
