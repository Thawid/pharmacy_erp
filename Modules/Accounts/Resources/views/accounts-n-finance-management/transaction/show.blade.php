@extends('dboard.index')

@section('title', 'Transaction Deatils')

@push('styles')
<style>

</style>
@endpush

@section('dboard_content')
<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header justify-content-between d-flex align-items-center">
                <h2 class="title-heading">Transaction Detail</h2>
                <a href="{{ route('transactions.index') }}" class="btn index-btn">Back</a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-hover table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Type</th>
                      </tr><!-- end tr -->
                    </thead><!-- end thead -->

                    <tbody>
                      <tr>
                        <td>
                          <strong class="text-right">Expense ID: </strong>0001
                        </td>
                        <td>Cash-Out</td>
                      </tr>

                      <tr>
                        <td>
                          <strong class="text-right">Deposit ID: </strong>0001
                        </td>
                        <td>Cash-Out</td>
                      </tr>

                      <tr>
                        <td>
                          <strong>Sales Invoice: </strong>0001
                        </td>
                        <td>Cash-Out</td>
                      </tr>
                    </tbody><!-- end tbody -->
                  </table><!-- end table -->
                </div><!-- end .table-responsive -->
              </div>
            </div>
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
  </div><!-- end .row -->
  @endsection


  @push('post_scripts')
  <!-- Page specific javascripts-->
  <!-- Data table plugin-->
  <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript">
    $('#sampleTable').DataTable();
  </script>
  @endpush