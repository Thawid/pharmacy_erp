@extends('dboard.index')

@section('title', 'Transaction')

@push('styles')
    <style>
    .widget-small.info.coloured-icon {
        background-color: #F3F8FF;
        color: #2a2a2a;
        box-shadow: rgb(38, 57, 77) 0px 20px 15px -20px;
      }
    </style>
@endpush

@section('dboard_content')
    <form action="{{route('transaction.search')}}" method="GET">
        <!-- data-area -->
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">

                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="title-heading">Transaction List</h2>
                            </div>
                        </div><!-- end.row -->
                        <hr>

                          <div class="row">
                            <div class="col-md-4 col-lg-4 col-xl-4">
                                <div class="widget-small info coloured-icon">
                                    <i class="icon fa fa-money fa-3x"></i>
                                    <div class="info">
                                        <h4>Cash-In</h4>
                                        <p><b>
                                        @if(isset($cashIn[0]))
                                          <p>
                                            {{ $cashIn[0]->total_amount}} BDT
                                          </p>
                                        @endif
                                        </b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xl-4">
                                <div class="widget-small info coloured-icon">
                                    <i class="icon fa fa-bank fa-3x"></i>
                                    <div class="info">
                                        <h4>Cash-Out</h4>
                                        <p><b>
                                        @if(isset($cashOut[0]))
                                          <p> 
                                            {{ $cashOut[0]->total_amount}} BDT
                                          </p>
                                        @endif
                                        </b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xl-4">
                                <div class="widget-small info coloured-icon">
                                    <i class="icon fa fa-balance-scale fa-3x"></i>
                                    <div class="info">
                                        <h4>Balance</h4>
                                        <p><b>
                                        {{ abs($cashIn[0]->total_amount - $cashOut[0]->total_amount) }} BDT
                                        </b></p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end.row -->

                        <div class="row">
                            <!-- select date -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">From Date</label>
                                    <input type="date" class="form-control" name="from_date">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">To Date</label>
                                    <input type="date" class="form-control" name="to_date">
                                </div>
                            </div>

                            <!-- submit button -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">&nbsp;</label>
                                    <button type="submit" class="btn filter-btn">Search</button>
                                </div>
                            </div>
                            <br><br>
                            <hr>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered" id="sampleTable">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Date</th>
                                            <th>Transaction Details</th>
                                            <th>Transaction Type</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr><!-- end tr -->
                                        </thead><!-- end thead -->

                                        <tbody>
                                        @foreach($transactions as $key=>$transaction)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{date('d-m-Y',strtotime($transaction->created_at))}}</td>
                                                <td>
                                                    <p>{{$transaction->sales_invoice??$transaction->deposit_no}}</p>
                                                    <p>{{$transaction->expense_invoice??$transaction->other_invoice}}</p>
                                                </td>
                                                <td>{{$transaction->transaction_type == 1 ? 'Cash Out' : 'Cash In' }}</td>
                                                <td>{{$transaction->amount}} <strong>BDT</strong></td>
                                                <td>
                                                    {{--<div class="d-md-flex justify-content-around">
                                                        <a href="{{ route('transactions.show', 1) }}"
                                                           class="btn details-btn"><i class="fa fa-eye"></i></a>
                                                    </div>--}}
                                                    {{--badhon vai says action no need--}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody><!-- end tbody -->
                                    </table><!-- end table -->
                                </div><!-- end .table-responsive -->
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </div><!-- end .tile -->
                </div><!-- end .col-md-12 -->
            </div><!-- end .row -->
        </div><!-- end .row -->
    </form>

@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
@endpush
