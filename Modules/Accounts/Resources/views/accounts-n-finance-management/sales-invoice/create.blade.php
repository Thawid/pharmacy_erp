@extends('dboard.index')
@section('title', '')
@push('styles')
    <style>
    </style>
@endpush
@section('dboard_content')

@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#sales-invoice-data-table').DataTable();
    </script>
@endpush
