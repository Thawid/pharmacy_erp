@extends('dboard.index')
@section('title', 'Sales Invoice List')
@push('styles')
<style>
  .sale-invoice-title-tile h2 {
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 23px;
    color: #294A65;
  }

  .sale-invoice-body-tile {
    border-radius: 8px;
  }

  .thead-color {
    background: #F4F4F4;
  }

  thead {
    color: #294A65;
  }

  tbody {
    color: #696F8C;
  }

  .table td {
    vertical-align: middle;
  }

  table.dataTable td:last-child i {
    font-size: 16px;
    margin: 0;
  }


  input::-webkit-input-placeholder {
    font-size: 16px;
  }

  div.dataTables_wrapper div.dataTables_filter label {
    font-weight: normal;
    white-space: nowrap;
    text-align: left;
    font-size: 0;
  }

  div.dataTables_wrapper div.dataTables_length label {
    font-weight: normal;
    text-align: left;
    white-space: nowrap;
    font-size: 0;
  }


  div.dataTables_wrapper div.dataTables_info {
    padding-top: 0.85em;
    white-space: nowrap;
    margin-left: 10px;
  }

  tbody h3 {
    font-size: 16px;
  }

  table.dataTable td:last-child i {
    font-size: 16px;
    margin: -1px;
  }

  thead th {
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
  }
</style>
@endpush
@section('dboard_content')

<!-- data-area -->
<form action="{{ route('sales.store.invoice') }}" method="POST">
  @csrf
  <div class="row">
    <div class="col-md-12">
      <div class="tile sale-invoice-body-tile">
        <div class="tile-body">

          <!-- title-area -->
          <div class="row align-items-center">
            <div class="col-md-9">
              <h2 class="title-heading">Today's Stores Sales Invoice</h2>
            </div><!-- end .col-md-9 -->

            <div class="col-md-3 text-right">
              <a class="btn create-btn" href="{{ route('store.invoice.archive') }}">Invoice Archive</a>
            </div><!-- end .col-md-3 -->
          </div><!-- end .row title-area -->
          <hr>

          <!-- filter-area -->
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <!-- select hub -->
                <div class="col-md-3">
                  <div class="form-group">
                    <select name="hub_id" id="select_hub" class="form-control">
                      <option value="" selected disabled>Select Hub</option>
                      @foreach($hubs as $hub)
                      <option value="{{$hub->id}}">{{$hub->name}}</option>
                      @endforeach
                    </select>
                  </div><!-- end.select-hub -->
                </div><!-- end.col-md-3 -->

                <!-- select store -->
                <div class="col-md-3">
                  <div class="form-group">
                    <select name="store_id" id="select_store" class="form-control">
                      <option value="" selected disabled>Select Store</option>
                      @foreach($stores as $store)
                      <option value="{{$store->id}}">{{$store->name}}</option>
                      @endforeach
                    </select>
                  </div><!-- end.select store -->
                </div><!-- end.col-md-3 -->

                <!-- select date -->
                <div class="col-md-3">
                  <div class="form-group">
                    <input type="date" class="form-control" name="created_at">
                  </div>
                </div>

                <!-- submit button -->
                <div class="col-md-3">
                  <div class="d-flex justify-content-end">
                    <button type="submit" class="btn filter-btn">Filter</button>
                  </div>
                </div>
              </div>
            </div><!-- end filter-area -->

            <!-- data table -->
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-hover" id="sales-invoice-data-table">
                <thead>
                  <tr>
                    <th width="10%" style="border-right-width: 4px;">#</th>
                    <th width="25%" style="border-right-width: 4px;">Store Info</th>
                    <th width="20%" style="border-right-width: 4px;">Hub</th>
                    <th width="20%" style="border-right-width: 4px;">No. of Invoices</th>
                    <th width="20%" style="border-right-width: 4px;">Amount</th>
                    <th width="5%">Action
                    </th>
                  </tr>
                </thead>

                <tbody>
                  @if(count($store_invoices) > 0)
                  @foreach($store_invoices as $key=>$store_invoice)
                  <tr>
                    <td>{{$key + 1}}</td>
                    <td class="align-item-center">
                      <h3>{{$store_invoice->store_name}}</h3>
                      <p>ID {{$store_invoice->id}}</p>
                      <p>{{$store_invoice->address}}</p>
                    </td>
                    <td>
                      {{$store_invoice->hub_name}}
                    </td>

                    <td>{{$store_invoice->total_invoice}}</td>
                    <td>{{$store_invoice->total_amount}} BDT</td>
                    <td style="margin: auto">
                      <a href="{{ route('sales-invoice.show', $store_invoice->store_id) }}" class="btn details-btn">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach

                  @endif
                </tbody>
              </table>
            </div><!--  end .table -->
          </div><!-- end .row -->
        </div><!-- end .tile-body -->
      </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
  </div><!-- end .row -->
</form>


@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  $('#sales-invoice-data-table').DataTable();
</script>

<script>
  $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
</script>
@endpush