@extends('dboard.index')
@section('title', 'Sales Invoice Details')
@push('styles')
    <style>

        .sales-invoice-details-body-tile {
            border-radius: 8px;
        }

        .thead-color {
            background: #F4F4F4;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin-left: 0;
        }


        input::-webkit-input-placeholder {
            font-size: 16px;
        }

        div.dataTables_wrapper div.dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            font-size: 0;
        }

        div.dataTables_wrapper div.dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            font-size: 0;
        }


        div.dataTables_wrapper div.dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
            margin-left: 10px;
        }

        tbody h3 {
            font-size: 16px;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: -1px;
        }

        thead th {
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
        }

    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="">
        <div class="row">
            <div class="col-md-12">
                <div class="tile sales-invoice-details-body-tile">
                    <div class="tile-body">
                        <!-- title-area -->
                        <div class="row align-items-center">
                            <div class="col-md-6 text-left">
                                <h2 class="title-heading">{{$storeInformation->store[0]->name}}</h2>
                                <p>ID: {{$storeInformation->store[0]->id}}</p>
                                <p>{{$storeInformation->store[0]->address}}</p>
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-6 text-right">
                                <div class="amount-area d-flex justify-content-between">
                                    <div class="today-amount w-100 mr-4">
                                        <label for="">Today Inoive Amount</label>
                                        <input type="text" class="form-control text-right"
                                               value="{{$today_invoice_amount[0]->today_invoice_amount}} BDT" readonly>
                                    </div>

                                    <div class="total-amount w-100">
                                        <label for="">Total Inoive Amount</label>
                                        <input type="text" class="form-control text-right"
                                               value="{{$total_invoice_amount[0]->total_invoice_amount}} BDT"
                                               readonly>
                                    </div>
                                </div>
                            </div><!-- end .col-md-6 -->
                        </div><!-- end .row title-area -->
                        <hr>

                        <div class="row">
                            <!-- data table -->
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover"
                                       id="vendor-invoice-data-table">
                                    <thead>
                                    <tr>
                                        <th width="5%" style="border-right-width: 4px;">#</th>
                                        <th width="40%" style="border-right-width: 4px;">Invoice Details</th>
                                        <th width="40%" style="border-right-width: 4px;">Amount</th>
                                        <th width="15%"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(isset($storeInvoices))
                                        @foreach($storeInvoices as $key=>$storeInvoice)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>
                                                    <h3>Inv ID: {{$storeInvoice->invoice_no}}</h3>
                                                    <p>Create Date: {{$storeInvoice->created_at}}</p>
                                                </td>
                                                <td>{{$storeInvoice->amount}}</td>
                                                <td>
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        {{--<a href="#"
                                                           class="btn details-btn">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>--}}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!--  end .table -->
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>


@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#vendor-invoice-data-table').DataTable();
    </script>

    <script>
        $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
    </script>
@endpush
