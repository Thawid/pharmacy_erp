<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Store\Entities\Store;
use Carbon\Carbon;

class StoreInvoice extends Model
{

    protected $guarded = [];

    public function store()
    {
        return $this->hasMany(Store::class,'id','store_id')->select(['id','name','address']);
    }

    public function hub()
    {
        return $this->hasMany(Store::class,'id','hub_id')->select(['id','name','address']);
    }

    public function storeInfo()
    {
        return $this->belongsTo(Store::class,'store_id','id');
    }


    public function getCreatedAtAttribute(){
        $carbondate = Carbon::parse($this->attributes['created_at']);
        $past = $carbondate->format('d-m-Y  h:i A');
        return $past;
    }

}
