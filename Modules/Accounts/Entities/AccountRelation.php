<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;

class AccountRelation extends Model
{
    protected $table = 'accounts_accounts_relation';
    protected $guarded = [];

}
