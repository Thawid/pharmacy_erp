<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;

class AccountStatement extends Model
{
    public $table = "account_statements";
    protected $fillable = ['company_id','company_type','account_closing_id','closing_year','account_type','balance'];
}
