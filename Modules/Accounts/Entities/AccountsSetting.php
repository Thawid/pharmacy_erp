<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;

class AccountsSetting extends Model
{

    protected $fillable = ['start_month','end_month','company_id','company_type'];
}
