<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;
class AccountClosing extends Model
{
    public $table = "account_closings";

    protected $fillable = [
        'company_id',
        'company_type',
        'closing_start_year',
        'closing_end_year',
        'closing_start_from',
        'closing_ends_at',
        'created_by',
    ];
}
