<?php

namespace Modules\Accounts\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Vendor\Entities\Vendor;

class VendorInvoice extends Model
{

    protected $guarded = [];

    public function getCreatedAtAttribute(){
        $carbondate = Carbon::parse($this->attributes['created_at']);
        $past = $carbondate->format('d-m-Y  h:i A');
        return $past;
    }

    public function vendorPayment()
    {
        return $this->hasOne(VendorPayment::class,'id','vendor_invoice_id')->with('vendor');
    }

    public function vendor()
    {
        return $this->hasOne(Vendor::class,'id','vendor_id');
    }



}
