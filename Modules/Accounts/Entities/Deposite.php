<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Store\Entities\Store;

class Deposite extends Model
{
    protected $guarded = [];

    public function store()
    {
        return $this->hasOne(Store::class,'id','store_id')->select(['id','name','address']);
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id','deposite_id');
    }

}
