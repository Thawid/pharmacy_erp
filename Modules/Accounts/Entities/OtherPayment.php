<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;

class OtherPayment extends Model
{
    protected $guarded = [];

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id','other_payment_id');
    }

}
