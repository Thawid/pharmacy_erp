<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Model;

class VendorPayment extends Model
{
    protected $guarded = [];

    public function vendorInvoice()
    {
        return $this->belongsTo(VendorInvoice::class,'vendor_invoice_id');
    }

}
