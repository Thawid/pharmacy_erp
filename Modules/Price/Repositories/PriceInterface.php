<?php
namespace Modules\Price\Repositories;
interface PriceInterface
{
  public function index();
  public function product_search($request);
}
