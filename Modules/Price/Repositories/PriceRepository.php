<?php
namespace Modules\Price\Repositories;

use Modules\Price\Entities\Price;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductGeneric;

class PriceRepository implements PriceInterface
{
    public function index()
    {
        return Price::with('product')->orderBy('id','DESC')->get();
    }

    public function product_search($request)
    {
        $request->validate([
            'product_type' => 'required',
            'product_variation' => 'required'
        ]);
        $searchstr = $request->product_search;
        if (strlen($searchstr) == 0) {
            return $search = [];
        }
        if ($request->product_variation == 'product') {
            $products = Product::with(['purchase_order.uom'])->where('product_type_id', $request->product_type)->where('product_name', 'LIKE', '%' . $searchstr . "%")->get();
            $dataitem = [];

            foreach ($products as $row) {
                $item = array(
                    'id' => $row->id,
                    'product_variation_name' => $row->product_name,
                    'uom_name' => isset($row->purchase_order->uom->uom_name)?$row->purchase_order->uom->uom_name:null,
                    'unit_price' => isset($row->purchase_order->uom_price)?$row->purchase_order->uom_price:0,
                );
                array_push($dataitem, $item);
            }

            return response()->json($dataitem);
        }

        $product_type = $request->product_type;
        if ($request->product_variation == 'generic_product') {
            $search = ProductGeneric::where('status', 1)->where('name', 'LIKE', '%' . $searchstr . "%")->with(['products' => function ($query) use ($product_type) {
                $query->with(['purchase_order.uom'])->where('product_type_id', $product_type)->get();
            }])->get();

            $dataitem = [];
            if (count($search) > 0) {
                foreach ($search as $data) {
                    foreach ($data->products as $row) {
                        $item = array(
                            'id' => $row->id,
                            'product_variation_name' => $row->product_name,
                            'uom_name' => $row->purchase_order->uom->uom_name?$row->purchase_order->uom->uom_name:0,
                            'unit_price' => $row->purchase_order->uom_price?$row->purchase_order->uom_price:0,
                        );
                        array_push($dataitem, $item);
                    }
                }
            }
            return response()->json($dataitem, 200);
        }
    }
    
}
