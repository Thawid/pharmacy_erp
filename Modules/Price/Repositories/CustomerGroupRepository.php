<?php


namespace Modules\Price\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Price\Http\Requests\CreateCustomerGroupRequest;
use Modules\Price\Http\Requests\UpdateCustomerGroupRequest;
use Illuminate\Http\Request;

class CustomerGroupRepository implements CustomerGroupInterface
{
    public function index()
    {
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $request->validate([
            'customer_group_name'=>'required',
            'customer_group_type'=>'required',
        ]);
        $current_date = Carbon::now()->toDateTimeString();

        // if($request->has('reg_from_date') && $request->has('reg_to_date')){
        //     $request->validate([
        //         'reg_from_date'=>'required',
        //         'reg_to_date'=>'required',
        //     ],[
        //         'reg_from_date.required'=>'Registration from date not be empty!',
        //         'reg_to_date.required'=>'Registration to date not be empty!'
        //     ]);
        // }


        
        if($request->has('reg_from_date') && $request->has('reg_to_date')){
            $request->validate([
                'min_order'=>'required',
                'max_order'=>'required',
            ],[
                'min_order.required'=>'Min Order field must not be empty!',
                'max_order.required'=>'Max Order field must not be empty!'
            ]);
        }


        
        if($request->has('min_expense') && $request->has('max_expense')){
            $request->validate([
                'min_expense'=>'required',
                'max_expense'=>'required',
            ],[
                'min_expense.required'=>'Min Expense field must not be empty!',
                'max_expense.required'=>'Max Expense field must not be empty!'
            ]);
        }

        $data = [
            'group_name'    => $request->customer_group_name,
            'reg_from'      => $request->reg_from_date,
            'reg_to'        => $request->reg_to_date,
            'min_order'     => $request->min_order,
            'max_order'     => $request->max_order,
            'min_expense'   => $request->min_amount,
            'max_expense'   => $request->max_amount,
            'location'      => $request->location,
            'customer_type' => $request->customer_group_type,
            'created_at'    => $current_date,
        ];

        DB::table('customer_groups')->insert($data);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'customer_group_name'=>'required',
            'customer_group_type'=>'required',
        ]);

        $current_date = Carbon::now()->toDateTimeString();

        $data = [
            'group_name'    => $request->customer_group_name,
            'reg_from'      => $request->reg_from_date,
            'reg_to'        => $request->reg_to_date,
            'min_order'     => $request->min_order,
            'max_order'     => $request->max_order,
            'min_expense'   => $request->min_amount,
            'max_expense'   => $request->max_amount,
            'location'      => $request->location,
            'customer_type' => $request->customer_group_type,
            'updated_at'    => $current_date,
        ];

        DB::table('customer_groups')->where('id', $id)->update($data);
    }

    public function destroy($id)
    {
        //
    }
}
