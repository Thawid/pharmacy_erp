<?php


namespace Modules\Price\Repositories;


use Modules\Price\Http\Requests\CreateCustomerGroupRequest;
use Modules\Price\Http\Requests\UpdateCustomerGroupRequest;
use Illuminate\Http\Request;

interface CustomerGroupInterface
{
    public function index();

    public function create();

    public function store(Request $request);

    public function show($id);

    public function edit($id);

    public function update(Request $request, $id);

    public function destroy($id);
}
