<?php

namespace Modules\Price\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Price\Repositories\CustomerGroupInterface;
use Modules\Price\Repositories\CustomerGroupRepository;

class PriceRepositoryServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(CustomerGroupInterface::class, CustomerGroupRepository::class);
        $this->app->bind(PriceInterface::class, PriceRepository::class);
    }


    public function provides()
    {
        return [];
    }
}
