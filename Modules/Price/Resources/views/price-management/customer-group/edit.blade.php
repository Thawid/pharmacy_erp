@extends('dboard.index')
@section('title', 'Update Customer Group')
@push('styles')
<style>
  .mt-30 {
    margin-top: 29px;
  }

  .-ml-60 {
    margin-left: -60px;
  }

  i {
    font-size: 18px;
  }

  @media (min-width: 992px) and (max-width: 1199px) {
    .reg-date-wrapper input {
      width: 160px;
    }
  }
</style>
@endpush
@section('dboard_content')


<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <div class="row align-items-center">
          <!-- title-area -->
          <div class="col-md-6">
            <h4 class="title-heading">Update Customer Group</h4>
          </div><!-- end .col-md-6 -->

          <div class="col-md-6 text-right">
            <a class="btn index-btn" href="{{ route('customer-group.index') }}">Back</a>
          </div><!-- end .col-md-6 -->
        </div><!-- end .row -->
        <hr>

        <!-- data-area -->
        <form action="{{ route('customer-group.update', $customer_group->id) }}" method="POST">
          @csrf
          @method('PUT')
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="reg-date-wrapper" class="form-label">Registration Date</label>
                <div class="reg-date-wrapper d-md-flex">
                  <input type="date" class="form-control mr-2" name="reg_from_date" value="{{ $customer_group->reg_from }}">
                  <input type="date" class="form-control" name="reg_to_date" value="{{ $customer_group->reg_to }}">
                </div><!-- .reg-date-wrapper -->
              </div><!-- end .form-group -->

              <div class="form-group">
                <label for="order_behave" class="forget-form">Order Behavior <span class="text-danger">*</span></label>
                <div class="order-behave-group d-md-flex">
                  <input type="text" class="form-control mr-2" name="min_order" value="{{ $customer_group->min_order }}" placeholder="minimum order" onkeypress="return (event.charCode !=8 && event.charCode ==0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                  <input type="text" class="form-control" name="max_order" value="{{ $customer_group->max_order }}" placeholder="maximum order" onkeypress="return (event.charCode !=8 && event.charCode ==0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                </div><!-- order-behave-group -->
              </div><!-- end .form-group -->

              <div class="form-group">
                <label for="customer-group-name" class="forget-form">Customer Group Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="customer_group_name" id="customer-group-name" value="{{ $customer_group->group_name }}" placeholder="Enter A Group Name">
              </div><!-- end form-group -->
            </div><!-- end .col-md-6 -->

            <div class="col-md-6">
              <div class="form-group">
                <label for="location" class="forget-form">Location</label>
                <input type="text" class="form-control" name="location" value="{{ $customer_group->location }}" placeholder="Dhaka, Bangladesh">
              </div><!-- end form-group -->

              <div class="form-group">
                <label for="expense-behave" class="forget-form">Expense Behavior</label>
                <div class="expense-behave-group d-md-flex">
                  <input type="text" class="form-control mr-2" name="min_amount" value="{{ $customer_group->min_expense }}" placeholder="minimum amount" onkeypress="return (event.charCode !=8 && event.charCode ==0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                  <input type="text" class="form-control" name="max_amount" value="{{ $customer_group->max_expense }}" placeholder="maximum amount" onkeypress="return (event.charCode !=8 && event.charCode ==0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                </div><!-- end .expense-behave-group -->
              </div><!-- end .form-group -->

              <div class="form-group">
              <button class="btn index-btn mt-30 form-control" type="submit">Update</button>
              </div>
            </div><!-- end.col-md-6 -->
          </div><!-- .row -->

          <div class="row">
              <div class="col-md-4">
                  <div class="radio-button-area d-md-flex align-content-center align-items-center">
                      <div class="form-check form-check-inline">
                          <!-- <input class="form-check-input" type="radio" name="customer_group_type"
                                  id="all_new_customer"
                                  value="1">
                          <label class="form-check-label" for="all_new_customer">
                              All New Customer
                          </label> -->
                      </div><!-- end .form-check -->

                      <div class="form-check form-check-inline">
                          <input class="form-check-input" type="hidden" checked name="customer_group_type" id="all_customer"
                                  value="2">
                          <!-- <label class="form-check-label" for="all_customer">
                              All Customer
                          </label> -->
                      </div><!-- end form-check -->
                  </div><!-- end .radio-button-area -->
              </div><!-- end .col-md-4 -->
          </div><!-- end .row -->
        </form><!-- end form -->
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->


@endsection


@push('post_scripts')
<!-- select2 -->
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<!-- select2 configuration -->
<script>
  $(document).ready(function() {
    $(".single-select").select2({
      width: '100%'
    });
  });
</script>
@endpush