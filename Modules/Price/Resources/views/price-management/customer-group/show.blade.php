@extends('dboard.index')

@section('title', 'Customer Group List')

@push('styles')
    

    <style>
        td {
            border: none
        }

    </style>
@endpush

@section('dboard_content')

<!-- data-area -->
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Customer Group List</h2>
                    </div><!-- end col-md-6 -->

                    <div class="col-md-6 text-right">
                        <a class="btn create-btn" href="{{ route('customer-group.create') }}">Add New Customer Gruop</a>
                    </div><!-- ene .col-md-6 -->
                </div><!-- end .row -->

                <!-- body -->
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th width="10%">#</th>
                                        <th width="25%">Created Date</th>
                                        <th width="25%">Group Name</th>
                                        <th width="10%">Action</th>
                                    </tr><!-- end tr -->
                                </thead><!-- end thead -->
                            </table><!-- end table -->
                        </div><!-- end .table-responsive -->
                    </div><!-- end.col-md-12 -->
                </div><!-- end.row -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->



@endsection

@push('post_scripts')

@endpush
