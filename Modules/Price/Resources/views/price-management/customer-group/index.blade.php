@extends('dboard.index')

@section('title', 'Customer Group List')

@push('styles')
@endpush

@section('dboard_content')

<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Customer Group List</h2>
          </div><!-- end col-md-6 -->

          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{ route('customer-group.create') }}">Add New Customer Gruop</a>
          </div><!-- ene .col-md-6 -->
        </div><!-- end .row -->
        <hr>

        <!-- body -->
        <div class="row mt-4">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover bg-white table-sm" id="sampleTable">
                <thead>
                  <tr>
                    <th width="10%">#</th>
                    <th>Created Date</th>
                    <th>Group Name</th>
                    <th>Action</th>
                  </tr>
                </thead><!-- end thead -->

                @if (isset($customer_groups))
                @forelse ($customer_groups as $key => $customer_group)
                <tbody>
                  <tr>
                    <td>
                      @if (isset($key))
                      {{ $key + 1 }}
                      @endif
                    </td>
                    <td>
                      <p>
                        @if (isset($customer_group->created_at))
                        {{ $customer_group->created_at }}
                        @endif
                      </p>
                      <p>
                        @if (isset($customer_group->created_at))
                        {{ \Carbon\Carbon::parse($customer_group->created_at)->diffForHumans() }}
                        @endif
                      </p>
                    </td>
                    <td>
                      <p>
                        @if (isset($customer_group->group_name))
                        {{ $customer_group->group_name }}
                        @endif

                      </p>
                    </td>

                    <td>
                      <div class="d-md-flex justify-content-around align-items-center">
                        <a href="{{ route('customer-group.edit', $customer_group->id) }}" class="btn edit-btn">
                          <i class="fa fa-pencil"></i></a>
                        <a href="{{ route('customer-group.show', $customer_group->id) }}" class="btn details-btn"><i class="fa fa-eye"></i></a>
                        <a href="{{ route('customer-group.destroy', $customer_group->id) }}" class="btn edit-btn"><i class="fa fa-refresh"></i></a>
                      </div>
                    </td>
                  </tr>
                </tbody><!-- end tbody -->
                @empty
                <h2 class="text-center">No Data Found!</h2>
                @endforelse
                @endif
              </table><!-- end table -->
            </div><!-- end .table-responsive -->
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
  </div><!-- end .row -->



  @endsection

  @push('post_scripts')

  @endpush