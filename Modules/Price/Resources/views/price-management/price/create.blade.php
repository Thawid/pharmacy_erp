@extends('dboard.index')
@section('title', 'Create Price')
@push('styles')

<style>
  nav>.nav-tabs>a.nav-item {
    font-size: 16px;
    font-weight: 600;
  }


  .c-ml-1 {
    margin-left: 1px;
  }

  .mt-25 {
    margin-top: 25px;
  }

  .mt-28 {
    margin-top: 28px;
  }

  .mt-35 {
    margin-top: 35px;
  }

  .mr-30 {
    margin-right: 30px;
  }

  .-ml-15 {
    margin-left: -15px;
  }

  #search_btn {
    font-size: 16px;
    color: #006ABD;
  }

  #search_btn:hover {
    color: white;
  }

  .width-150 {
    width: 150px;
  }

  .fs-15 {
    font-size: 15px;
    margin-top: 5px;
  }

  .adding-items-area {
    margin-bottom: 0;
  }

  @media (min-width: 992px) and (max-width: 1199px){
    nav>.nav-tabs>a.nav-item {
      font-size: 15px;
      font-weight: 600;
    }

    .nav-link {
      display: block;
      padding: 0.5rem 0px;
    }
  }

  
</style>
@endpush
@section('dboard_content')

<!-- title-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-9 text-left">
            <h2 class="title-heading">Create Price</h2>
          </div><!-- end .col-md-9 -->

          <div class="col-md-3 text-right">
            <a class="btn index-btn" href="{{ route('price.index') }}">Back</a>
          </div><!-- end .col-md-3 -->
        </div><!-- end .row -->
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->

<!-- data-area -->
<section id="tabs">
  <div class="row">
    @yield('price_contents')
  </div><!-- end .row -->
</section>
@endsection


@push('post_scripts')
<!-- select2 -->
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<!-- select2 configuration -->
<script>
  $(document).ready(function() {
    $(".single-select").select2({
      width: '100%'
    });
  });
</script>

@endpush