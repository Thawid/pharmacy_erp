@extends('dboard.index')

@section('title', 'Product Price List')

@push('styles')
    <style>
        table.dataTable td:last-child i {
            font-size: 15px;
        }

    </style>
@endpush

@section('dboard_content')

   


    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                     <!-- title-area -->
                    <div class="row justify-content-between align-items-center">
                            <div class="col-md-6">
                                <h2 class="title-heading">Product Price List</h2>
                            </div><!-- end col-md-6 -->
    
                            <div class="col-md-6 text-right">
                                <a class="btn create-btn" href="{{ route('price.create') }}">Add New Product Price</a>
                            </div><!-- ene .col-md-6 -->
                    </div><!-- end .row -->
                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm"
                                    id="bsDataTable">
                                    <thead>
                                        <tr>
                                            <td width="10%">#SL</td>
                                            <td width="52%">Product Name</td>
                                            <td width="30%">Date</td>
                                            <td width="8%">Action</td>
                                        </tr>
                                    </thead><!-- end thead -->

                                    <tbody>
                                        @if (!empty($prices))
                                            @foreach ($prices as $row)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $row->product->product_name }}</td>
                                                    <td> {{ $row->StorePrice }}</td>
                                                    <td>
                                                        <div class="d-flex justify-content-around align-items-center">
                                                            <a href="{{ route('price.show', $row->id) }}"
                                                                class="btn details-btn">
                                                                <i class="fa fa-eye"></i>
                                                            </a>

                                                            <a href="{{ route('pricing.box_packaging.edit', $row->id) }}"
                                                                class="btn edit-btn">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody><!-- end tbody -->
                                </table><!-- end table -->
                            </div><!-- end .table-responsive -->
                        </div>
                    </div>
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->


    @endsection
    @push('post_scripts')
        <!-- Page specific javascripts-->
        <!-- Data table plugin-->
        <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">
            $('#bsDataTable').DataTable();
        </script>
    @endpush
