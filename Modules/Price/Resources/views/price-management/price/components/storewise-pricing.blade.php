@extends('price::price-management.price.create')
@push('styles')
<style>
    .create-btn {
        font-size: 22px;
        line-height: 20px;
        font-weight: bolder;
    }
</style>
@endpush
@section('price_contents')
<div class="col-md-12 ">
    <!-- nav bar start from here -->
    @include('price::price-management.price.components.inc.navbar',['price'=>$prices])
    {{-- @if (isset($store_prices))
            <form method="post" action="{{ route('pricing.storewise_pricing.update', $id) }}">
    @else
    <form method="post" action="{{ route('pricing.storewise_pricing.store') }}">
        @endif --}}
        <form method="post" action="{{ route('pricing.storewise_pricing.store') }}">
            @csrf
            <div class="tile pt-4">
                <!-- hidden field area start from here -->
                <input type="hidden" value="{{ $prices->id }}" name="price_id" />
                <input type="hidden" value="{{ $product->id }}" name="product_id" />
                <input type="hidden" value="{{ $product->product_generic_id }}" name="generic_id" />
                <!-- hidden field area start end here -->

                <!-- start #individual_campaign_wise_area -->
                <div id="individual_campaign_wise_area">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Available Store</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- individual_campaign generated table area -->
                            <div class="adding-items-area" id="individual_store_discount_generated_table_data">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm mb-0">
                                    <thead>
                                        <tr class="">
                                            <th width="22%">Store Name  <span class="text-danger">*</span></th>
                                            <th width="20%">Regular Price  <span class="text-danger">*</span></th>
                                            <th width="20%">Discount Type  <span class="text-danger">*</span></th>
                                            <th width="15%">Discount Value  <span class="text-danger">*</span></th>
                                            <th width="20%">UOM  <span class="text-danger">*</span></th>
                                            <th width="3%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="auto_individual_store_price_table">
                                        <tr>
                                            <td>
                                                <select name="store_ids[]" id="select_store" class="form-control">
                                                    <option value="">--- Select Store ---</option>
                                                    @foreach ($stores as $store)
                                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" id="regular_price" placeholder="enter regular price" onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                            </td>

                                            <td>
                                                <select name="store_discount_types[]" id="select_individual_store_wise_discount_type" class="form-control">
                                                    <option value="" selected disabled>--- Select Discount Type ---</option>
                                                    <option value="1">Amount ($)</option>
                                                    <option value="2">Percentage (%)</option>
                                                </select>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" name="store_discount_values[]" id="individual_store_wise_discount_value" placeholder="enter discount value" onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                            </td>

                                            <td>
                                                <select name="uom_ids[]" id="select_available_store_uom" class="form-control">
                                                    <option value="">--- Select UOM ---</option>
                                                    @foreach ($product->productuom as $row)
                                                    <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </td>

                                            <td>
                                                <a class="btn create-btn w-100" id="add_individual_store_wise_discount_btn">+
                                                </a>
                                            </td>
                                        </tr>
                                        @if (isset($store_prices))
                                        @foreach ($store_prices as $store_price)
                                        <tr>
                                            <td>
                                                <input class="form-control" value="{{ $store_price->store->name }}" readonly>
                                                <input type="hidden" class="form-control" name="store_id[]" value="{{ $store_price->store_id }}" readonly>
                                            </td>
                                            <td><input type="text" class="form-control" name="regular_price[]" value="{{ $store_price->regular_price }}" readonly></td>
                                            <td>
                                            @if($store_price->store_discount_type == 1)
                                            <input type="text" class="form-control" value="Amount ($)" readonly>
                                            @elseif($store_price->store_discount_type == 2)
                                            <input type="text" class="form-control" value="Percentage (%)" readonly>
                                            @endif
                                            <input type="hidden" class="form-control" name="store_discount_type[]" value="{{ $store_price->store_discount_type }}" readonly>
                                            </td>
                                            
                                            <td><input type="text" class="form-control" name="store_discount_value[]" value="{{ $store_price->store_discount_value }}" readonly>
                                            </td>
                                            <td>
                                                <input class="form-control" value="{{ $store_price->productUoms->uom_name }}" readonly>
                                                <input type="hidden" class="form-control" name="uom_id[]" value="{{ $store_price->uom_id }}" readonly>
                                            </td>
                                            <td>
                                                <div class="btn w-100 btn-outline-danger remove_avl_store_ag_data">X</div>
                                            </td>

                                        </tr>
                                        @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div><!-- end #auto_individual_store_price_table -->
                        </div>
                    </div><!-- end .row -->
                </div><!-- end #individual_campaign_wise_area -->

                <hr>
                <div class="row text-right">
                    <div class="col-md-12">
                    <a href="{{route('pricing.storewise_vat_tax',$prices->id)}}"
                            class="btn index-btn">
                            Skip</a>
                        <button type="submit" class="btn index-btn" id="store-wise-price-submit-btn">Next</button>
                    </div>
                </div><!-- end .row -->
            </div><!-- end .tile -->
        </form>
        @endsection

        @push('post_scripts')
        <!-- discount-tab-scripts -->

        <script>
            $("#store-wise-price-submit-btn").hide();
        </script>

        <!-- add add_group_wise_discount -->
        <script>
            $(document).ready(function() {
                $("#add_individual_store_wise_discount_btn").on('click', function() {
                    let store_val = $("#select_store option:selected").val();
                    let store_name_text = $("#select_store option:selected").text();
                    let d_type_val = $("#select_individual_store_wise_discount_type").find(":selected").val();
                    let d_type_text = $("#select_individual_store_wise_discount_type").find(":selected").text();
                    let d_val = $("#individual_store_wise_discount_value").val();
                    let regular_price_val = $("#regular_price").val();
                    let avl_store_uom_val = $("#select_available_store_uom").find(":selected").val();
                    let avl_store_uom_text = $("#select_available_store_uom").find(":selected").text();

                    let set_empty_store_name = "";
                    let set_empty_regular_price = "";
                    let set_empty_d_type_val = "";
                    let set_empty_d_val = "";
                    let set_empty_avl_store_uom = "";

                    if (store_val > 0 && avl_store_uom_val > 0 && ((d_type_val > 0 && d_val > 0) || regular_price_val > 0)) {
                        $('#auto_individual_store_price_table').append(
                            `<tr>
                                <td>
                                    <input class="form-control" value="${store_name_text}" readonly>
                                    <input type="hidden" id="store_id_ag" name="store_id[]" value="${store_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${regular_price_val}" readonly>
                                    <input type="hidden" name="regular_price[]" value="${regular_price_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${(d_type_text == '--- Select Discount Type ---') ? '' : d_type_text }" readonly>
                                    <input type="hidden" name="store_discount_type[]" value="${d_type_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${d_val}" readonly>
                                    <input type="hidden" name="store_discount_value[]" value="${d_val}" readonly>
                                </td>

                                <td>
                                    <input class="form-control" value="${avl_store_uom_text}" readonly>
                                    <input type="hidden" name="uom_id[]" value="${avl_store_uom_val}">
                                </td>

                                <td>
                                    <div class="btn btn-outline-danger remove_avl_store_ag_data w-100">X</div>
                                </td>  
                            </tr>`);


                        // check validation
                        if ($("#store_id_ag").val() > 0 ) {
                            $("#store-wise-price-submit-btn").show();
                        } else {
                            $("#store-wise-price-submit-btn").hide();
                        }

                        $("#select_store").val(set_empty_store_name);
                        $("#regular_price").val(set_empty_regular_price);
                        $("#select_individual_store_wise_discount_type").val(set_empty_d_type_val);
                        $("#individual_store_wise_discount_value").val(set_empty_d_val);
                        $("#select_available_store_uom").val(set_empty_avl_store_uom);

                    } else {
                        alert("Please give valid input before add");
                    }
                });

                // remove generated row
                $(document).on('click', '.remove_avl_store_ag_data', function() {
                    $(this).parent().parent().remove();

                    // check validation
                    if ($("#store_id_ag").val() > 0 ) {
                        $("#store-wise-price-submit-btn").show();
                    } else {
                        $("#store-wise-price-submit-btn").hide();
                    }
                });
            });
        </script>

        @endpush