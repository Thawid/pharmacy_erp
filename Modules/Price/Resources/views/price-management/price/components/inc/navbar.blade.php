<nav>
    <!-- tab title -->
    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">

        <a href="{{ route('price.create') }}"
           class="nav-item nav-link {{ Request::is('product/price/create') ? 'active' : '' }}" id="nav-product-tab">Product</a>

         @if(empty($prices))
        <a class="nav-item nav-link {{ Request::is('product/box-packaging/create*') ? 'active' : '' }}"
           href="{{route('pricing.box_packaging.create')}}" id="nav-box-n-pack-tab">Box &
            Packaging</a>
         @else
         <a class="nav-item nav-link {{ request()->routeIs('pricing.box_packaging.edit') ? 'active' : '' }}"
           href="{{route('pricing.box_packaging.edit',$prices->id)}}" id="nav-box-n-pack-tab">Box &
            Packaging</a>
         @endif
        <a class="nav-item nav-link {{ Request::is('product/price-discount*') ? 'active' : '' }}" id="nav-discount-tab"
           href="{{route('pricing.discount.create',!empty($price)?$price->id:'')}}">Discount</a>

        <a class="nav-item nav-link {{ Request::is('product/campaign-price*') ? 'active' : '' }}"
           id="nav-campaign-price-tab" href="{{route('pricing.campaign_pricing',!empty($price)?$price->id:'')}}">Campaign Price</a>

        <a class="nav-item nav-link {{ Request::is('product/storewise-price*') ? 'active' : '' }}"
           id="nav-store-wise-price-tab" href="{{route('pricing.storewise_pricing',!empty($price)?$price->id:'')}}">Store Wise Price</a>

        <a href="{{route('pricing.storewise_vat_tax',!empty($price)?$price->id:'')}}" class="nav-item nav-link {{ Request::is('product/storewise-vat-tax*') ? 'active' : '' }}" id="nav-vat-n-tax-tab">Vat & Tax</a>

        <a class="nav-item nav-link {{ request()->routeIs('pricing.active.price') || request()->routeIs('pricing.active.*')  ? 'active' : '' }}" href="{{ route('pricing.active.price',!empty($price)?$price->id:'') }}">Active Price</a>
    </div>
</nav>
