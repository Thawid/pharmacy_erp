@extends('price::price-management.price.create')
@push('styles')
    <style>        
        .create-btn {
            font-size: 22px;
            line-height: 20px;
            font-weight: bolder;
        }
    </style>
@endpush
@section('price_contents')
    <div class="col-md-12 ">
        <!-- nav bar start from here -->
        @include('price::price-management.price.components.inc.navbar',['price'=>$prices])

        @if (!empty($prices))
            <form action="{{ route('pricing.box_packaging.update', $prices->id) }}" method="post">
            @else
                <form action="{{ route('pricing.box_packaging.store') }}" method="post">
        @endif
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="tile c-ml-1 pt-4">
                    <!-- individual price setting -->
                    <div class="row align-items-baseline">
                        <div class="col-md-6">
                            <div id="regular_selling_heading">
                                <label for="regular_selling_price" class="m-0 p-0">
                                    <h5 class="">Regular Selling Price (without vat & tax)</h5>
                                </label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-md-6">
                        <!-- <div class="d-md-flex"> -->
                            <div class="form-check form-check-inline">
                                <label class="form-check-label" for="set_equal_margin">
                                    Product Name:
                                </label>
                            </div><!-- end .form-check -->
                            <input type="text" class="form-control" value="{{ $product->product_name }}" readonly>
                        <!-- </div> -->
                      </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <!-- hidden field area start from here -->
                    <input type="hidden" value="{{ $product->id }}" name="product_id" /><!-- product_id -->
                    <input type="hidden" value="{{ $product->product_generic_id }}" name="generic_id" />
                    <input type="hidden" name="apply_for_lot" value="1" /><!-- apply-for-lot -->
                    <!-- hidden field area start end here -->
                    <div class="regular_price" id="regular_price">
                        {{-- <div class="row mt-5 align-items-center">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="base_quantity">Base Quantity</label>
                                    <input type="number" class="form-control" id="base_qty" value="1">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="uom">UOM</label>
                                    <select class="form-control " id="uom_id">
                                        <option value=" ">---Select UOM---</option>
                                        @foreach ($product->productuom as $row)
                                            <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="per_uom_price">MRP</label>
                                <input type="number" id="per_uom_price" value="{{ $product->purchase_order->unit_price ??' ' }}"
                                    onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))"
                                    class="form-control">
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="per_uom_price">Capacity Per UOM</label>
                                    <input type="number" disabled id="capacity_per_uom"
                                        class="capacity_per_uom form-control" value="">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <button type="button" id="uom_submit" class="btn create-btn mt-2"><i
                                        class="fa fa-plus-circle" aria-hidden="true"></i>Add</button>
                            </div>
                        </div><!-- end .row --> --}}

                        <!-- product generated table area -->
                        <div class="adding-items-area mt-4" id="bp_generated_data_table_1">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                <thead>
                                    <tr class="">
                                        <th width=" 20%">
                                        Base
                                        Quantity
                                        </th>
                                        <th width="20%">UOM  <span class="text-danger">*</span></th>
                                        <th width="20%">MRP  <span class="text-danger">*</span></th>
                                        <th width="20%">Currency  <span class="text-danger">*</span></th>
                                        <th width="20%">Capacity Per UOM  <span class="text-danger">*</span></th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="add_uom">
                                    <tr>
                                        <td>
                                            <input type="number" class="form-control" id="base_qty" value="1" readonly>
                                        </td>
                                        <td>
                                            <select class="form-control " id="uom_id">
                                                <option value=" ">---Select UOM---</option>
                                                @foreach ($product->productuom as $row)
                                                    <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" id="per_uom_price"
                                                value="{{ $product->purchase_order->unit_price ?? '' }}"
                                                onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))"
                                                class="form-control">
                                        <td>
                                            <input type="text" class="form-control" value="BDT" readonly>
                                        </td>
                                        </td>
                                        <td>
                                            <input type="number" disabled id="capacity_per_uom"
                                                class="capacity_per_uom form-control" value="">
                                        </td>
                                        <td>
                                            <button type="button" id="uom_submit" class="btn create-btn w-100">+</button>
                                        </td>
                                    </tr>
                                    @if (!empty($prices))
                                        @foreach ($prices->uom_price as $row)
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" value="1" readonly>
                                                    <input type="hidden" class="form-control" name="regular_base_qty[]"
                                                        id="regular_base_qty" value="1">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="{{ $row->product_uom->uom_name }}" readonly>
                                                    <input type="hidden"
                                                        class="regular_uom_id" name="regular_uom_id[]"
                                                        value="{{ $row->uom_id }}" /></td>
                                                <td>
                                                    <input type="text" class="form-control" value="{{ $row->uom_price }}" readonly> 
                                                    <input type="hidden" name="regular_uom_price[]"
                                                        value="{{ $row->uom_price }}" /></td>
                                                <td>
                                                    <input type="text" class="form-control" value="BDT" readonly>
                                                </td>
                                                
                                                <td>
                                                    <input type="text" class="form-control" value="{{ $row->capacity }}" readonly>
                                                    <input type="hidden" name="regular_capacity[]"
                                                        value="{{ $row->capacity }}" />
                                                </td>
                                                <td>
                                                    <input class="btn btn-outline-danger w-100" type="button" name="remove-edited-items"
                                                        id="remove-edited-items" value="X"></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    @if (!empty($prices->uom_price))
                                        <input type="hidden" name="apply_for_lot" value="1" />
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        {{-- <div class="text-right">
                            <div class="form-check form-check-inline">
                                <button type="button" class="btn btn-outline-primary" id="apply_for_all_lot">Apply
                                    for all
                                    Lot</button>
                            </div><!-- end .form-check -->
                        </div><!-- end #product_generated_data_table --> --}}
                    </div>
                    {{-- <hr>
                        <!-- individual price is commented for some reasion -->
                    <!-- lot wise price setting -->
                    <div class="lot-wise-price-setting" id="lot-wise-price-setting">
                        <div class="row mt-5">
                            <div class="col-md-12 mb-4">
                                <h2>Individual Lot Wise Price</h2>
                                <hr>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="lot">Select Lot</label>
                                    <select id="indi_lot" class="form-control single-select">
                                        <option value="">---Select Lot---</option>
    
                                        <option value="1001">1001</option>
                                        <option value="1002">1002</option>
                                        <option value="1003">1003</option>
    
                                    </select>
                                </div>
                            </div>
    
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="base_quantity">Base Quantity</label>
                                    <input type="number" disabled id="indi_base_qty" class="form-control" value="1">
                                </div>
                            </div>
    
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="uom">UOM</label>
                                    <select id="indi_uom_id" class="form-control single-select">
                                        <option value="">---Select UOM---</option>
                                        @foreach ($product->productuom as $row)
                                            <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
    
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="per_uom_price">MRP</label>
                                    <input type="number" id="indi_per_uom_price" class="form-control">
                                </div>
                            </div>
    
    
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="per_uom_price">Capacity Per UOM</label>
                                    <input type="number" id="indi_capacity" disabled class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-2">
                                <button type="button" id="indi_submit" class="btn btn-outline-primary mt-28 add-btn">Add
                                </button>
                            </div>
                        </div><!-- end .row -->
    
                        <!-- product generated table area -->
                        <div class="adding-items-area tile" id="bp_generated_data_table_2">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm -ml-15">
                                <thead>
                                    <tr class="">
                                        <th style=" width: 16.66%;">Lot</th>
                                        <th style="width: 16.66%;">Base Quantity</th>
                                        <th style="width: 16.66%;">UOM</th>
                                        <th style="width: 16.66%;">MRP</th>
                                        <th style="width: 16.66%;">Currency</th>
                                        <th style="width: 16.66%;">Capacity Per UOM</th>
                                        <th style="width: 16.66%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="individual_lot_wise">
                                    @if (!empty($prices))
                                        @foreach ($prices->lot_price as $row)
                                            <tr>
                                                <td>{{ $row->lot_id }} <input type="hidden" name="indi_lot[]"
                                                        value="{{ $row->lot_id }}" /></td>
    
                                                <td>1<input type="hidden" name="indi_base_qty[]" value="1" /></td>
    
                                                <td>{{ $row->productuom->uom_name ?? ' ' }}<input type="hidden"
                                                        class="indi_uom" name="indi_uom[]"
                                                        value="{{ $row->uom_id }}" /></td>
    
                                                <td>{{ $row->uom_price }}<input type="hidden" name="indi_price[]"
                                                        value="{{ $row->uom_price }}" /></td>
    
                                                <td>BDT</td>
    
                                                <td>{{ $row->capacity }} <input type="hidden" name="indi_capacity[]"
                                                        value="{{ $row->capacity }}" /></td>
    
                                                <td><input class="btn btn-outline-danger btn-sm" type="button"
                                                        onclick="delete_row(this)" value="x"></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div><!-- end #product_generated_data_table -->
                    </div><!-- end .lot-wise-price-setting --> --}}
                    <hr>
                    <div class="row text-right">
                        <div class="col-md-12">
                        
                            @if (!empty($prices))
                                <button type="submit" id="update-box-n-pack-next-btn" class="btn index-btn">Update<i
                                        class="pl-2 fa fa-arrow-circle-right"></i>
                                </button>
                            @else
                                <button type="submit" class="btn index-btn" id="box_packaging_next">Next</button>
                            @endif
                        </div>
                    </div><!-- end .row -->
                </div><!-- end .tile -->
            </div> <!-- end .col-md-12 -->
        </div>
        </form>
    @endsection
    @push('post_scripts')
    
        <!-- remove edited items -->
        <script>
            // remove item
            $("#add_uom").on('click', "#remove-edited-items", function() {
                    $(this).closest('tr').remove();
                    if ($("#regular_base_qty").val() > 0) {
                        $("#update-box-n-pack-next-btn").show();
                    } else {
                        $("#update-box-n-pack-next-btn").hide();
                    }
                })
        </script>

        {{-- UOM data add to row --}}
        <script>
            var selected_uom = [];
            $(document).ready(function() {
                $('#box_packaging_next').hide();
                $('#uom_submit').click(function() {
                    var base_qty = $('#base_qty').val();
                    var capacity_per_uom = $('#capacity_per_uom').val();
                    var uom_text = $('#uom_id').find(":selected").text();
                    var uom_val = $('#uom_id').find(":selected").val();
                    var uom_price = $('#per_uom_price').val();

                    var selected_uom_alert = false;
                    var exist_uoms = document.querySelectorAll('.regular_uom_id');
                    exist_uoms.forEach(function(e) {
                        var val = e.value;
                        if (val == uom_val) {
                            selected_uom_alert = true;
                        }
                    })
                    var html = `<tr>
                                    <td>
                                        <input class="form-control" type="text" value="${base_qty}" readonly>
                                        <input type="hidden" class="form-control" name="regular_base_qty[]" id="regular_base_qty" value="${base_qty}">
                                    </td>
                                    <td>
                                        <input class="form-control" type="text" value="${uom_text}" readonly>
                                        <input type="hidden" class="regular_uom_id" name="regular_uom_id[]" value="${uom_val}"/>
                                    </td>
                                    <td>
                                        <input class="form-control" type="text" value="${uom_price}" readonly>
                                        <input type="hidden" name="regular_uom_price[]" value="${uom_price}"/></td>
                                    <td>
                                        <input class="form-control" value="BDT" readonly></td>
                                    <td>
                                        <input class="form-control" type="text" value="${capacity_per_uom}" readonly>
                                        <input type="hidden" name="regular_capacity[]" value="${capacity_per_uom}"/>
                                    </td>
                                    <td>
                                        <input class="btn btn-outline-danger w-100" type="button" name="remove" id="remove" value="X">
                                    </td>
                                </tr>`;

                    if (uom_val == '') {
                        alert('Please Select a Uom');
                    } else if ($('#per_uom_price').val() == '') {
                        alert('Please set a price');
                    } else if ($('#capacity_per_uom').val() == '') {
                        alert('Capacity per uom can not be null');
                    } else if (selected_uom_alert) {
                        alert('UOM already exist');
                    } else {
                        $('#add_uom').append(html);
                        selected_uom.push(uom_val);
                        $('#box_packaging_next').show();
                        $('#update-box-n-pack-next-btn').show();
                        $('#apply_for_all_lot').show();

                        var capacity_per_uom = $('#capacity_per_uom').val(' ');
                        var uom_price = $('#per_uom_price').val(' ');
                        $('#uom_id').val(' ');

                    }

                });

                // remove item
                $("#add_uom").on('click', "#remove", function() {
                    $(this).closest('tr').remove();
                    if ($("#regular_base_qty").val() > 0) {
                        $("#box_packaging_next").show();
                    } else {
                        $("#box_packaging_next").hide();
                    }
                })


            });
        </script>

        <!-- get Capacity per uom -->
        <script>
            $(document).ready(function() {
                $('#uom_id').change(function(params) {

                    var uom_val = $('#uom_id').find(":selected").val();
                    var product_id = {{ $product->id }};
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'get',
                        url: "{{ route('pricing.capacity_per_uom') }}",
                        data: {
                            uom_val,
                            product_id
                        },
                        success: function(data) {
                            $('#capacity_per_uom').val(data.quantity_per_uom);

                        }
                    });
                });
            });
        </script>

        {{-- individual lot wise price --}}
        <script>
            $(document).ready(function() {
                $('#indi_uom_id').change(function(params) {
                    var uom_val = $('#indi_uom_id').find(":selected").val();
                    var product_id = {{ $product->id }};
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'get',
                        url: "{{ route('pricing.capacity_per_uom') }}",
                        data: {
                            uom_val,
                            product_id
                        },
                        success: function(data) {
                            $('#indi_capacity').val(data.quantity_per_uom);
                        }
                    });
                });
            });


            function delete_row(em) {
                $(em).closest('tr').remove();
            }
        </script>

        {{-- individual submit --}}
        <script>
            var indi_uom_select = [];
            $(document).ready(function() {
                $('#indi_submit').click(function() {
                    var indi_lot_text = $('#indi_lot').find(":selected").text();
                    var indi_lot_val = $('#indi_lot').find(":selected").val();
                    var indi_uom_text = $('#indi_uom_id').find(":selected").text();
                    var indi_uom_val = $('#indi_uom_id').find(":selected").val();

                    var indi_base_qty = $('#indi_base_qty').val();
                    var uom_price = $('#indi_per_uom_price').val();
                    var capacity = $('#indi_capacity').val();

                    var html = ` <tr>
                                    <td>${indi_lot_text} <input type="hidden" name="indi_lot[]" value="${indi_lot_val}"/></td>
                                    <td>${indi_base_qty}<input type="hidden" name="indi_base_qty[]" value="${indi_base_qty}"/></td>
                                    <td>${indi_uom_text}<input type="hidden" class="indi_uom" name="indi_uom[]" value="${indi_uom_val}"/></td>
                                    <td>${uom_price}<input type="hidden" name="indi_price[]" value="${uom_price}"/></td>
                                    <td>BDT</td>
                                    <td>${capacity} <input type="hidden" name="indi_capacity[]" value="${capacity}"/></td>
                                    <td><input class="btn btn-outline-danger btn-sm" type="button" onclick="delete_row(this)" value="x"></td>
                                </tr>`;

                    var indi_uom_alert = false;
                    var indi_uom = document.querySelectorAll('.indi_uom');
                    indi_uom.forEach(function(e) {
                        var val = e.value;
                        if (val == indi_uom_val) {
                            indi_uom_alert = true;
                        }
                    })

                    if (indi_lot_val == '') {
                        alert('Lot field must not be empty!')
                    } else if (indi_uom_val == "") {
                        alert('Uom field must not be empty!')
                    } else if (uom_price == '') {
                        alert('Price field must not be empty!')
                    } else if (indi_uom_alert) {
                        alert('Uom Already Exist!');
                    } else {
                        $('#individual_lot_wise').append(html);
                        indi_uom_select.push(indi_uom_val);
                        $('#box_packaging_next').show();

                        $('#indi_per_uom_price').val(' ');
                        $('#indi_capacity').val(' ');
                    }
                });
            });
        </script>

        {{-- submit apply for all lot --}}
        <script>
            $(document).ready(function() {
                $('#apply_for_all_lot').click(function() {
                    var is_product = $('#regular_base_qty').val();
                    if (is_product) {
                        var html = `<input type="hidden" name="apply_for_lot" value="1"/>`;
                        $('#regular_price').append(html);

                        $('#apply_for_all_lot').hide();
                        // $('#regular_selling_heading').hide();
                        // $('#regular_price').hide();
                    } else {
                        alert('Please add a UOM');
                    }
                })
            })
        </script>
    @endpush
