@extends('price::price-management.price.create')
@push('styles')
<style>
  .mt-28 {
    margin-top: 28px;
  }

  .create-btn {
    font-size: 22px;
    line-height: 20px;
    font-weight: bolder;
  }
</style>
@endpush
@section('price_contents')
    <div class="col-md-12 ">

        <!-- nav bar start from here -->

        @include('price::price-management.price.components.inc.navbar',['price'=>$prices])

<form action="{{ route('pricing.discount.store') }}" method="post">
    @csrf

    <div class="tile pt-4">
        <!-- start radio-button-operation -->
        <div class="row">
            <div class="col-md-5">
              <h5>All Customer Group Discount</h5>
            </div>
        </div>
        <hr>

        <!-- hidden field area start from here -->
        <input type="hidden" value="{{ $prices->id }}" name="price_id" />
        <input type="hidden" value="{{ $product->id }}" name="product_id" />
        <input type="hidden" value="{{ $product->product_generic_id }}" name="generic_id" />
        <input type="hidden" id="apply_for_all_group"name="apply_for_all_group"
        value="afag">
        <input type="hidden" id="apply_for_all_lot" name="apply_for_all_lot"
        value="afal">
        <!-- hidden field area start end here -->

        <!-- start #all_customer_area -->
        <div id="all_customer_area">
            {{-- <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="customer_all_group_discount_type">Select Discount Type</label>
                        <select name="customer_all_group_discount_type" id="select_cust_discount_type"
                            class="form-control">
                            <option value="">--- Select Discount Type ---</option>
                            <option value="1">Amount ($)</option>
                            <option value="2">Percentage (%)</option>
                        </select>
                    </div>
                </div><!-- end discount-type -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="customer_all_group_discount_value">Discount Value</label>
                        <input type="text" class="form-control"
                            name="customer_all_group_discount_value" id="all_cust_discount_value"
                            value="{{ old('customer_all_group_discount_value') }}"
                            placeholder="enter discount value"
                            onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                    </div>
                </div><!-- end of discount value -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="select_uom">Select UOM</label>
                        <select name="select_uom" id="select_uom" class="form-control">
                            <option value="">--- Select UOM ---</option>
                            @foreach ($product->productuom as $row)
                                <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div><!-- end of select uom -->

                <div class="col-md-3">
                    <a class="btn btn-outline-primary add-btn mt-28" id="add_all_customer_discount_btn">
                        <i class="fa fa-plus-circle"></i>
                        Add
                    </a>
                </div><!-- end .col-md-3 of add button -->
            </div> --}}

            <div class="row">
                <div class="col-md-12">
                    <!-- all customer group discount table area -->
                    <div class="adding-items-area" id="customer-group-discount_generated_table_data">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm mt-0">
                            <thead>
                                <tr class="">
                                    <th width="31%">Discount Type <span class="text-danger">*</span></th>
                                    <th width="31%">Discount Value <span class="text-danger">*</span></th>
                                    <th width="34%">UOM <span class="text-danger">*</span></th>
                                    <th width="3%" class="text-center">Action</th>
                                </tr>
                            </thead><!-- end thead -->
                            <tbody id="all_cust_table">
                                <tr>
                                    <td>
                                        <select name="customer_all_group_discount_type" id="select_cust_discount_type"
                                            class="form-control">
                                            <option value="">--- Select Discount Type ---</option>
                                            <option value="1">Amount ($)</option>
                                            <option value="2">Percentage (%)</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control"
                                        name="customer_all_group_discount_value" id="all_cust_discount_value"
                                        value="{{ old('customer_all_group_discount_value') }}"
                                        placeholder="enter discount value"
                                        onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                    </td>
                                    <td>
                                        <select name="select_uom" id="select_uom" class="form-control">
                                            <option value="">--- Select UOM ---</option>
                                            @foreach ($product->productuom as $row)
                                                <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <a class="btn create-btn w-100" id="add_all_customer_discount_btn">
                                            +
                                        </a>
                                    </td>
                                </tr>
                                @if (!empty($prices))
                                    @foreach ($prices->all_customer_group_price as $row)
                                        <tr>
                                            <td>
                                                @if ($row->discount_type == 1)
                                                    <input class="form-control" value="Amount ($)" readonly>
                                                @elseif($row->discount_type == 2)
                                                    <input class="form-control" value="Percentage (%)" readonly>
                                                @endif
                                                <input type="hidden"
                                                    name="all_customer_discount_type_ag[]"
                                                    value="{{ $row->discount_type }}">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" 
                                                value="{{ $row->discount_value }}" readonly>
                                                <input type="hidden" class="border-0"
                                                    name="all_cust_discount_value_ag[]"
                                                    value="{{ $row->discount_value }}" readonly>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" 
                                                value="{{ $row->productuom->uom_name ?? ' ' }}" readonly>
                                                <input type="hidden" class="border-0"
                                                    name="all_cust_uom_value_ag[]"
                                                    value="{{ $row->uom_id }}" readonly>
                                            </td>
                                            <td>
                                                <div class="btn btn-outline-danger w-100 all_cust_dis_remove_btn"
                                                    onclick="delete_row(this)">X</div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody><!-- end tbody -->
                        </table><!-- end table -->
                    </div><!-- end #customer_group_discount_data_table -->
                </div><!-- end .col-md-12 -->
            </div><!-- end .row -->
        </div><!-- end #all_customer_area -->

        {{-- <div class="row text-right">
            <div class="col-md-12" id="apply_for_all">
                <a class="btn btn-outline-primary" id="apply_for_all_group" name="apply_for_all_group"
                    value="afag">Apply For All Group</a>
                <a class="btn btn-outline-primary" id="apply_for_all_lot" name="apply_for_all_lot"
                    value="afal">Apply For All LOT</a>
            </div>
        </div><!-- end button-area --> --}}

        <!-- start #customer_group_wise_area -->
        <div id="customer_group_wise_area">
            <div class="row mt-5">
                <div class="col-md-5">
                    <h5>Customer Group Wise Discount</h5>
                </div>
            </div>
            <hr>
            {{-- <div class="row mt-4">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="select_group"
                            class="">Select Group</label>
                    <select name="" id="
                            select_customer_group" class="form-control">
                            <option value="" selected>--- Select Group ---</option>
                            @foreach ($group_names as $group_name)
                                <option value="{{ $group_name->id }}">
                                    {{ $group_name->group_name }}</option>
                            @endforeach
                            </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="select_cust_wise_discount_type">Select Discount Type</label>
                        <select name="" id="select_cust_wise_discount_type" class="form-control">
                            <option value="" selected disabled>--- Select Discount Type ---</option>
                            <option value="1">Amount ($)</option>
                            <option value="2">Percentage (%)</option>
                        </select>
                    </div>
                </div><!-- end discount-type -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="cust_discount_value">Discount Value</label>
                        <input type="text" class="form-control" name="" id="cust_discount_value"
                            placeholder="enter discount value"
                            onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                    </div>
                </div><!-- end of discount value -->

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="customer_group_wise_discount_uom">Select UOM</label>
                        <select name="customer_group_wise_discount_uom"
                            id="customer_group_wise_discount_uom" class="form-control">
                            <option value="">Select UOM</option>
                            @foreach ($product->productuom as $row)
                                <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div><!-- end of select uom -->

                <div class="col-md-1">
                    <a class="btn btn-outline-primary add-btn mt-28" id="add_group_wise_discount_btn"><i
                            class="fa fa-plus-circle"></i>
                        Add</a>
                </div>
            </div><!-- end .row --> --}}

            <div class="row">
                <div class="col-md-12">
                    <!-- product generated table area -->
                    <div class="adding-items-area" id="discount_generated_table_data">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm mb-0">
                            <thead>
                                <tr>
                                    <th width="45%">Customer Group Name <span class="text-danger">*</span></th>
                                    <th width="20%">Discount Type <span class="text-danger">*</span></th>
                                    <th width="20%">Discount Value <span class="text-danger">*</span></th>
                                    <th width="12%">UOM <span class="text-danger">*</span></th>
                                    <th width="3%" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="auto_discount_table">
                                <tr>
                                    <td>
                                        <select name="" id="select_customer_group" class="form-control">
                                            <option value="" selected>--- Select Group ---</option>
                                            @foreach ($group_names as $group_name)
                                                <option value="{{ $group_name->id }}">
                                                    {{ $group_name->group_name }}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td>
                                        <select name="" id="select_cust_wise_discount_type" class="form-control">
                                            <option value="" selected>Select Discount Type</option>
                                            <option value="1">Amount ($)</option>
                                            <option value="2">Percentage (%)</option>
                                        </select>
                                    </td>

                                    <td>
                                        <input type="text" class="form-control" name="" id="cust_discount_value"
                                        placeholder="enter discount value"
                                        onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                    </td>

                                    <td>
                                        <select name="customer_group_wise_discount_uom"
                                            id="customer_group_wise_discount_uom" class="form-control">
                                            <option value="">Select UOM </option>
                                            @foreach ($product->productuom as $row)
                                                <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td>
                                        <a class="btn create-btn w-100" id="add_group_wise_discount_btn">+</a>
                                    </td>
                                </tr>
                                @if (!empty($prices))
                                    @foreach ($prices->customar_group_prices as $row)
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" value="{{ $row->customergroup->group_name ?? ' ' }}" readonly>
                                                <input type="hidden" name="customar_group[]"
                                                    value="{{ $row->customer_group_id }}">
                                            </td>
                                            <td>
                                                @if ($row->customer_discount_type == 1)
                                                    <input type="text" class="form-control" value="Amount ($)" readonly> 
                                                @elseif($row->customer_discount_type == 2)
                                                    <input type="text" class="form-control" value="Percentage (%)" readonly>
                                                @endif

                                                <input type="hidden" class="border-0"
                                                    name="all_cust_discount_type_ag[]"
                                                    value="{{ $row->customer_discount_type }}">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" 
                                                value="{{ $row->customer_discount_value }}" readonly>
                                                <input type="hidden" class="border-0"
                                                    name="customer_discount_value[]"
                                                    value="{{ $row->customer_discount_value }}">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" 
                                                value="{{ $row->productUom->uom_name ?? ' ' }}" readonly>
                                                <input type="hidden" class="border-0"
                                                    name="customer_uom_value[]"
                                                    value="{{ $row->uom_id }}" readonly>
                                            </td>
                                            <td>
                                                <div class="btn btn-outline-danger w-100 all_cust_dis_remove_btn"
                                                    onclick="delete_row(this)">X</div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div><!-- end #product_generated_data_table -->
                </div><!-- end .col-md-10 -->
            </div><!-- end .row -->
        </div><!-- end #customer_group_wise_area -->

        {{-- <!-- start #lot_wise_area -->
        <div id="lot_wise_area" class="mt-5">
            <div class="row mt-4">
                <div class="col-md-4">
                    <div class="card p-2">
                        <div class="form-check">
                            <h4>LOT Wise Discount</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="select_lot"
                            class="">Select Lot</label>
        <select name="" id="
                            select_lot" class="form-control">
                            <option value="" selected>--- Select Group ---</option>
                            <option value="1">lot-1</option>
                            <option value="2">lot-2</option>
                            <option value="3">lot-3</option>
                            </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="select_lot_wise_discount_type">Select Discount Type</label>
                        <select name="" id="select_lot_wise_discount_type" class="form-control">
                            <option value="" selected>--- Select Discount Type ---</option>
                            <option value="1">Amount ($)</option>
                            <option value="2">Percentage (%)</option>
                        </select>
                    </div>
                </div><!-- end discount-type -->

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="lot_wise_discount_value">Discount Value</label>
                        <input type="text" class="form-control" name="" id="lot_wise_discount_value"
                            placeholder="enter discount value"
                            onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                    </div>
                </div><!-- end of discount value -->

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="select_lot_wise_uom">Select UOM</label>
                        <select id="select_lot_wise_uom" class="form-control">
                            <option value="">--- Select UOM ---</option>
                            @foreach ($product->productuom as $row)
                                <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div><!-- end of select uom -->

                <div class="col-md-2">
                    <a class="btn btn-outline-primary add-btn mt-28" id="add_lot_wise_discount_btn"><i
                            class="fa fa-plus-circle"></i>
                        Add</a>
                </div>
            </div><!-- end .row -->

            <div class="row">
                <div class="col-md-12">
                    <!-- lot_product generated table area -->
                    <div class="adding-items-area" id="lot_product_discount_generated_table_data">
                        <table class="table table-striped table-bordered table-hover bg-white table-sm"
                            style="margin-top: 10px">
                            <thead>
                                <tr class="">
                    <th width=" 45%">
                                    Lot Name</th>
                                    <th width="20%">Discount Type</th>
                                    <th width="20%">Discount Value</th>
                                    <th width="10%">UOM</th>
                                    <th width="5%" class="text-center">X</th>
                                </tr>
                            </thead>
                            <tbody id="auto_lot_discount_table">
                                @if (!empty($prices))
                                    @foreach ($prices->lot_discount_prices as $row)
                                        <tr>
                                            <td>
                                                {{ $row->lot_id }}
                                                <input type="hidden" class="form-control"
                                                    name="lot_discount[]" value="{{ $row->lot_id }}"
                                                    readonly>
                                            </td>
                                            <td>
                                                @if ($row->lot_discount_type == 1)
                                                    Amount ($)
                                                @elseif($row->lot_discount_type == 2)
                                                    Percentage (%)
                                                @endif
                                                <input type="hidden" class="form-control"
                                                    name="lot_discount_type[]"
                                                    value="{{ $row->lot_discount_type }}" readonly>
                                            </td>
                                            <td>
                                                {{ $row->lot_discount_value }}
                                                <input type="hidden" class="form-control"
                                                    name="lot_discount_value[]"
                                                    value="{{ $row->lot_discount_value }}" readonly>
                                            </td>

                                            <td>
                                                {{ $row->productuom->uom_name ?? ' ' }}
                                                <input type="hidden" class="form-control"
                                                    name="lot_wise_uom_ag[]"
                                                    value="{{ $row->uom_id }}">
                                            </td>
                                            <td>
                                                <div class="btn btn btn-danger cust_group_wise_item_remove btn-xs"
                                                    onclick="delete_row(this)">x</div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div><!-- end #lot_product_generated_data_table -->
                </div>
            </div><!-- end .row -->
        </div><!-- end #lot_wise_area --> --}}

        <hr>
        <div class="row text-right">
            <div class="col-md-12">
                <a href="{{route('pricing.campaign_pricing',$prices->id)}}"
                    class="btn index-btn">
                    Skip</a>
                <button type="submit" id="discount_submit_btn"
                    class="btn index-btn">
                    Next</button>
            </div>
        </div><!-- end .row -->

                    
    </div><!-- end .tile -->
</form>
@endsection

@push('post_scripts')
        <!-- discount-tab-scripts -->

        {{-- validation --}}
        <script>
            // $(document).ready(function() {
            //     function isDiscountEmpty() {
            //     if ($("#all_customer_discount_type_id_ag").val() > 0 || $("#customer_group_id_ag").val() > 0) {
            //         $("#discount_submit_btn").show();
            //         console.log('validated');
            //     } else {
            //         $("#discount_submit_btn").hide();
            //         console.log('not validate');
            //     }
            // }
            // })
        </script>

        <script>
            $(document).ready(function() {
            $("#discount_submit_btn").hide()
            });
        </script>

        <!-- add add_group_wise_discount -->
        <script>
            select_cust_group = [];
            $(document).ready(function() {
                $("#add_all_customer_discount_btn").on('click', function() {
                    let all_customer_discount_type_val = $("#select_cust_discount_type option:selected").val();
                    let all_customer_discount_type_text = $("#select_cust_discount_type option:selected")
                        .text();

                    let all_cust_discount_val = $("#all_cust_discount_value").val();

                    let all_customer_uom_val = $("#select_uom option:selected").val();
                    let all_customer_uom_text = $("#select_uom option:selected").text();

                    let set_empty_all_customer_discount_type_val = "";
                    let set_empty_all_cust_discount_val = "";
                    let set_empty_all_customer_uom_val = "";

                    if (all_customer_discount_type_val > 0 && all_cust_discount_val > 0 &&
                        all_customer_uom_val > 0) {
                        $("#all_cust_table").append(
                            `<tr>
                                <td>
                                    <input class="form-control" value="${all_customer_discount_type_text}" readonly>
                                    <input type="hidden" name="all_customer_discount_type_ag[]" id="all_customer_discount_type_id_ag" value="${all_customer_discount_type_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${all_cust_discount_val}" readonly>
                                    <input type="hidden" name="all_cust_discount_value_ag[]" value="${all_cust_discount_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${all_customer_uom_text}" readonly>
                                    <input type="hidden" name="all_cust_uom_value_ag[]" value="${all_customer_uom_val}">
                                </td>
                                
                                <td>
                                    <div class="btn btn-outline-danger all_cust_dis_remove_btn w-100">X</div>
                                </td>
                            </tr>`
                        );
                        $('#apply_for_all_group').show();
                        $('#apply_for_all_lot').show();
                        // remove generated row
                        $(document).on('click', '.all_cust_dis_remove_btn', function() {
                            $(this).parent().parent().remove();

                            // validation
                            if ($("#all_customer_discount_type_id_ag").val() > 0 || $("#customer_group_id_ag").val() > 0) {
                                $("#discount_submit_btn").show();
                            } else {
                                $("#discount_submit_btn").hide();
                            }
                        });

                        // clean the fields
                        $("#select_cust_discount_type").val(set_empty_all_customer_discount_type_val);
                        $("#all_cust_discount_value").val(set_empty_all_cust_discount_val);
                        $("#select_uom").val(set_empty_all_customer_uom_val);

                        // validation
                        if ($("#all_customer_discount_type_id_ag").val() > 0 || $("#customer_group_id_ag").val() > 0) {
                                $("#discount_submit_btn").show();
                            } else {
                                $("#discount_submit_btn").hide();
                            }
                    } else {
                        alert("Please give valid input before adding");
                    }
                });

                $("#add_group_wise_discount_btn").on('click', function() {
                    let group_val = $("#select_customer_group option:selected").val();
                    let group_name_text = $("#select_customer_group option:selected").text();
                    let d_type_val = $("#select_cust_wise_discount_type").find(":selected").val();
                    let d_type_text = $("#select_cust_wise_discount_type").find(":selected").text();
                    let d_val = $("#cust_discount_value").val();
                    let customer_group_wise_uom_val = $("#customer_group_wise_discount_uom option:selected")
                        .val();
                    let customer_group_wise_uom_text = $("#customer_group_wise_discount_uom option:selected")
                        .text();

                    let set_empty_group_name = "";
                    let set_empty_d_type_val = "";
                    let set_empty_d_val = "";
                    let set_empty_uom_val = "";

                    let checkGroupName = false;
                    select_cust_group.push(group_val);
                    select_cust_group.find(function(e) {
                        if (e == group_val) {
                            // checkGroupName = true;
                        }
                    });

                    if (group_val > 0 && d_type_val > 0 && d_val > 0 && customer_group_wise_uom_val > 0) {
                        $('#auto_discount_table').append(
                            `<tr>
                                <td>
                                    <input class="form-control" value="${group_name_text}" readonly>
                                    <input type="hidden" name="customar_group[]" id="customer_group_id_ag" value="${group_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${d_type_text}" readonly>
                                    <input type="hidden" name="all_cust_discount_type_ag[]" value="${d_type_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${d_val}" readonly>
                                    <input type="hidden" name="customer_discount_value[]" value="${d_val}">
                                </td>

                                <td>
                                    <input class="form-control" value="${customer_group_wise_uom_text}" readonly>
                                    <input type="hidden" name="customer_uom_value[]" value="${customer_group_wise_uom_val}">
                                </td>

                                <td>
                                    <div class="btn btn-outline-danger w-100 cust_group_wise_item_remove btn-xs">X</div>
                                </td>
                            </tr>`);

                        // remove generated row
                        $(document).on('click', '.cust_group_wise_item_remove', function() {
                            $(this).parent().parent().remove();   

                            // validation
                            if ($("#all_customer_discount_type_id_ag").val() > 0 || $("#customer_group_id_ag").val() > 0) {
                                $("#discount_submit_btn").show();
                            } else {
                                $("#discount_submit_btn").hide();
                            }
                        });

                        // empty all fields
                        $("#select_customer_group").val(set_empty_group_name);
                        $("#select_cust_wise_discount_type").val(set_empty_d_type_val);
                        $("#cust_discount_value").val(set_empty_d_val);
                        $("#customer_group_wise_discount_uom").val(set_empty_uom_val);

                        // validation
                        if ($("#all_customer_discount_type_id_ag").val() > 0 || $("#customer_group_id_ag").val() > 0) {
                                $("#discount_submit_btn").show();
                            } else {
                                $("#discount_submit_btn").hide();
                            }
                    } else {
                        alert("Please give valid input before add");
                    }
                });

                $("#add_lot_wise_discount_btn").on('click', function() {
                    let lot_val = $("#select_lot option:selected").val();
                    let lot_name_text = $("#select_lot option:selected").text();
                    let lot_d_type_val = $("#select_lot_wise_discount_type").find(":selected").val();
                    let lot_d_type_text = $("#select_lot_wise_discount_type").find(":selected").text();
                    let lot_d_val = $("#lot_wise_discount_value").val();
                    let lot_wise_uom_val = $("#select_lot_wise_uom option:selected").val();
                    let lot_wise_uom_text = $("#select_lot_wise_uom option:selected").text();

                    let set_empty_lot_name = "";
                    let set_empty_lot_d_type_val = "";
                    let set_empty_lot_d_val = "";
                    let set_empty_select_lot_uom_val = "";

                    if (lot_val > 0 && lot_d_type_val > 0 && lot_d_val > 0) {

                        $('#auto_lot_discount_table').append('<tr>' +
                            '<td>' + lot_name_text +
                            '<input type="hidden" class="form-control" name="lot_discount[]" value="' +
                            lot_val + '" readonly></td>' +

                            '<td>' + lot_d_type_text +
                            '<input type="hidden" class="form-control" name="lot_discount_type[]" value="' +
                            lot_d_type_val + '" readonly></td>' +

                            '<td>' + lot_d_val +
                            '<input type="hidden" class="form-control" name="lot_discount_value[]" value="' +
                            lot_d_val + '" readonly></td>' +

                            '<td>' + lot_wise_uom_text +
                            '<input type="hidden" class="form-control" name="lot_wise_uom_ag[]" value="' +
                            lot_wise_uom_val + '" readonly></td>' +

                            '<td>' +
                            '<div class="btn btn btn-danger lot_wise_item_remove btn-xs">x</div>' +
                            '</td>' +

                            '</tr>');

                        // remove generated row
                        $(document).on('click', '.lot_wise_item_remove', function() {
                            $(this).parent().parent().remove();
                        });

                        $("#select_lot").val(set_empty_lot_name);
                        $("#select_lot_wise_discount_type").val(set_empty_lot_d_type_val);
                        $("#lot_wise_discount_value").val(set_empty_lot_d_val);
                        $("#select_lot_wise_uom").val(set_empty_select_lot_uom_val);
                    } else {
                        alert("Please give valid input before add");
                    }
                });
            });
        </script>


        <!-- apply for all group -->
        <script>
            $(document).ready(function() {
                $('#apply_for_all_group').click(function() {
                    const html = '<input type="hidden" name="apply_for_all_group" value="1"/>';
                    $('#apply_for_all').append(html);
                    $('#apply_for_all_group').hide();
                })

                $('#apply_for_all_lot').click(function() {
                    const html = '<input type="hidden" name="apply_for_all_lot" value="1"/>';
                    $('#apply_for_all').append(html);
                    $('#apply_for_all_lot').hide();
                })
            });

            function delete_row(em) {
                $(em).closest('tr').remove();
            }
        </script>
    @endpush
