@extends('price::price-management.price.create')
@push('styles')
<style>
    .create-btn {
        font-size: 22px;
        line-height: 20px;
        font-weight: bolder;
    }
</style>
@endpush
@section('price_contents')
<div class="col-md-12 ">

    <!-- nav bar start from here -->

    @include('price::price-management.price.components.inc.navbar',['price'=>$prices])

    <form method="post" action="{{ route('pricing.campaign_pricing.store') }}">
        @csrf

        <div class="tile pt-4">
            <!-- start radio-button-operation -->
            <div class="row">
                <div class="col-md-5">
                    <h5>All Campaign Price</h5>
                </div>
            </div><!-- end .row -->
            <hr>
            <!-- hidden field area start from here -->

            <!-- hidden field area start from here -->
            <input type="hidden" value="{{ $prices->id }}" name="price_id" />
            <input type="hidden" value="{{ $product->id }}" name="product_id" />
            <input type="hidden" value="{{ $product->product_generic_id }}" name="generic_id" />
            <!-- hidden field area start end here -->
            <!-- hidden field area start end here -->

            <!-- start #all_campaign_area -->
            <div id="all_campaign_area">
                <div class="row">
                    <div class="col-md-12">
                        <div class="adding-items-area" id="all_campaign_price_generated_table_data">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                <thead>
                                    <tr class="">
                                        <th width="32.33%">Discount Type  <span class="text-danger">*</span></th>
                                        <th width="32.33%">Discount Value  <span class="text-danger">*</span></th>
                                        <th width="32.33%">UOM  <span class="text-danger">*</span></th>
                                        <th width="3%" class="text-center">Action</th>
                                    </tr>
                                </thead><!-- end thead -->
                                <tbody id="all_camp_table">
                                    <tr>
                                        <td>
                                            <select id="select_all_camp_discount_type" class="form-control">
                                                <option value="">--- Select Discount Type ---</option>
                                                <option value="1">Amount ($)</option>
                                                <option value="2">Percentage (%)</option>
                                            </select>
                                        </td>

                                        <td>
                                            <input type="text" class="form-control" id="all_camp_discount_val" value="{{ old('discount_value') }}" placeholder="enter discount value" onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                        </td>

                                        <td>
                                            <select id="select_all_camp_uom" class="form-control">
                                                <option value="">--- Select UOM ---</option>
                                                @foreach ($product->productuom as $row)
                                                <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <a class="btn create-btn w-100" id="add_all_camp_price_val">
                                                +
                                            </a>
                                        </td>
                                    </tr>

                                    @if (!empty($prices))
                                    @foreach ($prices->regular_campaign_discount as $row)
                                    <tr>
                                        <td>
                                            @if($row->discount_type == 1)
                                            <input class="form-control" value="Amount ($)" readonly>
                                            @elseif($row->discount_type == 2)
                                            <input class="form-control" value="Percentage (%)" readonly>
                                            @endif
                                            <input type="hidden" name="all_camp_discount_type_ag[]" value="{{$row->discount_type}}">
                                        </td>

                                        <td>
                                            <input class="form-control" value="{{$row->discount_value}}" readonly>
                                            <input type="hidden" name="all_camp_discount_value_ag[]" value="{{$row->discount_value}}" readonly>
                                        </td>

                                        <td>
                                            <input class="form-control" value="{{$row->product_uom->uom_name}}" readonly>
                                            <input type="hidden" class="all_camp_uom_value_ag" name="all_camp_uom_value_ag[]" value="{{$row->uom_id}}">
                                        </td>

                                        <td>
                                            <div class="btn btn-outline-danger w-100 all_camp_price_remove_btn" onclick="delete_row(this)">x</div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    @if($prices->ysn_discount_for_all_campaigns == 1)
                                    <input type="hidden" name="apply_for_all_campaign_value" value="1">
                                    @endif


                                </tbody><!-- end tbody -->
                            </table><!-- end table -->
                        </div><!-- end #all_campaign_price_generated_table_data -->
                    </div>
                </div>

                <!-- table-area -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- all campaign price table area -->

                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->

                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <button type="button" class="btn index-btn" id="apply_for_all_campaign" name="apply_for_all_campaign">Apply For All Campaign</button>
                        </div>
                    </div>
                </div><!-- end .row of buttons -->
            </div><!-- end #all_campaign_area -->

            <!-- start #individual_campaign_wise_area -->
            <div id="individual_campaign_wise_area" class="mt-4">
                <div class="row mt-4">
                    <div class="col-md-6">
                        <h5>Individual Campaign Wise Price</h5>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <!-- individual_campaign generated table area -->
                        <div class="adding-items-area" id="individual_campaign_rpice_generated_table_data">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm mb-0">
                                <thead>
                                    <tr class="">
                                        <th width="23.75%">Campaign Name  <span class="text-danger">*</span></th>
                                        <th width="23.75%">Discount Type  <span class="text-danger">*</span></th>
                                        <th width="23.75%">Discount Value  <span class="text-danger">*</span></th>
                                        <th width="23.75%">UOM  <span class="text-danger">*</span></th>
                                        <th width="3%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="auto_individual_campaign_price_table">
                                    <tr>
                                        <td>
                                            <select id="select_campaign" class="form-control">
                                                <option value="" selected>--- Select Campaign ---</option>
                                                <option value="1">Campaign-1</option>
                                                <option value="2">Campaign-2</option>
                                                <option value="3">Campaign-3</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select id="select_individual_campaign_wise_discount_type" class="form-control">
                                                <option value="" selected>--- Select Discount Type ---</option>
                                                <option value="1">Amount ($)</option>
                                                <option value="2">Percentage (%)</option>
                                            </select>
                                        </td>

                                        <td>
                                            <input type="text" class="form-control" name="" id="individual_campaign_wise_discount_value" placeholder="enter discount value" onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                        </td>

                                        <td>
                                            <select id="select_indi_camp_uom" class="form-control">
                                                <option value="">--- Select UOM ---</option>
                                                @foreach ($product->productuom as $row)
                                                <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <a class="btn create-btn w-100" id="add_individual_campaign_wise_discount_btn">+</a>
                                        </td>
                                    </tr>
                                    @if (!empty($prices))
                                    @foreach ($prices->individualCampaignWisePrice as $row)
                                    <tr>
                                        <td>
                                            <input class="form-control" value="{{$row->campaign_id}}" readonly>
                                            <input type="hidden" class="form-control" name="individual_campaign_name[]" value="{{ $row->campaign_id}}" readonly>
                                        </td>
                                        <td>
                                            @if($row->campaign_discount_type == 1)
                                            <input class="form-control" value="Amount ($)" readonly>
                                            @elseif($row->campaign_discount_type == 2)
                                            <input class="form-control" value="Percentage (%)" readonly>
                                            @endif
                                            <input type="hidden" class="form-control" name="individual_campaign_wise_discount_type[]" value="{{$row->campaign_discount_type}}" readonly>
                                        </td>

                                        <td>
                                            <input class="form-control" value="{{ $row->campaign_discount_value }}" readonly>
                                            <input type="hidden" class="form-control" name="individual_campaign_wise_discount_value[]" value="{{ $row->campaign_discount_value }}" readonly>
                                        </td>

                                        <td>
                                            <input class="form-control" value="{{ $row->productuom->uom_name }}" readonly>
                                            <input type="hidden" class="indi_camp_uom_value_ag" name="indi_camp_uom_value_ag[]" value="{{ $row->uom_id }}">
                                        </td>
                                        <td>
                                            <div class="btn btn-outline-danger w-100 indi_camp_price_remove_btn" onclick="delete_row(this)">X</div>
                                        </td>

                                    </tr>
                                    @endforeach
                                    @endif



                                </tbody>
                            </table>
                        </div><!-- end #auto_individual_campaign_price_table -->
                    </div>
                </div><!-- end .row -->
            </div><!-- end #individual_campaign_wise_area -->

            <hr>
            <div class="row text-right">
                <div class="col-md-12">
                <a href="{{route('pricing.storewise_pricing',$prices->id)}}"
                            class="btn index-btn">
                            Skip</a>
                    <button type="submit" class="btn index-btn" id="submit-next-btn">Next</button>
                </div>
            </div><!-- end .row -->
        </div><!-- end .tile -->
    </form>
    @endsection

    @push('post_scripts')

    <!-- hiding element -->
    <script>
        $('#apply_for_all_campaign').hide();
        $("#submit-next-btn").hide();
        console.log("hiding apply for all campaign button");
    </script>
    <!-- discount-tab-scripts -->

    <!-- add add_group_wise_discount -->
    <script>
        $(document).ready(function() {
            $("#add_all_camp_price_val").on('click', function() {
                let all_camp_discount_type_val = $("#select_all_camp_discount_type option:selected").val();
                let all_camp_discount_type_text = $("#select_all_camp_discount_type option:selected")
                    .text();

                let all_camp_discount_val = $("#all_camp_discount_val").val();

                let all_camp_uom_val = $("#select_all_camp_uom option:selected").val();
                let all_camp_uom_text = $("#select_all_camp_uom option:selected").text();

                let set_empty_all_camp_discount_type_val = "";
                let set_empty_all_camp_discount_val = "";
                let set_empty_all_camp_uom_val = "";

                let allCampCheckUom = false;
                let all_camp_uom_value_ag = document.querySelectorAll('.all_camp_uom_value_ag');

                all_camp_uom_value_ag.forEach(function(e) {
                    if (e.value == all_camp_uom_val) {
                        allCampCheckUom = true
                    }
                })

                if (allCampCheckUom) {
                    alert('The UOM is already Selected');
                } else {
                    if (all_camp_discount_type_val > 0 && all_camp_discount_val > 0 &&
                        all_camp_uom_val > 0) {
                        $("#all_camp_table").append(
                            '<tr>' +
                            '<td>' +
                            '<input class="form-control" value="' + all_camp_discount_type_text +
                            '" readonly>' +
                            '<input type="hidden" id="all_camp_discount_type_id_ag" name="all_camp_discount_type_ag[]" value="' +
                            all_camp_discount_type_val + '">' +
                            '</td>' +

                            '<td>' +
                            '<input class="form-control" value="' + all_camp_discount_val +
                            '" readonly>' +
                            '<input type="hidden" name="all_camp_discount_value_ag[]" value="' +
                            all_camp_discount_val + '" readonly>' +
                            '</td>' +

                            '<td>' +
                            '<input class="form-control" value="' + all_camp_uom_text +
                            '" readonly>' +
                            '<input type="hidden" class="all_camp_uom_value_ag" name="all_camp_uom_value_ag[]" value="' +
                            all_camp_uom_val + '">' +
                            '</td>' +

                            '<td>' +
                            '<div class="btn btn-outline-danger w-100 all_camp_price_remove_btn">X</div>' +
                            '</td>' +
                            '</tr>'
                        );

                        // check validation
                        if ($("#all_camp_discount_type_id_ag").val() > 0 || $("#individual_campaign_name_id_ag").val() > 0) {
                                $("#submit-next-btn").show();
                            } else {
                                $("#submit-next-btn").hide();
                        }

                        // showing the "apply for all camping button"
                        $('#apply_for_all_campaign').show();
                        console.log("show apply_for_all_campaign");

                        // remove generated row
                        $(document).on('click', '.all_camp_price_remove_btn', function() {
                            $(this).parent().parent().remove();

                            // check validation
                            if ($("#all_camp_discount_type_id_ag").val() > 0 || $("#individual_campaign_name_id_ag").val() > 0) {
                                $("#submit-next-btn").show();
                            } else {
                                $("#submit-next-btn").hide();
                            }
                        });

                        // clean the fields
                        $("#select_all_camp_discount_type").val(set_empty_all_camp_discount_type_val);
                        $("#all_camp_discount_val").val(set_empty_all_camp_discount_val);
                        $("#select_all_camp_uom").val(set_empty_all_camp_uom_val);
                    } else {
                        alert("Please Give Valid Input Before Add");
                    }
                }

            });

            $("#add_individual_campaign_wise_discount_btn").on('click', function() {
                let campaign_val = $("#select_campaign option:selected").val();
                let campaign_name_text = $("#select_campaign option:selected").text();
                let d_type_val = $("#select_individual_campaign_wise_discount_type").find(":selected")
                    .val();
                let d_type_text = $("#select_individual_campaign_wise_discount_type").find(":selected")
                    .text();
                let d_val = $("#individual_campaign_wise_discount_value").val();

                let indi_camp_uom_val = $("#select_indi_camp_uom option:selected").val();
                let indi_camp_uom_text = $("#select_indi_camp_uom option:selected").text();

                let set_empty_campaign_name = "";
                let set_empty_d_type_val = "";
                let set_empty_d_val = "";
                let set_empty_indi_camp_uom_val = "";

                let indiCampCheckUom = false;
                let indi_camp_uom_value_ag = document.querySelectorAll('.indi_camp_uom_value_ag');

                indi_camp_uom_value_ag.forEach(function(e) {
                    if (e.value == indi_camp_uom_val) {
                        indiCampCheckUom = true
                    }
                });

                if (indiCampCheckUom) {
                    alert("This Uom is already selected, Please Select Another UOM");
                } else {
                    if (campaign_val > 0 && d_type_val > 0 && d_val > 0) {
                        $('#auto_individual_campaign_price_table').append('<tr>' +
                            '<td>' +
                            '<input class="form-control" value="' + campaign_name_text +
                            '" readonly>' +
                            '<input type="hidden" class="form-control" id="individual_campaign_name_id_ag" name="individual_campaign_name[]" value="' +
                            campaign_val + '" readonly></td>' +

                            '<td>' +
                            '<input class="form-control" value="' + d_type_text +
                            '" readonly>' +
                            '<input type="hidden" class="form-control" name="individual_campaign_wise_discount_type[]" value="' +
                            d_type_val + '" readonly></td>' +

                            '<td>' +
                            '<input class="form-control" value="' + d_val +
                            '" readonly>' +
                            '<input type="hidden" class="form-control" name="individual_campaign_wise_discount_value[]" value="' +
                            d_val + '" readonly></td>' +

                            '<td>' +
                            '<input class="form-control" value="' + indi_camp_uom_text +
                            '" readonly>' +
                            '<input type="hidden" class="indi_camp_uom_value_ag" name="indi_camp_uom_value_ag[]" value="' +
                            indi_camp_uom_val + '">' +
                            '</td>' +

                            '<td>' +
                            '<div class="btn btn-outline-danger w-100 indi_camp_price_remove_btn">X</div>' +
                            '</td>' +

                            '</tr>');

                            // check validation
                            if ($("#all_camp_discount_type_id_ag").val() > 0 || $("#individual_campaign_name_id_ag").val() > 0) {
                                $("#submit-next-btn").show();
                            } else {
                                $("#submit-next-btn").hide();
                            }

                        // clean the fields
                        $("#select_campaign").val(set_empty_campaign_name);
                        $("#select_individual_campaign_wise_discount_type").val(set_empty_d_type_val);
                        $("#individual_campaign_wise_discount_value").val(set_empty_d_val);
                        $("#select_indi_camp_uom").val(set_empty_indi_camp_uom_val);

                        // remove generated row
                        $(document).on('click', '.indi_camp_price_remove_btn', function() {
                            $(this).parent().parent().remove();

                            // check validation
                            if ($("#all_camp_discount_type_id_ag").val() > 0 || $("#individual_campaign_name_id_ag").val() > 0) {
                                $("#submit-next-btn").show();
                            } else {
                                $("#submit-next-btn").hide();
                            }
                        });
                    } else {
                        alert("Please Give Valid Input Before Add");
                    }
                }
            });
        });

        function delete_row(em) {
                $(em).closest('tr').remove();
            }
    </script>

    {{-- submit apply for all campaign --}}
    <script>
        $(document).ready(function() {
            $('#apply_for_all_campaign').click(function() {
                let data = `<input type="hidden" name="apply_for_all_campaign_value" value="${1}">`;
                $("#all_campaign_area").append(data);
                $("#apply_for_all_campaign").hide();
                console.log("hiding the button");
            });
        });
    </script>

    @endpush