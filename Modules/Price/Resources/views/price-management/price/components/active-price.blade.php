@extends('price::price-management.price.create')
@push('styles')
<style>
    /*accordion style*/
    #accordion .panel {
        border-radius: 0;
        border: 0;
        margin-top: 0px;
    }

    #accordion a {
        display: block;
        padding: 10px 15px;
        border-bottom: 1px solid #7FC8A9;
        text-decoration: none;
        color: #4B6587;
    }

    #accordion .panel-heading a.collapsed:hover,
    #accordion .panel-heading a.collapsed:focus {
        background-color: #7FC8A9;
        color: white;
        transition: all 0.2s ease-in;
    }

    #accordion .panel-heading a.collapsed:hover::before,
    #accordion .panel-heading a.collapsed:focus::before {
        color: white;
    }

    #accordion .panel-heading {
        padding: 0;
        border-radius: 0px;
        text-align: center;
        background: rgba(0, 200, 128, 0.1);
    }

    #accordion .panel-heading a:not(.collapsed) {
        color: white;
        background-color: #7FC8A9;
        transition: all 0.2s ease-in;
    }

    /* Add Indicator fontawesome icon to the left */
    #accordion .panel-heading .accordion-toggle::before {
        font-family: 'FontAwesome';
        content: '\f00d';
        float: left;
        color: white;
        font-weight: lighter;
        transform: rotate(0deg);
        transition: all 0.2s ease-in;
    }

    #accordion .panel-heading .accordion-toggle.collapsed::before {
        color: #444;
        transform: rotate(-135deg);
        transition: all 0.2s ease-in;
    }

    .tile {
    border-radius: 8px 0 8px 8px;
  }

  .c-mr-1 {
    margin-right: 1px;
  }

  .font-weight-normal {
    font-weight: normal;
  }

</style>
@endpush
@section('price_contents')
<div class="col-md-12 ">

    <!-- nav bar start from here -->

    @include('price::price-management.price.components.inc.navbar',['price'=>$prices])

    <form method="post">
        @csrf

        <div class="tile pt-4 c-mr-1" >

            <div class="product-area">
                <h4 class="mb-1">Product Name</h4>
                <p>{{ $product ->product_name}}</p>
            </div>
            <hr>

            @if(!empty($uom_prices))
            <div class="selling-price-area mt-4">
                <h4 class="font-weight-normal">Regular Selling Price/UOM</h4>
                <div class="row">
                    <div class="col-md-12">

                        <!-- regular_selling generated table area -->
                        <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                <thead>
                                    <tr class="">
                                        <th style="width: 16.66%;">Base Quantity</th>
                                        <th style="width: 16.66%;">UOM</th>
                                        <th style="width: 16.66%;">Per UOM Price</th>
                                        <th style="width: 16.66%;">Currency</th>
                                        <th style="width: 16.66%;">Capacity Per UOM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($uom_prices as $uom_price )
                                    <tr>
                                        <td>1</td>
                                        <td>{{ $uom_price->product_uom->uom_name }}</td>
                                        <td>{{ $uom_price->uom_price }}</td>
                                        <td>{{ $uom_price->currency }}</td>
                                        <td>{{ $uom_price->capacity }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- end #regular_selling_generated_data_table -->
                    </div>
                </div><!-- end .row -->
            </div>
            @endif

            @if(!empty($lot_prices))
            <div class="selling-price-area mt-4">
                <h4 class="font-weight-normal">Individual Lot Wise Price</h4>
                <div class="row">
                    <div class="col-md-12">

                        <!-- regular_selling generated table area -->
                        <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                <thead>
                                    <tr class="">
                                        <th style="width: 16.66%;">Base Quantity</th>
                                        <th style="width: 16.66%;">UOM</th>
                                        <th style="width: 16.66%;">Per UOM Price</th>
                                        <th style="width: 16.66%;">Currency</th>
                                        <th style="width: 16.66%;">Capacity Per UOM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($lot_prices as $row )
                                    <tr>
                                        <td>1</td>
                                        <td>{{ $row->product_uom->uom_name }}</td>
                                        <td>{{ $row->uom_price }}</td>
                                        <td>{{ $row->currency }}</td>
                                        <td>{{ $row->capacity }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- end #regular_selling_generated_data_table -->
                    </div>
                </div><!-- end .row -->
            </div>
            @endif
            <hr>

       
            <div id="accordion" class="panel-group">
                <div class="panel">
                    <div class="panel-heading panel-bg">
                        <h4 class="panel-title title-heading">
                            <a href="#panelBodyOne" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Active Discount</a>
                        </h4>
                    </div>
                    <div id="panelBodyOne" class="panel-collapse collapse in show">
                        <div class="panel-body">
                        @if(!empty($price_discount_details))
                            <div class="selling-price-area mt-4">
                                <h4 class="font-weight-normal">All Customer Group Discount</h4>
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- regular_selling generated table area -->
                                        <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                                <thead>
                                                    <tr class="">
                                                        <th style="width: 16.66%;">UOM</th>
                                                        <th style="width: 16.66%;">Discount Type</th>
                                                        <th style="width: 16.66%;">Discount Value</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($price_discount_details as $row )

                                                    <tr>
                                                        <td>{{ $row->product_uom->uom_name }}</td>
                                                        @if($row->discount_type ==1)
                                                        <td>Amount ($)</td>
                                                        @else
                                                        <td>Percentage (%)</td>
                                                        @endif
                                                        <td>{{ $row->discount_value }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div><!-- end #regular_selling_generated_data_table -->
                                    </div>
                                </div><!-- end .row -->
                            </div>
                            @endif
                            @if(!empty($customer_group_prices))
                            <div class="selling-price-area mt-4">
                                <h4 class="font-weight-normal">Customer Group Wise Discount</h4>
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- regular_selling generated table area -->
                                        <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                                <thead>
                                                    <tr class="">
                                                        <th style="width: 16.66%;">UOM</th>
                                                        <th style="width: 16.66%;">Discount Type</th>
                                                        <th style="width: 16.66%;">Discount Value</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($customer_group_prices as $row )

                                                    <tr>
                                                        <td>{{ $row->productUom->uom_name }}</td>
                                                        @if($row->customer_discount_type ==1)
                                                        <td>Amount ($)</td>
                                                        @else
                                                        <td>Percentage (%)</td>
                                                        @endif
                                                        <td>{{ $row->customer_discount_value }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div><!-- end #regular_selling_generated_data_table -->
                                    </div>
                                </div><!-- end .row -->
                            </div>
                            @endif

                            @if(!empty($lot_discounts))
                            <div class="selling-price-area mt-4">
                                <h4 class="font-weight-normal">Lot Wise Discount</h4>
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- regular_selling generated table area -->
                                        <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                            <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                                <thead>
                                                    <tr class="">
                                                        <th style="width: 16.66%;">UOM</th>
                                                        <th style="width: 16.66%;">Discount Type</th>
                                                        <th style="width: 16.66%;">Discount Value</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($lot_discounts as $row )
                                                    <tr>
                                                        <td>{{ $row->product_uom->uom_name }}</td>
                                                        @if($row->lot_discount_type ==1)
                                                        <td>Amount ($)</td>
                                                        @else
                                                        <td>Percentage (%)</td>
                                                        @endif
                                                        <td>{{$row->lot_discount_value}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div><!-- end #regular_selling_generated_data_table -->
                                    </div>
                                </div><!-- end .row -->
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title title-heading">
                            <a href="#panelBodyTwo" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Campaign Price</a>
                        </h4>
                    </div>
                    
                
                    <div id="panelBodyTwo" class="panel-collapse collapse">
                        @if(!empty($campaign_prices))
                        <h4 class="font-weight-normal mt-4">All Campaign Price</h4>
                        <div class="panel-body">
                            <!-- regular_selling generated table area -->
                            <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                    <thead>
                                        <tr class="">
                                            <th style="width: 16.66%;">UOM</th>
                                            <th style="width: 16.66%;">Discount Type</th>
                                            <th style="width: 16.66%;">Discount Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($campaign_prices as $row )
                                        <tr>
                                            <td>{{ $row->product_uom->uom_name }}</td>
                                            @if($row->discount_type ==1)
                                            <td>Amount ($)</td>
                                            @else
                                            <td>Percentage (%)</td>
                                            @endif
                                            <td>{{$row->discount_value}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- end #regular_selling_generated_data_table -->
                        </div>
                        @endif

                        @if(!empty($campaign_discount))
                        <h4 class="font-weight-normal">Individual Campaign Wise Price</h4>
                        <div class="panel-body">
                            <!-- regular_selling generated table area -->
                            <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                    <thead>
                                        <tr class="">
                                            <th style="width: 16.66%;">UOM</th>
                                            <th style="width: 16.66%;">Discount Type</th>
                                            <th style="width: 16.66%;">Discount Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($campaign_discount as $row )
                                        <tr>
                                            <td>{{ $row->productUom->uom_name }}</td>
                                            @if($row->campaign_discount_type ==1)
                                            <td>Amount ($)</td>
                                            @else
                                            <td>Percentage (%)</td>
                                            @endif
                                            <td>{{$row->campaign_discount_value}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- end #regular_selling_generated_data_table -->
                        </div>
                        @endif
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title title-heading">
                            <a href="#panelBodyThree" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Store Wise Price</a>
                        </h4>
                    </div>
                    <div id="panelBodyThree" class="panel-collapse collapse">
                        @if(!empty($store_discounts))
                        <h4 class="font-weight-normal mt-4">Available Store Regular Price</h4>
                        <div class="panel-body">
                            <!-- regular_selling generated table area -->
                            <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                    <thead>
                                        <tr class="">
                                            <th style="width: 16.66%;">UOM</th>
                                            <th style="width: 16.66%;">Discount Type</th>
                                            <th style="width: 16.66%;">Discount Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($store_discounts as $row )
                                        <tr>
                                            <td>{{$row->productuoms->uom_name }}</td>
                                            @if($row->store_discount_type ==1)
                                            <td>Amount ($)</td>
                                            @else
                                            <td>Percentage (%)</td>
                                            @endif
                                            <td>{{$row->store_discount_value}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- end #regular_selling_generated_data_table -->
                        </div>
                        @endif
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title title-heading">
                            <a href="#panelBodyFore" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Vat & Tax</a>
                        </h4>
                    </div>
                    <div id="panelBodyFore" class="panel-collapse collapse">
                        <h4 class="font-weight-normal mt-4">All Store Vat & Tax</h4>
                        <div class="panel-body">
                            <!-- regular_selling generated table area -->
                            <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                    <thead>
                                        <tr class="">
                                            <th style="width: 16.66%;">UOM</th>
                                            <th style="width: 16.66%;">Discount Type</th>
                                            <th style="width: 16.66%;">Discount Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($price_vat_tax_details as $row )
                                        <tr>
                                            <td>{{$row->productuom->uom_name }}</td>
                                            @if($row->vat_tax_type ==1)
                                            <td>Amount ($)</td>
                                            @else
                                            <td>Percentage (%)</td>
                                            @endif
                                            <td>{{$row->vat_tax_value}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- end #regular_selling_generated_data_table -->
                        </div>


                        <h4 class="font-weight-normal">Individual Store Wise Vat & Tax</h4>
                        <div class="panel-body">
                            <!-- regular_selling generated table area -->
                            <div class="adding-items-area tile pl-0 pr-0" id="regular_selling_generated_data_table">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                    <thead>
                                        <tr class="">
                                            <th style="width: 16.66%;">UOM</th>
                                            <th style="width: 16.66%;">Discount Type</th>
                                            <th style="width: 16.66%;">Discount Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($store_vat_tax as $row )
                                        <tr>
                                            <td>{{$row->productuom->uom_name }}</td>
                                            @if($row->store_vat_tax_type ==1)
                                            <td>Amount ($)</td>
                                            @else
                                            <td>Percentage (%)</td>
                                            @endif
                                            <td>{{$row->store_vat_tax_value}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- end #regular_selling_generated_data_table -->
                        </div>


                    </div>
                </div>
            </div>

        </div><!-- end .tile -->
    </form>
</div>
@endsection