@extends('price::price-management.price.create')
@section('price_contents')
<style>
  .select2-result-repository__title {
    display: inline;
  }

  .select2-result-repository__uom {
    margin: 5px;
  }

  .tile {
    border-radius: 0 8px 8px 8px;
  }

  /* .select2-search--dropdown {
    
  } */

  .select2-search--dropdown {
    top: -28px;
    padding: 0px !important;
  }

  .select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 22px !important;
}
</style>
<div class="col-md-12 ">
  <!-- nav bar start from here -->

  @include('price::price-management.price.components.inc.navbar')

  <form action="{{ route('product.store') }}" method="post">
    @csrf
    <!-- tab content -->
    <div class="tab-content px-3 px-sm-0" id="nav-tabContent">
      <!-- product-content -->
      <div class="tab-pane fade show active" id="nav-product" role="tabpanel" aria-labelledby="nav-product-tab">
        <div class="row">
          <div class="col-md-12">
            <div class="tile c-ml-1">
              <div class="row mt-12 pr-5">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="select_product" class="form-label">Product Type <span class="text-danger">*</span></label>
                    <select name="product_type" id="product_type" class="form-control single-select">
                      <option value="" disabled selected>Select Product Type</option>
                      @foreach ($product_types as $row)
                      <option value="{{ $row->id }}">{{ $row->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div><!-- end .col-md-3 -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label for="select_product" class="form-label">Search By <span class="text-danger">*</span></label>
                    <select id="product_variation" name="product_variation" class="form-control single-select">
                      <option value="" disabled selected>Select Product Variation</option>
                      <option value="product">Product</option>
                      <option value="generic_product">Generic Product</option>
                    </select>
                  </div>
                </div><!-- end .col-md-3 -->


                <!-- <div class="col-md-6">
                                                                                        <div class="search_product">
                                                                                            <div class="form-group">
                                                                                                <label for="base_quantity">Search Product</label>
                                                                                                <input type="text" class="form-control" id="product_search" name="search" value="" placeholder="Search Product ">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div> -->

                <div class="col-md-6">
                  <div class="search_product">
                    <div class="form-group">
                      <label for="base_quantity">Product <span class="text-danger">*</span></label>
                      <select class="form-control select_product"></select>
                    </div>
                  </div>
                </div>




              </div><!-- end .row -->
              <div class="row">
                <div class="col-md-12">

                  <!-- product generated table area -->
                  <div class="adding-items-area" id="product_generated_data_table">
                    <table class="table table-striped table-bordered table-hover bg-white table-sm mt-4">
                      <thead>
                        <tr class="">
                          <th width="
                                                        25%">Product Name</th>
                          <th width="25%">Purchase UOM</th>
                          <th width="25%">Per Unit Price</th>
                          <th width="25%">Currency</th>
                        </tr>
                      </thead>
                      <tbody id="product_list">

                      </tbody>
                    </table>
                  </div><!-- end #product_generated_data_table -->

                  <!-- preloader area start -->
                  <div class="tile" id="preloader">
                    <div class="tile-body">
                      <div class="row text-center">
                        <div class="col-md-12">
                          <h6 class="text-center">Processing....</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- preloader area end -->
                </div>
              </div><!-- end .row -->
              <hr>
              <div class="row text-right">
                <div class="col-md-12">
                  <button class="btn index-btn" type="submit">Next</button>
                </div>
              </div><!-- end .row -->
            </div><!-- end .tile -->
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
      </div><!-- end #nav-product -->
    </div> <!-- end .tab-content -->
  </form>

  @endsection

  @push('post_scripts')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  // search product
  <script>
    $(document).ready(function() {
      $('#preloader').hide();
      $('#product_search').on('keyup', function() {
        var product_type = $('#product_type option:selected').val();
        var product_variation = $('#product_variation option:selected').val();
        var product_search = $('#product_search').val();

        alert(product_type);
        if (product_type == null) {
          alert('Product Type Must Not be empty!')
        }
        if (!product_variation) {
          alert('Product Variation Type Must Not be empty!')
        }


        $('#product_generated_data_table').hide();
        $('#preloader').show();

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          type: 'GET',
          url: "{{ route('product.search') }}",
          data: {
            product_type,
            product_variation,
            product_search
          },
          success: function(data) {
            $('#product_list').empty();
            $('#product_generated_data_table').show();
            $('#preloader').hide();

            $('#product_list').append(data);
          },
          error: function(err) {

            console.dir(err);

          }
        });
      });
    });
  </script>


  <script>
    $(document).ready(function() {
      $(".select_product").select2({
        ajax: {
          url: "{{ route('product.search') }}",
          dataType: 'json',
          delay: 250,
          data: function(params) {
            return {
              product_type: $('#product_type option:selected').val(),
              product_variation: $('#product_variation option:selected').val(),
              product_search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: data,


            };
          },
          cache: true
        },
        placeholder: 'Search for a repository',
        minimumInputLength: 2,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
      });
    })


    function formatRepo(repo) {
      if (repo.loading) {
        return repo.text;
      }

      var $container = $(
        "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__title'></div>" +
        "<small class='select2-result-repository__uom'></small>" +
        "</div>"
      );

      $container.find(".select2-result-repository__title").text(repo.product_variation_name);
      $container.find(".select2-result-repository__uom").text('(' + repo.uom_name + ')');


      return $container;
    }

    function formatRepoSelection(repo) {

      var html = `<tr>
                                <td>${repo.product_variation_name}</td>'
                                <td>${repo.uom_name}</td>
                                <td>${repo.unit_price}</td>
                                <td>BDT <div class="form-check"><input value='${repo.id}' type="hidden" name="product_id" id="product_id"></div></td>
                                '</tr>'`;

      $('#product_list').empty();
      if (repo.disabled == false) {
        $('#product_list').append(html);
      }

    }
  </script>



  @endpush