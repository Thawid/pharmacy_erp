@extends('price::price-management.price.create')
@push('styles')
    <style>
        .mt-28 {
            margin-top: 28px;
        }

        .apply_for_all_store {
            margin-top: 3px;
        }

        #add_indi_store_wise_btn {
            font-size: 18px;
            line-height: 18px;
        }

        .remove_indi_store_vt_ag_data {
            font-size: 18px;
            line-height: 18px;
        }

        #add_all_store_vat_tax_btn {
            font-size: 20px;
            line-height: 18px;
        }

        #remove_all_store_vat_tax_btn {
            font-size: 18px;
            line-height: 18px;
        }

        .remove_updated_individual_store_wise_vt_row {
            font-size: 20px;
            line-height: 18px;
        }

        .create-btn {
            color: gray !important;
            font-size: 25px !important;
            line-height: 20px !important;
        }

    </style>
@endpush
@section('price_contents')
    <div class="col-md-12 ">

        <!-- nav bar start from here -->

        @include('price::price-management.price.components.inc.navbar',['price'=>$prices])

        <form method="post" action="{{ route('pricing.vat_tax.store') }}">
            @csrf

            <div class="tile pt-4">
                <!-- start radio-button-operation -->
                <div class="row">
                    <div class="col-md-5">
                        <h5>All Store Vat & Tax</h5>
                    </div>
                </div><!-- end .row -->
                <hr>
                <!-- hidden field area start from here -->
                <input type="hidden" value="{{ $prices->id }}" name="price_id" />
                <input type="hidden" value="{{ $product->id }}" name="product_variation_id" />
                <input type="hidden" value="{{ $product->product_generic_id }}" name="generic_id" />
                <!-- hidden field area start end here -->

                <!-- start #all_store_vat_tax -->
                <div id="all_store_vat_tax">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- all store generated table area -->
                            <div class="adding-items-area" id="all_store_table_area">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm">
                                    <thead>
                                        <tr class="">
                                            <th width="32.33%">UOM <span class="text-danger">*</span></th>
                                            <th width="32.33%">Vat & Tax Type <span class="text-danger">*</span></th>
                                            <th width="32.33%">Vat & Tax Value <span class="text-danger">*</span></th>
                                            <th width="3%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="auto_generate_store_vat_tax_tbody">
                                        <!-- start take input row -->
                                        <tr>
                                            <td>
                                                <select id="select_all_store_uom" class="form-control">
                                                    <option value="">--- Select UOM ---</option>
                                                    @foreach ($product->productuom as $row)
                                                    <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </td>

                                            <td>
                                                <select name="vat_tax_type" id="select_all_store_vt_type"
                                                    class="form-control">
                                                    <option value="">--- Select vat & tax Type ---</option>
                                                    <option value="1">Amount ($)</option>
                                                    <option value="2">Percentage (%)</option>
                                                </select>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" id="all_store_vt_value"
                                                    value="{{ old('discount_value') }}" placeholder="enter discount value"
                                                    onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                            </td>


                                            <td>
                                                <a class="btn create-btn w-100"
                                                    id="add_all_store_vat_tax_btn">+</a>
                                            </td>
                                        </tr><!-- end .take input row -->

                                        <!-- update data for create file for all store  -->
                                        @if (!empty($prices))
                                            @foreach ($prices->allStoreVatnTax as $row)
                                                <tr>
                                                    <td>
                                                        @if (isset($row->productUom->uom_name))
                                                            <input class="form-control"
                                                                value="{{ $row->productUom->uom_name }}" readonly>
                                                            <input type="hidden" name="indi_store_wise_vt_uom[]"
                                                                value="{{ $row->uom_id }}">
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if ($row->vat_tax_type == '1')
                                                            <input class="form-control" value="Amount ($)" readonly>
                                                        @endif

                                                        @if ($row->vat_tax_type == '2')
                                                            <input class="form-control" value="Percentage (%)" readonly>
                                                        @endif

                                                        <input type="hidden" name="indi_vat_tax_type[]"
                                                            value="{{ $row->vat_tax_type }}">
                                                    </td>

                                                    <td>
                                                        <input class="form-control" value="{{ $row->vat_tax_value }}"
                                                            readonly>
                                                        <input type="hidden" name="indi_vat_tax_value[]"
                                                            value="{{ $row->vat_tax_value }}">

                                                    </td>

                                                    <td>
                                                        <div
                                                            class="btn btn-outline-danger remove_updated_individual_store_wise_vt_row w-100">X
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div><!-- end #auto_individual_campaign_price_table -->
                        </div>
                    </div><!-- end .row -->
                    <div class="row apply-vt-for-all-store text-right">
                        <div class="col-md-12">
                            <button type="button" class="btn index-btn" id="apply_vt_for_all_store"
                                name="apply_vt_for_all_store">Apply Vat Tax For All Store</button>
                        </div>
                    </div>
                </div><!-- end #all_store_vat_tax -->

                <!-- start #individual_store_wise_area -->
                <div id="individual_store_wise_area">
                    <div class="row mt-5">
                        <div class="col-md-6">
                            <h5>Individual Store Wise Vat & Tax</h5>
                        </div>
                    </div>
                    <hr>

                    <div class="row mt-2">
                        <div class="col-md-12">
                            <!-- individual_campaign generated table area -->
                            <div class="adding-items-area" id="individual_store_vat_and_tax_table">
                                <table class="table table-striped table-bordered table-hover bg-white table-sm mb-0">
                                    <thead>
                                        <tr class="text-center">
                                            <th width="22%">UOM <span class="text-danger">*</span></th>
                                            <th width="25%">Store Name <span class="text-danger">*</span></th>
                                            <th width="25%">Vat & Tax Type <span class="text-danger">*</span></th>
                                            <th width="25%">Vat & Tax Value <span class="text-danger">*</span></th>
                                            <th width="3%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="auto_individual_store_wise_price_table">
                                        <!-- the first input row -->
                                        <tr>
                                            <td>
                                                <select id="select_individual_store_wise_vt_uom" class="form-control">
                                                    <option value="">--- Select UOM ---</option>
                                                    @foreach ($product->productuom as $row)
                                                    <option value="{{ $row->uom_id }}">{{ $row->uom->uom_name ?? '' }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </td>

                                            <td>
                                                <select name="" id="select_campaign" class="form-control">
                                                    <option value="" selected>--- Select Store ---</option>
                                                    @foreach ($stores as $row)
                                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>

                                            <td>
                                                <select name="" id="select_individual_campaign_wise_discount_type"
                                                    class="form-control">
                                                    <option value="" selected>--- Select Vat & Tax Type ---</option>
                                                    <option value="1">Amount ($)</option>
                                                    <option value="2">Percentage (%)</option>
                                                </select>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" name=""
                                                    id="individual_campaign_wise_discount_value"
                                                    placeholder="enter Vat & Tax value"
                                                    onkeypress="return (event.charCode !=8 && event.charCode == 0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                                            </td>
                                            <td>
                                                <div class="btn create-btn w-100" id="add_indi_store_wise_btn">
                                                    +</div>
                                            </td>
                                        </tr><!-- end the first input row -->

                                        <!-- update data for create file  -->
                                        @if (!empty($prices))
                                            @foreach ($prices->individualStoreWiseVatnTax as $row)
                                                <tr>
                                                    <td>
                                                        @if (isset($row->productUom->uom_name))
                                                            <input class="form-control"
                                                                value="{{ $row->productUom->uom_name }}" readonly>
                                                            <input type="hidden" name="indi_store_wise_vt_uom[]"
                                                                value="{{ $row->productUom->id }}">
                                                        @endif
                                                    </td>

                                                    <td>
                                                        <input class="form-control" value="{{ $row->storeName->name }}"
                                                            readonly>
                                                        <input type="hidden" name="store_id[]"
                                                            value="{{ $row->storeName->id }}">

                                                    </td>

                                                    <td>
                                                        @if ($row->store_vat_tax_type == '1')
                                                            <input class="form-control" value="Amount ($)" readonly>
                                                        @endif

                                                        @if ($row->store_vat_tax_type == '2')
                                                            <input class="form-control" value="Percentage (%)" readonly>
                                                        @endif

                                                        <input type="hidden" name="indi_vat_tax_type[]"
                                                            value="{{ $row->store_vat_tax_type }}">
                                                    </td>

                                                    <td>
                                                        <input class="form-control"
                                                            value="{{ $row->store_vat_tax_value }}" readonly>
                                                        <input type="hidden" name="indi_vat_tax_value[]"
                                                            value="{{ $row->store_vat_tax_value }}">

                                                    </td>

                                                    <td>
                                                        <div
                                                            class="btn btn-outline-danger remove_updated_individual_store_wise_vt_row w-100">
                                                            X</div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div><!-- end #auto_individual_store_wise_price_table -->
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                </div><!-- end #individual_store_wise_area -->

                <hr>
                <div class="row text-right">
                    <div class="col-md-12">
                    <a href="{{route('pricing.active.price',$prices->id)}}"
                            class="btn index-btn">
                            Skip</a>
                        <button type="submit" class="btn index-btn"
                            id="vat-n-tax-next-btn">Next</button>
                    </div>
                </div><!-- end .row -->
            </div><!-- end .tile -->
        </form>
    @endsection

    @push('post_scripts')

        {{-- run on first try --}}
        <script>
            // hide next button 
            $("#vat-n-tax-next-btn").hide();
        </script>

        <script>
            $("#apply_vt_for_all_store").hide();
            $(document).on('click', '.remove_updated_individual_store_wise_vt_row', function() {
                $(this).parent().parent().remove();
            });
        </script>
        <!-- discount-tab-scripts -->

        <!-- add add_group_wise_discount -->
        <script>
            $(document).ready(function() {
                $("#add_all_store_vat_tax_btn").on('click', function() {
                    let all_store_vt_uom_value = $("#select_all_store_uom option:selected").val();
                    let all_store_vt_uom_text = $("#select_all_store_uom option:selected").text();
                    let all_store_vt_type_value = $("#select_all_store_vt_type option:selected").val();
                    let all_store_vt_type_text = $("#select_all_store_vt_type option:selected").text();
                    let all_store_vt_value = $("#all_store_vt_value").val();

                    let clear_all_store_vt_uom = "";
                    let clear_all_store_vt_type = "";
                    let clear_all_store_vt_value = "";

                    let is_multiple_uom_for_all_store_vt = false;
                    let all_store_vt_uom_array = document.querySelectorAll('.all_store_vt_uom_array');

                    all_store_vt_uom_array.forEach(function(e) {
                        if (e.value == all_store_vt_uom_value) {
                            is_multiple_uom_for_all_store_vt = true;
                        }
                    })

                    // check multiple uom or not
                    if (is_multiple_uom_for_all_store_vt) {
                        alert("Uom name is already exist!");
                    } else {
                        if (all_store_vt_uom_value > 0 && all_store_vt_type_value > 0 &&
                            all_store_vt_value >
                            0) {
                            $("#auto_generate_store_vat_tax_tbody").append(
                                `<tr>
                                    <td>
                                        <input value="${all_store_vt_uom_text}" class="form-control" readonly>
                                        <input type="hidden" value="${all_store_vt_uom_value}" class="all_store_vt_uom_array" name="all_store_vt_uom_ag[]" id="all_store_vt_uom_ag_id">
                                    </td>

                                    <td>
                                        <input value="${all_store_vt_type_text}" class="form-control" readonly>
                                        <input type="hidden" value="${all_store_vt_type_value}" name="all_store_vt_type_ag[]">
                                    </td>
                                    
                                    <td>
                                        <input value="${all_store_vt_value}" class="form-control" readonly>
                                        <input type="hidden" value="${all_store_vt_value}" name="all_store_vt_value_ag[]">
                                    </td>

                                    <td>
                                        <div class="btn btn-outline-danger w-100" id="remove_all_store_vat_tax_btn">X</div>
                                    </td>
                                </tr>`
                            );

                            // check next button validation
                            if (($("#all_store_vt_uom_ag_id").val() > 0) || 
                            ($("#individual_store_wise_vat_n_tax_uom_id").val() > 0)) {
                                $("#vat-n-tax-next-btn").show();
                            } else {
                                $("#vat-n-tax-next-btn").hide();
                            }

                            // clearing fields after insert
                            $("#select_all_store_uom").val(clear_all_store_vt_uom);
                            $("#select_all_store_vt_type").val(clear_all_store_vt_type);
                            $("#all_store_vt_value").val(clear_all_store_vt_value);

                            // show the button
                            $("#apply_vt_for_all_store").show();
                        } else {
                            alert("Please Give Valid Input");
                        }
                    }

                    // remove row on button click
                    $(document).on('click', '#remove_all_store_vat_tax_btn', function() {
                        $(this).parent().parent().remove();

                        // hide apply for all vat and tax button
                        if ($("#all_store_vt_uom_ag_id").val() > 0) {
                            $("#apply_vt_for_all_store").show();
                        } else {
                            $("#apply_vt_for_all_store").hide();
                        }

                        // check next button validation
                        if (($("#all_store_vt_uom_ag_id").val() > 0) || 
                            ($("#individual_store_wise_vat_n_tax_uom_id").val() > 0)) {
                                $("#vat-n-tax-next-btn").show();
                            } else {
                                $("#vat-n-tax-next-btn").hide();
                        }
                    });
                });


                $("#add_indi_store_wise_btn").on('click', function() {
                    let store_val = $("#select_campaign option:selected").val();
                    let store_name_text = $("#select_campaign option:selected").text();
                    let d_type_val = $("#select_individual_campaign_wise_discount_type").find(":selected")
                        .val();
                    let d_type_text = $("#select_individual_campaign_wise_discount_type").find(":selected")
                        .text();
                    let vat_val = $("#individual_campaign_wise_discount_value").val();

                    let indi_store_wise_uom_val = $("#select_individual_store_wise_vt_uom").find(":selected")
                        .val();
                    let indi_store_wise_uom_text = $("#select_individual_store_wise_vt_uom").find(":selected")
                        .text();

                    let set_empty_campaign_name = "";
                    let set_empty_d_type_val = "";
                    let set_empty_d_val = "";
                    let set_empty_indi_store_wise_uom = "";

                    let is_multiple_uom_for_individual_store_vt = false;
                    let individual_store_vt_uom_array = document.querySelectorAll(
                        '.individual_store_vt_uom_array');

                    individual_store_vt_uom_array.forEach(function(e) {
                        if (e.value == indi_store_wise_uom_val) {
                            is_multiple_uom_for_individual_store_vt = true;
                        }
                    })

                    if (is_multiple_uom_for_individual_store_vt) {
                        alert("The Uom is already selected!");
                    } else {
                        if (vat_val > 0 && d_type_val > 0 && vat_val > 0 && indi_store_wise_uom_val) {
                            $('#auto_individual_store_wise_price_table').append(
                                `<tr>
                                    <td> 
                                        <input class="form-control" value="${indi_store_wise_uom_text}" readonly> 
                                        <input type="hidden" class="individual_store_vt_uom_array" name="indi_store_wise_vt_uom[]" value="${indi_store_wise_uom_val}" id="individual_store_wise_vat_n_tax_uom_id"> 
                                    </td> 

                                    <td> 
                                        <input class="form-control" value="${store_name_text}" readonly> 
                                        <input type="hidden" name="store_id[]" value="${store_val}"> 
                                    </td> 

                                    <td> 
                                        <input class="form-control" value="${d_type_text}" readonly> 
                                        <input type="hidden" name="indi_vat_tax_type[]" value="${d_type_val}"> 
                                    </td> 

                                    <td> 
                                        <input type="text" class="form-control" name="indi_vat_tax_value[]" value="${vat_val}" readonly> 
                                    </td> 

                                    <td> 
                                        <div class="btn btn-outline-danger remove_indi_store_vt_ag_data w-100">X</div> 
                                    </td>
                                </tr>`);

                            // check next button validation
                            if (($("#all_store_vt_uom_ag_id").val() > 0) || 
                            ($("#individual_store_wise_vat_n_tax_uom_id").val() > 0)) {
                                $("#vat-n-tax-next-btn").show();
                            } else {
                                $("#vat-n-tax-next-btn").hide();
                            }

                            $("#select_campaign").val(set_empty_campaign_name);
                            $("#select_individual_campaign_wise_discount_type").val(set_empty_d_type_val);
                            $("#individual_campaign_wise_discount_value").val(set_empty_d_val);
                            $("#select_individual_store_wise_vt_uom").val(set_empty_indi_store_wise_uom);
                        } else {
                            alert("Please give valid input before add");
                        }
                    }
                });

                // remove generated row
                $(document).on('click', '.remove_indi_store_vt_ag_data', function() {
                    $(this).parent().parent().remove();

                    // check next button validation
                    if (($("#all_store_vt_uom_ag_id").val() > 0) || 
                        ($("#individual_store_wise_vat_n_tax_uom_id").val() > 0)) {
                        $("#vat-n-tax-next-btn").show();
                    } else {
                        $("#vat-n-tax-next-btn").hide();
                    }
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $('#apply_vt_for_all_store').click(function() {
                    let data = `<input type="hidden" name="apply_vt_for_all_store_value" value="${1}">`;
                    $(".apply-vt-for-all-store").append(data);
                    $("#apply_vt_for_all_store").hide();
                    console.log("hiding the button");
                });
            });
        </script>

    @endpush
