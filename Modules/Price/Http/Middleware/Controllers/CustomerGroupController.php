<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Modules\Price\Http\Requests\CreateCustomerGroupRequest;
use Modules\Price\Http\Requests\UpdateCustomerGroupRequest;
use Modules\Price\Repositories\CustomerGroupInterface;

class CustomerGroupController extends Controller
{

    private $customerGroupInterface;
    public function __construct(CustomerGroupInterface $customerGroupInterface)
    {
        $this->customerGroupInterface = $customerGroupInterface;
    }

    public function index()
    {
        $customer_groups = DB::table('customer_groups')->orderBy('id', 'desc')->get();
        return view('price::price-management.customer-group.index', compact('customer_groups'));
    }

    public function create()
    {
        return view('price::price-management.customer-group.create');
    }

    public function store(CreateCustomerGroupRequest $request)
    {
        $this->customerGroupInterface->store($request);

        return redirect()->route('customer-group.index')->with('success', 'Customer group has been Created successfully');
    }

    public function show($id)
    {
        $customer_group = DB::table('customer_groups')->where('id', $id)->first();
        return view('price::price-management.customer-group.show', compact('customer_group'));
    }

    public function edit($id)
    {
        $customer_group = DB::table('customer_groups')->where('id', $id)->first();
        return view('price::price-management.customer-group.edit', compact('customer_group'));
    }

    public function update(UpdateCustomerGroupRequest $request, $id)
    {
        $this->customerGroupInterface->update($request, $id);

        return redirect()->route('customer-group.index')->with('success', 'Customer group has been Updated successfully');
    }

    public function destroy($id)
    {
        //
    }
}
