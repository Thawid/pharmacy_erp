<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Price\Entities\Price;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductType;
use Modules\Product\Entities\ProductVariation;
use DB;
use Modules\Price\Entities\UomPrice;

class PriceController extends Controller
{
    public function index()
    {
        return view('price::price-management.price.index');
    }

    public function create()
    {
        $product_types = ProductType::where('status', 1)->get();
        return view('price::price-management.price.components.product_search', compact('product_types'));
    }

    public function store(Request $request)
    {
        return "store";
    }

    public function show($id)
    {
        return view('price::price-management.price.show');
    }

    public function edit($id)
    {
        return view('price::price-management.price.edit');
    }

    public function update(Request $request, $id)
    {
        return "update";
    }

    public function destroy($id)
    {
        return "destroy";
    }

    public function product_search(Request $request)
    {
        $searchstr = $request->product_search;

        if (strlen($searchstr) == 0) {
            return $search = [];
        }

        if ($request->product_variation == 'product') {
            $search = Product::where('product_type_id', $request->product_type)->with(['productVariations' => function ($query) use ($searchstr) {
                $query->with(['product_uom', 'purchase_order'])->where('product_variation_name', 'LIKE', '%' . $searchstr . "%")->get();
            }])->get();


            $output = "";

            if (count($search) > 0) {
                foreach ($search as $data) {
                    if (count($data->productVariations) > 0) {
                        foreach ($data->productVariations as $row) {
                            $output .= '<tr>' .
                                '<td>' . $row->product_variation_name . '</td>' .
                                '<td>' . $row->product_uom->uom_name . '</td>' .
                                '<td>' . $row->purchase_order->unit_price . '</td>' .
                                '<td>BDT</td>' .
                                '<td><div class="form-check"><input class="form-check-input" value=' . $row->id . ' type="radio" name="product_id"></div></td>' .
                                '</tr>';
                        }
                    } else {
                        $output = '<tr><th class="text-center" colspan="5">No Data Found!</th></tr>';
                    }
                }
            } else {
                $output = '<tr><th class="text-center" colspan="5">No Data Found!</th></tr>';
            }


            return response()->json($output, 200);
        }


        $product_type = $request->product_type;
        if ($request->product_variation == 'generic_product') {
            $search = ProductGeneric::where('status', 1)->where('name', 'LIKE', '%' . $searchstr . "%")->with(['products' => function ($query) use ($product_type) {
                $query->where('product_type_id', $product_type)->with('productVariations')->get();
            }])->get();

            $output = "";

            if (count($search) > 0) {
                foreach ($search as $products) {
                    foreach ($products->products as $data) {
                        foreach ($data->productVariations as $row) {
                            $output .= '<tr>' .
                                '<td>' . $row->product_variation_name . '</td>' .
                                '<td>100</td>' .
                                '<td>10</td>' .
                                '<td>BDT</td>' .
                                '<td><div class="form-check"><input class="form-check-input" value=' . $row->id . ' type="radio" name="product_id"></div></td>' .
                                '</tr>';
                        }
                    }
                }
            } else {
                $output = '<tr><th class="text-center" colspan="5">No Data Found!</th></tr>';
            }
            return response()->json($output, 200);
        }
    }


    // searching product store
    public function product_store(Request $request)
    {
        DB::beginTransaction();
        try {
            $product = ProductVariation::findOrFail($request->product_id);
            if ($product) {
                $price = new Price();
                $price->product_type_id             = $request->product_type;
                $price->product_variation_id        = $product->id;
                $price->regular_bundle_id           = 0;
                $price->product_generic_id          = 0;
                $price->save();
                DB::commit();
                return redirect()->route('pricing.box_packaging.create', $price->id);
                // return redirect()->route('pricing.box_packaging.create')->with('success', 'Advance Product Bundle has been Created successfully');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    // show box and packaging create form
    public function box_packaging_create($id = null)
    {
        if ($id == null) {
            return redirect()->route('price.create');
        }
        $price = Price::findOrFail($id);

        $product = ProductVariation::with(['purchase_order' => function ($query) {
            $query->latest();
        }, 'product'])->findOrFail($price->product_variation_id);

        $product_uom = ProductVariation::with('product_uom')->where('product_id', $product->product_id)->get();

        return view('price::price-management.price.components.box_packaging', compact('product', 'product_uom', 'id'));
    }

    // get capacity per uom
    public function capacity_per_uom(Request $request)
    {
        $capacity = ProductVariation::select('quantity_per_uom')->where('product_id', $request->product_id)->where('uom_id', $request->uom_val)->first();
        return response()->json($capacity, 200);
    }

    // store box and packaging data
    public function box_packaging_store(Request $request)
    {

        if ($request->pricing_selector == 'regular_selling') {
            $base_qty = $request->regular_base_qty;
            $uom_id = $request->regular_uom_id;
            $uom_price = $request->regular_uom_price;
            $capacity = $request->regular_capacity;
            $uom_price = [];
            for ($i = 0; $i < count($base_qty); $i++) {
                $uom_price[] = [
                    'price_id' => $request->price_id,
                    'product_variation_id' => $request->product_variation_id,
                    'regular_bundle_id' => 0,
                    'generic_id' => $request->generic_id,
                    'uom_id' => $uom_id[$i],
                    'uom_price' => $request->regular_uom_price[$i],
                    'currency' => 'BDT',
                    'capacity' => $capacity[$i],
                ];
            }

            Price::findOrFail($request->price_id)->update([
                'regular_price_type' => $request->margin_type,
                'regular_price_value' => $request->margin_amount,
                'ysn_equal_for_all_uom' => 1,
            ]);

            UomPrice::insert($uom_price);
        }

        return redirect()->route('pricing.discount', $request->price_id);
    }

    // price discount page show
    public function price_discount($id = null)
    {
        return view('price::price-management.price.components.discount');
    }
}
