<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Price\Entities\CampaignPrice;
use Modules\Price\Entities\StorePrice;
use Modules\Price\Entities\StoreVatTaxPrice;
use Modules\Price\Entities\UomPrice;
use Modules\Product\Entities\ProductVariation;
use Modules\Price\Entities\LotPrice;
use Modules\Price\Entities\CustomerGroupPrice;
use Modules\Price\Entities\LotDiscount;
use Modules\Price\Entities\Price;
use Modules\Price\Entities\PriceCampaignDiscountDetails;
use Modules\Price\Entities\PriceCustomerDiscountDetails;
use Modules\Price\Entities\PriceVatTaxDetails;
use Modules\Product\Entities\Product;
use DB;

class ActivePriceController extends Controller
{

    public function activePrice($id = null)
    {

        if ($id == null) {
            return redirect()->route('price.create');
        }
        
        $prices = Price::findOrFail($id);

        $product = Product::select('product_name')->findOrFail($prices->product_id);

        $uom_prices = UomPrice::with(['product', 'product_uom'])->where('price_id', $id)->get();
        $lot_prices = LotPrice::with(['product', 'product_uom'])->where('price_id', $id)->get();

        $price_discount_details = PriceCustomerDiscountDetails::with(['product', 'product_uom'])->where('price_id', $id)->get();
        $customer_group_prices = CustomerGroupPrice::with(['product', 'productUom'])->where('price_id', $id)->get();
        $lot_discounts = LotDiscount::with(['product', 'product_uom'])->where('price_id', $id)->get();

        $campaign_prices =PriceCampaignDiscountDetails::with(['product', 'product_uom'])->where('price_id', $id)->get();
        $campaign_discount = CampaignPrice::where('price_id', $id)->with(['productUom'])->get();

        $store_discounts = StorePrice::with('productUoms')->where('price_id', $id)->get();
        $store_vat_tax = StoreVatTaxPrice::with('productUom')->where('price_id', $id)->get();
        $price_vat_tax_details = PriceVatTaxDetails::with('productUom')->where('price_id', $id)->get();

        return view('price::price-management.price.components.active-price', compact('product','uom_prices','campaign_discount', 'store_discounts', 'store_vat_tax','lot_prices','price_discount_details','customer_group_prices','lot_discounts','lot_discounts','campaign_prices','prices','price_vat_tax_details'));
    }

}
