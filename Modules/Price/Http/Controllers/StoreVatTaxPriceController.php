<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Price\Entities\Price;
use Modules\Price\Entities\StoreVatTaxPrice;
use Modules\Product\Entities\ProductVariation;
use Modules\Store\Entities\Store;
use DB;
use Modules\Price\Entities\PriceVatTaxDetails;
use Modules\Product\Entities\Product;

class StoreVatTaxPriceController extends Controller
{
    public function store_vat_tax_create($id = null)
    {
        // return $request->all();
        if ($id == null) {
            return redirect()->route('price.create');
        }


        // --- start update
        $prices = [];
        $prices = Price::with(['individualStoreWiseVatnTax', 'allStoreVatnTax'])->findOrFail($id);
        $stores = Store::where('status', 1)->get(['id', 'name']);
        $product = Product::with('Productuom')->findOrFail($prices->product_id);
        $select_uom = ProductVariation::with('product_uom')->where('product_id', $product->product_id)->get();
        DB::table('price_temp')->where('price_id', $id)->delete();

        return view('price::price-management.price.components.store_vat_tax', compact('product','stores','prices'));
    }

    public function vat_tax_store(Request $request)
    {
        // return $request->all();
        try {
            
            if(empty($request->all_store_vt_type_ag)){
                if(PriceVatTaxDetails::where('price_id',$request->price_id)->exists()){
                    PriceVatTaxDetails::where('price_id',$request->price_id)->delete();
                }
            }

            // store or update data on vat tax details table
            if (($request->has('apply_vt_for_all_store_value') && $request->apply_vt_for_all_store_value == "1") && count($request->all_store_vt_uom_ag) > 0) {
                // return "okay";
                $all_store_vat_tax = [];
                for ($i = 0; $i < count($request->all_store_vt_uom_ag); $i++) {
                    $all_store_vat_tax[] = [
                        'price_id' => $request->price_id,
                        'product_variation_id' => $request->product_variation_id,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $request->all_store_vt_uom_ag[$i],
                        'vat_tax_type' => $request->all_store_vt_type_ag[$i],
                        'vat_tax_value' => $request->all_store_vt_value_ag[$i],
                    ];
                }
                // return "okay";
                // return $all_store_vat_tax;

                if (PriceVatTaxDetails::where('price_id', $request->price_id)->exists()) {
                    PriceVatTaxDetails::where('price_id', $request->price_id)->delete();
                }

                PriceVatTaxDetails::insert($all_store_vat_tax);

                Price::findOrFail($request->price_id)->update([
                    'ysn_vat_for_all_store' => $request->has('apply_for_all_lot') ? 1 : 0,
                ]);
            }

            if(empty($request->indi_vat_tax_value)){
                if(StoreVatTaxPrice::where('price_id',$request->price_id)->exists()){
                    StoreVatTaxPrice::where('price_id',$request->price_id)->delete();
                }
            }

            // store or update data on vat tax details table 'store vat tax' table
            if ($request->has('store_id') && !empty($request->store_id)) {
                $i = 0;
                $vat_tax = [];
                foreach ($request->store_id as $row) {
                    $vat_tax[] = [
                        'price_id' => $request->price_id,
                        'product_variation_id' => $request->product_variation_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $request->indi_store_wise_vt_uom[$i],
                        'store_id' => $request->store_id[$i],
                        'store_vat_tax_type' => $request->indi_vat_tax_type[$i],
                        'store_vat_tax_value' => $request->indi_vat_tax_value[$i],
                    ];
                    $i++;
                }

                // check if data already exist or not
                // if exist then it will be deleted
                if (StoreVatTaxPrice::where('price_id', $request->price_id)->exists()) {
                    StoreVatTaxPrice::where('price_id', $request->price_id)->delete();
                }

                StoreVatTaxPrice::insert($vat_tax);
            }
            return redirect()->route('pricing.active.price', $request->price_id);
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('price::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('price::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
