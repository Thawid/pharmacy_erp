<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Price\Entities\CustomerGroupPrice;
use Modules\Price\Entities\LotDiscount;
use Modules\Price\Entities\Price;
use Modules\Price\Entities\PriceCustomerDiscountDetails;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;

class PriceDiscountController extends Controller
{
    public function index()
    {
        return view('price::index');
    }

    public function create($id = null)
    {
        if ($id == null) {
            return redirect()->route('price.create');
        }
        $prices = [];
       $prices = Price::with(['customar_group_prices', 'lot_discount_prices', 'customar_group_prices.customerGroup', 'customar_group_prices.productUom', 'lot_discount_prices.productUom','all_customer_group_price','all_customer_group_price.productUom'])->findOrFail($id);        
        $product = Product::with('Productuom')->findOrFail($prices->product_id);
        $group_names = DB::table('customer_groups')->orderBy('id')->get();
        
        return view('price::price-management.price.components.discount', compact('group_names', 'product','prices'));
    }

    public function store(Request $request)
    {
        // return $request;

        try {

            if(empty($request->all_customer_discount_type_ag)){
                if(PriceCustomerDiscountDetails::where('price_id',$request->price_id)->exists()){
                    PriceCustomerDiscountDetails::where('price_id',$request->price_id)->delete();
                }
                Price::findOrFail($request->price_id)->update([
                    'ysn_discount_for_all_lots' => 0,
                    'ysn_discount_for_all_customers' =>0,
                ]);
            }
            

            if ( $request->has('all_customer_discount_type_ag') && count($request->all_customer_discount_type_ag) > 0) {

                

                $apply_for_all_group = [
                    'product_id' => 'required',
                    'generic_id' => 'required',
                    'all_cust_uom_value_ag' => 'required|array',
                    'all_customer_discount_type_ag' => 'required|array',
                    'all_cust_discount_value_ag' => 'required|array',
                ];

                $request->validate($apply_for_all_group);

                $customar_discount = [];
                for ($i = 0; $i < count($request->all_customer_discount_type_ag); $i++) {
                    $customar_discount[] = [
                        'price_id' => $request->price_id,
                        'product_id' => $request->product_id,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $request->all_cust_uom_value_ag[$i],
                        'discount_type' => $request->all_customer_discount_type_ag[$i],
                        'discount_value' => $request->all_cust_discount_value_ag[$i],
                    ];
                }
    
                if(PriceCustomerDiscountDetails::where('price_id',$request->price_id)->exists()){
                    PriceCustomerDiscountDetails::where('price_id',$request->price_id)->delete();
                }
                PriceCustomerDiscountDetails::insert($customar_discount);
    
                Price::findOrFail($request->price_id)->update([
                    'ysn_discount_for_all_lots' => $request->has('apply_for_all_lot')?1:0,
                    'ysn_discount_for_all_customers' =>$request->has('apply_for_all_group')?1:0,
                ]);
            }

            if(empty($request->customar_group)){
                if (CustomerGroupPrice::where('price_id', $request->price_id)->exists()) {
                    CustomerGroupPrice::where('price_id', $request->price_id)->delete();
                }
            }

            if ($request->has('customar_group') && !empty($request->customar_group)) {

                $customar_group = [
                    'product_id' => 'required',
                    'generic_id' => 'required',
                    'customer_uom_value' => 'required|array',
                    'customar_group' => 'required|array',
                    'all_cust_discount_type_ag' => 'required|array',
                    'customer_discount_value' => 'required|array',
                ];

                $request->validate($customar_group);
                $i = 0;
                $customer_group_wise_discount = [];
                foreach ($request->customar_group as $cust_group_id) {
                    $customer_group_wise_discount[] = [
                        'price_id' => $request->price_id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $request->customer_uom_value[$i],
                        'customer_group_id' => $request->customar_group[$i],
                        'customer_discount_type' => $request->all_cust_discount_type_ag[$i],
                        'customer_discount_value' => $request->customer_discount_value[$i],
                    ];
                    $i++;
                }
                if (CustomerGroupPrice::where('price_id', $request->price_id)->exists()) {
                    CustomerGroupPrice::where('price_id', $request->price_id)->delete();
                }
                CustomerGroupPrice::insert($customer_group_wise_discount);
            }
    
            if(empty($request->lot_discount)){
                if (LotDiscount::where('price_id', $request->price_id)->exists()) {
                    LotDiscount::where('price_id', $request->price_id)->delete();
                }
            }
            
            if ($request->has('lot_discount') && !empty($request->lot_discount)) {
                $i = 0;
                $lot_wise_discount = [];
                foreach ($request->lot_discount as $l_id) {
                    $lot_wise_discount[] = [
                        'price_id' => $request->price_id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $request->lot_wise_uom_ag[$i],
                        'lot_id' => $l_id,
                        'lot_discount_type' => $request->lot_discount_type[$i],
                        'lot_discount_value' => $request->lot_discount_value[$i],
                    ];
                    $i++;
                }
    
                if (LotDiscount::where('price_id', $request->price_id)->exists()) {
                    LotDiscount::where('price_id', $request->price_id)->delete();
                }
    
                LotDiscount::insert($lot_wise_discount);
            }
           
            return redirect()->route('pricing.campaign_pricing', $request->price_id);
        }catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }        
    }
}
