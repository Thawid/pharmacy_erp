<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CustomerController extends Controller
{

    public function index()
    {
        return view('price::price-management.customer.index');
    }

    public function create()
    {
        return view('price::price-management.customer.create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return view('price::price-management.customer.show');
    }

    public function edit($id)
    {
        return view('price::price-management.customer.edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
