<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Price\Entities\CampaignPrice;
use Modules\Price\Entities\Price;
use Modules\Price\Entities\PriceCampaignDiscountDetails;
use Modules\Price\Entities\UomPrice;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;

class CampaignPriceController extends Controller
{
    public function campaignPriceCreate($id = null)
    {
        if ($id == null) {
            return redirect()->route('price.create');
        }
        // --- start update
        $prices = [];
        $prices = Price::with(['individualCampaignWisePrice','regular_campaign_discount','regular_campaign_discount.product_uom'])->findOrFail($id);
        $product = Product::with('Productuom')->findOrFail($prices->product_id);
        return view('price::price-management.price.components.campaign-pricing', compact('product','prices'));
    }

    public function campaignPriceStore(Request $request)
    {
        try {

            if(empty($request->all_camp_discount_type_ag)){
                if(PriceCampaignDiscountDetails::where('price_id',$request->price_id)->exists()){
                    PriceCampaignDiscountDetails::where('price_id',$request->price_id)->delete();
                }
            }
            
            if($request->has('all_camp_discount_type_ag') && $request->has('apply_for_all_campaign_value') && !empty($request->all_camp_discount_type_ag)){
                $regular_campaign_price =[];
                for ($i=0; $i < count($request->all_camp_discount_type_ag) ; $i++) { 
                    $regular_campaign_price[] = [
                        'price_id' => $request->price_id,
                        'product_id' => $request->product_id,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $request->all_camp_uom_value_ag[$i],
                        'discount_type' => $request->all_camp_discount_type_ag[$i],
                        'discount_value' => $request->all_camp_discount_value_ag[$i],
                    ];
                }

                if(PriceCampaignDiscountDetails::where('price_id',$request->price_id)->exists()){
                    PriceCampaignDiscountDetails::where('price_id',$request->price_id)->delete();
                }
                PriceCampaignDiscountDetails::insert($regular_campaign_price);
                
                Price::findOrFail($request->price_id)->update([
                    'ysn_discount_for_all_campaigns' => 1,
                ]);
            }

            if(empty($request->individual_campaign_name)){
                if(CampaignPrice::where('price_id',$request->price_id)->exists()){
                    CampaignPrice::where('price_id',$request->price_id)->delete();
                }    
            }

            if ($request->has('individual_campaign_name') && !empty($request->individual_campaign_name)) {
                $i = 0;
                $campaign_price = [];
                for ($i=0; $i < count($request->indi_camp_uom_value_ag) ; $i++) { 
                    $campaign_price[] = [
                        'price_id' => $request->price_id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $request->indi_camp_uom_value_ag[$i],
                        'campaign_id' => $request->individual_campaign_name[$i],
                        'campaign_discount_type' => $request->individual_campaign_wise_discount_type[$i],
                        'campaign_discount_value' => $request->individual_campaign_wise_discount_value[$i],
                    ];
                }

                if(CampaignPrice::where('price_id',$request->price_id)->exists()){
                    CampaignPrice::where('price_id',$request->price_id)->delete();
                }

                CampaignPrice::insert($campaign_price);
            }

            $temp_price = DB::table('price_temp')->where('user_id',auth()->user()->id)->update([
                'end_point' => 'campign',
            ]);

            return redirect()->route('pricing.storewise_pricing', $request->price_id);
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }
}
