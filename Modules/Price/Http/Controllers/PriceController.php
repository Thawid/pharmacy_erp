<?php

namespace Modules\Price\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Price\Entities\LotPrice;
use Modules\Price\Entities\Price;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductType;
use Modules\Product\Entities\ProductVariation;
use Illuminate\Support\Facades\DB;
use Modules\Price\Entities\UomPrice;
use Modules\Price\Entities\CampaignPrice;
use Modules\Price\Entities\CustomerGroupPrice;
use Modules\Price\Entities\LotDiscount;
use Modules\Price\Entities\PriceCampaignDiscountDetails;
use Modules\Price\Entities\PriceCustomerDiscountDetails;
use Modules\Price\Entities\PriceVatTaxDetails;
use Modules\Price\Entities\StorePrice;
use Modules\Price\Entities\StoreVatTaxPrice;
use Modules\Price\Repositories\PriceRepository;
use Modules\Product\Entities\ProductUom;

class PriceController extends Controller
{
    private $prices;
    function __construct(PriceRepository $prices) {
        $this->prices = $prices;
      }


    public function index()
    {
        $prices = $this->prices->index();
        return view('price::price-management.price.index', compact('prices'));
    }

    // Show product Search table
    public function create()
    {
        $temp_price = DB::table('price_temp')->where('user_id', auth()->user()->id)->first();
        if ($temp_price) {
            if ($temp_price->end_point == 'discount') {
                return redirect()->route('pricing.discount.create', $temp_price->price_id)->withErrors('Please Complete Your task!');
            }
            if ($temp_price->end_point == 'campaign') {
                return redirect()->route('pricing.campaign_pricing', $temp_price->price_id)->withErrors('Please Complete Your task!');
            }

            if ($temp_price->end_point == 'store_wise_price') {
                return redirect()->route('pricing.storewise_pricing', $temp_price->price_id)->withErrors('Please Complete Your task!');
            }
        }
        $product_types = ProductType::where('status', 1)->get();
        return view('price::price-management.price.components.product_search', compact('product_types'));
    }

    // price Product Search
    public function product_search(Request $request)
    {

        return $this->prices->product_search($request);
        
       
    }


    // searching product store
    public function product_store(Request $request)
    {
        $request->validate([
            'product_id' => 'required'
        ], [
            'product_id.required' => 'Please Select A product'
        ]);

        return redirect()->route('pricing.box_packaging.create', $request->product_id);
    }

    // show box and packaging create form
    public function box_packaging_create($id = null)
    {
        $prices = [];
        $temp_price = DB::table('price_temp')->where('user_id', auth()->user()->id)->first();
        if ($temp_price) {
            $id = $temp_price->product_id;
            $prices = Price::with(['uom_price', 'lot_price', 'uom_price.product_uom', 'lot_price.productUom'])->where('product_id', $id)->where('id', $temp_price->price_id)->first();
        }
        if ($id == null) {
            return redirect()->route('price.create');
        }
        $product = Product::with('Productuom')->findOrFail($id);
        return view('price::price-management.price.components.box_packaging', compact('product','prices'));
    }


    // get capacity per uom
    public function capacity_per_uom(Request $request)
    {
        $capacity = ProductUom::select('quantity_per_uom')->where('product_id', $request->product_id)->where('uom_id', $request->uom_val)->first();
        return response()->json($capacity, 200);
    }

    // store box and packaging data
    public function box_packaging_store(Request $request)
    {
        try {

            $base_qty = $request->regular_base_qty;
            $uom_id = $request->regular_uom_id;
            $uom_price = $request->regular_uom_price;
            $capacity = $request->regular_capacity;

            $indi_lot = $request->indi_lot;
            $indi_base_qty = $request->indi_base_qty;
            $indi_uom = $request->indi_uom;
            $indi_price = $request->indi_price;
            $indi_capacity = $request->indi_capacity;
            $apply_for_lot = $request->apply_for_lot;

            $product = Product::findOrFail($request->product_id);
            $price = new Price();
            $price->product_type_id             = $product->product_type_id;
            $price->product_id                  = $product->id;
            $price->regular_bundle_id           = 0;
            $price->product_generic_id          = $product->product_generic_id;
            $price->save();

            $temp_price = DB::table('price_temp')->insert([
                'user_id' => auth()->user()->id,
                'price_id' => $price->id,
                'product_id' => $product->id,
                'end_point' => 'discount',
            ]);

            if (!empty($base_qty) && $request->has('apply_for_lot')) {
                $regular_validation = [
                    'product_id' => 'required',
                    'generic_id' => 'required',
                    'regular_uom_id' => 'required|array',
                    'regular_uom_price' => 'required|array',
                    'regular_capacity' => 'required|array',
                ];


                $request->validate($regular_validation);

                $uom_price = [];
                for ($i = 0; $i < count($base_qty); $i++) {
                    $uom_price[] = [
                        'price_id' => $price->id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $uom_id[$i],
                        'uom_price' => $request->regular_uom_price[$i],
                        'currency' => 'BDT',
                        'capacity' => $capacity[$i],
                    ];
                }


                UomPrice::insert($uom_price);

                Price::findOrFail($price->id)->update([
                    'ysn_equal_for_all_lot' => 1,
                ]);
            }



            if (!empty($indi_lot)) {
                $init_validation = [
                    'product_id' => 'required',
                    'generic_id' => 'required',
                    'indi_uom' => 'required|array',
                    'indi_price' => 'required|array',
                    'indi_capacity' => 'required|array',
                ];
                $request->validate($init_validation);
                $lot_price = [];
                for ($i = 0; $i < count($indi_lot); $i++) {
                    $lot_price[] = [
                        'price_id' => $price->id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $indi_uom[$i],
                        'lot_id' => $indi_lot[$i],
                        'uom_price' => $indi_price[$i],
                        'currency' => 'BDT',
                        'capacity' => $indi_capacity[$i],
                    ];
                }
                LotPrice::insert($lot_price);
            }
            return redirect()->route('pricing.discount.create', $price->id);
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }


    public function box_packaging_update(Request $request, $id)
    {


        try {

            $base_qty = $request->regular_base_qty;
            $uom_id = $request->regular_uom_id;
            $uom_price = $request->regular_uom_price;
            $capacity = $request->regular_capacity;

            $indi_lot = $request->indi_lot;
            $indi_base_qty = $request->indi_base_qty;
            $indi_uom = $request->indi_uom;
            $indi_price = $request->indi_price;
            $indi_capacity = $request->indi_capacity;
            $apply_for_lot = $request->apply_for_lot;


            $product = Product::findOrFail($request->product_id);

            $price = Price::findOrFail($id);
            $price->product_type_id             = $product->product_type_id;
            $price->product_id        = $product->id;
            $price->regular_bundle_id           = 0;
            $price->product_generic_id          = $product->product_generic_id;
            $price->save();

            if(empty($base_qty)){
                if(UomPrice::where('price_id',$price->id)->exists()){
                    UomPrice::where('price_id',$price->id)->delete();
                    Price::findOrFail($price->id)->update([
                        'ysn_equal_for_all_lot' => 0,
                    ]);
                }
            }

            if (!empty($base_qty) && $request->has('apply_for_lot')) {
                UomPrice::where('price_id', $id)->delete();
                $regular_validation = [
                    'product_id' => 'required',
                    'generic_id' => 'required',
                    'regular_uom_id' => 'required|array',
                    'regular_uom_price' => 'required|array',
                    'regular_capacity' => 'required|array',
                ];
                $request->validate($regular_validation);

                $uom_price = [];
                for ($i = 0; $i < count($base_qty); $i++) {
                    $uom_price[] = [
                        'price_id' => $price->id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $uom_id[$i],
                        'uom_price' => $request->regular_uom_price[$i],
                        'currency' => 'BDT',
                        'capacity' => $capacity[$i],
                    ];
                }

                UomPrice::insert($uom_price);

                Price::findOrFail($price->id)->update([
                    'ysn_equal_for_all_lot' => 1,
                ]);
            }
            if(empty($indi_lot)){
                if(LotPrice::where('price_id',$price->id)->exists()){
                    LotPrice::where('price_id',$price->id)->delete();
                }
            }

            if (!empty($indi_lot)) {

                $init_validation = [
                    'product_id' => 'required',
                    'generic_id' => 'required',
                    'indi_uom' => 'required|array',
                    'indi_price' => 'required|array',
                    'indi_capacity' => 'required|array',
                ];

                $request->validate($init_validation);

                $lot_price = [];
                for ($i = 0; $i < count($indi_lot); $i++) {
                    $lot_price[] = [
                        'price_id' => $price->id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $indi_uom[$i],
                        'lot_id' => $indi_lot[$i],
                        'uom_price' => $indi_price[$i],
                        'currency' => 'BDT',
                        'capacity' => $indi_capacity[$i],
                    ];
                }
                LotPrice::where('price_id', $id)->delete();

                LotPrice::insert($lot_price);
            }
            return redirect()->route('pricing.discount.create', $price->id);
        } catch (\Exception $e) {

            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

 

    // show price list

    public function show($id)
    {
        $prices = Price::findOrFail($id);

        $product = Product::select('product_name')->findOrFail($prices->product_id);

        $uom_prices = UomPrice::with(['product', 'product_uom'])->where('price_id', $id)->get();
        $lot_prices = LotPrice::with(['product', 'product_uom'])->where('price_id', $id)->get();

        $price_discount_details = PriceCustomerDiscountDetails::with(['product', 'product_uom'])->where('price_id', $id)->get();
        $customer_group_prices = CustomerGroupPrice::with(['product', 'productUom'])->where('price_id', $id)->get();
        $lot_discounts = LotDiscount::with(['product', 'product_uom'])->where('price_id', $id)->get();

        $campaign_prices =PriceCampaignDiscountDetails::with(['product', 'product_uom'])->where('price_id', $id)->get();
        $campaign_discount = CampaignPrice::where('price_id', $id)->with(['productUom'])->get();

        $store_discounts = StorePrice::with('productUoms')->where('price_id', $id)->get();
        $store_vat_tax = StoreVatTaxPrice::with('productUom')->where('price_id', $id)->get();
        $price_vat_tax_details = PriceVatTaxDetails::with('productUom')->where('price_id', $id)->get();

        return view('price::price-management.price.show', compact('product','uom_prices','campaign_discount', 'store_discounts', 'store_vat_tax','lot_prices','price_discount_details','customer_group_prices','lot_discounts','lot_discounts','campaign_prices','prices','price_vat_tax_details'));
    }

    // edit box and packaging

    public function box_packaging_edit($id)
    {
        $prices = Price::with(['uom_price', 'lot_price', 'uom_price.product_uom', 'lot_price.productUom'])->findOrFail($id);    
        if ($id == null) {
            return redirect()->route('price.create');
        }
        $product = Product::with('Productuom')->findOrFail($prices->product_id);
        return view('price::price-management.price.components.box_packaging', compact('product','id', 'prices'));   

    }
}
