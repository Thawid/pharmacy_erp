<?php
namespace Modules\Price\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Price\Entities\StorePrice;
use Modules\Price\Entities\Price;
use Modules\Price\Entities\UomPrice;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Store\Entities\Store;

class StorePriceController extends Controller
{
    public function storewisePriceCreate($id = null)
    {
        if ($id == null) {
            return redirect()->route('price.create');
        }
        $prices = Price::findOrFail($id);        

        $stores = Store::where('status',1)->get(['id','name']);
        $store_prices = StorePrice::where('price_id',$prices->id)->get();
        $product = Product::with('Productuom')->findOrFail($prices->product_id);
        return view('price::price-management.price.components.storewise-pricing', compact('product', 'stores','prices','store_prices'));
    }

    public function storewisePriceStore(Request $request)
    {
        try {
            $store_ids = $request->store_id;
            $discount_type = $request->store_discount_type;
            $discount_value = $request->store_discount_value;
            $uom_id = $request->uom_id;
            $regular_price = $request->regular_price;
            if(empty($uom_id)){
                if(StorePrice::where('price_id',$request->price_id)->exists()){
                    StorePrice::where('price_id',$request->price_id)->delete();
                }
            }

            
            if($request->has('store_id') && count($store_ids) > 0){
                $store_price = [];
                for ($i=0; $i < count($store_ids) ; $i++) { 
                    $store_price[] = [
                        'store_id' => $store_ids[$i],
                        'price_id' => $request->price_id,
                        'product_id' => $request->product_id,
                        'regular_bundle_id' => 0,
                        'generic_id' => $request->generic_id,
                        'uom_id' => $uom_id[$i],
                        'regular_price' => $regular_price[$i],
                        'store_discount_type' => $discount_type[$i],
                        'store_discount_value' => $discount_value[$i],
    
                    ];
                }

                if(StorePrice::where('price_id',$request->price_id)->exists()){
                    StorePrice::where('price_id',$request->price_id)->delete();
                }
                StorePrice::insert($store_price);
            }

            $temp_price = DB::table('price_temp')->where('user_id',auth()->user()->id)->update([
                'end_point' => 'store_wise_price',
            ]);
            
            return redirect()->route('pricing.storewise_vat_tax', $request->price_id);

        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }

    }
    public function storewisePriceUpdate(Request $request, $id)
    {

        try {
            $store_ids = $request->store_id;
            $discount_type = $request->store_discount_type;
            $discount_value = $request->store_discount_value;
            $uom_id = $request->uom_id;
            $regular_price = $request->regular_price;

            //$product = ProductVariation::with('product')->findOrFail($request->product_variation_id);
            $temp_price = DB::table('price_temp')->insert([
                'user_id' => auth()->user()->id,
                'price_id' => $request->price_id,
                'product_id' =>  $request->product_variation_id,
                'end_point' => 'storeprice',
            ]);

            $previous_data_remove = StorePrice::where('price_id',$id)->delete();

            $i = 0;
            $store_price = [];
            foreach ($store_ids as $store_id) {
                $store_price[] = [
                    'store_id' => $store_id,
                    'price_id' => $request->price_id,
                    'product_variation_id' => $request->product_variation_id,
                    'regular_bundle_id' => 0,
                    'generic_id' => $request->generic_id,
                    'uom_id' => $uom_id[$i],
                    'regular_price' => $regular_price[$i],
                    'store_discount_type' => $discount_type[$i],
                    'store_discount_value' => $discount_value[$i],

                ];
                $i++;
            }
            StorePrice::insert($store_price);

            return redirect()->route('pricing.storewise_vat_tax', $request->price_id);

        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }

    }
}
