<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\UOM;
use Modules\Store\Entities\Store;

class StoreVatTaxPrice extends Model
{
    protected $guarded = [];

    public function price()
    {
        return $this->belongsTo(Price::class, 'price_id', 'id');
    }

    public function productVariation()
    {
        return $this->belongsTo(ProductVariation::class, 'product_variation_id', 'id');
    }

    public function regularProductBundle()
    {
        return $this->belongsTo(RegularBundle::class, 'regular_bundle_id', 'id');
    }

    public function productGeneric()
    {
        return $this->belongsTo(ProductGeneric::class, 'product_generic_id', 'id');
    }

    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id');
    }

    public function storeName()
    {
        return $this->hasOne(Store::class, 'id', 'store_id');
    }
}
