<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;

class PriceCustomerDiscountDetails extends Model
{
    use HasFactory;

    protected $fillable = [];

    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }


    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'uom_id','id')->withDefault();
    }

    
    protected static function newFactory()
    {
        return \Modules\Price\Database\factories\PriceCustomerDiscountDetailsFactory::new();
    }
}
