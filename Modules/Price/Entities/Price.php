<?php

namespace Modules\Price\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\ProductVariation;
use Modules\Price\Entities\PriceVatTaxDetails;
use Modules\Product\Entities\Product;

class Price extends Model
{
    protected $guarded = [];
    protected $table = 'prices';

    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }

    public function product_variation()
    {
        return $this->belongsTo(ProductVariation::class)->withDefault();
    }
    public function uom_price()
    {
        return $this->hasMany(UomPrice::class, 'price_id', 'id')->orderByDesc('id');
    }

    public function lot_price()
    {
        return $this->hasMany(LotPrice::class, 'price_id', 'id')->orderByDesc('id');
    }

    public function uom_price_single()
    {
        return $this->belongsTo(UomPrice::class, 'id', 'price_id');
    }

    public function customar_group_prices()
    {
        return $this->hasMany(CustomerGroupPrice::class, 'price_id', 'id')->orderByDesc('id');
    }

    public function lot_discount_prices()
    {
        return $this->hasMany(LotDiscountPrice::class, 'price_id', 'id')->orderByDesc('id');
    }

    public function individualCampaignWisePrice()
    {
        return $this->hasMany(CampaignPrice::class, 'price_id', 'id')->orderByDesc('id')->with('productUom');
    }

    public function individualStoreWiseVatnTax()
    {
        return $this->hasMany(StoreVatTaxPrice::class, 'price_id', 'id')->with(['productUom', 'storeName'])->orderByDesc('id');
    }

    public function allStoreVatnTax()
    {
        return $this->hasMany(PriceVatTaxDetails::class, 'price_id', 'id')->with(['productUom'])->orderByDesc('id');
    }

    public function all_customer_group_price()
    {
        return $this->hasMany(PriceCustomerDiscountDetails::class,'price_id','id')->orderByDesc('id');
    }

    public function regular_campaign_discount()
    {
        return $this->hasMany(PriceCampaignDiscountDetails::class,'price_id','id')->orderByDesc('id');
    }

    public function getStorePriceAttribute($value)
    {
        $date =date_create($this->created_at);
        return date_format($date,"d/m/Y");
    }
}
