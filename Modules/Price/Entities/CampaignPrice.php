<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\UOM;

class CampaignPrice extends Model
{
    protected $guarded = [];
    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id', 'id');
    }
}
