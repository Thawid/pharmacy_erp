<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\UOM;
use Modules\Product\Entities\ProductVariation;
class UomPrice extends Model
{
    protected $guarded = [];

    public function uom()
    {
        return $this->belongsTo(UOM::class,);
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }

    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'uom_id','id')->withDefault();
    }

    public function store_product(){

        return $this->belongsTo(StoreAvailableProductDetails::class,'product_id','product_id')->withDefault();
    }

    public function price_customer_discount_details(){

        return $this->belongsTo(PriceCustomerDiscountDetails::class,'price_id','price_id')->latest()->withDefault();
    }




}
