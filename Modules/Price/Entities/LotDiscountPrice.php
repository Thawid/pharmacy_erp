<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\UOM;

class LotDiscountPrice extends Model
{
    protected $guarded = [];

    protected $table ='lot_discounts';
    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id', 'id');
    }
}
