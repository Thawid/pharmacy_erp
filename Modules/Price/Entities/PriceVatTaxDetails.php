<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\UOM;

class PriceVatTaxDetails extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Price\Database\factories\PriceVatTaxDetailsFactory::new();
    }

    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id');
    }
}
