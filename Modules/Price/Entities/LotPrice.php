<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;

class LotPrice extends Model
{
    protected $guarded = [];
    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }

    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'uom_id','id')->withDefault();
    }


    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id', 'id');
    }

    public function lot()
    {
        // 
    }
}
