<?php

namespace Modules\Price\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;

class CustomerGroupPrice extends Model
{
    protected $guarded = [];

    public function price()
    {
        return $this->belongsTo(Price::class, 'price_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }

    public function regularProductBundle()
    {
        return $this->belongsTo(RegularBundle::class, 'regular_bundle_id', 'id');
    }

    public function productGeneric()
    {
        return $this->belongsTo(ProductGeneric::class, 'product_generic_id', 'id');
    }

    public function customerGroup()
    {
        return $this->belongsTo(CustomerGroup::class, 'customer_group_id', 'id');
    }

    public function productUom()
    {
        return $this->belongsTo(UOM::class, 'uom_id', 'id');
    }
}
