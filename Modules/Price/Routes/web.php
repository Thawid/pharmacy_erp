<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth','price'])->prefix('product')->group(function () {
    // Route::middleware(['auth', 'price'])->prefix('price')->group(function () {
    // Route::get('/', 'PriceController@index');
    // Route::resource('/price', 'PriceController');

    // main price controller
    // Route::resource('/', 'PriceController');
    Route::resource('/price', 'PriceController');
    Route::resource('/customer-group', 'CustomerGroupController');

    Route::get('product-search', 'PriceController@product_search')->name('product.search');
    Route::post('product-store', 'PriceController@product_store')->name('product.store');

    Route::get('box-packaging/create/{id?}', 'PriceController@box_packaging_create')->name('pricing.box_packaging.create');
    Route::post('box-packaging/store/{id?}', 'PriceController@box_packaging_store')->name('pricing.box_packaging.store');
    Route::get('capacity-per-uom', 'PriceController@capacity_per_uom')->name('pricing.capacity_per_uom');

    Route::get('price-discount/create/{id?}', 'PriceDiscountController@create')->name('pricing.discount.create');
    Route::post('price-discount/store/{id?}', 'PriceDiscountController@store')->name('pricing.discount.store');
    Route::post('price-discount/update/{id}', 'PriceDiscountController@update')->name('pricing.discount.update');

    Route::get('campaign-price/{id?}', 'CampaignPriceController@campaignPriceCreate')->name('pricing.campaign_pricing');
    Route::post('campaign-price/store/{id?}', 'CampaignPriceController@campaignPriceStore')->name('pricing.campaign_pricing.store');

    Route::get('storewise-price/{id?}', 'StorePriceController@storewisePriceCreate')->name('pricing.storewise_pricing');
    Route::post('storewise-price/store/{id?}', 'StorePriceController@storewisePriceStore')->name('pricing.storewise_pricing.store');
    Route::post('storewise-price/update/{id?}', 'StorePriceController@storewisePriceUpdate')->name('pricing.storewise_pricing.update');

    Route::get('storewise-vat-tax/{id?}', 'StoreVatTaxPriceController@store_vat_tax_create')->name('pricing.storewise_vat_tax');
    Route::post('vat_tax/store', 'StoreVatTaxPriceController@vat_tax_store')->name('pricing.vat_tax.store');

    Route::get('active-price/{id?}', 'ActivePriceController@activePrice')->name('pricing.active.price');

    Route::post('box-packaging/update/{id?}', 'PriceController@box_packaging_update')->name('pricing.box_packaging.update');
    Route::get('box-packaging/edit/{id}', 'PriceController@box_packaging_edit')->name('pricing.box_packaging.edit');
});
