<?php
namespace Modules\POS\Repositories;
use Illuminate\Http\Request;
interface PosInterface
{
  public function create();
  public function point_of_sale_store(Request $request);
  public function product_search(Request $request);
  public function pos_product_add(Request $request);
  public function get_uom_price(Request $request);
  public function get_discount_price(Request $request);
  public function get_customer_info(Request $request);
  public function available_quentity(Request $request);
}
