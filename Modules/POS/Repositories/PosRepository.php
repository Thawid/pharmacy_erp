<?php

namespace Modules\POS\Repositories;

use DB;
use Modules\POS\Entities\PointOfSale;
use Modules\POS\Entities\PointOfSaleDetail;
use Modules\Accounts\Entities\StoreInvoice;
use Modules\Store\Entities\StoreUser;
use Modules\POS\Entities\Customer;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\POS\Repositories\PosInterface;
use Modules\Price\Entities\CustomerGroup;
use Modules\Price\Entities\CustomerGroupPrice;
use Modules\Price\Entities\PriceCustomerDiscountDetails;
use Modules\Price\Entities\PriceVatTaxDetails;
use Modules\Price\Entities\StorePrice;
use Modules\Price\Entities\StoreVatTaxPrice;
use Modules\Price\Entities\UomPrice;
use Modules\Product\Entities\ProductUom;
use Carbon\Carbon;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\Store\Entities\Store;

class PosRepository implements PosInterface
{
    /**
     * Show Point of sale page show
     *
     * @return Response
     */

    public function create()
    {
        $years = date('y');
        $month = date('m');
        $day = date('d');

        $last_invoice = DB::table('point_of_sales')->select('invoice_no')->orderBy('id', 'DESC')->first();
        if ($last_invoice) {
            $exist_invoice = $last_invoice->invoice_no;
            $exist_invoice = explode("-", $exist_invoice);
            $exist_invoice = $exist_invoice[2];
            $new_invoice = $exist_invoice + 1;
            return $invoice_no = $day . $month . $years . '-SI-' . $new_invoice;
        } else {
            return $invoice_no = $day . $month . $years . '-SI-' . '1000';
        }
    }


    /**
     * store point of sale
     *@param Request
     * @return Response
    */

    public function point_of_sale_store($request)
    {

        $request->validate(
            [
                'invoice_no' => 'required',
                'product_id' => 'required|array',
                'uom_id' => 'required|array',
                'order_qty' => 'required|array',
                'subtotal' => 'required|array',
            ],
            [
                'product_id.required' => 'Product Field is required',
                'order_qty.required' => 'Order Quantity Field is required',
                'uom_id.required' => 'Uom Field Must not be empty',
            ]
        );

        DB::beginTransaction();

        try {
            $store_user = StoreUser::with('store')->select('store_id', 'user_id')->where('user_id', auth()->user()->id)->first();
            $customar = Customer::where('customer_name', $request->name)->where('mobile_no', $request->mobile_no)->first();
            if (!$customar && !empty($request->name)) {
                $request->validate([
                    'name' => 'required',
                    'mobile_no' => 'required|regex:/(01)[0-9]{9}/',
                ]);
                $customar = new Customer();
                $customar->customer_name = $request->name;
                $customar->mobile_no = $request->mobile_no;
                $customar->membership_card = $request->membership_card;
                $customar->total_expense = $request->grand_total;
                $customar->total_order = 1;
                $customar->save();
            } else {

                Customer::where('customer_name', $request->name)->where('mobile_no', $request->mobile_no)->increment('total_expense', $request->grand_total);
                Customer::where('customer_name', $request->name)->where('mobile_no', $request->mobile_no)->increment('total_order', 1);
            }

            $point_of_sales = new PointOfSale();
            $point_of_sales->invoice_no = $request->invoice_no;
            $point_of_sales->customer_id = isset($customar->id) ? $customar->id : 0;
            $point_of_sales->counter_no = rand(1, 5);
            $point_of_sales->employee_id = auth()->user()->id;
            $point_of_sales->pos_machine_id = rand(1111, 9999);
            $point_of_sales->store_id = $store_user->store_id;
            $point_of_sales->hub_id = $store_user->store->hub_id;
            $point_of_sales->sale_discount = $request->sale_discount;
            $point_of_sales->total_discount = $request->total_discount;
            $point_of_sales->total_tax = $request->total_tax;
            $point_of_sales->other_cost = $request->other_cost;
            $point_of_sales->grand_total = $request->grand_total;
            $point_of_sales->paid_amount = $request->paid_amount;
            $point_of_sales->return_amount = $request->return_amount;
            $point_of_sales->payment_method = 'cash';
            $point_of_sales->save();

            $product_id = $request->product_id;
            $uom_id = $request->uom_id;
            $order_qty = $request->order_qty;
            $per_pcs_price = $request->per_pcs_price;
            $subtotal = $request->subtotal;
            $base_no = $request->base_no;
          
            
            $discount = json_decode($request->discount_arr);
            $vat = json_decode($request->vat_arr);

            $pos_item = [];
            for ($i = 0; $i < count($product_id); $i++) {

                $pos_item[] = [
                    'pos_id' => $point_of_sales->id,
                    'product_id' => $product_id[$i],
                    'uom_id' => $uom_id[$i],
                    'sku_no' => rand(1111, 9999),
                    'order_qty' => $order_qty[$i],
                    'per_unit_price' => $per_pcs_price[$i],
                    'discount' => $discount[$i],
                    'batch_no' => $base_no[$i],
                    'vat_amount' => $vat[$i],
                    'sub_total' => $subtotal[$i],
                    'created_at' => Carbon::now(),
                ];

                $store_available_product = StoreAvailableProduct::where('store_id', $store_user->store_id)->first();

                if ($store_available_product) {
                    $store_product = StoreAvailableProductDetails::select('available_quantity')->where('store_available_product_id', $store_available_product->id)->where('product_id', $product_id[$i])->first();
                    $available_quantity = $store_product->available_quantity;

                    $product_uoms = ProductUom::select('quantity_per_uom')->where('product_id', $product_id[$i])->where('uom_id', $uom_id[$i])->first();

                    if ($available_quantity > 0) {
                        $product_uoms = $product_uoms->quantity_per_uom;
                        $purchase_uom_qty = $product_uoms * $order_qty[$i];
                        StoreAvailableProductDetails::select('available_quantity')->where('store_available_product_id', $store_available_product->id)->where('product_id', $product_id[$i])->decrement('available_quantity', $purchase_uom_qty);

                        $store_stock =StoreStockEntryDetails::where('batch_no',$base_no[$i])->first();
                        
                        $store_stock->out_quantity += $purchase_uom_qty;
                        $store_stock->out_date = Carbon::now();

                        if($store_stock->received_quantity == $store_stock->out_quantity || $store_stock->received_quantity < $store_stock->out_quantity ){
                                $store_stock->stock_status = 'empty';
                            }
                        $store_stock->save();
                    }
                }
            }

            PointOfSaleDetail::insert($pos_item);

            if ($store_user) {
                StoreInvoice::create([
                    'store_id' => $store_user->store_id,
                    'hub_id' => $store_user->store->hub_id,
                    'invoice_no' => $request->invoice_no,
                    'status' => 'pending',
                    'amount' => $request->grand_total,
                    'created_at' => Carbon::now(),
                ]);
            }

            $store =Store::where('id',$store_user->store_id)->increment('total_balance',$request->grand_total);
            DB::commit();
            return $point_of_sales->id;
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error below" . $e->getMessage());
        }
    }



    /**
     * POS product search 
     * @param Request
     * @return Response
     */


    public function product_search($request)
    {
        $searchstr = $request->product_search;

        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        $productsitem =DB::table('store_stock_entry_details')->where('store_id',$store->store_id)->join('products', 'store_stock_entry_details.product_id', '=', 'products.id')->where('store_stock_entry_details.batch_no', 'LIKE', '%' . $searchstr . "%")->orwhere('products.product_name', 'LIKE', '%' . $searchstr . "%")->where('stock_status',NULL)->get();
        return $productsitem;
    }


    /**
     * product add on list
     * @param Request
     * @return Response
     */

    public function pos_product_add($request)
    {

        $store_stock_details = StoreStockEntryDetails::where('batch_no',$request->product_id)->first();

        $product_id = $store_stock_details->product_id;

        $uoms = UomPrice::with(['uom' => function ($query) {
            $query->latest();
        }])->where('product_id', $product_id)->get()->unique('uom_id');

        $product = UomPrice::with(['product', 'uom', 'store_product'])->where('product_id', $product_id)->orderBy('id', 'DESC')->first();

        if (!$product) {
            return response()->json('Product Price Not Set!', 404);
        }

        $product_uoms = ProductUom::select('quantity_per_uom')->where('product_id', $product_id)->where('uom_id', $product->uom_id)->first();

        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        $store_available_product = StoreAvailableProduct::where('store_id', $store->store_id)->first();

        $store_available_product = StoreAvailableProductDetails::select('available_quantity')->where('store_available_product_id', $store_available_product->id)->where('product_id', $product_id)->first();

        $product_quanitity_uom = $product_uoms->quantity_per_uom;
        $store_qty_uom = $store_available_product->available_quantity;

        if ($product_quanitity_uom > 0 && $store_qty_uom > 0) {
            $available_qty = $store_qty_uom / $product_quanitity_uom;
        } else {
            $available_qty = 0;
        }

        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        if ($store) {
            if (StorePrice::where('price_id', $product->price_id)->where('product_id', $product_id)->where('store_id', $store->store_id)->where('uom_id', $product->uom_id)->orderBy('id', 'desc')->exists()) {
                $store_price = StorePrice::where('price_id', $product->price_id)->where('product_id', $product_id)->where('store_id', $store->store_id)->where('uom_id', $product->uom_id)->orderBy('id', 'desc')->first();
                $store_price = isset($store_price->regular_price) ? $store_price->regular_price : 0;
            } else {
                $store_price = 0;
            }
        } else {
            $store_price = 0;
        }

        $uom_price = isset($product->uom_price) ? $product->uom_price : 0;
        $price = $uom_price + $store_price;

        $uom_item = [];

        foreach ($uoms as $row) {
            array_push($uom_item, $row);
        }

        return response()->json([
            'uoms' => $uom_item,
            'product' => $product,
            'price' => $price,
            'available_qty' => intval($available_qty),
            'base_no'=>$request->product_id,
        ]);
    }

    /**
     * get uom price
     * @param Request
     * @return Response
     */

    public function get_uom_price($request)
    {

        $prices = UomPrice::where('product_id', $request->product_id)->where('uom_id', $request->uom_id)->select('uom_price')->orderBy('id', 'DESC')->first();


        $product_uoms = ProductUom::select('quantity_per_uom')->where('product_id', $request->product_id)->where('uom_id', $request->uom_id)->first();

        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        $store_available_product = StoreAvailableProduct::where('store_id', $store->store_id)->first();

        $store_available_product = StoreAvailableProductDetails::select('available_quantity')->where('store_available_product_id', $store_available_product->id)->where('product_id', $request->product_id)->first();

        $product_quanitity_uom = $product_uoms->quantity_per_uom;
        $store_qty_uom = $store_available_product->available_quantity;

        if ($product_quanitity_uom > 0 && $store_qty_uom > 0) {
            $available_qty = $store_qty_uom / $product_quanitity_uom;
        } else {
            $available_qty = 0;
        }

        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        if ($store) {
            if (StorePrice::where('product_id', $request->product_id)->where('store_id', $store->store_id)->where('uom_id', $request->uom_id)->orderBy('id', 'desc')->exists()) {
                $store_price = StorePrice::where('product_id', $request->product_id)->where('store_id', $store->store_id)->where('uom_id', $request->uom_id)->select('regular_price')->orderBy('id', 'DESC')->first();
                $store_price = isset($store_price->regular_price) ? $store_price->regular_price : 0;
            } else {
                $store_price = 0;
            }
        } else {
            $store_price = 0;
        }

        $uom_price = isset($prices->uom_price) ? $prices->uom_price : 0;
        $price = $uom_price + $store_price;

        return response()->json([
            'prices' => $price,
            'available_qty' => $available_qty,
        ]);
    }

    /**
     * Get Discount Price
     * @param Request
     * @return Response
     */

    public function get_discount_price($request)
    {
        $product = $request->product;
        $uom = $request->uom;
        $order_qty = $request->order_qty;

        if (!empty($product) && !empty($order_qty)) {
            $discount = 0;
            $total_vat_tax = 0;
            $indi_discount = [];
            $indi_vat      = [];

            for ($i = 0; $i < count($product); $i++) {


                $product_uom_prices = UomPrice::where('product_id', $product[$i])->where('uom_id', $uom[$i])->select('uom_price', 'price_id')->orderBy('id', 'desc')->first();

                // customer group discount
                if ($request->has('phone_no') && $request->filled('phone_no')) {

                    $customers = Customer::where('mobile_no', $request->phone_no)->select('total_expense')->first();

                    // dd($customers);

                    if ($customers) {

                        $customer_group = CustomerGroup::where('min_expense', '<=', $customers->total_expense)->where('max_expense', '>=', $customers->total_expense)->first();

                        if (!$customer_group && !empty($customers->total_order)) {
                            $customer_group = CustomerGroup::where('min_order', '<=', $customers->total_order)->where('max_order', '>=', $customers->total_order)->first();
                        }

                        if (!$customer_group && !empty($customers->created_at)) {
                            $customer_group = CustomerGroup::where('reg_from', '<=', $customers->created_at)->where('reg_to', '>=', $customers->created_at)->first();
                        }

                        // dd($customer_group);

                        if ($customer_group) {


                            // customer group discount
                            $customer_group_discount = CustomerGroupPrice::where('price_id', $product_uom_prices->price_id)->where('product_id', $product[$i])->where('customer_group_id', $customer_group->id)->where('uom_id', $uom[$i])->orderBy('id', 'desc')->select('customer_discount_value')->first();
                        } else {

                            // price customer discount details
                            $price_customer_discount_details = PriceCustomerDiscountDetails::where('price_id', $product_uom_prices->price_id)->where('product_id', $product[$i])->where('uom_id', $uom[$i])->select('discount_value')->first();
                        }
                    } else {

                        // price customer discount details
                        $price_customer_discount_details = PriceCustomerDiscountDetails::where('price_id', $product_uom_prices->price_id)->where('product_id', $product[$i])->where('uom_id', $uom[$i])->select('discount_value')->first();
                    }
                } else {

                    // price customer discount details
                    $price_customer_discount_details = PriceCustomerDiscountDetails::where('price_id', $product_uom_prices->price_id)->where('product_id', $product[$i])->where('uom_id', $uom[$i])->select('discount_value')->first();
                }

                // if store is exist
                $store = StoreUser::where('user_id', auth()->user()->id)->first();

                if ($store) {

                    if (StorePrice::where('price_id', $product_uom_prices->price_id)->where('store_id', $store->store_id)->where('product_id', $product[$i])->where('uom_id', $uom[$i])->orderBy('id', 'desc')->exists()) {
                        // get store price
                        $store_prices = StorePrice::where('price_id', $product_uom_prices->price_id)->where('product_id', $product[$i])->where('uom_id', $uom[$i])->select('regular_price', 'store_discount_value')->orderBy('id', 'desc')->first();
                    }

                    // store vat tax
                    $vat_tax = StoreVatTaxPrice::where('price_id', $product_uom_prices->price_id)->where('product_variation_id', $product[$i])->where('uom_id', $uom[$i])->orderBy('id', 'desc')->first();
                    $vat_tax = isset($vat_tax->store_vat_tax_value) ? $vat_tax->store_vat_tax_value : 0;

                    if (!$vat_tax) {
                        $vat_tax = PriceVatTaxDetails::where('price_id', $product_uom_prices->price_id)->where('product_variation_id', $product[$i])->where('uom_id', $uom[$i])->orderBy('id', 'desc')->first();
                        $vat_tax = isset($vat_tax->vat_tax_value) ? $vat_tax->vat_tax_value : 0;
                    }
                } else {
                    $vat_tax = PriceVatTaxDetails::where('price_id', $product_uom_prices->price_id)->where('product_variation_id', $product[$i])->where('uom_id', $uom[$i])->orderBy('id', 'desc')->first();
                    $vat_tax = isset($vat_tax->vat_tax_value) ? $vat_tax->vat_tax_value : 0;
                }


                $product_uom_prices = isset($product_uom_prices->uom_price) ? $product_uom_prices->uom_price : 0;
                $store_reqular_prices = isset($store_prices->regular_price) ? $store_prices->regular_price : 0;

                $price_customer_discount_details = isset($price_customer_discount_details->discount_value) ? $price_customer_discount_details->discount_value : 0;
                $store_discount_price = isset($store_prices->store_discount_value) ? $store_prices->store_discount_value : 0;
                $vat_tax              = isset($vat_tax) ? $vat_tax : 0;

                $customer_group_discount = isset($customer_group_discount->customer_discount_value) ? $customer_group_discount->customer_discount_value : 0;

                // calculation discount
                $product_price = ($product_uom_prices + $store_reqular_prices) * $order_qty[$i];
                $customer_discount = ($product_price * $price_customer_discount_details) / 100;
                $customer_group_discount = ($product_price * $customer_group_discount) / 100;

                $product_price_after_discount = $product_price - $customer_discount;

                // if customer in group
                $product_price_after_discount = $product_price_after_discount - $customer_group_discount;

                // calculation store discount
                $discount_amount = ($product_price_after_discount * $store_discount_price) / 100;

                // calculate total discount

                $discount_amount = $discount_amount + $customer_discount + $customer_group_discount;

                $discount = $discount + $discount_amount;
                array_push($indi_discount, $discount_amount);

                //    set vat and tax
                $vat_tax_amount = ($product_price * $vat_tax) / 100;
                array_push($indi_vat, $vat_tax_amount);
                $total_vat_tax = $total_vat_tax + $vat_tax_amount;
            }

            return response()->json([
                'discount' => $discount,
                'vat_tax' => $total_vat_tax,
                'indi_discount' => $indi_discount,
                'indi_vat' => $indi_vat,
            ]);
        }
    }



    /**
     * Get Customer Information
     * @param Request
     * @return Response
     */

    public function get_customer_info($request)
    {
        $phone_no = $request->phone_no;
        $customers = Customer::where('mobile_no', $phone_no)->select('customer_name')->first();
        return $customers;
    }

     /**
     * Deduct from Available quentity
     * @param Request
     * @return Response
     */


    public function available_quentity($request)
    {
        $product_uoms = ProductUom::select('quantity_per_uom')->where('product_id', $request->product_id)->where('uom_id', $request->uom_id)->first();

        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        $store_available_product = StoreAvailableProduct::where('store_id', $store->store_id)->first();

       $store_available_product = StoreAvailableProductDetails::select('available_quantity')->where('store_available_product_id', $store_available_product->id)->where('product_id', $request->product_id)->first();
       
        $product_quanitity_uom = $product_uoms->quantity_per_uom;
        $store_qty_uom = $store_available_product->available_quantity;

        if ($product_quanitity_uom > 0 && $store_qty_uom > 0) {
            $available_qty = $store_qty_uom / $product_quanitity_uom - $request->order_qyt;

        } else {
            $available_qty = 0;
        }

        return $available_qty;
    }
}
