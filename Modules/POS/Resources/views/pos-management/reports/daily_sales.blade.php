@extends('dboard.index')
@section('title', 'Daily Sales Report')
@push('styles')
<style>
    .pending-store-deposite-receive {
        border-radius: 8px;
    }

    div.dataTables_wrapper div.dataTables_filter label {
        font-weight: normal;
        white-space: nowrap;
        text-align: left;
        font-size: 0;
    }

    div.dataTables_wrapper div.dataTables_length label {
        font-weight: normal;
        text-align: left;
        white-space: nowrap;
        font-size: 0;
    }


    div.dataTables_wrapper div.dataTables_info {
        padding-top: 0.85em;
        white-space: nowrap;
        margin-left: 10px;
    }

    table.dataTable td:last-child i {
        font-size: 16px;
        margin: -1px;
    }

    ul.title-heading {
        display: flex;
        padding: 0;
        margin: 0;
    }

    ul.title-heading li {
        list-style: none;
        margin-right: 15px;
    }

    ul.title-heading li a {
        text-decoration: none;
        position: relative;
    }

    ul.title-heading li a:hover {
        text-decoration: none;
    }

    ul.title-heading li a::before {
        position: absolute;
        content: "";
        height: 2px;
        width: 0;
        background: #0088FF;
        bottom: -5px;
        left: 0;
        right: 0;
        margin: auto;
        transition: .3s;
    }

    ul.title-heading li:hover a::before {
        width: 100%;
    }

    ul.title-heading li.active a {
        color: #0088FF;
        transition: .3s;
    }

    ul.title-heading li.active a::before {
        width: 100%;
    }

    /* .dataTable  thead {
            background: #294A65;
            color: white;
        }
        .dataTable  thead th{
            border: 1px solid transparent;
        } */
</style>
@endpush
@section('dboard_content')


<div class="row">
    <div class="col-md-12">
        <div class="tile pending-store-deposite-receive">
            <div class="tile-body">
                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Daily Sales Reports ({{$date}})</h2>
                    </div><!-- end .col-md-6 -->

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{ route('pos.index') }}">Back</a>
                    </div><!-- end .col-md-6 -->
                </div><!-- end .row -->
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <!-- <button type="button" class="btn btn-sm shadow-sm p-3 m-1 bg-white rounded mx-auto float-right pull-right" onclick="Export()">PDF</button> -->
                        <!-- <button type="button" class="btn btn-sm shadow-sm p-3 m-1 bg-white rounded mx-auto float-right pull-right" onclick="Export()">PDF</button> -->

                        <div class="reset_action mt-0">
                            <a  class="btn index-btn mr-2" type="button" id="btnExport" onclick="Export()">PDF <i class="pl-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
                            <a  class="btn index-btn" type="button" id="print_btn" onclick="printDiv('purchaseReport')">Print <i class="pl-2 fa fa-print"></i>
                            </a>
                        </div>
                    </div>
                    
                </div>
                <hr>

                <div class="row">
                    <!-- data table -->
                    <div class="col-md-12" id="purchaseReport">
                        <table class="table table-striped table-bordered table-hover" id="deposit-receive-table">
                            <thead>
                                <tr>
                                    <th width="5%" style="border-right-width: 4px;">#</th>
                                    <th width="" style="border-right-width: 4px;">Product Details</th>
                                    <th width="" style="border-right-width: 4px;">Generic</th>
                                    <th width="" style="border-right-width: 4px;">Manufacturer</th>
                                    <th width="">Sold Qty</th>
                                    <th width="">UOM</th>
                                    <th width="">Discount</th>
                                    <th width="">Vat & Tax</th>
                                    <th width="15%">Total Amount</th>
                                    <!-- <th width="15%">Action</th> -->
                                </tr>
                            </thead>

                            <tbody>
                                @if(!empty($sales_reports))
                                @foreach($sales_reports as $row)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>
                                        {{$row['product']}}
                                    </td>
                                    <td class="align-item-center">
                                        {{$row['uom']}}
                                    </td>

                                    <td>{{$row['manufacturer']}}</td>
                                    <td>{{$row['orderqty']}}</td>
                                    <td>{{$row['uom']}}</td>
                                    <td>{{$row['discount']}}</td>
                                    <td>{{$row['vat_amount']}}</td>
                                    <td>{{(float)$row['total'] - $row['discount'] +$row['vat_amount']}}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr class="text-center">
                                    <th colspan="8">No Data Found!</th>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div><!--  end .table -->
                </div><!-- end .row -->
            </div><!-- end .tile-body -->
        </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
</div><!-- end .row -->
</form>


@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{asset('js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/html2canvas.min.js')}}"></script>

<script>
    function Export() {
        html2canvas(document.getElementById('purchaseReport'), {
            onrendered: function(canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Purchase Report.pdf");
            }
        });
    }


    function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
</script>
<script type="text/javascript">
    $('#deposit-receive-table').DataTable();
</script>

<script>
    $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
</script>

<script>
    $(document).on('click', 'ul li', function() {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>
@endpush