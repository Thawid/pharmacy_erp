@extends('dboard.index')
@section('title', 'Sales Summary')
@push('styles')
<style>
    .pending-store-deposite-receive {
        border-radius: 8px;
    }

    div.dataTables_wrapper div.dataTables_filter label {
        font-weight: normal;
        white-space: nowrap;
        text-align: left;
        font-size: 0;
    }

    div.dataTables_wrapper div.dataTables_length label {
        font-weight: normal;
        text-align: left;
        white-space: nowrap;
        font-size: 0;
    }


    div.dataTables_wrapper div.dataTables_info {
        padding-top: 0.85em;
        white-space: nowrap;
        margin-left: 10px;
    }

    table.dataTable td:last-child i {
        font-size: 16px;
        margin: -1px;
    }

    ul.title-heading {
        display: flex;
        padding: 0;
        margin: 0;
    }

    ul.title-heading li {
        list-style: none;
        margin-right: 15px;
    }

    ul.title-heading li a {
        text-decoration: none;
        position: relative;
    }

    ul.title-heading li a:hover {
        text-decoration: none;
    }

    ul.title-heading li a::before {
        position: absolute;
        content: "";
        height: 2px;
        width: 0;
        background: #0088FF;
        bottom: -5px;
        left: 0;
        right: 0;
        margin: auto;
        transition: .3s;
    }

    ul.title-heading li:hover a::before {
        width: 100%;
    }

    ul.title-heading li.active a {
        color: #0088FF;
        transition: .3s;
    }

    ul.title-heading li.active a::before {
        width: 100%;
    }

    /* .dataTable  thead {
            background: #294A65;
            color: white;
        }
        .dataTable  thead th{
            border: 1px solid transparent;
        } */
</style>

<style>
        table {
            text-align: center;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .th-table-header, .th-table-info, .th-table-body {
            margin-bottom: 20px;
        }

        .th-table-header table tr td h1 {
            margin: 0;
            font-size: 35px;
        }

        .th-table-header table tr td h2 {
            margin: 0;
            font-size: 18px;
        }

        .th-table-header table tr td h3 {
            padding: 0;
            margin: 0;
            font-size: 24px;
        }

        .th-table-info p {
            margin: 0;
        }

        .th-table-info table tr td p {
            text-align: left;
        }
        .th-table-info table {
            text-align: center !important;
        }

        .th-table-header table tr td p {
            padding: 0;
            margin: 0;
            font-size: 16px;
        }

    </style>
@endpush
@section('dboard_content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Sales Summary</h2>
                    </div><!-- end .col-md-6-->

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{ route('pos.index') }}">Back</a>
                    </div><!-- end .col-md-6 -->
                </div><!-- end .row -->
                <hr>

                <!-- filter-area -->
                <form action="{{route('pos.product.filter')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Product type</label>
                                    <select name="product_type" id="product_filter" class="form-control product_filter">
                                        <option selected disabled>--Select Product Type---</option>
                                        @foreach($product_types as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Product Category</label>
                                    <select name="product_category" id="" class="form-control product_filter">
                                        <option selected disabled>---Select Product Category ---</option>
                                        @foreach($product_categores as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Select Product</label>
                                    <select name="product" class="form-control product_search">
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">From Date </label>
                                    <input type="date" name="from_date" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">To Date </label>
                                    <input type="date" name="to_date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Sales Counter</label>
                                    <select name="counter" id="" class="form-control">
                                        <option selected disabled>---Select Counter ---</option>
                                        <option value="1">Counter No: 01</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Report Type</label>
                                <input type="text" name="date" class="form-control">
                            </div>
                        </div> -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Manufacturer</label>
                                    <select name="manufacturer" id="" class="form-control product_filter">
                                        <option selected disabled>--- Select Manufacturers ---</option>
                                        @foreach($manufactures as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="">
                                    <label for="">&#8203;</label>
                                    <button type="submit" class="btn filter-btn">Filter</button>
                                </div>
                            </div>
                            @if(!empty($sales_reports))
                            <div class="reset_action">
                                <a class="btn create-btn mr-2 ml-2" type="button" id="btnExport" onclick="Export()">PDF <i class="pl-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                <a class="btn create-btn" type="button" id="print_btn" onclick="printDiv('purchaseReport')">Print <i class="pl-2 fa fa-print"></i>
                            </a>
                        </div>
                        @endif
                        </div>
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
            </form>

            <!-- table-area -->
            @if(!empty($sales_reports))
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="pending-store-deposite-receive" id="purchaseReport">
                        <div class="row">
                            <div class="th-table-header mx-auto text-center">
                                    <tr>
                                        <td>
                                            <h3><strong> Alesha Pharmacy </strong></h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        @if(isset($search_filter['store_name']))
                                        <td>
                                            <p> <strong>Store Name:{{$search_filter['store_name']}}</strong> </p>
                                        </td>
                                        @endif

                                        @if(isset($search_filter['product_name']))
                                        <td>
                                            <p> <strong>Product Name: {{$search_filter['product_name']}}</strong> </p>
                                        </td>
                                        @endif
                                    </tr>

                                    @if(isset($search_filter['product_type']))
                                    <tr>

                                        <td>
                                            <p> <strong>Product Type: {{$search_filter['product_type']}}</strong> </p>
                                        </td>
                                    </tr>
                                    @endif

                                    @if(isset($search_filter['categores']))
                                    <tr>
                                        <td>
                                            <p> <strong>Product Category: {{$search_filter['categores']}}</strong> </p>
                                        </td>
                                    </tr>
                                    @endif


                                    
                                    @if(isset($search_filter['date']))
                                    <tr>
                                        <td>
                                            <p> <strong>Date:</strong> {{$search_filter['date']}} </p>
                                        </td>
                                    </tr>
                                    @endif
            <!-- 
                                    <tr>
                                        <td>
                                            <p> <strong>Counter:</strong> </p>
                                        </td>
                                    </tr> -->
                                    @if(isset($search_filter['manufactures']))
                                    <tr>
                                        <td>
                                            <p> <strong>Manufacturer:</strong> {{$search_filter['manufactures']}} </p>
                                        </td>
                                    </tr>
                                    @endif
                            </div>

                        </div>
                        <div class="row">
                            <!-- data table -->
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover" id="deposit-receive-table">
                                    <thead>
                                        <tr>
                                            <th width="5%" style="border-right-width: 4px;">#</th>
                                            <th width="" style="border-right-width: 4px;">Product Details</th>
                                            <th width="" style="border-right-width: 4px;">Generic</th>
                                            <th width="" style="border-right-width: 4px;">Manufacturer</th>
                                            <th width="">Sold Qty</th>
                                            <th width="">UOM</th>
                                            <th width="">Discount</th>
                                            <th width="">Vat & Tax</th>
                                            <th width="15%">Total Amount</th>
                                            <!-- <th width="15%">Action</th> -->
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($sales_reports))
                                        @foreach($sales_reports as $row)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>
                                                {{$row['product']}}
                                            </td>
                                            <td class="align-item-center">
                                                {{$row['uom']}}
                                            </td>

                                            <td>{{$row['manufacturer']}}</td>
                                            <td>{{$row['orderqty']}}</td>
                                            <td>{{$row['uom']}}</td>
                                            <td>{{$row['discount']}}</td>
                                            <td>{{$row['vat_amount']}}</td>
                                            <td>{{(float)$row['total'] - $row['discount'] + $row['vat_amount']}}</td>
                                            <!-- <td>
                                                        <div class="d-flex justify-content-around align-items-center">
                                                            <a href="#" class="btn details-btn mr-2">
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </td> -->
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr class="text-center">
                                            <th colspan="8">No Data Found!</th>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div><!--  end .table -->
                        </div><!-- end .row -->
                    </div><!-- end .pending-store-deposite-receive -->
                </div><!-- end .col-md-12 -->
            </div><!-- end .row -->
            @endif

            </div><!-- end .tile-body -->
        </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
</div><!-- end .row -->



@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $('#deposit-receive-table').DataTable();
</script>

<script>
    $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
</script>

<script>
    $(document).on('click', 'ul li', function() {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>
<script>
    $(document).ready(function() {
        $('.product_filter').select2();

        $('.product_search').select2({
            ajax: {
                url: '{{ route('get.product.search')}}',
                data: function(params) {
                    return {
                        product_search: params.term
                    };
                },
                dataType: 'json',
                processResults: function(data, params) {
                    let product=[];
                    data.forEach(element => {
                        let item={
                            id:element.id,
                            text:element.product_name
                        }
                        product.push(item);
                    });
                    
                    return {
                        results: product,
                    };
                },

            },
            placeholder: 'Search for a repository',
            minimumInputLength: 2,
            // templateResult: formatRepo,
        });

    });

    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }

        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'></div>" +
            "<small class='select2-result-repository__uom'></small>" +
            "</div>"
        );

        $container.find(".select2-result-repository__title").text(repo.product_name);

        return $container;
    }
</script>


<script type="text/javascript" src="{{asset('js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/html2canvas.min.js')}}"></script>

<script>
    function Export() {
        html2canvas(document.getElementById('purchaseReport'), {
            onrendered: function(canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Purchase Report.pdf");
            }
        });
    }


    function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
</script>
@endpush