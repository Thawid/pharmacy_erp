@extends('dboard.index')
@section('title', 'Pending Store Deposite Receive')
@push('styles')
<style>
    .pending-store-deposite-receive {
        border-radius: 8px;
    }

    div.dataTables_wrapper div.dataTables_filter label {
        font-weight: normal;
        white-space: nowrap;
        text-align: left;
        font-size: 0;
    }

    div.dataTables_wrapper div.dataTables_length label {
        font-weight: normal;
        text-align: left;
        white-space: nowrap;
        font-size: 0;
    }


    div.dataTables_wrapper div.dataTables_info {
        padding-top: 0.85em;
        white-space: nowrap;
        margin-left: 10px;
    }

    table.dataTable td:last-child i {
        font-size: 16px;
        margin: -1px;
    }

    ul.title-heading {
        display: flex;
        padding: 0;
        margin: 0;
    }

    ul.title-heading li {
        list-style: none;
        margin-right: 15px;
    }

    ul.title-heading li a {
        text-decoration: none;
        position: relative;
    }

    ul.title-heading li a:hover {
        text-decoration: none;
    }

    ul.title-heading li a::before {
        position: absolute;
        content: "";
        height: 2px;
        width: 0;
        background: #0088FF;
        bottom: -5px;
        left: 0;
        right: 0;
        margin: auto;
        transition: .3s;
    }

    ul.title-heading li:hover a::before {
        width: 100%;
    }

    ul.title-heading li.active a {
        color: #0088FF;
        transition: .3s;
    }

    ul.title-heading li.active a::before {
        width: 100%;
    }

    /* .dataTable  thead {
            background: #294A65;
            color: white;
        }
        .dataTable  thead th{
            border: 1px solid transparent;
        } */
</style>

<style>
        table {
            text-align: center;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .th-table-header, .th-table-info, .th-table-body {
            margin-bottom: 20px;
        }

        .th-table-header table tr td h1 {
            margin: 0;
            font-size: 35px;
        }

        .th-table-header table tr td h2 {
            margin: 0;
            font-size: 18px;
        }

        .th-table-header table tr td h3 {
            padding: 0;
            margin: 0;
            font-size: 24px;
        }

        .th-table-info p {
            margin: 0;
        }

        .th-table-info table tr td p {
            text-align: left;
        }
        .th-table-info table {
            text-align: center !important;
        }

        .th-table-header table tr td p {
            padding: 0;
            margin: 0;
            font-size: 16px;
        }

    </style>
@endpush
@section('dboard_content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Sales Summary</h2>
                    </div><!-- end .col-md-6 -->

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{ route('pos.index') }}">Back</a>
                    </div><!-- end .col-md-6 -->
                </div><!-- end .row -->
                <hr>

                    <!-- filter -->
                    <form action="{{route('pos.hub.report.search')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                             
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Select Store</label>
                                        <select name="store_id" class="form-control product_search">
                                            <option value="0">All Store</option>
                                            @foreach($stores as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">From Date </label>
                                        <input type="date" name="from_date" class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">To Date </label>
                                        <input type="date" name="to_date" class="form-control">
                                    </div>
                                </div>
                              

                                <div class="col-md-3">
                                    <div class="">
                                        <label for="">&#8203;</label>
                                        <button type="submit" class="btn filter-btn">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                </form>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <div class="pending-store-deposite-receive">
                            <div class="row">
                                <div class="th-table-header mx-auto text-center">
                                        <tr>
                                            <td>
                                                <h3><strong> Alesha Pharmacy </strong></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if(isset($search_item['store_name']))
                                            <td>
                                                <p> <strong>Store Name:</strong> {{$search_item['store_name']}} </p>
                                            </td>
                                            @endif

                                            @if(isset($search_item['date']))
                                            <td>
                                                <p> <strong>Date :</strong> {{$search_item['date']}} </p>
                                            </td>
                                            @endif
                                        </tr>

                                </div>

                            </div>


                            <div class="row">
                                <!-- data table -->
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table-hover" id="deposit-receive-table">
                                        <thead>
                                            <tr>
                                                <th width="">Store Name</th>
                                                <th width="">No of Invoice</th>
                                                <th width="">Discount</th>
                                                <th width="15%">Invoice Amount</th>
                                                <!-- <th width="15%">Action</th> -->
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if(!empty($pos_itemms))
                                            @foreach($pos_itemms as $row)
                                                <tr>
                                                    <td>{{$row['store_name']}}</td>
                                                    <td>{{$row['invoice_no']}}</td>
                                                    <td>{{$row['discount']}}</td>
                                                    <td>{{$row['total']}}</td>
                                                </tr>
                                            @endforeach
                                            @else
                                            <tr class="text-center">
                                                <th colspan="8">No Data Found!</th>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div><!--  end .table -->
                            </div><!-- end .row -->
                        </div><!-- end .store-deposite-receive -->
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
            </div><!-- end .tile-body -->
        </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
</div><!-- end .row -->



@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $('#deposit-receive-table').DataTable();
</script>

<script>
    $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
</script>

<script>
    $(document).on('click', 'ul li', function() {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>
<script>
    $(document).ready(function() {
        $('.store_list').select2();

</script>
@endpush