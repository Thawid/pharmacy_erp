@extends('dboard.index')
@section('title', 'Pending Store Deposite Receive')
@push('styles')
<style>
    .pending-store-deposite-receive {
        border-radius: 8px;
    }

    div.dataTables_wrapper div.dataTables_filter label {
        font-weight: normal;
        white-space: nowrap;
        text-align: left;
        font-size: 0;
    }

    div.dataTables_wrapper div.dataTables_length label {
        font-weight: normal;
        text-align: left;
        white-space: nowrap;
        font-size: 0;
    }


    div.dataTables_wrapper div.dataTables_info {
        padding-top: 0.85em;
        white-space: nowrap;
        margin-left: 10px;
    }

    table.dataTable td:last-child i {
        font-size: 16px;
        margin: -1px;
    }

    ul.title-heading {
        display: flex;
        padding: 0;
        margin: 0;
    }

    ul.title-heading li {
        list-style: none;
        margin-right: 15px;
    }

    ul.title-heading li a {
        text-decoration: none;
        position: relative;
    }

    ul.title-heading li a:hover {
        text-decoration: none;
    }

    ul.title-heading li a::before {
        position: absolute;
        content: "";
        height: 2px;
        width: 0;
        background: #0088FF;
        bottom: -5px;
        left: 0;
        right: 0;
        margin: auto;
        transition: .3s;
    }

    ul.title-heading li:hover a::before {
        width: 100%;
    }

    ul.title-heading li.active a {
        color: #0088FF;
        transition: .3s;
    }

    ul.title-heading li.active a::before {
        width: 100%;
    }

    /* .dataTable  thead {
            background: #294A65;
            color: white;
        }
        .dataTable  thead th{
            border: 1px solid transparent;
        } */
</style>

<style>
    table {
        text-align: center;
        margin: 0;
        padding: 0;
        width: 100%;
    }

    .th-table-header,
    .th-table-info,
    .th-table-body {
        margin-bottom: 20px;
    }

    .th-table-header table tr td h1 {
        margin: 0;
        font-size: 35px;
    }

    .th-table-header table tr td h2 {
        margin: 0;
        font-size: 18px;
    }

    .th-table-header table tr td h3 {
        padding: 0;
        margin: 0;
        font-size: 24px;
    }

    .th-table-info p {
        margin: 0;
    }

    .th-table-info table tr td p {
        text-align: left;
    }

    .th-table-info table {
        text-align: center !important;
    }

    .th-table-header table tr td p {
        padding: 0;
        margin: 0;
        font-size: 16px;
    }
    .product_filter > option{
        padding: 25px;
        font-size: 20px;
        letter-spacing:30%

    }
</style>
@endpush
@section('dboard_content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Batch Wise Sale Summary</h2>
                    </div><!-- end .col-md-6-->

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{ route('pos.index') }}">Back</a>
                    </div><!-- end .col-md-6 -->
                </div><!-- end .row -->
                <hr>

                <!-- filter-area -->
                <form action="{{route('pos.batch.filter')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Search type</label>
                                        <select name="product_type" class="form-control product_filter text-center">
                                            <option selected disabled>--Select Filter Type---</option>
                                            <option @if(isset($status) && $status == 'regular') selected @endif  value="regular" >Regular</option>
                                            <option @if(isset($status) && $status == 'empty') selected @endif value="empty" >Empty</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="">
                                        <label for="">&#8203;</label>
                                        <button type="submit" class="btn filter-btn">Filter</button>
                                    </div>
                                </div>
                                @if(!empty($stock_reports) || !empty($empty_stock_reports))
                                <div class="reset_action">
                                    <a class="btn create-btn mr-2 ml-2" type="button" id="btnExport" onclick="Export()">PDF <i class="pl-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                    <a class="btn create-btn" type="button" id="print_btn" onclick="printDiv('purchaseReport')">Print <i class="pl-2 fa fa-print"></i>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                </form>

                <!-- store regular data show -->

                @if(!empty($stock_reports))
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pending-store-deposite-receive" id="purchaseReport">
                            <div class="row">
                                <!-- data table -->
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table-hover" id="deposit-receive-table">
                                        <thead>
                                            <tr>
                                                <th width="5%" style="border-right-width: 4px;">#</th>
                                                @if(auth()->user()->role == 'admin')
                                                    <th width="" style="border-right-width: 4px;">Store Name</th>
                                                @endif
                                                <th width="" style="border-right-width: 4px;">Product Name</th>
                                                <th width="" style="border-right-width: 4px;">Batch No</th>
                                                <th width="" style="border-right-width: 4px;">Total Quentity</th>
                                                <th width="" style="border-right-width: 4px;">Total Available Quentity</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if(!empty($stock_reports))
                                            @foreach($stock_reports as $row)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                @if(auth()->user()->role == 'admin')
                                                <td>{{$row->store_name}}</td>
                                                @endif
                                                <td>{{$row->product_name}}</td>
                                                <td>{{$row->batch_no}}</td>
                                                <td>{{$row->received_quantity}}</td>
                                                <td>{{$row->received_quantity - $row->out_quantity}}</td>
                                       
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr class="text-center">
                                                <th colspan="8">No Data Found!</th>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div><!--  end .table -->
                            </div><!-- end .row -->
                        </div><!-- end .pending-store-deposite-receive -->
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
                @endif
                <!-- store regular data show end-->


                
                <!-- store empty data show -->

                @if(!empty($empty_stock_reports))
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pending-store-deposite-receive" id="purchaseReport">
                            <div class="row">
                                <!-- data table -->
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table-hover" id="deposit-receive-table">
                                        <thead>
                                            <tr>
                                                <th width="5%" style="border-right-width: 4px;">#</th>
                                                @if(auth()->user()->role == 'admin')
                                                    <th width="" style="border-right-width: 4px;">Store Name</th>
                                                @endif
                                                <th width="" style="border-right-width: 4px;">Product Name</th>
                                                <th width="" style="border-right-width: 4px;">Batch No</th>
                                                <th width="" style="border-right-width: 4px;">Stock Out Date</th>
                                                <th width="" style="border-right-width: 4px;">Stock Status</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if(!empty($empty_stock_reports))
                                            @foreach($empty_stock_reports as $row)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                @if(auth()->user()->role == 'admin')
                                                <td>{{$row->store_name}}</td>
                                                @endif
                                                <td>{{$row->product_name}}</td>
                                                <td>{{$row->batch_no}}</td>
                                                <td>{{$row->out_date}}</td>
                                                <td>{{strtoupper($row->stock_status)}}</td>
                                       
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr class="text-center">
                                                <th colspan="8">No Data Found!</th>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div><!--  end .table -->
                            </div><!-- end .row -->
                        </div><!-- end .pending-store-deposite-receive -->
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
                @endif
                <!-- store empty data show end-->

            </div><!-- end .tile-body -->
        </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
</div><!-- end .row -->



@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script type="text/javascript" src="{{asset('js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/html2canvas.min.js')}}"></script>

<script type="text/javascript">
    $('#deposit-receive-table').DataTable();
</script>

<script>
    $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
</script>

<script>
    $(document).on('click', 'ul li', function() {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>




<script>
    function Export() {
        html2canvas(document.getElementById('purchaseReport'), {
            onrendered: function(canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Batch Wise Sales Report.pdf");
            }
        });
    }


    function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
</script>

@endpush