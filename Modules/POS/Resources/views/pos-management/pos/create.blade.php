@extends('dboard.index')
@section('title', 'Point of Sale')
@push('styles')
<style>
    /*remove default margin padding on paragraph tag*/
    p {
        padding: 0;
        margin: 0;
    }

    /*auto search end*/
    .input-icons i {
        position: absolute;
    }


    .remove-product {
        height: 20px;
        line-height: 0;
    }

    input.pa {
        width: 350px;
    }

    input.ra {
        width: 350px;
    }

    /*-----------------*/

    .autocomplete {
        /*the container must be positioned relative:*/
        position: relative;
        display: inline-block;
    }

    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        /*position the autocomplete items to be the same width as the container:*/
        top: 100%;
        left: 0;
        right: 0;
    }

    .autocomplete-items div {
        padding: 10px;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
    }

    .autocomplete-items div:hover {
        /*when hovering an item:*/
        background-color: #e9e9e9;
    }

    .autocomplete-active {
        /*when navigating through the items using the arrow keys:*/
        background-color: DodgerBlue !important;
        color: #ffffff;
    }

    .payment-buttons a:hover {
        background: rgba(0, 136, 255, 0.1);
        color: #0088FF;

    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 20px !important;
    }

    .index-btn {
        color: #0088FF !important;
    }

    .index-btn:hover {
        color: #0088FF !important;
    }

    .select2.select2-container {
        width: 670px;
    }

    .select2-container .select2-selection--single {
        height: 38px !important;
    }

    .select2-container--default .select2-selection--single {
        border: 2px solid #ced4da !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 35px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {

        line-height: 28px !important;
    }

    /*--------------------*/

    .sales-info>p>strong {
        width: 120px;
        padding-right: 8px;
    }

    .select2-search--dropdown {
        display: block;
        padding: 0 !important;
        position: absolute;
        width: 100%;
        top: -35px;
    }
</style>
@endpush

@section('dboard_content')
<form action="{{ route('point_of_sale.store') }}" method="post">
    @csrf
    <!-- data-area -->
    <div class="tile">
        <!-- title -->
        <div class="row align-items-center">
            <div class="col-md-6">
                <h2 class="title-heading">Point Of Sale</h2>
            </div><!-- end .col-md-6 -->

            {{-- <div class="col-md-6 text-right">
                    <a class="btn index-btn" href="{{ route('pos.index') }}">Back</a>
        </div><!-- end .col-md-6 --> --}}
    </div><!-- end .row -->
    <hr>

    <div class="row">
        <!-- invoice number -->
        <div class="col-md-2">
            <div class="form-group">
                <label for="invoice_no">Invoice No</label>
                <input type="text" placeholder="invoice no." value="{{ $invoice_no }}" readonly class="form-control" name="invoice_no" onkeypress="return (event.charCode !=8 && event.charCode ==0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
            </div>
        </div><!-- end .col-md-2 -->

        <!-- customer-info -->
        <div class="col-md-6">
            <label for="customer_info">
                Customer Information
            </label>
            <div class="customer-info d-md-flex">
                <input class="form-control" id="phone_no" type="text" name="mobile_no" onchange="checkPhoneNumber()" placeholder="Mobile No.">
                <input class="form-control" type="text" id="customar_name" name="name" placeholder="Name">
                <input class="form-control" type="text" name="membership_card" id="membership_card" oninput="apply_Card()" placeholder="Membership card">
            </div>
        </div><!-- end .col-md-6 -->

        <div class="col-md-4">
            <div class="counter-info bg-light p-2 border rounded">
                <p>{{ \Carbon\Carbon::now()->format('d M Y || H:i')}}</p>
                <p><strong>Counter No: </strong>02</p>
                <p><strong>Employ Name: </strong> {{ auth()->user()->name }}</p>
                <p><strong>POS Machine: </strong>6645</p>
            </div>
        </div><!-- end .col-md-4 -->
    </div><!-- end .row -->
    <hr>
    <div class="row ">
        <div class="col-md-8">
            <div class="form-group autocomplete">
                <label for="search_product">Search Product By Product Name</label>
                <div class="input-icons">
                    <i class="fa fa-search icon "></i>
                    <!-- <input type="text" class="form-control pl-4" id="search-bar" autocomplete="off"
                                                                                                                                                       placeholder="search here..."> -->
                    <select class="form-control pl-4 select_product" id="select_product" name="search_product"></select>
                </div>
            </div>
        </div><!-- end .col-md-7 -->
        <!-- <div class="col-md-5">
                    <input type="text" class="form-control" style="margin-top: 28px" placeholder="Manual Product Barcode">
                </div> -->
    </div><!-- end .row -->

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover bg-white table-md">
                    <thead>
                        <tr>
                            <!-- <td width="1%">SKU</td> -->
                            <td width="20%">Product Name</td>
                            <td width="20%">UOM</td>
                            <td width="10%">Avl. Qty</td>
                            <td width="10%">Order Qty</td>
                            <td width="10%">Unit Price</td>
                            <!-- <td width="10%">Discount %</td> -->
                            <td width="20%">Sub-Total</td>
                            <td width="5%">Action</td>
                        </tr>
                    </thead>
                    <tbody id="selected_product">


                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end .row -->

    <hr>

    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                <div class="form-group mt-3">
                    <label for="payment_method">Return Invoice </label>
                    <input type="text" step="any" class="form-control pa" oninput="apply_returnInvoice()" id="apply_returnInvoice_no" placeholder="Enter Your Return Invoice">
                </div>
            </div>


            <div class="form-group">
                <label for="payment_method">Payment Method</label>
                <div class="payment-buttons d-md-flex justify-content-between border rounded" style="width: 350px;">
                    <a class="btn">Cash</a>
                    <a class="btn">Credit/Debit Card</a>
                    <a class="btn">Mobile Banking</a>
                </div>

                <div class="form-group mt-3">
                    <label for="paid_amount">Paid Amount</label>
                    <input type="text" onkeyup="calculationGrandTotal()" step="any" id="paid_amount" class="form-control pa" name="paid_amount">
                </div>

                <div class="form-group">
                    <label for="return_amount">Change Amount</label>
                    <input type="text" onkeyup="calculationGrandTotal()" step="any" readonly id="return_amount" class="form-control ra" name="return_amount">
                </div>
            </div>
        </div>



        <div class="col-md-5">
            <div class="sales-info bg-light p-3 border rounded text-right">
                <p class="pb-2 form-inline w-100">
                    <strong class="">Sale Discount: </strong>
                    <input class="form-control text-right" type="number" step="any" id="sale_discount" readonly value="" name="sale_discount" onkeyup="calculationGrandTotal()"><span class="pl-2">BDT</span>
                </p>
                <p class="pb-2 form-inline">
                    <strong class="">Total Discount: </strong>
                    <input class="form-control text-right" type="number" step="any" id="total_discount" name="total_discount" onkeyup="calculationGrandTotal()" readonly><span class="pl-2">BDT</span>
                </p>
                <p class="pb-2 form-inline">
                    <strong class="">Total Tax: </strong>
                    <input class="form-control text-right" type="number" id="total_tax" step="any" name="total_tax" onkeyup="calculationGrandTotal()" readonly><span class="pl-2">BDT</span>
                </p>
                <p class="pb-2 form-inline">
                    <strong class="">Other Cost: </strong>
                    <input class="form-control text-right" type="text" id="other_cost" step="any" name="other_cost" onkeyup="calculationGrandTotal();"><span class="pl-2">BDT</span>
                </p>



                <p class="form-inline">
                    <strong class="">Grand Total:</strong>
                    <input type="hidden" id="discount_arr" value="" name="discount_arr">
                    <input type="hidden" id="vat_arr" value="" name="vat_arr">
                    <input class="form-control text-right" type="number" name="grand_total" id="grand_total" step="any" readonly>
                    <span class="pl-2">BDT</span>
                </p>

                <p class="pb-2 form-inline">
                    <strong class="">Ref. Invoice Amount: </strong>
                    <input class="form-control text-right" type="text" id="return_invoice" step="any" readonly><span class="pl-2">BDT</span>
                </p>


                <p class="pb-2 form-inline">
                    <strong class="" id="return_amount_title">Return Amount: (PAY/DUE) </strong>
                    <input class="form-control text-right" type="text" id="invoice_return_amount" step="any" readonly><span class="pl-2">BDT</span>
                </p>



            </div>
        </div>
    </div><!-- end .row -->
    <hr>
    <div class="row text-right">
        <div class="col-md-12">
            <div class="buttons">
                <button type="submit" class="btn index-btn">Confirm Sales</button>
                <!-- <a href="#" disabled class="btn btn-primary mr-2">Cancel Sales</a>
                        <a href="#" disabled class="btn btn-primary">Draft Sales</a> -->
            </div>
        </div>
    </div><!-- end .row -->


    </div><!-- end .tile -->
</form>
@endsection
@push('post_scripts')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>
    /*
     * Prevent deduct amount using mouse click
     * @return Product
     */

    function preventNonNumericalInput(e, em) {

        e = e || window.event;
        var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        var charStr = String.fromCharCode(charCode);
        var em = em;

        if (e.keyCode == 13) {
            $('.select_product').select2('open');
        }

        if (e.keyCode == 32) {
            removeProduct(em);
            $('.select_product').select2('open');
        }

        if (!charStr.match(/^[0-9]+$/))
            e.preventDefault();
    }
</script>

<script>
    /*
     * Product Search
     * @return Product
     */
    var uniqe_id = 0;
    var baseNo;

    $(document).ready(function() {
        $(".select_product").select2({
            ajax: {
                url: "{{ route('pos.product.search') }}",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        product_search: params.term
                    };
                },
                processResults: function(data, params) {
                    // console.log(data);
                    params.page = params.page || 1;

                    let products = [];
                    data.forEach(element => {
                        let item = {
                            id: element.batch_no,
                            text: element.product_name + '(' + element.batch_no + ')',
                        }
                        products.push(item);
                    })
                    return {
                        // results: data,
                        results: products,
                    };
                },
            },
            placeholder: 'Search for a Product',
            minimumInputLength: 2,
            // templateResult: formatRepo,
            // templateSelection: formatRepoSelection
        }).on('select2:closing', async function(e) {
            var product_id = e.target.value;
            await getProductData(product_id);
        });
        $('.select_product').select2('open');
    })


    /*
     * Show Product In Search Dropdown
     * @return Product Details
     */


    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }

        this.uniqe_id = Math.floor(Math.random() * 50);
        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'></div>" +
            "</div>"
        );
        $container.find(".select2-result-repository__title").text(repo.product_name + "(" + repo.batch_no + ")");

        return $container;
    }

    /*
     * Product Add On rows
     * @return Product Details
     */

    function getProductData(id) {
        $(".select_product").empty();
        var vm = this;
        var uniqid = this.uniqe_id;
        var product_id = id;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: "{{ route('pos.product.add') }}",
            data: {
                product_id,
            },
            success: function(data) {
                console.log(data);
                if (data.store_price != null) {

                    store_price = data.store_price.regular_price
                } else {
                    store_price = 0
                }

                var html =
                    `<tr class="deleted_product${data.base_no}" id="deleted_product${data.base_no}">
                               <input type="hidden" class="product_id" id="product_id${data.base_no}" name="product_id[]" value="${data.product.product.id}"/>
                               <input type="hidden" id="price_id${data.base_no}" value="${data.id}"/> 
                               <input type="hidden" class="discount_amount" id="discount_amount${data.base_no}" value=""/>
                            <td>
                                ${data.product.product.product_name} (${data.base_no})
                            </td>
                            <td> 
                                <select class="form-control uom_id" onchange="getUomPrice(this)" data-id="${data.base_no}" name="uom_id[]" id="uom_id${data.base_no}">`;

                data.uoms.forEach(function(e) {
                    if (e.uom_id == data.product.uom_id) {
                        html += `<option value="${e.uom_id}" selected>${e.uom.uom_name}</option>`;
                    } else {
                        html += `<option value="${e.uom_id}">${e.uom.uom_name}</option>`;
                    }

                })
                html += `</select>
                                <td>
                                <input type="hidden" class="form-control" id="const_avl_qty${data.base_no}" value="${data.available_qty}" readonly/>
                                <input type="hidden" class="form-control" id="batch_no${data.base_no}" name="base_no[]" value="${data.base_no}" readonly/>
                                <input type="number" class="form-control" id="avl_qty${data.base_no}" value="${data.available_qty}" readonly/>
                                </td>
                            </td>
                                <td>
                                    <input type="text" class="form-control order_qty" onkeypress="preventNonNumericalInput(event,this)" onkeyup="deduction_aval_qty(this)" data-id="${data.base_no}" id="order_qyt${data.base_no}" value="" required name="order_qty[]">
                                </td>
                                <td>
                                    <input type="number" class="form-control" readonly id="per_pcs_price${data.base_no}" name="per_pcs_price[]" value="${data.price}">
                                </td>
                               
                                <td>
                                    <input type="number" class="form-control total_price" data-id='${data.base_no}' readonly id="subtotal${data.base_no}"  name="subtotal[]">
                                </td>
                                <td>
                                    <div onclick="removeProduct(this)" class="btn btn-danger remove-product">x</div>
                                </td>
                            </tr>`;
                var product = document.querySelector('#product_id' + data.base_no);


                if (!product) {
                    $('#selected_product').append(html);
                    $('#order_qyt' + data.base_no).focus();
                } else {
                    alert('Product Already Added')
                    $('.select_product').select2('open');
                }
            },
            error: function(err) {
                console.log(err.responseJSON);
            }
        });

    }

    /*
     * Remove Product Rows
     *
     */

    function removeProduct(em) {
        $(em).closest('tr').remove();
        calculateTotal(em);
        calculationGrandTotal();
    }

    /*
     * Get Product By Uom ID When 
     * @return Product
     */

    function getUomPrice(em) {
        var element = em;
        var data_id = $(em).data('id');
        var product_id = $('#product_id' + data_id).val();
        var uom_id = $('#uom_id' + data_id).find(":selected").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: "{{ route('get.uom.price') }}",
            data: {
                product_id,
                uom_id
            },
            success: function(data) {
                var order_qyt = $('#order_qyt' + data_id).val() ? $('#order_qyt' + data_id).val() : 0;
                $('#per_pcs_price' + data_id).val(data.prices);
                $('#const_avl_qty' + data_id).val(data.available_qty);
                $('#avl_qty' + data_id).val(data.available_qty - order_qyt);
                calculateTotal(element);
            },
            error: function(err) {

            }
        });

    }
</script>


<script>
    /*
     * Calculation Total Price
     * @return Total Price
     */

    async function calculateTotal(em) {

        var id = $(em).data('id');


        const product = [];
        document.querySelectorAll('.product_id').forEach(function(e) {
            product.push(e.value);
        })

        const uom = [];
        document.querySelectorAll('.uom_id').forEach(function(e) {
            uom.push(e.options[e.selectedIndex].value)
        })

        const order_qty = [];
        document.querySelectorAll('.order_qty').forEach(function(e) {
            order_qty.push(e.value);
        })

        const phone_no = $('#phone_no').val();



        var avl_qty = $('#avl_qty' + id).val() ? $('#avl_qty' + id).val() : 0;
        var order_qyt = $('#order_qyt' + id).val() ? $('#order_qyt' + id).val() : 0;

        var discount_price = $('#discount_price' + id);

        var product_id = $('#product_id' + id).val();
        var uom_id = $('#uom_id' + id).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: "{{ route('get.discount.price') }}",
            data: {
                product_id,
                uom_id,
                product,
                phone_no,
                uom,
                order_qty
            },
            success: function(data) {


                $('#sale_discount').val(parseFloat(data.discount).toFixed(2));
                $('#total_tax').val(data.vat_tax);
                $('#total_discount').val(parseFloat(data.discount).toFixed(2));


                var per_pcs_price = $('#per_pcs_price' + id).val() ? $('#per_pcs_price' + id).val() : 0;
                var discount = $('#discount' + id).val() ? $('#discount' + id).val() : 0;

                total_price = parseFloat(order_qyt) * parseFloat(per_pcs_price);
                total_discount = (parseFloat(total_price) * parseFloat(discount)) / 100;
                total = parseFloat(total_price) - parseFloat(total_discount);

                $('#subtotal' + id).val(total);
                $('#discount_amount' + id).val(total_discount);
                $('#discount_arr').val(JSON.stringify(data.indi_discount));
                $('#vat_arr').val(JSON.stringify(data.indi_vat));

                var sale_discount = $('#sale_discount').val() ? $('#sale_discount').val() : 0;
                var total_discount = $('#total_discount').val() ? $('#total_discount').val() : 0;
                var total_tax = $('#total_tax').val() ? $('#total_tax').val() : 0;
                var other_cost = $('#other_cost').val() ? $('#other_cost').val() : 0;

                var total_sum = 0;
                $('.total_price').each(function() {
                    var inputval = $(this).val();
                    if ($.isNumeric(inputval)) {
                        total_sum += parseFloat(inputval);
                    }
                })

                var discount_amount = 0;
                $('.discount_amount').each(function() {
                    var inputval = $(this).val();
                    if ($.isNumeric(inputval)) {
                        discount_amount += parseFloat(inputval);
                    }
                })


                var discount_amount = parseFloat(discount_amount) + parseFloat(sale_discount);

                $('#total_discount').val(discount_amount.toFixed(2));


                var total_after_sale_discount = parseFloat(total_sum) - parseFloat(sale_discount);
                var total_after_tax = parseFloat(total_after_sale_discount) + parseFloat(total_tax);
                var grand_total = parseFloat(total_after_tax) + parseFloat(other_cost);
                $('#grand_total').val(parseFloat(grand_total.toFixed(2)));

                calculationGrandTotal();

            },
            error: function(err) {

            }
        });
    }
</script>

<script>
    /*
     * Calculation Grand Total
     * @return Total Price
     */

    function calculationGrandTotal() {

        var sale_discount = $('#sale_discount').val() ? $('#sale_discount').val() : 0;
        var total_discount = $('#total_discount').val() ? $('#total_discount').val() : 0;
        var total_tax = $('#total_tax').val() ? $('#total_tax').val() : 0;
        var other_cost = $('#other_cost').val() ? $('#other_cost').val() : 0;
        var paid_amount = $('#paid_amount').val() ? $('#paid_amount').val() : 0;

        var total_sum = 0;
        $('.total_price').each(function() {
            var inputval = $(this).val();
            if ($.isNumeric(inputval)) {
                total_sum += parseFloat(inputval);
            }
        })


        var discount_amount = 0;
        $('.discount_amount').each(function() {
            var inputval = $(this).val();
            if ($.isNumeric(inputval)) {
                discount_amount += parseFloat(inputval);
            }
        })

        var discount_amount = parseFloat(discount_amount) + parseFloat(sale_discount);

        $('#total_discount').val(discount_amount.toFixed(2));

        var total_after_sale_discount = parseFloat(total_sum) - parseFloat(sale_discount);
        var total_after_tax = parseFloat(total_after_sale_discount) + parseFloat(total_tax);
        var grand_total = parseFloat(total_after_tax) + parseFloat(other_cost);
        $('#grand_total').val(parseFloat(grand_total).toFixed(2));



        var grand_total = $('#grand_total').val() ? $('#grand_total').val() : 0;
        var return_invoice = $('#return_invoice').val() ? $('#return_invoice').val() : 0;
        var amount = grand_total - return_invoice;


        if (amount < 0) {
            $('#return_amount_title').html('Payable');
            $('#invoice_return_amount').val(Math.abs(amount));
        } else {
            $('#return_amount_title').html('Receivable');
            $('#invoice_return_amount').val(Math.abs(amount));

            if (paid_amount > 0) {
                var return_amount = parseFloat(amount) - parseFloat(paid_amount);
                if (return_amount < 0) {
                    $('#return_amount').val(Math.abs(parseFloat(return_amount).toFixed(2)));
                }
            } else {
                $('#return_amount').val(0);
            }

        }

        cal_total_discount = parseFloat(discount_amount) + parseFloat(sale_discount);

    }
</script>


<script>
    /*
     * Get Customer information using Phone number
     * @return Customer Information
     */

    $(document).ready(function() {
        $('#phone_no').keyup(function() {
            var phone_no = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: "{{ route('get.customer.info') }}",
                data: {
                    phone_no
                },
                success: function(data) {
                    if (data) {
                        $('#customar_name').val(data.customer_name);
                    }
                },
                error: function(err) {

                }
            });
        });
    });
</script>


<script>
    $(document).on('click', '.payment-buttons a', function() {
        $(this).addClass('index-btn').siblings().removeClass('index-btn');
    });
</script>

<script>
    /*
     * Check Customer Group wise Discount
     * @return Discount
     */

    function checkPhoneNumber() {
        const product = [];
        document.querySelectorAll('.product_id').forEach(function(e) {
            product.push(e.value);
        })

        const uom = [];
        document.querySelectorAll('.uom_id').forEach(function(e) {
            uom.push(e.options[e.selectedIndex].value)
        })

        const order_qty = [];
        document.querySelectorAll('.order_qty').forEach(function(e) {
            order_qty.push(e.value);
        })
        const phone_no = $('#phone_no').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: "{{ route('check.customar.phone.number') }}",
            data: {
                phone_no,
                product,
                uom,
                order_qty
            },
            success: function(data) {
                if (data.discount) {
                    $('#sale_discount').val(data.discount);
                    $('#total_discount').val(data.discount)
                }
                if (data.vat_tax) {
                    $('#total_tax').val(data.vat_tax);
                }

                calculationGrandTotal();
            },
            error: function(err) {

            }
        });
    }
</script>

<script>
    /*
     * Deduct Available Quentity
     * @return Available Quentity
     */

    function deduction_aval_qty(em) {

        var id = $(em).data('id');

        var avl_qty = $('#avl_qty' + id).val();
        var order_qyt = $('#order_qyt' + id).val();


        var product_id = $('#product_id' + id).val();
        var batch_no = $('#batch_no' + id).val();
        var uom_id = $('#uom_id' + id).find(":selected").val();

        $.ajax({
            type: 'GET',
            url: "{{ route('check.available.quentity') }}",
            data: {
                avl_qty,
                order_qyt,
                product_id,
                uom_id,
                batch_no
            },
            success: function(data) {

                if (data.store_stock_msg) {
                    alert(data.store_stock_msg)
                    const const_avl_qty = $('#const_avl_qty' + id).val();
                    $('#order_qyt' + id).val(' ');
                    $('#avl_qty' + id).val(const_avl_qty);
                    $('#subtotal' + id).val(0);
                    calculationGrandTotal();
                    calculateTotal(em);
                    return;
                }

                if (data.err_msg) {
                    alert(data.err_msg)
                    const const_avl_qty = $('#const_avl_qty' + id).val();
                    $('#order_qyt' + id).val('');
                    $('#avl_qty' + id).val(const_avl_qty);
                    $('#subtotal' + id).val(0);
                    calculationGrandTotal();
                    calculateTotal(em);
                } else {
                    $('#avl_qty' + id).val(data);
                    calculateTotal(em);
                }
            },
            error: function(err) {

            }
        });
        // console.log($(em));

    }
</script>



<script>
    /*
     * Apply Return Invoice
     * @return Apply Return Invoice
     */

    function apply_returnInvoice() {

        // var invoice = $(em).val();
        var invoice = $('#apply_returnInvoice_no').val();
        $.ajax({
            type: 'GET',
            url: "{{ route('apply.return-invoice') }}",
            data: {
                invoice
            },
            success: function(data) {
                var return_invoice = $('#return_invoice').val(data);
                var grand_total = $('#grand_total').val() ? $('#grand_total').val() : 0;
                var return_invoice = $('#return_invoice').val() ? $('#return_invoice').val() : 0;
                var amount = grand_total - return_invoice;

                if (amount < 0) {
                    $('#return_amount_title').html('Payable');
                    $('#invoice_return_amount').val(Math.abs(amount));
                } else {
                    $('#return_amount_title').html('Receivable');
                    $('#invoice_return_amount').val(Math.abs(amount));
                }

            },
            error: function(err) {

            }
        });
    }
</script>



<script>
    /*
     * Apply Card Item
     * @return Apply Card 
     */

    function apply_Card() {
        var card_no = $('#membership_card').val();
        $.ajax({
            type: 'GET',
            url: "{{ route('apply.card_information') }}",
            data: {
                card_no
            },
            success: function(data) {
                console.log(data);
            },
            error: function(err) {

            }
        });
    }
</script>




@endpush