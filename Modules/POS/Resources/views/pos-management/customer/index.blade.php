@extends('dboard.index')
@section('title', 'Customer List')
@push('styles')
<style>
  /*to aviod extra margin padding for heading tags and p tag*/
  p {
    padding: 0;
    margin: 0;
  }

  h5 {
    padding: 0;
    margin: 0;
  }

  .fa-plus-circle {
    font-size: 18px !important;
  }

  .customer_image_style {
    height: 50px;
    width: 50px;
    border-radius: 10px;
  }
</style>
@endpush
@section('dboard_content')


<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Customer List</h2>
          </div><!-- end .col-md-6 -->

          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{ route('customer.create') }}">Create New Customer</a>
          </div><!-- end .col-md-3 -->
        </div><!-- end .row -->
        <hr>


        <!-- body -->
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover bg-white" id="invoiceDataTable">
                <thead>
                  <tr>
                    <td width="5%">#SL</td>
                    <td width="20%">Customer Name</td>
                    <td width="20%">Customer Image</td>
                    <td width="15%">Customer Mobile No.</td>
                    <td width="25%">Customer Location</td>
                    <td width="10%">Action</td>
                  </tr>
                </thead>
                <tbody>
                  @if (!empty($customers))
                  @foreach ($customers as $row)
                  <tr>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->customer_name }}</td>
                    <td>
                      <img class="customer_image_style" src="{{ asset('customer_image' . '/' . $row->image) }}" alt="customer_image">
                    </td>
                    <td>{{ $row->mobile_no }}</td>
                    <td>{{ $row->location }}</td>
                    <td>
                      <div class="d-flex justify-content-around align-items-center">
                        <a class="btn edit-btn" href="{{ route('customer.edit', $row->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <a class="btn details-btn" href="{{ route('customer.show', $row->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- end .row -->
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- .row -->
@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  $('#invoiceDataTable').DataTable();
</script>

@endpush