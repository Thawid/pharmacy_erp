@extends('dboard.index')
@section('title', 'Update Customer')
@push('styles')
    <style>
      i {
          display: inline-block;
          font-size: 18px;
          line-height: 18px;
      }
      .thumb {
          margin: 10px 5px 0 0;
          height: 100px;
          width: 120px;
          border-radius: 15px;
      }
    </style>
@endpush
@section('dboard_content')

<!-- title-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
            <div class="col-md-6">
                <h2 class="title-heading">Update Customer</h2>
            </div><!-- end .col-md-6 -->

            <div class="col-md-6 text-right">
                <a class="btn index-btn" href="{{ route('customer.index') }}">Back</a>
            </div><!-- end .col-md-6 -->
        </div><!-- end .row -->
        <hr>

        <!-- form -->
        <form action="{{ route('customer.update', $customer->id) }}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          
          <div class="create-customer-area">
            <div class="row justify-content-center">
              <div class="col-md-6">
                <!-- customer-name -->
                <div class="form-group">
                    <label for="customer_name">Customer Name</label>
                    <input type="text" name="customer_name" id="customer_name" class="form-control"
                        value="{{ $customer->customer_name }}">
                </div><!-- end .customer-name -->

                <!-- membership-card -->
                <div class="form-group">
                    <label for="membership_card">Membership Card</label>
                    <input type="text" name="membership_card" id="membership_card"
                        class="form-control" value="{{ $customer->membership_card }}"
                        onkeypress="return (event.charCode !=8 && event.charCode ==0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))">
                </div><!-- end .membership-card -->

                
                <!-- customer_image -->
                <div class="form-group">
                    <label for="customer_image">Image</label>
                    <input type="file" name="customer_image" id="customer_image" class="form-control">
                    <div id="thumb-output"></div>
                </div><!-- customer-image -->
              </div><!-- end .col-md-6 -->

              <div class="col-md-6">
                <!-- mobile-no -->
                <div class="form-group">
                    <label for="mobile_number">Mobile No.</label>
                    <input type="text" name="mobile_number" id="mobile_number" class="form-control"
                        value="{{ $customer->mobile_no }}">
                </div><!-- end .mobile-no -->

                <!-- location -->
                <div class="form-group">
                  <label for="location">Location</label>
                  <input type="text" name="location" id="location" class="form-control" value="{{ $customer->location }}">
                </div><!-- end .location -->

                <!-- submit-button -->
                <div class="form-group text-right">
                  <button type="submit" class="form-control btn index-btn mt-4">Update</button>
                </div><!-- end.update-button -->
              </div><!-- end.col-md-6 -->
            </div><!-- end .row -->
          </div><!-- end .create-customer-area -->
        </form>
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->
@endsection
@push('post_scripts')
    <!-- single image preview -->

    <script>
        $(document).ready(function() {
            $('#customer_image').on('change', function() { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {

                    var data = $(this)[0].files; //this file data

                    $.each(data, function(index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                                .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function(file) { //trigger function on successful read
                                return function(e) {
                                    var img = $('<img/>').addClass('thumb').attr('src',
                                        e.target.result); //create image element
                                    $('#thumb-output').append(
                                        img); //append image to output element
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });

                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>
@endpush
