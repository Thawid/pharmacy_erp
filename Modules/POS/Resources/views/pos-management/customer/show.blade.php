@extends('dboard.index')
@section('title', 'Customer Profile')
@push('styles')
    <style>

        .customer_image_style {
            height: 200px;
            width: 500px;
            border-radius: 10px;
        }

    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Customer Information</h2>
                        </div><!-- end .col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn create-btn" href="{{ route('customer.index') }}">Back</a>
                        </div><!-- end .col-md-6 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- body -->
                    <div class="row">
                        <!-- customer-image -->
                        <div class="col-md-6">
                            <div class="customer-image">
                                <img class="customer_image_style"
                                    src="{{ asset('customer_image' . '/' . $customer->image) }}" alt="customer_image">
                            </div>
                        </div><!-- end .col-md-6 -->

                        <!-- customer-information -->
                        <div class="col-md-6">
                            <div class="customer-information">
                                @if (isset($customer->customer_name))
                                    <p class="mb-3">Customer Name: <span class="p-1 badge-info rounded">
                                            {{ $customer->customer_name }}</span>
                                    </p>
                                @endif

                                @if (isset($customer->mobile_no))
                                    <p class="mb-3">Customer Mobile No: <span
                                            class="p-1 badge-info rounded">{{ $customer->mobile_no }}</span> </p>
                                @endif

                                @if (isset($customer->membership_card))
                                    <p class="mb-3">Customer Memebership Card: <span
                                            class="p-1 badge-info rounded">{{ $customer->membership_card }}</span> </p>
                                @endif

                                @if (isset($customer->location))
                                    <p class="mb-3">Location: <span class="p-1 badge-info rounded">{{ $customer->location }}</span>
                                    </p>
                                @endif

                                @if (isset($customer->total_order))
                                    <p class="mb-3">Total Order: <span
                                            class="p-1 badge-info rounded">{{ $customer->total_order }}</span>
                                    </p>
                                @endif

                                @if (isset($customer->total_expense))
                                    <h4 class="mb-3">Total Expense: <span
                                            class="p-1 badge-info rounded">{{ $customer->total_expense }}</span> </h4>
                                @endif
                            </div>
                        </div>
                    </div><!-- end .row -->
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- .row -->
@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#invoiceDataTable').DataTable();
    </script>

@endpush
