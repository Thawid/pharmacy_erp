
@extends('dboard.index')
@section('title', 'Pending Store Deposite Receive')
@push('styles')
    <style>

        .pending-store-deposite-receive {
            border-radius: 8px;
        }

        div.dataTables_wrapper div.dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            font-size: 0;
        }

        div.dataTables_wrapper div.dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            font-size: 0;
        }


        div.dataTables_wrapper div.dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
            margin-left: 10px;
        }

        table.dataTable td:last-child i {
            font-size: 16px;
            margin: -1px;
        }

        ul.title-heading {
            display: flex;
            padding: 0;
            margin: 0;
        }

        ul.title-heading li {
            list-style: none;
            margin-right: 15px;
        }

        ul.title-heading li a {
            text-decoration: none;
            position: relative;
        }

        ul.title-heading li a:hover {
            text-decoration: none;
        }

        ul.title-heading li a::before {
            position: absolute;
            content: "";
            height: 2px;
            width: 0;
            background: #0088FF;
            bottom: -5px;
            left: 0;
            right: 0;
            margin: auto;
            transition: .3s;
        }

        ul.title-heading li:hover a::before {
            width: 100%;
        }

        ul.title-heading li.active a {
            color: #0088FF;
            transition: .3s;
        }

        ul.title-heading li.active a::before {
            width: 100%;
        }
        /* .dataTable  thead {
            background: #294A65;
            color: white;
        }
        .dataTable  thead th{
            border: 1px solid transparent;
        } */

    </style>
@endpush
@section('dboard_content')

    <!-- data-area -->
    <form action="">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Product type</label>
                            <select name="" id="" class="form-control">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                                <option value="">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Product Category</label>
                            <select name="" id="" class="form-control">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                                <option value="">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Select Product</label>
                            <input type="text" name="date" class="form-control">
                        </div>
                    </div>
                      <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Date </label>
                            <input type="date" name="date" class="form-control">
                        </div>
                    </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Sales Counter</label>
                                <select name="" id="" class="form-control">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                </select>
                            </div>
                        </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Report Type</label>
                            <input type="text" name="date" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Manufacturer</label>
                            <select name="" id="" class="form-control">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                                <option value="">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Vendor</label>
                            <select name="" id="" class="form-control">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                                <option value="">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="">
                            <label for="">&#8203;</label>
                            <button type="submit" class="btn filter-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="tile pending-store-deposite-receive">
                    <div class="tile-body">
                  
                        
                        <div class="row">
                           

                            <!-- data table -->
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover"
                                    id="deposit-receive-table">
                                    <thead>
                                        <tr>
                                            <th width="5%" style="border-right-width: 4px;">#</th>
                                            <th width="" style="border-right-width: 4px;">Product Details</th>
                                            <th width="" style="border-right-width: 4px;">Generic</th>
                                            <th width="" style="border-right-width: 4px;">Manufacturer</th>
                                            <th width="">Sold Qty</th>
                                            <th width="">Return Qty</th>
                                            <th width="">UOM</th>
                                            <th width="">Discount</th>
                                            <th width="15%">Total Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <h3>Banani Store</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td class="align-item-center">
                                                <h3>Deposit ID: 464684</h3>
                                            </td>

                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            <td>
                                                <div class="d-flex justify-content-around align-items-center">
                                                    <a href="#" class="btn details-btn mr-2">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <h3>Banani Store</h3>
                                                <p>Create Date: 22 Aug 2021</p>
                                            </td>
                                            <td class="align-item-center">
                                                <h3>Deposit ID: 464684</h3>
                                            </td>

                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            <td>Receive Pending</td>
                                            
                                            <td>
                                                <div class="d-flex justify-content-around align-items-center">
                                                    <a href="#" class="btn details-btn mr-2">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!--  end .table -->
                        </div><!-- end .row -->
                    </div><!-- end .tile-body -->
                </div><!-- end .tile -->
            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </form>


@endsection

@push('post_scripts')
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $('#deposit-receive-table').DataTable();
    </script>

    <script>
        $('.dataTables_wrapper div.dataTables_filter input').attr('placeholder', 'Search');
    </script>

    <script>
        $(document).on('click', 'ul li', function (){
            $(this).addClass('active').siblings().removeClass('active');
        });
    </script>
@endpush
