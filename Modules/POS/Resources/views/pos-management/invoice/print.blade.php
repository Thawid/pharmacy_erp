


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alesha Pharma</title>
</head>
<body>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Yanone+Kaffeesatz:wght@300;400;500&display=swap');
        body{
            font-family: 'Yanone Kaffeesatz', sans-serif;
        }
        #invoice {
                width: 320px;
                margin: 0 auto;
                font-size: 10px;
                background: #fff;
                padding: 20px 12px;
            }
        #invoice p {
            display: block;
            margin: 5px 0;
        }
        .logo-wrapper{
            height: 100px;
            margin: auto;
            text-align: center;
        }
        .logo-wrapper img {
            height: auto;
            width: 100%;
        }

        .invoice-title-wrapper {
            overflow-x: hidden;
            text-align: center;
        }
        .invoice-title-wrapper> p:first-child{
            font-size:16px;
        }

        .invoice-product-table-wrapper {
            overflow-x: hidden;
        }

        .invoice-calculation-area {
            overflow-x: hidden;
        }
        .invoice-calculation-area > div{
            display: flex;
            justify-content: flex-end;
         
        }
       .invoice-details-wrapper>div:first-child{
            display: flex;
            justify-content: space-between;
       }
        #invoice .separator{
            overflow: hidden;
        }
        #invoice .separator2{
            margin:0;
            line-height:0
        }
        thead th {
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
        }

        tbody {
            font-size: 15px;
        }

        table th, table td {
            padding: 2px !important;
            font-size: 10px;
            text-align: left;
        }
       
        .flex-item{
            display: flex;
            justify-content: flex-end
        }
        .flex-item > div:first-child{
            margin-right:20px;
            text-align: right
        }
        .flex-item > div:last-child{
            text-align: right;
            margin-right:5px;
            min-width: 18%;
        }
        .bar-code-wrapper span{
            width: 100% !important;
            height: 100% !important;
            margin-top:6px;
        }
        .bar-code-wrapper img {
            height: 100%;
            width:100%
        }
        #invoice .notes > p:nth-child(1), 
        #invoice .notes > p:nth-child(5),
        #invoice .notes > p:nth-child(6){
            text-align: center
        }
        #invoice .notes > p:first-child{
            font-size: 15px;
            margin-top:8px
        }
        #invoice .notes > p:nth-child(5){
            margin-top: 10px;
        }
        @media  print {
            * {
                font-size:16px!important;
            }
            td,th {padding: 5px 0;}
            .hidden-print {
                display: none !important;
            }
            @page  { margin: 0; } body { margin: 0.5cm; margin-bottom:1.6cm; } 
        }
  
    </style>
    <div id="invoice" class="printable-area" >
        <div class="logo-wrapper">
            <img src="{{ asset('img/alesha-pharmacy.png') }}" alt="logo"> 
        </div>
        <div class="invoice-title-wrapper">
            <p>{{ $store_invoice->storeInfo->name??' ' }}</p>
            <p>Address: {{ $store_invoice->storeInfo->address??' ' }}</p>
            <p>Mobile: {{ $store_invoice->storeInfo->phone_no??' ' }}</p>
            <p>www.aleshapharmacy.com</p>
            <p>*********************************************************</p>
        </div>
        <div class="invoice-details-wrapper">
            <div>
                <span>Invoice No: {{ $invoice->invoice_no }}</span>
                <span>Date: {{date('d-m-Y h:i A', strtotime($invoice->created_at))}}</span>
            </div>
            <p>Customer Info: {{ isset($invoice->customer->mobile_no)?$invoice->customer->mobile_no:"N/A" }} |
                {{ isset($invoice->customer->customer_name)?$invoice->customer->customer_name:" " }}</p>
            <p>Counter No: {{ $invoice->counter_no }}</p>
            <p>Employee Name: {{ $invoice->employee->name??' ' }}</p>
            <p>POS Machine:  {{ $invoice->pos_machine_id }}</p>
        </div>
        <p class="separator">***************************************************************************</p>
        <div class="invoice-product-table-wrapper">
            <table class="table table-striped table-bordered table-hover bg-white">
                <thead>
                <tr>
                    <!-- <th style="width: 5%">SL</th>
                    <th style="width: 30%">Item</th>
                    <th style="width: 15%">Unit Price</th>
                    <th style="width: 10%">Qty</th>
                    <th style="width: 10%">Dis%</th>
                    <th style="width: 20%">Sub Total</th> -->


                    <th>SL</th>
                    <th>Item</th>
                    <th>Unit Price</th>
                    <th>Uom</th>
                    <th>Qty</th>
                    <th>Dis%</th>
                    <th>Vat & Tax</th>
                    <th>Sub Total</th>
                </tr>
                </thead>
                <tbody>
                @if (!empty($pos_detials))
                @foreach ($pos_detials as $key=>$row)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{ $row['product'] }}</td>
                    <td>{{ $row['per_unit_price'] }}</td>
                    <td>{{ $row['uom'] }}</td>
                    <td>{{ $row['order_qty'] }}</td>
                    <td>{{ $row['discount'] }}</td>
                    <td>{{ $row['vat_amount'] }}</td>
                    <td>{{ $row['sub_total'] }}</td>
                </tr>
                @endforeach
                @endif
                </tbody>
            </table>
            <p>************************************************************************</p>
        </div>
        <div class="invoice-calculation-area">
            <div class="flex-item">
                <div>
                <p>- Sale Discount: </p>
                    <p>- Total Discount: </p>
                    <p>+ Total Tax: </p>
                    <p>+ Other Cost: </p>
                    <p>Payment Method: </p>
                </div>
                <div>
                <p>{{ $invoice->sale_discount }} BDT</p>
                    <p>{{ $invoice->total_discount }} BDT</p>
                    <p>{{ $invoice->total_tax }} BDT</p>
                    <p>{{ $invoice->other_cost }} BDT</p>
                   
                    <p>Cash</p>
                </div>
            </div>
            <p class="separator2">-------------------------------------------------------------------------------------------------------------------------</p>
            <div class="flex-item">
                <div>
                    <p>Grand Total: </p>
                    <p>Payment Method</p>
                    <p>On Cash:</p>
                    <p>On Card:</p>
                    <p>M.Banking:</p>
                </div>
                <div>
                    <p>{{ $invoice->grand_total }}</p>
                    <p>&nbsp;</p>
                    <p>0.0</p>
                    <p>0.0 BDT</p>
                    <p>0.0 BDT</p>
                </div>
            </div>
            <p class="separator2">-------------------------------------------------------------------------------------------------------------------------</p>
            <div class="flex-item">
                <div>
                    <p>Total Paid Amount: </p>
                    <p>+ Round Amount:</p>
                    <p>Change Amount:</p>
                </div>
                <div>
                    <p>{{ $invoice->paid_amount }}  BDT</p>
                    <p>0.0 BDT</p>
                    <p>{{ $invoice->return_amount }} BDT</p>
                </div>
            </div>

            @php
              $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
            @endphp

            <div class="bar-code-wrapper w-100">
                <span>{!! $generator->getBarcode($invoice->invoice_no, $generator::TYPE_CODE_128) !!}</span>
            </div>
        </div>
        <div class="notes">
            <p>Note:</p>
            <p> 1 &rpar; All heat sensitive medicines, sugar test strip and the cut strip of the medicine are non-refundable.</p>
            <p> 2 &rpar; When buying medicines, check the quantity and expiration date of the medicine at your own risk.</p>
            <p> 3 &rpar; Purchased goods are changeable within 72 hours and must bring alogn sales slip.</p>
            <p>Thank you for using our servises. Please do come again.</p>
            <p>***********************************</p>
        </div>
    </div>
</body>
    <script>
        window.print();
        setTimeout(function(){
           window.history.back();
        },300);
    </script>
</html>