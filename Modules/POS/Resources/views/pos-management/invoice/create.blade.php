@extends('dboard.index')
@section('title', 'Sales Invoice')
@push('styles')
<style>
    #invoice {
        width: 270px;
        margin: auto;
        font-size: 12px;
        overflow: auto;
    }

    #invoice span {
        display: block;
    }

    .invoice-title-wrapper {
        overflow-x: hidden;
    }

    .invoice-product-table-wrapper table {
        font-size: 12px;
    }

    .invoice-product-table-wrapper {
        overflow-x: hidden;
    }

    thead th {
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
    }

    tbody {
        font-size: 10px;
    }

    table th, table td {
        padding: 2px !important;
    }

    .invoice-calculation-area {
        overflow-x: hidden;
    }

    .logo-wrapper img {
        height: 50px;
        width: 120px;
    }

    .bar-code-wrapper img {
        height: 50px;
        width: 120px;
    }

</style>
    
@endpush
@section('dboard_content')

<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">

                <!-- title-area -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Sales Invoice</h2>
                    </div><!-- end .col-md-9 -->
                </div><!-- end .row -->
                <hr>

                <div id="invoice">
                    <div class="row justify-content-center m-auto">
                        <div class="col-md-12">
                            <div class="invoice-title-wrapper">
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <h5>Alesha Pharmacy - 01</h5>
                                        <p>Address: Ring Road, Shamoly</p>
                                        <p>Mobile: 017768686</p>
                                        <p>www.aleshapharmacy.com</p>
                                        <span>*********************************************************</span>
                                    </div>
                                </div><!-- end .row -->
                            </div>

                            <div class="invoice-details-wrapper">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span>Invoice No: 6879697</span>
                                        <span>Date: 22 July 2021 | 3.00 pm</span>
                                        <span>Customer Info: 0326879697 | nabil</span>
                                        <span>Counter No: 03</span>
                                        <span>Employee Name: Rahul Ahmed(BS3223)</span>
                                        <span>POS Machine: 78243</span>
                                    </div>
        
                                    {{-- <div class="col-md-4">
                                        <span>22 July 2021 | 3.00 pm</span>
                                    </div> --}}
                                </div>
                            </div>

                            <div class="invoice-product-table-wrapper">
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-bordered table-hover bg-white">
                                            <thead>
                                                <tr>
                                                    <th>SL</th>
                                                    <th>Item</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Dis%</th>
                                                    <th>Sub Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>01</td>
                                                    <td>Napa Extended 665mg UOM: PC</td>
                                                    <td>10</td>
                                                    <td>05</td>
                                                    <td>0</td>
                                                    <td>50.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <span>*********************************************************</span>
                                    </div>
                                </div>
                            </div>

                            <div class="invoice-calculation-area">
                                <div class="row text-right">
                                    <div class="col-md-6">
                                        <span>Sale Discount: </span>
                                        <span>Total Discount: </span>
                                        <span>Total Tax: </span>
                                        <span>Other Cost: </span>
                                        <span>Grand Total: </span>
                                        <span>Paid Amount: </span>
                                        <span>Return Amount: </span>
                                        <span>Payment Method: </span>
                                    </div>

                                    <div class="col-md-6">
                                        <span>100 BDT</span>
                                        <span>100 BDT</span>
                                        <span>100 BDT</span>
                                        <span>100 BDT</span>
                                        <span>100 BDT</span>
                                        <span>100 BDT</span>
                                        <span>100 BDT</span>
                                        <span>Cash</span>
                                    </div>
                                </div>
                                <span class="text-center mt-3">*********************************************************</span>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="logo-wrapper">
                                            <img src="{{ asset('img/alesha-pharmacy.png') }}" alt="logo">
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="bar-code-wrapper">
                                            <img src="{{ asset('img/barcode.png') }}" alt="barcode">
                                        </div>
                                    </div>
                                </div>
                                <span class="text-center mt-3">******************* Thank You ******************</span>
                            </div><!-- end .invoice-calculatio-area -->
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                </div><!-- end # -->
                <hr>
            </div><!-- end .tile-body -->
        </div><!-- tile -->
    </div><!-- col-md-12 -->
    </div><!-- row -->
@endsection

@push('post_scripts')
@endpush