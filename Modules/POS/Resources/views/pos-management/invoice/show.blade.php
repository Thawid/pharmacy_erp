@extends('dboard.index')
@section('title', 'Sales Invoice')
@push('styles')
<style>
    .main-content {
        background: #fff;
        padding: 30px 0;
    }

    #invoice {
        width: 320px;
        margin: 0 auto;
        font-size: 16px;
        overflow: auto;
        background: #f8f9fb;
        padding: 20px 12px;
    }

    #invoice p {
        display: block;
    }

    .invoice-title-wrapper {
        overflow-x: hidden;
        text-align: center;
    }

    .invoice-product-table-wrapper table {
        font-size: 25px;
    }

    .invoice-product-table-wrapper {
        overflow-x: hidden;
    }

    thead th {
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
    }

    tbody {
        font-size: 15px;
    }

    table th,
    table td {
        padding: 2px !important;
        font-size: 20;
    }

    .invoice-calculation-area {
        overflow-x: hidden;
    }

    .invoice-calculation-area>div {
        display: flex;
        justify-content: space-between;

    }

    /* .logo-wrapper{
        width: 300px;
        height: 300px;
    } */
    .logo-wrapper img {
        height: auto;
        width: 100%;
    }

    .bar-code-wrapper img {
        height: 50px;
        width: 120px;
    }

    #button-area i {
        font-size: 18px;
    }


    @media print {
        * {
            font-size: 21px;

        }

        td,
        th {
            padding: 5px 0;
        }

        .hidden-print {
            display: none !important;
        }

        @page {
            margin: 0;
        }

        body {
            margin: 0.5cm;
            margin-bottom: 1.6cm;
        }
    }
</style>

@endpush
@section('dboard_content')

{{-- <!-- title-area -->
    <div class="row align-items-center">
        <div class="col-md-6">
            <h2 class="title-heading">Sales Invoice</h2>
        </div><!-- end .col-md-9 -->
    </div><!-- end .row -->
    <hr> --}}
<div id="button-area" class="text-right">
    <a href="{{route('invoice.print',$invoice->id)}}" class="btn btn-outline-primary mr-3">Prints
        <i class="pl-2 fa fa-print"></i>
    </a>
</div>

<div id="invoice" class="printable-area">
    <div class="">
        <div class="logo-wrapper">
            <img src="{{ asset('img/alesha-pharmacy.png') }}" alt="logo">
        </div>
    </div>

    <div class="invoice-title-wrapper">
        <h5>{{ $store_invoice->storeInfo->name??' ' }}</h5>
        <p>Address: {{ $store_invoice->storeInfo->address??' ' }}</p>
        <p>Mobile: {{ $store_invoice->storeInfo->phone_no??' ' }}</p>
        <p>http://aleshapharmacy.com</p>
        <p>*********************************************************</p>
    </div>

    <div class="invoice-details-wrapper">
        <span>Invoice No: {{ $invoice->invoice_no }}</span>
        <span>Date: {{date('d-m-Y h:i A', strtotime($invoice->created_at))}}</span>
        <p>Customer Info: {{ isset($invoice->customer->mobile_no)?$invoice->customer->mobile_no:"N/A" }} |
            {{ isset($invoice->customer->customer_name)?$invoice->customer->customer_name:" " }}
        </p>
        <p>Counter No: {{ $invoice->counter_no }}</p>
        <p>Employee Name: {{ $invoice->employee->name??' ' }}</p>
        <p>POS Machine: {{ $invoice->pos_machine_id }}</p>
    </div>

    <div class="invoice-product-table-wrapper">
        <table class="table table-striped table-bordered table-hover bg-white">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Item</th>
                    <th>Unit Price</th>
                    <th>Uom</th>
                    <th>Qty</th>
                    <th>Dis%</th>
                    <th>Vat & Tax</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($pos_detials))
                @foreach ($pos_detials as $key=>$row)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{ $row['product'] }}</td>
                    <td>{{ $row['per_unit_price'] }}</td>
                    <td>{{ $row['uom'] }}</td>
                    <td>{{ $row['order_qty'] }}</td>
                    <td>{{ $row['discount'] }}</td>
                    <td>{{ $row['vat_amount'] }}</td>
                    <td>{{ $row['sub_total'] }}</td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <p>************************************************************************</p>
    </div>

    <div class="invoice-calculation-area">
        <div>
            <div>
                <p>Sale Discount: </p>
                <p>Total Discount: </p>
                <p>Total Tax: </p>
                <p>Other Cost: </p>
                <p>Grand Total: </p>
                <p>Paid Amount: </p>
                <p>Change Amount: </p>
                <p>Payment Method: </p>
            </div>

            <div>
                <p>{{ $invoice->sale_discount }} BDT</p>
                <p>{{ $invoice->total_discount }} BDT</p>
                <p>{{ $invoice->total_tax }} BDT</p>
                <p>{{ $invoice->other_cost }} BDT</p>
                <p>{{ $invoice->grand_total }} BDT</p>
                 <p>{{ $invoice->paid_amount }} BDT</p>
                 <p>{{ $invoice->return_amount }} BDT</p>
                <p>Cash</p>
            </div>
        </div>
        <p>*********************************************************</p>
        <div class="logo-bar_code-wrapper">
            <div class="bar-code-wrapper" style="width: 100%">
                <!-- <img  style="width: 100%" src="{{ asset('img/barcode.png') }}" alt="barcode"> -->
            </div>
        </div>
        <p class="text-center mt-3">********** Thank You **********</p>
    </div>
</div>


@endsection

@push('post_scripts')
<!-- print area plugin -->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.printarea.js') }}"></script>

{{-- script to print a specific area --}}
<script>
    $(function() {
        $("#print_btn").on('click', function() {

            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("#printable-area").printArea(options);
        });
    });
</script>
@endpush