@extends('dboard.index')
@section('title', 'Invoice List')
@push('styles')
<style>
  /*to aviod extra margin padding for heading tags and p tag*/
  p {
    padding: 0;
    margin: 0;
  }

  h5 {
    padding: 0;
    margin: 0;
  }

  .fa-plus-circle {
    font-size: 18px !important;
  }

  .modal {
    padding: 0 !important;
    /* override inline padding-right added from js */
  }

  .modal .modal-dialog {
    width: 100%;
    max-width: none;
    height: 100%;
    margin: 0;
  }

  .modal .modal-content {
    height: 100%;
    border: 0;
    border-radius: 0;
  }

  .modal .modal-body {
    overflow-y: auto;
  }
</style>
@endpush
@section('dboard_content')


<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title-area -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Invoice List</h2>
          </div><!-- end .col-md-9 -->

          <div class="col-md-6 text-right">
            <a class="btn create-btn mr-2" href="{{ route('invoice.create') }}">Invoice</a>
            <a class="btn index-btn" href="{{ route('pos.create') }}">Goto Quick Sell</a>
          </div><!-- end .col-md-3 -->
        </div><!-- end .row -->
        <hr>

        <!-- data-area -->
        <div class="row">
          <div class="col-md-12">
            <div class="row mt-4">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover bg-white" id="invoiceDataTable">
                    <thead>
                      <tr>
                        <th width="10%">#SL</th>
                        <th width="30%">Invoice Details</th>
                        <th width="30%">Customer Details</th>
                        <th width="20%">Total Amount</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if (!empty($point_of_sales))
                      @foreach ($point_of_sales as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>
                          <div class="">
                            <p><strong>{{ $row->invoice_no }}</strong></p>
                            <p>{{ $row->createdat }}</p>
                          </div>
                        </td>
                        <td>
                          <div class="">
                            <p class="mb-1"><strong>{{ isset($row->customer->customer_name)?$row->customer->customer_name:'N/A' }}</strong></p>
                            @if(isset($row->customer->mobile_no))
                            <span class="badge-info p-1 mt-1 rounded">{{ $row->customer->mobile_no }}</span>
                            @endif
                          </div>
                        </td>
                        <td> BDT {{ $row->grand_total }}</td>
                        <td>
                          <div class="">
                            <a class="btn details-btn" href="{{ route('invoice.show', $row->id) }}"><i class="fa fa-eye"></i></a>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div><!-- end .row -->
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->



        <!-- end .modal -->
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- .row -->
@endsection

@push('post_scripts')
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<!-- print area plugin -->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.printarea.js') }}"></script>
<script type="text/javascript">
  $('#invoiceDataTable').DataTable();
</script>

{{-- script to print a specific area --}}
<script>
  $(function() {
    $("#print_btn").on('click', function() {

      var mode = 'iframe'; //popup
      var close = mode == "popup";
      var options = {
        mode: mode,
        popClose: close
      };
      $("div.printable-area").printArea(options);
    });
  });
</script>
@endpush