@extends('dboard.index')
@section('title', 'POS Return')
@push('styles')

@endpush
@section('dboard_content')

<div class="tile">
    <!-- title -->
    <div class="row align-items-center">
        <div class="col-md-12">
            <h1 class="title-heading">Point of Sale Return</h1>
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
    <hr>

    <form action="{{route('product.return')}}" method="post">
        @csrf
        <!-- pos return search-area -->
        <div class="row align-items-center">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="search_invoice">Search Invoice</label>
                    <input type="text" class="form-control select2 w-100" name="invoice_no" id="" />
                </div><!-- end.form-group -->
            </div><!-- end.col-md-5 -->

            <div class="col-md-4">
                <button class="btn create-btn mt-2">Submit</button>
            </div><!-- end.col-md-4 -->
        </div><!-- end.row -->
    </form>
    <hr>

    @if(isset($point_of_sales) && !empty($point_of_sales))

    <form action="{{route('sale.return-store')}}" method="post">
        @csrf
    <!-- pos return content -->
    <div class="row">
        <div class="col-md-8">
            <div class="information-area">
                <div class="row">
                    <!-- customer-info -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="invoice_no">Invoice No</label>
                            <input type="text" placeholder="invoice no." value="{{ $point_of_sales->invoice_no }}" class="form-control" name="invoice_no" readonly>
                            <input type="hidden" value="{{ $point_of_sales->id}}" class="form-control" id="invoice_id" name="invoice_id" readonly>
                        </div><!-- end.form-group -->


                        <div class="form-group">
                            <label for="invoice_no">Return Invoice No</label>
                            <input type="text" placeholder="invoice no." value="{{ $invoice_no }}" class="form-control" name="return_invoice_no" readonly>
                        </div><!-- end.form-group -->

                        <div class="form-group">
                            <label for="customer_info">
                                Customer Information
                            </label>

                            <div class="customer-info">
                                <input class="form-control mb-2" id="phone_no" readonly value="{{ isset($point_of_sales->customer->mobile_no)?$point_of_sales->customer->mobile_no:"N/A" }}" type="text" placeholder="Mobile No.">
                                <input class="form-control" type="text" id="customar_name" readonly value="{{ isset($point_of_sales->customer->customer_name)?$point_of_sales->customer->customer_name:" " }}" placeholder="Name">
                                <input class="form-control" type="hidden" name="customar_id" id="customar_id" readonly value="{{ isset($point_of_sales->customer_id)?$point_of_sales->customer_id:0 }}">
                            </div><!-- end.customer-info -->
                            
                        </div><!-- end.form-group -->
                    </div><!-- end.col-md-6 -->

                    <!-- counter-info -->
                    <div class="col-md-6">
                        <div class="counter-info bg-light p-2 border rounded">
                            <p>{{date('d-m-Y h:i A', strtotime($point_of_sales->created_at))}}</p>
                            <p><strong>Counter No: </strong>{{ $point_of_sales->counter_no }}</p>
                            <p><strong>Employ Name: </strong> {{ $point_of_sales->employee->name??' ' }}</p>
                            <p><strong>POS Machine: </strong>{{ $point_of_sales->pos_machine_id }}</p>
                        </div><!-- end.counter-info -->
                    </div><!-- end.col-md-6 -->
                </div><!-- end.row -->
                <hr>
            </div><!-- end.information-area -->

            <div class="product-search-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="search">Search Product</label>
                            <select name="" id="product_search" aria-placeholder="search here" class="select2 form-control"></select>
                        </div><!-- end.form-inline -->
                    </div><!-- end.col-md-12 -->
                </div><!-- end.row -->
            </div><!-- end.product-search-wrapper -->

            <div class="generated-product-table-wrapper mt-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table-bordered table-striped table-hovered w-100">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Batch No</th>
                                        <th>Uom</th>
                                        <th>Purchase Qty</th>
                                        <th>Return Qty</th>
                                        <th width="18%">Status</th>
                                        <th>Less Price</th>
                                        <!-- <th>Return Price</th> -->
                                        <th>Action</th>
                                    </tr>
                                </thead><!-- end.thead -->

                                <tbody id="return_product_add">
                                </tbody><!-- end.tbody -->
                            </table><!-- end.table -->
                        </div><!-- end.table-responsive -->

                        <div class="card mt-4 p-2">
                            <div class="invoice-prices text-right">
                                <strong class="mb-2 d-block text-left">Return Price</strong>
                                <div class="sales-info bg-light p-3 border rounded d-flex justify-content-center w-100">
                                    <div class="w-100">
                                        <div class="d-flex align-items-center w-100 pb-2">
                                            <strong class="w-50 pr-2">Total: </strong>
                                            <input class="form-control text-right w-100" id="total_amount" name="total_amount" value="" type="number" step="any" readonly>
                                            <span class="pl-2">BDT</span>
                                        </div>

                                        <!-- <div class="d-flex align-items-center w-100 pb-2">
                                            <strong class="w-50 pr-2">Return Amount: </strong>
                                            <input class="form-control text-right w-100" id="return_amount" value="" type="number" step="any" readonly>
                                            <span class="pl-2">BDT</span>
                                        </div> -->
                                    </div>
                                </div>
                            </div><!-- end.invoice-prices -->
                        </div>

                    </div><!-- end.col-md-12 -->
                </div><!-- end.row -->
            </div><!-- end.generated-product-table-wrapper -->
        </div><!-- end.col-md-8 -->

        <div class="col-md-4">
            <div class="card p-2">
                <div class="previous-invoice">
                    <strong>Previous Invoice</strong>
                    <div class="table-responsive mt-2">
                        <table class="table-bordered table-striped table-hovered w-100">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Batch No</th>
                                    <th>UOM</th>
                                    <th>Qty</th>
                                    <th>Discount</th>
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                </tr><!-- end.tr -->
                            </thead><!-- end.thead -->

                            <tbody>
                                @if (!empty($point_of_sales->point_of_sales))
                                @foreach ($point_of_sales->point_of_sales as $key=>$row)
                                <tr>
                                    <td>{{ $row->product->product_name ?? ' ' }}</td>
                                    <td>{{ $row->batch_no}}</td>
                                    <td>{{ $row->uom->uom_name ?? ' ' }}</td>
                                    <td>{{ $row->order_qty }}</td>
                                    <td>{{ $row->discount }}</td>
                                    <td>{{ $row->per_unit_price }}</td>
                                    <td>{{ $row->sub_total - $row->discount + $row->vat_amount }}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody><!-- end.tbody -->
                        </table><!-- end.table -->
                    </div><!-- end.table-responsive -->
                </div><!-- end.previous-inovoice-->
            </div><!-- end.card -->

            <!--  -->
            <div class="card mt-4 p-2">
                <div class="invoice-prices text-right">
                    <strong class="mb-2 d-block text-left">Invoice Price</strong>
                    <div class="sales-info bg-light p-3 border rounded d-flex justify-content-center w-100">
                        <div class="w-100">
                            <div class="d-flex align-items-center w-100 pb-2">
                                <strong class="w-50 pr-2">Total Discount: </strong>
                                <input class="form-control text-right w-100" value="{{ $point_of_sales->total_discount }}" type="number" step="any" readonly>
                                <span class="pl-2">BDT</span>
                            </div>

                            <div class="d-flex align-items-center w-100 pb-2">
                                <strong class="w-50 text-right pr-2">Total Tax: </strong>
                                <input class="form-control text-right w-100" type="number" step="any" value="{{ $point_of_sales->total_tax }}" readonly>
                                <span class="pl-2">BDT</span>
                            </div>

                            <div class="d-flex align-items-center w-100 pb-2">
                                <strong class="w-50 text-right pr-2">Other Cost: </strong>
                                <input class="form-control text-right w-100" type="number" step="any" value="{{ $point_of_sales->other_cost }}" readonly>
                                <span class="pl-2">BDT</span>
                            </div>

                            <div class="d-flex align-items-center w-100">
                                <strong class="w-50 text-right pr-2">Grand Total: </strong>
                                <input class="form-control text-right w-100" id="grand_total" type="number" step="any" value="{{ $point_of_sales->grand_total }}" readonly>
                                <span class="pl-2">BDT</span>
                            </div>
                        </div>
                    </div>
                </div><!-- end.invoice-prices -->
            </div>
        </div><!-- end.col-md-4 -->
    </div><!-- end.row -->
    <hr>

    <div class="row text-right">
        <div class="col-md-12">
            <button class="btn create-btn">Submit</button>
        </div>
    </div>
    </form>
    @endif
</div><!-- end.tile -->

@endsection
@push('post_scripts')
<!-- select2 -->
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<!-- select2 configuration -->


<script>
    /*
     * Prevent deduct amount using mouse click
     * @return Product
     */

    function preventNonNumericalInput(e,em) {
        e = e || window.event;
        var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
        var charStr = String.fromCharCode(charCode);
        var em = em;

        // if(e.keyCode ==13){
        //     $('#product_search').select2('open');
        // }

        // if(e.keyCode ==32){
        //     removeProduct(em);
        //     $('#product_search').select2('open');
        // }
        
        if (!charStr.match(/^[0-9]+$/))
            e.preventDefault();
    }
</script>


<script>
    /*
     * Product Search
     * @return Product
     */
    var uniqe_id = 0;

    $(document).ready(function() {
        $("#product_search").select2({
            ajax: {
                url: "{{ route('pos.return.product.search') }}",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        invoice_id: $('#invoice_id').val(),
                        product_search: params.term
                    };
                },
                processResults: function(data, params) {
                    return {
                        results: data,
                    };
                },
            },
            placeholder: 'Search for a Product',
            minimumInputLength: 2,
            templateResult: formatRepo,
            // templateSelection: formatRepoSelection
        }).on('select2:closing', function(e) {
            var product_id = e.target.value;
            getProductData(product_id);
        });
        $('#product_search').select2('open');
    })


    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }

        this.uniqe_id = Math.floor(Math.random() * 50);
        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'></div>" +
            "<div class='select2-result-repository__uom'></div>" +
            "</div>"
        );
        // console.log(repo)
        $container.find(".select2-result-repository__title").text(repo.product_name+' ('+repo.uom_name+')');
        $container.find(".select2-result-repository__uom").text(repo.batch_no);
        return $container;
    }


    function getProductData(id) {
        
        if(!id){
            return;
        }
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url: "{{ route('sale.return.search-by-id') }}",
            data: {
                id: id
            },
            success: function(data) {
                // console.log(data);
                
                var html = ` <tr class="delete_return">
                                <td>
                                    <input class="form-control" type="text"  value="${data.product_name}" readonly>
                                    <input class="form-control pos_id" type="hidden" data-id="${uniqe_id}" id="pos_id${uniqe_id}" name="pos_id[]"  value="${data.id}" readonly>
                                    <input class="form-control batch_id" type="hidden" data-id="${uniqe_id}" id="batch_no${uniqe_id}" name="batch_no[]"  value="${data.batch_no}" readonly>
                                    <input class="form-control" name="product_id[]" data-id="${uniqe_id}" id="product_id${uniqe_id}" type="hidden"  value="${data.product_id}" readonly>
                                </td>

                                <td>
                                    <input class="form-control" type="text" value="${data.batch_no}" readonly>
                                </td>
                                <td>
                                    <input class="form-control" type="text" value="${data.uom_name}" readonly>
                                    <input class="form-control" name="uom_id[]" data-id="${uniqe_id}" id="uom_id${uniqe_id}" type="hidden" value="${data.uom_id}" readonly>
                                </td>

                                <td>
                                    <input class="form-control text-right purchase_qty${data.batch_no}${data.uom_id}" type="number"  value="${data.order_qty}" readonly>
                                </td>
                                <td>
                                    <input type="text" class="form-control return_qty return_qty${data.batch_no}${data.uom_id}" data-id="${uniqe_id}" onkeypress="preventNonNumericalInput(event,this)" data-item="${data.batch_no}${data.uom_id}" id="return_qty${uniqe_id}" name="return_qty[]" required onkeyup="checkavailqty(this);calculationTotalReturn()">
                                </td>
                                <td>
                                    <select name="return_type[]" class="select2 form-control">
                                        <option value="refund">Refund</option>
                                        <option value="replacement-refund">Replacement Refund</option>
                                        <option value="replacement-damage">Replacement Damage</option>
                                        <option value="damage_product">Damage Product</option>
                                    </select>
                                </td>
                                <td>
                                    <input class="form-control price" data-id="${uniqe_id}" id="deduct_price${uniqe_id}" name="deduct_price[]" type="number" onkeyup="calculationTotalReturn()" value="">
                                </td>

                               

                                <td>
                                    <button type="button" class="btn btn-danger" onclick="removeProduct(this)"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>`;
                $('#return_product_add').append(html);
                $('#return_qty'+uniqe_id).focus();
                

            }
        });

    }

    function calculationTotalReturn(){


        const quentity = [];
        document.querySelectorAll('.return_qty').forEach((item)=>{
            quentity.push(item.value);
        })

        const prices = []
        document.querySelectorAll('.price').forEach((item)=>{
            let itemprice = item.value?item.value:0;
            prices.push(itemprice);
        })

        const pos_id = []
        document.querySelectorAll('.pos_id').forEach((item)=>{
            pos_id.push(item.value);
        })

        const return_amount = $('#grand_total').val();

        $.ajax({
            type: 'get',
            url: "{{ route('sale.return.price') }}",
            data: {
                quentity,
                prices,
                pos_id,
                return_amount
            },
            success: function(data) {

                if(data.total_amount >= 0){
                    $('#total_amount').val(parseFloat(data.total_amount,2));
                // $('#return_amount').val(data.return_amount);
                }else{
                    $('#total_amount').val(0);
                }
                
            }
        });
    }


    function removeProduct(em) {
        $(em).closest('tr').remove();
        calculationTotalReturn();
    }


    function calculationTotalPrice(em) {
        let id = $(em).data('id');
        alert(id);
    }

    function checkavailqty(em){
        var id =$(em).data('item');
        var purchase_qty = $(`.purchase_qty${id}`).val();
        var return_qty = document.querySelectorAll(`.return_qty${id}`);
        console.log(purchase_qty)
        console.log(return_qty)

        var returnitem_qty =0;
        return_qty.forEach(item=>{
            returnitem_qty +=parseFloat(item.value);
        })
        
        
        if(purchase_qty < returnitem_qty){
            alert('Out of Quantity');
            $(em).val(' ');
        }
        
    }
</script>
@endpush