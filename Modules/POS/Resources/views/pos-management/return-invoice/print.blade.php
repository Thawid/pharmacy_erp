


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alesha Pharma</title>
</head>
<body>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Yanone+Kaffeesatz:wght@300;400;500&display=swap');
        body{
            font-family: 'Yanone Kaffeesatz', sans-serif;
        }
        #invoice {
                width: 320px;
                margin: 0 auto;
                font-size: 10px;
                background: #fff;
                padding: 20px 12px;
            }
        #invoice p {
            display: block;
            margin: 5px 0;
        }
        .logo-wrapper{
            height: 100px;
            margin: auto;
            text-align: center;
        }
        .logo-wrapper img {
            height: auto;
            width: 100%;
        }

        .invoice-title-wrapper {
            overflow-x: hidden;
            text-align: center;
        }
        .invoice-title-wrapper> p:first-child{
            font-size:16px;
        }

        .invoice-product-table-wrapper {
            overflow-x: hidden;
        }

        .invoice-calculation-area {
            overflow-x: hidden;
        }
        .invoice-calculation-area > div{
            display: flex;
            justify-content: flex-end;
         
        }
       .invoice-details-wrapper>div:first-child{
            display: flex;
            justify-content: space-between;
       }
        #invoice .separator{
            overflow: hidden;
        }
        #invoice .separator2{
            margin:0;
            line-height:0
        }
        thead th {
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
        }

        tbody {
            font-size: 15px;
        }

        table th, table td {
            padding: 2px !important;
            font-size: 10px;
            text-align: left;
        }
       
        .flex-item{
            display: flex;
            justify-content: flex-end
        }
        .flex-item > div:first-child{
            margin-right:20px;
            text-align: right
        }
        .flex-item > div:last-child{
            text-align: right;
            margin-right:5px;
            min-width: 18%;
        }
        .bar-code-wrapper{
            width: 100%;
            margin-top:6px;
        }
        .bar-code-wrapper img {
            height: 50px;
            width:100%
        }
        #invoice .notes > p:nth-child(1), 
        #invoice .notes > p:nth-child(5),
        #invoice .notes > p:nth-child(6){
            text-align: center
        }
        #invoice .notes > p:first-child{
            font-size: 15px;
            margin-top:8px
        }
        #invoice .notes > p:nth-child(5){
            margin-top: 10px;
        }
        @media  print {
            * {
                font-size:16px!important;
            }
            td,th {padding: 5px 0;}
            .hidden-print {
                display: none !important;
            }
            @page  { margin: 0; } body { margin: 0.5cm; margin-bottom:1.6cm; } 
        }
  
    </style>
    <div id="invoice" class="printable-area" >
        <div class="logo-wrapper">
            <img src="{{ asset('img/alesha-pharmacy.png') }}" alt="logo"> 
        </div>
        <div class="invoice-title-wrapper">
            <p>{{$return_invoice->store->name??''}}</p>
            <p>Address: {{$return_invoice->store->address??''}}</p>
            <p>Mobile: {{$return_invoice->store->phone_no??''}}</p>
            <p>www.aleshapharmacy.com</p>
            <p>*********************************************************</p>
        </div>
        <div class="invoice-details-wrapper">
            <div>
                <span>Return Invoice No: {{$return_invoice->return_invoice_no??''}}</span>
                <span>Ref. Invoice No: {{$return_invoice->ref_invoice_no??''}}</span>
                <span>Date: {{$return_invoice->created_at??''}}</span>
            </div>
            <p>Customer Info: {{$return_invoice->customer->customer_name??''}} |
            {{$return_invoice->customer->mobile_no??''}}</p>
            <p>Counter No: {{$return_invoice->counter_no??''}}</p>
            <!-- <p>Employee Name: {{$return_invoice->employee_id??''}}</p> -->
            <p>Employee Name: {{$return_invoice->employee->name??''}}</p>
        </div>
        <p class="separator">***************************************************************************</p>
        <div class="invoice-product-table-wrapper">
            <table class="table table-striped table-bordered table-hover bg-white">
                <thead>
                <tr>
                    <th style="width: 5%">SL</th>
                    <th style="width: 30%">Item</th>
                    <th style="width: 15%">Unit Price</th>
                    <th style="width: 10%">Qty</th>
                    <th style="width: 10%">Type</th>
                    <th style="width: 10%">Dis%</th>
                    <th style="width: 20%">Sub Total</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($return_product_details))
                @foreach($return_product_details as $row)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$row['product_name'] ?$row['product_name']:''}}</td>
                    <td>{{$row['uom_name']??' '}}</td>
                    <td>{{$row['quentity']??' '}}</td>
                    <td>{{$row['return_type']??' '}}</td>
                    <td>{{$row['return_price']??' '}}</td>
                    <td>{{$row['total_amount']??' '}}</td>
                </tr>
                @endforeach
                @endif
                </tbody>
            </table>
            <p>************************************************************************</p>
        </div>
        <div class="invoice-calculation-area">
            <!-- <div class="flex-item">
                <div>
                    <p>- Sale Discount: </p>
                    <p>- Total Discount: </p>
                    <p>+ Total Tax: </p>
                    <p>+ Other Cost: </p>
                </div>
                <div>
                    <p>0.0 BDT</p>
                    <p>90.0 BDT</p>
                    <p>10.0 BDT</p>
                    <p>0.0 BDT</p>
                </div>
            </div> -->
            <p class="separator2">-------------------------------------------------------------------------------------------------------------------------</p>
            <div class="flex-item">
                <div>
                    <p>Grand Total: </p>
                    <!-- <p>Payment Method</p>
                    <p>On Cash:</p>
                    <p>On Card:</p>
                    <p>M.Banking:</p> -->
                </div>
                <div>
                    <p>{{$return_invoice->total_amount}} BDT</p>
                    <!-- <p>&nbsp;</p>
                    <p>1500.0 BDT</p>
                    <p>0.0 BDT</p>
                    <p>0.0 BDT</p> -->
                </div>
            </div>
            <p class="separator2">-------------------------------------------------------------------------------------------------------------------------</p>
            <!-- <div class="flex-item">
                <div>
                    <p>Total Paid Amount: </p>
                    <p>+ Round Amount:</p>
                    <p>Change Amount:</p>
                </div>
                <div>
                    <p>1500.0 BDT</p>
                    <p>0.0 BDT</p>
                    <p>280.0 BDT</p>
                </div>
            </div> -->

            @php
              $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
            @endphp
            <div class="bar-code-wrapper">
            <span>{!! $generator->getBarcode($return_invoice->return_invoice_no, $generator::TYPE_CODE_128) !!}</span>
            </div>
        </div>
        <div class="notes">
            <p>Note:</p>
            <p> 1 &rpar; All heat sensitive medicines, sugar test strip and the cut strip of the medicine are non-refundable.</p>
            <p> 2 &rpar; When buying medicines, check the quantity and expiration date of the medicine at your own risk.</p>
            <p> 3 &rpar; Purchased goods are changeable within 72 hours and must bring alogn sales slip.</p>
            <p>Thank you for using our servises. Please do come again.</p>
            <p>***********************************</p>
        </div>
    </div>
</body>
    <script>
        window.print();
        setTimeout(function(){
           window.history.back();
        },300);
    </script>
</html>