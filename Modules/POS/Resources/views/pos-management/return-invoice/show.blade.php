@extends('dboard.index')
@section('title', 'Sales Return Invoice')
@push('styles')
<style>
    .main-content {
        background: #fff;
        padding: 30px 0;
    }

    #invoice {
        width: 320px;
        margin: 0 auto;
        font-size: 16px;
        overflow: auto;
        background: #f8f9fb;
        padding: 20px 12px;
    }

    #invoice p {
        display: block;
    }

    .invoice-title-wrapper {
        overflow-x: hidden;
        text-align: center;
    }

    .invoice-product-table-wrapper table {
        font-size: 25px;
    }

    .invoice-product-table-wrapper {
        overflow-x: hidden;
    }

    thead th {
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
    }

    tbody {
        font-size: 15px;
    }

    table th,
    table td {
        padding: 2px !important;
        font-size: 20;
    }

    .invoice-calculation-area {
        overflow-x: hidden;
    }

    .invoice-calculation-area>div {
        display: flex;
        justify-content: space-between;

    }

    /* .logo-wrapper{
        width: 300px;
        height: 300px;
    } */
    .logo-wrapper img {
        height: auto;
        width: 100%;
    }

    .bar-code-wrapper img {
        height: 50px;
        width: 120px;
    }

    #button-area i {
        font-size: 18px;
    }


    @media print {
        * {
            font-size: 21px;

        }

        td,
        th {
            padding: 5px 0;
        }

        .hidden-print {
            display: none !important;
        }

        @page {
            margin: 0;
        }

        body {
            margin: 0.5cm;
            margin-bottom: 1.6cm;
        }
    }
</style>

@endpush
@section('dboard_content')

{{-- <!-- title-area -->
    <div class="row align-items-center">
        <div class="col-md-6">
            <h2 class="title-heading">Sales Invoice</h2>
        </div><!-- end .col-md-9 -->
    </div><!-- end .row -->
    <hr> --}}
<div id="button-area" class="text-right">
    <a href="{{route('return.invoice.print',$return_invoice->id)}}" class="btn btn-outline-primary mr-3">Prints
        <i class="pl-2 fa fa-print"></i>
    </a>
</div>

<div id="invoice" class="printable-area">
    <div class="">
        <div class="logo-wrapper">
            <img src="{{ asset('img/alesha-pharmacy.png') }}" alt="logo">
        </div>
    </div>

    <div class="invoice-title-wrapper">
        <h5>{{$return_invoice->store->name??''}}</h5>
        <p>Address: {{$return_invoice->store->address??''}}</p>
        <p>Mobile: {{$return_invoice->store->phone_no??''}}</p>
        <p>http://aleshapharmacy.com</p>
        <p>*********************************************************</p>
    </div>

    <div class="invoice-details-wrapper">
        <span>Return Invoice No: {{$return_invoice->return_invoice_no??''}}</span> <br>
        <span>Ref Invoice No: {{$return_invoice->ref_invoice_no??''}}</span> <br>
        <span>Date: {{$return_invoice->created_at??''}}</span>
        <p>Customer Info: {{$return_invoice->customer->customer_name??''}} |
            {{$return_invoice->customer->mobile_no??''}}
        </p>
        <p>Counter No: {{$return_invoice->counter_no??''}}</p>
        <!-- <p>Employee Name: {{$return_invoice->employee_id??''}}</p> -->
        <p>Employee Name: {{$return_invoice->employee->name??''}}</p>
    </div>

    
    <div class="invoice-product-table-wrapper">
        <table class="table table-striped table-bordered table-hover bg-white">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Item</th>
                    <th>Uom</th>
                    <th>Qty</th>
                    <th>Status</th>
                    <th>Return Price</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($return_product_details))
                @foreach($return_product_details as $row)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$row['product_name'] ?$row['product_name']:''}}</td>
                    <td>{{$row['uom_name']??' '}}</td>
                    <td>{{$row['quentity']??' '}}</td>
                    <td>{{$row['return_type']??' '}}</td>
                    <td>{{$row['return_price']??' '}}</td>
                    <td>{{$row['total_amount']??' '}}</td>
                </tr>
                @endforeach
                @endif

            </tbody>
        </table>
        <p>************************************************************************</p>
    </div>

    <div class="invoice-calculation-area">
        <div>
            <div>
                <p>Grand Total: </p>
            </div>

            <div>
                <p>{{$return_invoice->total_amount}}</p>
               
            </div>
        </div>
        <p>*********************************************************</p>
        <div class="logo-bar_code-wrapper">
            <div class="bar-code-wrapper" style="width: 100%">
                <!-- <img  style="width: 100%" src="{{ asset('img/barcode.png') }}" alt="barcode"> -->
            </div>
        </div>
        <p class="text-center mt-3">********** Thank You **********</p>
    </div>
</div>


@endsection

@push('post_scripts')
<!-- print area plugin -->
<script type="text/javascript" src="{{ asset('js/plugins/jquery.printarea.js') }}"></script>

{{-- script to print a specific area --}}
<script>
    $(function() {
        $("#print_btn").on('click', function() {

            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("#printable-area").printArea(options);
        });
    });
</script>
@endpush