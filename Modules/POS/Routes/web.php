<?php

use Illuminate\Support\Facades\Route;
use Modules\POS\Http\Controllers\PosReturnController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->prefix('pos')->group(function () {
    Route::get('/', 'POSController@index')->middleware('store');
    Route::get('/product/search', 'POSController@product_search')->name('pos.product.search')->middleware('store');
    Route::get('/product/add', 'POSController@pos_product_add')->name('pos.product.add')->middleware('store');
    Route::get('/product/uom/price', 'POSController@get_uom_price')->name('get.uom.price')->middleware('store');
    Route::get('/product/discount/price', 'POSController@get_discount_price')->name('get.discount.price')->middleware('store');
    Route::post('/store', 'POSController@point_of_sale_store')->name('point_of_sale.store')->middleware('store');
    Route::get('/check/number', 'POSController@get_discount_price')->name('check.customar.phone.number')->middleware('store');

    Route::get('/check/available-quentity', 'POSController@available_quentity')->name('check.available.quentity')->middleware('store');

    Route::get('/customer/information', 'POSController@get_customer_info')->name('get.customer.info')->middleware('store');
    Route::resource('/pos', 'POSController')->middleware('store');
    Route::resource('/invoice', 'InvoiceSaleController')->middleware('store');
    Route::get('/invoice/{id}/print', 'InvoiceSaleController@print')->name('invoice.print');

    Route::resource('/customer', 'CustomerController')->middleware('store');
    Route::resource('/customer-detail', 'CustomerDetailController')->middleware('store');
    Route::get('/invoice-get-invoice/{id}', 'InvoiceSaleController@getInvoiceModal')->name('invoice.get.invoice')->middleware('store');




    Route::post('/sales-summary', 'SaleReportController@pos_product_filter')->name('pos.product.filter')->middleware('store');

    Route::get('/hub/sales-summary/report', 'SaleReportController@hub_sales_report')->name('pos.hub.sales.report');
    Route::post('/hub/sales-summary/report', 'SaleReportController@hub_sales_report_search')->name('pos.hub.report.search');

    Route::post('/sales-summary', 'SaleReportController@pos_product_filter')->name('pos.product.filter')->middleware('store');
    Route::get('/product-search', 'SaleReportController@report_product_search')->name('get.product.search')->middleware('store');

    Route::get('/daily-report', 'SaleReportController@index')->name('pos.daily.report')->middleware('store');
    Route::get('/sales-summary', 'SaleReportController@sales_summary')->name('pos.sales.summary')->middleware('storeOrHub');
    Route::get('/sales/batch-wise/report', 'SaleReportController@batch_wise_report')->name('sales.batch_wise.report')->middleware('storeOrHub');

    Route::post('/sales/batch-wise/report', 'SaleReportController@batch_wise_report')->name('pos.batch.filter')->middleware('storeOrHub');
    Route::resource('/sales-return', 'PosReturnController');

    Route::post('/sales-return', 'SalesReturnController@search_invoice')->name('product.return')->middleware('store');
    Route::get('/return/product-search', 'SalesReturnController@sale_return_search')->name('pos.return.product.search')->middleware('store');
    Route::get('/return/search-by-id', 'SalesReturnController@sale_return_search_byID')->name('sale.return.search-by-id');
    Route::get('/return/total-price', 'SalesReturnController@sale_return_total_price')->name('sale.return.price');
    Route::post('/return/store', 'SalesReturnController@sale_return_store')->name('sale.return-store');


    Route::get('/return-invoice','ReturnInvoiceController@index')->name('return-invoice.index');
    Route::get('/return-invoice/{id}/show','ReturnInvoiceController@show')->name('return.invoice.show');
    Route::get('/return-invoice/{id}/print','ReturnInvoiceController@return_invoice_print')->name('return.invoice.print');

    Route::get('/return-invoice/apply','POSController@return_invoice_apply')->name('apply.return-invoice');

    Route::get('/card/information','POSController@card_information')->name('apply.card_information');
    
});
