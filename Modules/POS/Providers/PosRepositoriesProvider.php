<?php

namespace Modules\POS\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\POS\Repositories\PosInterface;
use Modules\POS\Repositories\PosRepository;

class PosRepositoriesProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PosInterface::class, PosRepository::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
