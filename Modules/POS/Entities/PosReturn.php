<?php

namespace Modules\POS\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Store\Entities\Store;

class PosReturn extends Model
{
    use HasFactory;

    protected $fillable = [];

    public function pos_return_details()
    {
        return $this->hasMany(PosReturnDetails::class,'pos_return_id');
    }

    public function getcreatedatAttribute()
    {
        $date=date_create($this->attributes['created_at']);
        return date_format($date,"d-m-Y");

    }

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withDefault();
    }

    public function employee()
    {
        return $this->belongsTo(User::class,'employee_id','id')->withDefault();
    }

    public function store()
    {
        return $this->belongsTo(Store::class)->withDefault();
    }
    
    protected static function newFactory()
    {
        return \Modules\POS\Database\factories\PosReturnFactory::new();
    }
}
