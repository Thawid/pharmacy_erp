<?php

namespace Modules\POS\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\UOM;

class PosReturnDetails extends Model
{
    use HasFactory;

    protected $fillable = [];
    
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id')->withDefault();
    }

    public function uom()
    {
        return $this->belongsTo(UOM::class,'uom_id')->withDefault();
    }

    protected static function newFactory()
    {
        return \Modules\POS\Database\factories\PosReturnDetailsFactory::new();
    }
}
