<?php

namespace Modules\POS\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;

class PointOfSaleDetail extends Model
{
    use HasFactory;

    protected $fillable = [];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id')->withDefault();
    }

    public function uom()
    {
        return $this->belongsTo(UOM::class,'uom_id')->withDefault();
    }

    public function pos()
    {
        return $this->belongsTo(PointOfSale::class,'pos_id')->withDefault();
    }

    public function batch()
    {
        return $this->belongsTo(StoreStockEntryDetails::class,'batch_no','batch_no')->withDefault();
    }

    protected static function newFactory()
    {
        return \Modules\POS\Database\factories\PointOfSaleDetailFactory::new();
    }
}
