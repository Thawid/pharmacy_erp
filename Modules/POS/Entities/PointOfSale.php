<?php

namespace Modules\POS\Entities;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PointOfSale extends Model
{
    use HasFactory;

    protected $fillable = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withDefault();
    }

    public function point_of_sales()
    {
        return $this->hasMany(PointOfSaleDetail::class, 'pos_id');
    }

    public function createdat()
    {
        return $strtime = strtotime($this->created_at);
        return Carbon::createFromFormat('m/d/Y', $this->created_at);
    }

    public function getcreatedatAttribute()
    {
        $date=date_create($this->attributes['created_at']);
        return date_format($date,"d-m-Y");

    }

    
    public function getemployeeAttribute()
    {
        return User::select('name')->findOrFail($this->attributes['employee_id']);

    }
    

    protected static function newFactory()
    {
        return \Modules\POS\Database\factories\PointOfSaleFactory::new();
    }
}
