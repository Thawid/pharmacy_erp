<?php

namespace Modules\POS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\POS\Http\Requests\CreateCustomerRequest;
use Modules\POS\Http\Requests\UpdateCustomerRequest;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = DB::table('customers')->orderBy('id', 'desc')->get();
        // return $customers;
        return view('pos::pos-management.customer.index', compact('customers'));
    }

    public function create()
    {
        return view('pos::pos-management.customer.create');
    }

    public function store(CreateCustomerRequest $createCustomerRequest)
    {
        // return $createCustomerRequest->all();

        $imageName = '';
        if ($createCustomerRequest->hasfile('customer_image')) {
            $imageFile = $createCustomerRequest->customer_image;
            $imageName = 'photo_' . time() . rand(1, 100) . '.' . $imageFile->extension();
            if (!file_exists(public_path('customer_image'))) {
                mkdir(public_path('customer_image'), 0777);
            }
            $imageFile->move(public_path('customer_image'), $imageName);
        }

        $input = [
            'customer_name' => $createCustomerRequest->customer_name,
            'mobile_no' => $createCustomerRequest->mobile_number,
            'membership_card' => $createCustomerRequest->membership_card,
            'location' => $createCustomerRequest->location,
            'image' => $imageName,
        ];

        DB::table('customers')->insert($input);

        return redirect()->route('customer.index')->with('success', 'Customer has been created successfully');
    }

    public function show($id)
    {
        $customer = DB::table('customers')->where('id', $id)->first();
        return view('pos::pos-management.customer.show', compact('customer'));
    }

    public function edit($id)
    {
        $customer = DB::table('customers')->where('id', $id)->orderBy('id', 'desc')->first();
        // return $customer;
        return view('pos::pos-management.customer.edit', compact('customer'));
    }

    public function update(UpdateCustomerRequest $updateCustomerRequest, $id)
    {
        $imageName = '';
        if ($updateCustomerRequest->hasfile('customer_image')) {
            $imageFile = $updateCustomerRequest->customer_image;
            $imageName = 'photo_' . time() . rand(1, 100) . '.' . $imageFile->extension();
            if (!file_exists(public_path('customer_image'))) {
                mkdir(public_path('customer_image'), 0777);
            }
            $imageFile->move(public_path('customer_image'), $imageName);
        }

        $input = [
            'customer_name' => $updateCustomerRequest->customer_name,
            'mobile_no' => $updateCustomerRequest->mobile_number,
            'membership_card' => $updateCustomerRequest->membership_card,
            'location' => $updateCustomerRequest->location,
            'image' => $imageName,
        ];

        DB::table('customers')->where('id', $id)->update($input);
        return redirect()->route('customer.index')->with('success', 'Customer has been updated successfully');
    }

    public function destroy($id)
    {
        //
    }
}
