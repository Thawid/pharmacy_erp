<?php

namespace Modules\POS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\POS\Entities\PointOfSale;
use Modules\POS\Entities\PointOfSaleDetail;
use Modules\Product\Entities\Manufacturer;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Entities\ProductType;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use Carbon\Carbon;
use DB;

class SaleReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $date = Carbon::now()->format('Y-m-d');

        $store_user = StoreUser::with('store')->select('store_id', 'user_id')->where('user_id', auth()->user()->id)->first();

        $pos_items = PointOfSale::where('store_id', $store_user->store_id)->where('created_at', 'like', '%' . $date . '%')->get();

        $sale_ids = [];
        foreach ($pos_items as $row) {
            array_push($sale_ids, $row->id);
        }
        $sales_items = PointOfSaleDetail::whereIn('pos_id', $sale_ids)->with(['product', 'uom', 'product.manufacturer', 'product.pgeneric'])->orderBy('id', 'DESC')->get();
        $sales_items = $sales_items->groupBy(['product_id', 'uom_id']);

        $main_product = [];
        foreach ($sales_items as $items) {
            foreach ($items as $products) {

                $product = $products->first();
                $product_item = array();
                $product_item['product_id'] = $product->product->id ?? 'Not Found!';
                $product_item['product'] = $product->product->product_name ?? 'Not Found!';
                $product_item['manufacturer'] = $product->product->manufacturer->name ?? 'Not Found!';
                $product_item['generic'] = $product->product->pgeneric->name ?? 'Not Found!';
                $product_item['uom'] = $product->uom->uom_name ?? 'Not Found!';
                $product_item['uom_id'] = $product->uom->id ?? 'Not Found!';

                $orderqty = 0;
                $discount = 0;
                $total = 0;
                $vat_amount = 0;
                foreach ($products as $item) {
                    $orderqty += $item->order_qty;
                    $discount += $item->discount;
                    $vat_amount += $item->vat_amount;
                    $total += $item->sub_total;
                }

                $product_item['orderqty'] = $orderqty;
                $product_item['discount'] = $discount;
                $product_item['vat_amount'] = $vat_amount;
                $product_item['total'] = $total;

                array_push($main_product, $product_item);
            }
        }

        $sales_reports = collect($main_product);

        return view('pos::pos-management.reports.daily_sales', compact('sales_reports', 'date'));
    }


    public function sales_summary()
    {
        $product_types = ProductType::where('status', 1)->get();
        $manufactures = Manufacturer::where('status', 1)->get();
        $product_categores = ProductCategory::where('status', 1)->get();
        return view('pos::pos-management.reports.sales_summary', compact('product_types', 'manufactures', 'product_categores'));
    }

    public function pos_product_filter(Request $request)
    {

        $product_id = $request->product;
        $cat_id = $request->product_category;
        $product_type = $request->product_type;
        $product = $request->product;
        $manufactures = $request->manufacturer;
        $from_date = $request->from_date;
        $to_date = $request->to_date;

        if (isset($from_date) && isset($to_date)) {
            $request->validate([
                'from_date' => 'before:to_date',
                'to_date' => 'after:from_date'
            ]);
        }


        $store_user = StoreUser::with('store')->select('store_id', 'user_id')->where('user_id', auth()->user()->id)->first();

        $pos_items = PointOfSale::where('store_id', $store_user->store_id)->get();

        $sale_ids = [];
        foreach ($pos_items as $row) {
            array_push($sale_ids, $row->id);
        }

        $sales_items = PointOfSaleDetail::with(['product', 'uom', 'product.manufacturer', 'product.pgeneric'])->whereIn('pos_id', $sale_ids)->when(isset($product_id), function ($query) use ($product_id) {
            $query->where('product_id', $product_id);
        })

            ->when(isset($cat_id), function ($query) use ($cat_id) {
                $query->wherehas('product', function (Builder $query) use ($cat_id) {
                    $query->where('product_category_id', $cat_id);
                });
            })

            ->when(isset($product_type), function ($query) use ($product_type) {
                $query->wherehas('product', function (Builder $query) use ($product_type) {
                    $query->where('product_category_id', $product_type);
                });
            })

            ->when(isset($product), function ($query) use ($product) {
                $query->where('product_id', $product);
            })

            ->when(isset($manufactures), function ($query) use ($manufactures) {
                $query->wherehas('product', function (Builder $query) use ($manufactures) {
                    $query->where('manufacturer_id', $manufactures);
                });
            })

            ->when(isset($from_date) && isset($to_date), function ($query) use ($from_date, $to_date) {
                $query->whereBetween('created_at', [$from_date, $to_date]);
            })

            ->get();

        $sales_items = $sales_items->groupBy(['product_id', 'uom_id']);

        $main_product = [];
        foreach ($sales_items as $items) {
            foreach ($items as $products) {

                $product = $products->first();
                $product_item = array();
                $product_item['product_id'] = $product->product->id ?? 'Not Found!';
                $product_item['product'] = $product->product->product_name ?? 'Not Found!';
                $product_item['manufacturer'] = $product->product->manufacturer->name ?? 'Not Found!';
                $product_item['generic'] = $product->product->pgeneric->name ?? 'Not Found!';
                $product_item['uom'] = $product->uom->uom_name ?? 'Not Found!';
                $product_item['uom_id'] = $product->uom->id ?? 'Not Found!';

                $orderqty = 0;
                $discount = 0;
                $total = 0;
                $vat_amount = 0;
                foreach ($products as $item) {
                    $orderqty += $item->order_qty;
                    $discount += $item->discount;
                    $vat_amount += $item->vat_amount;
                    $total += $item->sub_total;
                }
                $product_item['orderqty'] = $orderqty;
                $product_item['discount'] = $discount;
                $product_item['vat_amount'] = $vat_amount;
                $product_item['total'] = $total - $discount;

                array_push($main_product, $product_item);
            }
        }



        $search_filter = array();
        if (isset($store_user->store_id)) {
            $store = Store::select('name')->findOrFail($store_user->store_id);
            $search_filter['store_name'] = $store->name;
        }

        if (isset($product_id)) {
            $product = Product::findOrFail($product_id);
            $search_filter['product_name'] = $product->product_name;
        }

        if (isset($product_type)) {
            $product_type = ProductType::findOrFail($product_type);
            $search_filter['product_type'] = $product_type->name;
        }

        if (isset($manufactures)) {
            $manufacture = Manufacturer::findOrFail($manufactures);
            $search_filter['manufactures'] = $manufacture->name;
        }

        if (isset($cat_id)) {
            $product_categore = ProductCategory::findOrFail($cat_id);
            $search_filter['categores'] = $product_categore->name;
        }

        if (isset($from_date) && isset($to_date)) {
            $search_filter['date'] = $from_date . ' TO ' . $to_date;
        }


        $sales_reports = collect($main_product);

        $product_types = ProductType::where('status', 1)->get();
        $manufactures = Manufacturer::where('status', 1)->get();
        $product_categores = ProductCategory::where('status', 1)->get();

        return view('pos::pos-management.reports.sales_summary', compact('product_types', 'manufactures', 'product_categores', 'sales_reports', 'search_filter'));
    }

    public function report_product_search(Request $request)
    {
        $searchstr = $request->product_search;
        $products = Product::where('product_name', 'LIKE', '%' . $searchstr . "%")->select('product_name', 'id')->get();

        return response()->json($products);

        return $request;
    }


    public function hub_sales_report()
    {
        $stores = Store::where('hub_id', auth()->user()->id)->get();
        return view('pos::pos-management.reports.hub_sales_report', compact('stores'));
    }

    public function hub_sales_report_search(Request $request)
    {
        // return $request;
        if (isset($from_date) && isset($to_date)) {
            $request->validate([
                'from_date' => 'before:to_date',
                'to_date' => 'after:from_date'
            ]);
        }


        $from_date = $request->from_date;
        $to_date = $request->to_date;


        $stores = Store::where('hub_id', auth()->user()->id)->get();

        $search_item = array();

        $search_item['date'] = $from_date . ' TO ' . $to_date;

        if ($request->store_id == 0) {
            $search_item['store_name'] = 'All Store';
            $point_of_sale = PointOfSale::with('point_of_sales')->withCount('point_of_sales')->when(isset($request->from_date), function ($query) use ($from_date, $to_date) {
                $query->whereBetween('created_at', [$from_date, $to_date]);
            })->get();

            $point_of_sale = $point_of_sale->groupBy('store_id');
        } else {
            $store = Store::findOrFail($request->store_id);
            $search_item['store_name'] = $store->name;
            $point_of_sale = PointOfSale::with('point_of_sales')->withCount('point_of_sales')->where('store_id', $request->store_id)->when(isset($request->from_date), function ($query) use ($from_date, $to_date) {
                $query->whereBetween('created_at', [$from_date, $to_date]);
            })->get();
            $point_of_sale = $point_of_sale->groupBy('store_id');
        }

        $discount = 0;
        $total = 0;
        $pos_itemms = [];
        foreach ($point_of_sale as $key => $pos) {
            $store = Store::select('name')->findOrFail($key);
            $invoice_no = count($pos);
            foreach ($pos as $key => $item) {
                $discount += $item->total_discount;
                $total += $item->grand_total;
            }

            $data = array();
            $data['store_name'] = $store->name;
            $data['invoice_no'] = $invoice_no;
            $data['discount'] = $discount;
            $data['total'] = $total - $discount;
            array_push($pos_itemms, $data);
        }


        return view('pos::pos-management.reports.hub_sales_report', compact('stores', 'pos_itemms', 'search_item'));
    }


    public function batch_wise_report(Request $request)
    {
        
        if (auth()->user()->role == 'admin') {
            if ($request->has('product_type') && $request->product_type == 'regular') {
                $status = $request->product_type;
                $stock_reports = DB::table('store_stock_entry_details')->where('stock_status',NULL)->join('products', 'products.id', '=', 'store_stock_entry_details.product_id')->join('stores','stores.id','=','store_stock_entry_details.store_id')->select('product_name', 'out_quantity', 'received_quantity', 'batch_no','store_id','name as store_name')->get();
                return view('pos::pos-management.reports.batch_wise_report', compact('stock_reports','status'));
            }
            
            if ($request->has('product_type') && $request->product_type == 'empty') {
                $status = $request->product_type;

                $empty_stock_reports = DB::table('store_stock_entry_details')->where('stock_status','empty')->join('products', 'products.id', '=', 'store_stock_entry_details.product_id')->join('stores','stores.id','=','store_stock_entry_details.store_id')->select('product_name', 'out_quantity', 'received_quantity', 'batch_no','out_date','stock_status','name as store_name')->get();
                return view('pos::pos-management.reports.batch_wise_report', compact('empty_stock_reports','status'));
            }
            $stock_reports=[];
            return view('pos::pos-management.reports.batch_wise_report', compact('stock_reports'));
        }

        if (auth()->user()->role == 'store_user') {
            if ($request->has('product_type') && $request->product_type == 'regular') {

                $store = StoreUser::where('user_id', auth()->user()->id)->first();
                $store_id = $store->store_id;
                $status = $request->product_type;

                $stock_reports = DB::table('store_stock_entry_details')->where('store_id', $store_id)->where('stock_status',NULL)->join('products', 'products.id', '=', 'store_stock_entry_details.product_id')->select('product_name', 'out_quantity', 'received_quantity', 'batch_no')->get();
                return view('pos::pos-management.reports.batch_wise_report', compact('stock_reports','status'));
            }
            
            if ($request->has('product_type') && $request->product_type == 'empty') {
                $store = StoreUser::where('user_id', auth()->user()->id)->first();
                $store_id = $store->store_id;
                $status = $request->product_type;

                $empty_stock_reports = DB::table('store_stock_entry_details')->where('store_id', $store_id)->where('stock_status','empty')->join('products', 'products.id', '=', 'store_stock_entry_details.product_id')->select('product_name', 'out_quantity', 'received_quantity', 'batch_no','out_date','stock_status')->get();
                return view('pos::pos-management.reports.batch_wise_report', compact('empty_stock_reports','status'));
            }

            $stock_reports=[];
            return view('pos::pos-management.reports.batch_wise_report', compact('stock_reports'));
        }
    }
}
