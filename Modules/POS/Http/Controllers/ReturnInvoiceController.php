<?php

namespace Modules\POS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\POS\Entities\PosReturn;
use Modules\POS\Entities\PosReturnDetails;
use Illuminate\Support\Collection;

class ReturnInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $pos_return = PosReturn::with('customer')->where('status', 0)->orderBy('id', 'desc')->get();
        return view('pos::pos-management.return-invoice.index', compact('pos_return'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('pos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $return_invoice = PosReturn::with(['customer', 'store', 'employee'])->findOrFail($id);

        $return_invoice_details = PosReturnDetails::with(['product', 'uom'])->where('pos_return_id', $return_invoice->id)->get();
        $return_invoice_details = $return_invoice_details->groupBy(['uom_id', 'return_type']);

        $return_product_details = [];
        foreach ($return_invoice_details as $uoms) {
            foreach ($uoms as $status) {
                $quentity = 0;
                $return_price = 0;
                $total_amount = 0;
                foreach ($status as $row) {
                    // return $row;
                    $items = [];
                    $items['product_name'] = $row->product->product_name ? $row->product->product_name : 'Not Found';
                    $items['uom_name'] = $row->uom->uom_name ? $row->uom->uom_name : 'Not Found';
                    $items['return_type'] = $row->return_type;
                    $quentity +=$row->return_qty;
                    $return_price +=$row->return_price;
                    $total_amount +=$row->total_amount;
                }
                $items['quentity'] = $quentity;
                $items['return_price'] = $return_price;
                $items['total_amount'] = $total_amount;
                array_push($return_product_details,$items);
            }
        }

        $return_product_details = new Collection($return_product_details);

        return view('pos::pos-management.return-invoice.show', compact('return_invoice','return_product_details'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('pos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    public function return_invoice_print($id)
    {
        // $return_invoice = PosReturn::with(['pos_return_details' => function ($q) {
        //     $q->with(['product', 'uom']);
        // }, 'customer' => function ($q) {
        //     $q->select('customer_name', 'mobile_no', 'id');
        // }, 'store' => function ($q) {
        //     $q->select('name', 'address', 'phone_no', 'id');
        // }, 'employee'])->findOrFail($id);

        $return_invoice = PosReturn::with(['customer', 'store', 'employee'])->findOrFail($id);

        $return_invoice_details = PosReturnDetails::with(['product', 'uom'])->where('pos_return_id', $return_invoice->id)->get();
        $return_invoice_details = $return_invoice_details->groupBy(['uom_id', 'return_type']);

        $return_product_details = [];
        foreach ($return_invoice_details as $uoms) {
            foreach ($uoms as $status) {
                $quentity = 0;
                $return_price = 0;
                $total_amount = 0;
                foreach ($status as $row) {
                    // return $row;
                    $items = [];
                    $items['product_name'] = $row->product->product_name ? $row->product->product_name : 'Not Found';
                    $items['uom_name'] = $row->uom->uom_name ? $row->uom->uom_name : 'Not Found';
                    $items['return_type'] = $row->return_type;
                    $quentity +=$row->return_qty;
                    $return_price +=$row->return_price;
                    $total_amount +=$row->total_amount;
                }
                $items['quentity'] = $quentity;
                $items['return_price'] = $return_price;
                $items['total_amount'] = $total_amount;
                array_push($return_product_details,$items);
            }
        }

        $return_product_details = new Collection($return_product_details);

        return view('pos::pos-management.return-invoice.print', compact('return_invoice','return_product_details'));
    }
}
