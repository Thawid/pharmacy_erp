<?php

namespace Modules\POS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PosReturnController extends Controller
{

    public function index()
    {
        return view('pos::pos-management.pos-return.index');
    }


    public function create()
    {
        return view('pos::create');
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        return view('pos::show');
    }


    public function edit($id)
    {
        return view('pos::edit');
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
