<?php

namespace Modules\POS\Http\Controllers\Api\V1;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\POS\Entities\PointOfSale;
use Modules\POS\Entities\POSCustomer;
use phpDocumentor\Reflection\Types\Null_;

class PosApiController extends Controller
{
    public function searchCustomer (Request $request) {
        
       
        $phone_number = $request->phone_number;
        $customers= DB::table('pos_customers')
        ->select('id','customer_name','mobile_no')
        ->where('mobile_no', 'LIKE', "%{$phone_number}%")
        ->limit(5)
        ->get();

      

        if(count($customers) == 0){

            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'No Customer Found', 
                    'data' => [
                        'customer'=>$customers
                        ]               

               ], 200);

        }else{
            return response()->json([
                'status' => 'success',
                'message' => 'Customer list by name',
                'data' => [
                    'customer'=>$customers
                    ]

            ], 200);
        }  
    
 }
}
