<?php

namespace Modules\POS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Accounts\Entities\StoreInvoice;
use Modules\POS\Entities\PointOfSale;
use Modules\POS\Entities\PointOfSaleDetail;

class InvoiceSaleController extends Controller
{
    public function index()
    {
        $point_of_sales = PointOfSale::with('customer')->orderBy('id', 'DESC')->get();
        return view('pos::pos-management.invoice.index', compact('point_of_sales'));
    }

    public function create()
    {
        return view('pos::pos-management.invoice.create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $invoice = PointOfSale::findOrFail($id);
        $store_invoice = StoreInvoice::with('storeInfo')->where('invoice_no', $invoice->invoice_no)->first();

        $point_of_sales = PointOfSaleDetail::with(['product','uom'])->where('pos_id',$id)->get();
        $point_of_sales = $point_of_sales->groupby(['product_id','uom_id']);

        $pos_data = [];
        foreach ($point_of_sales as $pos_items) {
            foreach ($pos_items as  $items) {
                $order_qty = 0;
                $discount = 0;
                $vat_amount = 0;
                $sub_total = 0;
                foreach ($items as $row) {
                    $order_qty +=$row->order_qty;
                    $discount +=$row->discount;
                    $vat_amount +=$row->vat_amount;
                    $sub_total +=$row->sub_total;
                    $item['product']=$row->product->product_name;
                    $item['per_unit_price']=$row->per_unit_price;
                    $item['uom']=$row->uom->uom_name;
                }

                $item['id']=1;
                $item['order_qty']=$order_qty;
                $item['discount']=$discount;
                $item['vat_amount']=$vat_amount;
                $item['sub_total']=$sub_total -$discount + $vat_amount;
                array_push($pos_data,$item);
            }
        }

        $pos_detials = new Collection($pos_data);

        
        return view('pos::pos-management.invoice.show', compact('invoice', 'store_invoice','pos_detials'));
    }
    public function print($id)
    {
        // $invoice = PointOfSale::with(['customer', 'point_of_sales', 'point_of_sales.product', 'point_of_sales.uom'])->findOrFail($id);
        // $store_invoice = StoreInvoice::with('storeInfo')->where('invoice_no', $invoice->invoice_no)->first();
        $invoice = PointOfSale::findOrFail($id);
        $store_invoice = StoreInvoice::with('storeInfo')->where('invoice_no', $invoice->invoice_no)->first();

        $point_of_sales = PointOfSaleDetail::with(['product','uom'])->where('pos_id',$id)->get();
        $point_of_sales = $point_of_sales->groupby(['product_id','uom_id']);

        $pos_data = [];
        foreach ($point_of_sales as $pos_items) {
            foreach ($pos_items as  $items) {
                $order_qty = 0;
                $discount = 0;
                $vat_amount = 0;
                $sub_total = 0;
                foreach ($items as $row) {
                    $order_qty +=$row->order_qty;
                    $discount +=$row->discount;
                    $vat_amount +=$row->vat_amount;
                    $sub_total +=$row->sub_total;
                    $item['product']=$row->product->product_name;
                    $item['per_unit_price']=$row->per_unit_price;
                    $item['uom']=$row->uom->uom_name;
                }

                $item['id']=1;
                $item['order_qty']=$order_qty;
                $item['discount']=$discount;
                $item['vat_amount']=$vat_amount;
                $item['sub_total']=$sub_total -$discount + $vat_amount;
                array_push($pos_data,$item);
            }
        }

        $pos_detials = new Collection($pos_data);
        return view('pos::pos-management.invoice.print', compact('invoice', 'store_invoice','pos_detials'));
    }

    public function edit($id)
    {
        return view('pos::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function getInvoiceModal($id)
    {
        $invoice = PointOfSale::with(['customer', 'point_of_sales', 'point_of_sales.product', 'point_of_sales.uom'])->findOrFail($id);
        return "get invoice modal";
    }
    

    // test table 
    public function testtableModal()
    {
        return view('pos::pos-management.table.index',);
    }

    public function invoice_print($id)
    {
        $invoice = PointOfSale::with(['customer', 'point_of_sales', 'point_of_sales.product', 'point_of_sales.uom'])->findOrFail($id);
        $store_invoice = StoreInvoice::with('storeInfo')->where('invoice_no', $invoice->invoice_no)->first();
        return view('pos::pos-management.invoice.print', compact('invoice', 'store_invoice'));
    }
}
