<?php

namespace Modules\POS\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Illuminate\Support\Facades\Gate;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\POS\Entities\Customer;
use Modules\POS\Entities\PosReturn;
use Modules\POS\Repositories\PosInterface;
use Modules\Product\Entities\ProductUom;
use Modules\Store\Entities\StoreUser;
use Illuminate\Support\Facades\Http;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\POS\Entities\PosReturnDetails;

class POSController extends Controller
{

    private $posRepositories;
    
    function __construct(PosInterface $posRepositories)
    {
        $this->posRepositories = $posRepositories;
    }


    /**
     * Show pos create page.
     *
     * @return Response
     */

    public function index()
    {
        return view('pos::pos-management.pos.index');
    }

    /**
     * Show Point of sale page show
     *
     * @return Response
     */


    public function create()
    {
        
        Gate::authorize('hasCreatePermission');
        $invoice_no = $this->posRepositories->create();
        return view('pos::pos-management.pos.create', compact('invoice_no'));
    }

    /**
     * store point of sale
     * @param Request
     * @return Response
     */

    public function point_of_sale_store(Request $request)
    {
        $point_of_sales = $this->posRepositories->point_of_sale_store($request);
        return redirect()->route('invoice.show', $point_of_sales);
    }


    /**
     * product search 
     * @param Request
     * @return Response
     */

    public function product_search(Request $request)
    {
        $products = $this->posRepositories->product_search($request);
        return response()->json($products);
    }

    /**
     * product add on list
     * @param Request
     * @return Response
     */

    public function pos_product_add(Request $request)
    {
        return $this->posRepositories->pos_product_add($request);
    }


    /**
     * get uom price
     * @param Request
     * @return Response
     */


    public function get_uom_price(Request $request)
    {
        return $this->posRepositories->get_uom_price($request);
    }

    /**
     * Get Discount Price
     * @param Request
     * @return Response
     */


    public function get_discount_price(Request $request)
    {
        return $this->posRepositories->get_discount_price($request);
    }

    /**
     * Get Customer Information
     * @param Request
     * @return Response
     */

    public function get_customer_info(Request $request)
    {
        $customers = $this->posRepositories->get_customer_info($request);
        return response()->json($customers);
    }


    /**
     * Deduct from Available quentity
     * @param Request
     * @return Response
     */

    public function available_quentity(Request $request)
    {
        $available_qty = $this->posRepositories->available_quentity($request);



        
        $store_stocks_details = StoreStockEntryDetails::select('received_quantity','out_quantity')->where('batch_no',$request->batch_no)->first();
        $store_stocks_qty = $store_stocks_details->received_quantity;
        $out_quantity = $store_stocks_details->out_quantity;
        $order_qty =$request->order_qyt;


        

        $product_uoms = ProductUom::select('quantity_per_uom')->where('product_id', $request->product_id)->where('uom_id', $request->uom_id)->first();
        $product_quanitity_uom = $product_uoms->quantity_per_uom;
        

        if ($product_quanitity_uom > 0 && $store_stocks_qty > 0) {
            $store_available_qty = (int)($store_stocks_qty- $out_quantity) / $product_quanitity_uom ;
            if($store_available_qty <=0){
                $store_available_qty =0;
            }
        } else {
            $store_available_qty = 0;
        }

        // return $store_available_qty;

        if($store_stocks_qty <= 0 || $store_available_qty < $order_qty ){
            return response()->json([
                'store_stock_msg' => 'Out of Quantity,Available Batch Quantity is '.(int)$store_available_qty,
            ]);
        }

        if ($available_qty > 0) {
            return response()->json(intval($available_qty));
        } else {
            return response()->json([
                'err_msg' => 'Quantity not be lass then available quantity',
            ]);
        }
    }


    public function return_invoice_apply(Request $request)
    {
        
       
        $pos_return = PosReturn::where('return_invoice_no',$request->invoice)->first();

    //    $pos_return_details = PosReturnDetails::where('pos_return_id',$pos_return->id)->whereIn('return_type',['replacement','demage_product'])->get();

        // $total_amount = 0;
        // foreach ($pos_return_details as $row) {
        //     $total_amount +=$row->total_amount;   
        // }
        if($pos_return && $pos_return ->status ==1){
            return response()->json([
                'msg'=>'Invoice Already exists',
            ]);
        }
        if($pos_return && $pos_return->status !=1 ){
            $amount = $pos_return->total_amount;
        }else{
            $amount = 0;
        }

        return response()->json($amount);
    }


    public function card_information()
    {
      return $response = Http::accept('application/json')->get('https://jsonplaceholder.typicode.com/posts');

    }
}
