<?php

namespace Modules\POS\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\POS\Entities\PointOfSale;
use DB;
use Modules\Accounts\Entities\StoreInvoice;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\POS\Entities\PointOfSaleDetail;
use Modules\POS\Entities\PosReturn;
use Modules\POS\Entities\PosReturnDetails;
use Modules\Product\Entities\ProductUom;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;

class SalesReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('pos::pos-management.pos-return.index');
    }

    

    public function search_invoice(Request $request)
    {
        $request->validate([
            'invoice_no' => 'required',
        ], [
            'invoice_no.required' => 'Please add an Invoice Number',
        ]);

        $years = date('y');
        $month = date('m');
        $day = date('d');

        $invoice_no = PointOfSale::where('invoice_no', $request->invoice_no)->first();

        if(!$invoice_no){
            return back()->withErrors('Invoice not found!');
        }

        if (isset($invoice_no) && $invoice_no->status == 1) {
            return back()->withErrors('This Invoice Return is completed!');
        }

        $return_invoice_no = DB::table('pos_returns')->select('return_invoice_no', 'store_id')->orderBy('id', 'DESC')->first();

        // if ($return_invoice_no) {

        //     $store_user = StoreUser::where('user_id', auth()->user()->id)->first();

        //     $store_user_id = $store_user->store_id;
        //     $return_store_id = $return_invoice_no->store_id;
        //     if ($store_user_id !== $return_store_id) {
        //         return back()->withErrors('This Invoice not available for this store!');
        //     }
        // }


        if ($return_invoice_no) {
            $exist_invoice = $return_invoice_no->return_invoice_no;
            $exist_invoice = explode("-", $exist_invoice);
            $exist_invoice = $exist_invoice[2];
            $new_invoice = $exist_invoice + 1;
            $invoice_no = $day . $month . $years . '-RI-' . $new_invoice;
        } else {
            $invoice_no = $day . $month . $years . '-RI-' . '1000';
        }

        $point_of_sales = PointOfSale::where('invoice_no', $request->invoice_no)->with(['point_of_sales', 'customer'])->first();
        return view('pos::pos-management.pos-return.index', compact('point_of_sales', 'invoice_no'));
    }


    public function sale_return_search(Request $request)
    {
        
        $search_product = $request->product_search;


        $invoice_product = DB::table('point_of_sale_details')->where('pos_id', $request->invoice_id)->join('products', 'point_of_sale_details.product_id', '=', 'products.id')->join('uoms', 'point_of_sale_details.uom_id', '=', 'uoms.id')->where('products.product_name', 'like', '%' . $search_product . '%')->select('point_of_sale_details.id','point_of_sale_details.batch_no', 'products.product_name', 'uoms.uom_name')->get();
        return response()->json($invoice_product);
    }

    public function sale_return_search_byID(Request $request)
    {

        $invoice_product = DB::table('point_of_sale_details')->where('point_of_sale_details.id', $request->id)->join('products', 'point_of_sale_details.product_id', '=', 'products.id')->join('uoms', 'point_of_sale_details.uom_id', '=', 'uoms.id')->select('point_of_sale_details.id', 'products.product_name', 'uoms.uom_name', 'point_of_sale_details.order_qty', 'point_of_sale_details.product_id', 'point_of_sale_details.uom_id','point_of_sale_details.batch_no')->first();
        return response()->json($invoice_product);
    }

    public function sale_return_total_price(Request $request)
    {
        // return $request;

        if (!empty($request->pos_id) && !empty($request->prices) && !empty($request->quentity)) {
            $pos_id = $request->pos_id;
            $add_prices = $request->prices;
            $quentity = $request->quentity;
            $total_amount = 0;

            for ($i = 0; $i < count($pos_id); $i++) {

                $prices = PointOfSaleDetail::findOrFail($pos_id[$i]);

                $quentity = $request->quentity[$i];
                $add_prices = $request->prices[$i];

                $per_unit_price = $prices->per_unit_price;
                $order_qty = $prices->order_qty;

                $discount = ($prices->discount / $order_qty) * $request->quentity[$i];

                $vat_amount = $prices->vat_amount;
                $vat_amount = ($prices->vat_amount / $prices->order_qty) * $request->quentity[$i];

                $amount = $per_unit_price * $request->quentity[$i]  - $discount;

                $amount = $amount + $vat_amount;
                $total = $amount - $add_prices;

                $total_amount += $total;
            }

            $return_amount = $request->return_amount;
            $return_amount = $total_amount - $return_amount;
            return response()->json([
                // 'return_amount'=>$return_amount,
                'total_amount' => $total_amount
            ]);
        }
    }


    public function sale_return_store(Request $request)
    {
        $request->validate(
            [
                'product_id' => 'required|array',
                'return_qty' => 'required|array',
            ],
            [
                'product_id.required' => 'Product Field is required',
                'return_qty.required' => 'Return Quantity Field is required',
            ]
        );

        DB::beginTransaction();
        try {

            $store_user = StoreUser::with('store')->select('store_id', 'user_id')->where('user_id', auth()->user()->id)->first();
            $pos_return = new PosReturn();
            $pos_return->ref_invoice_no = $request->invoice_no;
            $pos_return->return_invoice_no = $request->return_invoice_no;
            $pos_return->customer_id = $request->customar_id;
            $pos_return->total_amount = $request->total_amount;
            $pos_return->store_id = $store_user->store_id;
            $pos_return->counter_no = rand(1, 5);
            $pos_return->employee_id = auth()->user()->id;
            $pos_return->save();

            $product_id = $request->product_id;
            $uom_id = $request->uom_id;
            $return_qty = $request->return_qty;
            $return_type = $request->return_type;
            $deduct_price = $request->deduct_price;
            $return_details = [];
            for ($i = 0; $i < count($product_id); $i++) {
                $pos_id = $request->pos_id;
                $prices = PointOfSaleDetail::findOrFail($pos_id[$i]);
                $return_quentity = $request->return_qty[$i];
                $add_prices = $request->deduct_price[$i];
                $batch_no = $request->batch_no[$i];
                $per_unit_price = $prices->per_unit_price;
                $order_qty = $prices->order_qty;
                $discount = ($prices->discount / $order_qty) * $return_quentity;
                $vat_amount = $prices->vat_amount;
                $vat_amount = ($prices->vat_amount / $prices->order_qty) * $return_quentity;
                $amount = $per_unit_price * $return_quentity  - $discount;
                $amount = $amount + $vat_amount;
                $total = $amount - $add_prices;
                $purchase_price = $prices->per_unit_price * $return_quentity - $prices->discount + $prices->vat_amount;
                $purchase_price = $purchase_price / $return_quentity;
                $return_price = $total / $return_quentity;
                $return_details[] = [
                    'pos_return_id' => $pos_return->id,
                    'product_id' => $product_id[$i],
                    'uom_id' => $uom_id[$i],
                    'return_qty' => $return_qty[$i],
                    'return_type' => $return_type[$i],
                    'deduct_amount' => $deduct_price[$i],
                    'purchase_price' => $purchase_price,
                    'return_price' => $return_price,
                    'batch_no' => $batch_no,
                    'total_amount' => $total,
                ];
                if($return_type[$i] == 'refund' || $return_type[$i] == 'replacement-refund' ){    
                $store_available_product = StoreAvailableProduct::where('store_id', $store_user->store_id)->first();

                if ($store_available_product) {
                   
                   $store_product = StoreAvailableProductDetails::select('available_quantity')->where('store_available_product_id', $store_available_product->id)->where('product_id', $product_id[$i])->first();
                    $available_quantity = $store_product->available_quantity;

                   $product_uoms = ProductUom::select('quantity_per_uom')->where('product_id', $product_id[$i])->where('uom_id', $uom_id[$i])->first();

                    if ($available_quantity > 0) {
                        $product_uoms = $product_uoms->quantity_per_uom;
                        $purchase_uom_qty = $product_uoms * $return_qty[$i];
                        StoreAvailableProductDetails::select('available_quantity')->where('store_available_product_id', $store_available_product->id)->where('product_id', $product_id[$i])->increment('available_quantity', $purchase_uom_qty);

                        $store_stock =StoreStockEntryDetails::where('batch_no',$batch_no)->first();
                        
                        $store_stock->out_quantity -= $purchase_uom_qty;

                        if($store_stock->received_quantity == $store_stock->out_quantity || $store_stock->received_quantity < $store_stock->out_quantity ){
                                $store_stock->stock_status = 'empty';
                            }else{
                                $store_stock->stock_status = 0;
                            }
                        $store_stock->save();
                    }
                }
                }
              

            }

            $point_of_sale = PointOfSale::where('invoice_no', $request->invoice_no)->first();
            $point_of_sale->status = 1;
            $point_of_sale->save();

            $store_invoice =StoreInvoice::where('invoice_no',$request->invoice_no)->first();
            $store_invoice->amount -=$request->total_amount;
            $store_invoice->save();

            PosReturnDetails::insert($return_details);

            $store =Store::where('id',$store_user->store_id)->decrement('total_balance',$request->total_amount);

            DB::commit();

            return redirect()->route('return.invoice.show', $pos_return->id);
        } catch (\Exception $e) {

            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error below" . $e->getMessage());
        }
    }
}
