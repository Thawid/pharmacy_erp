<?php

namespace Modules\POS\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckStoreOrHubMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(storeModuleAuth() || hubModuleAuth()){
            return $next($request);
        }
        return abort(403,'No Permission');
    }
}
