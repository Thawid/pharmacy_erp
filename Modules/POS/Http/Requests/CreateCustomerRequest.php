<?php

namespace Modules\POS\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCustomerRequest extends FormRequest
{
    public function rules()
    {
        return [
            'customer_name' => 'required',
            'mobile_number' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
