@extends('dboard.index')

@section('title','Create Store Type Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Create Store Type</h2>
                        </div>
                        <div class="col-md-12 text-center">
                            <a class="btn btn-primary icon-btn" href="{{route('divisontype.index')}}"><i class="fa fa-arrow-left"></i>Back	</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <form method="POST" action="{{ route('divisontype.store') }}" >
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="tile">

                    {{--                    <div class="form-group">--}}
                    {{--                        <label class="control-label">Name</label>--}}
                    {{--                        <input class="form-control" type="text" placeholder="Enter full name">--}}
                    {{--                    </div>--}}

                    <div class="form-group">
                        <label class="control-label" >Store Type</label>
                        <input class="form-control" name="name" type="text" placeholder="Enter your name" value="{{ old('name') }}" required>
                    </div>

                    {{--                    <div class="form-group">--}}
                    {{--                        <label for="exampleSelect1">Role</label>--}}
                    {{--                        <select class="form-control" id="exampleSelect1">--}}
                    {{--                            <option selected="true" disabled="disabled">Select Role</option>--}}
                    {{--                            <option>Role One</option>--}}
                    {{--                            <option>Role Two</option>--}}
                    {{--                            <option>Role Three</option>--}}
                    {{--                            <option>Role Four</option>--}}
                    {{--                            <option>Role Five</option>--}}
                    {{--                        </select>--}}
                    {{--                    </div>--}}



                    {{--                    status--}}
                    {{--                    <div class="form-group">--}}
                    {{--                        <label class="control-label">Status</label>--}}
                    {{--                        <div class="form-check">--}}
                    {{--                            <label class="form-check-label">--}}
                    {{--                                <div class="animated-radio-button">--}}
                    {{--                                    <label>--}}
                    {{--                                        <input type="radio" name="status"><span class="label-text">Active</span>--}}
                    {{--                                    </label>--}}
                    {{--                                </div>--}}
                    {{--                            </label>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="form-check">--}}
                    {{--                            <label class="form-check-label">--}}
                    {{--                                <div class="animated-radio-button">--}}
                    {{--                                    <label>--}}
                    {{--                                        <input type="radio" name="status"><span class="label-text">Inactive</span>--}}
                    {{--                                    </label>--}}
                    {{--                                </div>--}}
                    {{--                            </label>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    status--}}


                    {{--                    <div class="form-group">--}}
                    {{--                        <label for="exampleInputPassword1">Password</label>--}}
                    {{--                        <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Password">--}}
                    {{--                    </div>--}}
                </div>
            </div>
            {{--            <div class="col-md-6">--}}
            {{--                <div class="tile">--}}
            {{--                    <div class="form-group">--}}
            {{--                        <label class="control-label">Email</label>--}}
            {{--                        <input class="form-control" type="email" placeholder="Enter email address">--}}
            {{--                    </div>--}}
            {{--                    <div class="form-group">--}}
            {{--                        <label class="control-label">User Permission</label>--}}
            {{--                        <div class="form-check">--}}
            {{--                            <label class="form-check-label">--}}
            {{--                                <div class="animated-checkbox">--}}
            {{--                                    <label>--}}
            {{--                                        <input type="checkbox"><span class="label-text">Create</span>--}}
            {{--                                    </label>--}}
            {{--                                </div>--}}
            {{--                            </label>--}}
            {{--                        </div>--}}
            {{--                        <div class="form-check">--}}
            {{--                            <label class="form-check-label">--}}
            {{--                                <div class="animated-checkbox">--}}
            {{--                                    <label>--}}
            {{--                                        <input type="checkbox"><span class="label-text">Read</span>--}}
            {{--                                    </label>--}}
            {{--                                </div>--}}
            {{--                            </label>--}}
            {{--                        </div>--}}
            {{--                        <div class="form-check">--}}
            {{--                            <label class="form-check-label">--}}
            {{--                                <div class="animated-checkbox">--}}
            {{--                                    <label>--}}
            {{--                                        <input type="checkbox"><span class="label-text">Edit</span>--}}
            {{--                                    </label>--}}
            {{--                                </div>--}}
            {{--                            </label>--}}
            {{--                        </div>--}}
            {{--                        <div class="form-check">--}}
            {{--                            <label class="form-check-label">--}}
            {{--                                <div class="animated-checkbox">--}}
            {{--                                    <label>--}}
            {{--                                        <input type="checkbox"><span class="label-text">Delete</span>--}}
            {{--                                    </label>--}}
            {{--                                </div>--}}
            {{--                            </label>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                    <div class="form-group">--}}
            {{--                        <label for="exampleInputPassword1">Confirm Password</label>--}}
            {{--                        <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Password">--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile-footer">
                    <button class="btn btn-primary btn-block" type="submit">Create Account</button>
                </div>
            </div>
        </div>
    </form>
@endsection
