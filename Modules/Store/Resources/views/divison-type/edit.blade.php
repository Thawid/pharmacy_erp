@extends('dboard.index')

@section('title','Edit Store Type Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Edit Division Type</h2>
                        </div>
                        <div class="col-md-12 text-center">
                            <a class="btn btn-primary icon-btn" href="{{route('divisontype.index')}}"><i class="fa fa-arrow-left"></i>Back</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <form method="POST" action="{{route('divisontype.update', $divisonTypes->id)}}">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="form-group">
                        <label class="control-label" >Divison Type</label>
                        <input class="form-control" name="name" type="text" placeholder="Enter your name" value="{{$divisonTypes->name}}" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile-footer">
                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                </div>
            </div>
        </div>
    </form>
@endsection
