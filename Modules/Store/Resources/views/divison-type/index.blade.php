
@extends('dboard.index')

@section('title','View Divison Type Interface')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <h2>Store Type Details</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn btn-primary icon-btn" href="{{route('divisontype.create')}}"><i class="fa fa-plus"></i>Add Divison	</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable">
                            <thead class="thead">
                            <tr>
                                <th>#</th>
                                <th>Store Type</th>
                                <th>Status</th>
                                <th>Created_At</th>
                                <th>Updated_At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($divisonTypes as  $key=> $divisonType)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{$divisonType->name}}</td>
                                    @if($divisonType->status == 1)
                                        <td>Active</td>
                                    @else
                                        <td>InActive</td>
                                    @endif
                                    <td>{{$divisonType->created_at}}</td>
                                    <td>{{$divisonType->updated_at}}</td>
                                    <td>
                                        <form id="delete_form{{$divisonType->id}}" method="POST" action="{{ route('divisontype.destroy',$divisonType->id) }}" onclick="return confirm('Are you sure?')">@csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-primary" type="submit">Change Status</button>
                                            <a class="btn btn-primary" href="{{route('divisontype.edit',$divisonType->id)}}"><i class="fa fa-lg fa-edit">Edit</i></a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush
