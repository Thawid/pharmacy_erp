@extends('dboard.index')

@section('title','Edit New Store Form')

@section('dboard_content')
   
    <form method="POST" action="{{route('storeconfiguration.update', $storeid->id)}}">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-6">
                                <h2 class="title-heading"> Store Configuration</h2>
                            </div>
                            <div class="col-md-6 text-right">
                                <a class="btn index-btn" href="{{route('storeconfiguration.index')}}">Back</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                               
                                    @if(isset($stores))
                                        <div class="form-group">
                                            <label for="exampleSelect1">Select Store</label>
                                            <input type="hidden" name="store_id" value="{{$storeid->id}}">
                                            <input type="hidden" name="address" value="{{$storeid->address}}">
                                            <input type="hidden" name="store_type_id" value="{{$storeid->store_type_id}}">
                                            <input type="hidden" name="email" value="{{$storeid->email}}">
                                            <input type="hidden" name="phone_no" value="{{$storeid->phone_no}}">
                                            <input type="hidden" name="status" value="{{$storeid->status}}">
                                            <input type="hidden" name="code" value="{{$storeid->code}}">
                
                                            <select class="form-control @error('name') is-invalid @enderror" name="name" id="exampleSelect1">
                                                @foreach($stores as $store)
                {{--                                    <option value="{{$store->name}}">{{$store->name}}</option>--}}
                                                    <option value="{{$store->name}}" @if($store->id == $storeid->id) selected @endif> {{ $store->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                
                
                                        @if(isset($stores))
                                            <div class="form-group">
                                                <label for="exampleSelect2">Store / Hub</label>
                
                                                <select class="select2 form-control @error('hub_id') is-invalid @enderror" name="hub_id" id="exampleSelect2">
                                                    <option selected="true" disabled="disabled">Select Store / Hub</option>
                                                    @foreach($stores as $store)
                                                        <option value="{{$store->id}}">{{$store->store_type}}</option>
                                                    @endforeach
                                                </select>
                                                @error('hub_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        @endif
                
                                        @if(isset($warehouses))
                                            <div class="form-group">
                                                <label for="exampleSelect3">Warehouse</label>
                
                                                <select class="form-control @error('warehouse_id') is-invalid @enderror" name="warehouse_id" id="exampleSelect3">
                                                    <option selected="true" disabled="disabled">Select Warehouse</option>
                                                    @foreach($warehouses as $warehouse)
                
                                                        <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                                                        {{--                                        <option value="{{$warehouse->id}}" @if($store->id == $storeid->id) selected @endif> {{ $warehouse->name }}</option>--}}
                                                    @endforeach
                                                </select>
                                                @error('warehouse_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        @endif
                
                {{--                        @if(isset($storeTypes))--}}
                {{--                            <div class="form-group">--}}
                {{--                                <label for="exampleSelect2">Store / Hub</label>--}}
                
                {{--                                <select class="form-control @error('hub_id') is-invalid @enderror" name="hub_id" id="exampleSelect2">--}}
                {{--                                    <option selected="true" disabled="disabled">Select Store / Hub</option>--}}
                {{--                                    @foreach($storeTypes as $hub)--}}
                {{--                                        <option value="{{$hub->id}}">{{$hub->name}}</option>--}}
                {{--                                    @endforeach--}}
                {{--                                </select>--}}
                {{--                                @error('hub_id')--}}
                {{--                                <div class="alert alert-danger">{{ $message }}</div>--}}
                {{--                                @enderror--}}
                {{--                            </div>--}}
                {{--                        @endif--}}
                
                {{--                        @if(isset($warehouses))--}}
                {{--                            <div class="form-group">--}}
                {{--                                <label for="exampleSelect3">Warehouse</label>--}}
                
                {{--                                <select class="form-control @error('warehouse_id') is-invalid @enderror" name="warehouse_id" id="exampleSelect3">--}}
                {{--                                    <option selected="true" disabled="disabled">Select Warehouse</option>--}}
                {{--                                    @foreach($warehouses as $warehouse)--}}
                
                {{--                                        <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>--}}
                {{--                                        <option value="{{$warehouse->id}}" @if($store->id == $storeid->id) selected @endif> {{ $warehouse->name }}</option>--}}
                {{--                                    @endforeach--}}
                {{--                                </select>--}}
                {{--                                @error('warehouse_id')--}}
                {{--                                <div class="alert alert-danger">{{ $message }}</div>--}}
                {{--                                @enderror--}}
                {{--                            </div>--}}
                {{--                        @endif--}}
                
                {{--                    @if(isset($storeTypes))--}}
                {{--                        <div class="form-group">--}}
                {{--                            <label for="exampleSelect3">Select Parent Store</label>--}}
                
                {{--                            <select class="form-control @error('parent_store_id') is-invalid @enderror" name="parent_store_id" id="exampleSelect3">--}}
                {{--                                @foreach($storeTypes as $storeParentType)--}}
                {{--                                    <option value="{{$storeParentType->id}}">{{$storeParentType->name}}</option>--}}
                {{--                                @endforeach--}}
                {{--                            </select>--}}
                {{--                            @error('parent_store_id')--}}
                {{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
                {{--                            @enderror--}}
                {{--                        </div>--}}
                {{--                    @endif--}}
                
                                    
                
                                    @if(isset($divisions))
                                        <div class="form-group">
                                            <label for="exampleSelect6">Division</label>
                
                                            <select class="form-control @error('division_id') is-invalid @enderror" name="division_id" id="exampleSelect6">
                                                @foreach($divisions as $division)
                                                    <option value="{{$division->id}}">{{$division->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('division_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                            </div><!-- end.col-md-6 -->

                            <div class="col-md-6">
                                @if(isset($policies))
                                    <div class="form-group">
                                        <label for="exampleSelect8">Policy</label>
            
                                        <select class="form-control @error('policy_id') is-invalid @enderror" name="policy_id" id="exampleSelect8">
                                            @foreach($policies as $policie)
                                                <option value="{{$policie->id}}">{{$policie->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('policy_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @endif

                                @if(isset($zones))
                                    <div class="form-group">
                                        <label for="exampleSelect7">Zone</label>
            
                                        <select class="form-control @error('zone_id') is-invalid @enderror" name="zone_id" id="exampleSelect7">
                                            @foreach($zones as $zone)
                                                <option value="{{$zone->id}}">{{$zone->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('zone_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @endif

                                @if(isset($currencies))
                                    <div class="form-group">
                                        <label for="exampleSelect5">Currency</label>
            
                                        <select class="form-control @error('currency_id') is-invalid @enderror" name="currency_id" id="exampleSelect5">
                                            @foreach($currencies as $currencie)
                                                <option value="{{$currencie->id}}">{{$currencie->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('currency_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @endif
                            </div><!-- end.col-md-6 -->
                        </div><!-- end.row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div>
                        </div><!-- end.row -->
                    </div>
                </div>
    
            </div>
        </div>
    </form>
@endsection

@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function (){
            $(".select2").select2({ width: '100%' });
        });
    </script>
@endpush






