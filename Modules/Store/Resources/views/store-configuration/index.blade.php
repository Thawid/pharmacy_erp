@extends('dboard.index')

@section('title', 'View Store / Hub Configuration Interface')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row justify-content-center align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Store Configuration Details</h2>
          </div>
          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{ route('storeconfiguration.create') }}">Add New Store</a>
          </div>
          @endcan
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                  <h5>Store List</h5>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                  <h5>Hub List</h6>
                </a>
              </li>
            </ul>
            <div class="tab-content " id="myTabContent">
              <div class="tab-pane fade show active " id="home" role="tabpanel" aria-labelledby="home-tab">

                <div class="row pt-4">
                  <div class="col-md-12">
                    <div class=" ">
                      <table class="table-responsive-xl table-responsive-md table-responsive-lg table-responsive table table-striped table-hover table-bordered " id="StoreTable">
                        <thead>
                          <tr>
                            <th>SL</th>
                            <th>Store Name</th>
                            <th>Warehouse Name</th>
                            <th>Store Hub</th>
                            <th>Store division</th>
                            <th>Store Zone</th>
                            <th>Store Currency</th>
                            <th>Store Policy</th>
                            <th>Address</th>
                            <th>Mobile No</th>
                            <th>Email Address</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if (isset($stores))
                          @foreach ($stores as $key => $store)
                          <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $store->name }}</td>
                            @foreach ($warehouses as $warehouse)
                            @if ($warehouse->id == $store->warehouse_id)
                            <td>{{ $warehouse->name }}</td>
                            @endif
                            @endforeach
                            @if ($store->warehouse_id == '')
                            <td></td>
                            @endif
                            <td>
                              @if (isset($store->hub->name))
                              {{ $store->hub->name }}
                              @endif
                            </td>
                            @if ($store->division_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->division->name }}</td>
                            @endif
                            @if ($store->zone_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->zone->name }}</td>
                            @endif
                            @if ($store->currency_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->currency->name }}</td>
                            @endif
                            @if ($store->policy_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->policy->name }}</td>
                            @endif
                            <td>{{ $store->address }}</td>
                            <td>{{ $store->phone_no }}</td>
                            <td>{{ $store->email }}</td>
                            <td>{{ $store->status == 1 ? 'Active' : 'InActive' }}</td>
                            <td>
                              <div class="d-md-flex justify-content-around align-items-center">


                                @can('hasDeletePermission')
                                <form class="mr-3" id="delete_form{{ $store->id }}" method="POST" action="{{ route('storeconfiguration.destroy', $store->id) }}" onclick="return confirm('Are you sure?')">@csrf
                                  <input name="_method" type="hidden" value="DELETE">
                                  <button class="btn edit-btn" type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                </form>
                                @endcan

                                {{-- @can('hasEditPermission')
                                <a class="btn edit-btn" href="{{ route('storeconfiguration.edit', $store->id) }}"><i class="fa fa-pencil"></i></a>
                                @endcan --}}

                              </div>
                            </td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div><!-- end.table-responsive -->
                  </div><!-- end.col-md-12 -->
                </div><!-- end.row -->
              </div>
              <div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <div class="row pt-4">
                  <div class="col-md-12">
                    <div class="">
                      <table class="table-responsive table-responsive-xl table-responsive-lg table-responsive-md table table-striped table-hover table-bordered" id="HubTable">
                        <thead>
                          <tr>
                            <th>SL</th>
                            <th>Hub Name</th>
                            <th>Hub division</th>
                            <th>Hub Zone</th>
                            <th>Hub Currency</th>
                            <th>Hub Policy</th>
                            <th>Address</th>
                            <th>Mobile No</th>
                            <th>Email Address</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if (isset($hubs))
                          @foreach ($hubs as $key => $store)
                          <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $store->name }}</td>
                            @if ($store->division_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->division->name }}</td>
                            @endif
                            @if ($store->zone_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->zone->name }}</td>
                            @endif
                            @if ($store->currency_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->currency->name }}</td>
                            @endif
                            @if ($store->policy_id == '')
                            <td></td>
                            @else
                            <td>{{ $store->policy->name }}</td>
                            @endif
                            <td>{{ $store->address }}</td>
                            <td>{{ $store->phone_no }}</td>
                            <td>{{ $store->email }}</td>
                            <td>{{ $store->status == 1 ? 'Active' : 'InActive' }}</td>
                            <td>
                              <div class="d-md-flex justify-content-around align-items-center">

                                @can('hasDeletePermission')
                                <form class="mr-3" id="delete_form{{ $store->id }}" method="POST" action="{{ route('storeconfiguration.destroy', $store->id) }}" onclick="return confirm('Are you sure?')">@csrf
                                  <input name="_method" type="hidden" value="DELETE">
                                  <button class="btn edit-btn" type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                </form>
                                @endcan


                                {{-- @can('hasEditPermission')
                                <a class="btn edit-btn" href="{{ route('storeconfiguration.edit', $store->id) }}"><i class="fa fa-pencil"></i></a>
                                @endcan --}}


                              </div>

                            </td>

                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div><!-- end.col-md-12 -->
                </div><!-- end.row -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  @endsection

  @push('post_scripts')
  <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>

  <script>
    $('.select2').select2();
  </script>
  <script>
    $(document).ready(function() {
      $('#StoreTable').DataTable();
      $('#HubTable').DataTable();
    });
    requisition
  </script>

  @endpush