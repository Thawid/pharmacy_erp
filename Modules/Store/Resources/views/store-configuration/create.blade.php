{{--<h1>createvvvvv</h1>--}}
@extends('dboard.index')

@section('title','Create New Store Form')

@section('dboard_content')
   
    <form method="POST" action="{{route('storeconfiguration.store')}}">
        @csrf
        <div class="tile">
            <div class="tile-body">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-6 text-left">
                        <h2 class="title-heading">Store Mapping</h2>
                    </div>
                    <div class="col-md-6 text-md-right">
                        <a class="btn index-btn" href="{{route('storeconfiguration.index')}}">Back</a>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        
                            @if(isset($stores))
                                <div class="form-group">
                                    <label for="exampleSelect1">Store Type</label>
                                    <select class="form-control @error('store_type') is-invalid @enderror" name="store_type" onchange="getData(this)">
                                        <option selected="true" disabled="disabled">Select Store Type</option>
                                        <option value="hub">Hub</option>
                                        <option value="store">Store</option>
                                    </select>
                                    @error('store_type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleSelect1">Store / Hub</label>
                                    <select class="select2 form-control @error('name') is-invalid @enderror" name="store_id" id="selectedType">
                                        <option selected="true" disabled="disabled">Select Store/Hub</option>
                                    </select>
                                    @error('store_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            @endif
        
                            
        
                            @if(isset($warehouses))
                                <div class="form-group" id="warehouse">
                                    <label for="exampleSelect2">Select Warehouse</label>
                                    <select class="form-control @error('warehouse_id') is-invalid @enderror" name="warehouse_id" id="selectedStore">
                                        <option selected="true" disabled="disabled">Select</option>
                                    </select>
                                    @error('warehouse_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            @endif
        
                            
                            @if(isset($divisions))
                                <div class="form-group">
                                    <label for="exampleSelect4">Division</label>
                                    <select class="form-control @error('division_id') is-invalid @enderror" name="division_id">
                                        <option selected="true" disabled="disabled">Select Division</option>
                                        @foreach($divisions as $storeType)
                                            <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('division_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            @endif
                            
                            
                    </div><!-- end.col-md-6 -->
                    <div class="col-md-6">
                        @if(isset($hubs))
                            <div class="form-group" id="hub">
                                <label for="exampleSelect1">Store Hub</label>
                                <select class="form-control @error('hub_id') is-invalid @enderror" name="hub_id" onchange="getStore(this)">
                                    <option selected="true" disabled="disabled">Select Hub</option>
                                    @foreach($hubs as $hub)
                                        <option value="{{$hub->id}}">{{$hub->name}}</option>
                                    @endforeach
                                </select>
                                @error('hub_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        @endif

                        @if(isset($policies))
                            <div class="form-group">
                                <label for="exampleSelect3">Policy</label>
                                <select class="form-control @error('policy_id') is-invalid @enderror" name="policy_id">
                                    <option selected="true" disabled="disabled">Select Policy</option>
                                    @foreach($policies as $storeType)
                                        <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                    @endforeach
                                </select>
                                @error('policy_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        @endif

                        @if(isset($zones))
                            <div class="form-group">
                                <label for="exampleSelect4">Zone</label>
                                <select class="form-control @error('zone_id') is-invalid @enderror" name="zone_id">
                                    <option selected="true" disabled="disabled">Select Zone</option>
                                    @foreach($zones as $storeType)
                                        <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                    @endforeach
                                </select>
                                @error('zone_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        @endif

                        @if(isset($currencies))
                            <div class="form-group">
                                <label for="exampleSelect5">Currency</label>
                                <select class="form-control @error('currency_id') is-invalid @enderror" name="currency_id">
                                    <option selected="true" disabled="disabled">Select Currency</option>
                                    @foreach($currencies as $storeType)
                                        <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                    @endforeach
                                </select>
                                @error('currency_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        @endif
                    </div><!-- end.col-md-6 -->
                </div><!-- end.row -->
                <hr>

                <div class="row text-right">
                    <div class="col-md-12">
                        <button class="btn create-btn" type="submit">Submit</button>
                    </div>
                </div><!-- end.row -->
            </div>
        </div>
    </form>
@endsection

@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function (){
            $(".select2").select2({ width: '100%' });
        });
    </script>


    <script>
        function getData(type){
            var value = type.value;
            var selectType = document.getElementById('selectedType');
            selectType.innerHTML = "";
            if (value == 'hub'){
                document.getElementById('warehouse').style.display = 'none';
                document.getElementById('hub').style.display = 'none';
            }else {
                document.getElementById('warehouse').style.display = 'block';
                document.getElementById('hub').style.display = 'block';
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('get_type') }}",
                method: 'get',
                data: {
                    type: value
                },
                success: function(result) {
                  let dummySelectOption = document.createElement('option');
                  dummySelectOption.value = "Select Store/Hub";
                  dummySelectOption.innerHTML = "Select Store/Hub";
                  selectType.append(dummySelectOption);
                    for (var i=0; i < result.data.length; i++) {
                        opt = document.createElement('option');
                        opt.value = result.data[i].id;
                        opt.innerHTML = result.data[i].name;
                        selectType.append(opt);
                        /*html =  '<option value="'+result.data[i].id+'">'+result.data[i].name+'</option>'
                        selectType.add(html);*/
                    }
                }
            });
        }
    </script>
    <script>
        function getStore(store){
            var value = store.value;
            // alert(value);
            var selectStore = document.getElementById('selectedStore');
            selectStore.innerHTML = "";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('get_store') }}",
                method: 'get',
                data: {
                    store: value
                },
                success: function(result) {
                    console.log(result)
                    for (var i=0; i<result.data.length; i++){
                        opt = document.createElement('option');
                        opt.value = result.data[i].id;
                        opt.innerHTML = result.data[i].name;
                        selectStore.appendChild(opt);
                        /*html =  '<option value="'+result.data[i].id+'">'+result.data[i].name+'</option>'
                        selectType.add(html);*/
                    }
                }
            });
        }
    </script>
@endpush
