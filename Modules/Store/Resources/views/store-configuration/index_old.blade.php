@extends('dboard.index')

@section('title','View Store Interface')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <h2>Store Configuration Details</h2>
                        </div>
                        @can('hasCreatePermission')
                         <div class="col-md-6 text-right">
                            <a class="btn btn-primary icon-btn" href="{{route('storeconfiguration.create')}}"><i class="fa fa-plus"></i>Add New Store</a>
                         </div>
                        @endcan


                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row" id="actionModal">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="StoreTable">
                            <thead class="thead">
                            <tr>
                                <th>SL</th>
                                <th>Store Name</th>
                                <th>Warehouse Name</th>
                                <th>Store Hub</th>
                                <th>Store division</th>
                                <th>Store Zone</th>
                                <th>Store Currency</th>
                                <th>Store Policy</th>
                                <th>Address</th>
                                <th>Mobile No</th>
                                <th>Email Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($stores))
                                @foreach($stores as $key => $store)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{$store->name}}</td>
                                        @foreach($warehouses as $warehouse)
                                            @if($warehouse->id == $store->warehouse_id)
                                                <td>{{$warehouse->name}}</td>
                                            @endif
                                        @endforeach
                                        @if($store->warehouse_id == '')
                                            <td></td>
                                        @endif
                                        <td>
                                            @if(isset($store->hub->name))
                                                {{$store->hub->name}}
                                            @endif
                                        </td>
                                        @if($store->division_id == '')
                                            <td></td>
                                        @else
                                            <td>{{$store->division->name}}</td>
                                        @endif
{{--                                        <td>Division</td>--}}
{{--                                         <td>{{$store->zone->name}}</td>--}}
                                        @if($store->zone_id == '')
                                            <td></td>
                                        @else
                                            <td>{{$store->zone->name}}</td>
                                        @endif
{{--                                        <td>Zone</td>--}}
{{--                                         <td>{{$store->currency->name}}</td>--}}
                                        @if($store->currency_id == '')
                                            <td></td>
                                        @else
                                            <td>{{$store->currency->name}}</td>
                                        @endif
{{--                                        <td>currency</td>--}}
{{--                                         <td>{{$store->policy->name}}</td>--}}
{{--                                        <td>Policy</td>--}}
                                        @if($store->policy_id == '')
                                            <td></td>
                                        @else
                                            <td>{{$store->policy->name}}</td>
                                        @endif

                                        <td>{{$store->address}}</td>
                                        <td>{{$store->phone_no}}</td>
                                        <td>{{$store->email}}</td>
                                        <td>{{$store->status == 1 ? 'Active' : 'InActive' }}</td>
                                        <td class="d-md-flex justify-content-center">
                                            @can('hasDeletePermission')
                                            <form class="mr-3" id="delete_form{{$store->id}}" method="POST" action="{{ route('storeconfiguration.destroy',$store->id) }}" onclick="return confirm('Are you sure?')">@csrf
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-primary" type="submit">Change Status</button>
                                            </form>
                                            @endcan

                                            @can('hasEditPermission')
{{--                                            <a class="btn btn-primary" href="{{route('storeconfiguration.edit',$store->id)}}"><i class="fa fa-lg fa-edit">Store Configuration</i></a>--}}
                                            @endcan

                                        </td>
                                    </tr>
                                @endforeach

                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">$('#StoreTable').DataTable();</script>
@endpush
