{{--<h1>createvvvvv</h1>--}}
@extends('dboard.index')

@section('title','Create New Store Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Store Mapping</h2>
                        </div>
                        <div class="col-md-12 text-center">
                            <a class="btn btn-primary icon-btn" href="{{route('storeconfiguration.index')}}"><i class="fa fa-arrow-left"></i>Back</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    {{--    <form method="POST" action="{{route('storeconfiguration.update', $stores->id)}}">--}}
    <form method="POST" action="{{route('storeconfiguration.store')}}">
        @csrf
        {{--        @method('PUT')--}}
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    {{--                    <div class="form-group">--}}
                    {{--                        <label class="control-label" >New Store Name</label>--}}
                    {{--                        <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter your name" value="{{ old('name') }}">--}}
                    {{--                        @php $i = 1 @endphp--}}
                    {{--                        <input type="hidden" name="code" value="{{rand()}}">--}}
                    {{--                        @error('name')--}}
                    {{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
                    {{--                        @enderror--}}
                    {{--                    </div>--}}

                    @if(isset($stores))
                        <div class="form-group">
                            <label for="exampleSelect1">Store</label>
                            <select class="form-control @error('name') is-invalid @enderror" name="store_id" id="exampleSelect1" value="">
                                <option selected="true" disabled="disabled">Select Store</option>
                                @foreach($stores as $storeType)
                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                    {{--                                    <input type="hidden" name="store_id" value="{{$stores->id}}">--}}
                                    {{--                                    <input type="hidden" name="address" value="{{$stores->address}}">--}}
                                    {{--                                    <input type="hidden" name="store_type_id" value="{{$stores->store_type_id}}">--}}
                                    {{--                                    <input type="hidden" name="email" value="{{$stores->email}}">--}}
                                    {{--                                    <input type="hidden" name="phone_no" value="{{$stores->phone_no}}">--}}
                                    {{--                                    <input type="hidden" name="status" value="{{$stores->status}}">--}}
                                    {{--                                    <input type="hidden" name="code" value="{{$stores->code}}">--}}
                                @endforeach
                            </select>
                            @error('store_type_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                    {{--                    @if(isset($storeTypes))--}}
                    {{--                        <div class="form-group">--}}
                    {{--                            <label for="exampleSelect2">Store Type</label>--}}
                    {{--                            <select class="form-control @error('parent_store_id') is-invalid @enderror" name="parent_store_id" id="exampleSelect2" value="">--}}
                    {{--                                <option selected="true" disabled="disabled">Select Store Type</option>--}}
                    {{--                                @foreach($storeTypes as $storeType)--}}
                    {{--                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                            @error('parent_store_id')--}}
                    {{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
                    {{--                            @enderror--}}
                    {{--                        </div>--}}
                    {{--                    @endif--}}
                    @if(isset($warehouses))
                        <div class="form-group">
                            <label for="exampleSelect2">Warehouse</label>
                            <select class="form-control @error('warehouse_id') is-invalid @enderror" name="warehouse_id" id="exampleSelect2" value="">
                                <option selected="true" disabled="disabled">Select Warehouse</option>
                                @foreach($warehouses as $warehouse)
                                    <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                                @endforeach
                            </select>
                            @error('warehouse_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                    @if(isset($storeTypes))
                        <div class="form-group">
                            <label for="exampleSelect1">Store Hub</label>
                            <select class="form-control @error('hub_id') is-invalid @enderror" name="hub_id" id="exampleSelect1" value="">
                                <option selected="true" disabled="disabled">Select Hub</option>
                                @foreach($storeTypes as $storeType)
                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                @endforeach
                            </select>
                            @error('hub_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                    @if(isset($policies))
                        <div class="form-group">
                            <label for="exampleSelect3">Policy</label>
                            <select class="form-control @error('policy_id') is-invalid @enderror" name="policy_id" id="exampleSelect3" value="">
                                <option selected="true" disabled="disabled">Select Policy</option>
                                @foreach($policies as $storeType)
                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                @endforeach
                            </select>
                            @error('policy_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                    @if(isset($divisions))
                        <div class="form-group">
                            <label for="exampleSelect4">Division</label>
                            <select class="form-control @error('division_id') is-invalid @enderror" name="division_id" id="exampleSelect4" value="">
                                <option selected="true" disabled="disabled">Select Policy</option>
                                @foreach($divisions as $storeType)
                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                @endforeach
                            </select>
                            @error('division_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                    @if(isset($zones))
                        <div class="form-group">
                            <label for="exampleSelect4">Zone</label>
                            <select class="form-control @error('zone_id') is-invalid @enderror" name="zone_id" id="exampleSelect4" value="">
                                <option selected="true" disabled="disabled">Select Policy</option>
                                @foreach($zones as $storeType)
                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                @endforeach
                            </select>
                            @error('zone_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                    @if(isset($currencies))
                        <div class="form-group">
                            <label for="exampleSelect5">Currency</label>
                            <select class="form-control @error('currency_id') is-invalid @enderror" name="currency_id" id="exampleSelect5" value="">
                                <option selected="true" disabled="disabled">Select Policy</option>
                                @foreach($currencies as $storeType)
                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>
                                @endforeach
                            </select>
                            @error('currency_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                    {{--                    <div class="form-group">--}}
                    {{--                        <label class="control-label" >Address</label>--}}
                    {{--                        <input class="form-control @error('address') is-invalid @enderror" name="address" type="text" placeholder="Enter your address" value="{{ old('address') }}">--}}
                    {{--                        @error('address')--}}
                    {{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
                    {{--                        @enderror--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group">--}}
                    {{--                        <label class="control-label" >Mobile No</label>--}}
                    {{--                        <input class="form-control @error('phone_no') is-invalid @enderror" name="phone_no" type="text" placeholder="Enter your mobile no" value="{{ old('phone_no') }}">--}}
                    {{--                        @error('phone_no')--}}
                    {{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
                    {{--                        @enderror--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group">--}}
                    {{--                        <label class="control-label" >Email Address</label>--}}
                    {{--                        <input class="form-control @error('email') is-invalid @enderror" name="email" type="email" placeholder="Enter your email" value="{{ old('email') }}">--}}
                    {{--                        @error('email')--}}
                    {{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
                    {{--                        @enderror--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group">--}}
                    {{--                        @include('store::shares.status')--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile-footer">
                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </form>
@endsection
