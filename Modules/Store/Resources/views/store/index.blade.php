@extends('dboard.index')

@section('title','View Store Interface')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row justify-content-center align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Store Details</h2>
          </div>
          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{route('store.create')}}">Add New Store</a>
          </div>
          @endcan
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive table-responsive-xl table-responsive-lg table-responsive-md">
              <table class="table table-hover table-striped table-bordered table-responsive-xl table-responsive-lg table-responsive-md" id="StoreTypeTable">
                <thead>
                  <tr>
                    <th>SL</th>
                    <th>Store Name</th>
                    <th>Store Type</th>
                    <th>Address</th>
                    <th>Mobile No</th>
                    <th>Email Address</th>
                    <th>Status</th>
                    <th width="10%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(isset($stores))
                  @foreach($stores as $key => $store)
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{$store->name}}</td>
                    {{-- <td>{{$store->type->name}}</td>--}}
                    <td>
                      @if($store->store_type == 'hub')
                      Hub
                      @else
                      Store
                      @endif
                    </td>
                    <td>{{$store->address}}</td>
                    <td>{{$store->phone_no}}</td>
                    <td>{{$store->email}}</td>
                    <td>
                      @if ($store->status == 1)
                      <span class="badge badge-success m-1 p-2">Active</span>
                      @else
                      <span class="badge badge-danger m-1 p-2">Inactive</span>
                      @endif
                    </td>
                    <td class="d-md-flex">
                      @can('hasEditPermission')
                      <a class="btn edit-btn mr-3" href="{{route('store.edit',$store->id)}}"><i class="fa fa-lg fa-pencil"></i></a>
                      @endcan
                      @can('hasDeletePermission')
                      <form class="mr-3" id="delete_form{{$store->id}}" method="POST" action="{{ route('store.destroy',$store->id) }}" onclick="return confirm('Are you sure?')">@csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn edit-btn" type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                      </form>
                      @endcan
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!-- end.table-responsive -->
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $('#StoreTypeTable').DataTable();
</script>
@endpush