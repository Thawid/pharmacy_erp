@extends('dboard.index')

@section('title','Create New Store Form')

@section('dboard_content')

<form method="POST" action="{{ route('store.store') }}">
  @csrf
  <div class="tile">
    <div class="tile-body">
      <div class="row justify-content-between align-items-center">
        <div class="col-md-6">
          <h2 class="title-heading">Create New Store</h2>
        </div>
        <div class="col-md-6 text-md-right">
          <a class="btn index-btn" href="{{route('store.index')}}"> Back</a>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">New Store Name</label>
            <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter your name" value="{{ old('name') }}">
            @php $i = 1 @endphp
            <input type="hidden" name="code" value="{{rand()}}">
            {{-- @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}
          </div>

          <div class="form-group">
            <label class="control-label">Address</label>
            <input class="form-control @error('address') is-invalid @enderror" name="address" type="text" placeholder="Enter your address" value="{{ old('address') }}">
            {{-- @error('address')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}
          </div>

          <div class="form-group">
            <label class="control-label">Email Address</label>
            <input class="form-control @error('email') is-invalid @enderror" name="email" type="email" placeholder="Enter your email" value="{{ old('email') }}">
            {{-- @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}
          </div>


        </div><!-- end.col-md-6 -->

        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleSelect1">Store Type</label>
            <select class="form-control @error('store_type') is-invalid @enderror" name="store_type" id="exampleSelect1" value="{{ old('store_type') }}">
              <option disabled selected>Select Store Type</option>

              <option value="hub">Hub</option>
              <option value="store">Store</option>
            </select>
            {{-- @error('store_type')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}
          </div><!-- end.form-group -->

          <div class="form-group">
            <label class="control-label">Mobile No</label>
            <input class="form-control @error('phone_no') is-invalid @enderror" name="phone_no" type="text" placeholder="Enter your mobile no" value="{{ old('phone_no') }}">
            {{-- @error('phone_no')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}
          </div><!-- end.form-group -->

          <div class="form-group">
            @include('store::shares.status')
          </div><!-- end.form-group -->
        </div><!-- end.col-md-6 -->
      </div>
      <hr>

      <div class="row text-right">
        <div class="col-md-12">
          <button class="btn create-btn" type="submit">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection