@extends('dboard.index')

{{--@section('title','Create New Store Form')--}}

{{--@section('dboard_content')--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="tile">--}}
{{--                <div class="tile-body">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-9 text-left">--}}
{{--                            <h2>Create New Store</h2>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-12 text-center">--}}
{{--                            <a class="btn btn-primary icon-btn" href=""><i class="fa fa-arrow-left"></i>Back</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
{{--    <form method="" action="" >--}}
{{--        @csrf--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="tile">--}}
{{--                    <div class="form-group">--}}
{{--                        <label class="control-label" >New Store Name</label>--}}
{{--                        <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter your name" value="{{ old('name') }}">--}}
{{--                        @php $i = 1 @endphp--}}
{{--                        <input type="hidden" name="code" value="{{rand()}}">--}}
{{--                        @error('name')--}}
{{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    @if(isset($storeTypes))--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="exampleSelect1">Store Type</label>--}}
{{--                            <select class="form-control @error('store_type_id') is-invalid @enderror" name="store_type_id" id="exampleSelect1" value="{{ old('store_type_id') }}">--}}
{{--                                <option selected="true" disabled="disabled">Select Store Type</option>--}}
{{--                                @foreach($storeTypes as $storeType)--}}
{{--                                    <option value="{{$storeType->id}}">{{$storeType->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                            @error('store_type_id')--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                    <div class="form-group">--}}
{{--                        <label class="control-label" >Address</label>--}}
{{--                        <input class="form-control @error('address') is-invalid @enderror" name="address" type="text" placeholder="Enter your address" value="{{ old('address') }}">--}}
{{--                        @error('address')--}}
{{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label class="control-label" >Mobile No</label>--}}
{{--                        <input class="form-control @error('phone_no') is-invalid @enderror" name="phone_no" type="text" placeholder="Enter your mobile no" value="{{ old('phone_no') }}">--}}
{{--                        @error('phone_no')--}}
{{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label class="control-label" >Email Address</label>--}}
{{--                        <input class="form-control @error('email') is-invalid @enderror" name="email" type="email" placeholder="Enter your email" value="{{ old('email') }}">--}}
{{--                        @error('email')--}}
{{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        @include('store::shares.status')--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="tile-footer">--}}
{{--                    <button class="btn btn-primary btn-block" type="submit">Create New Store</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </form>--}}
@endsection
