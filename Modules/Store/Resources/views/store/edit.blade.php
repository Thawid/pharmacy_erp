@extends('dboard.index')

@section('title','Edit New Store Form')

@section('dboard_content')

    <form method="POST" action="{{route('store.update', $store->id)}}">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-6">
                                <h2 class="title-heading">Edit New Store</h2>
                            </div>
                            <div class="col-md-6 text-right">
                                <a class="btn index-btn" href="{{route('store.index')}}">Back</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >New Store Name</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter your name" value="{{ $store->name }}">
                                    @php $i = 1 @endphp
                                    <input type="hidden" name="code" value="{{rand()}}">
                                    @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div><!-- end.form-group -->
                                    
                                <div class="form-group">
                                    <label class="control-label" >Address</label>
                                    <input class="form-control @error('address') is-invalid @enderror" name="address" type="text" placeholder="Enter your address" value="{{ $store->address }}">
                                    @error('address')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div><!-- end.form-group -->
                                
                                <div class="form-group">
                                    <label class="control-label" >Email Address</label>
                                    <input class="form-control @error('email') is-invalid @enderror" name="email" type="email" placeholder="Enter your email" value="{{ $store->email }}">
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div><!-- end.form-group -->
                            </div><!-- end.col-md-6 -->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleSelect1">Store Type</label>

                                    <select class="form-control @error('store_type') is-invalid @enderror" name="store_type" id="exampleSelect1" value="{{ old('store_type') }}">
                                            <option value="hub" {{ $store->store_type == 'hub' ? 'selected' : '' }}>Hub</option>
                                            <option value="store" {{ $store->store_type == 'store' ? 'selected' : '' }}>Store</option>
                                    </select>
                                    @error('store_type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div><!-- end.form-group -->

                                <div class="form-group">
                                    <label class="control-label" >Mobile No</label>
                                    <input class="form-control @error('phone_no') is-invalid @enderror" name="phone_no" type="text" placeholder="Enter your mobile no" value="{{ $store->phone_no }}">
                                    @error('phone_no')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div><!-- end.form-group -->

                                <div class="form-group">
                                    <label for="status" class="forget-form">Select Status</label>
                                    <select class="js-example-basic-single form-control" name="status">
                                        <option value="1" {{ $store->status == 1 ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ $store->status == 0 ? 'selected' : '' }}>InActive</option>
                                    </select>
                                </div><!-- form-group -->
                            </div><!-- end.col-md-6 -->
                        </div><!-- end.row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update New Store</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
@endsection






