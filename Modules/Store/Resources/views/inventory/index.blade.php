@extends('dboard.index')

@section('title','Store Report | Procurement')

@section('dboard_content')
    @push('styles')
        <style>
            @media print {
                @page {
                    margin: 20px;
                }

            }
            table {
                text-align: center;
                margin: 0;
                padding: 0;
                width: 100%;
            }

            .th-table-header, .th-table-info, .th-table-body {
                margin-bottom: 20px;
            }

            .th-table-header table tr td h1 {
                margin: 0;
                font-size: 35px;
            }

            .th-table-header table tr td h2 {
                margin: 0;
                font-size: 18px;
            }

            .th-table-header table tr td h3 {
                padding: 0;
                margin: 0;
                font-size: 24px;
            }

            .th-table-info p {
                margin: 0;
            }

            .th-table-info table tr td p {
                text-align: left;
            }
            .th-table-info table {
                text-align: center !important;
            }

            .th-table-header table tr td p {
                padding: 0;
                margin: 0;
                font-size: 16px;
            }

        </style>

    @endpush
    <div class="tile">
        <div class="tile-body">
            <div class="row">
                <div class="col-md-6 text-left">
                    <h2>Store Stock </h2>
                </div>
            </div>

        </div>
    </div>
    <div class="tile">
        <div class="tile-body">
                <div class="table-responsive bg-white p-3">
                    <table class="table table-striped table-hover store-stock" id="store-stock">
                        <thead class="thead">
                        <tr>
                            <th>SL</th>
                            <th>Product Name</th>
                            <th>Generic Name</th>
                            <th>Manufacturer</th>
                            <th>Available Stock</th>
                            <th>UOM</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--{{ dd($data) }}--}}
                        @if(!empty($data))
                            @foreach($data as $report)
                                <tr>
                                    <td> {{ $loop->iteration }}</td>

                                    <td> {{ $report->product_name }}</td>
                                    <td>{{ $report->generic_name }}</td>
                                    <td>{{ $report->manufacturer_name }}</td>
                                    <td>{{ $report->available_quantity }}</td>
                                    <td>{{ $report->uom_name }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>No Data Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
        </div>
    </div>



@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>

    <script type="text/javascript">$('.select2').select2();</script>
    <script>
        $(document).ready(function() {
            $('#store-stock').DataTable();
        });
    </script>

@endpush
