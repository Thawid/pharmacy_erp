@extends('dboard.index')

@section('title','Edit Store Type Form')

@section('dboard_content')
    
    <form method="POST" action="{{route('storetype.update', $storeTypes->id)}}">
        @csrf
        @method('PUT')
        
        <div class="tile">
            <div class="tile-body">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Edit Store Type</h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('storetype.index')}}"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" >Store Type</label>
                            <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter your name" value="{{$storeTypes->name}}">
    {{--                        @error('name')--}}
    {{--                        <div class="alert alert-danger">{{ $message }}</div>--}}
    {{--                        @enderror--}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status" class="forget-form">Select Status</label>
                            <select class="js-example-basic-single form-control" name="status">
                                <option value="1" {{ $storeTypes->status == 1 ? 'selected' : '' }}>Active</option>
                                <option value="0" {{ $storeTypes->status == 0 ? 'selected' : '' }}>InActive</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn create-btn" type="submit">Update Store Type</button>
                    </div>
                </div>

            </div>
        </div>
      
    </form>
@endsection






