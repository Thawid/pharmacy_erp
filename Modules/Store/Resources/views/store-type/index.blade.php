@extends('dboard.index')

@section('title','View Store Type Interface')

@push('styles')
<style>
  /* responsive design */
  /* Large devices (desktops, 992px and up) */
  @media (min-width: 992px) and (max-width: 1199px) {
    
  }

  /* Extra large devices (large desktops, 1200px and up) */
  @media (min-width: 1200px) {}
</style>
@endpush

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row justify-content-center align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Store Type Details</h2>
          </div>
          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{route('storetype.create')}}">Add Store Type</a>
          </div>
          @endcan
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive-md table-responsive-sm table-responsive-xl">
              <table class="table table-hover table-responsive-md table-responsive-sm table-responsive-xl table-bordered" id="StoreTypeTable">
                <thead class="thead">
                  <tr>
                    <th width="10%">SL</th>
                    <th width="50%">Store Type</th>
                    <th width="30%">Status</th>
                    <th width="10%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(isset($store_types))
                  @foreach($store_types as $key => $store_type)
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{$store_type->name}}</td>
                    <td>
                      @if ($store_type->status == 1)
                      <span class="badge badge-success m-1 p-2">Active</span>
                      @else
                      <span class="badge badge-danger m-1 p-2">Inactive</span>
                      @endif
                    </td>
                    <td>
                      @can('hasEditPermission')
                      <div class="d-flex justify-content-around align-items-center">
                        <a class="btn edit-btn" href="{{route('storetype.edit',$store_type->id)}}"><i class="fa fa-lg fa-pencil"></i></a>
                        @endcan
                        @can('hasDeletePermission')
                        <form class="" id="delete_form{{$store_type->id}}" method="POST" action="{{ route('storetype.destroy',$store_type->id) }}" onclick="return confirm('Are you sure?')">@csrf
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn edit-btn" type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                        </form>
                      </div>
                      @endcan
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!-- end.table-responsive -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $('#StoreTypeTable').DataTable();
</script>
@endpush