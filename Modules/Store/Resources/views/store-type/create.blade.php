@extends('dboard.index')

@section('title','Create Store Type Form')

@section('dboard_content')

    <form method="POST" action="{{ route('storetype.store') }}" >
        @csrf
        <div class="tile">
            <div class="tile-body">
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Create Store Type</h2>
                    </div>
                        <div class="col-md-6 text-md-right">
                            <a class="btn index-btn" href="{{route('storetype.index')}}"> Back</a>
                        </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" >Store Type</label>
                             <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter Store type name" value="{{ old('name') }}">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            @include('store::shares.status')
                        </div>
                    </div>
                </div>
                <div class="row text-right">
                    <div class="col-md-12 ">
                        <button class="btn create-btn" type="submit">Create Store Type</button>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
