@extends('dboard.index')

@section('title','Create New Store User Form')

@section('dboard_content')
    <div class="tile">
        <div class="tile-body">
            <!-- title -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h2 class="title-heading">Create New Store User</h2>
                </div>
                <div class="col-md-6 text-md-right">
                    <a class="btn index-btn" href="{{route('storeuser.index')}}">Back</a>
                </div>
            </div><!-- end.row -->
            <hr>

            <!-- body -->
            <form method="POST" action="{{ route('storeuser.store') }}" >
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleSelect1">Type</label>
                        <select class="form-control @error('store_type') is-invalid @enderror" name="store_type" onchange="getData(this)">
                            <option selected="true" disabled="disabled">Select Type</option>
                            <option value="hub">Hub</option>
                            <option value="store">Store</option>
                        </select>
                        @error('store_type')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div><!-- end.form-group -->

                    

                    @if(isset($user))
                        <div class="form-group">
                            <label for="exampleSelect1">User</label>
                            <select class="form-control @error('user_id') is-invalid @enderror" name="user_id" id="exampleSelect1" value="{{ old('user_id') }}">
                                <option selected="true" disabled="disabled">Select User</option>
                                @foreach($user as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div><!-- end.form-group -->
                    @endif

                    
                </div><!-- end.col-md-6 -->

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleSelect1">Store / Hub</label>
                        <select class="select2 form-control @error('name') is-invalid @enderror" name="store_id" id="selectedType">
                            <option selected="true" disabled="disabled">Select Store / Hub</option>
                        </select>
                        @error('store_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div><!-- end.form-group -->

                    <div class="form-group">
                        @include('store::shares.status')
                  </div><!-- end.form-group -->
                </div><!-- end.col-md-6 -->
            </div><!-- end.row -->
            <hr>

            <div class="row text-right">
                <div class="col-md-12">
                    <button class="btn create-btn" type="submit">Submit</button>
                </div><!-- end.col-md-12 -->
            </div><!-- end.row -->
        </form>
    </div><!-- end.tile-body -->
</div><!-- end.tile -->

@endsection


@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function (){
            $(".select2").select2({ width: '100%' });
        });
    </script>

    <script>
        function getData(type){
            var value = type.value;
            var selectType = document.getElementById('selectedType');
            selectType.innerHTML = "";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('g_type') }}",
                method: 'get',
                data: {
                    type: value
                },
                success: function(result) {
                  let dummySelectOption = document.createElement('option');
                  dummySelectOption.value = "Select Store/Hub";
                  dummySelectOption.innerHTML = "Select Store/Hub";
                  selectType.append(dummySelectOption);
                    for (var i=0; i<result.data.length; i++){
                        opt = document.createElement('option');
                        opt.value = result.data[i].id;
                        opt.innerHTML = result.data[i].name;
                        selectType.appendChild(opt);
                        /*html =  '<option value="'+result.data[i].id+'">'+result.data[i].name+'</option>'
                        selectType.add(html);*/
                    }
                }
            });
        }
    </script>
@endpush


