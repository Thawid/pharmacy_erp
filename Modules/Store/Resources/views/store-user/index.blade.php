@extends('dboard.index')

@section('title','View Store User Interface')

@section('dboard_content')

<div class="row" id="actionModal">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Store User Details</h2>
          </div>
          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{route('storeuser.create')}}">Add New Store User</a>
          </div>
          @endcan
        </div><!-- end.row -->
        <hr>

        <!-- table -->
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-striped table-bordered" id="StoreTypeTable">
                <thead>
                  <tr>
                    <th>SL</th>
                    <th>Store Type</th>
                    <th>Store Name</th>
                    <th>User Name</th>
                    <th>User Role</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(isset($storeUsers))
                  @foreach($storeUsers as $key => $storeUser)
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{$storeUser->store_type}}</td>
                    <td>{{$storeUser->store->name}}</td>
                    <td>{{$storeUser->user->name}}</td>
                    <td>{{$storeUser->user->role}}</td>
                    <td>{{$storeUser->status == 1 ? 'Active' : 'InActive' }}</td>
                    <td>
                      <div class="d-flex justify-content-around align-items-center">
                        @can('hasDeletePermission')
                        <form class="mr-3" id="delete_form{{$storeUser->id}}" method="POST" action="{{ route('storeuser.destroy',$storeUser->id) }}" onclick="return confirm('Are you sure?')">@csrf
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn edit-btn" type="submit"><i class="fa fa-refresh"></i></button>
                        </form>
                        @endcan

                        @can('hasEditPermission')
                        <a class="btn edit-btn" href="{{route('storeuser.edit',$storeUser->id)}}"><i class="fa fa-pencil"></i></a>
                        @endcan
                      </div>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!-- end.table-responsive -->
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->
@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $('#StoreTypeTable').DataTable();
</script>
@endpush