
@extends('dboard.index')

@section('title','Edit New Store Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <!-- title -->
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="title-heading">Edit New Store</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('storeuser.index')}}">Back</a>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>

                    <!-- form -->
                    <form method="POST" action="{{route('storeuser.update', $storeUsers->id)}}">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleSelect1">Select Type</label>
                                    <select class="form-control @error('store_type') is-invalid @enderror" name="store_type" onchange="getData(this)">
                                        <option selected="true" disabled="disabled">{{ $stype->store_type }}</option>
                                        <option value="hub">Hub</option>
                                        <option value="store">Store</option>
                                    </select>
                                    @error('store_type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div><!-- end.form-group -->
                            </div><!-- end.col-md-4 -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleSelect1">Select Store / Hub</label>
                                    <select class="select2 form-control @error('name') is-invalid @enderror" name="store_id" id="selectedType">
                                        <option selected="true" disabled="disabled">{{ $stype->name }}</option>
                                    </select>
                                    @error('store_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div><!-- end.form-group -->
                            </div><!-- end.col-md-4 -->

                            <div class="col-md-4">
                                @if(isset($users))
                                    <div class="form-group">
                                        <label for="exampleSelect1">Store User</label>
                                        <select class="form-control @error('store_id') is-invalid @enderror" name="user_id" id="exampleSelect1">
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}" @if($user->id == $storeUsers->user_id) selected @endif> {{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('store_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div><!-- end.form-group -->
                                @endif
                            </div><!-- end.col-md-4 -->
                        </div><!-- end.row -->
                        <hr>

                        <!-- button -->
                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update New Store</button>
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </form>
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
@endsection

@push('post_scripts')
    <!-- select2 -->
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <!-- select2 configuration -->
    <script>
        $(document).ready(function (){
            $(".select2").select2({ width: '100%' });
        });
    </script>

    <script>
        function getData(type){
            var value = type.value;
            var selectType = document.getElementById('selectedType');
            selectType.innerHTML = "";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('g_type') }}",
                method: 'get',
                data: {
                    type: value
                },
                success: function(result) {
                  let dummySelectOption = document.createElement('option');
                  dummySelectOption.value = "Select Store/Hub";
                  dummySelectOption.innerHTML = "Select Store/Hub";
                  selectType.append(dummySelectOption); 
                    for (var i=0; i<result.data.length; i++){
                        opt = document.createElement('option');
                        opt.value = result.data[i].id;
                        opt.innerHTML = result.data[i].name;
                        selectType.appendChild(opt);
                        /*html =  '<option value="'+result.data[i].id+'">'+result.data[i].name+'</option>'
                        selectType.add(html);*/
                    }
                }
            });
        }
    </script>
@endpush




