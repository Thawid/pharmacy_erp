<label for="status" class="forget-form">Status</label>
<select class="form-control select2" name="status" required>
  <option value="" disabled selected>Select Status</option>
  <option value="1" @if (old('status')=='1' ) selected="selected" @endif>Active</option>
  <option value="0" @if (old('status')=='0' ) selected="selected" @endif>InActive</option>
</select>