{{--<ul>--}}
{{--    @foreach($store_types as $store_type)--}}
{{--        <li>{{$store_type->name}}</li>--}}
{{--    @endforeach--}}
{{--</ul>--}}
{{--<label for="product_type" class="forget-form">Product Type*</label>--}}
{{--<select class="form-control select2 product-type single-select" name="{{ $field?? 'product_type_id' }}" id="product_type" onchange="generalInfo();">--}}
{{--    <option value="">Select Product Type</option>--}}
{{--    @foreach($product_types as $product_type)--}}
{{--        <option value="{{$product_type->id}}"--}}
{{--                @if (old('product_type_id') == $product_type->id) selected="selected" @endif>{{$product_type->name}}</option>--}}
{{--    @endforeach--}}
{{--</select>--}}
<label for="store" class="forget-form">Store Type*</label>
<select class="form-control select2" name="store_type_name">
    <option value="">Select Store Type</option>
{{--    <option value=""></option>--}}
    @foreach($store_types as $product_type)
        <option value="{{$product_type->id}}"
                @if (old('product_type_id') == $product_type->id) selected="selected" @endif>{{$product_type->name}}</option>
    @endforeach

</select>
