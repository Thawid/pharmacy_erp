<label for="store" class="forget-form">Store*</label>
<select class="form-control select2" name="store_name">
    <option value="">Select Store</option>
    {{--    <option value=""></option>--}}
    @foreach($stores as $product_type)
        <option value="{{$product_type->id}}"
                @if (old('product_type_id') == $product_type->id) selected="selected" @endif>{{$product_type->name}}</option>
    @endforeach

</select>
