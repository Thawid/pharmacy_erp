{{--@extends('dboard.index')--}}

{{--@section('title','Dashboard')--}}

{{--@section('dboard_content')--}}

{{--<div>--}}
{{--    <main class="app-content">--}}
{{--        <div class="clearix">--}}
{{--            <div  class="tile-body" >--}}
{{--                <div class="tile">--}}
{{--                    <h3 class="tile-title">Store Type</h3>--}}

{{--                    <div class="tile-body">--}}
{{--                        <form class="row">--}}
{{--                            <div class="form-group col-md-3">--}}
{{--                                <label class="control-label" >Store Type</label>--}}
{{--                                <input class="form-control" type="text" placeholder="Enter your name" >--}}
{{--                            </div>--}}

{{--                            <div class="form-group col-md-12 align-self-end">--}}
{{--                                <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i>ADD</button>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="tile">--}}
{{--                    <table class="table">--}}
{{--                        <thead>--}}
{{--                        <tr  class="table-success">--}}
{{--                            <th>#</th>--}}
{{--                            <th>Store Type</th>--}}
{{--                            <th>Action</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @foreach($store_types as $store_type)--}}
{{--                        <tr>--}}
{{--                            <td>{{ 1 }}</td>--}}
{{--                            <td>{{$store_type->name}}</td>--}}
{{--                            <td>false</td>--}}
{{--                        </tr>--}}
{{--                        @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </main>--}}
{{--</div>--}}


{{--@endsection--}}


@extends('dboard.index')

@section('title','User Create Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Assign  User To Store</h2>


                        </div>
                        <div class="col-md-3 text-center">
                            <a class="btn btn-primary icon-btn" href="{{route('user.details')}}"><i class="fa fa-arrow-left"></i>Back    </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <form action="{{route('storeuser.index')}}" method="POST">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">

                    <div class="form-group">
                        <label for="exampleSelect1">Slect Store</label>
                        <select class="form-control" id="exampleSelect1">
                            <option selected="true" disabled="disabled">Select Store</option>
                            @foreach ($storeTypes as $key => $storeType )
                                <option value="{{$storeType->name}}" >{{$storeType->name}}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleSelect1">Slect User</label>
                        <select class="form-control" id="exampleSelect1">
                            <option selected="true" disabled="disabled">Select User</option>
                            @foreach ($users as $key => $user )
                                <option value="{{$user->name}}" >{{$user->name}}</option>
                            @endforeach

                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile-footer">
                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </form>

@endsection
