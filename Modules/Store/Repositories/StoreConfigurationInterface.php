<?php


namespace Modules\Store\Repositories;


//use Modules\Store\Http\Requests\CreateStoreTypeRequest;
//use Modules\Store\Http\Requests\UpdateStoreTypeRequest;

//use GuzzleHttp\Psr7\Request;

use Modules\Store\Http\Requests\CreateStoreRequest;
use Modules\Store\Http\Requests\UpdateStoreConfigurationRequest;
use Modules\Store\Http\Requests\UpdateStoreRequest;

interface StoreConfigurationInterface
{
    public function index();

    public function create();

    public function store(UpdateStoreConfigurationRequest $request);

    public function edit($id);

    public function update(UpdateStoreConfigurationRequest $request, $id);

    public function destroy($id);
}
