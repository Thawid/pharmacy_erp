<?php

namespace Modules\Store\Repositories;

use Illuminate\Http\Request;
use Modules\Store\Http\Requests\CreateStoreTypeRequest;
use Modules\Store\Http\Requests\UpdateStoreTypeRequest;

interface StoreTypeInterface
{
	public function index();

	public function create();

    public function store(CreateStoreTypeRequest $request);

	public function edit($id);

	public function update(UpdateStoreTypeRequest $request, $id);

	public function destroy($id);
}
