<?php


namespace Modules\Store\Repositories;


use Illuminate\Http\Request;
use Modules\Store\Entities\Currency;

class CurrencyRepository implements CurrencyInterface
{
    public function index()
    {
        return Currency::all();
    }
    public function create()
    {

    }

    public function store(Request $request)
    {

    }
    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }
}
