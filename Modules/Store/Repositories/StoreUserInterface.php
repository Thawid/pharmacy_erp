<?php


namespace Modules\Store\Repositories;

use Illuminate\Http\Request;
use Modules\Store\Http\Requests\CreateStoreUserRequest;
use Modules\Store\Http\Requests\UpdateStoreUserRequest;

interface StoreUserInterface
{
    public function index();

    public function create();

    public function store(CreateStoreUserRequest  $request);

    public function edit($id);

    public function update(UpdateStoreUserRequest  $request, $id);

    public function destroy($id);
}
