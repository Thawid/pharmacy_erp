<?php

namespace Modules\Store\Repositories;
use Illuminate\Http\Request;
use Modules\Store\Entities\StoreType;
use Modules\Store\Http\Requests\CreateStoreTypeRequest;
use Modules\Store\Http\Requests\UpdateStoreTypeRequest;

class StoreTypeRepository implements StoreTypeInterface
{
    public function index()
    {
        return StoreType::all();
//        return StoreType::orderBy('id','DESC')->get();
    }
    public function create()
    {
        return StoreType::findOrFail();
    }
    public function store(CreateStoreTypeRequest $request)
    {
        $validate_data = $request->validated();

        if($validate_data) {
            $storeType = new StoreType();
            $storeType->name = $request->name;
            $storeType->status = $request->status;
            $storeType->save();
        }

    }
    public function edit($id)
    {
        return StoreType::findOrFail($id);
    }
    public function update(UpdateStoreTypeRequest $request, $id)
    {
        $request->validated();
        $storeType = StoreType::where('id', $id)->firstOrFail();
        $storeType->name = $request->name;
        $storeType->status = $request->status;
        $storeType->save();
    }
    public function destroy($id)
    {
        $store_Type = StoreType::findOrFail($id);
        $store_Type->status = !$store_Type->status;
        $store_Type->save();
    }
}
