<?php


namespace Modules\Store\Repositories;

use Illuminate\Http\Request;
use Modules\Store\Http\Requests\CreateStoreRequest;
use Modules\Store\Http\Requests\UpdateStoreRequest;

interface StoreInterface
{
    public function index();

    public function create();

    public function store(CreateStoreRequest $request);

//    public function showdata();

    public function edit($id);

    public function update(UpdateStoreRequest $request, $id);

    public function destroy($id);
}
