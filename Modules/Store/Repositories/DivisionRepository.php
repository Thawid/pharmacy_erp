<?php


namespace Modules\Store\Repositories;

use Illuminate\Http\Request;
use Modules\Store\Entities\Division;


class DivisionRepository implements DivisionInterface
{
    public function index()
    {
        return Division::all();
    }

    public function create()
    {

    }

    public function store()
    {

    }

    public function edit($id)
    {

    }

    public function update($id)
    {

    }

    public function destroy($id)
    {

    }

}
