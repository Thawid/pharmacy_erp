<?php


namespace Modules\Store\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Store\Entities\StoreUser;
use Modules\Store\Http\Requests\CreateStoreUserRequest;
use Modules\Store\Http\Requests\UpdateStoreUserRequest;

class StoreUserRepository implements StoreUserInterface
{
    public function index()
    {
//        return Store::all();
//        return Store::orderBy('id','DESC')->get();
        return StoreUser::with(['store', 'user'])->orderBy('id','DESC')->get();
    }
    public function create()
    {
        return StoreUser::findOrFail();
    }
    public function store(CreateStoreUserRequest  $request)
    {
        $request->validated();
        $store = new StoreUser();
        $store->store_type = $request->store_type;
        $store->store_id = $request->store_id;
        $store->user_id = $request->user_id;
        $store->status = $request->status;
        $store->save();
    }
    public function edit($id)
    {
       $data =  DB::table('store_users')
            ->join('stores','stores.id','=','store_users.store_id')
            ->join('users','users.id','=','store_users.user_id')
            ->select('store_users.id','store_users.user_id','store_users.store_id','stores.name as store_name','users.name')
            ->where('store_users.id',$id)
            ->first();
       return $data;
    }
    public function update(UpdateStoreUserRequest $request, $id)
    {
        $request->validated();
        $store = StoreUser::where('id', $id)->firstOrFail();
        $store->store_type = $request->store_type;
        $store->store_id = $request->store_id;
        $store->user_id = $request->user_id;
//        $store->status = $request->status;
        $store->save();
    }
    public function destroy($id)
    {
        $store = StoreUser::findOrFail($id);
        $store->status = !$store->status;
        $store->save();
    }
}
