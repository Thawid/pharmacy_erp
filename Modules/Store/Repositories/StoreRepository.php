<?php


namespace Modules\Store\Repositories;

use Illuminate\Http\Request;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreType;
use Modules\Store\Http\Requests\CreateStoreRequest;
use Modules\Store\Http\Requests\UpdateStoreRequest;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProduct;

class StoreRepository implements StoreInterface
{
    public function index()
    {
        return Store::with('type')->orderBy('id', 'DESC')->get();
    }

    public function create()
    {

    }

    public function store(CreateStoreRequest $request)
    {
    //    $stores = store::all();
       
        $storevadata = $request->validated();
        // foreach ($stores as $store) {
        //   if($store->phone_no == $request->phone_no && $store->store_type == $request->store_type){
        //     return redirect()->route('storeconfiguration.create')->with('success','Phone number already exist');
        //   }
        // }
       
        if ($storevadata) {
            $store = new Store();
            $store->store_type_id = 1;
            $store->store_type = $request->store_type;
            $store->name = $request->name;
            $store->address = $request->address;
            $store->phone_no = $request->phone_no;
            $store->email = $request->email;
            $store->code = $request->code;
            $store->status = $request->status;
            $store->save();
            if ($request->store_type == 'store') {
                $storeAvailableProduct = new StoreAvailableProduct();
                $storeAvailableProduct->store_id = $store->id;
                $storeAvailableProduct->status = 0;
                $storeAvailableProduct->save();
            }
        }
    
        return $store;
    }

    public function edit($id)
    {
        return Store::findOrFail($id);
    }

    public function update(UpdateStoreRequest $request, $id)
    {
        $request->validated();
        $storeType = Store::where('id', $id)->firstOrFail();
        $storeType->store_type_id = 1;
        $storeType->store_type = $request->store_type;
        $storeType->name = $request->name;
        $storeType->address = $request->address;
        $storeType->phone_no = $request->phone_no;
        $storeType->email = $request->email;
        $storeType->code = $request->code;
        $storeType->status = $request->status;
        $storeType->save();
    }

    public function destroy($id)
    {
        $store = Store::findOrFail($id);
        $store->status = !$store->status;
        $store->save();
    }
}
