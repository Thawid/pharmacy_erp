<?php


namespace Modules\Store\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreConfiguration;
use Modules\Store\Http\Requests\CreateStoreRequest;
use Modules\Store\Http\Requests\UpdateStoreRequest;
use Modules\Store\Http\Requests\UpdateStoreConfigurationRequest;

class StoreConfigurationRepository implements StoreConfigurationInterface
{
    public function index()
    {
        return Store::orderBy('id','DESC')->get();
//        return Store::with('type')->where('store_type','store')->orderBy('id','DESC')->get();
//        return Store::where('store_type','store')->orderBy('id','DESC')->get();
    }

    public function create()
    {
        return Store::findOrFail();
    }

    public function store(UpdateStoreConfigurationRequest $request)
    {
        $store = Store::where('id', $request->store_id)->firstOrFail();
        $store->store_type = $request->store_type;
        $store->warehouse_id = $request->warehouse_id;
        $store->hub_id = $request->hub_id;
        $store->parent_store_id = $request->store_id;
        $store->currency_id = $request->currency_id;
        $store->division_id = $request->division_id;
        $store->zone_id = $request->zone_id;
        $store->policy_id = $request->policy_id;
        $store->save();
    }

    public function edit($id)
    {
        return Store::findOrFail($id);
    }

    public function update(UpdateStoreConfigurationRequest $request, $id)
    {
        $request->validated();
        $storeType = Store::where('id', $id)->firstOrFail();
//        $store_type->hub_id = $request->hub_id;
//        $store_type->parent_store_id = 1;
//        $store_type->warehouse_id = $request->warehouse_id;
//        $store_type->currency_id = $request->currency_id;
//        $store_type->division_id = $request->division_id;
//        $store_type->zone_id = $request->zone_id;
//        $store_type->policy_id = $request->policy_id;
//        $store_type->name = $request->name;
//        $store_type->address = $request->address;
//        $store_type->phone_no = $request->phone_no;
//        $store_type->email = $request->email;
//        $store_type->code = $request->code;
//        $store_type->store_type_id = $request->store_type_id;
//        $store_type->status = $request->status;

        $storeType->hub_id = $request->hub_id;
        $storeType->parent_store_id = 1;
        $storeType->warehouse_id = $request->warehouse_id;
        $storeType->currency_id = $request->currency_id;
        $storeType->division_id = $request->division_id;
        $storeType->zone_id = $request->zone_id;
        $storeType->policy_id = $request->policy_id;
        $storeType->save();
    }

    public function destroy($id)
    {
        $store = StoreConfiguration::findOrFail($id);
        $store->status = !$store->status;
        $store->save();
    }
}
