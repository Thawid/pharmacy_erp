<?php


namespace Modules\Store\Repositories;


use Illuminate\Http\Request;
use Modules\Store\Entities\Policy;

class PolicyRepository implements PolicyInterface
{
    public function index()
    {
        return Policy::all();
    }
    public function create()
    {

    }

    public function store(Request $request)
    {

    }
    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }
}
