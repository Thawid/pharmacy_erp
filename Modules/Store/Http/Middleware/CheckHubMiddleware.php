<?php

namespace Modules\Store\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckHubMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(hubModuleAuth()){
            return $next($request);
        }
        return abort(403,'No Permission');
    }
}
