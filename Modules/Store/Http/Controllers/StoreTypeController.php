<?php

namespace Modules\Store\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Store\Repositories\StoreTypeInterface;
use Modules\Store\Http\Requests\CreateStoreTypeRequest;
use Modules\Store\Http\Requests\UpdateStoreTypeRequest;

class StoreTypeController extends Controller
{
    private $storeType;
    public function __construct(StoreTypeInterface $storeType)
    {
      
        $this->storeType = $storeType;
    }

    public function index()
    {
        $store_types = $this->storeType->index();
        return view('store::store-type.index', compact('store_types'));
    }

    public function create(Request $request)
    {
        Gate::authorize('hasCreatePermission');
        return view('store::store-type.create');
    }

    public function store(CreateStoreTypeRequest $request)
    {  
        Gate::authorize('hasCreatePermission');
        $storeTypes = $this->storeType->store($request);
        return redirect()->route('storetype.index')->with('success','Store Type has been stored successfully');
    }

    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $storeTypes = $this->storeType->edit($id);
        return view('store::store-type.edit', compact('storeTypes'));
    }

    public function update(UpdateStoreTypeRequest $request, $id)
    {
        Gate::authorize('hasEditPermission');
        $storeTypes = $this->storeType->update($request, $id);
        return redirect()->route('storetype.index')->with('success','Store Type has been updated successfully');
    }

    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->storeType->destroy($id);
        return redirect()->route('storetype.index')->with('success','Store Type status has been changed successfully');
    }
}
