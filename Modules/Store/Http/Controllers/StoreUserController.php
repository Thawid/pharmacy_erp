<?php

namespace Modules\Store\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\Warehouse;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use Modules\Store\Http\Requests\CreateStoreUserRequest;
use Modules\Store\Http\Requests\UpdateStoreUserRequest;
use Modules\Store\Repositories\StoreUserInterface;
use Modules\Store\Repositories\StoreInterface;
use Modules\User\Repositories\UserRepositoryInterface;

class StoreUserController extends Controller
{
    private $storeUser;
    private $store;
    private $user;
    public function __construct(StoreUserInterface  $storeUser, StoreInterface $store,UserRepositoryInterface $user)
    {
        $this->storeUser = $storeUser;
        $this->store = $store;
        $this->user = $user;
    }
    public function index()
    {
        $storeUsers = $this->storeUser->index();
        return view('store::store-user.index', compact('storeUsers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');
        $store = $this->store->index();
        $user = $this->user->index();
        return view('store::store-user.create', compact('store', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateStoreUserRequest $request)
    {
        Gate::authorize('hasCreatePermission');
//        return $request;
       $storeUser = $this->storeUser->store($request);
       return redirect()->route('storeuser.index')->with('Success', 'Stored has been Sucessfull');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('store::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $stores = $this->store->index();
        $users = $this->user->index();
        $storeUsers = $this->storeUser->edit($id);
        $suser = StoreUser::where('id',$id)->first();
        $stype = Store::where('store_type', $suser->store_type)->where('id', $suser->store_id)->first();

        return view('store::store-user.edit', compact( 'stores', 'users','storeUsers', 'stype'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateStoreUserRequest $request, $id)
    {
        Gate::authorize('hasCreatePermission');
//        return $request;
        $this->storeUser->update($request, $id);
        return redirect()->route('storeuser.index')->with('success','Store has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->storeUser->destroy($id);
        return redirect()->route('storeuser.index')->with('success','Store status has been changed successfully');
    }

    public function get_type(Request $request)
    {
        $type = Store::where('store_type', $request->input('type'))
            ->where('status',1)->get();
        return response()->json(['data' => $type]);
    }

}
