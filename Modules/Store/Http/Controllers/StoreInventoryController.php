<?php

namespace Modules\Store\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
class StoreInventoryController extends Controller
{


    public function store_inventory(){

        $store = StoreUser::select('store_id')->where('user_id', auth()->user()->id)->where('store_type', 'store')->first();
        $data = DB::table('store_available_products')
            ->join('store_available_product_details','store_available_products.id','=','store_available_product_details.store_available_product_id')
            ->join('products','store_available_product_details.product_id','=','products.id')
            ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
            ->join('product_types','products.product_type_id','=','product_types.id')
            ->join('product_generics','products.product_generic_id','=','product_generics.id')
            ->join('stores','store_available_products.store_id','=','stores.id')
            ->join('uoms','products.purchase_uom_id','=','uoms.id')
            ->where('stores.id',$store->store_id)
            ->select('products.product_name','product_generics.name as generic_name','manufacturers.name as manufacturer_name','store_available_product_details.available_quantity','uoms.uom_name')
            ->get();

        return view('store::inventory.index',compact('data'));
    }


    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('store::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('store::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('store::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('store::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
