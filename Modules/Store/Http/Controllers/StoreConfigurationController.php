<?php

namespace Modules\Store\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Store\Entities\Store;
use Modules\Inventory\Entities\Warehouse;
use Modules\Store\Http\Requests\CreateStoreRequest;
use Modules\Store\Http\Requests\UpdateStoreRequest;
use Modules\Store\Repositories\PolicyInterface;
use Modules\Store\Repositories\CurrencyInterface;
use Modules\Store\Repositories\DivisionInterface;
use Modules\Store\Repositories\StoreInterface;
use Modules\Store\Repositories\StoreConfigurationInterface;
use Modules\Store\Repositories\StoreTypeInterface;
use Modules\Store\Repositories\ZoneInterface;
use Modules\Store\Http\Requests\UpdateStoreConfigurationRequest;

class StoreConfigurationController extends Controller
{
    private $storeType;
    private $currency;
    private $zone;
    private $division;
    private $policy;
    private $store;
    private $storeid;
    private $storeConfiguration;
    public function __construct(StoreConfigurationInterface $storeConfiguration, StoreInterface $store, StoreTypeInterface $storeType, PolicyInterface $policy, DivisionInterface $division, ZoneInterface $zone, CurrencyInterface $currency)
    {
        $this->storeType = $storeType;
        $this->currency = $currency;
        $this->zone = $zone;
        $this->division = $division;
        $this->policy = $policy;
        $this->store = $store;
        $this->storeConfiguration = $storeConfiguration;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
//        $stores = $this->store->index();
//        $stores = $this->storeConfiguration->index();
        $warehouses = Warehouse::get();
        $stores = Store::with('type', 'hub')->where('store_type','store')->orderBy('id','DESC')->get();
        $hubs = Store::with('type')->where('store_type','hub')->orderBy('id','DESC')->get();
        return view('store::store-configuration.index', compact('stores', 'hubs','warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {

        Gate::authorize('hasCreatePermission');
        // return redirect()->route('storeconfiguration.edit',1);
       $warehouses = Warehouse::with('warehouseType', 'storeInfo')->latest()->get();
       $divisions = $this->division->index();
       $currencies = $this->currency->index();
       $zones = $this->zone->index();
       $policies = $this->policy->index();
       $storeTypes = $this->storeType->index();
       $stores = $this->store->index();
       $hubs = Store::where('store_type', 'hub')->where('status',1)->get();
       return view('store::store-configuration.create', compact('storeTypes', 'stores', 'policies', 'divisions', 'zones', 'currencies', 'warehouses','hubs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(UpdateStoreConfigurationRequest $request)
    {
        Gate::authorize('hasCreatePermission');
//        return $request;
        $store = $this->storeConfiguration->store($request);
        return redirect()->route('storeconfiguration.index')->with('success','Store has been stored successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
//    public function show($id)
//    {
//        return view('store::show');
//    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $warehouses = Warehouse::with('warehouseType', 'storeInfo')->latest()->get();
        $divisions = $this->division->index();
        $currencies = $this->currency->index();
        $zones = $this->zone->index();
        $policies = $this->policy->index();
        $storeTypes = $this->storeType->index();
        $stores = $this->store->index();
        $storeid = $this->store->edit($id);
        return view('store::store-configuration.edit', compact('storeTypes', 'stores', 'policies', 'divisions', 'zones', 'currencies', 'storeid', 'warehouses'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateStoreConfigurationRequest $request, $id)
    {
        Gate::authorize('hasEditPermission');
        $this->storeConfiguration->update($request, $id);
        return redirect()->route('storeconfiguration.index')->with('success','Store Configuration has been mapped successfully');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function get_type(Request $request)
    {
        $type = Store::where('store_type', $request->input('type'))
            ->where('status',1)->get();
//        $hub = Store::where('store_type', 'hub')
//            ->where('status',1)->get();
//        return response()->json(['data' => $type, 'value' => $hub]);
        return response()->json(['data' => $type]);
    }

    public function get_store(Request $request)
    {
        $store = Warehouse::where('store_id', $request->input('store'))
            ->where('status',1)->get();
        return response()->json(['data' => $store]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->store->destroy($id);
        return redirect()->route('storeconfiguration.index')->with('success','Store Configuration status has been changed successfully');
    }
}
