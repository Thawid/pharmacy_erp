<?php

namespace Modules\Store\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Store\Repositories\StoreInterface;
use Modules\Store\Repositories\StoreTypeInterface;
use Modules\Store\Http\Requests\CreateStoreRequest;
use Modules\Store\Http\Requests\UpdateStoreRequest;


class StoreController extends Controller
{
    private $store;
    private $storeType;
    public function __construct(StoreInterface $store, StoreTypeInterface $storeType)
    {

        $this->store = $store;
        $this->storeType = $storeType;
    }

    public function index()
    {
        $stores = $this->store->index();
        return view('store::store.index', compact('stores'));
    }

    public function create()
    {
        Gate::authorize('hasCreatePermission');
//        $storeTypes = $this->storeType->index();
        return view('store::store.create');
    }

    public function store(CreateStoreRequest $request)
    {
        Gate::authorize('hasCreatePermission');
        $store = $this->store->store($request);
        return redirect()->route('store.index')->with('success','Store has been stored successfully');
    }

    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
//        $storeTypes = $this->storeType->index();
        $store = $this->store->edit($id);
        return view('store::store.edit', compact('store'));
    }

    public function update(UpdateStoreRequest $request, $id)
    {   Gate::authorize('hasEditPermission');
        $store = $this->store->update($request, $id);
        return redirect()->route('store.index')->with('success','Store has been updated successfully');
    }

    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->store->destroy($id);
        return redirect()->route('store.index')->with('success','Store status has been changed successfully');
    }
}
