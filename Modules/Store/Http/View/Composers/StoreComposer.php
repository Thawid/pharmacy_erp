<?php


namespace Modules\Store\Http\View\Composers;


use Illuminate\View\View;
//use Modules\Product\Entities\ProductType;
use Modules\Store\Entities\StoreType;
use Modules\Store\Entities\Store;

class StoreComposer
{
    public function compose(View $view)
    {
//        $view->with('store_types', StoreType::orderBy('name')->get());
        $view->with('store_types', StoreType::where('status','=',1)->get(['id','name']));
        $view->with('stores', Store::where('status','=',1)->get(['id','name']));
//        $view->with('product_types', ProductType::where('status','=',1)->get(['id','name']));

    }

}
