<?php

namespace Modules\Store\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStoreTypeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|regex:/^[\pL\s\-]+$/u|min:2|max:30'
        ];
    }


    public function messages()
    {
        return [
            'name.required'=>'Store type field is required',
            'name.regex'=>'Store name must only contain letters',
            'name.min'=>'Store name must be at least 2 characters',
            'name.max'=>'Store name must be maximum 30 characters',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
