<?php

namespace Modules\Store\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50',
            'store_type' => 'required',
            'address' => 'string|min:5|max:255',
            // 'phone_no' => 'unique:stores|regex:/(01)[0-9]{9}/|min:11|max:14',
            'email' => 'string|email|unique:stores|regex:/(.+)@(.+)\.(.+)/i',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Store name field must be required',
            'name.min'=>'Store name at least 2 characters',
            'name.max'=>'Store name maximum 50 characters',
            'name.unique'=>'This Store already exits',
            'store_type_id.required'=>'Please select store type',
            // 'phone_no.unique'=>'This phone number already taken',
            'email.unique'=>'This email address already taken',


        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
