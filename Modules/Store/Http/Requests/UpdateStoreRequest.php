<?php

namespace Modules\Store\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50',
            'store_type' => 'required',
            'address' => 'string|min:5|max:255',

        ];

    }

    public function messages()
    {
        return [
            'name.required' => 'Store name field must be required',
            'name.min' => 'Store name must be at least 2 characters',
            'name.max' => 'Store name must be maximum 50 characters',
            'name.unique' => 'This Store already exits'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
