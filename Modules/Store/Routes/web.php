<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth','hq'])->prefix('store')->group(function() {
    Route::resource('storetype', 'StoreTypeController');
    Route::resource('store', 'StoreController');
    Route::resource('storeconfiguration', 'StoreConfigurationController');
    Route::resource('storeuser', 'StoreUserController');
    Route::get('/get-type', 'StoreConfigurationController@get_type')->name('get_type');
    Route::get('/get-store', 'StoreConfigurationController@get_store')->name('get_store');
    Route::get('/g-type', 'StoreUserController@get_type')->name('g_type');
    Route::get('/store-inventory','StoreInventoryController@store_inventory');
});
