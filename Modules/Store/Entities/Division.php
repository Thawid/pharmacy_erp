<?php

namespace Modules\Store\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Division extends Model
{
//    use HasFactory;

    protected $guarded = [];

//    protected static function newFactory()
//    {
//        return \Modules\Store\Database\factories\DivisionFactory::new();
//    }

    public function stores(){
        return $this->hasMany(Store::class);
    }
}
