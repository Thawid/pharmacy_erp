<?php

namespace Modules\Store\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\Warehouse;

class Store extends Model
{

    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo(StoreType::class,'store_type_id');
    }
    public function hub()
    {
        return $this->belongsTo(Store::class,'hub_id');
    }
    public function division()
    {
        return $this->belongsTo(Division::class,'division_id');
    }
    public function zone()
    {
        return $this->belongsTo(Zone::class,'zone_id');
    }
    public function currency()
    {
        return $this->belongsTo(Currency::class,'currency_id');
    }
    public function policy()
    {
        return $this->belongsTo(Policy::class,'policy_id');
    }
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class,'warehouse_id');
    }
    public function hubAvailableProduct()
    {
        return $this->belongsTo(HubAvailableProduct::class,'hub_id','hub_id');
    }


//    public function storeUser(){
//        return $this->hasMany(StoreUser::class);
//    }
}
