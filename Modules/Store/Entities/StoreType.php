<?php

namespace Modules\Store\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Store\Database\factories\StoreTypeFactory;

class StoreType extends Model
{
    use HasFactory;

    protected $fillable = ['name','status'];

    public function stores(){
        return $this->hasMany(Store::class);
    }

    protected static function StoreTypeFactory()
    {
        return StoreTypeFactory::new();
    }

}
