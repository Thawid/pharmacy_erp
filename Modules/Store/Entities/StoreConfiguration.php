<?php

namespace Modules\Store\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StoreConfiguration extends Model
{
//    use HasFactory;
//    protected $table =
    protected $guarded = [];

//    protected static function newFactory()
//    {
//        return \Modules\Store\Database\factories\StoreConfigurationFactory::new();
//    }

    public function stores(){
        return $this->hasMany(Store::class);
    }
}
