<?php

namespace Modules\Store\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Store\Repositories\PolicyRepository;
use Modules\Store\Repositories\CurrencyRepository;
use Modules\Store\Repositories\ZoneRepository;
use Modules\Store\Repositories\StoreRepository;
use Modules\Store\Repositories\StoreTypeRepository;
use Modules\Store\Repositories\DivisionRepository;
use Modules\Store\Repositories\StoreConfigurationRepository;
use Modules\Store\Repositories\PolicyInterface;
use Modules\Store\Repositories\CurrencyInterface;
use Modules\Store\Repositories\ZoneInterface;
use Modules\Store\Repositories\DivisionInterface;
use Modules\Store\Repositories\StoreTypeInterface;
use Modules\Store\Repositories\StoreInterface;
use Modules\Store\Repositories\StoreConfigurationInterface;
use Modules\Store\Repositories\StoreUserInterface;
use Modules\Store\Repositories\StoreUserRepository;

class StoreRepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        //$this->app->bind(StoreTypeInterface::class, StoreTypeRepository::class);
    }

    public function boot()
    {
        $this->app->bind(StoreTypeInterface::class, StoreTypeRepository::class);
        $this->app->bind(StoreInterface::class, StoreRepository::class);
        $this->app->bind(StoreConfigurationInterface::class, StoreConfigurationRepository::class);
        $this->app->bind(PolicyInterface::class, PolicyRepository::class);
        $this->app->bind(DivisionInterface::class, DivisionRepository::class);
        $this->app->bind(ZoneInterface::class, ZoneRepository::class);
        $this->app->bind(CurrencyInterface::class, CurrencyRepository::class);
        $this->app->bind(StoreUserInterface::class, StoreUserRepository::class);
    }

    public function provides()
    {
        return [];
    }
}
