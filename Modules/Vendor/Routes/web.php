<?php

use Illuminate\Support\Facades\Route;



   


Route::middleware('auth')->prefix('vendor')->group(function() {
   
    Route::resource('vendor-user', 'VendorUserController')->middleware('hq');
    Route::resource('vendortype', 'VendorTypeController')->middleware('hq');
    Route::resource('vendorgroup', 'VendorGroupController')->middleware('hq');
    Route::resource('vendors', 'VendorController')->middleware('hq');
    Route::resource('profile', 'ProfileController')->middleware('vendor');
    Route::resource('dashboard', 'DashboardController')->middleware('vendor');
    Route::resource('sellrequest', 'SellRequestController')->middleware('vendor');
    Route::resource('vendor-invoice', 'SellRequestController')->middleware('vendor');
   
   
    
    Route::get('/sell-request/edit/{id}','SellRequestController@sellRequestEdit')->name('sell.request.edit')->middleware('vendor');
    Route::get('/sell-request/details/{id}','SellRequestController@sellRequestDetails')->name('sell.request.details')->middleware('vendor');
    Route::post('/sell-request/update/{id}','SellRequestController@sell_request_update')->name('sell.request.update')->middleware('vendor');
    Route::get('/sell-request-status/','SellRequestController@sell_request_status_remove')->name('sell.request.remove')->middleware('vendor');
    Route::get('/sell-remain', 'SellRequestController@sellRemainRequest')->name('sell.request.remain')->middleware('vendor');
    Route::get('/sell-remain-details/{id}', 'SellRequestController@sellRemainDetailRequest')->name('sell.remain.details')->middleware('vendor');
    Route::get('/sell-remain-edit/{id}', 'SellRequestController@sellRemainEditRequest')->name('sell.remain.edit')->middleware('vendor');
    Route::get('invoices', 'InvoiceController@invoice')->name('invoice')->middleware('vendor');
    Route::post('vendor-filter','VendorController@vendor_details')->name('filter.vendor')->middleware('hq');
    Route::get('vendor-list','VendorController@get_all_vendor')->name('vendor.list')->middleware('hq');
    Route::get('get-product-by-type-id','VendorController@getProductByTypeId')->name('product.get.by.type.id');
    Route::get('vendor-profile/{id}', 'VendorProfileController@show')->middleware('hq');
    Route::get('vendor-details/{id}','VendorProfileController@vendor_details')->name('vendor.details')->middleware('hq');
    Route::get('/vendor-purchase-order/{id}','VendorProfileController@vendor_purchase_orders')->name('vendor.purchase.order')->middleware('vendor');
    Route::get('/vendor-purchase-product/{id}','VendorProfileController@vendor_purchase_product')->name('vendor.purchase.product')->middleware('vendor');
    Route::get('/vendor-purchase-payment/{id}','VendorProfileController@vendor_purchase_payment')->name('vendor.purchase.payment')->middleware('vendor');
    Route::get('/vendortype-edit/{id}', 'VendorTypeController@vendor_type_edit')->middleware('hq');
    Route::post('/vendortype-update', 'VendorTypeController@vednorUpdate')->middleware('hq');
    Route::get('/vendorgroup-edit/{id}', 'VendorGroupController@vendor_group_edit')->middleware('hq');
    Route::post('/vendorgroup-update', 'VendorGroupController@vendorUpdate')->middleware('hq');
   
   
    
});
