<?php


namespace Modules\Vendor\Repositories;


use Illuminate\Http\Request;
use Modules\Vendor\Http\Requests\VendorTypeRequest;
use Modules\Vendor\Http\Requests\UpdateVendorTypeRequest;

interface VendorTypeInterface
{
    public function index();

    public function create();

    public function store(VendorTypeRequest $request);

    public function edit($id);

    public function update(UpdateVendorTypeRequest $request, $id);

    public function destroy($id);

}
