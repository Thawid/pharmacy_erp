<?php


namespace Modules\Vendor\Repositories;

use Illuminate\Http\Request;
use Modules\Vendor\Http\Requests\VendorRequest;
use Modules\Vendor\Http\Requests\UpdateVendorRequest;

interface VendorInterface
{
    public function index();

    public function create();

    public function store(VendorRequest $request);

    public function edit($id);

    public function update(UpdateVendorRequest $request, $id);

    public function destroy($id);



}
