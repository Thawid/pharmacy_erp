<?php


namespace Modules\Vendor\Repositories;


use http\Env\Request;
use Modules\Vendor\Http\Requests\InvoiceRequest;



interface InvoiceInterface
{
    public function index();

    public function create();

    public function store(InvoiceRequest $request);

    public function edit($id);

    public function update(Request $request, $id);

    public function destroy($id);

}
