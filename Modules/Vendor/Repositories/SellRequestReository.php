<?php


namespace Modules\Vendor\Repositories;

use Illuminate\Http\Request;
use Modules\Vendor\Entities\VendorGroup;
use Modules\Vendor\Http\Requests\SellRequest;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Accounts\Entities\VendorInvoice;
use Modules\Vendor\Http\Requests\VendorGroupRequest;
use Modules\Vendor\Http\Requests\UpdateVendorGroupRequest;
use DataTables;
use DB;



class SellRequestReository  implements SellRequestInterface
{
    public function index()
    {
        return VendorGroup::latest()->get();
    }

    public function create()
    {

    }

    public function store(SellRequest $request)
    {

    }

    public function edit($id)
    {

    }
    public function update($request, $id)
    {
        //
    }
    public function destroy($id)
    {

    }

    public function sell_request_update($request,$id)
    {
        DB::beginTransaction();
        try {


            $purchase_order = PurchaseOrder::findOrFail($id);
            $purchase_order->hq_approve_requisition_id  = $request->hq_id;
            $purchase_order->purchase_order_date        = $request->purchase_order_date;
            $purchase_order->requested_delivery_date    = $request->delivery_date;
            $purchase_order->vendor_delivery_date       = $request->vendor_delivery_date;
            $purchase_order->sub_total                  = $request->sub_total;
            $purchase_order->overall_discount           = $request->overall_discount;
            $purchase_order->total                      = $request->total_price;
            $purchase_order->other_expense              = $request->other_expense;
            $purchase_order->vat_tax                    = $request->vat_tax;
            $purchase_order->priority                   = $request->priority;
            $purchase_order->vendor_id                  = $request->vendor_id;
            $purchase_order->status                     = 'Approved';
            $purchase_order->save();

           $product_id     = $request->product_id;
            $vendor_id      = $request->vendor_id;
            $unit_id        = $request->unit_id;
            $generic_id     = $request->generic_id;
            $quantity       = $request->delivered_quantity;
            //$remain_qty     = $request->remain_quantity;
            $uom_id         = $request->uom_id;
            $unit_price     = $request->uom_price;
            $discount       = $request->discount;

            $product_sub_total = $request->product_sub_total;
            for ($i = 0; $i < count($product_id); $i++) {
                $purchase_order_details = $purchase_order->purchase_order_details->where('product_id',$product_id[$i])->first();
                $purchase_order_details->purchase_order_id          = $purchase_order->id;
                $purchase_order_details->product_id                 = $product_id[$i];
                // $purchase_order_details->vendor_id               = $vendor_id;
                $purchase_order_details->generic_id                 = $generic_id[$i];
                $purchase_order_details->unit_id                    = $unit_id[$i];
                $purchase_order_details->uom_id                     = $uom_id[$i];
                $purchase_order_details->delivered_quantity         = $quantity[$i];
                $purchase_order_details->remain_quantity            = $request->order_quantity[$i] - $request->delivered_quantity[$i];
                $purchase_order_details->uom_price                  = $unit_price[$i];
                $purchase_order_details->discount                   = $discount[$i];
                $purchase_order_details->sub_total                  = $product_sub_total[$i];
                $purchase_order_details->status                     = "Approved";
                $purchase_order_details->save();
            }

           //Vendor-Invoice Store........................................
            $vendor_invoice = new VendorInvoice();
            if ($request->hasFile('attach_file')) {
                $attachment = $request->file('attach_file');
                $file_name = time() . '.' . $attachment->extension();
                $location = public_path('uploads/vendor-invoice');
                if (!is_dir($location)) {
                    mkdir($location, 0755);
                }
                $request->attach_file->move(public_path('uploads/vendor-invoice'), $file_name);
            }

            $vendor_invoice->vendor_id                  = $vendor_id;
            $vendor_invoice->purchase_order_id          = $purchase_order->id;
            $vendor_invoice->purchase_order_no          = $request->purchase_order_no;
            $vendor_invoice->invoice_no                 =  rand(0, 99999).date('his');
            $vendor_invoice->status                     = "Pending";
            $vendor_invoice->amount                     = $request->total_price;
            $vendor_invoice->attachment                     =  $file_name ?? NULL;;
            $vendor_invoice->save();


            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

}
