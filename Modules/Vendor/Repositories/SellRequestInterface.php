<?php


namespace Modules\Vendor\Repositories;


use http\Env\Request;
use Modules\Procurement\Http\Requests\PurchaseOrderRequest;
use Modules\Vendor\Http\Requests\SellRequest;


interface SellRequestInterface
{
    public function index();

    public function create();

    public function store(SellRequest $request);

    public function edit($id);

    public function update($request, $id);

    public function destroy($id);

    public function sell_request_update($request,$id);

}
