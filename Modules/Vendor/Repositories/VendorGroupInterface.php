<?php


namespace Modules\Vendor\Repositories;


use http\Env\Request;
use Modules\Vendor\Http\Requests\VendorGroupRequest;
use Modules\Vendor\Http\Requests\UpdateVendorGroupRequest;

interface VendorGroupInterface
{
    public function index();

    public function create();

    public function store(VendorGroupRequest $request);

    public function edit($id);

    public function update(UpdateVendorGroupRequest $request, $id);

    public function destroy($id);

}
