<?php


namespace Modules\Vendor\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use Modules\Vendor\Entities\Vendor;
use Modules\Vendor\Entities\VendorUser;

class VendorUserRepository implements VendorUserInterface
{



    public function index()
    {
        $all_vendor_user = DB::table('vendor_user')
        ->join('vendors','vendor_user.vendor_id','=','vendors.id')
        ->join('users','vendor_user.user_id','=','users.id')
        ->select('vendor_user.id','vendors.name as vendor_name','users.name as user_name','users.role as user_role','vendor_user.status as vendor_user_status')
        ->get();

        return $all_vendor_user;
    }



    public function create()
    {

        $data['users'] = User::select('id','name')->get();
        $data['vendors'] = Vendor::select('id','name')->get();
        return $data;

    }

    public function store($request)
    {
       
        $validated = $request->validate([
            'vendor_id' => 'required',
            'user_id' => 'required',
            'status' => 'required',
        ]);

        if($validated){

            DB::table('vendor_user')->insert([
                'vendor_id' => $request->vendor_id,
                'user_id' => $request->user_id,
                'status' => $request->status,
            ]);
        }
    
    }

    public function edit($id)
    {
        $data['users'] = User::select('id','name')->get();
        $data['vendors'] = Vendor::select('id','name')->get();
        $data['vendor_user'] = VendorUser::where('id',$id)->firstOrFail();
        return $data;
    }

    public function update($request, $id)
    {
       $vendor_user = VendorUser::find($id);
       $vendor_user->vendor_id = $request->vendor_id;
       $vendor_user->user_id = $request->user_id;
       $vendor_user->save();
    }

    public function destroy($id)
    {
        $store = VendorUser::findOrFail($id);
        $store->status = !$store->status;
        $store->save();

    }

   
}
