<?php


namespace Modules\Vendor\Repositories;

use Illuminate\Http\Request;
use Modules\Vendor\Entities\VendorGroup;
use Modules\Vendor\Http\Requests\InvoiceRequest;
use Modules\Vendor\Http\Requests\SellRequest;
use DataTables;



class InvoiceRepository implements InvoiceInterface
{
    public function index()
    {
        return VendorGroup::latest()->get();
    }

    public function create()
    {

    }

    public function store(InvoiceRequest $request)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }

}
