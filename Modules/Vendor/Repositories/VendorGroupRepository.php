<?php


namespace Modules\Vendor\Repositories;

use Illuminate\Http\Request;
use Modules\Vendor\Entities\VendorGroup;
use Modules\Vendor\Http\Requests\VendorGroupRequest;
use Modules\Vendor\Http\Requests\UpdateVendorGroupRequest;
use DataTables;



class VendorGroupRepository   implements VendorGroupInterface
{
    public function index()
    {
        return VendorGroup::latest()->get();
    }

    public function create()
    {

    }

    public function store(VendorGroupRequest $request)
    {
        $validate_data = $request->validated();
        if ($validate_data) {
            $vendor_group = new VendorGroup();
            $vendor_group->name = $request->input('name');
            $vendor_group->status = $request->input('status');
            $vendor_group->save();
        }


    }

    public function edit($id)
    {
        return VendorGroup::where('id', $id)->firstOrFail();
    }

    public function update(UpdateVendorGroupRequest $request, $id)
    {
        $validate = $request->validated();
        if ($validate) {
            $vendor_group = VendorGroup::where('id', $id)->firstOrFail();
            $vendor_group->name = $request->input('name');
            $vendor_group->save();
        }
    }

    public function destroy($id)
    {
        $vendorGroup = VendorGroup::findOrFail($id);
        $vendorGroup->status = !$vendorGroup->status;
        $vendorGroup->save();
    }

}
