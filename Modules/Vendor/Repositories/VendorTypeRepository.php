<?php


namespace Modules\Vendor\Repositories;

use Illuminate\Http\Request;
use Modules\Vendor\Http\Requests\VendorTypeRequest;
use Modules\Vendor\Http\Requests\UpdateVendorTypeRequest;
use Modules\Vendor\Entities\VendorType;
use DataTables;


class VendorTypeRepository implements VendorTypeInterface
{
    public function index()
    {

        return VendorType::latest()->get();
        /*DB::statement(DB::raw('set @rownum=0'));
        $data = VendorType::latest()->get(['id', 'name', 'status',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
        return DataTables::of($data)
            ->addColumn('status', function ($row) {
                return $row->status == 1 ? 'Active' : 'InActive';
            })
            ->addColumn('action', function ($type) {
                return '<div class="d-md-flex align-items-center overflow-hidden">
                <a class="btn btn-primary d-md-inline" href="' . route('vendortype.edit', $type->id) . '">
                    <i class="fa fa-lg fa-edit" ></i>Edit</a>
                    <form class="ml-3" id="delete_form' . $type->id . '" method="POST" action="' . route('units.destroy', $type->id) . '" onclick="deleteUnit();" accept-charset="UTF-8">
                                       <input name="_method" type="hidden" value="DELETE">
                                       <input type="hidden" name="_token" value="' . csrf_token() . '">
                                    <input class="btn btn-primary" type="submit" value="Change Status">
                    </form>
                    </div>';
            })
            ->rawColumns(['action'])
            ->removeColumn('id')
            ->make(true);*/
    }

    public function create()
    {

    }

    public function store(VendorTypeRequest $request)
    {
        $validate_data = $request->validated();
        if ($validate_data) {
            $vendor_type = new VendorType();
            $vendor_type->name = $request->input('name');
            $vendor_type->status = $request->input('status');
            $vendor_type->save();
        }
    }

    public function edit($id)
    {
        return VendorType::where('id', $id)->firstOrFail();
    }

    public function update(UpdateVendorTypeRequest $request, $id)
    {
        //dd($request->all());
        $validate = $request->validated();
        if ($validate) {
            $vendor_type = VendorType::where('id', $id)->firstOrFail();
            $vendor_type->name = $request->input('name');
            $vendor_type->save();
        }

    }

    public function destroy($id)
    {
        $vendorType = VendorType::findOrFail($id);
        $vendorType->status = !$vendorType->status;
        $vendorType->save();
    }

}
