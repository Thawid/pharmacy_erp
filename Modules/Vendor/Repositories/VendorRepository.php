<?php


namespace Modules\Vendor\Repositories;

use Illuminate\Http\Request;
use Modules\Vendor\Http\Requests\VendorRequest;
use Modules\Vendor\Http\Requests\UpdateVendorRequest;
use Illuminate\Support\Facades\DB;
use Modules\Vendor\Entities\Vendor;
use Modules\Vendor\Entities\VendorDetails;
use Carbon\Carbon;
use DataTables;


class VendorRepository implements VendorInterface
{



  public function index()
  {
  }

  public function get_all_vendor(Request $request)
  {

    $vendor_type = $request->vendor_type;
    $vendor_group = $request->group_type;
    $vendor_origin = $request->origin;
    if ($vendor_type != '' && $vendor_group != '' && $vendor_origin != '') {
      $vendor_details = Vendor::whereHas('vendor_details', function ($query) use ($vendor_type, $vendor_group, $vendor_origin) {
        return $query->where('group_id', '=', $vendor_group)->where('origin', $vendor_origin)->where('type_id', $vendor_type);
      })->with(['vendor_details', 'vgroup']);
    } else {
      $vendor_details = Vendor::latest()->with(['vendor_details', 'vgroup']);
    }
    return datatables()->of($vendor_details)
      ->addColumn('logo', function ($row) {
        if (isset($row->vendor_logo)) {
          return '<img src="' . asset('uploads' . '/vendor-logo' . "/" . $row->vendor_logo) . '" alt="Vendor Logo" height="60px" width="60px">';
        } else {
          return '<img src="' . asset('uploads/vendor-logo/logo-dummy.png') . '"
                                                 alt="Vendor Logo" height="60" width="60">';
        }
      })
      ->addColumn('details', function ($row) {
        return '<address>
                            <p> ' . $row->name . ' </p><br>
                            <abbr title="Phone"> </abbr>' . $row->phone_number . '<br/>
                            <a href="mailto:{{$vendor->email}}"> ' . $row->email . '</a><br/>
                            <p> ' . $row->address . ' </p><br>
                        </address>';
      })
      ->addColumn('vtype', function ($row) {
        $vtype = '';
        foreach ($row->vendor_details->unique('type_id') as $vdetails) {
          $vtype .= '<span class="badge rounded-pill bg-primary p-3">' . $vdetails->vtype->name . ' </span> ';
        }
        return  $vtype;
      })
      ->addColumn('action', function ($row) {

        $btn = '<div class="d-flex justify-content-around align-items-center">
        <a class="btn edit-btn" href="' . route('vendors.edit', $row->id) . '"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a> &nbsp
        <a class="btn details-btn" href="' . route('vendor.details', $row->id) . '"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"></i></a>
        </div>';

        return $btn;
      })
      ->rawColumns(['action', 'logo', 'vtype', 'details'])
      ->make(true);
  }


  public function create()
  {
  }

  public function store(VendorRequest $request)
  {
    //dd($request->all());

    $vadatedata = $request->validated();

    $vendor_name = $request->input('name');
    $vendor_type = $request->input('type_id');
    $vendor_group = $request->input('group_id');
    $product_id = $request->input('product_id');
    $product_type_id = $request->input('product_type_id');
    $phone_number = $request->input('phone_number');
    $email = $request->input('email');
    $address = $request->input('address');
    $origin = $request->input('origin');
    $insert_vendor_details = array();

    $logo_name = NULL;

    if ($vadatedata) {
      DB::beginTransaction();
      try {
        $vendor = new Vendor();
        if ($request->hasFile('vendor_logo')) {
          $image = $request->file('vendor_logo');
          $logo_name = time() . '.' . $image->extension();
          $location = public_path('uploads/vendor-logo');
          if (!is_dir($location)) {
            mkdir($location, 0755);
          }
          $request->vendor_logo->move(public_path('uploads/vendor-logo'), $logo_name);
        }
        $vendor->name = $vendor_name;
        $vendor->group_id = $vendor_group;
        $vendor->phone_number = $phone_number;
        $vendor->email = $email;
        $vendor->address = $address;
        $vendor->origin = $origin;
        $vendor->vendor_logo = $logo_name;
        $vendor->save();
        $vendor_id = $vendor->id;
        try {
          $counter = 0;
          foreach ($product_type_id as $product_type) {
            $insert_vendor_details[] = [
              'vendor_id' => $vendor_id,
              'type_id' => $vendor_type[$counter],
              'product_id' => $product_id[$counter],
              'product_type_id' => $product_type,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
            ];
            $counter++;
          }
          try {
            VendorDetails::insert($insert_vendor_details);

            DB::commit();
            return redirect()->back()->with('success', 'Vendor create successfully');
          } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors("Vendor can not added" . $e->getMessage());
          }
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect()->back()->withErrors("Vendor details data error " . $e->getMessage());
        }
      } catch (\Exception $e) {
        DB::rollBack();
        return redirect()->back()->withErrors("Vendor table error" . $e->getMessage());
      }
    }
  }

  public function edit($id)
  {
    $vendor_details = Vendor::with(['vendor_details', 'vgroup'])->where('id', $id)->first();
    return $vendor_details;
  }

  public function update(UpdateVendorRequest $request, $id)
  {
    //dd($request->all());

    $vendor_name = $request->input('name');
    $vendor_type = $request->input('type_id');
    $vendor_group = $request->input('group_id');
    $product_id = $request->input('product_id');
    $product_type_id = $request->input('product_type_id');
    $phone_number = $request->input('phone_number');
    $email = $request->input('email');
    $address = $request->input('address');
    $origin = $request->input('origin');
    $update_vendor_details = array();

    DB::beginTransaction();
    try {
      $vendor = Vendor::findOrFail($id);

      if ($request->hasFile('vendor_logo')) {
        $image = $request->file('vendor_logo');
        $vendor->vendor_logo = time() . '.' . $image->extension();
        $request->vendor_logo->move(public_path('uploads/vendor-logo'), $vendor->vendor_logo);
      }
      $vendor_id = $vendor->id;
      $vendor->name = $vendor_name;
      $vendor->group_id = $vendor_group;
      $vendor->phone_number = $phone_number;
      $vendor->email = $email;
      $vendor->address = $address;
      $vendor->origin = $origin;
      //$vendor->vendor_logo = $logo_name;
      $vendor->save();

      try {
        DB::table('vendor_details')->where('vendor_id', $id)->delete();
        $counter = 0;
        foreach ($product_type_id as $product_type) {
          $update_vendor_details[] = [
            'vendor_id' => $vendor_id,
            'type_id' => $vendor_type[$counter],
            'product_id' => $product_id[$counter],
            'product_type_id' => $product_type,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ];
          $counter++;
        }
        try {
          VendorDetails::insert($update_vendor_details);
          DB::commit();
          return redirect()->back()->with('success', 'Vendor update successfully');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect()->back()->withErrors("Vendor dosen't updated" . $e->getMessage());
        }
      } catch (\Exception $e) {
        DB::rollBack();
        return redirect()->back()->withErrors("Vendor dosen't updated" . $e->getMessage());
      }
    } catch (\Exception $e) {
      DB::rollBack();
      return redirect()->back()->withErrors("Vendor dosen't updated" . $e->getMessage());
    }
  }

  public function destroy($id)
  {
  }

  public function vendor_details($id)
  {
    $vendor_details = Vendor::with(['vendor_details', 'vgroup'])->where('id', $id)->first();
    return $vendor_details;
  }
}
