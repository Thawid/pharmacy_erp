<?php

namespace Modules\Vendor\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VendorGroup extends Model
{
    use HasFactory;

    protected $table = "vendor_groups";

    protected $fillable = ['name'];

    public function vendor(){
        return  $this->belongsTo(Vendor::class);
    }

    protected static function newFactory()
    {
        return \Modules\Vendor\Database\factories\VendorGroupFactory::new();
    }
}
