<?php

namespace Modules\Vendor\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductType;
use Modules\Product\Entities\ProductVariation;

class VendorDetails extends Model
{
    use HasFactory;

    protected $table = "vendor_details";

    protected $fillable = [
        'vendor_id',
        'type_id',
        'product_type_id'
    ];

    public function vendor(){
        return  $this->belongsTo(Vendor::class,'id','vendor_id');
    }

    public function vgroup(){
        return  $this->hasOne(VendorGroup::class,'id','group_id');
    }

    public function vtype(){
        return $this->hasOne(VendorType::class,'id','type_id')->select('id','name');
    }

    public function ptype(){
        return  $this->hasOne(ProductType::class,'id','product_type_id')->select('id','name');
    }

    public function pname(){
        return $this->hasOne(Product::class,'id','product_id')->select('id','product_name');
    }

    public function vendorName()
    {
        return $this->hasOne(Vendor::class,'id','vendor_id')->withDefault();
    }

    public function vendors(){
        return  $this->hasOne(Vendor::class,'id','vendor_id');
    }

    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }
    

    protected static function newFactory()
    {
        return \Modules\Vendor\Database\factories\VendorDetailsFactory::new();
    }
}
