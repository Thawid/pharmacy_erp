<?php

namespace Modules\Vendor\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VendorUser extends Model
{
    use HasFactory;
    protected $table = 'vendor_user';
    protected $fillable = ['vendor_id','user_id','status'];
    
    
}
