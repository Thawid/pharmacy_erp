<?php

namespace Modules\Vendor\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class Vendor extends Model
{
    use HasFactory;

    protected $table = "vendors";

    protected $fillable = [
        'name',
        'group_id',
        'phone_number',
        'email',
        'address',
        'origin'
    ];

    public function vgroup(){
        return  $this->hasOne(VendorGroup::class,'id','group_id');
    }

    public function vendor_details(){

        return  $this->hasMany(VendorDetails::class)->with(['pname','vtype','ptype']);

    }

    protected static function newFactory()
    {
        return \Modules\Vendor\Database\factories\VendorFactory::new();
    }
}
