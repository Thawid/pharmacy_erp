<?php

namespace Modules\Vendor\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VendorType extends Model
{
    use HasFactory;

    protected $table = "vendor_types";

    protected $fillable = ['name'];

    public function vdetails(){
        return  $this->belongsTo(VendorDetail::class,'type_id','id');
    }

    protected static function newFactory()
    {
        return \Modules\Vendor\Database\factories\VendorTypeFactory::new();
    }
}
