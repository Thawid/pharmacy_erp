@push('styles')

    <style>
        .required-field::before {
            content: "*";
            color: red;
        }

    </style>

@endpush

<label for="status" class="forget-form">Status <span class="required-field"></label>
<select class="form-control select2" name="status" required>
    <option value="" selected disabled>Select Status</option>
    <option value="1">Active</option>
    <option value="0">InActive</option>
</select>
