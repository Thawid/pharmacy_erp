<label for="vendor_type" class="forget-form ">Vendor Type *</label>
<select class="form-control select2 type_id " name="type_id" id="type_id">
    <option value="0"  disabled selected>Select Vendor Type</option>
    @foreach($vendor_type as $type)
        <option value="{{ $type->id }}"> {{ $type->name }}</option>
    @endforeach
</select>

