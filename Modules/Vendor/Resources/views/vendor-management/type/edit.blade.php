@extends('dboard.index')

@section('title','Edit Vendor Type Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Update Vendor Type</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('vendortype.index')}}">Back</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <form  action="{{ route('vendortype.update',$vendorType->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Vendor Type *</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" @if(isset($vendorType))  value="{{ $vendorType->name }} @endif" required>
                
                                </div>
                            </div>
                        </div><!-- end.row -->
                        <hr>
                
                        <div class="row text-center">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update Vendor Type</button>
                            </div>
                        </div><!-- row -->
                    </form>

                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-ms-12 -->
    </div><!-- end.row -->
    
@endsection






