@extends('dboard.index')

@section('title','View Vendor Type Interface')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Vendor Types</h2>
          </div>

          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{route('vendortype.create')}}">Add Vendor Type</a>
          </div>
          @endcan

        </div><!-- end.row -->
        <hr>

        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="VendorTypeTable">
                <thead class="thead">
                  <tr>
                    <th>SN</th>
                    <th>Vendor Type</th>
                    <th>Status</th>
                    <th width="10%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(isset($vendor_types))
                  @foreach($vendor_types as $vendor_type)
                  <tr>
                    <td>{{ $vendor_type->id }}</td>
                    <td>{{$vendor_type->name}}</td>
                    <td>
                      @if( $vendor_type->status == 1 )
                      <span class="badge badge-success m-1 p-2">Active</span>
                      @else
                      <span class="badge badge-danger m-1 p-2">Inactive</span>
                      @endif
                    </td>

                    <td>
                      <div class="d-md-flex justify-content-around align-items-center">
                        @can('hasDeletePermission')
                        <form action="{{route('vendortype.destroy',$vendor_type->id)}}" method="POST" onclick="return confirm('Are you sure?')">
                          @csrf
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn edit-btn" title="change status" type="submit"><i class="fa fa-refresh"></i></button>
                        </form>
                        @endcan

                        @can('hasEditPermission')
                        {{-- <a class="btn edit-btn editbtn" href="{{route('vendortype.edit',$vendor_type->id)}}" value="{{$vendor_type->id}}" data-toggle="modal" data-body="{{$vendor_type->name}}" data-target="#myModal"><i class="fa fa-lg fa-pencil"></i></a> --}}
                        <button class="btn edit-btn editbtn" href="#" title="edit vendor type" value="{{$vendor_type->id}}"><i class="fa fa-lg fa-pencil"></i></button>
                        @endcan
                      </div>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!-- end.table-responsive -->
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->

<!-- The Modal -->
<div class="modal" id="vendorModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Update Vendor Type</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="{{url('/vendor/vendortype-update')}}" method="POST" enctype="multipart/form-data">
        @csrf
        {{-- @method('POST') --}}
        <!-- Modal body -->
        <div class="modal-body">
          <input type="hidden" class="form-control" id="id" name="id">
          <label class="control-label" for="body">Vendor Type *</label>
          <input class="form-control" name="name" type="text" id="vendorId" required>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn create-btn">Update</button>
        </div>

      </form>

    </div>
  </div>
</div>


@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $('#VendorTypeTable').DataTable();
</script>
<script>
  $(document).ready(function() {
    // $(document).on('click', '.editbtn' function(){
    //     //  $('#vendorModal').modal('show');


    //     var vendor_id = $(this).val();
    //     alert(vendor_id);

    //     $tr = $(this).closest('tr');
    //     var data = $tr.children("td").map(function(){
    //         return $(this).text();
    //     }).get();
    //   console.log(data);

    //  $('#vendorId').val(data[1]);

    //  });


    $(document).on('click', '.editbtn', function() {
      var vendor_id = $(this).val();

      var url = "{{ url('/vendor/vendortype-edit/')}}/" + vendor_id

      //  alert(url);
      $('#vendorModal').modal('show');

      $.ajax({
        type: 'GET',
        url: url,
        // data: "data",
        // dataType: "dataType",
        success: function(response) {
          console.log(response);
          //    var vendorName = response.name;
          //    console.log(vendorName);
          $('#id').val(response.id);
          $('#vendorId').val(response.name);

        }
      });
    });

  })
</script>

@endpush