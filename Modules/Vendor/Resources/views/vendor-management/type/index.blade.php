@extends('dboard.index')

@section('title','View Vendor Type Interface')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Vendor Types</h2>
          </div>

          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{route('vendortype.create')}}">Add Vendor Type</a>
          </div>
          @endcan

        </div><!-- end.row -->
        <hr>

        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="VendorTypeTable">
                <thead class="thead">
                  <tr>
                    <th>SN</th>
                    <th>Vendor Type</th>
                    <th>Status</th>
                    <th width="10%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(isset($vendor_types))
                  @foreach($vendor_types as $vendor_type)
                  <tr>
                    <td>{{ $vendor_type->id }}</td>
                    <td>{{$vendor_type->name}}</td>
                    <td>
                      @if( $vendor_type->status == 1 )
                      <span class="badge badge-success m-1 p-2">Active</span>
                      @else
                      <span class="badge badge-danger m-1 p-2">Inactive</span>
                      @endif
                    </td>

                    <td>
                      <div class="d-md-flex justify-content-around align-items-center">
                        @can('hasDeletePermission')
                        <form action="{{route('vendortype.destroy',$vendor_type->id)}}" method="POST" onclick="return confirm('Are you sure?')">
                          @csrf
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn edit-btn" type="submit"><i class="fa fa-refresh"></i></button>
                        </form>
                        @endcan

                        @can('hasEditPermission')
                        <a class="btn edit-btn" href="{{route('vendortype.edit',$vendor_type->id)}}"><i class="fa fa-lg fa-pencil"></i></a>
                        @endcan
                      </div>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!-- end.table-responsive -->
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->


@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $('#VendorTypeTable').DataTable();
</script>
@endpush