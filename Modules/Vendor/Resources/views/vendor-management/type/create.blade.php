
@extends('dboard.index')

@section('title','Create Vendor Type Form')

@push('styles')

 <style>
   .required-field::before {
     content: "*";
   color: red;
}
 </style>
    
@endpush

@section('dboard_content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">

                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Create Vendor Type</h2>
                    </div>

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('vendortype.index')}}">Back</a>
                    </div>
                </div><!-- end.row -->
                <hr>

                <!-- data -->
                <form method="POST" action="{{ route('vendortype.store') }}" >
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" >Vendor Type <span class="required-field"></label>
                                <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter vendor type name" value="{{ old('name') }}" required>
                            </div>
                        </div>
            
                        <div class="col-md-6">
                            <div class="form-group">
                                @include('vendor::shares.status')
                            </div>
                        </div>
                    </div><!-- end.row -->
                    <hr>
            
                    <div class="row text-right">
                        <div class="col-md-12">
                            <button class="btn create-btn" type="submit">Submit</button>
                        </div>
                    </div><!-- end.row -->
                </form>
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->

@endsection
