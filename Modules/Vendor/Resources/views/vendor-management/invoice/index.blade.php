@extends('dboard.index')

@section('title','Sell Request Details')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile mb-1">
                <div class="tile-body">

                    <div class="row align-items-center">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading"> Invoice List <span class="text-primary"></span></h2>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            {{--<ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('vendor/sellrequest') ? 'active' : '' }} " aria-current="page"
                       href="{{route('sellrequest.index')}}">
                        <h4 class="title-heading">Sell Request List </h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('vendor/sell-remain') ? 'active' : '' }} "
                       href="{{route('sell.request.remain')}}"><h4>Remain List</h4></a>
                </li>
            </ul>--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive bg-light p-3 ">
                        <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
                            <thead>
                            <tr>
                                <th>Details</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($sell_request_list))
                                @foreach($sell_request_list as $key=>$sellRequest)
                                    <tr>
                                        <td>
                                            <address>
                                                <strong>Vendor: {{$sellRequest->vendor->name}} </strong><br>
                                                <strong>Purchase_No:{{$sellRequest->purchase_order_no}}</strong><br>
                                                Created Date: {{$sellRequest->purchase_order_date}} <br>
                                                Request Delivery Date: {{$sellRequest->requested_delivery_date}}<br>
                                            </address>
                                        </td>
                                        @if($sellRequest->priority == 1)
                                            <td><strong">High</strong></td>
                                        @elseif($sellRequest->priority == 2)
                                            <td><strong>Low</strong></td>
                                        @endif


                                        <td>

                                            @if($sellRequest->status == 'Pending')
                                                <p>Pending</p>

                                            @elseif($sellRequest->status == 'Approved')
                                                <p>Approved</p>
                                            @elseif(isset($sellRequest->purchase_order_details[0]) && $sellRequest->purchase_order_details[0]->order_quantity !== $sellRequest->purchase_order_details[0]->delivered_quantity)
                                                <p>Partial</p>
                                            @endif
                                        </td>

                                        <td>
                                            <a href="{{route('sell.request.details',$sellRequest->id)}}"
                                               class="btn details-btn" type="button"><i class="fa fa-eye"
                                                                                        aria-hidden="true"
                                                                                        data-toggle="tooltip"
                                                                                        title="Details"> </i></a>

                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div><!-- end .col-md-12 -->
            </div><!-- end .row -->
        </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection
@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#requisitionList').DataTable();
        });
    </script>
@endpush
