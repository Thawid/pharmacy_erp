@extends('dboard.index')

@section('title','Vendor Profile')

@section('dboard_content')


<div class="tile">
    <div class="tile-body">
        <div class="row align-items-center" id="">
            <div class="col-md-6">
                 <h4 class="title-heading pb-1">{{$vendor_details[0]->vendors->name}}</h4>
                <p>{{  $vendor_details[0]->vendors->phone_number }}</p>
                <p>{{  $vendor_details[0]->vendors->email }}</p>
                <p>{{  $vendor_details[0]->vendors->address }}</p>
            </div><!-- end.col-md-6 --> 
    
            
           <div class="col-md-6">
                {{-- <p>Vendor Group : {{ $vendor->vgroup->name }}</p> --}}
                <p> Vendor Type : @foreach($vendor_details->unique('type_id') as $vdetails)
    
                    {{$vdetails->vtype->name.','}}
                    @endforeach
                </p>
               <p> Origin : {{  $vendor_details[0]->vendors->origin }}</p> 
            </div><!-- end.col-md-6 -->
        </div><!-- end.row -->
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-lg-3">
        <div class="widget-small primary coloured-icon">
            <i class="icon fa fa-users fa-3x"></i>
            <div class="info">
                <h4>Total Purchase Order</h4>
                <p><b>{{$vendor_purchase_order}}</b></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3">
        <div class="widget-small info coloured-icon">
            <!-- <i class="icon fa fa-thumbs-o-up fa-3x"></i> -->
            <i class="icon fa fa-home fa-3x"></i>
            <div class="info">
                <h4>Purchase Amount</h4>
                <p><b>{{$total_purchase_amount[0]->total_amount}}</b></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3">
        <div class="widget-small warning coloured-icon">
            <i class="icon fa fa-files-o fa-3x"></i>
            <div class="info">
                <h4>Paid Amount</h4>
                <p><b>{{$total_pay_amount[0]->total_pay_amount}}</b></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3">
        <div class="widget-small danger coloured-icon">
            <i class="icon fa fa-star fa-3x"></i>
            <div class="info">
                <h4>Due Amount</h4>
                <p><b>{{$due_amount}}</b></p>
            </div>
        </div>
    </div>
</div>

    
    <div class="tile">
        <div class="tile-body">
            <div class="row">
                <div class="col-md-12 ">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{route('vendor.purchase.order',isset($purchase_order[0]->id))}}">
                                <h4>Purchase Orders</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{route('vendor.purchase.product', isset($vendor_details[0]->id))}}">
                                <h4>Products</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{route('vendor.purchase.payment',isset($payment_history[0]->id))}}">
                                <h4>Payment History</h4>
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link {{ request()->is('procurement/hq-requisition') ? 'active' : '' }} " href="{{route('hq.requisitions')}}"><h4>HQ Requisition</h4></a>
                        </li> --}}
                    </ul>
                    <div class="row">
                                <div class="col-md-12 table-responsive" style="padding-bottom: 30px">
                                    <div class="table-responsive bg-white p-3 ">
                                        <table class="table table-striped table-hover requisition-list-hq" id="requisitionListHq">
                                            <thead class="thead thead-light">
                                            <tr>
                                                <th>Product Info: </th>
                        
                                                <th>Purchase Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                          
                                                @foreach ($vendor_details as $item)
                                                    <tr>
                                                      
                                                        <td><address> 
                                                        
                                                        <p>Product Name:   {{ $item->product->product_name}}  </p>
                                                        <p>Brand Name:  {{$item->product->brand_name}} </p>
                                                      
                                                        </address></td>
                                                        <td>{{$item->product->purchase_price}}</td>
                                                      
                                                    </tr>
                                                    @endforeach 
                                            </tbody> 
                                        </table>
                                    </div>
                                </div> <!-- end .col-md-12 -->
                            </div> <!-- end .row -->
                </div> <!-- end .col-md-12 -->
            </div><!-- end .row -->
        
        </div>
    </div>


@endsection
