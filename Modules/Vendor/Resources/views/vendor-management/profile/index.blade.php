@extends('dboard.index')

@section('title','Vendor Profile')

@section('dboard_content')


<div class="tile">
    <div class="tile-body">
        <div class="row align-items-center" id="">
            <div class="col-md-6">
                <h4 class="title-heading pb-1">{{ $vendor->name }}</h4>
                <p>{{ $vendor->phone_number }}</p>
                <p>{{ $vendor->email }}</p>
                <p>{{ $vendor->address }}</p>
            </div><!-- end.col-md-6 -->

            <div class="col-md-6">
                {{-- <p>Vendor Group : {{ $vendor->vgroup->name }}</p> --}}
                <p> Vendor Type : @foreach($vendor->vendor_details->unique('type_id') as $vdetails)

                    {{$vdetails->vtype->name.','}}
                    @endforeach
                </p>
                <p> Origin : {{ $vendor->origin }}</p>
            </div><!-- end.col-md-6 -->
        </div><!-- end.row -->
    </div>
</div>

    <div class="row">
        <div class="col-md-6 col-lg-3">
            <div class="widget-small primary coloured-icon">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Total Purchase Order</h4>
                    <p><b>{{$vendor_purchase_order}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small info coloured-icon">
                <!-- <i class="icon fa fa-thumbs-o-up fa-3x"></i> -->
                <i class="icon fa fa-home fa-3x"></i>
                <div class="info">
                    <h4>Purchase Amount</h4>
                    <p><b>{{isset($total_purchase_amount[0])?$total_purchase_amount[0]->total_amount: 0}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small warning coloured-icon">
                <i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Paid Amount</h4>
                    <p><b>{{isset($total_pay_amount[0]) ? $total_pay_amount[0]->total_pay_amount : 0}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small danger coloured-icon">
                <i class="icon fa fa-star fa-3x"></i>
                <div class="info">
                    <h4>Due Amount</h4>
                    <p><b>{{$due_amount}}</b></p>
                </div>
            </div>
        </div>
    </div>


    <div class="tile">
        <div class="tile-body">
            <div class="row">
                <div class="col-md-12 ">
                    <ul class="nav nav-tabs">
                        @if(isset($purchase_order[0]))
                            <li class="nav-item">

                                <a class="nav-link" aria-current="page"
                                   href="{{route('vendor.purchase.order', $purchase_order[0]['id'])}}">
                                    <h4>Purchase Orders</h4>
                                </a>
                            </li>
                        @endif
                        @if(isset($product_history[0]))
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page"
                                   href="{{route('vendor.purchase.product',$product_history[0]['id'])}}">
                                    <h4>Products</h4>
                                </a>
                            </li>
                        @endif
                        @if(!empty($payment_history[0]))
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page"
                                   href="{{route('vendor.purchase.payment',$payment_history[0]->id)}}">
                                    <h4>Payment History</h4>
                                </a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="javascript:void(0);">
                                    <h4>Payment History</h4>
                                </a>
                            </li>
                        @endif

                        {{-- <li class="nav-item">
                            <a class="nav-link {{ request()->is('procurement/hq-requisition') ? 'active' : '' }} " href="{{route('hq.requisitions')}}"><h4>HQ Requisition</h4></a>
                        </li> --}}
                    </ul>
                    <div class="row">
                                <div class="col-md-12 table-responsive" style="padding-bottom: 30px">
                                    <div class="table-responsive bg-white p-3 ">
                                        <table class="table table-striped table-hover requisition-list-hq" id="requisitionListHq">
                                            <thead class="thead thead-light">
                                            <tr>
                                                <th>Purchase Info</th>
                                                <th>Hub Info</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>


                                                    <tr>
                                                        <td><address>

                                                        <p>Purchase Order No: {{isset($purchase_order[0]) ? $purchase_order[0]->purchase_order_no : 0}}</p>
                                                        <p>Requstion Date: {{isset($purchase_order[0]) ? $purchase_order[0]->requested_delivery_date : null}} </p>
                                                        <p>Delievry Date: {{isset($purchase_order[0]) ? $purchase_order[0]->vendor_delivery_date: null}} </p>
                                                        </address></td>
                                                        <td>Hub Requestion NO: {{isset($purchase_order[0]) ? $purchase_order[0]->hub_requisition_no: 0}}</td>
                                                        <td>{{isset($purchase_order[0]) ? $purchase_order[0]->total: 0}}</td>

                                                    </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> <!-- end .col-md-12 -->
                            </div> <!-- end .row -->
                </div> <!-- end .col-md-12 -->
            </div><!-- end .row -->

        </div>
    </div>


@endsection
