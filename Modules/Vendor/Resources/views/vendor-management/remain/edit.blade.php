@extends('dboard.index')

@section('title','Edit Sell Request ')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Edit Remain Request</h2>
                        </div>
                        <div class="col-md-3 text-md-right">
                            <a class="btn btn-outline-primary icon-btn" href="{{route('sell.request.remain')}}"><i class="fa fa-arrow-right"></i> Sell Remain List</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="tile mb-1">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <p><strong style="color: deepskyblue">Purchase Order No:  {{$sell_request_edit->purchase_order_no}}</strong></p>
                            <p><strong style="color: deepskyblue">HUB Requisition No: {{$sell_request_edit->hub_requisition_no}}</strong></p>
                            {{--                            <p><strong>Vendor Name:  {{$sell_request_edit->vendor->name}}</strong></p>--}}
                        </div>
                        <div class="col-md-4 form-group">
                            <p><strong>Request Date:  {{$sell_request_edit->purchase_order_date}}</strong></p>
                            <p><strong>Request Delivery Date: {{$sell_request_edit->purchase_order_date}} </strong></p>

                            @if($sell_request_edit->priority == 1)
                                <P><strong style="color: red">Priority: High </strong></P>
                            @elseif($sell_request_edit->priority == 2)
                                <P><strong style="color: green">Priority: Low </strong></P>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="{{route('sell.request.update', $sell_request_edit->id)}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="form-group col-md-12">
                <div class="tile m-0">
                    <div class="tile-body">
                        <div class="row">
                            <input type="hidden" name="purchase_order_no" value="{{$sell_request_edit->purchase_order_no}}">
                            <input type="hidden" name="vendor_id" value="{{$sell_request_edit->vendor_id}}">
                            <div class="col-md-3 form-group">
                                <label for=""> Request Date *</label>
                                <input type="date" name="purchase_order_date" id="purchase_order_date" value="{{$sell_request_edit->purchase_order_date}}" placeholder="yyyy-mm-dd" class="form-control" readonly="" required>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for=""> Request Delivery Date *</label>
                                <input type="date" name="delivery_date" id="delivery_date" value="{{$sell_request_edit->requested_delivery_date}}" placeholder="yyyy-mm-dd" class="form-control" readonly="" required>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for=""> Vendor Delivery Date *</label>
                                <input type="date" name="vendor_delivery_date" value="{{ old('vendor_delivery_date') }}" class="form-control">
                            </div>

                            <div class="col-md-3 text-left">
                                <div class="form-group">
                                    <label for="vendor" class="forget-form"> Priority *</label>
                                    <select class="form-control vendor_group " name="priority" id="priority" required>
                                        <option value="">Select Priority</option>
                                        <option {{$sell_request_edit->priority == 1 ?'selected':''}} value="1">High</option>
                                        <option {{$sell_request_edit->priority == 2 ?'selected':''}} value="2">Low</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group col-md-12 p-0">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <table class="table table-striped table-bordered table_field table-sm" id="table_field" style="margin-top: 10px">
                            <thead class="thead-light">
                            <tr class="bg-white">
                                <th>Product Name</th>
                                <th>Generic Name</th>
                                <th width="10%">Unit</th>
                                {{--                                <th width="10%">Vendor</th>--}}
                                <th width="10%">UOM</th>
                                <th>Order Quantity</th>
                                <th>Delivery Quantity</th>
                                <th>Remain Quantity</th>
                                <th>UOM Price</th>
                                <th>Discount (%)</th>
                                <th>Sub Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sell_request_edit->purchase_order_details as $sellRequestDetail)

                                {{--                             {{dd($sell_request_edit)}}--}}
                                <tr>
                                    <td>
                                        <select class="form-control" name="product_id[]" id="productNameId">
                                            <option value="{{$sellRequestDetail->product_id}}" readonly="">{{$sellRequestDetail->product->product_name ?? ' '}}</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input class="form-control" type="hidden" name="generic_id[]" id="genericId" value="{{$sellRequestDetail->generic_id}}" readonly>
                                        <input class="form-control" type="text" value="{{$sellRequestDetail->generic->name}}" readonly>
                                    </td>
                                    <td>
                                        <select class="form-control" name="unit_id[]">
                                            <option value="{{$sellRequestDetail->unit_id}}" readonly="">{{$sellRequestDetail->unit->unit_name}}</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" name="uom_id[]">
                                            <option value="{{$sellRequestDetail->uom_id}}" readonly="">{{$sellRequestDetail->uom->uom_name??' '}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" name="order_quantity[]" id="order_quantity_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$sellRequestDetail->order_quantity}}" readonly=""></td>
                                    <td>
                                        <input class="form-control" type="number" name="delivered_quantity[]" id="delivered_quantity_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$sellRequestDetail->delivered_quantity}}" readonly="">
                                    </td>
                                    <td>
                                        <input class="form-control" type="number" name="remain_quantity[]" id="remain_quantity_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$sellRequestDetail->remain_quantity}}" readonly="">
                                    </td>
                                    <td><input class="form-control" type="number" required name="uom_price[]" id="unit_price_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$sellRequestDetail->uom_price}}" readonly=""></td>
                                    <td><input class="form-control" type="number" name="discount[]" step="0.01" min="1" max="100" id="discount_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$sellRequestDetail->discount !=0?$sellRequestDetail->discount:''}}"></td>
                                    <td><input class="form-control sub_total text-center" type="number" name="product_sub_total[]" id="sub_total_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$sellRequestDetail->sub_total}}" readonly=""></td>
                                    {{--<input class="form-control" type="hidden" name="remain_quantity" id="remain_quantity" data-id="{{$loop->index}}" value="{{$sellRequestDetail->remain_quantity}}">--}}

                                </tr>

                            @endforeach

                            <tr class="mt-3">
                                <th colspan="9" class="text-right">Sub Total</th>
                                <td class="text-center "><input class="form-control text-center" id="sub_total" type="number" name="sub_total" value="{{$sell_request_edit->sub_total}}" readonly=""></td>
                            </tr>
                            <tr class="mt-3">
                                <th colspan="9" class="text-right">Overall Discount (TK)</th>
                                <td class="text-center"><input class="form-control text-center" step="0.01" id="overall_discount" type="number" name="overall_discount" value="{{$sell_request_edit ->overall_discount}}"></td>
                            </tr>

                            <tr class="mt-3">
                                <th colspan="9" class="text-right">Total</th>
                                <td class="text-center"><input class="form-control text-center" id="total_price" type="number" name="total_price" value="{{$sell_request_edit->total}}" readonly></td>
                            </tr>

                            <tr class="mt-3">
                                <th colspan="9" class="text-right">Other Expense</th>
                                <td class="text-center"><input class="form-control text-center" id="other_expense" type="number" name="other_expense" value="{{$sell_request_edit ->other_expense}}"></td>
                            </tr>


                            <tr class="mt-3">
                                <th colspan="9" class="text-right">Vat & Tax</th>
                                <td class="text-center"><input class="form-control text-center" id="vat_tax" type="number" name="vat_tax" value="{{$sell_request_edit ->vat_tax}}"></td>
                                <input class="form-control text-center" type="hidden" name="hq_id" value="{{$sell_request_edit->hq_requisition_id}}" readonly="">
                            </tr>
                            {{--                            <td></td>--}}
                            </tbody>
                        </table>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-left">
                <button class="btn btn-primary text-left">Attached File <input type="file" name="attach_file"></button>
            </div>
        </div>

        <div class="col-sm-12">
            <button type="submit" class="btn btn-primary pull-right  m-4">Save & Update</button>
        </div>
        </div>
    </form>

@endsection
@push('post_scripts')
    <script>
        $(document).ready(function() {
            $('.form-control').on('input', function() {
                var id = $(this).data("id");
                var remain_quantity = $('#remain_quantity' + id).val();
                alert(remain_quantity)
                // var order_quantity = $('#order_quantity_' + id).val();



                var unit_price = $('#unit_price_' + id).val() ? $('#unit_price_' + id).val() : 0;
                var discount = $('#discount_' + id).val() ? $('#discount_' + id).val() : 0;
                var sub_total = $('#sub_total_' + id).val() ? $('#sub_total_' + id).val() : 0;
                var overall_discount = $('#overall_discount').val() ?$('#overall_discount').val():0;



                // remain_quantity = parseFloat(order_quantity) - parseFloat(delivered_quantity);
                // $('#remain_quantity').val(parseFloat(remain_quantity));


                total_price = parseFloat(remain_quantity) * parseFloat(unit_price);
                total_discount = (parseFloat(total_price) * parseFloat(discount)) / 100;
                total = parseFloat(total_price) - parseFloat(total_discount)

                $('#sub_total_' + id).val(total);
                var total_sum = 0;
                $('.sub_total').each(function() {
                    var inputval = $(this).val();
                    if ($.isNumeric(inputval)) {
                        total_sum += parseFloat(inputval);
                    }
                })

                if($.isNumeric(total_sum)){
                    $('#sub_total').val(parseFloat(total_sum));

                    var total_price = parseFloat(parseFloat(total_sum) - parseFloat(overall_discount));
                    $('#total_price').val(parseFloat(total_price));
                }
            })
        });
    </script>

    <script type="text/javascript" lang="javascript">
        // Remove Item
        $(document).on('click',  '.item_remove', function(){
            $(this).closest('tr').remove();
            var delete_id = $(this).attr('id');


            $.ajax({
                url: "{{route('sell.request.remove')}}",
                type: 'get',
                data: {purchase_order_details_id:delete_id},
                success: function(response){
                    console.log(response.data);
                }
            });

        });

    </script>

@endpush
