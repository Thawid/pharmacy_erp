@extends('dboard.index')

@section('title', 'Update Vendor User')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Update Vendor User</h2>
          </div>
          <div class="col-md-6 text-right">
            <a class="btn index-btn" href="{{ route('vendor-user.index') }}">Back</a>
          </div>
        </div><!-- end.row -->
        <hr>

        <!-- data -->
        <form method="POST" action="{{ route('vendor-user.update', $users_and_vendors['vendor_user']->id) }}">
          @csrf
          @method('PATCH')
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label for="exampleSelect1">Select Vendor </label>
                <select class="form-control " name="vendor_id">
                  <option selected="true" disabled="disabled">Select</option>
                  @foreach ($users_and_vendors['vendors'] as $vendor )
                  <option value="{{$vendor->id}}" @if ($vendor->id == $users_and_vendors['vendor_user']->vendor_id) selected @endif >{{$vendor->name}}</option>
                  @endforeach
                </select>
              </div>
            </div><!-- end.col-md-6 -->

            <div class="col-md-6 col-lg-6 col-xl-6">
            <div class="form-group">
                <label for="exampleSelect1">Select User </label>
                <select class="form-control " name="user_id">
                  <option selected="true" disabled="disabled">Select</option>
                  @foreach ($users_and_vendors['users'] as $user )
                  <option value="{{$user->id}}" @if ($user->id == $users_and_vendors['vendor_user']->user_id) selected @endif >{{$user->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div><!-- end.row -->
          <hr>

          <div class="row text-right">
            <div class="col-md-12">
              <button class="btn create-btn" type="submit">Update</button>
            </div>
          </div>
        </form>
      </div><!-- end.tite-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->


@endsection