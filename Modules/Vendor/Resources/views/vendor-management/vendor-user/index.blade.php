@extends('dboard.index')

@section('title','View Vendor User Interface')

@section('dboard_content')

    <div class="row" id="actionModal">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6 text-left">
                            <h2 class="title-heading">Vendor User Details</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn create-btn" href="{{route('vendor-user.create')}}">Add New Vendor User</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- data -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="StoreTypeTable">
                                    <thead class="thead">
                                    <tr>
                                        <th>SL</th>
                                        <th>Vendor Name</th>
                                        <th>User  Name</th>
                                        <th>User  Role</th>
                                        <th>Status</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($all_vendor_user))
                                        @foreach( $all_vendor_user as $key => $vendorUser)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{$vendorUser->vendor_name}}</td>
                                                <td>{{$vendorUser->user_name}}</td>
                                                <td>{{$vendorUser->user_role}}</td>
                                                <td>{{$vendorUser->vendor_user_status == 1 ? 'Active' : 'InActive' }}</td>
                                                <td class="">
                                                    <div class="d-md-flex justify-content-around align-items-center">
                                                        <form class="mr-3" id="delete_form{{$vendorUser->id}}" method="POST" action="{{ route('vendor-user.destroy',$vendorUser->id) }}" onclick="return confirm('Are you sure?')">@csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            <button class="btn edit-btn" type="submit"><i class="fa fa-refresh"></i></button>
                                                        </form>

                                                        <a class="btn edit-btn " href="{{route('vendor-user.edit',$vendorUser->id)}}"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- end.table-responsive -->
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">$('#StoreTypeTable').DataTable();</script>
@endpush
