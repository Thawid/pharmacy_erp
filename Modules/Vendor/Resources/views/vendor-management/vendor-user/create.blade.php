@extends('dboard.index')

@section('title', 'Create New Vendor User Form')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Create New Vendor User</h2>
          </div>
          <div class="col-md-6 text-right">
            <a class="btn index-btn" href="{{ route('vendor-user.index') }}">Back</a>
          </div>
        </div><!-- end.row -->
        <hr>

        <!-- data -->
        <form method="POST" action="{{ route('vendor-user.store') }}">
          @csrf
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label for="exampleSelect1">Select Vendor </label>
                <select class="form-control " name="vendor_id">
                  <option selected="true" disabled="disabled">Select</option>
                  @foreach ($users_and_vendors['vendors'] as $vendor )
                  <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                  @endforeach
                </select>
              </div><!-- end.form-group -->

              <div class="form-group">
                @include('store::shares.status')
              </div>
            </div><!-- end.col-md-6 -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleSelect1">Select User </label>
                <select class="form-control " name="user_id">
                  <option selected="true" disabled="disabled">Select</option>
                  @foreach ($users_and_vendors['users'] as $user )
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
                </select>
              </div><!-- end.form-group -->

              <div class="form-group">
              <button class="btn create-btn form-control mt-4" type="submit">Submit</button>
              </div><!-- end.form-group -->
            </div><!-- end.col-md-6 -->
          </div><!-- end.row -->
        </form>
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->

@endsection