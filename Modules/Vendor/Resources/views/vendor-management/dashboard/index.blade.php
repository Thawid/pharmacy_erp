@extends('dboard.index')
@section('title','Vendor Dashboard')
@section('dboard_content')

    <div class="row">
        <div class="col-md-6 col-lg-4 col-xl-4">
            <div class="widget-small primary coloured-icon">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Total Purchase Order</h4>
                    <p><b>{{$total_purchase_orders}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
            <div class="widget-small info coloured-icon">
                <!-- <i class="icon fa fa-thumbs-o-up fa-3x"></i> -->
                <i class="icon fa fa-home fa-3x"></i>
                <div class="info">
                    <h4>Purchase Amount</h4>
                    <p><b>{{$total_purchase_amount[0]->grand_total ?? 0}} BDT</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
            <div class="widget-small warning coloured-icon">
                <i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Total Vendors</h4>
                    <p><b>{{$total_vendors}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
            <div class="widget-small danger coloured-icon">
                <i class="icon fa fa-star fa-3x"></i>
                <div class="info">
                    <h4>Total Pending</h4>
                    <p><b>{{$total_pending}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
            <div class="widget-small danger coloured-icon">
                <i class="icon fa fa-star fa-3x"></i>
                <div class="info">
                    <h4>Recent Order</h4>
                    <p><b>{{$recent_orders}}</b></p>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">Vendor Statistic</h3>
                <div id="bar_chart"></div>
            </div>
        </div>

    </div>--}}
@endsection
@push('post_scripts')
    {{--<script type="text/javascript" src="{{ asset('js/apex_chart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart_custom.js') }}"></script>
    <script>
        // bar chart

        var options = {
            chart: {
                type: 'bar',
                height: 280,
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '48%',
                },
            },
            colors: ['#0062FF', '#D5D7E3'],
            series: [
                {
                    name: 'Sales',
                    data: [
                        @php
                            if(count($monthly_sales) > 0){
                                foreach ($monthly_sales as $sale){
                                    echo $sale->total . ",";
                                }
                            }
                        @endphp
                    ]

                }
            ],
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: [
                    @php
                        if(count($monthly_sales) > 0){
                              foreach ($monthly_sales as $sale){
                                  echo '"'.$sale->month .'",';
                              }
                        }
                    @endphp
                ],
                axisBorder: {
                    show: true,
                    color: 'rgba(0,0,0,0.05)'
                },
            },
            grid: {
                row: {
                    colors: ['transparent', 'transparent'], opacity: .2
                },
                borderColor: 'rgba(0,0,0,0.05)'
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            },
            legend: {
                show: false
            }
        };
        var chart = new ApexCharts(document.querySelector("#bar_chart"), options);
        chart.render();
    </script>--}}
@endpush
