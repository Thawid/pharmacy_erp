@extends('dboard.index')

@section('title','View Vendor Type Interface')

@section('dboard_content')

<div class="tile">
  <div class="tile-body">

    <!-- title -->
    <div class="row align-items-center">
      <div class="col-md-6">
        <h2 class="title-heading">Vendor List </h2>
      </div>

      @can('hasCreatePermission')
      <div class="col-md-6 text-md-right">
        <a class="btn create-btn" href="{{route('vendors.create')}}">Add Vendor</a>
      </div>
      @endcan
    </div><!-- end.row -->
    <hr>

    <!-- body -->
    <form action="" id="reset-form">
      <div class="row">
        <div class="col-md-3 col-lg-3 col-xl-3">
          <label for="vendor" class="forget-form">Vendor Type*</label>
          <select class="form-control select2" name="vendor_type" id="vendor_type">
            <option value="" selected disabled>Select vendor Type</option>
            @foreach($vendor_type as $vtype)
            <option value="{{ $vtype->id }}"> {{ $vtype->name }}</option>
            @endforeach
          </select>
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3">
          <label for="vendor" class="forget-form">Vendor Group Type*</label>
          <select class="form-control select2" name="group_type" id="group_type">
            <option value="" selected disabled>Select vendor group</option>
            @foreach($vendor_group as $vgroup)
            <option value="{{ $vgroup->id }}"> {{ $vgroup->name }}</option>
            @endforeach

          </select>
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3">
          @include('vendor::vendor-management.shares.vendor-origins')
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3 text-right">
          <div class=" reset_action">
            <button type="button" name="filter" id="filter" class="btn index-btn mr-2">Search
            </button>

            <button type="button" name="reset" id="reset" class="btn index-btn">
              <i class="fa fa-refresh"></i>
            </button>
          </div>
        </div>
      </div><!-- end.row -->
    </form>
    <hr>

    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover vendor-list" id="vendorList">
            <thead>
              <tr>
                <th>Logo</th>
                <th>Vendor Details</th>
                <th>Vendor Type</th>
                <th width="15%">Action</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div><!-- end.table-res -->
      </div><!-- end.col-md-12 -->
    </div><!-- end.row -->

  </div><!-- end.tile-body -->
</div><!-- end.tile -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script type="text/javascript">
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {

    var vendor_type = $('#vendor_type').val();
    var group_type = $('#group_type').val();
    var origin = $('#origin').val();

    var dataTable = $('.vendor-list').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url: "{{ route('vendor.list') }}",
        'data': function(data) {
          //console.log(data);
          data.vendor_type = $('#vendor_type').val();
          data.group_type = $('#group_type').val();
          data.origin = $('#origin').val();

        }
      },
      columns: [{
          data: 'logo',
          name: 'logo',
          orderable: false,
          searchable: false
        },
        {
          data: 'details',
          name: 'details',
          searchable: false
        },
        {
          data: 'vtype',
          name: 'vtype',
          searchable: false
        },
        {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false
        },
        {
          data: 'name',
          name: 'name',
          orderable: false,
          searchable: true,
          visible: false
        },
        {
          data: 'phone_number',
          name: 'phone_number',
          orderable: false,
          searchable: true,
          visible: false
        },
        {
          data: 'email',
          name: 'email',
          orderable: false,
          searchable: true,
          visible: false
        },
      ]
    });


    $('#filter').click(function() {
      //console.log(dataTable);
      dataTable.draw();

    });

    $('#reset').click(function() {
      $("#vendor_type").select2("val", " ");
      $('#vendor_type').select2({
        placeholder: "Select vendor Type"
      });
      $("#group_type").select2("val", " ");
      $('#group_type').select2({
        placeholder: "Select Group Type"
      });
      $("#origin").select2("val", " ");
      $('#origin').select2({
        placeholder: "Select Origin"
      });
      dataTable.draw();
    });

  });
</script>
@endpush