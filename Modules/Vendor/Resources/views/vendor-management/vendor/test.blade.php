
@extends('dboard.index')

@section('title','Create Vendor Type Form')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2>Add New Vendor</h2>
                        </div>
                        <div class="col-md-12 text-center">
                            <a class="btn btn-primary icon-btn" href="{{route('vendors.index')}}"><i class="fa fa-arrow-left"></i>Back</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <form method="POST" action="{{ route('vendors.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                                <div class="row">

                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label class="control-label" >Vendor Name</label>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter vendor name" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label for="vendor" class="forget-form">Vendor Type*</label>
                                            <select class="form-control select2" name="type_id">
                                                @foreach($vendor_type as $type)
                                                    <option value="{{ $type->id }}"> {{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label for="vendor" class="forget-form">Vendor Group*</label>
                                            <select class="form-control select2" name="group_id">
                                                <option value="">Select Group</option>
                                                @foreach($vendor_group as $group)
                                                    <option value="{{ $group->id }}"> {{ $group->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label for="vendor" class="forget-form">Product Type</label>
                                            <select class="form-control select2" name="product_type_id[]" multiple>
                                                @foreach($product_type as $ptype)
                                                    <option value="{{ $ptype->id }}"> {{ $ptype->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label class="control-label" >Moblie</label>
                                            <input class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" type="text" placeholder="Enter mobile number" value="{{ old('phone_number') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label class="control-label" >Email</label>
                                            <input class="form-control @error('email') is-invalid @enderror" name="email" type="text" placeholder="Enter email " value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <div class="form-group">
                                            <label class="control-label" >Address</label>
                                            <input class="form-control @error('address') is-invalid @enderror" name="address" type="text" placeholder="Enter address" value="{{ old('address') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label class="control-label" >Vendor Logo</label>
                                            <input type="file" class="form-control" id="logoImg" name="vendor_logo" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <div class="form-group">
                                            <label for="vendor" class="forget-form">Vendor Origin</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" id="local" name="origin" value="Local">
                                            <label class="form-check-label" for="local">Local</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" id="foreign" name="origin" value="Foreign">
                                            <label class="form-check-label" for="foreign">Foreign</label>
                                        </div>
                                    </div>

                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label for="vendor" class="forget-form">Vendor Type*</label>
                                            <select class="form-control vendor_type " name="vendor_type" >
                                                <option value="vendor_type">Supplier</option>
                                                <option value="">Manu</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 text-left">
                                        <div class="form-group">
                                            <label for="vendor" class="forget-form">Product Type*</label>
                                            <select class="form-control product_type " name="product_type" >
                                                <option value="product_type">Vac</option>
                                                <option value="">Tab</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-1 mt-4">
                                        <div class="form-group ">
                                            <button type="submit" class="btn btn-success btn-block add_item ">Add</button>
                                        </div>
                                    </div>

                                    <div class="mt-4">
                                        <div class="form-group ">
                                            <button type="submit" class="btn btn-secondary  mr-3">Cancel</button>
                                        </div>
                                    </div>

                                </div>
                        </div>
                    </div>
                </div>
                </div>

                <div class="row" id="actionModal">
                    <div class="col-md-12">
                        <div class="tile">
                            <div class="tile-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered" id="vendorList">
                                        <thead class="thead">
                                        <tr>
                                            <th>SN</th>
                                            <th>Vendor Type</th>
                                            <th>Product Type</th>

                                        </tr>
                                        </thead>
                                        <tbody class="item_add_area">
                                        <tr>


                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        {{ csrf_field() }}

        <div class="form-group col-md-12">

            <br><br>
            <div class="form-group col-md-12">
                <div class="row">


                    <div class="col-md-2">
                        @include('product::shares.product-types')

                    </div>




                    <div class="col-md-1" style="padding-left: 0;">
                        <label for=""></label>
                        <a href="javascript:" class="btn btn-success btn-block add_item" style="margin-top: 4px;">Add</a>
                    </div>
                </div><br><br>
                <table class="table table-striped table-bordered table-sm" style="margin-top: 10px">
                    <thead>
                    <tr class="bg-white">
                        <th style="width: 25%;">Vendor Type</th>
                        <th style="width: 15%;">Product Type</th>

                    </tr>
                    </thead>
                    <tbody class="item_add_area">
                    <tr>
                        <td>easrtgre</td>
                    </tr>
                    </tbody>

                </table>


                <div class="col-sm-12" style="text-align: right;">
                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                </div>

            </div>


    </form>

@endsection
@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $('.select2').select2({});
    </script>
    <script type="text/javascript" lang="javascript">
        // Item Add function goes here
        $(document).on('click', '.add_item', function(){


            var type         = $('.type').val();
            var product_type_id     = $('.product_type_id').val();




            $('.item_add_area').append('<tr>'+
                '<td><input type="text" class="form-control vendor_type" name="vendor_type[]" value="'+ type +'"></td>'+
                '<input name="type_id[]" type="hidden" value="' + type_id + '" class="">\n'+
                '<td><input type="text" class="form-control product_type_id" name="product_type_id[]" value="'+ product_type_id +'"></td>'+
                '<input name="product_type_id[]" type="hidden" value="' + product_type_id + '" class="">\n'+
                '<td><a href="#" class="btn btn btn-danger item_remove btn-xs">x</a></td>'+
                '</tr>');


            $('.type').val('');

            $('.product_type_id').val('');

        });

        // Remove Item
        $(document).on('click', '.item_remove', function(){
            $(this).parent().parent().remove();

        })




    </script>
    <script type="text/javascript" lang="javascript">




        // Item Add function goes here

        $(document).on('click', '.add_item', function(){

            var type         = $('.type').val();
            var quantity     = $('.quantity').val();
            var unit_price   = $('.unit_price').val();

            $('.item_add_area').append('<tr>'+
                '<td><input type="text" class="form-control item_type" name="item_type[]" value="'+ type +'"></td>'+
                '<td><input type="text" class="form-control item_quantity" name="item_quantity[]" value="'+ quantity +'"></td>'+
                '<td><input type="text" class="form-control item_unit_price" name="item_unit_price[]" value="'+ unit_price +'"></td>'+
                '<td><a href="#" class="btn btn btn-danger item_remove btn-xs">x</a></td>'+
                '</tr>');
            $('.unit_price').val('');

            $('.type').val('');

            $('.unit_type').val('');

        });

        // Remove Item
        $(document).on('click', '.item_remove', function(){
            $(this).parent().parent().remove();

        })




    </script>
@endpush

@extends('layouts.default')
@section('content')
    @if(session()->has('msg'))
        <div class="alert alert-success">
            {{ session()->get('msg') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12" style="">
                <div class="hpanel">
                    <div class="panel-heading hbuilt">

                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form action="{{ url('sales') }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group col-md-12">
                                    <div class="row">

                                        <div class="col-md-4" style="margin-top: 8px;">
                                            <label for="billing_address"> Address:</label>
                                            <textarea name="billing_address" id="billing_address" class="form-control billing_address" placeholder="Billing Address"></textarea>
                                        </div>
                                        <div class="col-md-4" style="margin-top: 8px;">
                                            <label for="customer_id">Email</label>
                                            <input type="email" name="email" class="form-control">
                                        </div>
                                    </div>

                                </div>
                                <br><br>
                                <div class="form-group col-md-12">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <label for="type">Course:</label>
                                            <input name="type" id="type" class="form-control type" placeholder="Package Type">

                                        </div>
                                        <div class="col-md-1">
                                            <label for="quantity">Section:</label>
                                            <input name="quantity" id="quantity" class="form-control quantity"  placeholder="Quantity">
                                            <small class="text-danger text_view"></small>
                                        </div>
                                        <div class="col-md-1">
                                            <label for="unit_price">Routine:</label>
                                            <input name="unit_price" id="unit_price" class="form-control unit_price" placeholder="Price">
                                        </div>

                                        <div class="col-md-1" style="padding-left: 0;">
                                            <label for=""></label>
                                            <a href="javascript:" class="btn btn-success btn-block add_item" style="margin-top: 4px;">Add</a>
                                        </div>
                                    </div><br><br>
                                    <table class="table table-striped table-bordered table-sm" style="margin-top: 10px">
                                        <thead>
                                        <tr class="bg-white">
                                            <th style="width: 25%;">Course</th>
                                            <th style="width: 15%;">Section</th>
                                            <th style="width: 10%;">Routine</th>


                                        </tr>
                                        </thead>
                                        <tbody class="item_add_area">
                                        </tbody>

                                    </table>


                                    <div class="col-sm-12" style="text-align: right;">
                                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                                    </div>

                                </div>


                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" lang="javascript">




        // Item Add function goes here

        $(document).on('click', '.add_item', function(){

            var type         = $('.type').val();
            var quantity     = $('.quantity').val();
            var unit_price   = $('.unit_price').val();



            $('.item_add_area').append('<tr>'+
                '<td><input type="text" class="form-control item_type" name="item_type[]" value="'+ type +'"></td>'+
                '<td><input type="text" class="form-control item_quantity" name="item_quantity[]" value="'+ quantity +'"></td>'+
                '<td><input type="text" class="form-control item_unit_price" name="item_unit_price[]" value="'+ unit_price +'"></td>'+
                '<td><a href="#" class="btn btn btn-danger item_remove btn-xs">x</a></td>'+
                '</tr>');
            $('.unit_price').val('');

            $('.type').val('');

            $('.unit_type').val('');




        });




        // Remove Item
        $(document).on('click', '.item_remove', function(){
            $(this).parent().parent().remove();

        })





    </script>
@stop
