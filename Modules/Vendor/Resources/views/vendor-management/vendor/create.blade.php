
@extends('dboard.index')

@section('title','Create Vendor Type Form')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Add New Vendor</h2>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a class="btn index-btn" href="{{route('vendors.index')}}">Back</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- body -->
                    <form method="POST" action="{{ route('vendors.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label for=""> Vendor Name *</label>
                                    <input type="text" name="name" value="{{old('name')}}" class="form-control" >
                                </div>
                
                                <div class="form-group" >
                                    <label for="customer_id">Email</label>
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                                </div>
                
                                <div class="form-group" >
                                    <label for="">Address</label>
                                    <input type="text" name="address" value="{{ old('address') }}" class="form-control">
                                </div>
                            </div>
                
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="vendor" class="forget-form">Vendor Group *</label>
                                    <select class="form-control vendor_group " name="group_id" >
                                        <option value="">Select Group</option>
                                        @foreach($vendor_group as $vgroup)
                                        <option  value="{{$vgroup->id}}"> {{ $vgroup->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                
                                <div class="form-group" >
                                    <label for="">Mobile *</label>
                                    <input type="text" name="phone_number" value="{{ old('phone_number') }}" class="form-control">
                                </div>
                
                                <div class="radio-wrapper">
                                    <div class="form-group">
                                        <label for="vendor" class="forget-form">Vendor Origin</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="local" name="origin" value="Local">
                                        <label class="form-check-label" for="local">Local</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="foreign" name="origin" value="Foreign">
                                        <label class="form-check-label" for="foreign">Foreign</label>
                                    </div>
                                </div>
                            </div><!-- end.col-md-6 -->
                        </div><!-- end.row -->
                        <hr>
                
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="image-upload-wrap">
                
                                        <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" id="logoImg" name="vendor_logo"  />
                
                                        <div class="drag-text">
                                            <h3>Drag and drop  Vendor Logo</h3>
                                        </div>
                                    </div><!-- end.image-upload -->
                
                                    <div class="file-upload-content">
                                        <img class="file-upload-image" src="#" alt="your image" />
                                            <button type="button" onclick="removeUpload()" class="remove-image">  <i class="fa fa-times"></i></button>
                                    </div><!-- end.file-upload-content -->
                                </div>
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                        <hr>
                
                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                @include('vendor::shares.vendor-types')
                                <input type="hidden" id="vendor" class="vendor">
                            </div>
                
                            <div class="col-md-3 col-lg-3">
                                @include('product::shares.product-types')
                                <input type="hidden" id="product">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <label for="product" class="forget-form"> Product Name *</label>
                                <select class="form-control select2 product-name " name="product_id" id="productId" >
                                    
                                    <option value="" disabled selected>Select Product</option>
                                </select>
                                <input type="hidden" id="productNameId" class="product">
                            </div>
                
                            <div class="col-md-3 col-lg-3 mt-4">
                                <div class="form-group">
                                    <label for=""></label>
                                    <a href="javascript:" class="btn create-btn add_item">Add</a>
                                </div>
                            </div>
                        </div><!-- end.row -->
                        <hr>
                
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-sm">
                                    <thead>
                                    <tr class="bg-white">
                                        <th>Vendor Type</th>
                                        <th>Product Type</th>
                                        <th>Product Name</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody class="item_add_area">
                
                                    </tbody>
                
                                </table><!-- end.table -->
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                        <hr>
                
                        <div class="row text-right">
                            <div class="col-sm-12">
                                <button type="submit" class="btn create-btn">Submit</button>
                            </div>
                        </div>
                    </form>

                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->

@endsection
@push('post_scripts')

    <script type="text/javascript">
        $('.select2').select2({});
    </script>

    <script>
        $(document).ready(function() {
            $('#type_id').on('change', function () {

                var vendorName = $(".type_id option:selected").text();

                var vendor = $('#vendor').val(vendorName);

            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#productId').on('change', function () {

                var product = $(".product-name option:selected").text();
                var productId = $('#productNameId').val(product);
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#product_type').on('change', function () {

                var productName = $(".product-type option:selected").text();

                var product = $('#product').val(productName);
            });
        });
    </script>
    <script type="text/javascript" lang="javascript">
        // Item Add function goes here


        $(document).on('click', '.add_item', function(){

            var type = $('.product-type').val();
            var productName = $("#product").val();
            // alert(productName);/
            var vendorType = $('.type_id').val();
            var vendorName = $('#vendor').val();

            var product =  $('.product-name').val();
            var prodName = $('#productNameId').val();

            // Check Vendor Type
            if(vendorType==null ){
                alert('Please select the vendor type ');
                return false;
            }
            
            // Check Product Type
            if(type==null ){
                alert('Please select the product type ');
                return false;
            }
            
            // Check Product
            if(prodName=="" ){
                alert('Please select the product ');
                return false;
            }


                $('.type_id').val(0).change();

            $('.item_add_area').append('<tr>'+
                '<td>'+vendorName+'<input type="hidden" class="form-control vendor_type" name="type_id[]" value="'+ vendorType +'" readonly></td>'+
                '<td>'+productName+'<input type="hidden" class="form-control product_type" name="product_type_id[]" value="'+ type +'" readonly></td>'+
                '<td>'+prodName+'<input type="hidden" class="form-control product_name" name="product_id[]" value="'+ product +'" readonly></td>'+
                '<td><a href="#" class="btn btn btn-danger item_remove btn-xs">x</a></td>'+
                '</tr>');
            $('#vendor').val(0).change();

               $('.product-type').val('').change();
               $("#product").val('').change();

                $('.product-name').val('').change();
                $('#productNameId').val('').change();

        });

        // Remove Item
        $(document).on('click', '.item_remove', function(){
            $(this).parent().parent().remove();

        });

        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                $('.image-upload-wrap').hide();

                $('.file-upload-image').attr('src', e.target.result);
                $('.file-upload-content').show();

                $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
            }

            function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                    $('.image-upload-wrap').addClass('image-dropping');
                });
                $('.image-upload-wrap').bind('dragleave', function () {
                    $('.image-upload-wrap').removeClass('image-dropping');
            });


            function generalInfo(){
                var val = $('.product-type').val()
               
               
                $.ajax({

                    type: 'GET',
                    url: '{{route("product.get.by.type.id")}}',
                    data: { type_id: val},
                    
                    success:function(data){
                        var result = '';
                        result+=`<option value="" disabled selected>Select Product</option>`
                        data.products.forEach(product => {
                          
                           result+=`<option value="${product.id}">${product.product_name}</option>`
                        });

                        $('.product-name').html(result)
                    }

                })
             
               
            }


           

    </script>
@endpush

