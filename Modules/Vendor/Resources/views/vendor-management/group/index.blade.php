@extends('dboard.index')

@section('title', 'View Vendor Group Interface')

@section('dboard_content')
<div class="row" id="actionModal">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Vendor Groups </h2>
          </div>

          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{ route('vendorgroup.create') }}">Add Vendor Group </a>
          </div>
          @endcan
        </div><!-- end.row -->
        <hr>
        <!-- data -->
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover" id="VendorGroupTable">
                <thead>
                  <tr>
                    <th>SL</th>
                    <th>Vendor Group</th>
                    <th>Status</th>
                    <th width="8%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if (isset($vendor_groups))
                  @foreach ($vendor_groups as $vendor_group)
                  <tr>
                    <td>{{ $vendor_group->id }}</td>
                    <td>{{ $vendor_group->name }}</td>
                    <td>
                      @if ($vendor_group->status == 1)
                      <span class="badge badge-success m-1 p-2">Active</span>
                      @else
                      <span class="badge badge-danger m-1 p-2">Inactive</span>
                      @endif
                    </td>

                    <td>
                      <div class="d-md-flex justify-content-around align-items-center">
                        @can('hasDeletePermission')
                        <form action="{{ route('vendorgroup.destroy', $vendor_group->id) }}" class="mr-3" method="POST" onclick="return confirm('Are you sure?')">
                          @csrf
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn" type="submit"><i class="fa fa-refresh"></i></button>
                        </form>
                        @endcan

                        @can('hasEditPermission')
                        {{-- <a id="edit-button" class="btn edit-btn"
                                                                    data-toggle="modal" data-target="#vendorgroup_add"><i
                                                                        class="fa fa-pencil" aria-hidden="true"
                                                                        data-toggle="tooltip" title="Edit Vendor" onclick="editGroup(this)" data-id="{{$vendor_group->id}}" data-name="{{$vendor_group->name}}"></i></a> --}}
                        <a class="btn edit-btn" href="{{ route('vendorgroup.edit', $vendor_group->id) }}"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit Vendor"></i></a>
                        @endcan
                      </div>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div><!-- end.table-responsive -->
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end.tite-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="vendorgroup_add" tabindex="-1" role="dialog" aria-labelledby="vendorgroup_addLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="vendorgroup_addLabel">Vendor Group Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>


      <form method="POST" action="{{ route('vendorgroup.update',1)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="hidden" name="id" id="id">
              <label class="control-label">Vendor Group *</label>
              <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="1" required>

            </div>
          </div>
        </div><!-- end.row -->
        <hr>

        <div class="row justify-content-center text-center align-items-center">
          <div class="col-md-6 text-center">
            <button class="btn create-btn" type="submit">Update</button>
          </div>
        </div><!-- end.row -->
      </form>



    </div>

  </div>
</div>

@endsection

@push('post_scripts')
<script>
  function editGroup(e) {
    var id = $(e).data('id');
  }
</script>

<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  $('#VendorGroupTable').DataTable();
</script>

@endpush