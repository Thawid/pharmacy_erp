@extends('dboard.index')

@section('title','Vendor Group Type Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Vendor Group </h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('vendorgroup.index')}}">Back</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- body -->
                    <form method="POST" action="{{ route('vendorgroup.update',$vendorGroup->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Vendor Group *</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" @if(isset($vendorGroup))  value="{{ $vendorGroup->name }} @endif" required>
                
                                </div>
                            </div>
                        </div><!-- end.row -->
                        <hr>
                
                        <div class="row justify-content-center text-center align-items-center">
                            <div class="col-md-6 text-center">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div>
                        </div><!-- end.row -->
                    </form>


                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->

    
@endsection






