@extends('dboard.index')

@section('title','Sell Request')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile mb-1">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-9 text-left">
                            <h2 class="titile-heading"> Sell Request Details <span class="text-primary"></span> </h2>
                        </div>
                        <div class="col-md-3 text-md-right">
                            <a class="btn index-btn" href="{{route('sellrequest.index')}}">Sell Request
                                List</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- header -->
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <p><strong>Purchase Order No: {{$sell_details->purchase_order_no}}</strong></p>
                            <p><strong>HUB Requisition No: {{$sell_details->hub_requisition_no}}</strong></p>
{{--                            <p><strong>Vendor Name: {{$order_details->vendor->name ?? ' '}}</strong></p>--}}
                        </div>
                        <div class="col-md-4 form-group">
                            <p><strong>Request Date: {{$sell_details->purchase_order_date}}</strong></p>
                            <p><strong>Request Delivery Date: {{$sell_details->requested_delivery_date}}</strong></p>
                            @if($sell_details->priority == 1)
                                <P><strong">Priority: High </strong></P>
                            @elseif($sell_details->priority == 2)
                                <P><strong>Priority: Low </strong></P>
                            @endif
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- body -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    <th>Unit</th>
                                    <th>UOM</th>
                                    <th>Order Quantity</th>
                                    <th>Delivery Quantity</th>
                                    {{--<th>Remain Quantity</th>--}}
                                    <th>Unit Price</th>
                                    <th>Discount</th>
                                    <th>Sub Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sell_details->purchase_order_details as $row)
                                    <tr>
                                        <td>{{$row->product->product_name ?? ' '}}</td>
                                        <td>{{$row->generic->name ?? ' '}}</td>
                                        <td>{{$row->unit->unit_name ?? ' '}}</td>
                                        <td>{{$row->uom->uom_name}}</td>
                                        <td>{{$row->order_quantity}}</td>
                                        <td>{{$row->delivered_quantity}}</td>
                                        {{--<td>{{$row->remain_quantity}}</td>--}}
                                        <td>{{$row->uom_price}}</td>
                                        <td>{{$row->discount}}</td>
                                        <td>{{$row->sub_total}}</td>
    
                                    </tr>
                                @endforeach
                                <tr>
    
                                    <td colspan="8" class="text-right">Sub Total</td>
                                    <td>{{$sell_details->sub_total}}</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-right">Overall Discount</td>
                                    <td>{{$sell_details->overall_discount}}</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-right">Total</td>
                                    <td>{{$sell_details->total}}</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-right">Other Expense</td>
                                    <td>{{$sell_details->other_expense}}</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-right">Vat & Tax</td>
                                    <td>{{$sell_details->vat_tax}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- end.col-md-12 -->
                    <div><!-- end.row -->

                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
@endsection
