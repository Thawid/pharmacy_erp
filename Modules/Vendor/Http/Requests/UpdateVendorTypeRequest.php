<?php

namespace Modules\Vendor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVendorTypeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|regex:/^[\pL\s\-]+$/u|min:5|max:20'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Vendor type field is required',
            'name.regex'=>'Vendor type must only contain letters',
            'name.min'=>'Vendor type must be at least 5 characters',
            'name.max'=>'Vendor type must be maximum 20 characters',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
