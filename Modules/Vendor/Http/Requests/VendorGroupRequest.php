<?php

namespace Modules\Vendor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VendorGroupRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:vendor_groups|regex:/^[\pL\s\-]+$/u|min:5|max:20'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Vendor group name must be required',
            'name.regex'=>'Vendor group must only contain letters',
            'name.min'=>'Vendor group must be at least 5 characters',
            'name.max'=>'Vendor group must be maximum 20 characters',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
