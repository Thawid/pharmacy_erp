<?php

namespace Modules\Vendor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVendorRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|regex:/^[\pL\s\-]+$/u|min:5|max:20',
            'type_id'=>'required|array',
            'group_id'=>'required|numeric',
            'product_type_id'=>'required|array',
            'phone_number'=>'required|regex:/(01)[0-9]{9}/|min:11|max:14',
            'email'=>'email|regex:/(.+)@(.+)\.(.+)/i',
            'vendor_logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'address'=>'string|min:5|max:255'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Vendor name field must be required',
            'name.regex'=>'Vendor name must only contain letters',
            'name.min'=>'Vendor name must be at least 5 characters',
            'name.max'=>'Vendor name must be maximum 20 characters',
            'group_id.required' => 'Please select vendor group',
            'product_type_id.required'=>'Please select product type',
            'product_type_id.array'=>'Product type must be an array',
            'vendor_logo.max'=>'Maximum logo size is 2 MB',
            'phone_number.required'=>'Phone number is required field',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
