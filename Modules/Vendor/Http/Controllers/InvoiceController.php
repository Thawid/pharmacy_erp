<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Vendor\Entities\VendorUser;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
       // return view('vendor::vendor-management.invoice.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('vendor::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Request $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('vendor::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    public function invoice()
    {

        //$invoice = PurchaseOrder::with(['purchase_order_details','vendor', 'purchase_order_details.product', 'purchase_order_details.unit','purchase_order_details.uom'])->first();

        $vendor = VendorUser::where('user_id',auth()->user()->id)->first();
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $sell_request_list = PurchaseOrder::with(['hq_requisition', 'vendor', 'purchase_order_details'])
                ->orderBy('id', 'DESC')
                ->get();

        } else {
            $sell_request_list = PurchaseOrder::with(['hq_requisition', 'vendor', 'purchase_order_details'])
                ->where('vendor_id',$vendor->vendor_id)
                ->orderBy('id', 'DESC')
                ->get();
        }

        return view('vendor::vendor-management.invoice.index', compact('sell_request_list'));
    }
}
