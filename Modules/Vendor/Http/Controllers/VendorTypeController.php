<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Vendor\Repositories\VendorTypeInterface;
use Modules\Vendor\Http\Requests\VendorTypeRequest;
use Modules\Vendor\Http\Requests\UpdateVendorTypeRequest;
use Modules\Vendor\Entities\VendorType;

class VendorTypeController extends Controller
{

    protected $vendor_type;
    public function __construct(VendorTypeInterface $vendor_type){
        $this->vendor_type = $vendor_type;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $vendor_types = $this->vendor_type->index();
        return view('vendor::vendor-management.type.indexnew',compact('vendor_types'));
    }


    /*
     * Get all vendor type using ajax request
     *
     * */


    public function get_all_vendor_type(){
        return $this->vendor_type->index();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');

        return view('vendor::vendor-management.type.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(VendorTypeRequest $request)
    {
        $this->vendor_type->store($request);
        return redirect()->route('vendortype.index')->with('success','Vendor type add successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        //Gate::authorize('hasReadPermission');
        return view('vendor::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        // $vendorType = $this->vendor_type->edit($id);

        // return response()->json()([
        //    'status'=>200,
        //    'vendorType'=>$vendorType
        // ]);
        // return view('vendor::vendor-management.type.edit',compact('vendorType'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateVendorTypeRequest $request, $id)
    {
        $this->vendor_type->update($request,$id);
        return redirect()->route('vendortype.index')->with('success','Update vendor type successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->vendor_type->destroy($id);
        return redirect()->route('vendortype.index')->with('success','Status update successfully');
    }

    public function vendor_type_edit($id)
    {
        Gate::authorize('hasEditPermission');
        // $vendorType = $this->vendor_type->edit($id);
        $vendorType = VendorType::where('id', $id)->first();
        return response()->json($vendorType);
    }


    public function vednorUpdate(Request $request)
    {
        $vendorType = VendorType::where('id', $request->id)->update(['name' => $request->name]);
        return redirect()->route('vendortype.index')->with('success','Update vendor type successfully'); 
    }
}
