<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Routing\Controller;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $total_vendors = DB::table('vendors')->where('status', 1)->count();
        $total_pending = DB::table('vendor_invoices')->where('status', '=', "Pending")->count();
        $recent_orders = DB::table('purchase_orders')->whereDate('created_at', Carbon::today())->where('purchase_order_no', '!=', null)->count();
        $total_purchase_orders = DB::table('purchase_orders')->where('purchase_order_no', '!=', null)->count();
        $total_purchase_amount = DB::table('purchase_orders')->where('purchase_order_no', '!=', null)->select(DB::raw('sum(total) as grand_total'))->get();
        return view('vendor::vendor-management.dashboard.index',
            compact('total_vendors', 'total_pending', 'recent_orders', 'total_purchase_orders', 'total_purchase_amount')
        );
    }
}
