<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Vendor\Entities\VendorUser;
use Modules\Vendor\Repositories\SellRequestInterface;

class SellRequestController extends Controller
{

    private $sellRequestRepositores;
    public function __construct(SellRequestInterface $sellRequestRepositores)
    {
        $this->sellRequestRepositores = $sellRequestRepositores;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $vendor = VendorUser::where('user_id',auth()->user()->id)->first();
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $sell_request_list = PurchaseOrder::with(['hq_requisition', 'vendor', 'purchase_order_details'])
                ->orderBy('id', 'DESC')
                ->get();

        } else {
            $sell_request_list = PurchaseOrder::with(['hq_requisition', 'vendor', 'purchase_order_details'])
                ->where('vendor_id',$vendor->vendor_id)
                ->orderBy('id', 'DESC')
                ->get();
        }
        return view('vendor::vendor-management.sell.index',compact('sell_request_list'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('vendor::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('vendor::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('vendor::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update( $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function sellRequestEdit(Request $request, $id)
    {
        $sell_request_edit = PurchaseOrder::with(['purchase_order_details','vendor', 'purchase_order_details.product', 'purchase_order_details.unit','purchase_order_details.uom'])->findOrFail($id);
        return view('vendor::vendor-management.sell.edit', compact('sell_request_edit'));
    }

    public function sellRequestDetails(Request $request, $id)
    {
        $sell_details = PurchaseOrder::with(['purchase_order_details','vendor', 'purchase_order_details.product', 'purchase_order_details.unit','purchase_order_details.uom'])->findOrFail($id);

        return view('vendor::vendor-management.sell.details', compact('sell_details'));
    }

    public function sell_request_update(Request $request, $id)
    {
       //return $request;
        $this->sellRequestRepositores->sell_request_update($request,$id);
        return redirect()->route('sellrequest.index')->with('success', 'Sell Request Update Success');
    }

    public  function  sell_request_status_remove(Request $request)
    {
        $purchase_order_details_id =$request->purchase_order_details_id;
        PurchaseOrderDetails::where('id', $purchase_order_details_id)->update([
            'status' => 'Approved'
        ]);

    }

    public  function  sellRemainRequest(Request $request)
    {



       $remain_request_list = DB::table('purchase_orders')
            ->join('purchase_order_details', 'purchase_orders.id', '=', 'purchase_order_details.purchase_order_id')
            ->groupBy('purchase_order_id')
            ->selectRaw('sum(purchase_order_details.remain_quantity) as remain_quantity, purchase_orders.id')
            ->havingRaw('remain_quantity > 0')
            ->select('purchase_orders.*', 'purchase_order_details.remain_quantity', 'purchase_order_details.order_quantity','purchase_order_details.delivered_quantity')
            ->get();

        return view('vendor::vendor-management.remain.remain',compact('remain_request_list'));
    }


    public  function sellRemainDetailRequest(Request $request, $id)
    {
        $sell_details = PurchaseOrder::with(['purchase_order_details','vendor', 'purchase_order_details.product', 'purchase_order_details.unit','purchase_order_details.uom'])->findOrFail($id);
        return view('vendor::vendor-management.remain.details', compact('sell_details'));
    }


    public  function sellRemainEditRequest(Request $request, $id)
    {

        $sell_request_edit = PurchaseOrder::with(['purchase_order_details','vendor', 'purchase_order_details.product', 'purchase_order_details.unit','purchase_order_details.uom'])->findOrFail($id);
        return view('vendor::vendor-management.remain.edit', compact('sell_request_edit'));
    }


}
