<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Vendor\Http\Requests\VendorRequest;
use Modules\Vendor\Http\Requests\UpdateVendorRequest;
use Modules\Vendor\Repositories\VendorInterface;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Accounts\Entities\VendorPayment;
use Modules\Product\Entities\Product;
use Modules\Vendor\Entities\VendorDetails;

use DB;

class VendorProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    protected $vendor;

    public function __construct(VendorInterface $vendor){
        $this->vendor = $vendor;
    }

    public function index()
    {

        return view('vendor::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('vendor::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('vendor::vendor-management.vendor-profile.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('vendor::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

      public function vendor_details($id){
        $vendor_purchase_order = DB::table('purchase_orders')->where('status',  'approved')->count();
        $total_purchase_amount = DB::table('purchase_orders')->select(DB::raw('sum(total) as total_amount'))->get();
        $total_pay_amount = DB::table('vendor_payments')->select(DB::raw('sum(pay_amount) as total_pay_amount'))->where('status',  'Approved')->get();
        $due_amount = $total_purchase_amount[0]->total_amount - $total_pay_amount[0]->total_pay_amount;
        $purchase_order = PurchaseOrder::where('id', $id)->get();
        $vendor = $this->vendor->vendor_details($id);
        $product_history = Product::where('id', $id)->get();
        $payment_history = VendorPayment::join('vendor_invoices','vendor_payments.vendor_invoice_id','=','vendor_invoices.id')
        ->where('vendor_invoices.vendor_id',$id)
        ->get();
        //dd($payment_history);
        return view('vendor::vendor-management.profile.index',compact('vendor', 'vendor_purchase_order', 'total_purchase_amount', 'total_pay_amount', 'due_amount', 'purchase_order','payment_history','product_history'));
    }

    public function vendor_purchase_orders($id)
    {
       
        $vendor = $this->vendor->vendor_details($id);
        $vendor_purchase_order = DB::table('purchase_orders')->where('status',  'approved')->count();
        $total_purchase_amount = DB::table('purchase_orders')->select(DB::raw('sum(total) as total_amount'))->get();
        $total_pay_amount = DB::table('vendor_payments')->select(DB::raw('sum(pay_amount) as total_pay_amount'))->where('status',  'Approved')->get();
        $due_amount = $total_purchase_amount[0]->total_amount - $total_pay_amount[0]->total_pay_amount;
        $purchase_order = PurchaseOrder::where('id', $id)->get();
        $product_history = Product::where('id', $id)->get();
        $payment_history = VendorPayment::where('id', $id)->get();
        return view('vendor::vendor-management.profile.index',compact('vendor', 'vendor_purchase_order', 'total_purchase_amount', 'total_pay_amount', 'due_amount', 'purchase_order', 'payment_history', 'product_history'));
    }

    public function vendor_purchase_product($id)
    {
        $vendor_details = VendorDetails::with(['vendors', 'vtype', 'ptype',  'product'])
                            ->where('vendor_id',$id)
                            ->orderBy('id', 'DESC')
                            ->get();

        $purchase_order = PurchaseOrder::where('id', $id)->get();
        $payment_history = VendorPayment::where('id', $id)->get();
        $vendor_purchase_order = DB::table('purchase_orders')->where('status',  'approved')->count();
        $total_purchase_amount = DB::table('purchase_orders')->select(DB::raw('sum(total) as total_amount'))->get();
        $total_pay_amount = DB::table('vendor_payments')->select(DB::raw('sum(pay_amount) as total_pay_amount'))->where('status',  'Approved')->get();
        $due_amount = $total_purchase_amount[0]->total_amount - $total_pay_amount[0]->total_pay_amount;
        // $vendor = $this->vendor->vendor_details($id);
        return view('vendor::vendor-management.profile.product',compact('vendor_details','vendor_purchase_order', 'total_purchase_amount', 'total_pay_amount', 'due_amount',  'purchase_order', 'payment_history'));
    }

    public function vendor_purchase_payment($id)
    {
       
        $payment_history = VendorPayment::where('id', $id)->get();
        $purchase_order = PurchaseOrder::where('id', $id)->get();
        $product_history = Product::where('id', $id)->get();
        $vendor_purchase_order = DB::table('purchase_orders')->where('status',  'approved')->count();
        $vendor = $this->vendor->vendor_details($id);
        $total_purchase_amount = DB::table('purchase_orders')->select(DB::raw('sum(total) as total_amount'))->get();
        $total_pay_amount = DB::table('vendor_payments')->select(DB::raw('sum(pay_amount) as total_pay_amount'))->where('status',  'Approved')->get();
        $due_amount = $total_purchase_amount[0]->total_amount - $total_pay_amount[0]->total_pay_amount;
        return view('vendor::vendor-management.profile.payment', compact('payment_history', 'vendor', 'due_amount', 'total_purchase_amount', 'total_pay_amount', 'vendor_purchase_order', 'purchase_order', 'product_history'));
    }

    


}
