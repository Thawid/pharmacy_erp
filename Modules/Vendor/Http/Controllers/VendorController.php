<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Vendor\Http\Requests\VendorRequest;
use Modules\Vendor\Http\Requests\UpdateVendorRequest;
use Modules\Vendor\Repositories\VendorInterface;
use Modules\Vendor\Entities\VendorType;
use Modules\Vendor\Entities\VendorGroup;
use Modules\Product\Entities\ProductType;
use Modules\Vendor\Entities\VendorDetails;
use DB;

class VendorController extends Controller
{

    protected $vendor;

    public function __construct(VendorInterface $vendor){
        $this->vendor = $vendor;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $vendor_type = VendorType::select('id','name')->where('status',1)->get();
        $vendor_group = VendorGroup::select('id','name')->where('status',1)->get();
        return view('vendor::vendor-management.vendor.index',compact('vendor_type','vendor_group'));
    }


    public function get_all_vendor(Request $request){
      return  $this->vendor->get_all_vendor($request);
    }



    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(Request $request)
    {
        Gate::authorize('hasCreatePermission');
        $vendor_type = VendorType::select('id','name')->where('status',1)->get();
        $vendor_group = VendorGroup::select('id','name')->where('status',1)->get();
        $product_type = ProductType::select('id','name')->where('status',1)->get();

        $product_name = Product::select('id','product_name')->get();

        return view('vendor::vendor-management.vendor.create',compact('vendor_type','vendor_group','product_type','product_name'));

        
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(VendorRequest $request)
    {
        $result = $this->vendor->store($request);
        if ($result) {
            return redirect()->route('vendors.index')->with('success', 'Vendor created successfully');
        } else {
            return redirect()->back()->with('error', 'Vendor dosent crate');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('vendor::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {

        $vendor = $this->vendor->edit($id);
        $vendor_type = VendorType::select('id','name')->where('status',1)->get();
        $vendor_group = VendorGroup::select('id','name')->where('status',1)->get();
        $product_type = ProductType::select('id','name')->where('status',1)->get();
        $product_name = Product::select('id','product_name')->get();
        return view('vendor::vendor-management.vendor.edit',compact('vendor','vendor_type','vendor_group','product_type','product_name'));

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateVendorRequest $request, $id)
    {
        $this->vendor->update($request, $id);
        return redirect()->back()->with('success','Vendor update successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    // public function vendor_details($id){
    //     $vendor_purchase_order = DB::table('purchase_orders')->where('status',  'approved')->count();
    //     $total_purchase_amount = DB::table('purchase_orders')->select(DB::raw('sum(total) as total_amount'))->get();
    //     $total_pay_amount = DB::table('vendor_payments')->select(DB::raw('sum(pay_amount) as total_pay_amount'))->where('status',  'Approved')->get();
    //     $due_amount = $total_purchase_amount[0]->total_amount - $total_pay_amount[0]->total_pay_amount;
    //  return   $vendor = $this->vendor->vendor_details($id);
    //     return view('vendor::vendor-management.profile.index',compact('vendor', 'vendor_purchase_order', 'total_purchase_amount', 'total_pay_amount', 'due_amount'));
    // }

    public function getProductByTypeId(Request $request)
    {

        $product_name = Product::select('id','product_name')->where('product_type_id',$request->type_id)->get();
        return response()->json([
            'products' => $product_name
        ]);
    }
}
