<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Vendor\Repositories\VendorUserInterface;

class VendorUserController extends Controller
{

    private $vendor_user;
    public function __construct(VendorUserInterface $vendor_user){
        $this->vendor_user = $vendor_user;
    }
    
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
       $all_vendor_user=  $this->vendor_user->index();
       return view('vendor::vendor-management.vendor-user.index',compact('all_vendor_user'));
       
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $users_and_vendors = $this->vendor_user->create();
        return view('vendor::vendor-management.vendor-user.create',compact('users_and_vendors'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      $this->vendor_user->store($request);
      return redirect()->route('vendor-user.index')->with('success','Vendor User Create Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('vendor::vendor-management.vendor-user.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $users_and_vendors = $this->vendor_user->edit($id);
        return view('vendor::vendor-management.vendor-user.edit',compact('users_and_vendors'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
       $this->vendor_user->update($request,$id);
       return redirect()->route('vendor-user.index')->with('success','Vendor User Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $this->vendor_user->destroy($id);
        return redirect()->back()->with('success','Vendor User Status Change Successfully');
    }
}
