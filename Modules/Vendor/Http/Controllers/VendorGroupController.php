<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Vendor\Repositories\VendorGroupInterface;
use Modules\Vendor\Http\Requests\VendorGroupRequest;
use Modules\Vendor\Http\Requests\UpdateVendorGroupRequest;
use Modules\Vendor\Entities\VendorGroup;

class VendorGroupController extends Controller
{
   protected $vendor_group;
    public function __construct(VendorGroupInterface $vendor_group){
        $this->vendor_group = $vendor_group;
    }
    public function index()
    {
        $vendor_groups = $this->vendor_group->index();
        return view('vendor::vendor-management.group.indexnew',compact('vendor_groups'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');
        return view('vendor::vendor-management.group.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(VendorGroupRequest $request)
    {
        $this->vendor_group->store($request);
        return redirect()->route('vendorgroup.index')->with('success','Vendor group add successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        //Gate::authorize('hasReadPermission');
        return view('vendor::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $vendorGroup = $this->vendor_group->edit($id);
        return view('vendor::vendor-management.group.edit',compact('vendorGroup'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateVendorGroupRequest $request, $id)
    {
        $this->vendor_group->update($request,$id);
        return redirect()->route('vendorgroup.index')->with('success','Update vendor group successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->vendor_group->destroy($id);
        return redirect()->route('vendorgroup.index')->with('success','Status update successfully');
    }

    public function vendor_group_edit($id)
    {
        Gate::authorize('hasEditPermission');
        // $vendorType = $this->vendor_type->edit($id);
        $vendorGroup = VendorGroup::where('id', $id)->first();
        return response()->json($vendorGroup);
    }

    public function vendorUpdate(Request $request)
    {
        $vendorGroup = VendorGroup::where('id', $request->id)->update(['name' => $request->name]);
        return redirect()->route('vendorgroup.index')->with('success','Update vendor group successfully'); 
    }


}
