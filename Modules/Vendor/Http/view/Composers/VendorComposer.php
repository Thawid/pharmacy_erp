<?php

namespace Modules\Vendor\Http\View\Composers;

use Modules\Product\Entities\ProductType;

use Illuminate\View\View;

class VendorComposer
{


    public function compose(View $view)
    {
        $view->with('product_types', ProductType::get(['id','name']));

    }

}
