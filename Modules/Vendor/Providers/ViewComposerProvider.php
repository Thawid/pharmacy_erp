<?php

namespace Modules\Vendor\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Vendor\Entities\VendorType;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    public function boot()
    {
        view()->composer('shares.vendor-type', function ($view) {
            $view->with('vendor_type', VendorType::select('id','name')->where('status','=',1)->get());
        });
    }
}
