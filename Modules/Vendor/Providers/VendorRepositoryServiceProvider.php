<?php

namespace Modules\Vendor\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Vendor\Repositories\SellRequestInterface;
use Modules\Vendor\Repositories\SellRequestReository;
use Modules\Vendor\Repositories\VendorTypeRepository;
use Modules\Vendor\Repositories\VendorGroupRepository;
use Modules\Vendor\Repositories\VendorRepository;
use Modules\Vendor\Repositories\VendorTypeInterface;
use Modules\Vendor\Repositories\VendorGroupInterface;
use Modules\Vendor\Repositories\VendorInterface;
use Modules\Vendor\Repositories\VendorUserRepository;
use Modules\Vendor\Repositories\VendorUserInterface;


class VendorRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(VendorUserInterface::class, VendorUserRepository::class);
        $this->app->bind(VendorTypeInterface::class, VendorTypeRepository::class);
        $this->app->bind(VendorGroupInterface::class, VendorGroupRepository::class);
        $this->app->bind(VendorInterface::class, VendorRepository::class);
        $this->app->bind(SellRequestInterface::class, SellRequestReository::class);
        


    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
