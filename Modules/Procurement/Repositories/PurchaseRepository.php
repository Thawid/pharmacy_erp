<?php

namespace Modules\Procurement\Repositories;

use DB;
use Modules\Procurement\Entities\HqRequisitionDetails;
use Modules\Procurement\Entities\HqRequisition;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Procurement\Http\Requests\PurchaseOrderRequest;


class PurchaseRepository implements PurchaseInterface
{

    public function order_pending_list()
    {
        return $hq_requisitions =HqRequisition::with(['vendor'])->groupBy('vendor_id')->where('status',0)->latest()->get();
    }

    public function purchase_order_store(PurchaseOrderRequest $request)
    {

        //dd($request->all());

        DB::beginTransaction();
        try {
            $random_number = rand(1111, 999) . time();
            $purchase_order = new PurchaseOrder();
            $purchase_order->hq_approve_requisition_id  = $request->hq_approve_requisition_id;
            $purchase_order->purchase_order_no          = $random_number;
            $purchase_order->purchase_order_date        = $request->purchase_order_date;
            $purchase_order->requested_delivery_date    = $request->delivery_date;
            $purchase_order->sub_total                  = $request->sub_total;
            $purchase_order->overall_discount           = $request->overall_discount;
            $purchase_order->total                      = $request->total_price;
            $purchase_order->other_expense              = $request->other_expense;
            $purchase_order->vat_tax                    = $request->vat_tax;
            $purchase_order->priority                   = $request->priority;
            $purchase_order->vendor_id                  = $request->vendor_id;
            $purchase_order->hub_id                     = $request->hub_id;
            $purchase_order->store_type                 = $request->store_type;
            $purchase_order->hub_requisition_id         = $request->hub_requisition_id;
            $purchase_order->hub_requisition_no         = $request->hub_requiquisition_no;
            $purchase_order->save();

            $product_id     = $request->product_id;
            $vendor_id      = $request->vendor_id;
            $unit_id        = $request->unit_id;
            $generic_id        = $request->generic_id;
            $quantity       = $request->qty;
            $uom_id         = $request->uom_id;
            $unit_price     = $request->uom_price;
            $discount       = $request->discount;
            $product_sub_total = $request->product_sub_total;

            $purchase_order_details=[];
            for ($i = 0; $i < count($product_id); $i++) {

                $purchase_order_details[] =[
                    'purchase_order_id'     =>$purchase_order->id,
                    'product_id'            =>$product_id[$i],
                    'generic_id'            =>$generic_id[$i],
                    'unit_id'               =>$unit_id[$i],
                    'uom_id'                =>$uom_id[$i],
                    'order_quantity'        =>$quantity[$i],
                    'uom_price'             =>$unit_price[$i],
                    'discount'              =>$discount[$i],
                    'sub_total'             =>$product_sub_total[$i],
                ];
            }

            PurchaseOrderDetails::insert($purchase_order_details);
            DB::commit();
            HqRequisitionDetails::where('vendor_id',$request->vendor_id)
                ->where('hq_approve_requisition_id',$request->hq_approve_requisition_id)
                ->where('status',0)
                ->update(['status' => 1]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function purchase_order_create($id,$req_id)
    {
        /*$hqrequisitons_head = HqRequisition::with('vendor')->where('vendor_id',$id)->where('status',0)->get();
        $hqrequisitons_id =$hqrequisitons_head->map(function($item){
            return $item->id;
        });
        return HqRequisitionDetails::with(['generic', 'product_variation', 'unit'])->whereIn('hq_requisition_id', $hqrequisitons_id)->where('status','=', 0)->get();*/

        $hq_req_details = DB::table('hq_approve_requisitions')
            ->join('hq_approve_requisition_details','hq_approve_requisitions.id','=','hq_approve_requisition_details.hq_approve_requisition_id')
            ->join('products','hq_approve_requisition_details.product_id','products.id')
            ->join('product_generics','hq_approve_requisition_details.generic_id','=','product_generics.id')
            ->join('product_unit','hq_approve_requisition_details.unit_id','=','product_unit.id')
            ->join('uoms','hq_approve_requisition_details.uom_id','=','uoms.id')
            ->join('vendors','hq_approve_requisition_details.vendor_id','=','vendors.id')
            ->select('hq_approve_requisitions.hub_id','hq_approve_requisitions.store_type','hq_approve_requisitions.hub_requisition_id','hq_approve_requisitions.hub_requiquisition_no','products.id as product_id','products.product_name','products.purchase_price','product_generics.id as generic_id','product_generics.name as generic_name','product_unit.id as unit_id','product_unit.unit_name','hq_approve_requisition_details.purchase_quantity','hq_approve_requisition_details.hq_approve_requisition_id','uoms.id as uom_id','uoms.uom_name','vendors.id as vendor_id','vendors.name as vendor_name')
            ->where('hq_approve_requisition_details.vendor_id',$id)
            ->where('hq_approve_requisition_details.hq_approve_requisition_id',$req_id)
            ->where('hq_approve_requisition_details.status','=',0)
            ->get();

        return $hq_req_details;
    }

    public function purchase_order_update(PurchaseOrderRequest $request,$id)
    {

        DB::beginTransaction();
        try {
            $purchase_order = PurchaseOrder::findOrFail($id);
            $purchase_order->hq_approve_requisition_id  = $request->hq_id;
            $purchase_order->purchase_order_date        = $request->purchase_order_date;
            $purchase_order->requested_delivery_date    = $request->delivery_date;
            $purchase_order->sub_total                  = $request->sub_total;
            $purchase_order->overall_discount           = $request->overall_discount;
            $purchase_order->total                      = $request->total_price;
            $purchase_order->other_expense              = $request->other_expense;
            $purchase_order->vat_tax                    = $request->vat_tax;
            $purchase_order->priority                   = $request->priority;
            $purchase_order->vendor_id                  = $request->vendor_id;
            $purchase_order->save();

            $product_id     = $request->product_id;
            $vendor_id      = $request->vendor_id;
            $unit_id        = $request->unit_id;
            $generic_id        = $request->generic_id;
            $quantity       = $request->qty;
            $uom_id         = $request->uom_id;
            $unit_price     = $request->uom_price;
            $discount       = $request->discount;

            $product_sub_total = $request->product_sub_total;
            for ($i = 0; $i < count($product_id); $i++) {
                $purchase_order_details = $purchase_order->purchase_order_details->where('product_id',$product_id[$i])->first();
                $purchase_order_details->purchase_order_id          = $purchase_order->id;
                $purchase_order_details->product_id                 = $product_id[$i];
                // $purchase_order_details->vendor_id               = $vendor_id;
                $purchase_order_details->generic_id                 = $generic_id[$i];
                $purchase_order_details->unit_id                    = $unit_id[$i];
                $purchase_order_details->uom_id                     = $uom_id[$i];
                $purchase_order_details->order_quantity             = $quantity[$i];
                $purchase_order_details->uom_price                  = $unit_price[$i];
                $purchase_order_details->discount                   = $discount[$i];
                $purchase_order_details->sub_total                  = $product_sub_total[$i];
                $purchase_order_details->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function show($id)
    {
        $hqrequisitons_head = HqRequisition::with('vendor')->where('vendor_id',$id)->where('status',0)->get();
        $hqrequisitons_id =$hqrequisitons_head->map(function($item){
            return $item->id;
        });
        return HqRequisitionDetails::with(['generic', 'product', 'unit'])->whereIn('hq_requisition_id', $hqrequisitons_id)->where('status', 0)->get();

    }
}
