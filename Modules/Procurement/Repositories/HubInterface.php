<?php


namespace Modules\Procurement\Repositories;
use Modules\Procurement\Http\Requests\UpdateHubRequest;


use Illuminate\Http\Request;

interface HubInterface
{
    public function index();

    public function hub_index();

    public function requisition_pending_list();

    public function requisition_process_list();

    public function requisition_process_confirm_view();
    
    public function requisition_list();

    public function create();

    public function store(Request $request);

    public function requisition_process_confirm_store(Request $request);

    public function pending_requisition_merge(Request $request);

    public function show($id);

    public function requisition_pending_details($id);

    public function requisition_process_details($id);

    public function requisition_details($id);

    public function edit($id);

    public function requisition_pending_modify($id);

    public function requisition_process_add($id);
    
    public function requisition_process_addAll(Request $request);

    public function update(UpdateHubRequest $request,$id);

    public function destroy(Request $request);

}
