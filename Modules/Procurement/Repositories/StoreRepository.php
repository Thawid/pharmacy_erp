<?php


namespace Modules\Procurement\Repositories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Procurement\Entities\StoreRequisitionDetails;
use Modules\Procurement\Http\Requests\StoreRequisitionFormRequest;
use Modules\Procurement\Http\Requests\UpdateStoreRequisitionFormRequest;
use Modules\Procurement\Repositories\StoreInferface;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use Illuminate\Support\Facades\Gate;

class StoreRepository implements StoreInferface
{
    public function index()
    {

        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer'){
            $requisition = StoreRequisition::latest()->get();

        }else{
            $store = StoreUser::where('user_id', auth()->user()->id)
                ->where('store_type','store')
                ->where('status', 1)
                ->first();
            $requisition = StoreRequisition::latest()->where('store_id',$store->store_id)->get();
        }
        return $requisition;
    }

    public function create()
    {
    }

    public function store(StoreRequisitionFormRequest $request)

    {
        //dd($request->all());
        DB::beginTransaction();
        try {
            // $request->validated();
            $product_id = $request->product;
            $generic_id = $request->generic_id;
            $vendor_id = $request->vendor;
            $unit_id = $request->unit_id;
            $quantity = $request->qty;
            $uom_id = $request->uom_id;
            $purchase_price = $request->purchase_price;

            $store = StoreUser::where('user_id', auth()->user()->id)->where('status', 1)->first();
            $get_store_type = Store::select('store_type','warehouse_id')->where('id',$store->store_id)->firstOrFail();
            if ($store) {
                $requisitionNo = rand(1111, 9999). time();
                $storeRequisition = new StoreRequisition();
                $storeRequisition->store_requisition_no    = $requisitionNo;
                $storeRequisition->store_id                = $store->store_id;
                $storeRequisition->requisition_date        = $request->requisition_date;
                $storeRequisition->delivery_date           = $request->delivery_date;
                $storeRequisition->priority                = $request->priority;
                $storeRequisition->status                  = 'Pending';
                $storeRequisition->store_type              = $get_store_type->store_type;
                $storeRequisition->warehouse_id            = $get_store_type->warehouse_id;
                $storeRequisition->save();

                $store_requisition_id = $storeRequisition->id;
                $count = count($product_id);
                for ($i = 0; $i < $count; $i++) {
                    $details[] = [
                        'store_requisition_id' => $store_requisition_id,
                        'product_id' => $product_id[$i],
                        'generic_id' => $generic_id[$i],
                        'vendor_id' => $vendor_id[$i],
                        'unit_id' => $unit_id[$i],
                        'uom_id' => $uom_id[$i],
                        'request_quantity' => $quantity[$i],
                        'purchase_price'=>$purchase_price[$i],
                        'status'=>'Pending',
                        'created_at' => Carbon::now(),
                    ];
                }

                StoreRequisitionDetails::insert($details);
                DB::commit();
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors("Requisition dosen't created" . $e->getMessage());
        }
    }

    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $requisition_details = StoreRequisition::with(['details','details.product_uom','details.product','details.unit','details.vendor','details.generic','store'])->where('id', $id)->first();
        return $requisition_details;
    }

    public function update(UpdateStoreRequisitionFormRequest $request, $requisition_no)
    {
        //dd($request->all());
        Gate::authorize('hasEditPermission');
        $quantity = $request->qty;
        $requisition_no = $request->store_requisition_id;
        $req_details_id = $request->req_details_id;

        $requisition_date = $request->requisition_date;
        $delivery_date = $request->delivery_date;
        $priority = $request->priority;

        $new_data = $request->new;

        DB::beginTransaction();
        try {
            $store_requisition = StoreRequisition::where('id', $requisition_no)->first();
            $store_requisition->requisition_date = $requisition_date;
            $store_requisition->delivery_date = $delivery_date;
            $store_requisition->priority = $priority;
            $store_requisition->save();

            try {

                $req_old_id = [];
                $old_requisition = DB::table('store_requisition_details')
                    ->where('store_requisition_id', $requisition_no)
                    ->get();

                foreach ($old_requisition as $old_req){
                    $req_old_id[] = $old_req->id;
                }
                $deleted_product = array_diff($req_old_id, $req_details_id);

                if(count($deleted_product) != 0){
                    foreach ($deleted_product as $deleted_id){
                        DB::table('store_requisition_details')->where('id', $deleted_id)->delete();
                    }
                }

                $updated_product = array_intersect($req_old_id, $req_details_id);
                $count = 0;
                foreach ($updated_product as $update_id){
                    DB::table('store_requisition_details')
                        ->where('id', $update_id)
                        ->update([
                            'request_quantity'=>$quantity[$count],
                            'updated_at' => Carbon::now()
                        ]);
                    $count++;
                }

                try {
                    $new_product = isset($request->new['product']) && !empty($request->new['product']) ? count($request->new['product']) : 0;
                    if($new_product != 0):
                        $newProduct = [];
                        for($i = 0; $i < $new_product; $i++){
                            $single_product = array(
                                'store_requisition_id' => $new_data['store_requisition_id'][$i],
                                'product_id' => $new_data['product'][$i],
                                'generic_id' => $new_data['generic_id'][$i],
                                'vendor_id' => $new_data['vendor'][$i],
                                'unit_id' => $new_data['unit_id'][$i],
                                'request_quantity' => $new_data['qty'][$i],
                                'uom_id' => $new_data['uom_id'][$i],
                                'status'=>$new_data['status'][$i],
                                'updated_at'=>Carbon::now(),
                            );
                            $newProduct[] = $single_product;

                        }

                        StoreRequisitionDetails::insert($newProduct);
                    endif;
                    DB::commit();
                    return true;
                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors("Requisition dosen't updated" . $e->getMessage());
                }
            } catch (\Exception $e) {
                return redirect()->back()->withErrors("Requisition dosen't updated" . $e->getMessage());
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors("Requisition dosen't updated" . $e->getMessage());
        }
    }

    public function destroy(Request $request)
    {
        Gate::authorize('hasDeletePermission');
        if ($request->id) {
            $store_requisition = StoreRequisition::where('id', $request->id)->first();
            $store_requisition->delete();
            DB::table('store_requisition_details')->where('store_requisition_id', $request->id)->delete();
            return true;
        } else {
            return  false;
        }
    }

    public function show($id)
    {
        $requisition_details = StoreRequisition::with(['details','details.product_uom','details.product','details.unit','details.vendor','details.generic','store'])->where('id', $id)->first();
        return $requisition_details;
    }

    public function getProductByID($id)
    {
       return Product::with(['pgeneric','productUnit','product_uom','vendors','vendors.vendorName'])
           ->findOrFail($id);
    }
}
