<?php


namespace Modules\Procurement\Repositories;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\StoreUser;
use Modules\Inventory\Entities\PurchaseDistributionRequests;
use Modules\Inventory\Entities\PurchaseDistributionRequestDetails;
use Modules\Procurement\Entities\HubApproveRequisition;
use Modules\Procurement\Entities\HubApproveRequisitionDetails;
use Modules\Procurement\Entities\HubRequisition;
use Modules\Procurement\Entities\HubRequisitionDetails;
use Modules\Procurement\Repositories\HubInterface;
use Modules\Procurement\Http\Requests\UpdateHubRequest;
use Modules\Product\Entities\ProductVariation;

class HubRepository implements HubInterface
{
    public function index()
    {
       return "Hub Index";
    }

    public function hub_index()
    {
        return "Hub Index 2";

    }

    public function requisition_pending_list()
    {

        $store = StoreUser::where('user_id', auth()->user()->id)
            ->where('store_type','hub')
            ->where('status', 1)
            ->first();

        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer'){
            $pending_requisition_list = DB::table('purchase_distribution_requests')
                ->join('stores','purchase_distribution_requests.store_id','=','stores.id')
                ->select('purchase_distribution_requests.id','purchase_distribution_requests.store_requisition_no','purchase_distribution_requests.requisition_date','purchase_distribution_requests.delivery_date','stores.name as store_name','purchase_distribution_requests.priority','purchase_distribution_requests.status')
                ->orderBy('purchase_distribution_requests.id','DESC')
                ->get();

        }else{
            $pending_requisition_list = DB::table('purchase_distribution_requests')
           ->join('stores','purchase_distribution_requests.store_id','=','stores.id')
           ->select('purchase_distribution_requests.id','purchase_distribution_requests.store_requisition_no','purchase_distribution_requests.requisition_date','purchase_distribution_requests.delivery_date','stores.name as store_name','purchase_distribution_requests.priority','purchase_distribution_requests.status')
           ->where('stores.hub_id',$store->store_id)
           ->orderBy('purchase_distribution_requests.id','DESC')
           ->get();
        }
       return $pending_requisition_list;
    }

    public function requisition_process_list()
    {

        $process_list['total_new_requisition']=HubApproveRequisitionDetails::where('status','Add')->count();

        $process_list['process'] = DB::table('hub_approve_requisition_details')
        ->join('hub_approve_requisitions','hub_approve_requisition_details.hub_approve_requisition_id','=','hub_approve_requisitions.id')
         ->join('products','hub_approve_requisition_details.product_id','=','products.id')
         ->join('stores','hub_approve_requisitions.store_id','=','stores.id')
         ->select('hub_approve_requisition_details.id','products.id as product_id','products.product_name','stores.name as store_name')
         ->where('hub_approve_requisition_details.status','=','Pending')
        ->get();

        return $process_list;

    }

    public function requisition_process_confirm_view()
    {
        $new_requisition['hub_approve_requisition_details_id']=DB::table('hub_approve_requisition_details')
        ->select('hub_approve_requisition_details.id')
        ->where('hub_approve_requisition_details.status','=','Add')
        ->get();


         $new_requisition['requisition_data'] = DB::table('hub_approve_requisition_details')
        ->join('hub_approve_requisitions','hub_approve_requisition_details.hub_approve_requisition_id','=','hub_approve_requisitions.id')
        ->join('stores','hub_approve_requisitions.store_id','=','stores.id')

        ->join('products','hub_approve_requisition_details.product_id','=','products.id')
        ->join('product_generics','hub_approve_requisition_details.generic_id','=','product_generics.id')
        ->join('product_unit','hub_approve_requisition_details.unit_id','=','product_unit.id')
        ->join('vendors','hub_approve_requisition_details.vendor_id','=','vendors.id')
        ->join('uoms','hub_approve_requisition_details.uom_id','=','uoms.id')



        ->select('stores.hub_id','stores.store_type','products.id as product_id','products.product_name','product_generics.id as generic_id','product_generics.name as generic_name','product_unit.id as unit_id','product_unit.unit_name','vendors.id as vendor_id','vendors.name as vendor_name','uoms.id as uom_id','uoms.uom_name',DB::raw('sum(hub_approve_requisition_details.purchase_quantity) as purchase_quantity'))
        ->where('hub_approve_requisition_details.status','=','Add')
        ->groupBy('hub_approve_requisition_details.product_id')
        ->groupBy('hub_approve_requisition_details.vendor_id')
        ->get();

        return $new_requisition;
    }

    public function requisition_list()
    {
       $requisition_list = HubRequisition::orderBy('id','DESC')->get();
       return $requisition_list;
    }


    public function create()
    {
        $products = Product::select(['id', 'product_name'])->get();
        return $products;

    }

    public function store(Request $request)
    {



        $login_user_id = Auth::user()->id;
        $storeUser = StoreUser::firstWhere('user_id',$login_user_id);
        $login_user_hub_id= $storeUser->store_id;


        $hub_requisition_no = random_int(100000, 999999);
        $hub_id = $login_user_hub_id;
        $requisition_date = $request->requisition_date;
        $delivery_date = $request->delivery_date;
        $priority = $request->priority;
        $status = "Pending";
        $store_type = "hub";

        $product_id = $request->product;
        $generic_id = $request->generic_id;
        $unit_id = $request->unit_id;
        $vendor_id = $request->vendor;
        $request_quantity = $request->qty;
        $uom_id = $request->uom_id;


        DB::beginTransaction();

        try{

        $hubRequisition= new HubRequisition();
        $hubRequisition->hub_requisition_no = $hub_requisition_no;
        $hubRequisition->hub_id =$hub_id;
        $hubRequisition->store_type = $store_type;
        $hubRequisition->requisition_date = $requisition_date;
        $hubRequisition->delivery_date = $delivery_date;
        $hubRequisition->priority = $priority;
        $hubRequisition->status = $status;
        $hubRequisition->save();

            try{

                foreach($product_id as $key => $value){
                    $data['hub_requisition_id']=$hubRequisition->id;
                    $data['product_id']=$value;
                    $data['generic_id']=$generic_id[$key];
                    $data['unit_id']=$unit_id[$key];
                    $data['vendor_id']=$vendor_id[$key];
                    $data['request_quantity']=$request_quantity[$key];
                    $data['uom_id']=$uom_id[$key];
                    $data['status'] = $status;

                    HubRequisitionDetails::create($data);

                }

                DB::commit();
                return true;
            }catch(\Exception $e){
                DB::rollBack();
                return redirect()->back()->withErrors("Requisition Details dosen't Create" . $e->getMessage());
            }
        }
        catch(\Exception $e)
        {
            return redirect()->back()->withErrors("Requisition dosen't Crate - Hub Req Create Problem " . $e->getMessage());
        }








    }

    public function requisition_process_confirm_store(Request $request)
    {
       //return $request->all();

        $hub_requisition_no = random_int(100000, 999999);
        $hub_id = $request->hub_id;
        $store_type = $request->store_type;
        $requisition_date = $request->requisition_date;
        $delivery_date = $request->delivery_date;
        $priority = $request->priority;

        $product_id = $request->product_id;
        $generic_id = $request->generic_id;
        $unit_id = $request->unit_id;
        $vendor_id = $request->vendor_id;
        $purchase_quantity = $request->purchase_quantity;
        $uom_id = $request->uom_id;

        $hub_approve_requisition_details_id = $request->hub_approve_requisition_details_id;

        $hubRequisition= new HubRequisition();
        $hubRequisition->hub_requisition_no = $hub_requisition_no;
        $hubRequisition->hub_id =$hub_id;
        $hubRequisition->store_type = $store_type;
        $hubRequisition->requisition_date = $requisition_date;
        $hubRequisition->delivery_date = $delivery_date;
        $hubRequisition->priority = $priority;
        $hubRequisition->status = 'Pending';
        $hubRequisition->save();

        foreach($product_id as $key => $value){
            $data['hub_requisition_id']=$hubRequisition->id;
            $data['product_id']=$value;
            $data['generic_id']=$generic_id[$key];
            $data['unit_id']=$unit_id[$key];
            $data['vendor_id']=$vendor_id[$key];
            $data['request_quantity']=$purchase_quantity[$key];
            $data['uom_id']=$uom_id[$key];
            $data['status'] = "Pending";

            HubRequisitionDetails::create($data);
        }

       DB::table('hub_approve_requisition_details')
       ->whereIn('id',$hub_approve_requisition_details_id)
       ->update(['status' => 'Confirm']);


    }


    public function pending_requisition_merge(Request $request)
    {
       $hub_approve_requisition= new HubApproveRequisition();

       $hub_approve_requisition->store_requisition_id = $request->store_requisition_id;
       $hub_approve_requisition->store_requisition_no = $request->store_requisition_no;
       $hub_approve_requisition->store_id = $request->store_id;
       $hub_approve_requisition->store_type = $request->store_type;
       $hub_approve_requisition->requisition_date = $request->requisition_date;
       $hub_approve_requisition->delivery_date = $request->delivery_date;
       $hub_approve_requisition->priority = $request->priority;
       $hub_approve_requisition->warehouse_id = $request->warehouse_id;
       $hub_approve_requisition->purchase_distribution_request_id=$request->purchase_distribution_requests_id;
       $hub_approve_requisition->status = 'hub_approved';
       $hub_approve_requisition->save();

       $hub_approve_requisition_id = $hub_approve_requisition->id;
       $product_id=$request->product_id;
       $generic_id=$request->generic_id;
       $vendor_id=$request->vendor_id;
       $unit_id=$request->unit_id;
       $purchase_quantity=$request->purchase_quantity;
       $uom_id=$request->uom_id;
       $status ='Pending';

        $purchaseDistributionRequests=PurchaseDistributionRequests::find($request->purchase_distribution_requests_id);
       $purchaseDistributionRequests->status = "Approved";
       $purchaseDistributionRequests->save();

       foreach($product_id as $key => $value){
           $data['hub_approve_requisition_id']=$hub_approve_requisition_id;
           $data['product_id']=$value;
           $data['generic_id']=$generic_id[$key];
           $data['vendor_id']=$vendor_id[$key];
           $data['unit_id']=$unit_id[$key];
           $data['purchase_quantity']=$purchase_quantity[$key];
           $data['uom_id']=$uom_id[$key];
           $data['status']=$status;
           HubApproveRequisitionDetails::create($data);
       }




    }


    public function show($id)
    {
        return "Hub Show";
    }


    public function requisition_pending_details($id)
    {
        $pending_requisition_details['single_data']=DB::table('purchase_distribution_requests')
        ->join('stores','purchase_distribution_requests.store_id','=','stores.id')
        ->select('stores.name as store_name','stores.code as store_no','stores.store_type','purchase_distribution_requests.requisition_date','purchase_distribution_requests.delivery_date','purchase_distribution_requests.priority','purchase_distribution_requests.status')
        ->where('purchase_distribution_requests.id','=',$id)
        ->first();

       $pending_requisition_details['details'] = DB::table('purchase_distribution_requests')
        ->join('purchase_distribution_request_details','purchase_distribution_requests.id','=','purchase_distribution_request_details.purchase_distribution_request_id')
        ->join('products','purchase_distribution_request_details.product_id','=','products.id')
        ->join('product_generics','purchase_distribution_request_details.generic_id','=','product_generics.id')
        ->join('product_unit','purchase_distribution_request_details.unit_id','=','product_unit.id')
        ->join('uoms','purchase_distribution_request_details.uom_id','=','uoms.id')
        ->join('vendors','purchase_distribution_request_details.vendor_id','=','vendors.id')
        ->select('products.product_name','product_generics.name as generic_name','product_unit.unit_name','uoms.uom_name','vendors.name as vendor_name','purchase_distribution_request_details.purchase_quantity')

        ->where('purchase_distribution_requests.id','=',$id)
        ->get();


       return $pending_requisition_details;
    }

    public function requisition_process_details($id)
    {
       $details = DB::table('hub_approve_requisition_details')
       ->join('hub_approve_requisitions','hub_approve_requisition_details.hub_approve_requisition_id','=','hub_approve_requisitions.id')
        ->join('stores','hub_approve_requisitions.store_id','=','stores.id')

        ->join('products','hub_approve_requisition_details.product_id','=','products.id')
        ->join('product_generics','hub_approve_requisition_details.generic_id','=','product_generics.id')
        ->join('product_unit','hub_approve_requisition_details.unit_id','=','product_unit.id')
        ->join('vendors','hub_approve_requisition_details.vendor_id','=','vendors.id')
        ->join('uoms','hub_approve_requisition_details.uom_id','=','uoms.id')

        ->select('stores.name as store_name','stores.code as store_no','stores.store_type','hub_approve_requisitions.requisition_date','hub_approve_requisitions.delivery_date','hub_approve_requisitions.priority','hub_approve_requisition_details.status','products.product_name','product_generics.name as generic_name','product_unit.unit_name','uoms.uom_name','vendors.name as vendor_name','hub_approve_requisition_details.purchase_quantity')



       ->where('hub_approve_requisition_details.id',$id)->first();
       return $details;
    }


    public function requisition_details($id)
    {
        $requisition_details['single_data']=DB::table('hub_requisitions')

        ->where('hub_requisitions.id','=',$id)
        ->first();

         $requisition_details['details'] = DB::table('hub_requisitions')
        ->join('hub_requisition_details','hub_requisitions.id','=','hub_requisition_details.hub_requisition_id')
        ->join('products','hub_requisition_details.product_id','=','products.id')
        ->join('product_generics','hub_requisition_details.generic_id','=','product_generics.id')
        ->join('product_unit','hub_requisition_details.unit_id','=','product_unit.id')
        ->join('uoms','hub_requisition_details.uom_id','=','uoms.id')
        ->join('vendors','hub_requisition_details.vendor_id','=','vendors.id')
        ->select('products.product_name','product_generics.name as generic_name','product_unit.unit_name','uoms.uom_name','vendors.name as vendor_name','hub_requisition_details.request_quantity')

        ->where('hub_requisitions.id','=',$id)
        ->get();
        return $requisition_details;
    }

    public function edit($id)
    {
        return "Hub Edit";

    }

    public function requisition_pending_modify($id)
    {
        $pending_requisition_process['single_data']=DB::table('purchase_distribution_requests')
        ->join('stores','purchase_distribution_requests.store_id','=','stores.id')
        ->select('store_requisition_id','store_requisition_no','store_id','stores.name as store_name','stores.code as store_no','stores.store_type','purchase_distribution_requests.requisition_date','purchase_distribution_requests.delivery_date','purchase_distribution_requests.priority','purchase_distribution_requests.warehouse_id','purchase_distribution_requests.id as purchase_distribution_requests_id','purchase_distribution_requests.status')
        ->where('purchase_distribution_requests.id','=',$id)
        ->first();

        $pending_requisition_process['details'] = DB::table('purchase_distribution_requests')
        ->join('purchase_distribution_request_details','purchase_distribution_requests.id','=','purchase_distribution_request_details.purchase_distribution_request_id')
        ->join('products','purchase_distribution_request_details.product_id','=','products.id')
        ->join('product_generics','purchase_distribution_request_details.generic_id','=','product_generics.id')
        ->join('product_unit','purchase_distribution_request_details.unit_id','=','product_unit.id')
        ->join('uoms','purchase_distribution_request_details.uom_id','=','uoms.id')
        ->join('vendors','purchase_distribution_request_details.vendor_id','=','vendors.id')
        ->select('products.id as product_id','products.product_name','product_generics.id as generic_id','product_generics.name as generic_name','product_unit.id as unit_id','product_unit.unit_name','uoms.id as uom_id','uoms.uom_name','vendors.id as vendor_id','vendors.name as vendor_name','purchase_distribution_request_details.purchase_quantity')

        ->where('purchase_distribution_requests.id','=',$id)
        ->get();

        return $pending_requisition_process;
    }

    public function requisition_process_add($id)
    {
        $hub_approved_requisition_details = HubApproveRequisitionDetails::find($id);
        $hub_approved_requisition_details->status = 'Add';
        $hub_approved_requisition_details->save();

    }

    public function requisition_process_addAll(Request $request)
    {
        $id = $request->ids;
        $statusUpdate=HubApproveRequisitionDetails::whereIn('id',explode(",",$id))->update(["status" => "Add"]);



        // $hub_approved_requisition_details = HubApproveRequisitionDetails::find($id);
        // $hub_approved_requisition_details->status = 'Add';
        // $hub_approved_requisition_details->save();

    }

    public function update(UpdateHubRequest $request, $id)
    {

        return "Hub Update";
    }






    public function destroy(Request $request)
    {
        return "Hub destroy";
    }
}
