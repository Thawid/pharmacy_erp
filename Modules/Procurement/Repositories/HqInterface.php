<?php


namespace Modules\Procurement\Repositories;
use Illuminate\Http\Request;
use Modules\Procurement\Http\Requests\HqFormRequest;


interface HqInterface
{
    public function index();

    public function create();

    public function store();

    public function show($id);

    public function edit($id);

    public function update(HqFormRequest $request, $id);

    public function destroy($id);

    public function hq_req_details($id,$req_id);

    public function hq_requisition();

    public function hq_approved_requisition();


}
