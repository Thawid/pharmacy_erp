<?php


namespace Modules\Procurement\Repositories;

use Illuminate\Http\Request;
use Modules\Procurement\Repositories\ProcurementInterface;



class ProcurementRepository implements ProcurementInterface
{
    public function index()
    {

    }

    public function create()
    {

    }

    public function store()
    {

    }

    public function edit($id)
    {

    }

    public function update($id)
    {

    }

    public function destroy($id)
    {

    }

}
