<?php
namespace Modules\Procurement\Repositories;

use Modules\Procurement\Http\Requests\PurchaseOrderRequest;

interface PurchaseInterface
{
    public function order_pending_list();

    public function purchase_order_store(PurchaseOrderRequest $request);

    public function purchase_order_create($id,$req_id);

    public function purchase_order_update(PurchaseOrderRequest $request,$id);

    public function show($id);

}
