<?php


namespace Modules\Procurement\Repositories;


use Illuminate\Http\Request;
use Modules\Procurement\Http\Requests\StoreRequisitionFormRequest;
use Modules\Procurement\Http\Requests\UpdateStoreRequisitionFormRequest;

interface StoreInferface
{
    public function index();

    public function create();

    public function show($id);

    public function store(StoreRequisitionFormRequest $request);

    public function edit($id);

    public function update(UpdateStoreRequisitionFormRequest $request, $id);

    public function destroy(Request $request);
    public function getProductByID($id);

}
