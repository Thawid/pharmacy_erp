<?php


namespace Modules\Procurement\Repositories;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\Entities\HqRequisition;
use Modules\Procurement\Entities\HqRequisitionDetails;
use Modules\Procurement\Entities\HubRequisition;
use Modules\Procurement\Entities\HubRequisitionDetails;
use Modules\Procurement\Http\Requests\HqFormRequest;

class HqRepository implements HqInterface
{

    public function index()
    {
        $hub_requisition = HubRequisition::with('store')->orderBy('id','DESC')->get();
        return $hub_requisition;

    }

    public function create()
    {
        return "HQ Create";
    }

    public function store()
    {
        return "HQ Store";
    }

    public function show($id)
    {
        $hub_requisition_details = DB::table('hub_requisitions')
            ->join('hub_requisition_details','hub_requisitions.id','=','hub_requisition_details.hub_requisition_id')
            ->join('stores','hub_requisitions.hub_id','=','stores.id')
            ->join('products','hub_requisition_details.product_id','=','products.id')
            ->join('product_generics','hub_requisition_details.generic_id','=','product_generics.id')
            ->join('product_unit','hub_requisition_details.unit_id','=','product_unit.id')
            ->join('vendors','hub_requisition_details.vendor_id','=','vendors.id')
            ->join('uoms','hub_requisition_details.uom_id','=','uoms.id')
            ->select('hub_requisitions.*','hub_requisition_details.request_quantity','hub_requisition_details.status','products.product_name','product_generics.name as generic_name','vendors.name as vendor_name','product_unit.unit_name','uoms.uom_name')
            ->where('hub_requisition_id',$id)
            ->get();
        return $hub_requisition_details;

    }

    public function edit($id)
    {
        $hqrequisiton = HubRequisitionDetails::with(['requisitions','product','generic','unit','uom','vendor'])
            ->select('hub_requisition_details.id as hub_req_details_id','hub_requisition_details.*')
            ->where('hub_requisition_id',$id)
            ->get();
        $hqrequisiton = $hqrequisiton->groupBy('hub_requisition_id');
        $hqrequisiton = $hqrequisiton->all();
        return $hqrequisiton;

    }

    public function update(HqFormRequest $request, $id)
    {
        //dd($request->all());
        $product_id = $request->product;
        $generic_id = $request->generic_id;
        $vendor_id = $request->vendor_id;
        $unit_id = $request->unit_id;
        $quantity = $request->qty;
        $uom_id = $request->uom_id;
        $hub_id = $request->hub_id;
        $store_type = $request->store_type;
        $hub_requisition_id = $request->hub_requisition_id;
        $hub_requisition_no = $request->hub_requisition_no;
        $update_id = $request->id;

        $requisition_date = $request->requisition_date;
        $delivery_date = $request->delivery_date;
        $priority = $request->priority;
        $save_merge = $request->merge;
        $insert_requisition = [];

        if($save_merge){

            DB::beginTransaction();
            //$requisitionNo = rand(1111,9999). time();
            try {

                $hq_requisition = new HqRequisition();
                $hq_requisition->store_type = $store_type;
                $hq_requisition->requisition_date = $requisition_date;
                $hq_requisition->delivery_date = $delivery_date;
                $hq_requisition->priority = $priority;
                $hq_requisition->hub_id = $hub_id;
                $hq_requisition->hub_requisition_id = $hub_requisition_id;
                $hq_requisition->hub_requiquisition_no = $hub_requisition_no;
                $hq_requisition->status = 0;
                $hq_requisition->save();
                $hq_requisition_id = $hq_requisition->id;

                try {
                    $counter = 0;
                    foreach ($product_id as $product){
                        $insert_requisition[]= [
                            'hq_approve_requisition_id'=>$hq_requisition_id,
                            'vendor_id'=>$vendor_id[$counter],
                            'product_id'=>$product,
                            'generic_id'=>$generic_id[$counter],
                            'unit_id'=>$unit_id[$counter],
                            'purchase_quantity'=>$quantity[$counter],
                            'uom_id'=>$uom_id[$counter],
                            'created_at' => Carbon::now(),
                        ];
                        $counter++;
                    }

                    try {
                        HqRequisitionDetails::insert($insert_requisition);

                        $old_req_details_id = [];
                        $old_product = DB::table('hub_requisition_details')
                            ->where('hub_requisition_id',$hub_requisition_id)
                            ->where('status','=','Pending')
                            ->get();

                        foreach ($old_product as $old_product_id){
                            $old_req_details_id[] = $old_product_id->id;
                        }

                        $cancel_product_id = array_diff($old_req_details_id,$update_id);

                        $approve_product_id = array_intersect($old_req_details_id,$update_id);

                        if(count($cancel_product_id) != 0){
                            foreach ($cancel_product_id  as $cancel_id){
                                DB::table('hub_requisition_details')
                                    ->where('hub_requisition_id',$hub_requisition_id)
                                    ->where('id', $cancel_id)
                                    ->where('status','=','Pending')
                                    ->update(['status' => 'Cancel']);
                            }
                        }

                        foreach ($approve_product_id as $approved_id){
                            DB::table('hub_requisition_details')
                                ->where('id', $approved_id)
                                ->update(['status' => 'Approved']);
                        }

                        DB::commit();
                        return true;
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return redirect()->back()->withErrors("Requisition dosen't merge " . $e->getMessage());
                    }
                }catch (\Exception $e){
                    return redirect()->back()->withErrors("Requisition dosen't merge Hq Req Details Table Problem " . $e->getMessage());
                }
            }catch (\Exception $e){
                return redirect()->back()->withErrors("Requisition dosen't merge - Hq Req Table Problem " . $e->getMessage());
            }
        }
    }



    public function hq_requisition(){

    $hq_requisitions = DB::table('hq_approve_requisitions')
        ->join('hq_approve_requisition_details','hq_approve_requisitions.id','=','hq_approve_requisition_details.hq_approve_requisition_id')
        ->join('vendors','hq_approve_requisition_details.vendor_id','=','vendors.id')
        ->select('hq_approve_requisitions.id as req_id','hq_approve_requisitions.hub_requisition_id','hq_approve_requisitions.hub_requiquisition_no','hq_approve_requisitions.requisition_date','hq_approve_requisitions.delivery_date','hq_approve_requisitions.status','vendors.name','vendors.id as vendor_id','hq_approve_requisition_details.hq_approve_requisition_id',
            DB::raw('CASE WHEN SUM(hq_approve_requisition_details.status) = COUNT(hq_approve_requisition_details.status) THEN 1 ELSE 0 END as approved'))
        ->groupBy(['hq_approve_requisition_details.vendor_id','hq_approve_requisition_details.hq_approve_requisition_id'])
        ->orderBy('hq_approve_requisitions.id','DESC')
        ->get();
        $hq_requisitions = $hq_requisitions->groupBy(['hq_approve_requisition_id','vendor_id']);
        return $hq_requisitions;
    }

    public function hq_approved_requisition(){

        $hq_requisitions = DB::table('hq_approve_requisitions')
            ->join('hq_approve_requisition_details','hq_approve_requisitions.id','=','hq_approve_requisition_details.hq_approve_requisition_id')
            ->join('vendors','hq_approve_requisition_details.vendor_id','=','vendors.id')
            ->select('hq_approve_requisitions.id as req_id','hq_approve_requisitions.hub_requisition_id','hq_approve_requisitions.hub_requiquisition_no','hq_approve_requisitions.requisition_date','hq_approve_requisitions.delivery_date','hq_approve_requisitions.status','vendors.name','vendors.id as vendor_id','hq_approve_requisition_details.hq_approve_requisition_id',
                DB::raw('CASE WHEN SUM(hq_approve_requisition_details.status) = COUNT(hq_approve_requisition_details.status) THEN 1 ELSE 0 END as approved'))
            ->groupBy(['hq_approve_requisition_details.vendor_id','hq_approve_requisition_details.hq_approve_requisition_id'])
            ->orderBy('hq_approve_requisitions.id','DESC')
            ->get();
        $hq_requisitions = $hq_requisitions->groupBy(['hq_approve_requisition_id','vendor_id']);
        return $hq_requisitions;
    }


    public function hq_req_details($id,$req_id){
        $hq_req_details = DB::table('hq_approve_requisitions')
            ->join('hq_approve_requisition_details','hq_approve_requisitions.id','=','hq_approve_requisition_details.hq_approve_requisition_id')
            ->join('products','hq_approve_requisition_details.product_id','products.id')
            ->join('product_generics','hq_approve_requisition_details.generic_id','=','product_generics.id')
            ->join('product_unit','hq_approve_requisition_details.unit_id','=','product_unit.id')
            ->join('uoms','hq_approve_requisition_details.uom_id','=','uoms.id')
            ->select('products.product_name','product_generics.name as generic_name','product_unit.unit_name','hq_approve_requisition_details.purchase_quantity','uoms.uom_name')
            ->where('hq_approve_requisition_details.vendor_id',$id)
            ->where('hq_approve_requisition_details.hq_approve_requisition_id',$req_id)
            ->get();

        return $hq_req_details;
    }

    public function destroy($id)
    {

    }

}
