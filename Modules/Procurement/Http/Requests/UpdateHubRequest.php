<?php

namespace Modules\Procurement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class UpdateHubRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->request->get('action') == 'Save & Quit') {
            return [
                'priority'=>'required',
                'product'=>'required',
                'generic_id'=>'required',
                'unit_id'=>'required',
                'vendor'=>'required',
                'qty'=>'required|array',
                'qty.*' => 'integer|gte:1',
            ];
       }

        return [
            'requisition_date'=>'required|date|before:delivery_date',
            'delivery_date'=>'required|date|after:requisition_date',
            'priority'=>'required',
            'product'=>'required',
            'generic_id'=>'required',
            'unit_id'=>'required',
            'vendor'=>'required',
            'qty'=>'required|array',
            'qty.*' => 'integer|gte:1',
        ];
       
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
