<?php

namespace Modules\Procurement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'purchase_order_date'=>'required|date|before:delivery_date',
            'delivery_date'=>'required|date|after:purchase_order_date',
            'priority'=>'required',
            'product_id'=>'required|array',
            'generic_id'=>'required|array',
            'unit_id'=>'required|array',
            'vendor_id'=>'required',
            'uom_id'=>'required|array',
            'qty'=>'required|array',
            'qty.*' => 'integer|gte:1',
            'uom_price'=>'required|array',
            'unit_price.*' => 'integer|gte:1',
            'product_sub_total'=>'required|array',
            'sub_total'=>'required',
            'total_price'=>'required',
        ];
    }



    public function messages()
    {
        return[
            'purchase_order_date.required'=>'Purchase Order date must be required',
            'purchase_order_date.before'=>'Purchase Order date must be before Delivery Date',
            'delivery_date.required'=>'Delivery date must be required',
            'delivery_date.after'=>'Delivery date must be after Purchase order date',
            'priority.required'=>'Priority field is required',
            'product_id.required'=>'Product field is required',
            'generic_id.required'=>'Generic field is required',
            'unit_id.required'=>'Unit field is required',
            'vendor_id.required'=>'Vendor is required',
            'uom_id.required'=>'UOM field is required',
            'qty.required'=>'Quantity field is required',
            'qty.*.integer'=>'Invalid quantity',
            'qty.*.gte'=>'Product quantity must be grater then or equal one',
            'unit_price.required'=>'Unit Price field is required',
            'uom_price.*.gte'=>'Unit Price must be grater then or equal one',
            'product_sub_total.required'=>'Product Sub Total field is required',
            'sub_total.required'=>'Sub Total field is required',
            'total_price.required'=>'Total field is required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
