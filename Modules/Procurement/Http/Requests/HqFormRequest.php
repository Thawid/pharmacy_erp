<?php

namespace Modules\Procurement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HqFormRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'requisition_date'=>'required|date|before:delivery_date',
            'delivery_date'=>'required|date|after:requisition_date',
            'priority'=>'required',
            'product'=>'required',
            'generic_id'=>'required',
            'unit_id'=>'required',
            'qty'=>'required|array',
            'qty.*' => 'integer|gte:1',
        ];
    }


    public function messages()
    {
        return [
            'qty.required'=>'Product quantity must be required',
            'qty.*.integer'=>'Invalid product quantity',
            'qty.*.gte'=>'Quantity must be greater then or equal one'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
