<?php

namespace Modules\Procurement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequisitionFormRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'requisition_date'=>'required|date|before:delivery_date',
            'delivery_date'=>'required|date|after:requisition_date',
            'priority'=>'required',
            'product'=>'required|array',
            'generic_id'=>'array',
            'unit_id'=>'array',
            'vendor'=>'array',
            'qty'=>'required|array',
            'qty.*' => 'integer|gte:1',
        ];
    }


    public function messages()
    {
        return [
            'requisition_date.required'=>'Requisition date must be required',
            'requisition_date.date'=>'Invalid date format',
            'requisition_date.before'=>'Requisition date must be before a delivery date',
            'delivery_date.required'=>'Delivery date must be required',
            'delivery_date.date'=>'Invalid date format',
            'delivery_date.after'=>'Delivery date must be after a requisition date',
            'priority.required'=>'Requisition priority is required filed',
            'product.required'=>'Please select product first',
            'product.array'=>'Invalid product field format',
            'generic_id.array'=>'Invalid generic field format',
            'unit_id.array'=>'Invalid unit field format',
            'vendor.array'=>'Invalid vendor field format',
            'qty.array'=>'Invalid quantity field format',
            'qty.required'=>'Please fill quantity field',
            'qty.*.integer'=>'Invalid quantity',
            'qty.*.gte'=>'Product quantity must be grater then or equal one',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
