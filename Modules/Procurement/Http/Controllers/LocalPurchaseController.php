<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Store\Entities\StoreUser;
use Modules\Product\Entities\Product;
use Modules\Procurement\Entities\LocalPurchase;
use Modules\Procurement\Entities\LocalPurchaseDetails;
use Modules\Store\Entities\Store;
use Carbon\Carbon;
use DB;


class LocalPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

            $store = StoreUser::where('user_id', auth()->user()->id)
                ->where('store_type','store')
                ->where('status', 1)
                ->first();
            $local_purchase = LocalPurchase::with(['details','details.product_uom','details.product','details.unit','details.generic'])
                 ->orderBy('local_purchases.id','DESC')
                 ->where('store_id',$store->store_id)
                 ->get();

        return view('procurement::procurement-management.local-purchase.index', compact('local_purchase'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        $store_available_product = StoreAvailableProduct::where('store_id', $store->store_id)->first();

        $products = DB::table('store_available_product_details')->where('store_available_product_id',$store_available_product->id)
                   ->join('products', 'store_available_product_details.product_id', '=', 'products.id')->get();
        $uom      = DB::table('uoms')->get();
        return view('procurement::procurement-management.local-purchase.create', compact('products', 'uom'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //  return $request;

       DB::beginTransaction();
       try {


           $product_id = $request->product;
           $generic_id = $request->generic_id;
           $unit_id = $request->unit_id;
           $quantity = $request->purchase_qty;
           $uom_id = $request->uom_id;
        //    $uom_price = $request->uom_price;
        //    $discount = $request->discount;
        //    $sub_total = $request->sub_total;
        //    $purchase_price = $request->purchase_price;

           $store = StoreUser::where('user_id', auth()->user()->id)->where('status', 1)->first();
           $get_store_type = Store::select('store_type')->where('id',$store->store_id)->firstOrFail();
           if ($store) {
                 $requisitionNo = rand(1111, 9999). time();
                 $storeRequisition = new LocalPurchase();
                 $storeRequisition->total                   = $request->total;
                 $storeRequisition->purchase_order_no       = $requisitionNo;
                 $storeRequisition->store_id                = $store->store_id;
                 $storeRequisition->purchase_date           = $request->purchase_date;
                 $storeRequisition->purchase_sub_total	  = $request->purchase_sub_total;
                 $storeRequisition->store_type              = $get_store_type->store_type;
                 $storeRequisition->total                   = $request->total;
                 $storeRequisition->overall_discount        = $request->overall_discount;

                $storeRequisition->save();

               $local_purchase_id = $storeRequisition->id;
               $count = count($product_id);
               for ($i = 0; $i < $count; $i++) {
                   $details[] = [
                       'local_purchase_id' => $local_purchase_id,
                       'product_id' => $product_id[$i],
                       'generic_id' => $generic_id[$i],
                       'unit_id' => $unit_id[$i],
                       'uom_id' => $uom_id[$i],
                       'purchase_qty' => $quantity[$i],
                    //    'uom_price' => $uom_price[$i],
                    //    'discount' => $discount[$i],
                    //    'sub_total' => $sub_total[$i],
                    //    'purchase_price'=>$purchase_price[$i],
                       'status'=>'Pending',
                       'created_at' => Carbon::now(),
                   ];
               }

               LocalPurchaseDetails::insert($details);
               DB::commit();
               return redirect()->route('local-purchase.index')->with('success','Requisition create successfully');
           } else {
               return false;
           }
       } catch (\Exception $e) {
           DB::rollback();
           return redirect()->back()->withErrors("local dosen't created" . $e->getMessage());
       }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $local_purchase_details = DB::table('local_purchase_details')
                                 ->join('local_purchases', 'local_purchase_details.local_purchase_id', '=', 'local_purchases.id')
                                 ->join('products', 'local_purchase_details.product_id', '=', 'products.id')
                                 ->join('product_generics', 'local_purchase_details.generic_id', '=', 'product_generics.id')
                                 ->join('product_unit','local_purchase_details.unit_id','=','product_unit.id')
                                 ->join('uoms','local_purchase_details.uom_id','=','uoms.id')
                                 ->select('local_purchases.purchase_order_no','local_purchases.purchase_date','local_purchases.purchase_sub_total','local_purchases.total','local_purchases.overall_discount','products.product_name','product_generics.name as generic_name','product_unit.unit_name','uoms.uom_name','local_purchase_details.purchase_qty','local_purchase_details.uom_price','local_purchase_details.discount','local_purchase_details.sub_total')
                                 ->where('local_purchase_details.local_purchase_id',$id)
                                 ->get();


        return view('procurement::procurement-management.local-purchase.details', compact('local_purchase_details'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $local_purchase = LocalPurchase::with(['details','details.product_uom','details.product','details.unit','details.generic','store'])->where('id', $id)->first();
        // return  $local_purchase_edit;
        return view('procurement::procurement-management.local-purchase.edit',compact('local_purchase'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    public function local_purchase_update(Request $request, $id)
    {
       // dd($request->all());

        DB::beginTransaction();
        try {
            $local_purchase = LocalPurchase::find($id);
            $local_purchase->purchase_sub_total = $request->sub_total;
            $local_purchase->overall_discount = $request->overall_discount;
            $local_purchase->total = $request->total_price;
            $local_purchase->status = 1;
            $local_purchase->save();

            foreach ($request->product_id as $key=>$product){
                DB::table('local_purchase_details')->where('local_purchase_id',$id)
                    ->where('product_id',$product)
                    ->update([
                        'uom_price'=>$request->uom_price[$key],
                        'discount'=>$request->discount[$key],
                        'sub_total'=>$request->product_sub_total[$key],
                    ]);
            }
            DB::commit();
            return redirect()->route('local-purchase.index')->with('success','Purchase create successfully');
        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors($e->getMessage());
        }

    }
}
