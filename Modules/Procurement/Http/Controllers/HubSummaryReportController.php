<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\Warehouse;
use Modules\Price\Entities\UomPrice;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;

class HubSummaryReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $get_warehouses = [];
        $hub_available_product_details = [];
        $get_products = [];
        $hub = StoreUser::where('user_id', auth()->user()->id)->where('store_type', '=', 'hub')->where('status', 1)->first();

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer' || auth()->user()->role == 'hub_user') {
            $get_stores = Store::with('warehouse')->where('hub_id', $hub->store_id)->where('status', 1)->get();
            foreach ($get_stores as $store) {
                $get_warehouses[] = $store->warehouse;
            }
        }
        $warehouses = array_unique($get_warehouses);
        $hub_available_products = HubAvailableProduct::where('hub_id', $hub->store_id)->select('id')->get();

        foreach ($hub_available_products as $hub_available_product) {
            $hub_available_product_details[] = HubAvailableProductDetails::
                join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                ->where('hub_available_product_id', $hub_available_product->id)
                ->select('products.id', 'products.product_name')
                ->get();
        }

        foreach ($hub_available_product_details as $available_product_details) {
            foreach ($available_product_details as $product) {
                $get_products[] = $product;
            }
        }
        $products = array_unique($get_products);

        return view('procurement::procurement-management.report.hub-summary-report',compact('products', 'warehouses'));
    }

    public function get_hub_report(Request $request)
    {
        $get_data = [];
        $single_product_details = [];
        $product_name = null;
        $hub_available_product_details = [];
        $uom_prices = [];
        $get_price_foreach_product = [];
        $purchase_order_details = [];
        $get_store_price_foreach_product = [];
        $get_price_single_product = null;
        $get_store_price_single_product = null;
        $warehouse_name = Warehouse::find($request->data['warehouse_id']);
        $hub = StoreUser::where('user_id', auth()->user()->id)->where('store_type', '=', 'hub')->where('status', 1)->first();
        $hub_available_products = HubAvailableProduct::where('hub_id', $hub->store_id)->get('id');

        // for single data
        if ($request->data['product_id'] !== '0') {
            foreach ($hub_available_products as $hub_available_product) {
                $single_product_details[] = HubAvailableProductDetails::
                    join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                    ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                    ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                    ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                    ->join('uoms', 'products.purchase_uom_id', '=', 'uoms.id')
                    ->where('product_id', $request->data['product_id'])
                    ->where('hub_available_product_id', $hub_available_product->id)
                    ->select('products.id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'hub_available_product_details.available_quantity', 'uoms.uom_name')
                    ->first();
            }
            foreach ($single_product_details as $single_product) {
                $get_data[] = $single_product;
                if (!empty($single_product) && $single_product->available_quantity !== 0) {
                    $get_price_single_product[] = UomPrice::select('id', 'product_id', 'uom_price')->where('product_id', $single_product->id)->get()->last();
                    $get_store_price_single_product[] = PurchaseOrderDetails::select('id', 'product_id', 'uom_price')->where('product_id', $single_product->id)->get()->last();
                }
            }
            $uom_prices = array_unique($get_price_single_product);
            $purchase_order_details = array_unique($get_store_price_single_product);
            $product_name = Product::find($request->data['product_id']);
        }else {
            //select warehouse only
            foreach ($hub_available_products as $hub_available_product) {
                $hub_available_product_details[] = HubAvailableProductDetails::
                    join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                    ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                    ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                    ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                    ->join('uoms', 'products.purchase_uom_id', '=', 'uoms.id')
                    ->where('hub_available_product_id', $hub_available_product->id)
                    ->select('products.id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'hub_available_product_details.available_quantity', 'uoms.uom_name')
                    ->get();
            }

            foreach ($hub_available_product_details as $available_product_details) {
                foreach ($available_product_details as $product) {
                    $get_data[] = $product;
                    if ($product->available_quantity !== 0) {
                        $get_price_foreach_product[] = UomPrice::select('id', 'product_id', 'uom_price')->where('product_id', $product->id)->get()->last();
                        $get_store_price_foreach_product[] = PurchaseOrderDetails::select('id', 'product_id', 'uom_price')->where('product_id', $product->id)->get()->last();
                    }
                }
            }
            $uom_prices = array_unique($get_price_foreach_product);
            $purchase_order_details = array_unique($get_store_price_foreach_product);
        }
        $data = $get_data;

        return view('procurement::procurement-management.report.hub-summary-report-view', compact('data', 'uom_prices', 'purchase_order_details','warehouse_name', 'product_name'));

    }
}
