<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\Price\Entities\UomPrice;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;

class StoreSummaryReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $store = [];
        $products = [];
        $get_products = [];
        $store_available_product_details = [];
        $store_user = StoreUser::where('user_id', auth()->user()->id)->where('store_type', '=', 'store')->where('status', 1)->first();

        if (auth()->user()->role == 'store_user') {
            $store = Store::select('id', 'name')->where('id', $store_user->store_id)->where('store_type', '=', 'store')->where('status', 1)->first();
            $store_available_products = StoreAvailableProduct::where('store_id', $store->id)->select('id')->get();
            foreach ($store_available_products as $store_available_product) {
                $store_available_product_details[] = StoreAvailableProductDetails::
                join('products', 'store_available_product_details.product_id', '=', 'products.id')
                    ->where('store_available_product_id', $store_available_product->id)
                    ->select('products.id', 'products.product_name')
                    ->get();
            }
            foreach ($store_available_product_details as $available_product_details) {
                foreach ($available_product_details as $product) {
                    $get_products[] = $product;
                }
            }
            $products = array_unique($get_products);
        }

        return view('procurement::procurement-management.report.store-summary-report',compact('products', 'store'));
    }

    public function get_store_report(Request $request)
    {
        $get_data = [];
        $single_product_details = [];
        $product_name = null;
        $get_price_foreach_product = [];
        $get_store_price_foreach_product = [];
        $get_price_single_product = null;
        $get_store_price_single_product = null;
        $store_user = StoreUser::where('user_id', auth()->user()->id)->where('store_type', '=', 'store')->where('status', 1)->first();
        $store = Store::select('id', 'name')->where('id', $store_user->store_id)->where('store_type', '=', 'store')->where('status', 1)->first();
        $store_available_products = StoreAvailableProduct::where('store_id', $store_user->store_id)->get('id');

        // for single data
        if ($request->data['product_id'] !== '0') {
            foreach ($store_available_products as $store_available_product) {
                $single_product_details[] = StoreAvailableProductDetails::
                join('products', 'store_available_product_details.product_id', '=', 'products.id')
                    ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                    ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                    ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                    ->join('uoms', 'products.purchase_uom_id', '=', 'uoms.id')
                    ->where('product_id', $request->data['product_id'])
                    ->where('store_available_product_id', $store_available_product->id)
                    ->select('products.id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'store_available_product_details.available_quantity', 'uoms.uom_name')
                    ->first();
            }
            foreach ($single_product_details as $single_product) {
                $get_data[] = $single_product;
                if (!empty($single_product) && $single_product->available_quantity !== 0) {
                    $get_price_single_product[] = UomPrice::select('id', 'product_id', 'uom_price')->where('product_id', $single_product->id)->get()->last();
                    $get_store_price_single_product[] = PurchaseOrderDetails::select('id', 'product_id', 'uom_price')->where('product_id', $single_product->id)->get()->last();
                }
            }
            $uom_prices = array_unique($get_price_single_product);
            $purchase_order_details = array_unique($get_store_price_single_product);
            $product_name = Product::find($request->data['product_id']);
        }else {
            //select store only
            foreach ($store_available_products as $store_available_product) {
                $hub_available_product_details[] = StoreAvailableProductDetails::
                join('products', 'store_available_product_details.product_id', '=', 'products.id')
                    ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                    ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                    ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                    ->join('uoms', 'products.purchase_uom_id', '=', 'uoms.id')
                    ->where('store_available_product_id', $store_available_product->id)
                    ->select('products.id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'store_available_product_details.available_quantity', 'uoms.uom_name')
                    ->get();
            }

            foreach ($hub_available_product_details as $available_product_details) {
                foreach ($available_product_details as $product) {
                    $get_data[] = $product;
                    if ($product->available_quantity !== 0) {
                        $get_price_foreach_product[] = UomPrice::select('id', 'product_id', 'uom_price')->where('product_id', $product->id)->get()->last();
                        $get_store_price_foreach_product[] = PurchaseOrderDetails::select('id', 'product_id', 'uom_price')->where('product_id', $product->id)->get()->last();
                    }
                }
            }
            $uom_prices = array_unique($get_price_foreach_product);
            $purchase_order_details = array_unique($get_store_price_foreach_product);
        }
        $data = $get_data;

        return view('procurement::procurement-management.report.store-summary-report-view', compact('data', 'uom_prices', 'store', 'purchase_order_details', 'product_name'));

    }
}
