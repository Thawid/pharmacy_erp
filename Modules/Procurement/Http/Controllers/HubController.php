<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Procurement\Repositories\HubInterface;
use Modules\Product\Entities\ProductVariation;
use Modules\Procurement\Http\Requests\UpdateHubRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Modules\Procurement\Entities\HubApproveRequisitionDetails;
use Modules\Store\Entities\StoreUser;

class HubController extends Controller
{

   private $hub_requisition;

   public function __construct(HubInterface $hub_requisition){
       $this->hub_requisition = $hub_requisition;
   }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $hub_i = $this->hub_requisition->index();

    }

    //////////////////////  Hub Requisition All list  //////////////////////

    public function requisition_pending_list()
    {

       $pending_requisition= $this->hub_requisition->requisition_pending_list();
       return view('procurement::procurement-management.hub.pending_requisition_list', compact('pending_requisition'));

    }

    public function requisition_process_list()
    {
        $process_list=$this->hub_requisition->requisition_process_list();
        return view('procurement::procurement-management.hub.process_requisition_list',compact('process_list'));
    }

    public function requisition_process_confirm_view()
    {
       $new_requisition=$this->hub_requisition->requisition_process_confirm_view();
       return view('procurement::procurement-management.hub.process_requisition_confirm', compact('new_requisition'));
    }


    public function requisition_list()
    {
        $requisition_list= $this->hub_requisition->requisition_list();
        return view('procurement::procurement-management.hub.requisition_list',compact('requisition_list'));
    }

    //////////////////////  Hub Requisition All list  //////////////////////


    //////////////////////  Hub Requisition All Details  //////////////////////

    public function requisition_pending_details($id)
    {


       $pending_requisition_details= $this->hub_requisition->requisition_pending_details($id);
       return view('procurement::procurement-management.hub.pending_requisition_details',compact('pending_requisition_details'));
    }

    public function requisition_process_details($id)
    {

       $details=$this->hub_requisition->requisition_process_details($id);
       return view('procurement::procurement-management.hub.process_requisition_details',compact('details'));
    }

    public function requisition_details($id)
    {

       $details= $this->hub_requisition->requisition_details($id);
       return view('procurement::procurement-management.hub.requisition_details',compact('details'));
    }



    //////////////////////  Hub Requisition All Details  //////////////////////


    //////////////////////  Hub Requisition All Modify  //////////////////////

    public function requisition_pending_modify($id)
    {

        $pending_requisition_process= $this->hub_requisition->requisition_pending_modify($id);
        return view('procurement::procurement-management.hub.pending_requisition_modify',compact('pending_requisition_process'));
    }

    public function pending_requisition_merge(Request $request)
    {
       // return $request->all();
       $this->hub_requisition->pending_requisition_merge($request);
       return redirect()->route('hub.requisition.pending.list')->with('success','Successfully Hub Approve Requisition Save');
    }

    public function requisition_process_add($id)
    {
        $this->hub_requisition->requisition_process_add($id);
        return redirect()->route('hub.requisition.process.list')->with('success', 'This Product Add to New Requisition');
    }

    public function requisition_process_addAll(Request $request)
    {

         $this->hub_requisition->requisition_process_addAll($request);
        return response()->json(['success'=>'All Products Add to New Requisition']);
    }



    //////////////////////  Hub Requisition All Modify  //////////////////////




    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');
        $products=$this->hub_requisition->create();
        return view('procurement::procurement-management.hub.create',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $this->hub_requisition->store($request);
        return redirect()->route('hub.requisition.list')->with('success', 'Successfully Create Hub Requisition');

    }

    public function requisition_process_confirm_store(Request $request)
    {

        $this->hub_requisition->requisition_process_confirm_store($request);
        return redirect()->route('hub.requisition.process.list')->with('success', 'Successfully Create Hub Requisition');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $hub_requisition_details = $this->hub_requisition->show($id);
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $hub_requisition_details = $this->hub_requisition->edit($id);

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateHubRequest $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */

    public function destroy(Request $request)
    {

    }

}
