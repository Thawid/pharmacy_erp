<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Procurement\Repositories\ProcurementReportInterface;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use Modules\Vendor\Entities\Vendor;


class ProcurementReportController extends Controller
{

    private $procurementReport;

    public function __construct(ProcurementReportInterface $procurementReport){
        $this->procurementReport = $procurementReport;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $stores = Store::select('id', 'name')
                ->where('store_type', 'store')
                ->get();
        } else {
            $hub_user = StoreUser::select('store_id')->where('user_id', auth()->user()->id)->where('store_type', 'hub')->first();
            $stores = Store::select('id', 'name')
                ->where('hub_id', $hub_user->store_id)
                ->where('store_type', 'store')
                ->get();
        }
        return view('procurement::procurement-management.report.store-report',compact('stores'));
    }


    public function get_store_report(Request $request){

        $store_id = $request->data['store_id'];
        $start_date = $request->data['start_date'];
        $start_date = $start_date . ' 00:00:00';
        $end_date = $request->data['end_date'];
        $priority = $request->data['priority'];
        $status = $request->data['status'];
        $date_one = date_create($start_date);

        $show_date_one = date_format($date_one, 'd-m-Y');
        if (empty($end_date)) {
            $show_date_two = '';
        } else {
            $date_two = date_create($end_date);
            $show_date_two = date_format($date_two, 'd-m-Y');
        }
        if (empty($end_date)) {
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date . ' 23:59:59';

        $data = DB::table('store_requisitions')
            ->join('store_requisition_details','store_requisitions.id','=','store_requisition_details.store_requisition_id')
            ->join('stores','store_requisitions.store_id','=','stores.id')
            ->select('store_requisitions.store_requisition_no','store_requisitions.priority','store_requisitions.status','store_requisitions.requisition_date','store_requisitions.delivery_date',
                'stores.name',DB::raw('SUM(store_requisition_details.purchase_price) AS purchase_price'))
            ->groupBy('store_requisition_details.store_requisition_id')
            ->whereBetween('store_requisitions.created_at', [$start_date, $end_date])
            ->orWhere('stores.id',$store_id)
            ->orWhere('store_requisitions.status',$status)
            ->orWhere('store_requisitions.priority',$priority)
            ->get();
        return view('procurement::procurement-management.report.store-report-view',compact('data'));
    }


    public function show_purchase_report(){
        $hubs = Store::where('store_type','=','hub')->get();
        $vendors = Vendor::all();
        return view('procurement::procurement-management.report.purchase-report',compact('hubs','vendors'));
    }


    public function get_purchase_report(Request $request){
        //dd($request->data);
        $vendor_id = $request->data['vendor_id'];
        $hub_id = $request->data['hub_id'];
        $start_date = $request->data['start_date'];
        $start_date = $start_date . ' 00:00:00';
        $end_date = $request->data['end_date'];
        /*$priority = $request->data['priority'];
        $status = $request->data['status'];*/
        $date_one = date_create($start_date);

        $show_date_one = date_format($date_one, 'd-m-Y');
        if (empty($end_date)) {
            $show_date_two = '';
        } else {
            $date_two = date_create($end_date);
            $show_date_two = date_format($date_two, 'd-m-Y');
        }
        if (empty($end_date)) {
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date . ' 23:59:59';

        $data = DB::table('purchase_orders')
            ->join('vendors','purchase_orders.vendor_id','=','vendors.id')
            ->join('stores','purchase_orders.hub_id','=','stores.id')
            ->select('purchase_orders.total','stores.name as hub_name','vendors.name as vendor_name',
            DB::raw('SUM(purchase_orders.total) as total')
            )
            ->groupBy('purchase_orders.vendor_id','purchase_orders.hub_id')
            ->whereBetween('purchase_orders.created_at', [$start_date, $end_date])
            ->orWhere('stores.id',$hub_id)
            ->orWhere('vendors.id',$vendor_id)
            ->get();
        //dd($data);
        return view('procurement::procurement-management.report.purchase-report-view',compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('procurement::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
