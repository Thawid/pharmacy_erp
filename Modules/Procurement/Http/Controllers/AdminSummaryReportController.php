<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Price\Entities\UomPrice;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\Store;

class AdminSummaryReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $hubs = [];
        $hub_available_products = [];
        $per_hub_available_products = [];

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $hubs = Store::select('id', 'name')->where('store_type', 'hub')->where('status', 1)->get();
        }
        foreach ($hubs as $hub) {
            $hub_available_products[] = HubAvailableProduct::with('hubAvailableProductDetails')->select('id')->where('hub_id', $hub->id)->get();
        }

        foreach ($hub_available_products as $temp_hub_available_product) {
            foreach ($temp_hub_available_product as $hub_available_product) {
                $per_hub_available_products[] = HubAvailableProductDetails::
                    join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                    ->where('hub_available_product_id',$hub_available_product->id)
                    ->get();
            }
        }

        return view('procurement::procurement-management.report.admin-summary-report',compact('per_hub_available_products', 'hubs'));
    }

    function get_each_hub_available_products(Request $request)
    {
        $per_hub_available_products = [];
        $data = [];
        $hub_id = $request->hub_id;
        $hub_available_products[] = HubAvailableProduct::with('hubAvailableProductDetails')->select('id')->where('hub_id', $hub_id)->get();

        foreach ($hub_available_products as $temp_hub_available_product) {
            foreach ($temp_hub_available_product as $hub_available_product) {
                $per_hub_available_products[] = HubAvailableProductDetails::
                join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                    ->where('hub_available_product_id',$hub_available_product->id)
                    ->get();
            }
        }

        foreach ($per_hub_available_products as $temp_data) {
            foreach ($temp_data as $item) {
                $data[] = $item;
            }
        }

        return $data;
    }

    public function get_admin_report(Request $request)
    {
        $get_data = [];
        $get_new_data = [];
        $new_single_data = [];
        $new_data = [];
        $uom_prices = [];
        $get_price_foreach_product = [];
        $purchase_order_details = [];
        $get_store_price_foreach_product = [];
        $get_price_single_product = null;
        $get_store_price_single_product = null;
        $product = null;
        if ($request->data['product_id'] === '0' && $request->data['hub_id'] === '0') {
            return view('procurement::procurement-management.report.admin-summary-report-view');
        }
        $hub_available_products = HubAvailableProduct::where('hub_id', $request->data['hub_id'])->get();
        $hub = Store::select('name')->where('id', $request->data['hub_id'])->where('status', 1)->first();

        // for hub only
        if ($request->data['product_id'] === 'Select Product') {
            foreach ($hub_available_products as $hub_available_product) {
                $get_data[] = HubAvailableProductDetails::
                join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                    ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                    ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                    ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                    ->join('uoms', 'products.purchase_uom_id', '=', 'uoms.id')
                    ->where('hub_available_product_id', $hub_available_product->id)
                    ->select('products.id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'hub_available_product_details.available_quantity', 'uoms.uom_name')
                    ->get();
            }
            foreach ($get_data as $available_product_details) {
                foreach ($available_product_details as $product_details) {
                    $get_new_data[] = $product_details;
                    if ($product_details->available_quantity !== 0) {
                        $get_price_foreach_product[] = UomPrice::select('id', 'product_id', 'uom_price')->where('product_id', $product_details->id)->get()->last();
                        $get_store_price_foreach_product[] = PurchaseOrderDetails::select('id', 'product_id', 'uom_price')->where('product_id', $product_details->id)->get()->last();
                    }
                }
            }
            $new_data = array_unique($get_new_data);
            $uom_prices = array_unique($get_price_foreach_product);
            $purchase_order_details = array_unique($get_store_price_foreach_product);
        }

        // if product selected for report
        if ($request->data['product_id'] !== '0' && $request->data['product_id'] !== 'Select Product') {
            $product = Product::select('product_name')->where('id', $request->data['product_id'])->first();
            foreach ($hub_available_products as $hub_available_product) {
                $new_single_data[] = HubAvailableProductDetails::
                    join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                    ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                    ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                    ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                    ->join('uoms', 'products.purchase_uom_id', '=', 'uoms.id')
                    ->where('product_id', $request->data['product_id'])
                    ->where('hub_available_product_id', $hub_available_product->id)
                    ->select('products.id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'hub_available_product_details.available_quantity', 'uoms.uom_name')
                    ->first();
            }
            foreach ($new_single_data as $single_data) {
                if (!empty($single_data) && $single_data->available_quantity !== 0) {
                    $get_price_single_product[] = UomPrice::select('id', 'product_id', 'uom_price')->where('product_id', $single_data->id)->get()->last();
                    $get_store_price_single_product[] = PurchaseOrderDetails::select('id', 'product_id', 'uom_price')->where('product_id', $single_data->id)->get()->last();
                }
            }
            $new_data = array_unique($new_single_data);
            $uom_prices = $get_price_single_product;
            $purchase_order_details = $get_store_price_single_product;
        }
        $data = $new_data;

        return view('procurement::procurement-management.report.admin-summary-report-view', compact('data', 'uom_prices', 'purchase_order_details', 'hub', 'product'));

    }
}
