<?php

namespace Modules\Procurement\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Procurement\Entities\StoreRequisitionDetails;
use Modules\Procurement\Http\Requests\StoreRequisitionFormRequest;
use Modules\Procurement\Http\Requests\UpdateStoreRequisitionFormRequest;
use Modules\Procurement\Repositories\StoreInferface;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Store\Entities\StoreUser;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{

    private $storeRepository;

    public function __construct(StoreInferface $storeRepository)
    {
        $this->storeRepository = $storeRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {


        $requisitions = $this->storeRepository->index();
        return view('procurement::procurement-management.store.index',compact('requisitions'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {

        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        $store_available_product = StoreAvailableProduct::where('store_id', $store->store_id)->first();
        $products = DB::table('store_available_product_details')->where('store_available_product_id',$store_available_product->id)->join('products', 'store_available_product_details.product_id', '=', 'products.id')->get();
        // $products = Product::select(['id', 'product_name'])->get();
        return view('procurement::procurement-management.store.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreRequisitionFormRequest $request)
    {
        $store =$this->storeRepository->store($request);
        if($store){
            return redirect()->route('procurestore.index')->with('success','Requisition create successfully');
        }else{
            return redirect()->route('procurestore.index')->withErrors("Sorry!Store User Not Found!");
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {

        $requisition_details = $this->storeRepository->show($id);
        return view('procurement::procurement-management.store.details',compact('requisition_details'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $products = Product::select(['id', 'product_name'])->get();
        $requisition_details = $this->storeRepository->edit($id);
        return view('procurement::procurement-management.store.edit',compact('requisition_details','products'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateStoreRequisitionFormRequest $request, $id)
    {
        $this->storeRepository->update($request, $id);
        return redirect()->route('procurestore.index')->with('success', 'Requisition update successfully');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Request $request)
    {
        //dd($request->all());
        $this->storeRepository->destroy($request);
        return redirect()->back()->with('success','Requisition deleted successfully');
    }

    // Get Product By ID

    public function getProductByID($id)
    {
        $products =$this->storeRepository->getProductByID($id);
        return response()->json($products);
    }

    // product save
    public function productSave(Request $request)
    {
        return $request;
    }


    public function requisitionDetails($id){
      $hold_request = StoreRequisition::join('hold_distribution_requests','store_requisitions.store_requisition_no','=','hold_distribution_requests.store_requisition_no')
            ->join('hold_distribution_request_details','hold_distribution_requests.id','=','hold_distribution_request_details.hold_distribution_request_id')
           ->join('products','hold_distribution_request_details.product_id','=','products.id')
           ->join('product_unit','hold_distribution_request_details.unit_id','=','product_unit.id')
           ->join('uoms','hold_distribution_request_details.uom_id','=','uoms.id')
           ->select('store_requisitions.store_requisition_no','products.product_name','product_unit.unit_name','uoms.uom_name','hold_distribution_request_details.hold_quantity','hold_distribution_requests.approximate_delivery_date')
           ->where('store_requisitions.store_requisition_no','=',$id)
           ->get();
        $regular_distribution = StoreRequisition::join('regular_distribution_requests','store_requisitions.store_requisition_no','=','regular_distribution_requests.store_requisition_no')
            ->join('regular_distribution_request_details','regular_distribution_requests.id','=','regular_distribution_request_details.regular_distribution_request_id')
            ->join('products','regular_distribution_request_details.product_id','=','products.id')
            ->join('product_unit','regular_distribution_request_details.unit_id','=','product_unit.id')
            ->join('uoms','regular_distribution_request_details.uom_id','=','uoms.id')
            ->select('store_requisitions.store_requisition_no','products.product_name','product_unit.unit_name','uoms.uom_name','regular_distribution_request_details.distribution_quantity')
            ->where('store_requisitions.store_requisition_no','=',$id)
            ->get();
       $purchase_qty = StoreRequisition::join('purchase_distribution_requests','store_requisitions.store_requisition_no','=','purchase_distribution_requests.store_requisition_no')
            ->join('purchase_distribution_request_details','purchase_distribution_requests.id','=','purchase_distribution_request_details.purchase_distribution_request_id')
            ->join('products','purchase_distribution_request_details.product_id','=','products.id')
            ->join('product_unit','purchase_distribution_request_details.unit_id','=','product_unit.id')
            ->join('uoms','purchase_distribution_request_details.uom_id','=','uoms.id')
            ->select('store_requisitions.store_requisition_no','products.product_name','product_unit.unit_name','uoms.uom_name','purchase_distribution_request_details.purchase_quantity')
            ->where('store_requisitions.store_requisition_no','=',$id)
            ->get();
        return view('procurement::procurement-management.store.requisition-details',compact('hold_request','regular_distribution','purchase_qty'));
    }
}
