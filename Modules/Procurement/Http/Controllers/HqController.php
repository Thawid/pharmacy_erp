<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\Http\Requests\HqFormRequest;
use Modules\Procurement\Repositories\HqInterface;
use Modules\Product\Entities\ProductVariation;
use Modules\Vendor\Entities\Vendor;

class HqController extends Controller
{


    protected $hqRequisition;

    public function __construct(HqInterface $hqRequisition){

        $this->hqRequisition = $hqRequisition;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

        $requisitions = $this->hqRequisition->index();
        return view('procurement::procurement-management.hq.index',compact('requisitions'));
    }

    public function approved_list(){
        $requisitions = $this->hqRequisition->index();
        return view('procurement::procurement-management.hq.approve-list',compact('requisitions'));
    }

    public function hq_requisition(){
        $hq_requisitions = $this->hqRequisition->hq_requisition();
        return view('procurement::procurement-management.hq.hq_requisition',compact('hq_requisitions'));
    }

    public function hq_approved_requisition(){
        $hq_requisitions = $this->hqRequisition->hq_requisition();
        return view('procurement::procurement-management.hq.hq-approve-requisition',compact('hq_requisitions'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('procurement::procurement-management.hq.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $hub = DB::table('hub_requisitions')
            ->join('stores','hub_requisitions.hub_id','=','stores.id')
            ->select('stores.name as store_name','stores.address','stores.phone_no','stores.email','stores.code')
            ->where('hub_requisitions.id',$id)
            ->first();
        $requisition_details = $this->hqRequisition->show($id);
        return view('procurement::procurement-management.hq.details',compact('requisition_details','hub'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $hub = DB::table('hub_requisitions')
            ->join('stores','hub_requisitions.hub_id','=','stores.id')
            ->select('stores.id as hub_id','stores.store_type','stores.name as store_name','stores.address','stores.phone_no','stores.email','stores.code')
            ->where('hub_requisitions.id',$id)
            ->first();

        $requisition_details = $this->hqRequisition->edit($id);
        //return $requisition_details;
        return view('procurement::procurement-management.hq.edit',compact('requisition_details','hub'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(HqFormRequest $request, $id)
    {

        $this->hqRequisition->update($request,$id);
        return redirect()->route('procurehq.index')->with('success','Requisition merge successfully');


    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    public function hq_req_details($id,$req_id){

        $vendor = DB::table('hq_approve_requisition_details')
            ->join('vendors','hq_approve_requisition_details.vendor_id','=','vendors.id')
            ->select('vendors.*')
            ->where('hq_approve_requisition_details.vendor_id',$id)
            ->first();
        $hq_req_details = $this->hqRequisition->hq_req_details($id,$req_id);
        return view('procurement::procurement-management.hq.req_details',compact('hq_req_details','vendor'));
    }

    public function approved_req_details($id){
        $hub = DB::table('hub_requisitions')
            ->join('stores','hub_requisitions.hub_id','=','stores.id')
            ->select('stores.name as store_name','stores.address','stores.phone_no','stores.email','stores.code')
            ->where('hub_requisitions.id',$id)
            ->first();
        $requisition_details = DB::table('hub_requisitions')
            ->join('hub_requisition_details','hub_requisitions.id','=','hub_requisition_details.hub_requisition_id')
            ->join('stores','hub_requisitions.hub_id','=','stores.id')
            ->join('products','hub_requisition_details.product_id','=','products.id')
            ->join('product_generics','hub_requisition_details.generic_id','=','product_generics.id')
            ->join('product_unit','hub_requisition_details.unit_id','=','product_unit.id')
            ->join('vendors','hub_requisition_details.vendor_id','=','vendors.id')
            ->join('uoms','hub_requisition_details.uom_id','=','uoms.id')
            ->select('hub_requisitions.*','hub_requisition_details.request_quantity','hub_requisition_details.status','products.product_name','product_generics.name as generic_name','vendors.name as vendor_name','product_unit.unit_name','uoms.uom_name')
            ->where('hub_requisition_id',$id)
            ->get();
        return view('procurement::procurement-management.hq.approved-details',compact('hub','requisition_details'));
    }

}
