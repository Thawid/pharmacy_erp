<?php

namespace Modules\Procurement\Http\Controllers;
use DB;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Procurement\Entities\HqRequisition;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Procurement\Http\Requests\PurchaseOrderRequest;
use Modules\Procurement\Repositories\PurchaseInterface;


class PurchaseController extends Controller
{

    private $purchaseRepositores;

    public function __construct(PurchaseInterface $purchaseRepositores)
    {
        $this->purchaseRepositores = $purchaseRepositores;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function order_pending_list()
    {
        $purchase_orders = $this->purchaseRepositores->order_pending_list();
        return view('procurement::procurement-management.purchase.pending_list', compact('purchase_orders'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function purchase_order_store(PurchaseOrderRequest $request)
    {
        $this->purchaseRepositores->purchase_order_store($request);
        return redirect()->route('hq.requisitions')->with('success', 'Purchase Order Processing Success');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function pending_order_details($id)
    {
        $hqrequisitons = $this->purchaseRepositores->show($id);
        return view('procurement::procurement-management.purchase.details', compact('hqrequisitons'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function purchase_order_create($id,$req_id)
    {
        $hqrequisitons = $this->purchaseRepositores->purchase_order_create($id,$req_id);
        return view('procurement::procurement-management.purchase.create', compact('hqrequisitons'));
    }

    // show purchase order list
    public function purchase_order_list()
    {
        $purchase_order_list = PurchaseOrder::with(['hq_requisition','vendor'])->orderBy('id', 'DESC')->get();
        return view('procurement::procurement-management.purchase.order_list', compact('purchase_order_list'));
    }

    // Purchase order Process
    public function purchase_order_edit($id)
    {

      $purchase_order = PurchaseOrder::with(['purchase_order_details','vendor', 'purchase_order_details.product','purchase_order_details.unit','purchase_order_details.uom'])
          ->findOrFail($id);
      return view('procurement::procurement-management.purchase.edit_order', compact('purchase_order'));
    }

    // purchase processing order details
    public function purchase_order_details($id)
    {
        $order_details = PurchaseOrder::with(['purchase_order_details','vendor', 'purchase_order_details.product', 'purchase_order_details.unit','purchase_order_details.uom'])->findOrFail($id);
        return view('procurement::procurement-management.purchase.order_details', compact('order_details'));
    }

    // purchase order update
    public function purchase_order_update(PurchaseOrderRequest $request, $id)
    {
        $this->purchaseRepositores->purchase_order_update($request,$id);
        return redirect()->route('purchase-orders.list')->with('success', 'Purchase Order Update Success');
    }
}
