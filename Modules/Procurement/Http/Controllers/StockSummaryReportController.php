<?php

namespace Modules\Procurement\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductUom;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;

class StockSummaryReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $store = StoreUser::where('user_id', auth()->user()->id)
            ->where('store_type', '=','hub')
            ->where('status', 1)
            ->first();
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $get_hub = '';
        }else{
            $get_hub = Store::select('id')->where('id', $store->store_id)->firstOrFail();
        }
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $products = DB::table('hub_available_products')
                ->join('hub_available_product_details', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                ->select('products.id', 'products.product_name')
                ->get();
        }else{
            $products = DB::table('hub_available_products')
                ->join('hub_available_product_details', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                ->where('hub_available_products.hub_id', $get_hub->id)
                ->select('products.id', 'products.product_name')
                ->get();
        }
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $manufacturers = DB::table('hub_available_products')
                ->join('hub_available_product_details', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->select('manufacturers.id', 'manufacturers.name')
                ->get();
        }else{
            $manufacturers = DB::table('hub_available_products')
                ->join('hub_available_product_details', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->where('hub_available_products.hub_id', $get_hub->id)
                ->select('manufacturers.id', 'manufacturers.name')
                ->get();
        }
        $manufacturers = $manufacturers->unique('id');
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $productTypes = DB::table('hub_available_products')
                ->join('hub_available_product_details', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                ->select('product_types.id', 'product_types.name')
                ->get();
        }else{
            $productTypes = DB::table('hub_available_products')
                ->join('hub_available_product_details', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'hub_available_product_details.product_id', '=', 'products.id')
                ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                ->where('hub_available_products.hub_id', $get_hub->id)
                ->select('product_types.id', 'product_types.name')
                ->get();
        }
        $productTypes = $productTypes->unique('product_types.id');
        return view('procurement::procurement-management.report.hub-stock-product-report',compact('products','manufacturers','productTypes'));
    }
    public function get_product_stock_summary_report(Request $request){

        $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','hub')->value('store_id');
        $product_id = $request->data['product_id'];
        //$product_type_id = $request->data['product_type_id'];
        $manufacturer_id = $request->data['manufacturer_id'];

        $product_name = $request->data['product_name'];
        //$product_type = $request->data['product_type'];
        $manufacture_name = $request->data['manufacture_name'];

        $start_date = $request->data['start_date'];
        $start_date = $start_date . ' 00:00:00';
        $end_date = $request->data['end_date'];

        $date_one = date_create($start_date);

        $show_date_one = date_format($date_one, 'd-m-Y');

        if (empty($end_date)) {
            $show_date_two = '';
        } else {
            $date_two = date_create($end_date);
            $show_date_two = date_format($date_two, 'd-m-Y');
        }
        if (empty($end_date)) {
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date . ' 23:59:59';

        if($product_id != ''){
        $data = DB::table('hub_available_product_details')
            ->join('hub_available_products','hub_available_products.id','=','hub_available_product_details.hub_available_product_id')
            ->join('products','hub_available_product_details.product_id','=','products.id')
            ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
            //->join('product_types','products.product_type_id','=','product_types.id')
            ->join('product_generics','products.product_generic_id','=','product_generics.id')
            ->join('uoms','products.purchase_uom_id','=','uoms.id')
            ->select('hub_available_product_details.product_id','manufacturers.id','hub_available_products.hub_id','products.product_name','product_generics.name as generic_name','manufacturers.name as manufacturer_name','hub_available_product_details.available_quantity','uoms.uom_name')
            ->whereBetween('hub_available_product_details.created_at', [$start_date, $end_date])
            ->orWhere('products.id',$product_id)
            ->where('hub_available_products.hub_id', $hub_id)
            ->orWhere('manufacturers.id',$manufacturer_id)
            //->orWhere('product_types.id',$product_type_id)
            ->get();
        }else if($product_id == null) {
            $data = DB::table('hub_available_product_details')
                ->join('hub_available_products', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'products.id', '=', 'hub_available_product_details.product_id')
                ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
                ->join('product_types','products.product_type_id','=','product_types.id')
                ->join('product_generics','products.product_generic_id','=','product_generics.id')
                ->join('uoms','hub_available_product_details.uom_id','=','uoms.id')
                ->select('hub_available_product_details.product_id','manufacturers.id','hub_available_products.hub_id','products.product_name','product_generics.name as generic_name','manufacturers.name as manufacturer_name','hub_available_product_details.available_quantity','uoms.uom_name')
                ->where('hub_available_products.hub_id', $hub_id)
                ->get();
        }

        //dd($data);
        return view('procurement::procurement-management.report.hub-stock-product-report-view',compact('data','show_date_one','show_date_two','product_name','manufacture_name'));

    }


    public function hub_product_expiry_report(){

       $store = StoreUser::where('user_id', auth()->user()->id)
           ->where('store_type', '=','hub')
           ->where('status', 1)
           ->first();
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $get_hub = '';
        }else{
            $get_hub = Store::select('id')->where('id', $store->store_id)->firstOrFail();
        }
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $products = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->select('products.id', 'products.product_name', 'hub_stock_entry_details.product_id')
                ->get();
        }else{
            $products = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->where('hub_stock_entries.hub_id', $get_hub->id)
                ->select('products.id', 'products.product_name', 'hub_stock_entry_details.product_id')
                ->get();
        }

         $products = $products->unique('product_id');

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $manufacturers = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->select('manufacturers.id', 'manufacturers.name')
                ->get();
        }else{
            $manufacturers = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->where('hub_stock_entries.hub_id', $get_hub->id)
                ->select('manufacturers.id', 'manufacturers.name')
                ->get();
        }
        $manufacturers = $manufacturers->unique('id');

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $productCategory = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                ->select('product_categories.id', 'product_categories.name')
                ->get();
        }else{
            $productCategory = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                ->where('hub_stock_entries.hub_id', $get_hub->id)
                ->select('product_categories.id', 'product_categories.name')
                ->get();
        }
        $productCategory = $productCategory->unique('id');
        return view('procurement::procurement-management.report.hub-product-expiry-report',compact('products','manufacturers','productCategory'));
    }


    public function get_product_expiry_report(Request $request){
        //dd($request->data);
        $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','hub')->value('store_id');
        $warehouse_id = Store::where('hub_id',$hub_id)->value('warehouse_id');

        $product_id = $request->data['product_id'];
        $product_category_id = $request->data['product_category_id'];
        $manufacturer_id = $request->data['manufacturer_id'];
        $expire_within = $request->data['expire_within'];

        $product_name = $request->data['product_name'];
        $product_category = $request->data['product_category'];
        $manufacture_name = $request->data['manufacture_name'];
        $expire_with_in = $request->data['expire_with_in'];

        $start_date = $request->data['start_date'];
        $start_date = $start_date . ' 00:00:00';
        $end_date = $request->data['end_date'];

        $date_one = date_create($start_date);

        $show_date_one = date_format($date_one, 'd-m-Y');

        if (empty($end_date)) {
            $show_date_two = '';
        } else {
            $date_two = date_create($end_date);
            $show_date_two = date_format($date_two, 'd-m-Y');
        }
        if (empty($end_date)) {
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date . ' 23:59:59';
        $startDate = Carbon::today();
        $endDate = Carbon::today()->addDays($expire_within);

       if($product_id != ''){
           $data = DB::table('hub_stock_entry_details')
               ->join('hub_stock_entries','hub_stock_entries.id','=','hub_stock_entry_details.hub_stock_entry_id')
               ->join('products','hub_stock_entry_details.product_id','=','products.id')
               ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
               ->join('product_categories','products.product_category_id','=','product_categories.id')
               ->join('product_generics','products.product_generic_id','=','product_generics.id')
               ->select('hub_stock_entry_details.batch_no','products.product_name','product_generics.name as generic_name','manufacturers.name as manufacturer_name','hub_stock_entry_details.expiry_date','hub_stock_entry_details.sku')
               ->whereBetween('hub_stock_entry_details.expiry_date', [$start_date, $end_date])
               ->orWhereBetween('hub_stock_entry_details.expiry_date', [$startDate, $endDate])
               ->orWhere('products.id',$product_id)
               ->where('hub_stock_entry_details.warehouse_id',$warehouse_id)
               ->orWhere('manufacturers.id',$manufacturer_id)
               ->orWhere('product_categories.id',$product_category_id)
               ->get();
       }else if($product_id == null){

           $data = DB::table('hub_stock_entry_details')
               ->join('hub_stock_entries','hub_stock_entries.id','=','hub_stock_entry_details.hub_stock_entry_id')
               ->join('products','hub_stock_entry_details.product_id','=','products.id')
               ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
               ->join('product_categories','products.product_category_id','=','product_categories.id')
               ->join('product_generics','products.product_generic_id','=','product_generics.id')
               ->select('hub_stock_entry_details.batch_no','products.product_name','product_generics.name as generic_name','manufacturers.name as manufacturer_name','hub_stock_entry_details.expiry_date','hub_stock_entry_details.sku')
               ->where('hub_stock_entry_details.warehouse_id',$warehouse_id)
               ->get();
       }
        //dd($data);
        return view('procurement::procurement-management.report.hub-product-expiry-report-view',compact('data','show_date_one','show_date_two','product_name','manufacture_name','product_category'));
    }

    public function hub_product_expiry_alert_report(){

        $get_hub = StoreUser::select('store_id')
            ->where('store_type','hub')
            ->where('user_id',auth()->user()->id)
            ->first();
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $products = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->select('products.id', 'products.product_name', 'hub_stock_entry_details.product_id')
                ->get();
        }else{
            $products = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->where('hub_stock_entries.hub_id', $get_hub->store_id)
                ->select('products.id', 'products.product_name', 'hub_stock_entry_details.product_id')
                ->get();
        }
        $products = $products->unique('product_id');
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $manufacturers = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->select('manufacturers.id', 'manufacturers.name')
                ->get();
        }else{
            $manufacturers = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->where('hub_stock_entries.hub_id', $get_hub->store_id)
                ->select('manufacturers.id', 'manufacturers.name')
                ->get();
        }
        $manufacturers = $manufacturers->unique('id');
        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $productCategory = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                ->select('product_categories.id', 'product_categories.name')
                ->get();
        }else{
            $productCategory = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                ->where('hub_stock_entries.hub_id', $get_hub->store_id)
                ->select('product_categories.id', 'product_categories.name')
                ->get();
        }
        $productCategory = $productCategory->unique('id');
        return view('procurement::procurement-management.report.hub-product-expiry-alert-report',compact('products','manufacturers','productCategory'));
    }

    public function get_hub_product_expiry_alert_report(Request $request){

        //$hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','hub')->value('store_id');
        $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','hub')->value('store_id');
        $warehouse_id = Store::where('hub_id',$hub_id)->value('warehouse_id');
        $product_id = $request->data['product_id'];
        $product_category_id = $request->data['product_category_id'];

        $expire_within = $request->data['expire_within'];

        $product_name = $request->data['product_name'];
        $product_category = $request->data['product_category'];

        $expire_with_in = $request->data['expire_with_in'];

        $startDate = Carbon::today();
        $endDate = Carbon::today()->addDays($expire_within);
        //$now = Carbon::today()->addDays($expire_within);
        if($product_id != '') {
            $data = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                ->select('hub_stock_entry_details.batch_no','hub_stock_entries.hub_id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'hub_stock_entry_details.expiry_date', 'hub_stock_entry_details.sku')
                ->whereBetween('hub_stock_entry_details.expiry_date', [$startDate, $endDate])
                ->orWhere('products.id', $product_id)
                ->where('hub_stock_entry_details.warehouse_id', $warehouse_id)
                ->orWhere('product_categories.id', $product_category_id)
                ->get();
        }else if($product_id == null){
            $data = DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->join('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                ->join('product_generics', 'products.product_generic_id', '=', 'product_generics.id')
                ->select('hub_stock_entry_details.batch_no','hub_stock_entries.hub_id', 'products.product_name', 'product_generics.name as generic_name', 'manufacturers.name as manufacturer_name', 'hub_stock_entry_details.expiry_date', 'hub_stock_entry_details.sku')
                ->whereBetween('hub_stock_entry_details.expiry_date', [$startDate, $endDate])
                ->where('hub_stock_entry_details.warehouse_id', $warehouse_id)
                ->get();
        }
        //dd($data);
        return view('procurement::procurement-management.report.hub-product-expiry-alert-report-view',compact('data','product_name','product_category'));
    }


    public function store_summary_report(){

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $stores = Store::select('id', 'name')
                ->where('store_type', 'store')
                ->get();
        } else {
            $hub_user = StoreUser::select('store_id')->where('user_id', auth()->user()->id)->where('store_type', 'hub')->first();
            $stores = Store::select('id', 'name')
                ->where('hub_id', $hub_user->store_id)
                ->where('store_type', 'store')
                ->get();
        }

        $products = [];
        $data = [];
        foreach ($stores as $store) {
           $products[] = StoreAvailableProduct::join('store_available_product_details', 'store_available_products.id', '=', 'store_available_product_details.store_available_product_id')
                ->join('products', 'store_available_product_details.product_id', '=', 'products.id')
                ->select('products.id', 'products.product_name')
                ->where('store_available_products.store_id',$store->id)
                ->get();

        }

        foreach ($products as $product){
            foreach ($product as $product_id){
                $data[] = $product_id;
            }
        }
        $store_products = array_unique($data);

        $manufacturers = DB::table('store_available_products')
            ->join('store_available_product_details','store_available_products.id','=','store_available_product_details.store_available_product_id')
            ->join('products','store_available_product_details.product_id','=','products.id')
            ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
            ->select('manufacturers.id','manufacturers.name')
            ->get();

        $manufacturers = $manufacturers->unique('id');

        $productTypes = DB::table('store_available_products')
            ->join('store_available_product_details','store_available_products.id','=','store_available_product_details.store_available_product_id')
            ->join('products','store_available_product_details.product_id','=','products.id')
            ->join('product_types','products.product_type_id','=','product_types.id')
            ->select('product_types.id','product_types.name')
            ->get();

        $productTypes = $productTypes->unique('id');

        return view('procurement::procurement-management.report.store-product-summary-report',compact('store_products','manufacturers','productTypes','stores'));
    }


    public function get_store_product_summary_report(Request $request){
        //dd($request->data);

        $store_id = $request->data['store_id'];
        $product_id = $request->data['product_id'];
        $product_type_id = $request->data['product_type_id'];
        $manufacturer_id = $request->data['manufacturer_id'];

        $product_name = $request->data['product_name'];
        $product_type = $request->data['product_type'];
        $manufacture_name = $request->data['manufacture_name'];

        $start_date = $request->data['start_date'];
        $start_date = $start_date . ' 00:00:00';
        $end_date = $request->data['end_date'];

        $date_one = date_create($start_date);

        $show_date_one = date_format($date_one, 'd-m-Y');

        if (empty($end_date)) {
            $show_date_two = '';
        } else {
            $date_two = date_create($end_date);
            $show_date_two = date_format($date_two, 'd-m-Y');
        }
        if (empty($end_date)) {
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date . ' 23:59:59';

        $data = DB::table('store_available_products')
            ->join('store_available_product_details','store_available_products.id','=','store_available_product_details.store_available_product_id')
            ->join('products','store_available_product_details.product_id','=','products.id')
            ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
            ->join('product_types','products.product_type_id','=','product_types.id')
            ->join('product_generics','products.product_generic_id','=','product_generics.id')
            ->join('stores','store_available_products.store_id','=','stores.id')
            ->join('uoms','products.purchase_uom_id','=','uoms.id')
            ->whereBetween('store_available_product_details.created_at', [$start_date, $end_date])
            ->orWhere('products.id',$product_id)
            ->orWhere('manufacturers.id',$manufacturer_id)
            ->orWhere('product_types.id',$product_type_id)
            ->orWhere('stores.id',$store_id)
            ->select('products.product_name','product_generics.name as generic_name','manufacturers.name as manufacturer_name','store_available_product_details.available_quantity','uoms.uom_name')
            ->get();
        //dd($data);
        return view('procurement::procurement-management.report.store-product-summary-report-view',compact('data','show_date_one','show_date_two','product_name','manufacture_name','product_type'));
    }



    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('procurement::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    public function available_products_report()
    {
        $store_user = StoreUser::where('user_id',auth()->user()->id)->where('store_type','store')->first();
        if($store_user != null){

            $user_store_id = $store_user->store_id;
            $store_available_products = DB::table('store_available_products')
            ->join('store_available_product_details','store_available_products.id','=','store_available_product_details.store_available_product_id')
            ->join('products','store_available_product_details.product_id','=','products.id')
            ->select('store_available_product_details.product_id as id','products.product_name')
            ->where('store_available_products.store_id',$user_store_id)
            ->get();


            return view('procurement::procurement-management.report.available-products',compact('store_available_products'));

        }else{
            return "hub ro admin";
        }






    }

    public function get_uom_by_product_id(Request $request)
    {
        $product_id =  $request->product_id;
        $uoms = DB::table('product_uoms')
        ->join('uoms','uoms.id','=','product_uoms.uom_id')
        ->select('product_uoms.uom_id','uoms.uom_name')
        ->where('product_uoms.product_id',$product_id)
        ->get();

        return response()->json([
            'uoms' => $uoms
        ]);
    }

    public function get_available_products_report(Request $request)
    {
        $store_user = StoreUser::where('user_id',auth()->user()->id)->where('store_type','store')->first();
        $user_store_id = $store_user->store_id;


            $product_id = $request->data['product_id'];
            $product_uom_id = $request->data['product_uom_id'];
            $store_available_product = DB::table('store_available_products')
            ->join('store_available_product_details','store_available_product_details.store_available_product_id','=','store_available_products.id')
            ->join('products','store_available_product_details.product_id','=','products.id')
            ->join('product_generics','product_generics.id','=','products.product_generic_id')
            ->join('manufacturers','manufacturers.id','=','products.manufacturer_id')
            ->join('uoms','uoms.id','=','store_available_product_details.uom_id')


            ->select('products.product_name','product_generics.name as generic_name','manufacturers.name as manufacturer','store_available_product_details.available_quantity')
            ->where('store_available_products.store_id','=',$user_store_id)
            ->where('store_available_product_details.product_id','=',$product_id)
            ->first();



            $product_uom_quantity = DB::table('product_uoms')
            ->join('uoms','uoms.id','=','product_uoms.uom_id')
            ->select('product_uoms.quantity_per_uom','uoms.uom_name')
            ->where('product_uoms.product_id',$product_id)
            ->where('product_uoms.uom_id',$product_uom_id)
            ->first();



            return view('procurement::procurement-management.report.available-products-view',compact('store_available_product','product_uom_quantity'));

    }
}
