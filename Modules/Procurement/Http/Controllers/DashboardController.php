<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Routing\Controller;
use DB;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Store\Entities\StoreUser;

class DashboardController extends Controller
{
    public function index()
    {
        $userId = auth()->user()->id;
        $store_id = StoreUser::where('store_type', '=', "store")->where('user_id', $userId)->value('store_id');
        $hub_id = StoreUser::where('store_type', '=', "hub")->where('user_id', $userId)->value('store_id');
        $latest_store_requisitions = StoreRequisition::where('store_id', $store_id)->latest()->take(5)->get();
        $latest_purchase_orders = PurchaseOrder::where('hub_id', $hub_id)->latest()->take(5)->get(['purchase_order_date','requested_delivery_date','status','total']);
        $total_store_requisitions = DB::table('store_requisitions')->where('store_requisition_no', '!=', null)->count();
        $total_hub_requisitions = DB::table('hub_requisitions')->where('hub_requisition_no', '!=', null)->count();
        $total_purchase_amount = DB::table('purchase_orders')->select(DB::raw('sum(total) as total_amount'))->get();
        $total_purchase_order = DB::table('purchase_orders')->where('total', '!=', null)->count();
        $monthly_expense = DB::table('purchase_orders')->select(
            DB::raw('LEFT(MONTHNAME(created_at),3) as month'),
            DB::raw('sum(other_expense) as expenses'))
            ->groupBy('month')
            ->get();

        $store_wise_requisitions =  DB::table('store_requisitions')
            ->join('stores', 'stores.id','=', 'store_requisitions.store_id')
            ->select('stores.name','store_requisitions.store_id')
            ->where('store_requisitions.store_type', "store")
            ->groupBy('store_requisitions.store_id')
            ->get();
        return view('procurement::dashboard',
            compact('total_store_requisitions',
                'total_hub_requisitions',
                'total_purchase_amount',
                'total_purchase_order',
                'monthly_expense','latest_store_requisitions',
                'latest_purchase_orders','store_wise_requisitions')
        );
    }
}
