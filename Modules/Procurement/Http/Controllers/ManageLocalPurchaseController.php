<?php

namespace Modules\Procurement\Http\Controllers;

use Modules\Procurement\Entities\LocalPurchase;
use Modules\Procurement\Entities\LocalPurchaseDetails;
use Modules\Product\Entities\ProductUnit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Store\Entities\StoreUser;

class ManageLocalPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $store = StoreUser::where('user_id', auth()->user()->id)->where('store_type','store')->where('status', 1)->first();
        $local_purchase = LocalPurchase::with(['details','details.product_uom','details.product','details.unit','details.generic'])->where('store_id',$store->store_id)->get();

        return view('procurement::procurement-management.manage-local-purchase.index', compact('local_purchase'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $local_purchase_details = DB::table('local_purchase_details')->where('local_purchase_id', $id)
            ->join('local_purchases', 'local_purchase_details.local_purchase_id', '=', 'local_purchases.id')
            ->join('product_generics', 'local_purchase_details.generic_id', '=', 'product_generics.id')
            ->join('products', 'local_purchase_details.product_id', '=', 'products.id')->get();

        return view('procurement::procurement-management.manage-local-purchase.details', compact('local_purchase_details'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $store = StoreUser::where('user_id', auth()->user()->id)->first();
        $units = ProductUnit::where('status', 1)->get();
        $store_available_product = StoreAvailableProduct::where('store_id', $store->store_id)->first();
        $products = DB::table('store_available_product_details')->where('store_available_product_id',$store_available_product->id)->join('products', 'store_available_product_details.product_id', '=', 'products.id')->get();
        $uoms = DB::table('uoms')->get();
        $local_purchase = LocalPurchase::with(['details','details.product_uom','details.product','details.unit','details.generic','store'])->where('id', $id)->first();
        $local_purchase_details = $local_purchase->details;

        return view('procurement::procurement-management.manage-local-purchase.edit',compact('local_purchase', 'local_purchase_details', 'products', 'uoms', 'units'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return null
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            if (!empty($request->product)) {

                $new_local_purchase_ids = $request->purchase_details_ids;
                $product_id = $request->product;
                $generic_id = $request->generic_id;
                $unit_id = $request->unit_id;
                $quantity = $request->purchase_qty;
                $uom_id = $request->uom_id;
                LocalPurchase::where('id', $id)->update(['purchase_date' => $request->purchase_date]);

                // delete section
                $old_old_ids = [];
                $old_local_purchase_details_ids = DB::table('local_purchase_details')->where('local_purchase_id', $id)->get('id');
                foreach ($old_local_purchase_details_ids as $old_local_purchase_details) {
                    $old_old_ids[] = $old_local_purchase_details->id;
                }
                $deletable_local_purchase_ids = array_diff($old_old_ids, $new_local_purchase_ids);
                if (count($deletable_local_purchase_ids) != 0) {
                    foreach ($deletable_local_purchase_ids as $deletable_local_purchase_id) {
                        DB::table('local_purchase_details')->where('id', $deletable_local_purchase_id)->delete();
                    }
                }

                // update local purchase details
                foreach ($product_id as $key => $product) {
                    $check_local_purchase = LocalPurchaseDetails::where('local_purchase_id', $id)->where('product_id', $product_id[$key])->update([
                        'local_purchase_id' => $id,
                        'product_id' => $product_id[$key],
                        'generic_id' => $generic_id[$key],
                        'unit_id' => $unit_id[$key],
                        'uom_id' => $uom_id[$key],
                        'purchase_qty' => $quantity[$key],
                        'status' => 'Pending',
                    ]);

                    if ($check_local_purchase === 0) {
                        $new_local_purchase_details = [
                            'local_purchase_id' => $id,
                            'product_id' => $product_id[$key],
                            'generic_id' => $generic_id[$key],
                            'unit_id' => $unit_id[$key],
                            'uom_id' => $uom_id[$key],
                            'purchase_qty' => $quantity[$key],
                            'status' => 'Pending',
                            'created_at' => Carbon::now(),
                        ];
                        LocalPurchaseDetails::insert($new_local_purchase_details);
                    }
                }
                DB::commit();
                return redirect()->route('manage-local-purchase.index')->with('success', 'Local purchase update successful!');
            }else {
                return redirect()->back()->withErrors("Local Purchase can not be empty.");
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors("Local purchase does not updated!" . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return null
     */
    public function destroy($id)
    {
        $local_perchase = LocalPurchase::find($id);
        $local_perchase->delete();

        return redirect()->back();
    }

}
