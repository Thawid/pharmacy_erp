<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->prefix('procurement')->group(function() {


    /* Hub All Route */
    Route::get('/procurement-dashboard', 'DashboardController@index')->name('procurement.dashboard');

    Route::get('/hub-requisition/pending','HubController@requisition_pending_list')->name('hub.requisition.pending.list')->middleware('hub');
    Route::get('/hub-requisition/pending/details/{id}','HubController@requisition_pending_details')->name('hub.requisition.pending.details')->middleware('hub');
    Route::get('/hub-requisition/pending/modify/{id}','HubController@requisition_pending_modify')->name('hub.requisition.pending.modify')->middleware('hub');
    Route::post('/hub-requisition/pending/merge','HubController@pending_requisition_merge')->name('hub.requisition.pending.merge')->middleware('hub');


    Route::get('/hub-requisition/process','HubController@requisition_process_list')->name('hub.requisition.process.list')->middleware('hub');
    Route::get('/hub-requisition/process/details/{id}','HubController@requisition_process_details')->name('hub.requisition.process.details')->middleware('hub');
    Route::get('/hub-requisition/process/add/{id}','HubController@requisition_process_add')->name('hub.requisition.process.add')->middleware('hub');
    Route::get('/hub-requisition/process/addAll','HubController@requisition_process_addAll')->name('hub.requisition.process.addAll')->name('hub.requisition.process.addAll')->middleware('hub');
    Route::get('/hub-requisition/process/confirm/','HubController@requisition_process_confirm_view')->name('hub.requisition.process.confirm.view')->middleware('hub');
    Route::post('/hub-requisition/process/confirm/store','HubController@requisition_process_confirm_store')->name('hub.requisition.process.confirm.store')->middleware('hub');


    Route::get('/hub-requisition','HubController@requisition_list')->name('hub.requisition.list')->middleware('hub');
    Route::get('/hub-requisition/details/{id}','HubController@requisition_details')->name('hub.requisition.details')->middleware('hub');

    Route::resource('procurehub', 'HubController')->middleware('hub');

    /* Hub All Route */


    Route::resource('procurehq', 'HqController')->middleware('hq');
    Route::get('approved-list', 'HqController@approved_list')->name('approved.list')->middleware('hq');
    Route::get('approved-details/{id}', 'HqController@approved_req_details')->name('approved.details')->middleware('hq');
    Route::resource('procurestore', 'StoreController')->middleware('store');
    Route::get('/get-product/{id}','StoreController@getProductByID')->middleware('storeOrHub');
    Route::post('/product/save','StoreController@productSave')->middleware('store');

    Route::get('requisitionDetails/{id}','StoreController@requisitionDetails')->middleware('store');


    Route::get('/hq-requisition','HqController@hq_requisition')->name('hq.requisitions')->middleware('hq');
    Route::get('/hq-req-details/{id}/{req_id}','HqController@hq_req_details')->name('hqReq.Details')->middleware('hq');

    Route::get('/hq-approved-requisition','HqController@hq_approved_requisition')->name('hq.approved.requisition')->middleware('hq');

    Route::get('/purchase-order/pending','PurchaseController@order_pending_list')->name('purchase-orders.pending.list')->middleware('hq');

    Route::get('/purchase-order/process','PurchaseController@purchase_order_list')->name('purchase-orders.list')->middleware('hq');

    Route::get('/purchase-order/create/{id}/{req_id}','PurchaseController@purchase_order_create')->name('purchase-order.create')->middleware('hq');

    Route::get('/pending/order/details/{id}','PurchaseController@pending_order_details')->name('purchase-orders.pending.details')->middleware('store');

    Route::post('/purchase-order/store','PurchaseController@purchase_order_store')->name('purchase-orders.store')->middleware('hq');

    Route::get('/purchase-order/edit/{id}','PurchaseController@purchase_order_edit')->name('purchase.orders.edit')->middleware('hq');

    Route::get('/purchase-order/details/{id}','PurchaseController@purchase_order_details')->name('purchase.orders.details')->middleware('hq');

    Route::post('/purchase-order/update/{id}','PurchaseController@purchase_order_update')->name('purchase-order.update')->middleware('store');

    Route::get('/store-report','ProcurementReportController@index')->middleware('hub');
    Route::post('/get-store-data','ProcurementReportController@get_store_report')->name('get.store.data')->middleware('hub');
    Route::get('/purchase-report','ProcurementReportController@show_purchase_report')->middleware('hq');
    Route::post('/get-purchase-report','ProcurementReportController@get_purchase_report')->name('get.purchase.report')->middleware('hq');

    Route::get('summary-report-pdf-view','ProcurementReportController@summary-report-pdf-view');
    Route::get('download-pdf','ProcurementReportController@downloadPdf')->name('employee.list.pdf');
    
    Route::get('/hub-stock-product-report','StockSummaryReportController@index');
    Route::post('/get-product-stock-summary-report','StockSummaryReportController@get_product_stock_summary_report')->name('get.product.stock.summary.report');
    //Store Stock Summary Report
    Route::get('/store-stock-product-report','StockSummaryReportController@index');    
    Route::post('/get-product-stock-summary-report','StockSummaryReportController@get_product_stock_summary_report')->name('get.product.stock.summary.report');

    Route::get('/hub-product-expiry-report','StockSummaryReportController@hub_product_expiry_report');
    Route::post('/get-product-expiry-report','StockSummaryReportController@get_product_expiry_report')->name('get.product.expiry.report');

    Route::get('/hub-product-expiry-alert-report','StockSummaryReportController@hub_product_expiry_alert_report')->middleware('hub');
    Route::post('/get-hub-product-expiry-alert-report','StockSummaryReportController@get_hub_product_expiry_alert_report')->name('get.hub.product.expiry.alert.report')->middleware('hub');

    Route::get('/store-product-summary-report','StockSummaryReportController@store_summary_report')->middleware('hub');
    Route::post('/get-store-product-summary-report','StockSummaryReportController@get_store_product_summary_report')->name('get.store.product.summary.report')->middleware('hub');

    Route::get('/reports/available-products-report','StockSummaryReportController@available_products_report');
    Route::get('/reports/get-uom-by-product-id','StockSummaryReportController@get_uom_by_product_id')->name('get.uom.by.product.id');
    Route::post('/reports/get-available-products-report','StockSummaryReportController@get_available_products_report')->name('get.available.product.report');

    Route::resource('local-purchase', 'LocalPurchaseController')->middleware('store');
    Route::post('/local-purchase/update/{id}', 'LocalPurchaseController@local_purchase_update')->name('local.purchase.update')->middleware('store');
    Route::resource('manage-local-purchase', 'ManageLocalPurchaseController')->middleware('store');

    Route::get('hub-summary-report', 'HubSummaryReportController@index')->name('admin-summary-report.index')->middleware('hub');
    Route::post('get-hub-summary-report','HubSummaryReportController@get_hub_report')->name('get-hub-summary-report')->middleware('hub');

    Route::get('admin-summary-report', 'AdminSummaryReportController@index')->middleware('system')->name('admin-summary-report');
    Route::get('admin-summary-report/get-hub-available-products', 'AdminSummaryReportController@get_each_hub_available_products')->middleware('system')->name('admin-summary-report.get-hub-available-products');
    Route::post('get-admin-summary-report','AdminSummaryReportController@get_admin_report')->middleware('system')->name('get-admin-summary-report');

    Route::get('store-summary-report', 'StoreSummaryReportController@index')->middleware('store')->name('store-summary-report');
    Route::post('get-store-summary-report','StoreSummaryReportController@get_store_report')->middleware('store')->name('get-store-summary-report');
    Route::get('admin-summary-report', 'AdminSummaryReportController@index')->name('admin-summary-report');
    Route::get('admin-summary-report/get-hub-available-products', 'AdminSummaryReportController@get_each_hub_available_products')->name('admin-summary-report.get-hub-available-products');
    Route::post('get-admin-summary-report','AdminSummaryReportController@get_admin_report')->name('get-admin-summary-report');

});
