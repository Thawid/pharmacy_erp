<?php

namespace Modules\Procurement\Providers;

use Illuminate\Support\ServiceProvider;

use Modules\Procurement\Repositories\HqInterface;
use Modules\Procurement\Repositories\HqRepository;
use Modules\Procurement\Repositories\HubInterface;
use Modules\Procurement\Repositories\HubRepository;

use Modules\Procurement\Repositories\ProcurementReportInterface;
use Modules\Procurement\Repositories\ProcurementReportRepository;
use Modules\Procurement\Repositories\PurchaseInterface;
use Modules\Procurement\Repositories\PurchaseRepository;
use Modules\Procurement\Repositories\StoreInferface;
use Modules\Procurement\Repositories\StoreRepository;

class ProcurementRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StoreInferface::class, StoreRepository::class);
        $this->app->bind(HubInterface::class,HubRepository::class);
        $this->app->bind(HqInterface::class,HqRepository::class);
        $this->app->bind(PurchaseInterface::class,PurchaseRepository::class);
        $this->app->bind(ProcurementReportInterface::class,ProcurementReportRepository::class);

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
