@extends('dboard.index')

@section('title','Requisition Details | HQ')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading">Requisition Details (HQ)</h2>
                        </div>
                        <div class="col-md-3 text-md-right">
                            <a class="btn index-btn" href={{route('hq.approved.requisition')}}>Approved List</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <div class="vendor-info">
                        <div class="row">
                            @if(isset($vendor))
    
                                <div class="col-md-6 text-left">
                                    <p><strong>Vendor Name :  {{ $vendor->name }}</strong></p>
                                    <p><strong>Phone : {{ $vendor->phone_number }}</strong></p>
                                    <p><strong>Email : {{ $vendor->email }}</strong></p>
                                </div>
                                <div class="col-md-6 text-md-left">
                                    <p><strong>Origin : {{ $vendor->origin }}</strong></p>
                                    <p><strong>Address : {{ $vendor->address }}</strong></p>
                                </div>
                            @endif
                        </div><!-- end.row -->
                    </div><!-- end.vendor-info -->

                    <!-- form -->
                    <form  method="POST" action="" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Generic Name</th>
                                            <th>Unit</th>
                                            <th>Quantity</th>
                                            <th>UOM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($hq_req_details))
                
                                            @foreach($hq_req_details as  $requisition)
                                                <tr>
                                                    <td><input type="text" class="form-control"  value="{{ $requisition->product_name }}" readonly></td>
                                                    <td><input class="form-control" type="text" value="{{ $requisition->generic_name }}" readonly></td>
                
                                                    <td><input type="text" class="form-control" value="{{$requisition->unit_name}}" readonly></td>
                                                    <td><input class="form-control" type="number"  value="{{$requisition->purchase_quantity}}" readonly></td>
                                                    <td><input class="form-control" type="text"  value="{{$requisition->uom_name}}" readonly></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table><!-- end.table -->
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </form><!-- end.form -->
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
@endsection


@push('post_scripts')

@endpush

