@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')
    <style>

    </style>
@endpush
@section('dboard_content')


    <div class="row">
        <div class="col-md-12 ">
            {{-- <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('procurement/procurehq') ? 'active' : '' }} " aria-current="page" href="{{route('procurehq.index')}}">
                        <h4>Pending Requisition </h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('procurement/hq-requisition') ? 'active' : '' }} " href="{{route('hq.requisitions')}}"><h4>HQ Requisition</h4></a>
                </li>
            </ul> --}}
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="title-heading">Hub Approved Requisition</h1>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Req Info</th>
                                        <th>HUB Info</th>
                                        <th>Requisition Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($requisitions))
                                        @foreach($requisitions as $requisition)
                                            @if($requisition->hubreqdetailsstatus == 'Approved')
                                                <tr>
                                                    <td> {{ $loop->iteration }}</td>
                                                    <td>
                                                        <address>
                                                            <strong>Requisition Id: {{$requisition->hub_requisition_no}}</strong><br>
                                                            Created Date: {{$requisition->requisition_date}}<br>
                                                            Request Delivery Date: {{$requisition->delivery_date}} <br>
                                                        </address>
                                                    </td>
                                                    <td><strong>{{$requisition->store->name}}</strong></td>
                                                    <td><strong>{{($requisition->priority == '1') ? 'High' : 'Low'}}</strong></td>
                                                    <td><strong>{{$requisition->hubreqdetailsstatus}}</strong></td>
                                                    <td>
                                                        <div class="d-flex justify-content-around align-items">
                                                            <a href="{{route('approved.details',$requisition->id)}}" class="btn details-btn" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
        $(document).ready( function () {
            $('#requisitionList').DataTable();
        } );

        $(document).ready( function () {
            $('#requisitionListHq').DataTable();
        } );
    </script>

@endpush

