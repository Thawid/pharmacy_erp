@extends('dboard.index')

@section('title','Requisition Details | HQ')

@section('dboard_content')
    @push('styles')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <style>
            @media print {
                @page {
                    margin: 20px;
                }

            }
        </style>
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">HUB-Requisition Details</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href={{route('procurehq.index')}}>Requisition List</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- printare -->
                    <div id="printableArea">
                        <div class="row">
                            <div class="col-md-12">
                                @if(isset($hub))

                                    <div class="col-md-6 text-left">
                                        <p><strong>HUB Name : {{ $hub->store_name }}</strong></p>
                                        <p><strong>Phone : {{ $hub->phone_no }}</strong></p>
                                        <p><strong>Email : {{ $hub->email }}</strong></p>
                                    </div>
                                    <div class="col-md-6 text-md-left">
                                        <p><strong>Code : {{ $hub->code }}</strong></p>
                                        <p><strong>Address : {{ $hub->address }}</strong></p>
                                    </div>
                                @endif
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                        <hr>

                        <form method="POST" action="" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                        <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Generic Name</th>
                                            <th>Unit</th>
                                            <th>Vendor</th>
                                            <th>Quantity</th>
                                            <th>UOM</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($requisition_details))

                                            @foreach($requisition_details as $requisition)
                                                <tr>
                                                    <td><input type="text" class="form-control"
                                                               value="{{ $requisition->product_name }}" readonly></td>
                                                    <td><input class="form-control" type="text"
                                                               value="{{ $requisition->generic_name }}" readonly></td>

                                                    <td><input type="text" class="form-control"
                                                               value="{{$requisition->unit_name}}" readonly></td>
                                                    <td><input type="text" class="form-control"
                                                               value="{{$requisition->vendor_name}}" readonly></td>
                                                    <td><input class="form-control" type="number"
                                                               value="{{$requisition->request_quantity}}" readonly></td>
                                                    <td><input class="form-control" type="text"
                                                               value="{{$requisition->uom_name}}" readonly></td>
                                                    <td><input class="form-control" type="text"
                                                               value="{{$requisition->status}}" readonly></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div><!-- end.col-md-12 -->
                            </div><!-- end.row -->
                            <hr>
                        </form>
                    </div><!-- end#printarea -->

                    <div class="row text-right">
                        <div class="col-md-12">
                            <button class="btn index-btn" onclick="printDiv('printableArea')">
                            Print
                            </button>
                        </div>
                    </div>
                </div><!-- end.tile-bdoy -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->

    
@endsection


@push('post_scripts')
    <script>
        function printDiv(divId) {
            var printContents = document.getElementById(divId).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endpush

