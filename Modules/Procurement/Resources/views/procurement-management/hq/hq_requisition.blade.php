@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')
    <style>

    </style>
@endpush
@section('dboard_content')

    {{-- <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-9 text-left">
                    <h2 class="title-heading">Requisition List-HQ </h2>
                </div>
            </div>

        </div>
    </div> --}}

    <div class="row">
        <div class="col-md-12 ">
            {{-- <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('procurement/procurehq') ? 'active' : '' }} " aria-current="page" href="{{route('procurehq.index')}}">
                        <h4>Pending Requisition </h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('procurement/hq-requisition') ? 'active' : '' }} " href="{{route('hq.requisitions')}}"><h4>HQ Requisition</h4></a>
                </li>
            </ul> --}}
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="title-heading">Vendor Pending Requisition</h1>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>

                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover requisition-list-hq" id="requisitionListHq">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Req Info</th>
                                        <th>Status</th>
                                        <th width="8%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $countItem = 1
                                    @endphp
                                    @if(isset($hq_requisitions))
                                        @foreach($hq_requisitions as $hq_requisition)
                                           @foreach($hq_requisition as $rows)
                                               @foreach($rows as $row)
                                                   @if($row->approved == 0)
                                                        <tr>
                                                           <td>{{$countItem++ }}</td>
                                                           <td>
                                                               <address>
                                                                   <strong>Vendor : {{ $row->name }}</strong><br>
                                                                   Req. No : {{ $row->hub_requiquisition_no }}<br>
                                                                   Req. Date : {{ $row->requisition_date }} <br>
                                                                   Del. Date : {{ $row->delivery_date }}
                                                               </address>
                                                           </td>
                                                           <td><strong>{{($row->approved == '1') ? 'Approved' : 'Pending'}}</strong></td>
                                                           <td>
                                                               {{--<a href="{{ route('hqReq.Details',$hq_requisition->vendor_id) }}" class="btn btn-outline-primary btn-sm" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>--}}
                                                               @if($row->approved == '1')
                                                                   <a href="javascript:void(0);" class="btn edit-btn" type="button" disabled=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Process"> </i></a>
                                                               @else
                                                                   <a href="{{route('purchase-order.create',[$row->vendor_id,$row->req_id])}}" class="btn edit-btn" type="button"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Process"> </i></a>
                                                               @endif
                                                           </td>
                                                        </tr>
                                                    @endif
                                               @endforeach
                                           @endforeach
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end .col-md-12 -->
                    </div> <!-- end .row -->
                </div>
            </div>
        </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
       $(document).ready( function () {
            $('#requisitionListHq').DataTable();
        } );
    </script>

@endpush

