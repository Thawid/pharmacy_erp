@extends('dboard.index')

@section('title','Update Requisition | HQ')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-6 text-left">
                            <h2 class="title-heading">HUB Requisition</h2>
                        </div>

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href={{route('procurehq.index')}}>Requisition
                                List</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- info-area -->
                    <div class="row">
                        @if(isset($hub))
                            <div class="col-md-6">
                                <p><strong>HUB Name :  {{ $hub->store_name }}</strong></p>
                                <p><strong>Phone : {{ $hub->phone_no }}</strong></p>
                                <p><strong>Email : {{ $hub->email }}</strong></p>
                            </div><!-- end.col-md-6 -->

                            <div class="col-md-6">
                                <p><strong>Code : {{ $hub->code }}</strong></p>
                                <p><strong>Address : {{ $hub->address }}</strong></p>
                            </div><!-- end.col-md-6 -->
                        @endif
                    </div><!-- end.row -->
                    <hr>

                    <!-- form -->
                    @if(isset($requisition_details))
                        @foreach($requisition_details as $key => $products)
                            <form  method="POST" action="{{ route('procurehq.update',$key) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for=""> Request Date *</label>
                                        <input type="date" name="requisition_date" id="requisition_date" value=""  class="form-control">
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for=""> Request Delivery Date *</label>
                                        <input type="date" name="delivery_date" id="delivery_date" value=""  class="form-control">
                                    </div>

                                    <div class="col-md-4 text-left">
                                        <div class="form-group">
                                            <label for="vendor" class="forget-form"> Priority *</label>
                                            <select class="form-control vendor_group " name="priority" id="priority">
                                                <option value="">Select Priority</option>
                                                <option value="1">High</option>
                                                <option value="2">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                </div><!-- end.row -->

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                            <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Generic Name</th>
                                                <th>Unit</th>
                                                <th>Vendor</th>
                                                <th>Quantity</th>
                                                <th>UOM</th>
                                                <th width="3%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($products as $product)
                                                    {{--{{ dd($product->product->product_id) }}--}}
                                                    <tr>
                                                        <input type="hidden" name="hub_id" value="{{ $hub->hub_id }}">
                                                        <input type="hidden" name="store_type" value="{{ $hub->store_type }}">
                                                        <input type="hidden" name="id[]" value="{{ $product->hub_req_details_id }}">
                                                        <input type="hidden" name="hub_requisition_id" value="{{ $product->hub_requisition_id }}">
                                                        <input type="hidden" name="hub_requisition_no" value="{{ $product->requisitions->hub_requisition_no }}">
                                                        <td>
                                                                <input type="hidden" name="product[]" value="{{$product->product->id}}">
                                                            <input type="text" class="form-control"  value="{{ $product->product->product_name }}" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="generic_id[]" value="{{ $product->generic->id }}">
                                                            <input class="form-control" type="text" value="{{ $product->generic->name }}" readonly>
                                                        </td>

                                                        <td>
                                                            <input type="hidden" name="unit_id[]" value="{{$product->unit->id}}">
                                                            <input type="text" class="form-control" value="{{$product->unit->unit_name}}" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="vendor_id[]" value="{{$product->vendor->id}}">
                                                            <input type="text" class="form-control" value="{{$product->vendor->name}}" readonly>
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="number" name="qty[]" value="{{$product->request_quantity}}">
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="hidden" name="uom_id[]"  value="{{$product->uom->id}}">
                                                            <input class="form-control" type="text"  value="{{$product->uom->uom_name}}" readonly>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-outline-danger" type="button" name="remove" id="remove">X</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div><!-- end.col-md-12 -->
                                </div><!-- end.row -->
                                <hr>

                                <div class="row text-right">
                                    <div class="col-md-12">
                                        <input type="submit" name="merge" class="btn create-btn" value="Merge & Save">
                                    </div><!-- end.col-md-12 -->
                                </div><!-- end.row -->
                            </form>
                        @endforeach
                    @endif
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->

@endsection


@push('post_scripts')
    <script>
        $("#table_field").on('click', "#remove", function() {

            $(this).closest('tr').remove();
        })
    </script>
@endpush

