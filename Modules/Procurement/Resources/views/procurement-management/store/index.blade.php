@extends('dboard.index')

@section('title','Store Requisition List')
@push('styles')
    <style>

    </style>
@endpush
@section('dboard_content')

<div class="tile">
    <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
            <div class="col-md-6">
                <h2 class="title-heading">Store Requisition List </h2>
            </div>
            @if(auth()->user()->role !== 'admin')
                @can('hasCreatePermission')
                <div class="col-md-6 text-right">
                    <a class="btn create-btn" href={{route('procurestore.create')}}>Create New </a>
                </div>
                @endcan
            @endif
        </div><!-- end.row -->
        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Requisition Info</th>
                            <th>Priority</th>
                            <th>Status</th>
                            <th width="12%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($requisitions))
                            @foreach($requisitions as $requisition)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        <address>
                                            <strong>Requisition Id: {{$requisition->store_requisition_no}}</strong><br>
                                            Created Date: {{$requisition->requisition_date}}<br>
                                            Request Delivery Date: {{$requisition->delivery_date}} <br>
                                        </address>
                                    </td>
                                    <td>{{$requisition->priority}}</td>
                                    <td>{{ $requisition->storestatus }}</td>
                                    <td>
                                        <div class="d-flex justify-content-around align-items-center">
                                            <a href="{{route('procurestore.show',$requisition->id)}}" class="btn details-btn" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"></i></a>
                                            @if(auth()->user()->role !== 'admin')
                                            @if($requisition->storeupdate == 'Approved')
                                                    <button class="btn edit-btn" type="submit" disabled=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"></i></button>
                                                    <button class="btn edit-btn" type="submit" disabled=""><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button>
                                                @else

                                                @can('hasEditPermission')
                                                <a href="{{route('procurestore.edit',$requisition->id)}}" class="btn edit-btn" type="button" title="edit"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a>
                                                @endcan
                                                @can('hasDeletePermission')
                                                    <form id="delete_form{{$requisition->requisition_no}}" method="POST"
                                                          action="{{ route('procurestore.destroy',$requisition->id) }}"
                                                          onclick="return confirm('Are you sure?')">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input name="id" type="hidden" value="{{$requisition->id}}">
                                                        <button class="btn edit-btn" type="submit"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button>
                                                    </form>
                                                @endcan

                                            @endif
                                            @endif
                                            @if($requisition->storeupdate !== 'Pending')
                                                <a href="javascript:void(0);" class="btn details-btn requisition-details" title="pop up"
                                                   data-toggle="modal"
                                                   data-target="#requisitionDetails"
                                                   data-id="{{$requisition->store_requisition_no}}">
                                                   <i class="fa fa-external-link-square" aria-hidden="true"></i>
                                                </a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div><!-- end.table-responsive -->
            </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
    </div><!-- end.tile-body -->
</div><!-- end.tile -->
<div class="modal fade" id="requisitionDetails">
    <div class="modal-dialog modal-lg">
        <div id="requisition-data"> </div>
    </div>
</div>
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
        $(document).ready( function () {
            $('#requisitionList').DataTable({
                'order':[[0,"asc"]]
            });

        } );
    </script>

    <script>
        $(document).ready(function () {

            $(".requisition-details").click(function (e) {
                $currID = $(this).attr("data-id");
                $.get("requisitionDetails/"+$currID, function (data) {
                        console.log(data);
                        $('#requisition-data').html(data);
                    }
                );
            });
        });
    </script>

@endpush

