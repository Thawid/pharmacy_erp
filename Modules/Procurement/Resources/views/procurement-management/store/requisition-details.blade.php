
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Requisition Details </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="card-body">
            <div class="container mt-3">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#hold">On Hold</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#distribute">Ready To Distribute</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#purchase">Going For Purchase</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="hold" class="container tab-pane active"><br>
                        <h5 class="pb-3">Hold Qty </h5>
                        <?php $date =$hold_request[0]['approximate_delivery_date']; ?>
                            @if(!empty($hold_request)) <p class="pb-2"> Delivery Date : {{ $date }} </p>  @endif
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <th>SL</th>
                            <th>Product Name</th>
                            <th>Unit</th>
                            <th>Hold Qty</th>
                            <th>UOM</th>
                            </thead>
                            <tbody>
                            @if(!empty($hold_request))
                                @foreach($hold_request as $details)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $details->product_name }}</td>
                                        <td>{{ $details->unit_name }}</td>
                                        <td>{{ $details->hold_quantity }}</td>
                                        <td>{{ $details->uom_name }}</td>
                                        <?php $date = $details->delivery_date ?>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div id="distribute" class="container tab-pane fade"><br>
                        <h5 class="pb-3 pt-3">Distribute Qty</h5>
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <th>SL</th>
                            <th>Product Name</th>
                            <th>Unit</th>
                            <th>Distribute Qty</th>
                            <th>UOM</th>
                            </thead>
                            <tbody>
                            @if(!empty($regular_distribution))
                                @foreach($regular_distribution as $details)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $details->product_name }}</td>
                                        <td>{{ $details->unit_name }}</td>
                                        <td>{{ $details->distribution_quantity }}</td>
                                        <td>{{ $details->uom_name }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div id="purchase" class="container tab-pane fade"><br>
                        <h5 class="pb-3 pt-3">Purchase Qty</h5>
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <th>SL</th>
                            <th>Product Name</th>
                            <th>Unit</th>
                            <th>Purchase Qty</th>
                            <th>UOM</th>
                            </thead>
                            <tbody>
                            @if(!empty($purchase_qty))
                                @foreach($purchase_qty as $details)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $details->product_name }}</td>
                                        <td>{{ $details->unit_name }}</td>
                                        <td>{{ $details->purchase_quantity }}</td>
                                        <td>{{ $details->uom_name }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


