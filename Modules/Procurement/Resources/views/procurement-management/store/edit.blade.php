@extends('dboard.index')

@section('title','Update Requisition | STORE')
@push('styles')
    <style>
        .btn-sm, .btn-group-sm > .btn {
            padding: 0.22rem 0.3rem;
            font-size: 0.765625rem;
            line-height: 1.5;
            border-radius: 3px;
        }
    </style>
@endpush
@section('dboard_content')
@push('style')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Update Requisition</h2>
                    </div>

                    <div class="col-md-6 text-md-right">
                        <a class="btn index-btn" href={{route('procurestore.index')}}>Requisition
                            List</a>
                    </div><!-- end.col -->
                </div><!-- end.row -->
                <hr>

                <!-- form -->
                <form method="POST" action="{{ route('procurestore.update',$requisition_details->id) }}" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="row">
                      <div class="col-md-12">
                        <div class="row">
                            <input type="hidden" name="store_requisition_id" value="{{$requisition_details->id}}">
                            <input type="hidden" name="new[store_requisition_id]" value="{{$requisition_details->id}}">

                            <div class="col-md-4 form-group">
                                <label for=""> Request Date *</label>
                                <input type="date" name="requisition_date" id="requisition_date" value="{{$requisition_details->requisition_date}}" class="form-control" required>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for=""> Request Delivery Date *</label>
                                <input type="date" name="delivery_date" id="delivery_date" value="{{ $requisition_details->delivery_date }}" class="form-control" required>
                            </div>

                            <div class="col-md-4 text-left">
                                <div class="form-group">
                                    <label for="vendor" class="forget-form"> Priority *</label>
                                    <select class="form-control vendor_group " name="priority" id="priority" required>
                                        <option value="High" @if($requisition_details->priority ==  "High") selected @endif>High</option>
                                        <option value="Low" @if($requisition_details->priority == "Low") selected @endif>Low</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- end.row -->
                      </div><!-- end.col-md-12 -->

                      <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                              <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Generic Name</th>
                                        <th>Available Vendor</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>UOM</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($requisition_details))
                                    @foreach($requisition_details->store_edits as $row)

                                    <tr>
                                        <input type="hidden" name="req_details_id[]" value="{{ $row->id }}">
                                        <td>
                                            <select class="form-control product" name="product[]" id="" data-id="0">
                                                <option value="{{ $row->product_id }}">{{ $row->product->product_name }}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" name="generic_id[]" value="{{ $row->generic_id }}">
                                            <input class="form-control" type="text" name="generic_name" value="{{ $row->generic->name }}" readonly>
                                        </td>
                                        <td>
                                            <select class="form-control" name="vendor[]" id="">
                                                <option value="{{ $row->vendor_id }}">{{ $row->vendor->name }}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" name="unit_id[]" id="">
                                                <option value="{{$row->unit_id}}">{{$row->unit->unit_name}}</option>
                                            </select>
                                        </td>
                                        <td><input class="form-control" type="number" name="qty[]" value="{{$row->request_quantity}}"></td>
                                        <td>
                                            <select class="form-control" name="uom_id[]" id="">
                                                <option value="{{$row->uom_id}}">{{$row->product_uom->uom_name}}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <button class="btn btn-outline-danger btn-sm w-100" type="button" name="remove" id="remove"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button>
                                        </td>


                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <input class="btn create-btn" type="button" data-toggle="tooltip" title="Add More" name="add" id="add" value="+">
                          </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                        <hr>

                        <div class="row">
                          <div class="col-md-12 text-right">
                              <div class="btn-group">
                                  <button type="submit" class="btn create-btn">Update</button>
                              </div>
                          </div>
                      </div><!-- row -->
                      </div><!-- end.col-md-12 -->

                      
                  </div><!-- end.row -->
              </form>
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </div><!-- edn.col-md-12 -->
</div><!-- end.row -->



@endsection

@push('post_scripts')

<!-- js script start from here -->
<script type="text/javascript" src="{{asset('js/plugins/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript">
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
</script>



<script type="text/javascript">
    $('.select2').select2({});
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var x = 1;

        $("#add").click(function() {

            var html = `'<tr>' + '<td> <select class="form-control product select2" onchange="getProduct(this)" data-id="${x}" name="new[product][]"><option selected disabled>--- Select a Product ----</option>@foreach($products as $row)<option value="{{$row->id}}">{{$row->product_name}}</option>@endforeach</select></td>' +
                    '<td><input class="form-control generic_id_${x}"  type="hidden" name="new[generic_id][]" id="generic_id_${x}" required=""><input class="form-control generic_name_${x}" id="generic_name_${x}" type="text" value="" name=generic_name[]" required="" id="generic${x}" readonly></td>' +
                    '<td><select class="form-control vendor_${x}" name="new[vendor][]" id="vendor_${x}"><option value="">---Select A Vendor---</option></select></td>' +
                    ' <td><input class="form-control unit_id_${x}"  type="hidden" name="new[unit_id][]" id="unit_id_${x}" required=""><input class="form-control unit_${x}" type="text" name="unit[]" id="unit_${x}" required=""></td>+
                    '<td>
                    <input class="form-control" type="number" name="new[qty][]" required="" id=""></td>' +<td><select class="form-control uom_${x}" name="new[uom_id][]" id="uom_${x}"><option selected disabled></option></select>
                    <input class="form-control status_${x}"  type="hidden" name="new[status][]" value="Pending" required="">
                    </td>'
                    '<td><button class="btn btn-outline-danger btn-sm w-100" type="button" name="remove" id="remove"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button></td>' + '</tr>'`;
            $("#table_field").append(html);
            x++;

            $('.select2').select2({
                width: '100%'
            });
        });

        $("#table_field").on('click', "#remove", function() {

            $(this).closest('tr').remove();
        })
    });
</script>


<!-- Get Product Item -->
<script>

    $(document).ready(function(){
       $('#preloader').hide();
   });

   function getAjaxReq(e){
       var id = $(e).val();
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
       $.ajax({
           type: 'GET',
           url: "{{ url('/procurement/get-product') }}/" + id,
           dataType: "json",
           success: function(res) {
               console.log(res);
               $('#showdata').show();
               $('#preloader').hide();
               var selectorID = $(e).attr('data-id');

               if (res.pgeneric.name) {
                   $(`.generic_name_${selectorID}`).val(res.pgeneric.name)
                   $(`.generic_id_${selectorID}`).val(res.pgeneric.id)
               } else {
                   $(`.generic_name_${selectorID}`).val('not found!')
                   $(`.generic_id_${selectorID}`).val('not found!')
               }

               if (res.product_unit.unit_name) {
                   $(`.unit_${selectorID}`).val(res.product_unit.unit_name)
                   $(`.unit_id_${selectorID}`).val(res.product_unit.id)
               } else {
                   $(`.unit_${selectorID}`).val('not found!')
                   $(`.unit_id_${selectorID}`).val('not found!')
               }
               $(`.uom_${selectorID}`).empty();

               $(`.uom_${selectorID}`).append('<option value="' + res.product_uom.id + '">' + res.product_uom.uom_name + '</option>');



               $(`.vendor_${selectorID}`).empty();
               $.each(res.vendors, function(index, vendorlist) {
                   console.log(vendorlist)
                   $(`.vendor_${selectorID}`).append('<option value="' + vendorlist.id + '">' + vendorlist.vendor_name.name + '</option>');
               });



           }
       });
   }

   function getProduct(e) {
       var id = $(e).val();

       let productarray = [];
       const products = document.getElementsByClassName('product');
       var count = true;
       for( const product of products ) {
           if(productarray.indexOf(product.value) == '-1'){
               productarray.push(product.value);
               count = true;
           }else{
               count = false;
               product.parentElement.parentElement.remove();
               alert('This product is already exists');

           }

       }

       if(count == true){
           $('#showdata').hide();
           $('#preloader').show();
           getAjaxReq(e);
       }

   }
</script>

<!-- select2 -->

<script>
    $(document).ready(function() {
        $('.select2').select2({
            width: '100%'
        });
    });
</script>

<script>
    if (localStorage.getItem("parent")) {
        //document.getElementById("parent").innerHTML = localStorage.getItem("parent");
    }
    setInterval(function () {
        //localStorage.setItem("parent", document.getElementById("parent").innerHTML);

    }, 2000)
</script>

<script>
    function saveData(){
        var data = $('#procurestore').serializeArray();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: "{{ url('/procurement/product/save') }}/",
            data:data,
            dataType: "json",
            success: function(res) {
                //console.log(res);
            }
        });
    }
</script>
@endpush
