@extends('dboard.index')

@section('title','Requisition Details | Store')

@section('dboard_content')
@push('style')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <!-- title -->
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="title-headinng"> Requisition Details </h2>
                    </div><!-- end.col-md-6 -->

                    <div class="col-md-6 text-md-right">
                        <a class="btn index-btn" href="{{route('procurestore.index')}}">Requisition
                            List</a>
                    </div><!-- end.col-md-6 -->
                </div><!-- end.row -->
                <hr>

                <!-- form -->
                <form method="POST" action="" enctype="multipart/form-data">
                @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <p><strong>Store: {{$requisition_details->store->name}}</strong></p>
                                    <P><strong>Store Type: {{$requisition_details->store->type->name}}</strong></P>
                                    <P><strong>Priority: {{($requisition_details->priority) }} </strong></P>
                                </div>
                                <div class="col-md-3 form-group">
                                    <p><strong>Request Date: {{$requisition_details->requisition_date}}</strong></p>
                                    <p><strong>Request Delivery Date: {{$requisition_details->delivery_date}}</strong></p>
                                </div><!-- end.col-md-3 -->
                            </div><!-- end.row -->
                        </div><!-- end.col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Generic Name</th>
                                            <th>Available Vendor</th>
                                            <th>Unit</th>
                                            <th>Req. Quantity</th>
                                            <th>Distribute Qty</th>
                                            <th>UOM</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(isset($requisition_details))
                                        @foreach($requisition_details->details as $row)

                                        <tr>
                                            <td>
                                                <select class="form-control" name="" id="">
                                                    <option value="{{ $row->product_id}}" readonly="">{{ $row->product->product_name }}</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control" type="text" name="generic_name" value="{{ $row->generic->name ?? ' ' }}" readonly>
                                            </td>
                                            <td>
                                                <select class="form-control" name="vendor" id="vendor">
                                                    <option value="{{ $row->vendor_id }}" readonly="">{{ $row->vendor->name ?? ' ' }}</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control" name="unit" id="unit">
                                                    <option value="{{$row->unit_id}}" readonly="">{{$row->unit->unit_name ??' '}}</option>
                                                </select>
                                            </td>
                                            <td><input class="form-control" type="number" name="qty" value="{{$row->request_quantity}}" readonly=""></td>
                                            <td><input class="form-control" type="number" name="qty" value="{{$row->distribute_quantity}}" readonly=""></td>
                                            <td>
                                                <select class="form-control" name="unit" id="unit">
                                                    <option value="" readonly="">{{$row->product_uom->uom_name}}</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" value="{{ $row->status }}" disabled></td>
                                        </tr>

                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                </div>
                            </div><!-- end.row -->
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                    <hr>
                </form>
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->

@endsection


@push('post_scripts')

<script type="text/javascript">
    $('.select2').select2({});
</script>

<script>
    $(document).ready(function() {
        $('#product_type').on('change', function() {
            var productName = $(".product-type option:selected").text();
            var product = $('#product').val(productName);
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#vendor_type').on('change', function() {
            var vendorName = $(".vendor-type option:selected").text();
            var vendor = $('#vendor').val(vendorName);
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#unit_type').on('change', function() {
            var unitName = $(".unit_type option:selected").text();
            var unit = $('#unit').val(unitName);
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var html = '<tr>' + '<td><select class="form-control" name="" id=""><option value="">Product</option></select></td>' +
            '<td><input class="form-control" type="text" name="generic" required=""></td>' +
            '<td><select class="form-control" name="vendor" id="vendor"><option value="">Vendor</option></select></td>' +
            ' <td><select class="form-control" name="unit" id="unit"><option value="">Unit</option></select></td>' +
            '<td><input class="form-control" type="number" name="number" required=""></td>' +
            '<td><button class="btn btn-outline-danger btn-sm" type="button" name="remove" id="remove"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button>' + ' ' +
            '<button class="btn btn-outline-primary btn-sm" type="button" name="" id=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></button></td>' + '</tr>';

        var x = 1;
        var max = 10;
        $("#add").click(function() {
            // alert('ok');
            if (x <= max) {
                $("#table_field").append(html);
                x++;
            }
        });

        $("#table_field").on('click', "#remove", function() {
            $(this).closest('tr').remove();
            x--;
        })
    });
</script>


@endpush
