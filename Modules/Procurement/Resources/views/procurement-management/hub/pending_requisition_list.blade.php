@extends('procurement::procurement-management.hub.index')
@section('title','Hub Pending Requisition list')
@push('styles')
    <style>

        nav > .nav-tabs > a.nav-item {
            font-size: 18px;
        }

        .badge {
          font-size: 90%;
          padding: 10px;
        }
    </style>
@endpush

@section('contents')
    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="title-heading">Pending Requisition List</h1>
                </div><!-- end.col-md-6 -->
            </div><!-- end.row -->
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <table class="table table-bordered table-striped table-hover requisition-list table-responsive-xl table-responsive-lg" id="requisitionList">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Requisition  Info</th>
                                    <th>Store Info</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(!empty($pending_requisition))
                                @foreach($pending_requisition as $row)
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td>
                                        <address>
                                            <strong>Requisition_No:{{$row->store_requisition_no}} </strong><br>
                                            Created Date: {{$row->requisition_date}}<br>
                                            Request Delivery Date: {{$row->delivery_date}}<br>
                                        </address>
                                    </td>
                                    <td>
                                        <strong>{{$row->store_name}}</strong>
                                    </td>
                                    <td>
                                        <strong>{{$row->priority}}</strong>
                                    </td>
                                    <td>
                                        <strong class="badge badge-info">{{$row->status}}</strong>
                                    </td>

                                    <td>
                                        <div class="d-flex justify-content-around">
                                        <a href="{{route('hub.requisition.pending.details',$row->id)}}" class="btn details-btn" type="button" name="" id=""><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>


                                        @if($row->status == "Pending")
                                        @can('hasEditPermission')
                                        <a href="{{route('hub.requisition.pending.modify',$row->id)}}" class="btn details-btn" type="button" name="" id=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit" > </i></a>
                                        @endcan

                                        @else

                                        @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!-- end.col-md-12 -->
            </div><!-- end.row -->
        </div><!-- end.tile-body -->
    </div><!-- end.tile -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();
    });
</script>
@endpush
