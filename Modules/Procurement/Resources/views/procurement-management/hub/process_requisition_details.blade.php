@extends('dboard.index')

@section('title','Pending Requisition Details | HUB')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading">Process Requisition Details (HUB)</h2>
                        </div>
                        <div class="col-md-3 text-md-right">
                            <a class="btn btn-outline-primary icon-btn" href="{{route('hub.requisition.process.list')}}"> <i class="fa fa-arrow-right"></i>  Process Requisition
                                List</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                       
                        
                            <div class="col-md-6 text-left">
                                <p><strong>Store : </strong>{{$details->store_name}}</p>
                                <p><strong>Store No : </strong>{{$details->store_name}}</p>
                                <p><strong>Store Type : </strong>{{$details->store_type}} </p>
                                <p><strong>Status : </strong> {{$details->status}}</p>
                            </div>
                            <div class="col-md-6 text-md-left">
                                <p><strong>Request Date : </strong>{{$details->requisition_date}}</p>
                                <p><strong>Delivery Date : </strong>{{$details->delivery_date}}</p>
                                <p><strong>Priority : </strong>{{$details->priority}}</p>
                                
                            </div>
                        
                    </div>

                </div>
            </div>

        </div>
    </div>

    
        <div class="row">
            <div class="form-group col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row">
                            <table class="table table-striped table-bordered table_field table-sm" id="table_field"
                                   style="margin-top: 10px">
                                <thead class="thead-light">
                                <tr class="bg-white">
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    <th>Unit</th>
                                    <th>Available Vendor</th>
                                    <th>Purchase Quantity</th>
                                    <th>UOM</th>
                                </tr>
                                </thead>
                                <tbody>

                               
                               
                                                <tr>
                                                    <td><input type="text" class="form-control"  value="{{$details->product_name}}" readonly></td>
                                                    <td><input class="form-control" type="text" value="{{$details->generic_name}}" readonly></td>

                                                    <td><input type="text" class="form-control" value="{{$details->unit_name}}" readonly></td>
                                                    <td><input class="form-control" type="text"  value="{{$details->vendor_name}}" readonly></td>
                                                    <td><input class="form-control" type="text"  value="{{$details->purchase_quantity}}" readonly></td>
                                                    <td><input class="form-control" type="text"  value="{{$details->uom_name}}" readonly></td>
                                                </tr>
                                               
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
@endsection


@push('post_scripts')

@endpush

