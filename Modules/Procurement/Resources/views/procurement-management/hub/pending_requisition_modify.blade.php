@extends('dboard.index')

@section('title','Process Pending Requisition | HUB')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Process Pending Requisition (HUB)</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('hub.requisition.pending.list')}}">Pending Requisition
                                List</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- info area -->
                    <div class="row mb-4">
                        @if(isset($pending_requisition_process))
                            <div class="col-md-6 text-left">
                                <p><strong>Store : </strong>{{$pending_requisition_process['single_data']->store_name}}</p>
                                <p><strong>Store No : </strong>{{$pending_requisition_process['single_data']->store_no}}</p>
                                <p><strong>Store Type : </strong> {{$pending_requisition_process['single_data']->store_type}}</p>
                                <p><strong>Status : </strong> {{$pending_requisition_process['single_data']->status}}</p>
                            </div>
                            <div class="col-md-6 text-md-left">
                                <p><strong>Request Date : </strong>{{$pending_requisition_process['single_data']->requisition_date}}</p>
                                <p><strong>Delivery Date : </strong>{{$pending_requisition_process['single_data']->delivery_date}}</p>
                                <p><strong>Priority : </strong>{{$pending_requisition_process['single_data']->priority}}</p>

                            </div>
                        @endif
                    </div><!-- end.row -->

                    <form  method="POST" action="{{route('hub.requisition.pending.merge')}}" enctype="multipart/form-data">
                        @csrf
                
                        <input type="hidden" name="store_id" value="{{$pending_requisition_process['single_data']->store_id}}" >
                        <input type="hidden" name="store_type" value="{{$pending_requisition_process['single_data']->store_type}}" >
                        <input type="hidden" name="store_requisition_id" value="{{$pending_requisition_process['single_data']->store_requisition_id}}" >
                        <input type="hidden" name="store_requisition_no" value="{{$pending_requisition_process['single_data']->store_requisition_no}}" >
                        <input type="hidden" name="requisition_date" value="{{$pending_requisition_process['single_data']->requisition_date}}" >
                        <input type="hidden" name="delivery_date" value="{{$pending_requisition_process['single_data']->delivery_date}}" >
                        <input type="hidden" name="priority" value="{{$pending_requisition_process['single_data']->priority}}" >
                        <input type="hidden" name="warehouse_id" value="{{$pending_requisition_process['single_data']->warehouse_id}}" >
                        <input type="hidden" name="purchase_distribution_requests_id" value="{{$pending_requisition_process['single_data']->purchase_distribution_requests_id}}" >
                
                        <!-- table -->
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                    <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Generic Name</th>
                                        <th>Unit</th>
                                        <th>Available Vendor</th>
                                        <th>Purchase Quantity</th>
                                        <th>UOM</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                
                                    @if(isset($pending_requisition_process))
                                        @foreach($pending_requisition_process['details'] as $row)
                
                                                    <tr>
                                                        <input type="hidden" name=product_id[] value="{{$row->product_id}}">
                                                        <td><input type="text" class="form-control"  value="{{$row->product_name}}" readonly></td>
                                                        <input type="hidden" name=generic_id[] value="{{$row->generic_id}}">
                                                        <td><input class="form-control" type="text" value="{{$row->generic_name}}" readonly></td>
                                                        <input type="hidden" name=unit_id[] value="{{$row->unit_id}}">
                                                        <td><input type="text" class="form-control" value="{{$row->unit_name}}" readonly></td>
                                                        <input type="hidden" name=vendor_id[] value="{{$row->vendor_id}}">
                                                        <td><input class="form-control" type="text"  value="{{$row->vendor_name}}" readonly></td>
                                                        <td><input class="form-control" type="text" name="purchase_quantity[]"  value="{{$row->purchase_quantity}}" readonly></td>
                                                        <input  type="hidden" name="uom_id[]" value="{{$row->uom_id}}">
                                                        <td><input class="form-control" type="text"  value="{{$row->uom_name}}" readonly></td>
                                                        <td>
                                                        <input class="btn btn-danger" type="button" name="remove" id="remove" value="&#10006;">
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                        @endif
                                    </tbody>
                
                                </table>
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                        <hr>
                
                        <!-- button -->
                        <div class="row text-right">
                            <div class="col-md-12">
                                <button type="submit" class="btn create-btn">Merge</button>
                            </div>
                        </div><!-- end.row -->
                    </form>
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->


    
@endsection


@push('post_scripts')

@endpush

