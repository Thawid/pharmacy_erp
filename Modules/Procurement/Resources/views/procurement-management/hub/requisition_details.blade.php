@extends('dboard.index')

@section('title','Pending Requisition Details | HUB')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading">Requisition Details (HUB)</h2>
                        </div>
                        <div class="col-md-3 text-md-right">
                            <a class="btn index-btn" href="{{route('hub.requisition.list')}}">Requisition
                                List</a>
                        </div><!-- end.col-md-3 -->
                    </div><!-- end.row -->
                    <hr>

                    <!-- info -->
                    <div class="row">
                        @if(isset($details))
                            <div class="col-md-6 text-left">
                                <p><strong>Requisition No : </strong>{{$details['single_data']->hub_requisition_no}}</p>
                                <p><strong>Store Type : </strong> {{$details['single_data']->store_type}}</p>
                                <p><strong>Status : </strong> {{$details['single_data']->status}}</p>
                            </div>
                            <div class="col-md-6 text-md-left">
                                <p><strong>Request Date : </strong>{{$details['single_data']->requisition_date}}</p>
                                <p><strong>Delivery Date : </strong>{{$details['single_data']->delivery_date}}</p>
                                <p><strong>Priority : </strong>{{$details['single_data']->priority}}</p>

                            </div>
                        @endif
                    </div><!-- end.row -->
                    <hr>

                    <!-- table -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Generic Name</th>
                                <th>Unit</th>
                                <th>Available Vendor</th>
                                <th>Purchase Quantity</th>
                                <th>UOM</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($details))
                                @foreach($details['details'] as $row)

                                    <tr>
                                        <td><input type="text" class="form-control"  value="{{$row->product_name}}" readonly></td>
                                        <td><input class="form-control" type="text" value="{{$row->generic_name}}" readonly></td>

                                        <td><input type="text" class="form-control" value="{{$row->unit_name}}" readonly></td>
                                        <td><input class="form-control" type="text"  value="{{$row->vendor_name}}" readonly></td>
                                        <td><input class="form-control" type="text"  value="{{$row->request_quantity}}" readonly></td>
                                        <td><input class="form-control" type="text"  value="{{$row->uom_name}}" readonly></td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->

@endsection


@push('post_scripts')

@endpush

