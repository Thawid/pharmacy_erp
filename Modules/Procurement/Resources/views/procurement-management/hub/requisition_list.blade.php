@extends('procurement::procurement-management.hub.index')
@section('title','Hub Pending Requisition list')
@push('styles')
    <style>
        nav > .nav-tabs > a.nav-item {
            font-size: 18px;
        }

        .badge {
          font-size: 90%;
          padding: 10px;
        }
    </style>
@endpush
@section('contents')

<div class="tile">
    <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
            <div class="col-md-6 text-left">
                <h2 class="title-heading">Requisition List </h2>
            </div>
            @can('hasCreatePermission')
            <div class="col-md-6 text-md-right">
                <a class="btn create-btn" href={{route('procurehub.create')}}>Create New</a>
            </div>
            @endcan
           
        </div><!-- end.row -->
        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
                        <thead>
                            <tr>

                                <th>SL</th>
                                <th>Requisition  Info</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th width="8%">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                         @if(!empty($requisition_list))
                            @foreach($requisition_list as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <address>
                                        <strong>Requisition No: </strong>{{$row->hub_requisition_no}}<br>
                                        Created Date: {{$row->requisition_date}}<br>
                                        Request Delivery Date: {{$row->delivery_date}} <br>
                                    </address>
                                </td>
                                <td>
                                    <strong>{{$row->priority}}</strong>
                                </td>
                                <td>
                                    <strong class="badge badge-info">{{$row->hubReqdetailsstatus}}</strong>
                                </td>

                                <td>
                                    <a href="{{route('hub.requisition.details',$row->id)}}" class="btn details-btn" type="button" name="" id="" title="Details"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"></i></a>
                                </td>
                            </tr>
                             @endforeach
                            @endif
                        </tbody>
                    </table>
                </div><!-- end.table-responsive -->
            </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
    </div><!-- end.tile-body -->
</div><!-- end.tile -->



@endsection
@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();
    });
</script>
@endpush
