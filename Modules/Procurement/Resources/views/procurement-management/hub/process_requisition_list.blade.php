@extends('procurement::procurement-management.hub.index')
@section('title','Hub Pending Requisition list')
@push('styles')
    <style>
        .btn-sm, .btn-group-sm > .btn {
            padding: 0.20rem 0.2rem;
            font-size: 0.765625rem;
            line-height: 1.5;
            border-radius: 3px;
        }

        table.dataTable td:last-child i {
            opacity: 0.9;
            font-size: 18px;
            margin: 0 3px;
        }
        nav > .nav-tabs > a.nav-item {
            font-size: 18px;
        }
    </style>
@endpush

@section('contents')
    <div class="tile">
        <div class="tile-body">
            <div class="row">
                <div class="col-md-6 text-left">
                    <button class="btn index-btn update_all" data-url="{{ url('procurement/hub-requisition/process/addAll') }}">All Selected Add </button>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{route('hub.requisition.process.confirm.view')}}">
                    <button type="button" class="btn index-btn">
                        New Requisition <span class="badge badge-light">{{ $process_list['total_new_requisition']}}</span>
                    </button>
                    </a>
                </div>
            </div><!-- end.row -->
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
                            <thead>
                                <tr>
                                    <th width="50px"><input type="checkbox" id="master"></th>
                                    <th>Product Name</th>
                                    <th>Store Info</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                 @if(!empty($process_list))
                                @foreach($process_list['process'] as $row)
                                <tr  id="tr_{{$row->id}}">
                                    <td><input type="checkbox" class="sub_chk" data-id="{{$row->id}}"></td>
                                    <td>
                                            <strong style="color:lightseagreen">{{$row->product_name}}</strong>
                                    </td>
                                    <td>
                                        <strong>{{$row->store_name}}</strong>
                                    </td>
                                    <td>
                                        <a href="{{route('hub.requisition.process.details',$row->id)}}" class="btn btn-outline-primary btn-sm" type="button" name="" id=""><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                                        <a href="{{route('hub.requisition.process.add',$row->id)}}" class="btn btn-outline-primary btn-sm" type="button" name="" id=""><i class="fa fa-plus-circle" aria-hidden="true" data-toggle="tooltip" title="Add to Requisition"> </i></a>
                                    </td>
                                </tr>
                             @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div><!-- end.table-responsive -->
                </div><!-- end.col-md-12 -->
            </div><!-- end.row -->
        </div><!-- end.tile-body -->
    </div><!-- end.tile -->
@endsection


@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();

        $('#master').on('click',function(e){

            if($(this).is(':checked',true)){
                $('.sub_chk').prop('checked',true);
            }else{
                $('.sub_chk').prop('checked',false);
            }
        })

        $('.update_all').on('click',function(e){

            var allVals = [];

            $(".sub_chk:checked").each(function(){
                allVals.push($(this).attr('data-id'));
            })

            if(allVals.length <=0){
                alert("Please select row.");
            }else{
                var check = confirm('Are you sure you want to Add this product ?');
                if(check == true){
                    var join_selected_value = allVals.join(",");

                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        data: 'ids='+join_selected_value,
                        success: function(data){
                           location.reload();
                        },
                        error: function(data){
                            alert(data.responseText);
                        }
                    });


                }
            }

        })
    });
</script>
@endpush
