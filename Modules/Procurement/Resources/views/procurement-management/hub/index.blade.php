@extends('dboard.index')

@section('title','Hub list')
@push('styles')
    <style>
        nav > .nav-tabs > a.nav-item {
            font-size: 18px;
        }
    </style>
@endpush
@section('dboard_content')
{{-- <div class="tile">
    <div class="tile-body">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link {{ request()->routeIs('hub.requisition.pending.list') ? 'active' : '' }} " aria-current="page" href="{{route('hub.requisition.pending.list')}}">
                            <h4>Pending Requisition List </h4>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->routeIs('hub.requisition.process.list') ? 'active' : '' }} " href="{{route('hub.requisition.process.list')}}"><h4>Process requisition List</h4></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->routeIs('hub.requisition.list') ? 'active' : '' }} " href="{{route('hub.requisition.list')}}"><h4>Requisition List</h4></a>
                    </li>
                </ul>
            </div>
            <div class="col-md text-md-right">
                <!-- <a class="btn btn-outline-primary icon-btn" href={{route('procurehq.create')}}><i class="fa fa-arrow-right"></i> Purchase List</a> -->
            </div>
        </div>

    </div>
</div> --}}
@yield('contents')

@endsection
@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();
    });
</script>

@endpush
