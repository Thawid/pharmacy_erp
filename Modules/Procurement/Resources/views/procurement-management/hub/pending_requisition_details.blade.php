@extends('dboard.index')

@section('title','Pending Requisition Details | HUB')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush

<!-- content -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Pending Requisition Details (HUB)</h2>
                        </div><!-- end.col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('hub.requisition.pending.list')}}">Pending Requisition
                                List</a>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>

                    <!-- info -->
                    <div class="row">
                        @if(isset($pending_requisition_details))
                            <div class="col-md-6 text-left">
                                <p><strong>Store : </strong>{{$pending_requisition_details['single_data']->store_name}}</p>
                                <p><strong>Store No : </strong>{{$pending_requisition_details['single_data']->store_no}}</p>
                                <p><strong>Store Type : </strong> {{$pending_requisition_details['single_data']->store_type}}</p>
                                <p><strong>Status : </strong> {{$pending_requisition_details['single_data']->status}}</p>
                            </div>
                            <div class="col-md-6 text-md-left">
                                <p><strong>Request Date : </strong>{{$pending_requisition_details['single_data']->requisition_date}}</p>
                                <p><strong>Delivery Date : </strong>{{$pending_requisition_details['single_data']->delivery_date}}</p>
                                <p><strong>Priority : </strong>{{$pending_requisition_details['single_data']->priority}}</p>
                                
                            </div>
                        @endif
                    </div><!-- end.row -->
                    <hr>

                    <!-- table -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    <th>Unit</th>
                                    <th>Available Vendor</th>
                                    <th>Purchase Quantity</th>
                                    <th>UOM</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($pending_requisition_details))
                                    @foreach($pending_requisition_details['details'] as $row)
                               
                                                <tr>
                                                    <td><input type="text" class="form-control"  value="{{$row->product_name}}" readonly></td>
                                                    <td><input class="form-control" type="text" value="{{$row->generic_name}}" readonly></td>

                                                    <td><input type="text" class="form-control" value="{{$row->unit_name}}" readonly></td>
                                                    <td><input class="form-control" type="text"  value="{{$row->vendor_name}}" readonly></td>
                                                    <td><input class="form-control" type="text"  value="{{$row->purchase_quantity}}" readonly></td>
                                                    <td><input class="form-control" type="text"  value="{{$row->uom_name}}" readonly></td>
                                                </tr>
                                                @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
    
@endsection


@push('post_scripts')

@endpush

