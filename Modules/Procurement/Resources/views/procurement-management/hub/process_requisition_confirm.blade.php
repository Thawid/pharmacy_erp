@extends('dboard.index')

@section('title','Process Pending Requisition | HUB')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <!-- tile -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Process Requisition Confirmation (HUB)</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('hub.requisition.process.list')}}">Process Requisition
                                List</a>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>

                    <!-- form -->
                    <form  method="POST" action="{{route('hub.requisition.process.confirm.store')}}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for=""> Request Date *</label>
                                <input type="date" name="requisition_date" id="requisition_date" value="" placeholder="yyyy-mm-dd" class="form-control" required>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for=""> Request Delivery Date *</label>
                                <input type="date" name="delivery_date" id="delivery_date" value="" placeholder="yyyy-mm-dd" class="form-control" required>
                            </div>

                            <div class="col-md-4 text-left">
                                <div class="form-group">
                                    <label for="vendor" class="forget-form"> Priority *</label>
                                    <select class="form-control vendor_group " name="priority" id="priority" required>
                                        <option value="">Select Priority</option>
                                        <option value="High">High</option>
                                        <option value="Low">Low</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- end.row -->
                        <hr>

                        @foreach($new_requisition['hub_approve_requisition_details_id'] as $row)
                            <input type="hidden" name="hub_approve_requisition_details_id[]" value="{{$row->id }}">
                        @endforeach

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                    <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Generic Name</th>
                                        <th>Unit</th>
                                        <th>Available Vendor</th>
                                        <th>Purchase Quantity</th>
                                        <th>UOM</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if(isset($new_requisition))
                                        @foreach($new_requisition['requisition_data'] as $row)

                                        <tr>
                                            <input type="hidden" name="hub_id" value="{{$row->hub_id}}">
                                            <input type="hidden" name="store_type" value="{{$row->store_type}}">
                                            <input type="hidden" name=product_id[] value="{{$row->product_id}}">
                                            <td><input type="text" class="form-control"  value="{{$row->product_name}}" readonly></td>
                                            <input type="hidden" name=generic_id[] value="{{$row->generic_id}}">
                                            <td><input class="form-control" type="text" value="{{$row->generic_name}}" readonly></td>
                                            <input type="hidden" name=unit_id[] value="{{$row->unit_id}}">
                                            <td><input type="text" class="form-control" value="{{$row->unit_name}}" readonly></td>
                                            <input type="hidden" name=vendor_id[] value="{{$row->vendor_id}}">
                                            <td><input class="form-control" type="text"  value="{{$row->vendor_name}}" readonly></td>
                                            <td><input class="form-control" type="text" name="purchase_quantity[]"  value="{{$row->purchase_quantity}}" readonly></td>
                                            <input  type="hidden" name="uom_id[]" value="{{$row->uom_id}}">
                                            <td><input class="form-control" type="text"  value="{{$row->uom_name}}" readonly></td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button type="submit" class="btn create-btn ">Confirm</button>
                            </div>
                        </div>
                    </form>
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->


@endsection


@push('post_scripts')

@endpush