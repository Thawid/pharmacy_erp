@extends('procurement::procurement-management.purchase.index')
@section('title','Purchase pending order list')
@section('contents')
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
                <thead>
                    <tr>
                        <th>Vendor Name</th>
                        <th>Requisition Type</th>
                        <th>Status</th>
                        <th width="12%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($purchase_orders))
                    @foreach($purchase_orders as $row)
                    <tr>
                        <td>
                            <address>
                                <strong style="color:lightseagreen">{{$row->vendor->name }}</strong><br>
                            </address>
                        </td>
                        <td><strong style="color: red">Bulk</strong></td>
                        <td><strong style="color:dodgerblue">{{($row->status == '1') ? 'Processing' : 'Pending'}}</strong></td>
                        

                       <td>
                            <div class="d-flex justify-content-around align-items-center">
                                <a href="{{route('purchase-orders.pending.details',$row->vendor_id)}}" class="btn details-btn" type="button" name="" id=""><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                            @if($row->status == 0)
                                <a href="{{route('purchase-order.create',$row->vendor_id)}}" class="btn edit-btn" type="button" name="" id=""><i class="fa fa-truck" aria-hidden="true" data-toggle="tooltip" title="Processing"> </i></a>
                            @else
                                <button class="btn edit-btn" type="button" disabled name="" id=""><i class="fa fa-truck" aria-hidden="true" data-toggle="tooltip" title="Processing"> </i></button>
                            </div>
                            <!-- <button class="btn btn-outline-danger btn-sm" type="button" name="remove" id="remove"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button> -->
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div><!-- end.table-responsive -->
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->
@endsection
@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();
    });
</script>

@endpush