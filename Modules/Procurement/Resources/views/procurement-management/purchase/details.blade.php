@extends('dboard.index')

@section('title','Purchase Order Details')

@section('dboard_content')
@push('style')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush
<div class="row">
    <div class="col-md-12">
        <div class="tile mb-1">
            <div class="tile-body">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading"> Pending Order Details</span> </h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('purchase-orders.pending.list')}}">Purchase
                            List</a>
                    </div>
                </div><!-- end.row -->
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="bg-white">
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    <th>Vendor Name</th>
                                    <th>Unit</th>
                                    <th>UOM</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($hqrequisitons as $row)
                                <tr>
                                    <td>{{$row->product->product_name ?? ' '}}</td>
                                    <td>{{$row->generic->name ?? ' '}}</td>
                                    <td>{{$row->hq_requisition->vendor->name ?? ' '}}</td>
                                    <td>{{$row->unit->unit_name ?? ' '}}</td>
                                    <td>{{$row->uom->uom_name ?? ' '}}</td>
                                    <td>{{$row->quantity}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- end.col-md-12 -->
                </div><!-- end.row -->
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </div><!-- end.col-md-12 -->
</div>

@endsection
