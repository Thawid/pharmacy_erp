@extends('procurement::procurement-management.purchase.index')
@section('title','Purchase process order list')
@section('contents')
<div class="table-responsive bg-light p-3 ">
    <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
        <thead>
            <tr>
                <th>Details</th>
                <th>Priority</th>
                <th>Status</th>
                <th width="8%">Action</th>
            </tr>
        </thead>
        <tbody>

            @if(!empty($purchase_order_list))
            @foreach($purchase_order_list as $row)
                {{--{{ dd($row) }}--}}
            <tr>
                <td>
                    <address>
                        <strong>Vendor: {{$row->vendor->name}}</strong><br>
                        <strong>Purchase_No: {{$row->purchase_order_no}}</strong><br>
                        Created Date: {{$row->purchase_order_date}}<br>
                        Request Delivery Date: {{$row->requested_delivery_date}} <br>
                    </address>
                </td>
                @if($row->priority == 1)
                <td><strong>High</strong></td>
                @elseif($row->priority == 2)
                <td><strong>Low</strong></td>
                @endif
                <td> <strong>{{ $row->status }}</strong></td>
                <td>
                    <div class="d-flex justify-content-around align-items-center">

                        <a href="{{route('purchase.orders.details',$row->id)}}" class="btn details-btn" type="button" name="" id=""><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                        @if($row->status == 'Approved')
                            <a href="javascript:void(0);" class="btn edit-btn" type="button" disabled=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a>
                        @else
                            <a href="{{route('purchase.orders.edit',$row->id)}}" class="btn edit-btn" type="button" name="" id=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a>
                        @endif
                    </div>
                    <!-- <button class="btn btn-outline-danger btn-sm" type="button" name="remove" id="remove"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button> -->
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection
@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();
    });
</script>
@endpush
