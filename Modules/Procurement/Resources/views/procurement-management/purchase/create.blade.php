@extends('dboard.index')

@section('title','Purchase Order Process')

@section('dboard_content')
@push('style')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Create Purchase Order </h2>
          </div>
          <div class="col-md-6 text-md-right">
            <a class="btn index-btn" href="{{route('hq.requisitions')}}">HQ
              Requisitions</a>
          </div>
        </div><!-- end.row -->
        <hr>

        <!-- form -->
        <form method="POST" action="{{route('purchase-orders.store')}}" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-4 form-group">
              <label for=""> Request Date <span class="text-danger">*</span></label>
              <input type="date" name="purchase_order_date" id="purchase_order_date" value="{{old('purchase_order_date')}}" placeholder="yyyy-mm-dd" class="form-control" required>
            </div>
            <div class="col-md-4 form-group">
              <label for=""> Request Delivery Date <span class="text-danger">*</span></label>
              <input type="date" name="delivery_date" id="delivery_date" value="{{old('delivery_date')}}" placeholder="yyyy-mm-dd" class="form-control" required>
            </div>

            <div class="col-md-4 text-left">
              <div class="form-group">
                <label for="vendor" class="forget-form"> Priority <span class="text-danger">*</span></label>
                <select class="form-control vendor_group " name="priority" id="priority" required>
                  <option value="">Select Priority</option>
                  <option {{old('delivery_date') == 1?'selected':''}} value="1">High</option>
                  <option {{old('delivery_date') == 2?'selected':''}} value="2">Low</option>
                </select>
              </div>
            </div>
          </div><!-- end.row -->
          <hr>

          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped table-bordered table_field table-sm mb-0 table-responsive-lg table-responsive-xl" id="table_field">
                <thead>
                  <tr>
                    <th>Product Name</th>
                    <th>Generic Name</th>
                    <th>Unit</th>
                    <th>Vendor</th>
                    <th>Order Quantity</th>
                    <th>UOM</th>
                    <th>UOM Price</th>
                    <th>Discount (%)</th>
                    <th>Sub Total</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($hqrequisitons as $row)
                  <tr>
                    <td>
                      <select class="form-control" name="product_id[]" id="">
                        <option value="{{$row->product_id}}" readonly="">{{$row->product_name ?? ' '}}</option>
                      </select>
                    </td>
                    <td>
                      <input class="form-control" type="hidden" name="generic_id[]" value="{{$row->generic_id}}" readonly>
                      <input class="form-control" type="text" value="{{$row->generic_name}}" readonly>
                    </td>
                    <td>
                      <select class="form-control" name="unit_id[]">
                        <option value="{{$row->unit_id}}" readonly="">{{$row->unit_name}}</option>
                      </select>
                    </td>

                    <td>
                      <select class="form-control" name="vendor_id">
                        <option value="{{$row->vendor_id}}" readonly="">{{$row->vendor_name}}</option>
                      </select>
                    </td>

                    <td><input class="form-control" type="number" name="qty[]" id="qty_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->purchase_quantity}}" readonly=""></td>
                    <td>
                      <select class="form-control" name="uom_id[]">
                        <option value="{{$row->uom_id}}" readonly="">{{$row->uom_name??' '}}</option>
                      </select>
                    </td>
                    <td><input class="form-control" type="text" required name="uom_price[]" id="unit_price_{{$loop->index}}" data-id="{{$loop->index}}" value="{{ $row->purchase_price }}"></td>
                    <td><input class="form-control" type="number" name="discount[]" step="0.01" min="1" max="100" id="discount_{{$loop->index}}" data-id="{{$loop->index}}" value="{{ old('discount.$loop->index') }}"></td>
                    <td><input class="form-control sub_total text-center" type="number" name="product_sub_total[]" id="sub_total_{{$loop->index}}" data-id="{{$loop->index}}" value="{{ $row->purchase_quantity * $row->purchase_price }}" readonly=""></td>
                    <td>
                      <input class="form-control text-center" type="hidden" name="hq_approve_requisition_id" value="{{$row->hq_approve_requisition_id}}" readonly="">
                      <input class="form-control text-center" type="hidden" name="hub_id" value="{{$row->hub_id}}" readonly="">
                      <input class="form-control text-center" type="hidden" name="store_type" value="{{$row->store_type}}" readonly="">
                      <input class="form-control text-center" type="hidden" name="hub_requisition_id" value="{{$row->hub_requisition_id}}" readonly="">
                      <input class="form-control text-center" type="hidden" name="hub_requiquisition_no" value="{{$row->hub_requiquisition_no}}" readonly="">
                    </td>
                  </tr>
                  @endforeach
                  <tr class="mt-3">
                    <th colspan="8" class="text-right">Sub Total</th>
                    <td class="text-center"><input class="form-control text-center" id="sub_total" type="number" name="sub_total" value="{{old('sub_total')}}" readonly=""></td>
                  </tr>
                  <tr class="mt-3">
                    <th colspan="8" class="text-right">Overall Discount (TK)</th>
                    <td class="text-center"><input class="form-control text-center" step="0.01" id="overall_discount" type="number" name="overall_discount" value="{{old('overall_discount')}}"></td>
                  </tr>

                  <tr class="mt-3">
                    <th colspan="8" class="text-right">Total</th>
                    <td class="text-center"><input class="form-control text-center" id="total_price" type="number" name="total_price" value="{{old('total_price')}}" readonly></td>
                  </tr>

                  <tr class="mt-3">
                    <th colspan="8" class="text-right">Other Expense</th>
                    <td class="text-center"><input class="form-control text-center" id="other_expense" type="number" name="other_expense" value="{{old('other_expense')}}"></td>
                  </tr>
                  <tr class="mt-3">
                    <th colspan="8" class="text-right">Vat & Tax</th>
                    <td class="text-center"><input class="form-control text-center" id="vat_tax" type="number" name="vat_tax" value="{{old('vat_tax')}}"></td>
                  </tr>
                </tbody>
              </table>
            </div><!-- end.col-md-12 -->
            </div><!-- end.row -->
            <hr>

            <div class="row text-right">
              <div class="col-md-12">
                <button type="submit" class="btn create-btn mr-2" disabled>Confirm Vendor</button>
                <button type="submit" class="btn create-btn mr-2">Process Payment</button>
                <button type="submit" class="btn create-btn" disabled>Generate GRN</button>
              </div><!-- end.col-md-12 -->
            </div><!-- end.row -->
        </form>
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->



@endsection
@push('post_scripts')
<script>
  $(document).ready(function() {
    $('.form-control').on('input', function() {
      var id = $(this).data("id");
      var qty = $('#qty_' + id).val();

      var unit_price = $('#unit_price_' + id).val() ? $('#unit_price_' + id).val() : 0;
      var discount = $('#discount_' + id).val() ? $('#discount_' + id).val() : 0;
      var sub_total = $('#sub_total_' + id).val() ? $('#sub_total_' + id).val() : 0;
      var overall_discount = $('#overall_discount').val() ? $('#overall_discount').val() : 0;

      total_price = parseFloat(qty) * parseFloat(unit_price);
      total_discount = (parseFloat(total_price) * parseFloat(discount)) / 100;
      total = parseFloat(total_price) - parseFloat(total_discount)

      $('#sub_total_' + id).val(total);
      var total_sum = 0;
      $('.sub_total').each(function() {
        var inputval = $(this).val();
        if ($.isNumeric(inputval)) {
          total_sum += parseFloat(inputval);
        }
      })

      if ($.isNumeric(total_sum)) {
        $('#sub_total').val(parseFloat(total_sum));

        var total_price = parseFloat(parseFloat(total_sum) - parseFloat(overall_discount));
        $('#total_price').val(parseFloat(total_price));
      }
    })
  });
</script>

@endpush