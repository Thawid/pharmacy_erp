@extends('dboard.index')

@section('title','Edit Purchase Order')

@section('dboard_content')
@push('style')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="row align-items-center">
                    <div class="col-md-6 text-left">
                        <h2 class="title-heading">Edit Purchase Order</h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('purchase-orders.list')}}">Purchase
                            List</a>
                    </div>
                </div><!-- end.row -->
                <hr>

                <!-- info -->
                <div class="row">
                    <div class="col-md-4 form-group">
                        <p><strong>Purchase Order No: {{$purchase_order->purchase_order_no}}</strong></p>
                        <p><strong>HUB Requisition No: {{$purchase_order->hub_requisition_no}}</strong></p>
                        <p><strong>Vendor Name: {{$purchase_order->vendor->name ?? ' '}}</strong></p>
                    </div>
                    <div class="col-md-4 form-group">
                        <p><strong>Request Date: {{$purchase_order->purchase_order_date}}</strong></p>
                        <p><strong>Request Delivery Date: {{$purchase_order->requested_delivery_date}}</strong></p>
                        @if($purchase_order->priority == 1)
                        <P><strong>Priority: High </strong></P>
                        @elseif($purchase_order->priority == 2)
                        <P><strong>Priority: Low </strong></P>
                        @endif
                    </div>
                </div><!-- end.row -->
                <hr>

                <!-- form -->
                <form method="POST" action="{{route('purchase-order.update',$purchase_order->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for=""> Request Date *</label>
                            <input type="date" name="purchase_order_date" id="purchase_order_date" value="{{$purchase_order->purchase_order_date}}" placeholder="yyyy-mm-dd" class="form-control" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for=""> Request Delivery Date *</label>
                            <input type="date" name="delivery_date" id="delivery_date" value="{{$purchase_order->requested_delivery_date}}" placeholder="yyyy-mm-dd" class="form-control" required>
                        </div>

                        <div class="col-md-4 text-left">
                            <div class="form-group">
                                <label for="vendor" class="forget-form"> Priority *</label>
                                <select class="form-control vendor_group " name="priority" id="priority" required>
                                    <option value="">Select Priority</option>
                                    <option {{$purchase_order->priority == 1 ?'selected':''}} value="1">High</option>
                                    <option {{$purchase_order->priority == 2 ?'selected':''}} value="2">Low</option>
                                </select>
                            </div>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <div class="row">
                        <table class="table table-striped table-bordered table_field table-sm" id="table_field" style="margin-top: 10px">
                            <thead class="thead-light">
                                <tr class="bg-white">
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    <th width="10%">Unit</th>
                                    <th width="10%">Vendor</th>
                                    <th width="10%">UOM</th>
                                    <th>Ordered Quantity</th>
                                    <th>UOM Price</th>
                                    <th>Discount (%)</th>
                                    <th>Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_order->purchase_order_details as $row)
                                   {{-- {{ dd($row) }}--}}
                                <tr>
                                    <td>
                                        <select class="form-control" name="product_id[]" id="">
                                            <option value="{{$row->product_id}}" readonly="">{{$row->product->product_name ?? ' '}}</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input class="form-control" type="hidden" name="generic_id[]" value="{{$row->generic_id}}" readonly>
                                        <input class="form-control" type="text" value="{{$row->generic->name}}" readonly>
                                    </td>
                                    <td>
                                        <select class="form-control" name="unit_id[]">
                                            <option value="{{$row->unit_id}}" readonly="">{{$row->unit->unit_name}}</option>
                                        </select>
                                    </td>

                                    <td>
                                        <select class="form-control" name="vendor_id">
                                            <option value="{{$purchase_order->vendor_id}}" readonly="">{{$purchase_order ->vendor->name ?? ' '}}</option>
                                        </select>
                                    </td>

                                    <td>
                                        <select class="form-control" name="uom_id[]">
                                            <option value="{{$row->uom_id}}" readonly="">{{$row->uom->uom_name??' '}}</option>
                                        </select>
                                    </td>
                                    <td><input class="form-control" type="number" name="qty[]" id="qty_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->order_quantity}}" readonly=""></td>
                                    <td><input class="form-control" type="number" required name="uom_price[]" id="unit_price_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->uom_price}}"></td>
                                    <td><input class="form-control" type="number" name="discount[]" step="0.01" min="1" max="100" id="discount_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->discount !=0?$row->discount:''}}"></td>
                                    <td><input class="form-control sub_total text-center" type="number" name="product_sub_total[]" id="sub_total_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->sub_total}}" readonly=""></td>
                                </tr>
                                @endforeach
                                <tr class="mt-3">
                                    <th colspan="8" class="text-right">Sub Total</th>
                                    <td class="text-center"><input class="form-control text-center" id="sub_total" type="number" name="sub_total" value="{{$purchase_order ->sub_total}}" readonly=""></td>
                                </tr>
                                <tr class="mt-3">
                                    <th colspan="8" class="text-right">Overall Discount (TK)</th>
                                    <td class="text-center"><input class="form-control text-center" step="0.01" id="overall_discount" type="number" name="overall_discount" value="{{$purchase_order ->overall_discount}}"></td>
                                </tr>

                                <tr class="mt-3">
                                    <th colspan="8" class="text-right">Total</th>
                                    <td class="text-center"><input class="form-control text-center" id="total_price" type="number" name="total_price" value="{{$purchase_order ->total}}" readonly></td>
                                </tr>

                                <tr class="mt-3">
                                    <th colspan="8" class="text-right">Other Expense</th>
                                    <td class="text-center"><input class="form-control text-center" id="other_expense" type="number" name="other_expense" value="{{$purchase_order ->other_expense}}"></td>
                                </tr>


                                <tr class="mt-3">
                                    <th colspan="8" class="text-right">Vat & Tax</th>
                                    <td class="text-center"><input class="form-control text-center" id="vat_tax" type="number" name="vat_tax" value="{{$purchase_order ->vat_tax}}"></td>
                                </tr>
                                <td><input class="form-control text-center" type="hidden" name="hq_id" value="{{$purchase_order->hq_requisition_id}}" readonly=""></td>
                            </tbody>
                        </table>
                        <div>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <div class="row text-right">
                        <div class="col-md-12">
                        <button type="submit" class="btn create-btn">Save & Update</button>
                    </div>
                </form>
            </div><!-- end.tile-bdoy -->
        </div><!-- end.tile -->
    </div><!-- end.col-md12 -->
</div><!-- end.row -->




@endsection
@push('post_scripts')
<script>
    $(document).ready(function() {
        $('.form-control').on('input', function() {
            var id = $(this).data("id");
            var qty = $('#qty_' + id).val();

            var unit_price = $('#unit_price_' + id).val() ? $('#unit_price_' + id).val() : 0;
            var discount = $('#discount_' + id).val() ? $('#discount_' + id).val() : 0;
            var sub_total = $('#sub_total_' + id).val() ? $('#sub_total_' + id).val() : 0;
            var overall_discount = $('#overall_discount').val() ?$('#overall_discount').val():0;

            total_price = parseFloat(qty) * parseFloat(unit_price);
            total_discount = (parseFloat(total_price) * parseFloat(discount)) / 100;
            total = parseFloat(total_price) - parseFloat(total_discount)

            $('#sub_total_' + id).val(total);
            var total_sum = 0;
            $('.sub_total').each(function() {
                var inputval = $(this).val();
                if ($.isNumeric(inputval)) {
                    total_sum += parseFloat(inputval);
                }
            })

            if($.isNumeric(total_sum)){
                $('#sub_total').val(parseFloat(total_sum));

                var total_price = parseFloat(parseFloat(total_sum) - parseFloat(overall_discount));
                $('#total_price').val(parseFloat(total_price));
            }
        })
    });
</script>

@endpush
