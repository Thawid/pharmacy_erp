@extends('dboard.index')

@section('title','Purchase Order Details')

@section('dboard_content')
@push('styles')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<style>
  @media print {
    @page {
      margin: 20px;
    }

  }
</style>
@endpush

<div id="printableArea">
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <!-- tile -->
          <div class="row align-items-center">
            <div class="col-md-6">
              <h2 class="title-heading"> Purchase Order Details</h2>
            </div><!-- end.col-md-6 -->

            <div class="col-md-6 text-right">
              <a class="btn index-btn" href="{{route('purchase-orders.list')}}">Purchase
                List</a>
            </div>
          </div><!-- end.row -->
          <hr>

          <div class="row">
            <div class="col-md-4 form-group">
              <p><strong>Purchase Order
                  No: {{$order_details->purchase_order_no}}</strong></p>
              <p><strong>HUB Requisition
                  No: {{$order_details->hub_requisition_no}}</strong></p>
              <p><strong>Vendor Name: {{$order_details->vendor->name ?? ' '}}</strong></p>
              <p><strong>GRN Status : {{ $order_details->grn_receive_status }}</strong></p>
            </div>
            <div class="col-md-4 form-group">
              <p><strong>Request Date: {{$order_details->purchase_order_date}}</strong></p>
              <p><strong>Request Delivery Date: {{$order_details->requested_delivery_date}}</strong></p>
              @if($order_details->priority == 1)
              <P><strong>Priority: High </strong></P>
              @elseif($order_details->priority == 2)
              <P><strong>Priority: Low </strong></P>
              @endif
            </div>
          </div><!-- end.row -->
          <hr>

          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-hover table-striped table-responsive-lg table-responsive-xl">
                <thead>
                  <tr>
                    <th>Product Name</th>
                    <th>Generic Name</th>
                    <th>Unit</th>
                    <th>UOM</th>
                    <th>Order Quantity</th>
                    <th>Delivered Quantity</th>
                    {{--<th>Remain Quantity</th>--}}
                    <th>Unit Price</th>
                    <th>Discount</th>
                    <th>Sub Total</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($order_details->purchase_order_details as $row)
                  <tr>
                    <td>{{$row->product->product_name ?? ' '}}</td>
                    <td>{{$row->generic->name ?? ' '}}</td>
                    <td>{{$row->unit->unit_name ?? ' '}}</td>
                    <td>{{$row->uom->uom_name}}</td>
                    <td>{{$row->order_quantity}}</td>
                    <td>{{$row->delivered_quantity ?? 0}}</td>
                    {{--<td>{{$row->remain_quantity ?? 0}}</td>--}}
                    <td>{{$row->uom_price}}</td>
                    <td>{{$row->discount}}</td>
                    <td>{{$row->sub_total}}</td>
                  </tr>
                  @endforeach
                  <tr>
                    <td colspan="8" class="text-right">Sub Total</td>
                    <td>{{$order_details->sub_total}}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-right">Overall Discount</td>
                    <td>{{$order_details->overall_discount}}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-right">Total</td>
                    <td>{{$order_details->total}}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-right">Other Expense</td>
                    <td>{{$order_details->other_expense}}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-right">Vat & Tax</td>
                    <td>{{$order_details->vat_tax}}</td>
                  </tr>
                </tbody>
              </table>
            </div><!-- end.col-md-12 -->
          </div><!-- end.row -->
          <hr>

          <div class="row text-right">
            <div class="col-md-12">
              <button class="btn index-btn" onclick="printDiv('printableArea')">Print</button>
            </div><!-- end.col-md-12 -->
          </div><!-- end.row -->
        </div><!-- end.tile-body -->
      </div><!-- end.tile -->
    </div><!-- end.col-md-12 -->
  </div><!-- end.row -->
</div><!-- end#printableArea -->


@endsection
@push('post_scripts')
<script>
  function printDiv(divId) {
    var printContents = document.getElementById(divId).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
  }
</script>
@endpush