@extends('dboard.index')

@section('title','View Requisition List Interface')
@push('styles')
    <style>
    </style>
@endpush
@section('dboard_content')
<div class="tile">
    <div class="tile-body">
        <div class="row">
            <div class="col-lg-16">
            <a class="nav-link {{ request()->is('procurement/purchase-order/process') ? 'active' : '' }} " href="{{route('purchase-orders.list')}}"><h4>Purchase Order List</h4></a>
            </div>
            <div class="col-md text-md-right">
                <!-- <a class="btn btn-outline-primary icon-btn" href={{route('procurehq.create')}}><i class="fa fa-arrow-right"></i> Purchase List</a> -->
            </div>
        </div>

    </div>
</div>
@yield('contents')

@endsection
@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();
    });
</script>

@endpush
