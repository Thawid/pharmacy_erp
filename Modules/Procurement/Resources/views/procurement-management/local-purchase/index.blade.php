@extends('dboard.index')
@section('title','Store Requisition List')
@push('styles')
    <style>

    </style>
@endpush
@section('dboard_content')

<div class="tile">
    <div class="tile-body">
        <!-- title -->
        <div class="row align-items-center">
            <div class="col-md-6">
                <h2 class="title-heading">Local Purchase List </h2>
            </div>
            <div class="col-md-6 text-right">
                <a class="btn create-btn" href={{route('local-purchase.create')}}>Create Local Purchase </a>
            </div>
        </div><!-- end.row -->
        <hr>
 <div class="table-responsive bg-light p-3 ">
    <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
        <thead>
            <tr>
                <th>SL</th>
                <th>Details</th>
                <th>Store type</th>
                <th width="8%">Action</th>
            </tr>
        </thead>
        <tbody>

         @if(isset($local_purchase))
         @foreach($local_purchase as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                    <address>
                        <strong>Product name:  {{$row->details[0]->product->product_name}} </strong><br>
                        <strong>Purchase_No: {{$row->purchase_order_no}} </strong><br>
                                Created Date: {{$row->created_at}} <br>
                    </address>
                </td>

                <td> <strong>{{$row->store_type}}</strong></td>
                <td>
                    <div class="d-flex justify-content-around align-items-center">

                        <a href="{{route('local-purchase.show',$row->id)}}" class="btn details-btn mr-1" type="button" name="" id=""><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                        @if($row->status == 0)
                        <a href="{{route('local-purchase.edit',$row->id)}}" class="btn edit-btn" type="button" name="" id=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a>
                        @else
                            <a href="javascript:void(0);" class="btn edit-btn" type="button" disabled=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a>
                        @endif
                    </div>
                    <!-- <button class="btn btn-outline-danger btn-sm" type="button" name="remove" id="remove"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button> -->
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection
@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#requisitionList').DataTable();
    });
</script>
@endpush
