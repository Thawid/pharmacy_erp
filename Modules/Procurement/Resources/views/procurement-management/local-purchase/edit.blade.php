@extends('dboard.index')

@section('title','Edit Purchase Order')

@section('dboard_content')
@push('style')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="row align-items-center">
                    <div class="col-md-6 text-left">
                        <h2 class="title-heading">Edit Local Purchase Order</h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('local-purchase.index')}}"> Local Purchase List
                            </a>
                    </div>
                </div><!-- end.row -->
                <hr>


                <hr>

                <!-- form -->
                <form method="POST" action="{{route('local.purchase.update',$local_purchase->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    {{-- {{ dd($local_purchase) }} --}}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table_field table-sm w-100" id="table_field" style="margin-top: 10px">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Generic Name</th>
                                            <th>Purchase Quantity</th>
                                            <th width="10%">Unit</th>
                                            <th width="10%">UOM</th>
                                            <th>UOM Price</th>
                                            <th>Discount (%)</th>
                                            <th>Sub Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($local_purchase->details as $row)
                                        {{-- {{ dd($row) }} --}}
                                        <tr class="w-100">
                                            <td>
                                                <select class="form-control" name="product_id[]" id="">
                                                    <option value="{{$row->product_id}}" readonly="">{{$row->product->product_name ?? ' '}}</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control" type="hidden" name="generic_id[]" value="{{$row->generic_id}}" readonly>
                                                <input class="form-control" type="text" value="{{$row->generic->name}}" readonly>
                                            </td>
                                            <td><input class="form-control" type="number" name="qty[]" id="qty_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->purchase_qty}}" readonly=""></td>
                                            <td>
                                                <select class="form-control" name="unit_id[]">
                                                    <option value="{{$row->unit_id}}" readonly="">{{$row->unit->unit_name}}</option>
                                                </select>
                                            </td>

                                            <td>
                                                <select class="form-control" name="uom_id[]">
                                                    <option value="{{$row->uom_id}}" readonly="">{{$row->product_uom->uom_name}}</option>
                                                </select>
                                            </td>
                                            <td><input class="form-control" type="number" required name="uom_price[]" id="unit_price_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->uom_price}}"></td>
                                            <td><input class="form-control" type="number" name="discount[]" step="0.01" min="1" max="100" id="discount_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->discount !=0?$row->discount:''}}"></td>
                                            <td><input class="form-control sub_total text-center" type="number" name="product_sub_total[]" id="sub_total_{{$loop->index}}" data-id="{{$loop->index}}" value="{{$row->sub_total}}" readonly=""></td>
                                        </tr>
                                        @endforeach

                                        <tr class="mt-3">
                                            <th colspan="7" class="text-right">Sub Total</th>
                                            <td class="text-center"><input class="form-control text-center" id="sub_total" type="number" name="sub_total" value="{{$local_purchase->sub_total}}" readonly=""></td>
                                        </tr>
                                        <tr class="mt-3">
                                            <th colspan="7" class="text-right">Overall Discount (TK)</th>
                                            <td class="text-center"><input class="form-control text-center" step="0.01" id="overall_discount" type="number" name="overall_discount" value="{{$local_purchase->discount}}"></td>
                                        </tr>

                                        <tr class="mt-3">
                                            <th colspan="7" class="text-right">Total</th>
                                            <td class="text-center"><input class="form-control text-center" id="total_price" type="number" name="total_price" value="{{ $local_purchase->total }}" readonly></td>
                                        </tr>

                                        {{-- <tr class="mt-3">
                                            <th colspan="7" class="text-right">Other Expense</th>
                                            <td class="text-center"><input class="form-control text-center" id="other_expense" type="number" name="other_expense" value="{{ $local_purchase->other_expense }}"></td>
                                        </tr> --}}


                                        {{-- <tr class="mt-3">
                                            <th colspan="7" class="text-right">Vat & Tax</th>
                                            <td class="text-center"><input class="form-control text-center" id="vat_tax" type="number" name="vat_tax" value="{{$local_purchase->vat_tax}}"></td>
                                        </tr> --}}
                                        {{-- <td class=""><input class="form-control text-center" type="hidden" name="hq_id" value="" readonly=""></td> --}}
                                    </tbody>
                                </table>

                            </div><!-- table-responsive -->
                        </div><!-- col-md-12 -->
                    </div><!-- end.row -->
                    <hr>

                    <div class="row text-right">
                        <div class="col-md-12">
                        <button type="submit" class="btn create-btn">Save & Update</button>
                    </div>
                </form>
            </div><!-- end.tile-bdoy -->
        </div><!-- end.tile -->
    </div><!-- end.col-md12 -->
</div><!-- end.row -->




@endsection
@push('post_scripts')
<script>
    $(document).ready(function() {
        $('.form-control').on('input', function() {
            var id = $(this).data("id");
            var qty = $('#qty_' + id).val();

            var unit_price = $('#unit_price_' + id).val() ? $('#unit_price_' + id).val() : 0;
            var discount = $('#discount_' + id).val() ? $('#discount_' + id).val() : 0;
            var sub_total = $('#sub_total_' + id).val() ? $('#sub_total_' + id).val() : 0;
            var overall_discount = $('#overall_discount').val() ?$('#overall_discount').val():0;

            total_price = parseFloat(qty) * parseFloat(unit_price);
            total_discount = (parseFloat(total_price) * parseFloat(discount)) / 100;
            total = parseFloat(total_price) - parseFloat(total_discount)

            $('#sub_total_' + id).val(total);
            var total_sum = 0;
            $('.sub_total').each(function() {
                var inputval = $(this).val();
                if ($.isNumeric(inputval)) {
                    total_sum += parseFloat(inputval);
                }
            })

            if($.isNumeric(total_sum)){
                $('#sub_total').val(parseFloat(total_sum));

                var total_price = parseFloat(parseFloat(total_sum) - parseFloat(overall_discount));
                $('#total_price').val(parseFloat(total_price));
            }
        })
    });
</script>

@endpush
