@extends('dboard.index')

@section('title','Create Regular Requisition')

@section('dboard_content')
@push('style')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
@endpush
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Add Local Purchase</h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('local-purchase.index')}}">Back</a>
                    </div><!-- end.col-md-6 -->
                </div><!-- end.row -->
                <hr>
               


                <form method="POST" action="{{ route('local-purchase.store') }}" id="local-purchase" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label for=""> Request Date *</label>
                                    <input type="date" name="purchase_date" id="purchase_date" value="" placeholder="yyyy-mm-dd" class="form-control" required>
                                </div>
                            </div><!-- end.row -->
                        </div><!-- end.col-md-12 -->

                        <div class="col-md-12 mt-3"  id="">
                            <div id="showdata">
                                <div class="row">
                                    <div class="col-md-12">
                                            <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                                <thead>
                                                    <tr>
                                                        <th>Product Name</th>
                                                        <th>Generic Name</th>
                                                        <th>Unit</th>
                                                        <th>Quantity</th>
                                                        <th>UOM</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select class="form-control product select2" onchange="getProduct(this)" name="product[]" data-id="0" required>
                                                                <option value="" selected disabled>--- Select a Product ----</option>
                                                                @foreach($products as $row)
                                                                <option value="{{$row->product_id}}">{{$row->product_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            {{-- <input type="hidden" name="purchase_price[]" class="purchase_price_0" id="purchase_price_0"> --}}
                                                            {{-- <input type="hidden" name="overall_discount[]" class="overall_discount_0" id="overall_discount_0">
                                                            <input type="hidden" name="total[]" class="total_0" id="total_0"> --}}
                                                        </td>
                                                        <td>
                                                            <input class="form-control generic_id_0" id="generic_id_0" type="hidden" name="generic_id[]" required="">
                                                            <input class="form-control generic_name_0" id="generic_name_0" type="text" name="generic_name[]" required="">
                                                        </td>

                                                        <td>
                                                            <input class="form-control unit_id_0" type="hidden" name="unit_id[]" id="unit_id_0" required="">
                                                            <input class="form-control unit_0" disabled type="text" name="unit[]" id="unit_0" required="">
                                                        </td>
                                                        <td>
                                                            <input class="form-control purchase_qty" type="number" name="purchase_qty[]" id="purchase_qty_0" required="">
                                                        </td>
                                                        <td>
                                                            <select class="form-control uom_0" name="uom_id[]" id="uom_0">
                                                                <option selected disabled>---Select A UOM---</option>
                                                                @foreach($uom as $row)
                                                                <option value="{{$row->id}}">{{$row->uom_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>

                                                        <td>
                                                            <input class="btn w-100 index-btn" type="button" name="add" id="add" value="+">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                        </table>
                                    </div><!-- end.col-md-12 -->
                                </div><!-- end.row -->
                            </div><!-- end.showdata -->
                        </div><!-- end.col-md-12 -->

                            <!-- preloader area start -->
                            <div class="tile" id="preloader">
                                <div class="tile-body">
                                    <div class="row text-center">
                                        <div class="col-md-12">
                                        <h5 class="text-center">Processing....</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="col-sm-12 mt-2 text-right">
                                <button type="submit" class="btn create-btn">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->



@endsection
@push('post_scripts')

<!-- js script start from here -->
<script type="text/javascript" src="{{asset('js/plugins/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript">
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
</script>



<script type="text/javascript">
    $('.select2').select2({});
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var x = 1;

        $("#add").click(function() {

            var html = `'<tr>' + '<td> <select class="form-control product select2" onchange="getProduct(this)" data-id="${x}" name="product[]" id="product"><option selected disabled>--- Select a Product ----</option>@foreach($products as $row)<option value="{{$row->id}}">{{$row->product_name}}</option>@endforeach</select>
            <input type="hidden" name="purchase_price[]" class="purchase_price_${x}" id="purchase_price_${x}"></td>'+
                    '<td><input class="form-control generic_id_${x}"  type="hidden" name="generic_id[]" id="generic_id_${x}" required=""><input class="form-control generic_name_${x}" id="generic_name_${x}" type="text" value="" name="generic_name[]" required="" id="generic${x}"></td>' +
                    ' <td><input class="form-control unit_id_${x}"  type="hidden" name="unit_id[]" id="unit_id_${x}" required=""><input class="form-control unit_${x}" type="text" name="unit[]" id="unit_${x}" required=""></td>+
                    '<td><input class="form-control purchase_qty_${x}" type="number" name="purchase_qty[]" required="" id="purchase_qty_${x}"></td>' +<td><select class="form-control uom_${x}" name="uom_id[]" id="uom_${x}"><option selected disabled>---Select A UOM---</option>@foreach($uom as $row)
                                                                <option value="{{$row->id}}">{{$row->uom_name}}</option>
                                                                @endforeach</select></td>'
                    '<td><input class="btn btn-outline-danger btn-sm" type="button" name="remove" id="remove" value="x"></td>' + '</tr>'`;
            $("#table_field").append(html);
            x++;

            $('.select2').select2({
                width: '100%'
            });
        });

        $("#table_field").on('click', "#remove", function() {

            $(this).closest('tr').remove();
        })
    });
</script>


<script>
    $(document).ready(function(){
        $('#preloader').hide();
    });

    function getAjaxReq(e){
        var id = $(e).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: "{{ url('/procurement/get-product') }}/" + id,
            dataType: "json",
            success: function(res) {
                console.log(res);
                $('#showdata').show();
                $('#preloader').hide();
                var selectorID = $(e).attr('data-id');

                if(res.purchase_price){
                    $(`.purchase_price_${selectorID}`).val(res.purchase_price);
                }
                if (res.pgeneric.name) {
                    $(`.generic_name_${selectorID}`).val(res.pgeneric.name)
                    $(`.generic_id_${selectorID}`).val(res.pgeneric.id)
                } else {
                    $(`.generic_name_${selectorID}`).val('not found!')
                    $(`.generic_id_${selectorID}`).val('not found!')
                }

                if (res.product_unit.unit_name) {
                    $(`.unit_${selectorID}`).val(res.product_unit.unit_name)
                    $(`.unit_id_${selectorID}`).val(res.product_unit.id)
                } else {
                    $(`.unit_${selectorID}`).val('not found!')
                    $(`.unit_id_${selectorID}`).val('not found!')
                }
                // $(`.uom_${selectorID}`).empty();

                // $(`.uom_${selectorID}`).append('<option value="' + res.product_uom.id + '">' + res.product_uom.uom_name + '</option>');
                // $(`.vendor_${selectorID}`).empty();
                // $.each(res.vendors, function(index, vendorlist) {
                //     //console.log(vendorlist)
                //     $(`.vendor_${selectorID}`).append('<option value="' + vendorlist.vendor_name.id + '">' + vendorlist.vendor_name.name + '</option>');
                // });



            }
        });
    }

    function getProduct(e) {
        var id = $(e).val();

        let productarray = [];
        const products = document.getElementsByClassName('product');
        var count = true;
        for( const product of products ) {
            //console.log(product.parentElement.parentElement);
            if(productarray.indexOf(product.value) == '-1'){
                productarray.push(product.value);
                count = true;


            }else{
                count = false;
                product.parentElement.parentElement.remove();
                alert('This product is already exists');

            }

        }

        if(count == true){
            $('#showdata').hide();
            $('#preloader').show();
            getAjaxReq(e);
        }
        //console.log(productarray);
        //console.log(count);

    }
</script>
<!-- Get Product Item -->

<!-- select2 -->

<script>
    $(document).ready(function() {
        $('.select2').select2({
            width: '100%'
        });
    });
</script>

<script>
    if (localStorage.getItem("parent")) {
        //document.getElementById("parent").innerHTML = localStorage.getItem("parent");
    }
    setInterval(function () {
        //localStorage.setItem("parent", document.getElementById("parent").innerHTML);

    }, 2000)
</script>

<script>
    $('select[name=product]').on('change', function(){
        /*var pid = $(this).val();
        alert(pid)
        var table = $('#table_field');
        var check_value = $(table).find("tr").data('id');
        if(check_value == pid) {
            console.log("ID exist")
        }
        else {
            console.log("Not exist");
        }*/
    });
</script>
@endpush
