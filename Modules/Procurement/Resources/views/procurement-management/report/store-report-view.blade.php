<div id="printReport" class="row">
    <div class="col-md-12">
        <div class="table-responsive">
    <table class="table table-bordered table-striped table-hover vendor-list" id="vendorList">
        <thead>
        <tr>
            <th>SL</th>
            <th>Req. Info</th>
            <th>Store</th>
            <th>Amount</th>
            <th>Priority</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
       {{-- {{ dd($data) }}--}}
        @if(isset($data))
            @foreach($data as $report)
                <tr>
                    <td> {{ $loop->iteration }}</td>
                    <td>
                        <strong> Req. No {{ $report->store_requisition_no }}</strong> <br>
                        <strong> Order Date {{ $report->requisition_date }}</strong><br>
                        <strong> Req. Delivery Date {{ $report->delivery_date }}</strong>
                    </td>
                    <td> {{ $report->name }}</td>
                    <td>{{ $report->purchase_price }}</td>
                    <td>{{ $report->priority }}</td>
                    <td>{{ $report->status }}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
    </div>
</div>
