@extends('dboard.index')

@section('title','Store Report | Procurement')

@section('dboard_content')
    @push('styles')
        <style>
            @media print {
                @page {
                    margin: 20px;
                }

            }
            table {
                text-align: center;
                margin: 0;
                padding: 0;
                width: 100%;
            }

            .th-table-header, .th-table-info, .th-table-body {
                margin-bottom: 20px;
            }

            .th-table-header table tr td h1 {
                margin: 0;
                font-size: 35px;
            }

            .th-table-header table tr td h2 {
                margin: 0;
                font-size: 18px;
            }

            .th-table-header table tr td h3 {
                padding: 0;
                margin: 0;
                font-size: 24px;
            }

            .th-table-info p {
                margin: 0;
            }

            .th-table-info table tr td p {
                text-align: left;
            }
            .th-table-info table {
                text-align: center !important;
            }

            .th-table-header table tr td p {
                padding: 0;
                margin: 0;
                font-size: 16px;
            }

        </style>

    @endpush
    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h2 class="title-heading"> Expiry Report </h2>
                </div>
            </div><!-- end.row -->
            <hr>

            <!-- form -->
            <form class="" method="post">
                <meta name="csrf-token" content="{{ csrf_token() }}"/>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="product_id" class="">Product</label>
                            <select class="form-control select2" name="product_id" id="product_id">
                                <option value="0">Select Product</option>
                                <option value="">All Product</option>
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}"> {{ $product->product_name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" id="hidden" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="product_category_id" class="">Product Category</label>
                            <select class="form-control select2" name="product_category_id" id="product_category_id">
                                <option value="0">Select Product Category</option>
                                @foreach($productCategory as $category)
                                    <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="manufacturer_id" class="">Manufacturer</label>
                            <select class="form-control select2" name="manufacturer_id" id="manufacturer_id">
                                <option value="0">Select Manufacturer</option>
                                @foreach($manufacturers as $manufacturer)
                                    <option value="{{ $manufacturer->id }}"> {{ $manufacturer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="expire_within" class="">Expire Within</label>
                            <select class="form-control select2" name="expire_within" id="expire_within">
                                <option value="0">Select Expire Within</option>
                                <option value="7">Next 7 Days</option>
                                <option value="15">Next 15 Days</option>
                                <option value="30">Next 30 Days</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="start_date" class="">Date From</label>
                            <input type="date" name="start_date" id="start_date" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="end_date" class="">Date To</label>
                            <input type="date" name="end_date" id="end_date" class="form-control">
                        </div>
                    </div>
                    
                </div><!-- end.row -->
                <hr>
                <div class="row">
                <div class="col-md-12 text-right">
                        <div class="reset_action m-0">
                            <input type='button' class="btn index-btn" value='Search' id='send'>
                        </div>
                    </div>
                </div>
            </form>
            <hr>
            <div id="result"></div>
        </div><!-- tile-bdoy -->
    </div><!-- end.tile -->



@endsection

@push('post_scripts')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script type="text/javascript">$('.select2').select2();</script>
    <script>
        /*var dataTable = $('.vendor-list').DataTable({
            processing: true,
            serverSide: true,
        });*/

        function sendDataUsingjQuery() {
            //let product_name = $( "#product_id option:selected" ).text();
            //console.log(product_name)
            let params = {
                "product_id": $("#product_id").val(),
                "product_category_id": $("#product_category_id").val(),
                "manufacturer_id": $("#manufacturer_id").val(),
                "expire_within": $("#expire_within").val(),
                "start_date": $("#start_date").val()!=='mm/dd/yyyy'?$("#start_date").val():'',
                "end_date": $("#end_date").val(),
                "product_name" : $( "#product_id option:selected" ).text()!=='Select Product'?$( "#product_id option:selected" ).text():'',
                "product_category" : $( "#product_category_id option:selected" ).text()!=='Select Product Category'?$( "#product_category_id option:selected" ).text():'',
                "manufacture_name" : $( "#manufacturer_id option:selected" ).text()!=='Select Manufacturer'?$( "#manufacturer_id option:selected" ).text():'',
                "expire_with_in" : $( "#expire_within option:selected" ).text()!=='Select Expire Within'?$( "#expire_within option:selected" ).text():'',

            }
            console.log(params);
            $.ajax({
                "method": "POST",
                "url": '{{route('get.product.expiry.report')}}',
                "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                "data": {
                    data: {
                        "product_id": params.product_id,
                        "product_category_id": params.product_category_id,
                        "start_date": params.start_date,
                        "end_date": params.end_date,
                        "manufacturer_id":params.manufacturer_id,
                        "expire_within":params.expire_within,
                        "product_name":params.product_name,
                        "product_category":params.product_category,
                        "manufacture_name":params.manufacture_name,
                        "expire_with_in":params.expire_with_in,
                    }
                }
            }).done(function (response) {
                $("#result").html(response);
            });
            return false;
        }

        document.getElementById("send").addEventListener("click", function () {
            sendDataUsingjQuery();
        });

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endpush
