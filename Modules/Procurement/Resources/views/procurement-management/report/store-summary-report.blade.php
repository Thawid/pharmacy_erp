@extends('dboard.index')

@section('title','Store Report | Procurement')

@section('dboard_content')
    @push('styles')
        <style>
            @media print {
                @page {
                    margin: 20px;
                }
            }
            table {
                text-align: center;
                margin: 0;
                padding: 0;
                width: 100%;
            }

            .th-table-header, .th-table-info, .th-table-body {
                margin-bottom: 20px;
            }

            .th-table-header table tr td h1 {
                margin: 0;
                font-size: 35px;
            }

            .th-table-header table tr td h2 {
                margin: 0;
                font-size: 18px;
            }

            .th-table-header table tr td h3 {
                padding: 0;
                margin: 0;
                font-size: 24px;
            }

            .th-table-info p {
                margin: 0;
            }

            .th-table-info table tr td p {
                text-align: left;
            }
            .th-table-info table {
                text-align: center !important;
            }

            .th-table-header table tr td p {
                padding: 0;
                margin: 0;
                font-size: 16px;
            }

        </style>

    @endpush
    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-6 text-left">
                    <h2 class="title-heading">Store Summary Report </h2>
                </div>
            </div><!-- end.row -->
            <hr>
            <!-- form -->
            <form class="" method="post">
                <meta name="csrf-token" content="{{ csrf_token() }}"/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="store_id" class="">Store</label>
                            <select class="form-control select2" name="store_id" id="store_id">
                                <option value="{{ $store->id }}"> {{ $store->name ?? '' }}</option>
                            </select>
                            <input type="hidden" id="hidden" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="product_id" class="">Product</label>
                            <select class="form-control select2" name="product_id" id="product_id">
                                <option value="0">Select Product</option>
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}"> {{ $product->product_name ?? '' }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" id="hidden" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="reset_action">
                            <input type='button' class="btn index-btn" value='Search' id='send'>
                        </div>
                    </div>
                </div><!-- end.row -->
            </form>
            <hr>
            <div id="result"></div>
        </div><!-- end.tile-body -->
    </div><!-- end.tile -->

@endsection

@push('post_scripts')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script type="text/javascript">$('.select2').select2();</script>
    <script>

        function sendDataUsingjQuery() {
            let params = {
                "store_id": $("#store_id").val(),
                "product_id": $("#product_id").val(),
            }
            console.log(params);
            $.ajax({
                "method": "POST",
                "url": '{{route('get-store-summary-report')}}',
                "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                "data": {
                    data: {
                        "store_id": params.store_id,
                        "product_id": params.product_id,
                    }
                }
            }).done(function (response) {
                $("#result").html(response);
            });
            return false;
        }

        document.getElementById("send").addEventListener("click", function () {
            sendDataUsingjQuery();
        });

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
@endpush
