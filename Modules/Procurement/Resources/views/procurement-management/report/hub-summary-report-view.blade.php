<div id="printableArea">
    <div class="th-table-header">
        <table>
            <tr>
                <td><h3><strong> Alesha Permachy    </strong></h3></td>
            </tr>
            <tr>
                <td> <p>  <strong>Hub Summary Report</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong> @if(isset($warehouse_name)) Warehouse Name : {{ $warehouse_name->name ?? '' }} @endif</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong> @if(isset($product_name)) Product Name : {{ $product_name->product_name ?? '' }} @endif</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong>Report Created : @php echo date('d-m-Y'); @endphp </strong> </p></td>
            </tr>

        </table>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover vendor-list" id="vendorList">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Name</th>
                        <th>Generic Name</th>
                        <th>Manufacturer</th>
                        <th>Available Stock</th>
                        <th>Price</th>
                        <th>Store Price</th>
                        <th>UOM</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            @foreach($data as $report)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $report->product_name }}</td>
                                    <td>{{ $report->generic_name }}</td>
                                    <td>{{ $report->manufacturer_name }}</td>
                                    <td>{{ $report->available_quantity }}</td>
                                    <td>
                                        @if($report->available_quantity !== 0)
                                            @foreach($uom_prices as $uom_price)
                                                @if(!empty($uom_price) && $report->id === $uom_price->product_id)
                                                    {{ $uom_price->uom_price * $report->available_quantity ?? 0 }}
                                                @endif
                                            @endforeach
                                        @else
                                            {{ 0 }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($report->available_quantity !== 0)
                                            @foreach($purchase_order_details as $purchase_order)
                                                @if(!empty($purchase_order) && $report->id === $purchase_order->product_id)
                                                    {{ $purchase_order->uom_price * $report->available_quantity ?? 0 }}
                                                @endif
                                            @endforeach
                                        @else
                                            {{ 0 }}
                                        @endif
                                    </td>
                                    <td>{{ $report->uom_name }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>No Data Found</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div><!-- end.table-res -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
</div><!-- end#printable -->

<hr>
<div class="row text-right">
    <div class="col-md-12">
        <button class="btn index-btn" onclick="printDiv('printableArea')">
        Print
    </button>
    </div>
</div>
