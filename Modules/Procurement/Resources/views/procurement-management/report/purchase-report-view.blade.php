<div id="printReport" class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="purchaseReport">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Hub</th>
                    <th>Vendor</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                 {{--{{ dd($data) }}--}}
                @if(!empty($data))
                    @foreach($data as $report)
                        <tr>
                            <td> {{ $loop->iteration }}</td>

                            <td> {{ $report->hub_name }}</td>
                            <td>{{ $report->vendor_name }}</td>
                            <td>{{ $report->total }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>No Data Found</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->
