@extends('dboard.index')

@section('title','Store Report | Procurement')

@section('dboard_content')
    @push('styles')
        <style>
            @media print {
                @page {
                    margin: 20px;
                }

            }
            </style>
    @endpush
    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-6 text-left">
                    <h2 class="title-heading">Requisition Summary Report </h2>
                </div>
            </div><!-- end.row -->
            <hr>

            <!-- form -->
            <form class="" method="post">
                <meta name="csrf-token" content="{{ csrf_token() }}"/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="store_id" class="">Select Store</label>
                            <select class="form-control select2" name="store_id" id="store_id">
                                <option value="0">Select Store</option>
                                @foreach($stores as $store)
                                    <option value="{{ $store->id }}"> {{ $store->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="start_date" class="">Date From</label>
                            <input type="date" name="start_date" id="start_date" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="end_date" class="">To From</label>
                            <input type="date" name="end_date" id="end_date" class="form-control">
                        </div>
                    </div>
                </div><!-- end.row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="priority" class="forget-form">Select Priority</label>
                            <select class="form-control select2" name="priority" id="priority">
                                <option value="">Select Priority</option>
                                <option value="High">High</option>
                                <option value="Low">Low</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="status" class="">Select Status</label>
                            <select class="form-control select2" name="status" id="status">
                                <option value="">Select Status</option>
                                <option value="Approved">Approved</option>
                                <option value="Pending">Pending</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="reset_action">
                            <input type='button' class="btn index-btn mr-2" value='Filter' id='send'>
                            <a href="#" class="btn create-btn mr-2" type="button" id="btnExport" onclick="Export()">Pdf </a>
                            <a href="#" class="btn create-btn" type="button" id="print_btn" onclick="printDiv('printReport')">Print
                            </a>
                        </div>
                    </div>
                </div><!-- end.row -->
            </form>
            <hr>
            <div id="result"></div>

        </div><!-- end.tile-body -->
    </div><!-- end.tile -->




@endsection

@push('post_scripts')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/html2canvas.min.js')}}"></script>

    <script type="text/javascript">
        function Export() {
            html2canvas(document.getElementById('vendorList'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Summary Report.pdf");
                }
            });
        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    <script type="text/javascript">$('.select2').select2();</script>
    <script>
        /*var dataTable = $('.vendor-list').DataTable({
            processing: true,
            serverSide: true,
        });*/

        function sendDataUsingjQuery() {

            let params = {
                "store_id": $("#store_id").val(),
                "start_date": $("#start_date").val(),
                "end_date": $("#end_date").val(),
                "priority":$("#priority").val(),
                "status":$("#status").val()
            }
            console.log(params);
            $.ajax({
                "method": "POST",
                "url": '{{route('get.store.data')}}',
                "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                "data": {
                    data: {
                        "store_id": params.store_id,
                        "start_date": params.start_date,
                        "end_date": params.end_date,
                        "priority":params.priority,
                        "status":params.status
                    }
                }
            }).done(function (response) {
                $("#result").html(response);
            });
            return false;
        }

        document.getElementById("send").addEventListener("click", function () {
            sendDataUsingjQuery();
        });
    </script>
@endpush
