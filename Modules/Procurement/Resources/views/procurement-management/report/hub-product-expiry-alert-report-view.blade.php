<div id="printableArea">
    <div class="th-table-header">
        <table>
            <tr>
                <td><h3><strong> Alesha Permachy    </strong></h3></td>
            </tr>
            <tr>
                <td> <p>  <strong> Expiry Alert Report </strong> </p></td>
            </tr>

            <tr>
                <td> <p>  <strong> @if(isset($product_name)) Product Name : {{ $product_name }} @endif</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong> @if(isset($product_category)) Product Category : {{ $product_category }} @endif</strong> </p></td>
            </tr>

            <tr>
                <td> <p>  <strong>Report Created : @php echo date('d-m-Y'); @endphp </strong> </p></td>
            </tr>

        </table>
    </div><!-- end.th-table-header -->

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover vendor-list" id="vendorList">
            <thead>
            <tr>
                <th>SL</th>
                <th>Product Name</th>
                <th>Generic Name</th>
                <th>Remain Days</th>
                <th>Manufacturer</th>
                <th>Batch No</th>
                <th>Expire Date</th>

            </tr>
            </thead>
            <tbody>
            {{--{{ dd($data) }}--}}
            @if(!empty($data))
                @foreach($data as $report)
                    <tr>
                        <td> {{ $loop->iteration }}</td>

                        <td>
                            {{ $report->product_name }}<br>
                            SKU : {{ $report->sku }}
                        </td>
                        <td>{{ $report->generic_name }}</td>
                        <td>
                            <strong>
                                @php
                                    $startDate = \Carbon\Carbon::today();

                                @endphp
                                @if(($startDate >= $report->expiry_date))
                                    {{ 'Expired' }}
                                @else
                                    {{ $startDate->diffInDays($report->expiry_date).' '.'Day Remain' }}
                                @endif
                            </strong>
                        </td>
                        <td>{{ $report->manufacturer_name }}</td>
                        <td>{{ $report->batch_no ?? null }}</td>
                        <td>{{ date('d F Y', strtotime($report->expiry_date)) }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td>No Data Found</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div><!-- end.table-responsive -->
</div><!-- end#printarea -->
<hr>

<div class="row text-right">
    <div class="col-md-12">
        <button class="btn index-btn" onclick="printDiv('printableArea')">
        Print
        </button>
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->
