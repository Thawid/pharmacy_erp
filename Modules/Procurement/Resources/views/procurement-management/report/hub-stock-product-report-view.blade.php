<div id="printableArea">
    <div class="th-table-header">
        <table>
            <tr>
                <td><h3><strong> Alesha Permachy    </strong></h3></td>
            </tr>
            <tr>
                <td> <p>  <strong>Stock Summary Report</strong> </p></td>
            </tr>
            <tr>
                <td> <p> @if($show_date_two != '') <strong>  Date : {{ $show_date_one }} @endif @if($show_date_two !== '') To {{ $show_date_two }} </strong> @endif</p></td>
            </tr>
            <tr>
                <td> <p>  <strong> @if(isset($product_name)) Product Name : {{ $product_name }} @endif</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong> @if(isset($product_type)) Product Type : {{ $product_type }} @endif</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong> @if(isset($manufacture_name)) Manufacturer : {{ $manufacture_name }} @endif</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong>Report Created : @php echo date('d-m-Y'); @endphp </strong> </p></td>
            </tr>

        </table>
    </div><!-- end.th-table-header -->

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover vendor-list" id="vendorList">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Name</th>
                        <th>Generic Name</th>
                        <th>Manufacturer</th>
                        <th>Available Stock</th>
                        <th>UOM</th>
                    </tr>
                    </thead>
                    <tbody>
                        {{--{{ dd($data) }}--}}
                        @if(!empty($data))
                        @foreach($data as $report)
                            <tr>
                                <td> {{ $loop->iteration }}</td>

                                <td> {{ $report->product_name }}</td>
                                <td>{{ $report->generic_name }}</td>
                                <td>{{ $report->manufacturer_name }}</td>
                                <td>{{ $report->available_quantity }}</td>
                                <td>{{ $report->uom_name }}</td>
                            </tr>
                        @endforeach
                        @else
                            <tr>
                                <td>No Data Found</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div><!-- end.table-res -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
</div><!-- end#printable -->

<hr>
<div class="row text-right">
    <div class="col-md-12">
        <button class="btn index-btn" onclick="printDiv('printableArea')">
        Print
    </button>
    </div>
</div>
