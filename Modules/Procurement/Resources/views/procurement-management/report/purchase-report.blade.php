@extends('dboard.index')

@section('title','Store Report | Procurement')

@section('dboard_content')
    @push('styles')
        <style>
            @media print {
                @page {
                    margin: 20px;
                }

            }
        </style>
    @endpush
    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h2 class="title-heading">Purchase Report </h2>
                </div>
            </div><!-- end.row -->
            <hr>

            <form class="" method="post">
                <meta name="csrf-token" content="{{ csrf_token() }}"/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="hub_id" class="">Select HUB</label>
                            <select class="form-control select2" name="hub_id" id="hub_id">
                                <option value="0">Select HUB</option>
                                @foreach($hubs as $hub)
                                    <option value="{{ $hub->id }}">{{ $hub->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="start_date" class="">Date From</label>
                            <input type="date" name="start_date" id="start_date" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="end_date" class="">Date To</label>
                            <input type="date" name="end_date" id="end_date" class="form-control">
                        </div>
                    </div>
                </div><!-- end.row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="vendor_id" class="">Select Vendor</label>
                            <select class="form-control select2" name="vendor_id" id="vendor_id">
                                <option value="0">Select Vendor</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="reset_action">
                            <input type='button' class="btn index-btn mr-2" value='Search' id='send'>
                            <a href="#" class="btn create-btn mr-2 ml-2" type="button" id="btnExport" onclick="Export()">PDF <i class="pl-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
                            <a href="#" class="btn create-btn" type="button" id="print_btn" onclick="printDiv('printReport')">Print <i class="pl-2 fa fa-print"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <hr>
            <div id="result"></div>
        </div><!-- end.tile-body -->
    </div><!-- end.tile -->

@endsection

@push('post_scripts')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/html2canvas.min.js')}}"></script>

    <script type="text/javascript">$('.select2').select2();</script>
    <script type="text/javascript">
        function Export() {
            html2canvas(document.getElementById('purchaseReport'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Purchase Report.pdf");
                }
            });
        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    <script>
        /*var dataTable = $('.vendor-list').DataTable({
            processing: true,
            serverSide: true,
        });*/

        function sendDataUsingjQuery() {

            let params = {
                "hub_id": $("#hub_id").val(),
                "vendor_id": $("#vendor_id").val(),
                "start_date": $("#start_date").val(),
                "end_date": $("#end_date").val(),
                /*"priority":$("#priority").val(),
                "status":$("#status").val()*/
            }
            console.log(params);
            $.ajax({
                "method": "POST",
                "url": '{{route('get.purchase.report')}}',
                "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                "data": {
                    data: {
                        "hub_id": params.hub_id,
                        "vendor_id": params.vendor_id,
                        "start_date": params.start_date,
                        "end_date": params.end_date,
                        "priority":params.priority,
                        "status":params.status
                    }
                }
            }).done(function (response) {
                $("#result").html(response);
            });
            return false;
        }

        document.getElementById("send").addEventListener("click", function () {
            sendDataUsingjQuery();
        });
    </script>
@endpush
