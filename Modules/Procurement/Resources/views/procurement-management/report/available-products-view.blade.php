<div id="printableArea">
    <div class="th-table-header">
        <table>
            <tr>
                <td><h3><strong> Alesha Permachy    </strong></h3></td>
            </tr>
            <tr>
                <td> <p>  <strong>Available Products Report</strong> </p></td>
            </tr>
            <tr>
                <td> <p>  <strong>  Date : dsgf  </strong></p></td>
            </tr>
            

        </table>
    </div><!-- end.th-table-header -->

    <div class="row">
        <div class="col-md-12">
            @if ($store_available_product == null)
                <h2 class="text-center">No Data Found</h2>
            @endif
            @if(!empty($store_available_product))
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover vendor-list" id="vendorList">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Name</th>
                        <th>Generic Name</th>
                        <th>Manufacturer</th>
                        <th>Available Stock</th>
                        <th>UOM</th>
                    </tr>
                    </thead>
                    <tbody>
                       
                            <tr>
                                <td> 1</td>
                                <td> {{ $store_available_product->product_name }}</td>
                                <td>{{ $store_available_product->generic_name }}</td>
                                <td>{{ $store_available_product->manufacturer }}</td>
                                <td>{{ floor($store_available_product->available_quantity / $product_uom_quantity->quantity_per_uom) }}</td>
                                <td>{{ $product_uom_quantity->uom_name }}</td>
                            </tr>
                        
                    </tbody>
                </table>
            </div><!-- end.table-res -->
            @endif
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
</div><!-- end#printable -->

<hr>
<div class="row text-right">
    <div class="col-md-12">
        <button class="btn index-btn" onclick="printDiv('printableArea')">
        Print
    </button>
    </div>
</div>