@extends('dboard.index')

@section('title','Local Purchase Order Details')

@section('dboard_content')
@push('styles')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<style>
    @media print {
        @page {
            margin: 20px;
        }
    }
</style>
@endpush

<div id="printableArea">
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <!-- tile -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Local Purchase Order Details</h2>
                        </div><!-- end.col-md-6 -->

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('manage-local-purchase.index')}}"> Local Purchase
                                List</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <div class="row">
                        <div class="col-md-4 form-group">
                            <p><strong>Local Purchase Order
                                    No: {{$local_purchase_details[0]->purchase_order_no}} </strong></p>
                        </div>
                        <div class="col-md-4 form-group">
                            <p><strong>Purchase Date: {{$local_purchase_details[0]->purchase_date}} </strong></p>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>

                                <tr>
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    {{-- <th>Unit</th> --}}
                                    <th>Order Quantity</th>
                                    <th>Uom Prices</th>
                                    <th>Discount</th>
                                    <th>Sub Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($local_purchase_details as $row)
                                    <tr>
                                        <td>{{ $row->product_name}}</td>
                                        <td>{{$row->name}}</td>
                                        {{-- <td>{{$row->name}}</td> --}}
                                        <td>{{$row->purchase_qty}}</td>
                                        <td>{{$row->uom_price}}</td>
                                        <td>{{$row->discount}}</td>
                                        <td>{{$row->purchase_sub_total}}</td>

                                    </tr>
                                @endforeach

                                <tr>
                                    <td colspan="8" class="text-right">Sub Total</td>
                                    <td>{{$local_purchase_details[0]->purchase_sub_total}}</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-right">Overall Discount</td>
                                    <td>{{$local_purchase_details[0]->overall_discount}}</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-right">Total</td>
                                    <td>{{$local_purchase_details[0]->total}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                    <hr>

                    <div class="row text-right">
                        <div class="col-md-12">
                            <button class="btn index-btn" onclick="printDiv('printableArea')">Print</button>
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
</div><!-- end#printableArea -->


@endsection
@push('post_scripts')
    <script>
        function printDiv(divId) {
            var printContents = document.getElementById(divId).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endpush
