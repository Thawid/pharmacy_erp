@extends('dboard.index')

@section('title','Update Local Purchase')
@push('styles')
    <style>
        .btn-sm, .btn-group-sm > .btn {
            padding: 0.22rem 0.3rem;
            font-size: 0.765625rem;
            line-height: 1.5;
            border-radius: 3px;
        }
    </style>
@endpush
@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="title-heading">Update Local Purchase</h2>
                        </div>

                        <div class="col-md-6 text-md-right">
                            <a class="btn index-btn" href={{route('manage-local-purchase.index')}}>Local Purchase List</a>
                        </div><!-- end.col -->
                    </div><!-- end.row -->
                    <hr>

                    <!-- form -->
                    <form method="POST" action="{{ route('manage-local-purchase.update',$local_purchase->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <input type="hidden" name="local_purchase_id" value="{{$local_purchase->id}}">
                                    <div class="col-md-4 form-group">
                                        <label for=""> Request Date *</label>
                                        <input type="date" name="purchase_date" id="purchase_date" value="{{$local_purchase->purchase_date}}" class="form-control" required>
                                    </div>

                                </div><!-- end.row -->
                            </div><!-- end.col-md-12 -->

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                            <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Generic Name</th>
                                                <th>Unit</th>
                                                <th>Quantity</th>
                                                <th>UOM</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($local_purchase_details))
                                                @foreach($local_purchase_details as $row)
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="purchase_details_ids[]" value="{{ $row->id }}">
                                                            <select class="form-control product" name="product[]" id="" data-id="0">
                                                                <option value="{{ $row->product_id }}">{{ $row->product->product_name ?? '' }}</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="generic_id[]" value="{{ $row->generic_id }}">
                                                            <input class="form-control" type="text" name="generic_name[]" value="{{ $row->generic->name ?? '' }}" readonly>
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="unit_id[]" id="">
                                                                @foreach($units as $unit)
                                                                    @if(!empty($row))
                                                                        <option value="{{ $unit->id }}" {{ $row->unit_id == $unit->id ? 'selected' : '' }} >{{ $unit->unit_name ?? '' }}</option>
                                                                    @else
                                                                        <option value="{{ $unit->id }}">{{ $unit->unit_name ?? '' }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input class="form-control purchase_qty" type="number" name="purchase_qty[]" id="purchase_qty_0" value="{{$row->purchase_qty ?? ''}}" required="">
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="uom_id[]" id="">
                                                                @foreach($uoms as $uom)
                                                                    @if(!empty($row))
                                                                        <option value="{{ $uom->id }}" {{ $row->uom_id == $uom->id ? 'selected' : '' }} >{{ $uom->uom_name ?? '' }}</option>
                                                                    @else
                                                                        <option value="{{ $uom->id }}">{{ $uom->uom_name ?? '' }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-outline-danger btn-sm" type="button" name="remove" id="remove"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        <input class="btn create-btn" type="button" data-toggle="tooltip" title="Add More" name="add" id="add" value="+">
                                    </div><!-- end.col-md-12 -->
                                </div><!-- end.row -->
                            </div><!-- end.col-md-12 -->

                            <div class="row col-md-12">
                                <div class="col-md-6"></div>
                                <div class="col-md-6 text-right">
                                    <div class="btn-group">
                                        <button type="submit" class="btn create-btn">Update</button>
                                    </div>
                                </div>
                            </div><!-- row -->
                        </div><!-- end.row -->
                    </form>
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- edn.col-md-12 -->
    </div><!-- end.row -->



@endsection

@push('post_scripts')

    <!-- js script start from here -->
    <script type="text/javascript" src="{{asset('js/plugins/bootstrap-datepicker.min.js')}}"></script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
    </script>



    <script type="text/javascript">
        $('.select2').select2({});
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            var x = 1;

            $("#add").click(function() {

                var html = `'<tr>' + '<td> <select class="form-control product select2" onchange="getProduct(this)" data-id="${x}" name="product[]" id="product"><option selected disabled>--- Select a Product ----</option>@foreach($products as $row)<option value="{{$row->id}}">{{$row->product_name}}</option>@endforeach</select>
                <input type="hidden" name="purchase_price[]" class="purchase_price_${x}" id="purchase_price_${x}"></td>'+
                    '<td><input class="form-control generic_id_${x}"  type="hidden" name="generic_id[]" id="generic_id_${x}" required=""><input class="form-control generic_name_${x}" id="generic_name_${x}" type="text" value="" name="generic_name[]" required="" id="generic${x}"></td>' +
                    '<td><select class="form-control" name="unit_id[]" id="">
                        @foreach($units as $unit)
                            <option value="{{ $unit->id }}">{{ $unit->unit_name ?? '' }}</option>
                        @endforeach
                    </select>
                </td>'+'<td><input class="form-control purchase_qty_${x}" type="number" name="purchase_qty[]" required="" id="purchase_qty_${x}"></td>' +<td><select class="form-control uom_${x}" name="uom_id[]" id="uom_${x}"><option selected disabled>---Select A UOM---</option>@foreach($uoms as $list)
                <option value="{{$list->id}}">{{$list->uom_name}}</option>@endforeach</select></td>'
                    '<td><input class="btn btn-outline-danger btn-sm" type="button" name="remove" id="remove" value="x"></td>' + '</tr>'`;
                $("#table_field").append(html);
                x++;

                $('.select2').select2({
                    width: '100%'
                });
            });

            $("#table_field").on('click', "#remove", function() {

                $(this).closest('tr').remove();
            })
        });
    </script>


    <!-- Get Product Item -->
    <script>

        $(document).ready(function(){
            $('#preloader').hide();
        });

        function getAjaxReq(e){
            var id = $(e).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: "{{ url('/procurement/get-product') }}/" + id,
                dataType: "json",
                success: function(res) {
                    console.log(res);
                    $('#showdata').show();
                    $('#preloader').hide();
                    var selectorID = $(e).attr('data-id');

                    if (res.pgeneric.name) {
                        $(`.generic_name_${selectorID}`).val(res.pgeneric.name)
                        $(`.generic_id_${selectorID}`).val(res.pgeneric.id)
                    } else {
                        $(`.generic_name_${selectorID}`).val('not found!')
                        $(`.generic_id_${selectorID}`).val('not found!')
                    }

                    if (res.product_unit.unit_name) {
                        $(`.unit_${selectorID}`).val(res.product_unit.unit_name)
                        $(`.unit_id_${selectorID}`).val(res.product_unit.id)
                    } else {
                        $(`.unit_${selectorID}`).val('not found!')
                        $(`.unit_id_${selectorID}`).val('not found!')
                    }
                    $(`.uom_${selectorID}`).empty();

                    $(`.uom_${selectorID}`).append('<option value="' + res.product_uom.id + '">' + res.product_uom.uom_name + '</option>');



                    $(`.vendor_${selectorID}`).empty();
                    $.each(res.vendors, function(index, vendorlist) {
                        console.log(vendorlist)
                        $(`.vendor_${selectorID}`).append('<option value="' + vendorlist.id + '">' + vendorlist.vendor_name.name + '</option>');
                    });
                }
            });
        }

        function getProduct(e) {
            var id = $(e).val();

            let productarray = [];
            const products = document.getElementsByClassName('product');
            var count = true;
            for( const product of products ) {
                if(productarray.indexOf(product.value) == '-1'){
                    productarray.push(product.value);
                    count = true;
                }else{
                    count = false;
                    product.parentElement.parentElement.remove();
                    alert('This product is already exists');
                }
            }

            if(count == true){
                $('#showdata').hide();
                $('#preloader').show();
                getAjaxReq(e);
            }
        }
    </script>

    <!-- select2 -->

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                width: '100%'
            });
        });
    </script>

    <script>
        if (localStorage.getItem("parent")) {
            //document.getElementById("parent").innerHTML = localStorage.getItem("parent");
        }
        setInterval(function () {
            //localStorage.setItem("parent", document.getElementById("parent").innerHTML);

        }, 2000)
    </script>

    <script>
        function saveData(){
            var data = $('#procurestore').serializeArray();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: "{{ url('/procurement/product/save') }}/",
                data:data,
                dataType: "json",
                success: function(res) {
                    //console.log(res);
                }
            });
        }
    </script>
@endpush
