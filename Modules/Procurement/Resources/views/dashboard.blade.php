@extends('dboard.index')
@section('title','Vendor Dashboard')

@push('styles')
  <style>
    @media (min-width: 992px) and (max-width: 1199px) {
      #recent_pol {
        font-size: 20px;
        font-weight: bold;
      }

      #recent_sr {
        font-size: 20px;
        font-weight: bold;
      }
    }
  </style>
@endpush
@section('dboard_content')

    <div class="row">
        <div class="col-lg-6 col-xl-3">
            <div class="widget-small primary coloured-icon">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Total Purchase Order</h4>
                    <p><b>{{$total_purchase_order}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-3">
            <div class="widget-small info coloured-icon">
                <!-- <i class="icon fa fa-thumbs-o-up fa-3x"></i> -->
                <i class="icon fa fa-home fa-3x"></i>
                <div class="info">
                    <h4>Total Purchase Amount</h4>
                    <p><b>{{isset($total_purchase_amount[0])? ceil($total_purchase_amount[0]->total_amount) : 0}}
                            BDT</b></p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-3">
            <div class="widget-small warning coloured-icon">
                <i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Total Store Requisitions</h4>
                    <p><b>{{$total_store_requisitions}}</b></p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-3">
            <div class="widget-small danger coloured-icon">
                <i class="icon fa fa-star fa-3x"></i>
                <div class="info">
                    <h4>Total Hub Requisitions</h4>
                    <p><b>{{$total_hub_requisitions}}</b></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">Monthly Purchase Amount</h3>
                <div id="bar_chart" style="min-height: 290px;"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">Store Wise Requisitions</h3>
                <div id="pi-chart" class="d-flex justify-content-center"></div>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-md-6">
        {{--latest 5 store requisitions--}}
        @if(isset($latest_store_requisitions))
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="title-heading" id="recent_sr">Recent Store Requisition</h2>
                        </div>
                    </div><!-- end.row -->
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover requisition-list"
                                       id="requisitionList">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Requisition Info</th>
                                        <th>Priority</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($latest_store_requisitions as $requisition)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <address>
                                                    <strong>Requisition
                                                        Id: {{$requisition->store_requisition_no}}</strong><br>
                                                        Created Date: {{$requisition->requisition_date}}
                                                </address>
                                            </td>
                                            <td>{{$requisition->priority}}</td>
                                            <td>{{ $requisition->storestatus }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div><!-- end.table-responsive -->
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                </div>
            </div>
        @endif
        {{--end latest 5 store requisitions--}}
    </div>
    <div class="col-md-6">
        {{--latest 5 purchase order--}}
        @if(isset($latest_purchase_orders))
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="title-heading" id="recent_pol">Recent Purchase Order List</h2>
                        </div>
                    </div><!-- end.row -->
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover requisition-list"
                                       id="requisitionList">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Purchase Order Date</th>
                                        <th>Requested Ddelivery Date</th>
                                        <th>Total Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($latest_purchase_orders as $latest_purchase_order)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{$latest_purchase_order->purchase_order_date}}</td>
                                            <td>{{$latest_purchase_order->requested_delivery_date}}</td>
                                            <td>{{$latest_purchase_order->total}}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div><!-- end.table-responsive -->
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                </div>
            </div>
        @endif
        {{--end latest 5 purchase order--}}
    </div>
</div>

@endsection
@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/apex_chart.js') }}"></script>
    <script>
        // bar chart

        var options = {
            chart: {
                type: 'bar',
                height: 280,
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '48%',
                },
            },
            colors: ['#0062FF', '#D5D7E3'],
            series: [
                {
                    name: 'Amount',
                    data: [
                        @php
                            if(count($monthly_expense) > 0){
                                foreach ($monthly_expense as $expense){
                                    echo $expense->expenses . ",";
                                }
                            }
                        @endphp
                    ]

                }
            ],
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: [
                    @php
                        if(count($monthly_expense) > 0){
                              foreach ($monthly_expense as $expense){
                                  echo '"'.$expense->month .'",';
                              }
                        }
                    @endphp
                ],
                axisBorder: {
                    show: true,
                    color: 'rgba(0,0,0,0.05)'
                },
            },
            grid: {
                row: {
                    colors: ['transparent', 'transparent'], opacity: .2
                },
                borderColor: 'rgba(0,0,0,0.05)'
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            },
            legend: {
                show: false
            }
        };
        var chart = new ApexCharts(document.querySelector("#bar_chart"), options);
        chart.render();
    </script>
    <script>
        //  pi chart
        var series = [
            @php
                if(count($store_wise_requisitions) > 0){
                    foreach ($store_wise_requisitions as $row){
                        echo $row->store_id . ",";
                    }
                }
            @endphp
        ];
        let labels = [
            @php
                if(count($store_wise_requisitions) > 0){
                      foreach ($store_wise_requisitions as $row){
                          echo '"'.$row->name .'",';
                      }
                }
            @endphp
        ];

        let newSeries = [];
        let newLabels = [];
        let grouped = 0;
        series.forEach((s, i) => {
            if (s < 10) {
                grouped += s;
            }
            if (s >= 0) {
                newSeries.push(s);
                newLabels.push(labels[i]);
            }
        });

        if (grouped > 0) {
            newSeries.push(grouped);
            newLabels.push("Others");
        }

        var options = {
            legend: {
                show: false,
            },
            series: newSeries,
            chart: {
                width: 380,
                type: "pie",
            },
            colors: ["#008FFB", "#00E396", "#FEB019", "#ccc"],
            labels: newLabels,
        };

        var chart = new ApexCharts(document.querySelector("#pi-chart"), options);
        chart.render();
    </script>
@endpush
