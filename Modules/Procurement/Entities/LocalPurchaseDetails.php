<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;
use Modules\Vendor\Entities\Vendor;

class LocalPurchaseDetails extends Model
{
    use HasFactory;

    protected $fillable = [];
     /*
     * Each details belongs  to requisition
     * */
    public function requisition(){
        return $this->hasOne(StoreRequisition::class)->withDefault();
    }

    /*
     * Every requisition has may product
     * */


    public function product(){
        return $this->belongsTo(Product::class)->withDefault();
    }

    /*
     * Every requisition has many product unit
     * */

    public function unit(){
        return $this->belongsTo(ProductUnit::class)->withDefault();
    }


    /*
     * Every requisition has many vendor
     *
     * */

    public function vendor(){
        return $this->belongsTo(Vendor::class)->withDefault();
    }

    /*
     * Every requisition has many generic name
     * */


    public function generic(){
        return $this->belongsTo(ProductGeneric::class);
    }

    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'uom_id','id')->withDefault();
    }

    
    public function requisitionsList()
    {
        return $this->belongsToMany(StoreRequisition::class);
    }

    // public function available_qty()
    // {
    //     return $this->belongsTo(HubAvailableProductDetails::class,'product_id','product_id')->withDefault();
    // }
    
    protected static function newFactory()
    {
        return \Modules\Procurement\Database\factories\LocalPurchaseDetailsFactory::new();
    }
}
