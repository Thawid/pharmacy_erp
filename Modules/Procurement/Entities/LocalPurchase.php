<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Store\Entities\Store;

class LocalPurchase extends Model
{
    use HasFactory;

    protected $fillable = [];
    
    protected static function newFactory()
    {
        return \Modules\Procurement\Database\factories\LocalPurchaseFactory::new();
    }

    public function details(){
        return $this->hasMany(LocalPurchaseDetails::class,'local_purchase_id','id');
    }

    public function store(){
        return $this->hasOne(Store::class,'id','store_id')->with(['type']);
    }


}
