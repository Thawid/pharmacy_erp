<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;
use Modules\Vendor\Entities\Vendor;

class HubRequisitionDetails extends Model
{
    use HasFactory;

    protected $table = "hub_requisition_details";

    protected $guarded = [];

    public function requisitions(){
        return $this->hasOne(HubRequisition::class,'id','hub_requisition_id');
    }

    public function product(){
        return $this->belongsTo(Product::class)->withDefault();
    }

    public function generic(){
        return $this->belongsTo(ProductGeneric::class)->withDefault();
    }

    public function unit(){
        return $this->belongsTo(ProductUnit::class)->withDefault();
    }

    public function uom(){

        return $this->belongsTo(UOM::class)->withDefault();
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class)->withDefault();
    }


}
