<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Modules\Store\Entities\Store;

class HubRequisition extends Model
{
    use HasFactory;

    protected $table = "hub_requisitions";

    protected $guarded = [];


    public function store(){
        return $this->hasOne(Store::class,'id','hub_id');
    }


    public function getHubReqDetailsStatusAttribute(){

        $array = [];
        $hub_details_status=DB::table('hub_requisition_details')->select('status')->where('hub_requisition_id','=',$this->id)->get();
        foreach($hub_details_status as $hub_status){
            array_push($array, $hub_status->status);
        }
        if(in_array('Pending',$array)){
            return "Pending";
        }else{
            return  "Approved";
        }

    }

   
}
