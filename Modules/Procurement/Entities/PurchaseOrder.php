<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Store\Entities\Store;
use Modules\Vendor\Entities\Vendor;

class PurchaseOrder extends Model
{
    use HasFactory;
    protected $table = "purchase_orders";
    protected $fillable = [];

    public function hq_requisition()
    {
        return $this->belongsTo(HqRequisition::class)->withDefault();
    }

    public function purchase_order_details()
    {
        return $this->hasMany(PurchaseOrderDetails::class,'purchase_order_id');
    }
    public function vendor()
    {
        return $this->belongsTo(Vendor::class)->withDefault();
    }

    public function hub_info(){
        return $this->hasOne(Store::class,'id','hub_id')->where('store_type','=','hub')->withDefault();
    }


    protected static function newFactory()
    {
        return \Modules\Procurement\Database\factories\PurchaseOrderFactory::new();
    }


}
