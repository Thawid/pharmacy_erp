<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Modules\Store\Entities\Store;

class StoreRequisition extends Model
{
    use HasFactory;

    protected $fillable = [];


    public function details(){
        return $this->hasMany(StoreRequisitionDetails::class,'store_requisition_id','id');
    }

    public function store_edits(){
        return $this->hasMany(StoreRequisitionDetails::class,'store_requisition_id','id')->where('status','!=','Cancelled');
    }

    /*
     * Each requisition has one store
     * */


    public function store(){
        return $this->hasOne(Store::class,'id','store_id')->with(['type']);
    }


    public function getStoreStatusAttribute()
    {

        $array = [];
        $hub_details_status = DB::table('store_requisition_details')->select('status')->where('store_requisition_id', '=', $this->id)->get();
        foreach ($hub_details_status as $hub_status) {
            array_push($array, $hub_status->status);
        }

        if (in_array('Pending', $array)) {
            return "Pending";
        } elseif (in_array('Partial', $array)) {
            return "Partial";
        } else {
            return "Approved";
        }

    }


    public function getStoreUpdateAttribute()
    {

        $array = [];
        $hub_details_status=DB::table('store_requisition_details')->select('status')->where('store_requisition_id','=',$this->id)->get();
        foreach($hub_details_status as $hub_status){
            array_push($array, $hub_status->status);
        }
        if(in_array('Approved',$array) || in_array('Partial',$array)) {
            return "Approved";
        }else{
            return 'Pending';
        }

    }

    public function get_store(){
        return $this->belongsTo(StoreRequisitionDetails::class)->withDefault();
    }

    protected static function newFactory()
    {
        return \Modules\Procurement\Database\factories\StoreRequisitionFactory::new();
    }

    // public function store()
    // {
    //     return $this->belongsTo(Store::class);
    // }

}
