<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;
use Modules\Vendor\Entities\Vendor;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\UOM;

class HqRequisitionDetails extends Model
{
    use HasFactory;

    protected $table = 'hq_approve_requisition_details';
    protected $guarded = [];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'id','product_id');
    }

    public function generic()
    {
        return $this->belongsTo(ProductGeneric::class)->withDefault();
    }

    public function unit()
    {
        return $this->belongsTo(ProductUnit::class)->withDefault();
    }
    public function hq_requisition()
    {
        return $this->belongsTo(HqRequisition::class)->withDefault();
    }

    public function uom()
    {
        return $this->belongsTo(UOM::class)->withDefault();
    }


    protected static function newFactory()
    {
        return \Modules\Procurement\Database\factories\HQRequisitionDetailsFactory::new();
    }
}
