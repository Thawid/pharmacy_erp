<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HqApproveRequisition extends Model
{
    use HasFactory;

    protected $fillable = [];
    
    protected static function newFactory()
    {
        return \Modules\Procurement\Database\factories\HqApproveRequisitionFactory::new();
    }
}
