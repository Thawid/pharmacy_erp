<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HubApproveRequisition extends Model
{
    use HasFactory;

    protected $table = 'hub_approve_requisitions';
    protected $fillable = ['store_requition_id','store_requisition_no','store_id','store_type','requisition_date','delivery_date','priority','warehouse_id','purchase_distribution_request_id','status'];
    
    
}
