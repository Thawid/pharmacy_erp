<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Vendor\Entities\Vendor;

class HqRequisition extends Model
{
    use HasFactory;

    protected $table = "hq_approve_requisitions";
    protected $guarded = [];
    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }


    protected static function newFactory()
    {
        return \Modules\Procurement\Database\factories\HqRequisitionFactory::new();
    }
}
