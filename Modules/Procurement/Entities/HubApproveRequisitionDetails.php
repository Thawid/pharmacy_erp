<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HubApproveRequisitionDetails extends Model
{
    use HasFactory;

    protected $table = 'hub_approve_requisition_details';
    protected $fillable = ['hub_approve_requisition_id','product_id','generic_id','unit_id','vendor_id','uom_id','purchase_quantity','status'];
    
    
}
