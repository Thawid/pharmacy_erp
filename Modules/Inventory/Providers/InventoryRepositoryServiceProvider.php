<?php

namespace Modules\Inventory\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Inventory\Repositories\DistributeRepository;
use Modules\Inventory\Repositories\DistributeRepositoryInterface;
use Modules\Inventory\Repositories\RequisitionRepositoryInterface;
use Modules\Inventory\Repositories\RequisitionRepository;
use Modules\Inventory\Repositories\WarehouseRepository;
use Modules\Inventory\Repositories\WarehouseRepositoryInterface;
use Modules\Inventory\Repositories\WarehouseTypeRepository;
use Modules\Inventory\Repositories\WarehouseTypeRepositoryInterface;
use Modules\Inventory\Repositories\InventoryLocationRepository;
use Modules\Inventory\Repositories\InventoryLocationRepositoryInterface;
use Modules\Inventory\Repositories\InventoryGRNRepository;
use Modules\Inventory\Repositories\InventoryGRNInterface;
use Modules\Inventory\Repositories\WarehouseUserInterface;
use Modules\Inventory\Repositories\WarehouseUserRepository;

class InventoryRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RequisitionRepositoryInterface::class, RequisitionRepository::class);
        $this->app->bind(DistributeRepositoryInterface::class, DistributeRepository::class);
        $this->app->bind(WarehouseTypeRepositoryInterface::class, WarehouseTypeRepository::class);
        $this->app->bind(WarehouseRepositoryInterface::class, WarehouseRepository::class);
        $this->app->bind(InventoryLocationRepositoryInterface::class,InventoryLocationRepository::class);
        $this->app->bind(InventoryGRNInterface::class,InventoryGRNRepository::class);
        $this->app->bind(WarehouseUserInterface::class,WarehouseUserRepository::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
