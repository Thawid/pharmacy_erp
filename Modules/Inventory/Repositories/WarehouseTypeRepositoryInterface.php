<?php

namespace Modules\Inventory\Repositories;

use Modules\Inventory\Http\Requests\CreateWarehouseTypeRequest;
use Modules\Inventory\Http\Requests\UpdateWarehouseTypeRequest;

interface WarehouseTypeRepositoryInterface
{
    public function index();

    public function create();

    public function store(CreateWarehouseTypeRequest $request);

    public function show($id);

    public function edit($id);

    public function update(UpdateWarehouseTypeRequest $request, $id);

    public function destroy($id);

}