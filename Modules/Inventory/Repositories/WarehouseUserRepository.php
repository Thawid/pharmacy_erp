<?php


namespace Modules\Inventory\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use Modules\Inventory\Entities\Warehouse;
use Modules\Inventory\Entities\WarehouseUser;

class WarehouseUserRepository implements WarehouseUserInterface
{



    public function index()
    {
        $all_warehouse_user = DB::table('warehouse_user')
        ->join('warehouses','warehouse_user.warehouse_id','=','warehouses.id')
        ->join('users','warehouse_user.user_id','=','users.id')
        ->select('warehouse_user.id','warehouses.name as warehouse_name','users.name as user_name','users.role as user_role','warehouse_user.status as warehouse_user_status')
        ->get();

        return $all_warehouse_user;
    }



    public function create()
    {

        $data['users'] = User::select('id','name')->get();
        
        $data['warehouses'] = Warehouse::select('id','name')->get();
        return $data;

    }

    public function store($request)
    {
       
        $validated = $request->validate([
            'warehouse_id' => 'required',
            'user_id' => 'required',
            'status' => 'required',
        ]);

        if($validated){

            DB::table('warehouse_user')->insert([
                'warehouse_id' => $request->warehouse_id,
                'user_id' => $request->user_id,
                'status' => $request->status,
            ]);
        }
    
    }

    public function edit($id)
    {
        $data['users'] = User::select('id','name')->get();
        $data['warehouses'] = Warehouse::select('id','name')->get();
        $data['warehouse_user'] = WarehouseUser::where('id',$id)->firstOrFail();
        return $data;
    }

    public function update($request, $id)
    {
       $warehouse_user = WarehouseUser::find($id);
       $warehouse_user->warehouse_id = $request->warehouse_id;
       $warehouse_user->user_id = $request->user_id;
       $warehouse_user->save();
    }

    public function destroy($id)
    {
        $warehouse_user = WarehouseUser::findOrFail($id);
        $warehouse_user->status = !$warehouse_user->status;
        $warehouse_user->save();

    }
    

   
}
