<?php

namespace Modules\Inventory\Repositories;

use Modules\Inventory\Http\Requests\CreateInventoryLocationRequest;
use Modules\Inventory\Http\Requests\UpdateInventoryLocationRequest;

interface InventoryLocationRepositoryInterface
{
    public function index();

    public function create();

    public function store(CreateInventoryLocationRequest $request);

    public function show($id);

    public function edit($id);

    public function update(UpdateInventoryLocationRequest $request, $id);

    public function destroy($id);

}