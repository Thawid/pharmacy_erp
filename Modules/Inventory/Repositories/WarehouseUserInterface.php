<?php


namespace Modules\Inventory\Repositories;

use Illuminate\Http\Request;




interface WarehouseUserInterface
{
    public function index();

    public function create();

    public function store(Request $request);

    public function edit($id);

    public function update(Request $request, $id);

    public function destroy($id);

}
