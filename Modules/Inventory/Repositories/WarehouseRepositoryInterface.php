<?php

namespace Modules\Inventory\Repositories;

use Illuminate\Http\Request;
use Modules\Inventory\Http\Requests\CreateWarehouseRequest;
use Modules\Inventory\Http\Requests\UpdateWarehouseRequest;

interface WarehouseRepositoryInterface
{
    public function index();

    public function create();

    public function store(CreateWarehouseRequest $request);

    public function show($id);

    public function edit($id);

    public function update(Request $request, $id);

    public function destroy($id);
}