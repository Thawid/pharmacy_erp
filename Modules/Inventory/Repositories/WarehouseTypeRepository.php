<?php


namespace Modules\Inventory\Repositories;

use Modules\Inventory\Http\Requests\CreateWarehouseTypeRequest;
use Modules\Inventory\Http\Requests\UpdateWarehouseTypeRequest;
use Modules\Inventory\Entities\WarehouseType;


class WarehouseTypeRepository implements WarehouseTypeRepositoryInterface
{
    public function index()
    {
        return WarehouseType::all();
    }

    public function create()
    {

    }

    public function store(CreateWarehouseTypeRequest $request)
    {
        $validate_data = $request->validated();
        if ($validate_data) {
            $vendor_type = new WarehouseType();
            $vendor_type->name = $request->input('name');
            $vendor_type->status = $request->input('status');
            $vendor_type->save();
        }
       
    }
    public function show($id)
    {
       return WarehouseType::where('id',$id)->first();
    }
    public function edit($id)
    {
        return WarehouseType::findOrFail($id);
    }

    public function update(UpdateWarehouseTypeRequest $request, $id)
    {
       
        $validate = $request->validated();
        if ($validate) {
            $vendor_type = WarehouseType::where('id', $id)->firstOrFail();
            $vendor_type->name = $request->input('name');
            $vendor_type->save();
        }
        
    }

    public function destroy($id)
    {
        $checkStatus = WarehouseType::Where('id',$id)->first();
        WarehouseType::Where('id',$id)->update(['status'=>!$checkStatus->status]);
    }
   

}
