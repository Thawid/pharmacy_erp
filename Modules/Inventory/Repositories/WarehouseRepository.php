<?php

namespace Modules\Inventory\Repositories;

use Modules\Inventory\Http\Request\UpdateWarehouseRequest;
use Modules\Inventory\Http\Requests\CreateWarehouseRequest;
use DataTables;
use Illuminate\Http\Request;
use Modules\Inventory\Entities\Warehouse;

class WarehouseRepository implements WarehouseRepositoryInterface
{
    public function index(){
        return Warehouse::with('warehouseType', 'storeInfo')->latest()->get();
    }

    public function create(){

    }

    public function store(CreateWarehouseRequest $request){
        $validate_date = $request->validated();
        if($validate_date){
            $warehouse = new Warehouse();
            $warehouse->store_id            = $request->input('store_id');
            $warehouse->warehouse_type_id   = $request->input('warehouse_type_id'); 
            $warehouse->name                = $request->input('name'); 
            $warehouse->address             = $request->input('address'); 
            $warehouse->phone               = $request->input('phone'); 
            $warehouse->email               = $request->input('email'); 
            $warehouse->code                = $request->input('code'); 
            $warehouse->status              = $request->input('status'); 

            $warehouse->save();
        }
    }

    public function show($id){
        
        return Warehouse::where('id', $id)->first();
    }

    public function edit($id){
        return Warehouse::with('warehouseType', 'storeInfo')->findOrFail($id);
    }

    public function update(Request $request, $id){
        $validate_date = $request->validated();
        if($validate_date){
            $warehouse = Warehouse::where('id', $id)->firstOrFail();
            $warehouse->store_id            = $request->input('store_id');
            $warehouse->warehouse_type_id   = $request->input('warehouse_type_id'); 
            $warehouse->name                = $request->input('name'); 
            $warehouse->address             = $request->input('address'); 
            $warehouse->phone               = $request->input('phone'); 
            $warehouse->email               = $request->input('email'); 
            $warehouse->code                = $request->input('code'); 
            $warehouse->status              = $request->input('status'); 

            $warehouse->save();
        }
    }

    public function destroy($id)
    {
        $checkStatus = Warehouse::where('id', $id)->first();
        Warehouse::where('id', $id)->update(['status' => !$checkStatus->status]);
    }
    
}