<?php


namespace Modules\Inventory\Repositories;
use Modules\Inventory\Http\Requests\CreateInventoryGRNRequest;
use Modules\Inventory\Http\Requests\UpdateInventoryGRNRequest;


interface InventoryGRNInterface
{
    public function index();

    public function create();

    public function store(CreateInventoryGRNRequest $request);

    public function show(UpdateInventoryGRNRequest $request, $id);

    public function edit($id);

    public function update(UpdateInventoryGRNRequest $request, $id);

    public function destroy($id);
}
