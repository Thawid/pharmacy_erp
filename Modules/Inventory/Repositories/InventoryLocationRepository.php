<?php


namespace Modules\Inventory\Repositories;

use Modules\Inventory\Http\Requests\CreateInventoryLocationRequest;
use Modules\Inventory\Http\Requests\UpdateInventoryLocationRequest;
use Modules\Inventory\Entities\InventoryLocation;
use Modules\Store\Entities\Store;


class InventoryLocationRepository implements InventoryLocationRepositoryInterface
{
    public function index()
    {

        return InventoryLocation::join('stores','stores.id','=','inventory_location.hub_id')
            ->select('inventory_location.id','inventory_location.status','inventory_location.type','inventory_location.state','inventory_location.floor_no','inventory_location.room_no','inventory_location.room_point','inventory_location.self_no','inventory_location.name as invt_name','stores.name as hub_name')
            ->orderBy('inventory_location.id','DESC')
            ->get();
    }

    public function create()
    {
        return $stores = Store::select('id', 'name')->where('store_type', 'hub')->where('status', 1)->get();
    }

    public function store(CreateInventoryLocationRequest $request)
    {
        $validate_data = $request->validated();
        if ($validate_data) {
            $inventory_location = new InventoryLocation();
            $inventory_location->hub_id = $request->input('hub_id');
            $inventory_location->warehouse_id = $request->input('warehouse');
            $inventory_location->type = $request->input('type');
            $inventory_location->state= $request->input('state');
            $inventory_location->floor_no = $request->input('floor');
            $inventory_location->room_no= $request->input('room');
            $inventory_location->room_point= $request->input('room_point');
            $inventory_location->self_no= $request->input('self');
            $inventory_location->name= $request->input('self_name');
            $inventory_location->status= $request->input('status');
            $inventory_location->save();
        }

    }
    public function show($id)
    {
       return InventoryLocation::where('id',$id)->first();
    }
    public function edit($id)
    {
        return InventoryLocation::findOrFail($id);
    }

    public function update(UpdateInventoryLocationRequest $request, $id)
    {

        $validate = $request->validated();
        if ($validate) {
            $inventory_location = InventoryLocation::where('id', $id)->firstOrFail();
            $inventory_location->hub_id = $request->input('hub_id');
            $inventory_location->warehouse_id = $request->input('warehouse');
            $inventory_location->type = $request->input('type');
            $inventory_location->state= $request->input('state');
            $inventory_location->floor_no = $request->input('floor');
            $inventory_location->room_no= $request->input('room');
            $inventory_location->room_point= $request->input('room_point');
            $inventory_location->self_no= $request->input('self');
            $inventory_location->name= $request->input('name');
            $inventory_location->save();
        }

    }

    public function destroy($id)
    {
        $checkStatus = InventoryLocation::Where('id',$id)->first();
        InventoryLocation::Where('id',$id)->update(['status'=>!$checkStatus->status]);
    }


}
