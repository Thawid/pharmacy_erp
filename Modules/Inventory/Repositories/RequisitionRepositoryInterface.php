<?php


namespace Modules\Inventory\Repositories;

use Illuminate\Support\Facades\Request;

interface RequisitionRepositoryInterface
{
    public function index();

    public function create();

    public function store(Request $request);

    public function show($id);

    public function edit($id);

    public function update(Request $request);

    public function destroy($id);
}
