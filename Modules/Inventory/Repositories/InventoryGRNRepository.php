<?php


namespace Modules\Inventory\Repositories;
use Modules\Inventory\Entities\WarehouseUser;
use Modules\Inventory\Http\Requests\CreateInventoryGRNRequest;
use Modules\Inventory\Http\Requests\UpdateInventoryGRNRequest;
use Modules\Inventory\Entities\InventoryGRN;
use Modules\Procurement\Entities\HubRequisition;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Product\Entities\ProductVariation;
use Modules\Store\Entities\Store;

class InventoryGRNRepository implements InventoryGRNInterface
{
    public function index(){
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $purchaseOrders = PurchaseOrder::with('hub_info', 'purchase_order_details')
                ->where('status', '=', 'Approved')
                ->get();
            return $purchaseOrders;
        }else {
            $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
            $hub_id = Store::where('warehouse_id',$warehouse_id)->value('hub_id');
            $purchaseOrders = PurchaseOrder::with('hub_info', 'purchase_order_details')
                ->where('hub_id', '=', $hub_id)
                ->where('status', '=', 'Approved')
                ->get();
            return $purchaseOrders;
        }

    }

    public function create(){

    }

    public function store(CreateInventoryGRNRequest $request){

    }

    public function show(UpdateInventoryGRNRequest $request, $id){
        return "ZZZ";
    }

    public function edit($id){
          return $purchaseOrders   = PurchaseOrder::with('purchase_order_details')
              ->where('id',$id)
              ->first();
    }

    public function update(UpdateInventoryGRNRequest $request, $id){

    }

    public function destroy($id){

    }
}
