<?php

namespace Modules\Inventory\Repositories;

use Illuminate\Support\Facades\Request;
use Modules\Inventory\Entities\HoldDistributionRequestDetails;
use Modules\Inventory\Entities\HoldDistributionRequests;
use Modules\Inventory\Entities\PurchaseDistributionRequestDetails;
use Modules\Inventory\Entities\PurchaseDistributionRequests;
use Modules\Inventory\Entities\RegularDistributionRequestDetails;
use Modules\Inventory\Entities\RegularDistributionRequests;
use Modules\Inventory\Entities\WarehouseUser;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Procurement\Entities\StoreRequisitionDetails;
use Modules\Store\Entities\StoreUser;

class RequisitionRepository implements RequisitionRepositoryInterface
{

    public function index()
    {
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer'){
            $requisitions =   StoreRequisition::latest()->where('status','Pending')->get();
        }else{
            $warehouse = WarehouseUser::where('user_id', auth()->user()->id)->where('status', 1)->first();
            $requisitions = StoreRequisition::where('warehouse_id',$warehouse->warehouse_id)->where('status','Pending')->get();
        }

        return $requisitions;
    }

    public function create(){

    }

    public function store($request)
    {

        $array = [];
        if( $request->has('distribute_qty') && count( $request->get('distribute_qty'))>0){

            for($i=0;$i<count($request->get('distribute_qty')); $i++){
                if($request->distribute_qty[$i] > $request->available_qty[$i] ){
                    return redirect()->back()->withErrors('Available quantity must be grater then or equal distribute quantity');
                }
            }

        }
        $regular_requisition_details = new RegularDistributionRequestDetails;
        $purchase_requisition_details = new PurchaseDistributionRequestDetails;
        $hold_requisition_details = new HoldDistributionRequestDetails;

        $regular_distribute = array_filter($request->distribute_qty);
        $regular_distribute_count = count($regular_distribute);
        $purchase_distribute = array_filter($request->purchase_qty);
        $purchase_distribute_count = count($purchase_distribute);
        $hold_distribute = array_filter($request->hold_qty);
        $hold_distribute_count = count($hold_distribute);

        foreach ($request->product_id as $i => $product){
            $data = array(
                'product_id'            => $request->product_id[$i],
                'generic_id'            => $request->generic_id[$i],
                'unit_id'               => $request->unit_id[$i],
                'vendor_id'             => $request->vendor_id[$i],
                'uom_id'                => $request->uom_id[$i],
                'request_quantity'      => $request->request_qty[$i],
                'available_quantity'    => $request->available_qty[$i],
                'distribution_quantity' => $request->distribute_qty[$i],
                'purchase_quantity'     => $request->purchase_qty[$i],
                'hold_quantity'         => $request->hold_qty[$i],
            );
            array_push($array,$data);
        }
        $counter = count($array);

        if ($regular_distribute_count > 0) {
            $value = $this->requisitionRequestData($request);
            RegularDistributionRequests::insert($value);
            $requisition = RegularDistributionRequests::orderBy('id','DESC')->first();

            for ($i=0; $i<$counter; $i++){
                if ($array[$i]['distribution_quantity'] != 0){
                    $column = 'regular_distribution_request_id';
                    $value = $this->requisitionRequestDetailsData($array,$i,$column,$requisition);
                    $products = StoreRequisitionDetails::where('product_id',$value['product_id'])->first();
                    $products->update(['distribute_quantity' => $products->distribute_quantity+$array[$i]['distribution_quantity'], 'status' => 'Complete']);
                    $regular_requisition_details->insert($value);
                }
            }
        }
        if ($purchase_distribute_count > 0) {
            $value = $this->requisitionRequestData($request);
            PurchaseDistributionRequests::insert($value);
            $requisition = PurchaseDistributionRequests::orderBy('id','DESC')->first();


            for ($i=0; $i<$counter; $i++){
                if ($array[$i]['purchase_quantity'] != 0){
                    $column = 'purchase_distribution_request_id';
                    $value = $this->requisitionRequestDetailsData($array,$i,$column,$requisition);
                    $products = StoreRequisitionDetails::where('product_id',$value['product_id'])->first();
                    $products->update(['distribute_quantity' => $products->distribute_quantity+$array[$i]['distribution_quantity'], 'status' => 'Complete']);
                    $purchase_requisition_details->insert($value);
                }
            }
        }
        if ($hold_distribute_count > 0) {
            $value = $this->holdRequisitionRequestData($request);
            HoldDistributionRequests::insert($value);
            $requisition = HoldDistributionRequests::orderBy('id','DESC')->first();


            for ($i=0; $i<$counter; $i++){
                if ($array[$i]['hold_quantity'] != 0){
                    $column = 'hold_distribution_request_id';
                    $value = $this->requisitionRequestDetailsData($array,$i,$column,$requisition);
                    $products = StoreRequisitionDetails::where('product_id',$value['product_id'])
                        ->where('store_requisition_id',$request->requisition_id)->first();
                    $products->update(['distribute_quantity' => $products->distribute_quantity+$array[$i]['distribution_quantity'], 'status' => 'Partial']);
                    $hold_requisition_details->insert($value);
                }
            }
        }

        StoreRequisition::where('store_requisition_no', $request->store_requisition_no)->update(['status' => "Approved"]);
    }

    private function requisitionRequestData($request)
    {
        $data = array(
            'store_requisition_id' => $request->requisition_id,
            'store_requisition_no' => $request->store_requisition_no,
            'store_id'             => $request->store_id,
            'store_type'           => $request->store_type,
            'requisition_date'     => $request->requisition_date,
            'delivery_date'        => $request->requisition_delivery_date,
            'priority'             => $request->requisition_priority,
            'warehouse_id'         => $request->warehouse_id,
            'status'               => 'Pending'
        );

        return $data;
    }

    private function holdRequisitionRequestData($request)
    {
        $data = array(
            'store_requisition_id' => $request->requisition_id,
            'store_requisition_no' => $request->store_requisition_no,
            'store_id' => $request->store_id,
            'store_type' => $request->store_type,
            'requisition_date' => $request->requisition_date,
            'delivery_date' => $request->requisition_delivery_date,
            'approximate_delivery_date' => $request->approximate_delivery_date,
            'priority' => $request->requisition_priority,
            'warehouse_id' => $request->warehouse_id,
            'status' => 'Pending'
        );

        return $data;
    }

    private function requisitionRequestDetailsData($array, $i,$column,$requisition)
    {
        $value = array(
            $column => $requisition->id,
            'product_id'            => $array[$i]['product_id'],
            'generic_id'            => $array[$i]['generic_id'],
            'unit_id'               => $array[$i]['unit_id'],
            'vendor_id'             => $array[$i]['vendor_id'],
            'uom_id'                => $array[$i]['uom_id'],
            'request_quantity'      => $array[$i]['request_quantity'],
            'available_quantity'    => $array[$i]['available_quantity'],
            'distribution_quantity' => $array[$i]['distribution_quantity'],
            'purchase_quantity'     => $array[$i]['purchase_quantity'],
            'hold_quantity'         => $array[$i]['hold_quantity'],
        );

        return $value;
    }

    public function show($id){

    }

    public function edit($id){

    }

    public function update(Request $request){

    }

    public function destroy($id)
    {
        StoreRequisitionDetails::where('id', $id)->update(['status' => "Cancelled"]);

    }

}
