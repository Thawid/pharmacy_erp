<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Price\Entities\UomPrice;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\UOM;
use Modules\Vendor\Entities\Vendor;

class RegularDistributionRequestDetails extends Model
{
    use HasFactory;

    protected $table = "regular_distribution_request_details";
    protected $fillable = [];

    public function requisition()
    {
        return $this->belongsTo(RegularDistributionRequests::class,'regular_distribution_request_id','id');
    }


    public function unit(){
        return $this->belongsTo(ProductUnit::class)->withDefault();
    }


    /*
     * Every requisition has many vendor
     *
     * */

    public function vendor(){
        return $this->belongsTo(Vendor::class)->withDefault();
    }

    /*
     * Every requisition has many generic name
     * */


    public function generic(){
        return $this->belongsTo(ProductGeneric::class);
    }

    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'uom_id','id')->withDefault();
    }


    public function requisitionsList()
    {
        return $this->belongsToMany(StoreRequisition::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }

    public function uomPrice()
    {
        return $this->belongsTo(UomPrice::class,'product_id','product_id')->withDefault();
    }


}
