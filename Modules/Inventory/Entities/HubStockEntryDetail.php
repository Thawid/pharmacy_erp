<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\InventoryGrn;
use Modules\Inventory\Entities\Warehouse;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;
use Modules\Product\Entities\ProductUnit;
use Modules\Inventory\Entities\HubStockEntry;

class HubStockEntryDetail extends Model
{
   protected $guarded = [];

   public function uom()
   {
      return $this->belongsTo(UOM::class, 'uom_id', 'id');
   }

    public function unit()
    {
        return $this->belongsTo(ProductUnit::class, 'unit_id', 'id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'id');
    }

   public function product()
   {
      return $this->belongsTo(Product::class)->withDefault();
   }


    public function grn()
    {
        return $this->belongsTo(InventoryGrn::class);
    }

    public function hsentry()
    {
        return $this->belongsTo(HubStockEntry::class, 'hub_stock_entry_id', 'id');
    }

    protected $casts = [
        'expiry_date' => 'datetime:Y-m-d',
    ];
}
