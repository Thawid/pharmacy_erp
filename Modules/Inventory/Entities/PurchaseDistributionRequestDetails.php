<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\UOM;
use Modules\Vendor\Entities\Vendor;

class PurchaseDistributionRequestDetails extends Model
{
    use HasFactory;

    protected $table = "purchase_distribution_request_details";
    protected $fillable = [];

    public function requisition()
    {
        return $this->belongsTo(PurchaseDistributionRequests::class,'purchase_distribution_request_id','id');
    }


    public function unit(){
        return $this->belongsTo(ProductUnit::class)->withDefault();
    }


    /*
     * Every requisition has many vendor
     *
     * */

    public function vendor(){
        return $this->belongsTo(Vendor::class)->withDefault();
    }

    /*
     * Every requisition has many generic name
     * */


    public function generic(){
        return $this->belongsTo(ProductGeneric::class);
    }

    public function product_uom()
    {
        return $this->belongsTo(UOM::class,'uom_id','id')->withDefault();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }
}
