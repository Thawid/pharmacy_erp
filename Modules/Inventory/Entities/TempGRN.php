<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TempGRN extends Model
{
    use HasFactory;
    protected $table = 'inventory_temp_grns';
    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Inventory\Database\factories\TempGRNFactory::new();
    }
}
