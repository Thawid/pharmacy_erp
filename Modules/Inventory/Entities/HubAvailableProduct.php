<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HubAvailableProduct extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Inventory\Database\factories\HubAvailableProductFactory::new();
    }

    public function hubAvailableProductDetails()
    {
        return $this->hasMany(HubAvailableProductDetails::class);
    }
}
