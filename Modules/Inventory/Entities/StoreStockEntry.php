<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StoreStockEntry extends Model
{
    use HasFactory;

    protected $table = 'store_stock_entries';
    protected $fillable = ['store_id','distribution_id','lot_no','requisition_no','status'];
    

}
