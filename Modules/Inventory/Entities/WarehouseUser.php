<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WarehouseUser extends Model
{
    use HasFactory;

    protected $table = 'warehouse_user';
    protected $fillable = ['warehouse_id','user_id','status'];
    
    
}
