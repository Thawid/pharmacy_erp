<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class InventoryLocation extends Model
{
    use HasFactory;

    protected $table = 'inventory_location';
    protected $fillable = ['warehouse_id','type','state','floor_no','room_no','room_point','self_no','status'];



}
