<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class InventoryGrnDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'grn_process_id',
        'product_id',
        'generic_id',
        'uom_id',
        'unit_id',
        'ordered_quantity',
        'received_quantity',
        'ysnReceived',
        'ysnQc',
        'ysnStock',
        'status'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Inventory\Database\factories\InventoryGrnDetailsFactory::new();
    }
}
