<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\HubStockEntryDetail;
use Modules\Inventory\Entities\InventoryGrn;
use Modules\Inventory\Entities\Warehouse;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\UOM;
use Modules\Store\Entities\Store;



class HubStockEntry extends Model
{

    protected $guarded = [];


    public function grn()
    {
        return $this->belongsTo(InventoryGrn::class, 'grn_process_id', 'id');
    }
    public function hub()
    {
        return $this->belongsTo(Store::class, 'hub_id', 'id');
    }

    public function hubStockEntryDetails()
    {
        return $this->hasMany(HubStockEntryDetail::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'hub_id', 'store_id');
    }

    public function Details()
    {
        return $this->belongsTo(HubStockEntryDetail::class, 'id', 'hub_stock_entry_id');
    }
}
