<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Warehouse extends Model
{
    use HasFactory;

    protected $fillable = ['store_id', 'warehouse_type_id', 'name', 'address', 'phone_no', 'email', 'code', 'status'];

    protected static function newFactory()
    {
        return \Modules\Inventory\Database\factories\WarehouseFactory::new();
    }

    public function warehouseType()
    {
        return $this->hasMany('\Modules\Inventory\Entities\WarehouseType', 'id', 'warehouse_type_id');       
    }

    public function storeInfo(){
        return $this->hasMany('\Modules\Store\Entities\Store', 'id', 'store_id');
    }
}
