<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class InventoryGrn extends Model
{
    use HasFactory;

    protected $fillable = [
        'purchase_order_no',
        'hub_requisition_no',
        'hub_id',
        'purchase_order_id',
        'requisition_type',
        'lot_no',
        'request_date',
        'delivery_date',
        'delivery_point',
        'priority',
        'image',
        'note',
        'status'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Inventory\Database\factories\InventoryGrnFactory::new();
    }
}