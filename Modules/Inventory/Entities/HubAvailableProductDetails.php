<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\ProductVariation;


class HubAvailableProductDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'available_quantity'
    ];

    public function product()
    {
        return $this->belongsTo(ProductVariation::class, 'id','product_id')->with('product',);
    }

    public function productunit()
    {
        return $this->belongsTo(ProductUnit::class,'unit_id','id')->withDefault();
    }

    protected static function newFactory()
    {
        return \Modules\Inventory\Database\factories\HubAvailableProductDetailsFactory::new();
    }
}
