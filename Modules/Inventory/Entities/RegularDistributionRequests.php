<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Store\Entities\Store;


class RegularDistributionRequests extends Model
{
    use HasFactory;

    protected $table = "regular_distribution_requests";
    protected $fillable = [];

    public function details()
    {
        return $this->hasMany(RegularDistributionRequestDetails::class,'regular_distribution_request_id','id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class,'store_id','id');
    }

}
