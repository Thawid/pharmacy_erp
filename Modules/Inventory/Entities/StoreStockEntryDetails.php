<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StoreStockEntryDetails extends Model
{
    use HasFactory;

    protected $table= 'store_stock_entry_details';
    protected $fillable = ['store_stock_entry_id','sku','product_id','unit_id','uom_id','ordered_quantity','received_quantity','out_quantity','out_date','stock_status','store_id','batch_no','expire_date','warehouse_id','status'];
    
   
}
