<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StoreGoodReceiveDetails extends Model
{
    use HasFactory;

    protected $table = 'store_good_receive_details';
    protected $fillable = ['store_good_receive_id','product_id','generic_id','unit_id','vendor_id','uom_id','request_quantity','distribution_quantity','status'];
}
