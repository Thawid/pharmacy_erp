<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Distributions extends Model
{
    use HasFactory;

    protected $table = "distributions";
    protected $fillable = [];
    
}
