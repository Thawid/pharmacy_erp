<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WarehouseType extends Model
{
    use HasFactory;

    protected $table = 'warehouse_type';

    protected $fillable = ['name','status'];
    
    
}
