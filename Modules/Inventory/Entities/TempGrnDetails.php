<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TempGrnDetails extends Model
{
    use HasFactory;
    protected $table = 'inventory_temp_grn_details';
    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Inventory\Database\factories\TempGrnDetailsFactory::new();
    }
}
