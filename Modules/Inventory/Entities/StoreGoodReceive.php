<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StoreGoodReceive extends Model
{
    use HasFactory;

    protected $table = 'store_good_receive';
    protected $fillable = ['distribution_id','store_id','store_type','status','requisition_date','delivery_date','requisition_no','priority_distribution_type'];
    
    
}
