<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DistributionDetails extends Model
{
    use HasFactory;

    protected $table = "distribution_details";
    protected $fillable = [];
  
}
