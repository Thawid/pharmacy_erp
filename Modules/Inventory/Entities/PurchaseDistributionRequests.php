<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Store\Entities\Store;

class PurchaseDistributionRequests extends Model
{
    use HasFactory;

    protected $table = "purchase_distribution_requests";
    protected $fillable = [];

    public function products()
    {
        return $this->belongsToMany(RegularDistributionRequestDetails::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class,'store_id','id');
    }

}
