<?php

use Illuminate\Support\Facades\Route;


Route::middleware('auth')->prefix('inventory')->group(function() {

    Route::get('/dashboard', 'DashboardController@index')->name('inventory.dashboard');

    Route::resource('/warehouse-user', 'WarehouseUserController');
    Route::resource('/warehouse-type', 'WarehouseTypeController')->middleware('hub');
    Route::get('/', 'InventoryController@index');
    Route::resource('/warehouse', 'WarehouseController')->middleware('warhouseOrHub');
    Route::resource('inventory-location','InventoryLocationController')->middleware('warhouseOrHub');

    Route::get('/g-store', 'InventoryLocationController@get_store')->name('g_store');

    Route::resource('inventory-GRN','InventoryGRNController')->middleware('warehouse');
    Route::get('grnProcess/{purchase_order_id}/{product_id}','InventoryGRNController@grnProcess')->name('grn.process');
    Route::post('batch-process','InventoryGRNController@process_batch_no')->name('batch.process');


    Route::get('hub-stocks/list','HubStockEntryController@index')->name('hubStockEntry.list')->middleware('warehouse');
    Route::get('hub-stocks/create/{id}','HubStockEntryController@hubStockEntryCreate')->name('hubStockEntry.create');
    Route::post('hub-stocks/store','HubStockEntryController@hubStockEntryStore')->name('hubStockEntry.store')->middleware('warehouse');
    Route::get('available-stock','HubStockEntryController@availableStock')->name('hubStockEntry.availablestock')->middleware('warhouseOrHub');
    Route::get('available-stock-details/{id}','HubStockEntryController@availableStockDetails')->name('hubStockEntry.availablestock.details')->middleware('warhouseOrHub');
    //Start Store Stock Entry//
    Route::get('store-stocks/list','StoreStockEntryController@index')->name('storeStockEntry.list')->middleware('store');
    Route::get('store-stocks/create/{id}','StoreStockEntryController@storeStockEntryCreate')->name('storeStockEntry.create');
    Route::post('store-stocks/store','StoreStockEntryController@storeStockEntryStore')->name('storeStockEntry.store')->middleware('store');
    // End Store Stock Entry//



    Route::resource('requisition', 'RequisitionController')->middleware('warehouse');
    Route::get('/requisitionlist', 'RequisitionController@requisition_list')->name('requisitionlist');
    //Route::get('/panding-details', 'RequisitionController')->name('panding.details');
    Route::get('/pen-process/{id}', 'RequisitionController@process')->name('process');

    //Purchase Distribute
    Route::get('/purchase-distribution', 'Distribution\PurchaseDistributeController@purchase_distribution')->name('purchase.distribution');
    Route::get('/purchase-distribution-show/{id}', 'Distribution\PurchaseDistributeController@purchase_distribution_show')->name('purchase.distribution.show');
    Route::get('/purchase-distribution-process', 'Distribution\PurchaseDistributeController@purchase_distribution_process')->name('purchase.distribution.process');

    //Regular Distribute
    Route::get('/distribution-process', 'Distribution\RegularDistributeController@distribution_process')->name('distribution.process');
    Route::get('/distribution-process-show/{id}', 'Distribution\RegularDistributeController@distribution_process_show')->name('distribution.process.show');
    Route::get('/distribution-process-process/{id}', 'Distribution\RegularDistributeController@distribution_process_process')->name('distribution.process.process');
    Route::post('/distribution-process-store', 'Distribution\RegularDistributeController@distribution_process_store')->name('distribution.process.store');

    //Hold Distribute
    Route::get('/hold-distribution', 'Distribution\HoldDistributeController@hold_distribution')->name('hold.distribution');
    Route::get('/hold-distribution-details/{id}', 'Distribution\HoldDistributeController@hold_distribution_details')->name('hold.distribution.details');
    Route::get('/hold-distribution-process/{id}', 'Distribution\HoldDistributeController@hold_distribution_process')->name('hold.distribution.process');
    Route::post('/distribute-hold-requisition','Distribution\HoldDistributeController@distribute_hold_quantity')->name('distribute.hold.quantity');

    //Barcode for warehouse
    Route::resource('barcode', 'BarcodeController')->middleware('warehouse');
    Route::get('barcode/{grn_id}/generate/{stock_id}','BarcodeController@generate')->name('barcode.generate')->middleware('warehouse');

    //Barcode for store
    Route::get('store-barcode', 'StoreBarcodeController@index')->name('store-barcode')->middleware('store');
    Route::get('store-barcode/{entry_id}/show/{batch}/product/{product_id}', 'StoreBarcodeController@show_store_product')->name('store-barcode.show.product')->middleware('store');
    Route::post('store-barcode/{entry_id}/generate/{product_id}', 'StoreBarcodeController@generate_barcode')->name('store-barcode.generate')->middleware('store');

    Route::get('/independent-stock','IndependentStockController@index')->name('independent.stock');
    Route::post('/store-independent-stock','IndependentStockController@store')->name('store.independent.stock');

    Route::get('/stock-alert-lists', 'StockAlertController@index')->name('compare.stock.alert');
    Route::get('/return-products','WarehouseReturnProductController@index')->name('warehouse.return.products');
    Route::get('/damage-products','WarehouseDamageProductController@index')->name('warehouse.damage.products');
});
