<?php

namespace Modules\Inventory\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckWarehouseOrHubMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(warehouseModuleAuth() || hubModuleAuth()){
            return $next($request);
        }
        return abort(403,'No Permission');
    }
}
