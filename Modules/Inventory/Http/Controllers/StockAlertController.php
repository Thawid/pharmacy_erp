<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Entities\WarehouseUser;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;

class StockAlertController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $userId = auth()->user()->id;
        $store_id = StoreUser::where('store_type', '=', "store")->where('user_id', $userId)->value('store_id');
        $hub_id = StoreUser::where('store_type', '=', "hub")->where('user_id', $userId)->value('store_id');
        $warehouse_id = WarehouseUser::where('user_id', $userId)->value('warehouse_id');
        $get_hub_id = Store::where('warehouse_id',$warehouse_id)->value('hub_id');
        $alert_qty_lists = [];
        if($store_id){
           $alert_qty_lists = DB::table('store_available_product_details')
               ->join('store_available_products', 'store_available_products.id', '=', 'store_available_product_details.store_available_product_id')
               ->join('products', 'products.id', '=', 'store_available_product_details.product_id')
               ->select('store_available_products.store_id','store_available_product_details.product_id','products.product_name','store_available_product_details.available_quantity','store_available_product_details.stock_alert')
               ->whereRaw('store_available_product_details.available_quantity <= store_available_product_details.stock_alert')
               ->where('store_available_products.store_id', $store_id)
               ->get();
        }
        if($hub_id){
            $alert_qty_lists = DB::table('hub_available_product_details')
                ->join('hub_available_products', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'products.id', '=', 'hub_available_product_details.product_id')
                ->select('hub_available_products.hub_id','hub_available_product_details.product_id','products.product_name','hub_available_product_details.available_quantity','hub_available_product_details.stock_alert')
                ->whereRaw('hub_available_product_details.available_quantity <= hub_available_product_details.stock_alert')
                ->where('hub_available_products.hub_id', $hub_id)
                ->get();
        }
        if($warehouse_id){
            $alert_qty_lists = DB::table('hub_available_product_details')
                ->join('hub_available_products', 'hub_available_products.id', '=', 'hub_available_product_details.hub_available_product_id')
                ->join('products', 'products.id', '=', 'hub_available_product_details.product_id')
                ->select('hub_available_products.hub_id','hub_available_product_details.product_id','products.product_name','hub_available_product_details.available_quantity','hub_available_product_details.stock_alert')
                ->whereRaw('hub_available_product_details.available_quantity <= hub_available_product_details.stock_alert')
                ->where('hub_available_products.hub_id', $get_hub_id)
                ->get();
        }

        return view('inventory::inventory-management.stock-alert.index',compact('alert_qty_lists'));
    }
}
