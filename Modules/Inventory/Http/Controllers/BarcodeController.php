<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\InventoryGrn;
use Modules\Inventory\Entities\InventoryGrnDetails;
use Modules\Inventory\Entities\HubStockEntry;
use Modules\Inventory\Entities\HubStockEntryDetail;
use Modules\Inventory\Entities\InventoryLocation;
use Modules\Inventory\Entities\WarehouseUser;
use Illuminate\Contracts\Support\Renderable;
use Modules\Store\Entities\Store;

class BarcodeController extends Controller
{
    /**
     * Display a list of the stock entries.
     *
     * @return Renderable
     */
    public function index()
    {
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $grn_processes = InventoryGrn::latest()->where('requisition_type', 'Approved')->get();
        }else{
            $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
            $hub_id = Store::where('warehouse_id',$warehouse_id)->value('hub_id');
            $grn_processes = InventoryGrn::latest()->where('requisition_type', 'Approved')
                ->where('hub_id',$hub_id)
                ->get();
        }
        return view('inventory::inventory-management.barcode.index', compact('grn_processes'));
    }

    /**
     * Display a short data of the selected stock entry.
     *
     * @param $id
     * @return Renderable
     */
    public function show($id)
    {
        $inventory_locations = InventoryLocation::where('status', 1)->get(['id', 'state', 'warehouse_id']);
        $grn_process = InventoryGrn::where('id', $id)->firstOrFail();
        $grn_process_details = InventoryGrnDetails::where('grn_process_id', $grn_process->id)->get();
        $stock_entry = HubStockEntry::with('hubStockEntryDetails')->where('grn_process_id', $id)->first();

        return view('inventory::inventory-management.barcode.create', compact('grn_process', 'grn_process_details', 'stock_entry', 'inventory_locations'));
    }

    /**
     * Process and generate Barcode.
     *
     * @param $grn_id
     * @param $stock_id
     * @return Renderable
     */
    public function generate($grn_id, $stock_id)
    {
        $stock_entry_details = HubStockEntryDetail::with('product')->where('id', $stock_id)->firstOrFail();
        $inventory_locations = InventoryLocation::where('status', 1)->get(['id', 'state', 'warehouse_id']);
        $grn_process_details = InventoryGrnDetails::where('grn_process_id', $grn_id)->where('product_id', $stock_entry_details->product_id)->get()->last();
        $stock_entry = HubStockEntry::where('grn_process_id', $grn_id)->first();

        return view('inventory::inventory-management.barcode.barcodes', compact( 'stock_entry','stock_entry_details', 'grn_process_details', 'inventory_locations'));
    }
}
