<?php

namespace Modules\Inventory\Http\Controllers\Distribution;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\PurchaseDistributionRequestDetails;
use Modules\Inventory\Entities\PurchaseDistributionRequests;

class PurchaseDistributeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function purchase_distribution()
    {
        $purchase_distributes = PurchaseDistributionRequests::with('store')->orderBy('purchase_distribution_requests.id','DESC')->get();

        return view('inventory::inventory-management.distribution.purchase-distribution', compact('purchase_distributes'));
    }

    public function purchase_distribution_show($id)
    {
        $purchase_distribution_requests = PurchaseDistributionRequests::find($id);
        $purchase_distribution_request_details = PurchaseDistributionRequestDetails::with(['product','unit','generic','product_uom'])
            ->where('purchase_distribution_request_id',$purchase_distribution_requests->id)
            ->get();
        return view('inventory::inventory-management.distribution.purchase-distribution-details', compact('purchase_distribution_requests', 'purchase_distribution_request_details'));
    }

    public function purchase_distribution_process()
    {
        return view('inventory::inventory-management.distribution.purchase-distribution-process');
    }
}
