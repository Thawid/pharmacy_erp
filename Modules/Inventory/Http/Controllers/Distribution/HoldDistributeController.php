<?php

namespace Modules\Inventory\Http\Controllers\Distribution;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Modules\Inventory\Entities\DistributionDetails;
use Modules\Inventory\Entities\Distributions;
use Modules\Inventory\Entities\HoldDistributionRequestDetails;
use Modules\Inventory\Entities\HoldDistributionRequests;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\HubStockEntryDetail;
use Modules\Inventory\Entities\HubStockOut;
use Modules\Inventory\Entities\HubStockOutDetails;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\Inventory\Entities\StoreStockEntry;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\Inventory\Http\Requests\HoldDistributionRequest;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Procurement\Entities\StoreRequisitionDetails;
use Modules\Store\Entities\Store;
use Picqer\Barcode\Barcode;

class HoldDistributeController extends Controller
{
    public function hold_distribution()
    {
        $hold_distributes = HoldDistributionRequests::with('store')->where('status','=','Pending')->get();

        return view('inventory::inventory-management.hold-distribution.hold-distribution',compact('hold_distributes'));
    }

    public function hold_distribution_details($id)
    {
        $hold_distribution_requests = HoldDistributionRequests::find($id);

       $hold_distribution_request_details = HoldDistributionRequestDetails::with(['product','unit','generic','product_uom'])
            ->where('hold_distribution_request_id',$hold_distribution_requests->id)
            ->get();

        $hub_id = Store::where('id',$hold_distribution_requests->store_id)->select('hub_id')->first();
        $available_products = HubAvailableProduct::where('hub_id',$hub_id->hub_id)->first();
        return view('inventory::inventory-management.hold-distribution.hold-distribution-details',compact('hold_distribution_requests','hold_distribution_request_details','available_products'));
    }

    public function hold_distribution_process($id)
    {

       $hold_distribution_requests = HoldDistributionRequests::find($id);
       $hold_distribution_request_details = HoldDistributionRequestDetails::with(['product','unit','generic','product_uom', 'uomPrice'])
            ->where('hold_distribution_request_id',$hold_distribution_requests->id)
            ->get();

       $hub_id = Store::where('id',$hold_distribution_requests->store_id)->select('hub_id')->first();
       $available_products = HubAvailableProduct::where('hub_id',$hub_id->hub_id)->first();

       return view('inventory::inventory-management.hold-distribution.hold-distribution-process',compact('hold_distribution_request_details','hold_distribution_requests','available_products'));
    }


    public function distribute_hold_quantity(Request $request)
    {
        if( $request->has('distribution_quantity') && count( $request->get('distribution_quantity'))>0){
            for($i=0;$i<count($request->get('distribution_quantity')); $i++){
                if($request->distribution_quantity[$i] > $request->available_quantity[$i] ){
                    //return 'Distribute qty : '. $request->distribution_quantity[$i].'this is available qty :'.$request->available_quantity[$i];
                    return redirect()->back()->withErrors('Available quantity must be grater then or equal distribute quantity');
                }
            }

        }
        $distribution_no = $request->distribution_no;
        $store_id = $request->store_id;
        $store_type = $request->store_type;
        $store_requisition_id = $request->store_requisition_id;
        $product_id = $request->product_id;
        $generic_id = $request->generic_id;
        $unit_id = $request->unit_id;
        $vendor_id = $request->vendor_id;
        $request_quantity = $request->request_quantity;
        $distribution_quantity = $request->distribution_quantity;
        $uom_id = $request->uom_id;
        $warehouse_id = $request->warehouse_id;
        $store_requisition_no = $request->store_requisition_no;
        $available_quantity = $request->available_quantity;

        $stock_entry = [];
        $hub_stock_out = [];
        $distribution = [];
        $count_product = count($product_id);
        DB::beginTransaction();
        /*------ update store requisitions details according to store req id -------*/
        try {
            for($i = 0; $i < $count_product; $i++){
              $old_distribute_qty = StoreRequisitionDetails::where('product_id', $product_id[$i])
                  ->where('store_requisition_id',$store_requisition_id)
                  ->where('status','=','Partial')
                  ->first();
              $qty = $old_distribute_qty->distribute_quantity + $request_quantity[$i];
              $req_updare =  StoreRequisitionDetails::where('store_requisition_id',$store_requisition_id)
                  ->where('product_id', $product_id[$i])
                  ->where('status','=','Partial')
                  ->first();

              $req_updare->distribute_quantity = $qty;
              $req_updare->status = 'Approved';
              $req_updare->save();
          }
            try {

                /*----- insert data in to distribution and distribution details ------*/

                $distribution = new Distributions();
                $distribution->distribution_no = $distribution_no;
                $distribution->store_id = $store_id;
                $distribution->store_type = $store_type;
                $distribution->requisition_date = $request->requisition_date;
                $distribution->delivery_date = date("Y-m-d");
                $distribution->priority_distribution_type = "Low";
                $distribution->status = "Completed";
                $distribution->save();
                $distribution_id = $distribution->id;

                for ($i = 0; $i < $count_product; $i++) {
                    $distribution = [
                        'distribution_id' => $distribution_id,
                        'product_id' => $product_id[$i],
                        'generic_id' => $generic_id[$i],
                        'unit_id' => $unit_id[$i],
                        'vendor_id' => $vendor_id[$i],
                        'uom_id' => $uom_id[$i],
                        'request_quantity' => $request_quantity[$i],
                        'distribution_quantity' => $distribution_quantity[$i],
                        'status' => 'Processed',
                        'created_at' => Carbon::now(),
                    ];

                    DistributionDetails::insert($distribution);

                }
                /*----- insert data in to hub stock out  and hub stock out details ------*/
                try {

                    $hub_stock_out = new HubStockOut();
                    $get_hub_id = Store::where('id',$store_id)->select('hub_id')->first();
                    $hub_id = $get_hub_id->hub_id;

                    $hub_stock_out->hub_id = $hub_id;
                    $hub_stock_out->status = 0;
                    $hub_stock_out->save();
                    $hub_stock_out_id = $hub_stock_out->id;

                    for ($i=0; $i<$count_product; $i++){
                        $hub_stock_out = [
                            'hub_stock_out_id'=> $hub_stock_out_id,
                            'product_id'=>$product_id[$i],
                            'unit_id'=>$unit_id[$i],
                            'uom_id'=>$uom_id[$i],
                            'out_quantity'=>$distribution_quantity[$i],
                            'status'=>'Distributed'
                        ];

                        HubStockOutDetails::insert($hub_stock_out);

                    }
                    /*----- update hub available product details (available product) ------*/
                    try {

                        $hub = Store::where('id',$request->store_id)->first();
                        $hub_available = HubAvailableProduct::where('hub_id',$hub->hub_id)->first();
                        $update = [];

                        for ($i=0; $i<$count_product; $i++){
                            $old_qty = HubAvailableProductDetails::where('product_id', $product_id[$i])->where('hub_available_product_id',$hub_available->id)->first();

                            $update_qty = $old_qty->available_quantity - $distribution_quantity[$i];
                            $hub_stock_entry_details = HubStockEntryDetail::select('id', 'stock_quantity', 'out_quantity')->where('product_id', $old_qty->product_id)->where('hub_id', $hub_available->hub_id)->where('status', 1)->get();
                            $old_hub_stock_entry_stock_quantity = $hub_stock_entry_details->sum('stock_quantity');
                            $old_hub_stock_entry_out_quantity = $hub_stock_entry_details->sum('out_quantity');
                            $variable_qty = $distribution_quantity[$i];
                            $value = 0;
                            if ($old_hub_stock_entry_stock_quantity >= $distribution_quantity[$i] && ($old_hub_stock_entry_stock_quantity - $old_hub_stock_entry_out_quantity) >= $distribution_quantity[$i]) {
                                foreach ($hub_stock_entry_details as $hub_stock_entry_data) {
                                    for ($j = 1; $j <= $variable_qty; $j++) {
                                        if ($variable_qty >= 0 && $hub_stock_entry_data->stock_quantity >= ($hub_stock_entry_data->out_quantity +$j)) {
                                            $value = $j;
                                        }
                                    }
                                    if ($variable_qty > 0 && ($hub_stock_entry_data->out_quantity + $value) <= $hub_stock_entry_data->stock_quantity) {
                                        $update[] = HubStockEntryDetail::where('id', $hub_stock_entry_data->id)->update([
                                            'out_quantity' => $hub_stock_entry_data->out_quantity + $value,
                                            'out_date'     => Carbon::now(),
                                            'status'       => $hub_stock_entry_data->stock_quantity <= $hub_stock_entry_data->out_quantity + $value ? 0 : 1,
                                        ]);
                                    }
                                    $variable_qty -= $value;
                                }
                                $hub_available_product =  HubAvailableProductDetails::where('hub_available_product_id',$hub_available->id)->where('product_id', $product_id[$i])->first();
                                $hub_available_product->available_quantity = $update_qty;
                                $hub_available_product->save();
                            }else {
                                return redirect()->back()->withErrors("Distribution quantity is greater then hub stock quantity or not available in hub stock");
                            }
                        }
                        /*----- update store available product details ------*/
                        try {
                            $store_available_product = StoreAvailableProduct::where('store_id',$store_id)->first();

                            for ($i=0; $i<$count_product; $i++){
                                $old_qty = StoreAvailableProductDetails::where('product_id', $product_id[$i])
                                    ->where('store_available_product_id',$store_available_product->id)
                                    ->first();

                                $update_qty = $old_qty->available_quantity + $distribution_quantity[$i];

                                $store_available_products =  StoreAvailableProductDetails::where('store_available_product_id',$store_available_product->id)
                                    ->where('product_id', $product_id[$i])
                                    ->first();
                                //dd($store_available_products->available_quantity);
                                $store_available_products->available_quantity = $update_qty;
                                $store_available_products->save();
                            }
                            try {
                                $store_stock_entry = new StoreStockEntry();
                                $store_stock_entry->store_id = $store_id;
                                $store_stock_entry->distribution_id = $distribution_no;
                                $store_stock_entry->lot_no = 'EmptyLot01';
                                $store_stock_entry->requisition_no = $store_requisition_no;
                                $store_stock_entry->status = 0;
                                $store_stock_entry->save();
                                $store_stock_entry_id = $store_stock_entry->id;

                                for($i = 0; $i<$count_product; $i++){
                                    $stock_entry = [
                                        'store_stock_entry_id'=>$store_stock_entry_id,
                                        'sku'=>'sku1101',
//                                      'barcode'=>$barcode,
                                        'product_id'=>$product_id[$i],
                                        'unit_id'=>$unit_id[$i],
                                        'uom_id'=>$uom_id[$i],
                                        'ordered_quantity'=>$request_quantity[$i],
                                        'received_quantity'=>$distribution_quantity[$i],
                                        'warehouse_id'=>$warehouse_id,
                                        'status'=>0
                                    ];
                                    StoreStockEntryDetails::insert($stock_entry);
                                }
                                try {

                                    DB::table('hold_distribution_requests')
                                        ->where('store_requisition_id',$store_requisition_id)
                                        ->update(['status'=>'Approved']);
                                    DB::commit();
                                    return  redirect()->route('hold.distribution')->with('success','Successfully distributed');
                                }catch (\Exception $e){
                                    DB::rollBack();
                                    return redirect()->back()->withErrors("Something went wrong " . $e->getMessage());
                                }
                            }catch (\Exception $e){
                                DB::rollBack();
                                return redirect()->back()->withErrors("Something went wrong stock entry details" . $e->getMessage());
                            }
                        }catch (\Exception $e){
                            DB::rollBack();
                            return redirect()->back()->withErrors("Something went wrong store store available product details" . $e->getMessage());
                        }

                    }catch (\Exception $e){
                        DB::rollBack();
                        return redirect()->back()->withErrors("Something went wrong hub available product details" . $e->getMessage());
                    }

                }catch (\Exception $e){
                    DB::rollBack();
                    return redirect()->back()->withErrors("Something went wrong hub stock out details" . $e->getMessage());
                }

            }catch (\Exception $e){
                DB::rollBack();
                return redirect()->back()->withErrors("Something went wrong distribution details" . $e->getMessage());
            }
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors("Something went wrong store req details" . $e->getMessage());
        }


    }
}
