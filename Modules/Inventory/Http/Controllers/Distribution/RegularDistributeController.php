<?php

namespace Modules\Inventory\Http\Controllers\Distribution;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\DistributionDetails;
use Modules\Inventory\Entities\Distributions;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\HubStockEntryDetail;
use Modules\Inventory\Entities\HubStockOut;
use Modules\Inventory\Entities\HubStockOutDetails;
use Modules\Inventory\Entities\RegularDistributionRequestDetails;
use Modules\Inventory\Entities\RegularDistributionRequests;
use Modules\Inventory\Entities\StoreGoodReceive;
use Modules\Inventory\Entities\StoreGoodReceiveDetails;
use Modules\Inventory\Entities\StoreStockEntry;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use PhpParser\Node\Expr\New_;
use DB;


class RegularDistributeController extends Controller
{
    public function distribution_process()
    {
        $regular_distributes = RegularDistributionRequests::where('status','pending')->with('store')->get();

        return view('inventory::inventory-management.distribution-process.distribution-process',compact('regular_distributes'));
    }

    public function distribution_process_show($id)
    {
        $regular_distribution_requests = RegularDistributionRequests::find($id);
        $regular_distribution_request_details = RegularDistributionRequestDetails::with(['product','unit','generic','product_uom'])
            ->where('regular_distribution_request_id',$regular_distribution_requests->id)
            ->get();

        return view('inventory::inventory-management.distribution-process.distribution-process-details',compact('regular_distribution_requests','regular_distribution_request_details'));
    }

    public function distribution_process_process($id)
    {
        $regular_distribute_requests = RegularDistributionRequests::find($id);
        $regular_distribution_request_details = RegularDistributionRequestDetails::with(['product','unit','generic','product_uom','uomPrice'])
            ->where('regular_distribution_request_id',$regular_distribute_requests->id)
            ->get();

        return view('inventory::inventory-management.distribution-process.distribution-process-process',compact('regular_distribution_request_details','regular_distribute_requests'));
    }

    public function distribution_process_store(Request $request)
    {
        
        $distribution = new Distributions;
        $distribution_details = new DistributionDetails;

        $hub_stock_out = new HubStockOut;
        $hub_stock_out_details = new HubStockOutDetails;

        $distribution->distribution_no = $request->distribute_id;
        $distribution->store_id = $request->store_id;
        $distribution->store_type = $request->store_type;
        $distribution->requisition_date = $request->requisition_date;
        $distribution->delivery_date = $request->delivery_date;
        $distribution->priority_distribution_type = $request->priority;
        $distribution->status = 'Completed';
        $distribution->save();
        $distribution_id = $distribution->orderBy('id','DESC')->value('id');

        $array = [];
        foreach ($request->product_id as $i => $product){
            $data = array(
                'distribution_id'       => $distribution_id,
                'product_id'            => $request->product_id[$i],
                'generic_id'            => $request->generic_id[$i],
                'unit_id'               => $request->unit_id[$i],
                'vendor_id'             => $request->vendor_id[$i],
                'uom_id'                => $request->uom_id[$i],
                'request_quantity'      => $request->request_quantity[$i],
                'distribution_quantity' => $request->distribution_quantity[$i],
                'status'                => 'Processed'
            );
            array_push($array,$data);
        }

        $hub = Store::where('id',$request->store_id)->first();
        $hub_available = HubAvailableProduct::where('hub_id',$hub->hub_id)->first();
        $hub_stock_out->hub_id = $hub->hub_id;
        $hub_stock_out->save();
        $hub_stock_out_id = $hub_stock_out->orderBy('id','DESC')->value('id');

        $hub_stock_out_value = [];
        foreach ($request->product_id as $i => $product){
            $data = array(
                'hub_stock_out_id'  => $hub_stock_out_id,
                'product_id'        => $request->product_id[$i],
                'unit_id'           => $request->unit_id[$i],
                'uom_id'            => $request->uom_id[$i],
                'out_quantity'      => $request->distribution_quantity[$i],
                'status'            => 'Distributed'
            );
            array_push($hub_stock_out_value,$data);
        }
        $hub_stock_out_details->insert($hub_stock_out_value);

        $count = count($array);

        for ($i=0; $i<$count; $i++) {
            $productDetails = HubAvailableProductDetails::where('hub_available_product_id',$hub_available->id)->where('product_id',$array[$i]['product_id'])->first();
            $productDetails->update(['available_quantity' => $productDetails->available_quantity-$array[$i]['distribution_quantity']]);
        }

        $distribution_details->insert($array);
        RegularDistributionRequests::where('id', $request->distribute_id)->update(['status' => "Approved"]);

        // Update Store Good Receive table
        $storeGoodReceive = new StoreGoodReceive();
        $storeGoodReceive->distribution_id = $distribution_id;
        $storeGoodReceive->store_id = $request->store_id;
        $storeGoodReceive->store_type = $request->store_type;
        $storeGoodReceive->requisition_date = $request->requisition_date;
        $storeGoodReceive->delivery_date = $request->delivery_date;
        $storeGoodReceive->requisition_no = $request->store_requisition_no;
        $storeGoodReceive->priority_distribution_type = $request->priority;
        $storeGoodReceive->status = 'Pending';
        $storeGoodReceive->save();


        $hub_id = Store::select('hub_id')->where('id', (int)$request->store_id)->first();
        $hub_id = (int)$hub_id->hub_id;
        $distributed_product_id = $request->product_id;
        $distributed_product_qty = $request->distribution_quantity;

        for($i = 0; array_key_exists($i, $distributed_product_id); $i++)
        {
            $available_products = HubStockEntryDetail::where('hub_id', $hub_id)
            ->where('product_id', (int)$distributed_product_id[$i])
            ->where('stock_status', NULL)
            ->get();

            $remain_qty = $distributed_product_qty[$i];

            foreach($available_products as $product)
            {
                $current_qty = $product->stock_quantity - $product->out_quantity;

                if($remain_qty != 0)
                {
                    if($remain_qty < $current_qty)
                    {
                        // Update Hub Stock Entry Detail table
                        $hub_stock_entry_detail = HubStockEntryDetail::where('id', $product->id)->firstorFail();
                        $hub_stock_entry_detail->out_quantity = $hub_stock_entry_detail->out_quantity + $remain_qty;
                        $hub_stock_entry_detail->out_date = now();
                        $hub_stock_entry_detail->save();

                        // Update Store Good Receive Detail table
                        $storeGoodReceiveDetails = new StoreGoodReceiveDetails();
                        $storeGoodReceiveDetails->store_good_receive_id = $storeGoodReceive->id;
                        $storeGoodReceiveDetails->product_id = $product->product_id;
                        $storeGoodReceiveDetails->generic_id = $request->generic_id[$i];
                        $storeGoodReceiveDetails->unit_id = $request->unit_id[$i];
                        $storeGoodReceiveDetails->store_id = $request->store_id;
                        $storeGoodReceiveDetails->vendor_id = $request->vendor_id[$i];
                        $storeGoodReceiveDetails->uom_id = $request->uom_id[$i];
                        $storeGoodReceiveDetails->batch_no = $hub_stock_entry_detail->batch_no;
                        $storeGoodReceiveDetails->expiry_date = $product->expiry_date;
                        $storeGoodReceiveDetails->warehouse_id = $request->warehouse_id;
                        $storeGoodReceiveDetails->request_quantity = $distributed_product_qty[$i];
                        $storeGoodReceiveDetails->distribution_quantity = $remain_qty;
                        $storeGoodReceiveDetails->status = 'Pending';
                        $storeGoodReceiveDetails->save();

                        // Reduce remain quantity
                        $remain_qty = 0;
                    }
                    else{
                        // Update Hub Stock Entry Detail table
                        $hub_stock_entry_detail = HubStockEntryDetail::where('id', $product->id)->firstorFail();
                        $hub_stock_entry_detail->out_quantity = $hub_stock_entry_detail->out_quantity + $hub_stock_entry_detail->stock_quantity;
                        $hub_stock_entry_detail->out_date = now();
                        $hub_stock_entry_detail->stock_status = "empty";
                        $hub_stock_entry_detail->save();

                        $remain_qty = $remain_qty - $current_qty;

                        // Update Store Good Receive Detail table
                        $storeGoodReceiveDetails = new StoreGoodReceiveDetails();
                        $storeGoodReceiveDetails->store_good_receive_id = $storeGoodReceive->id;
                        $storeGoodReceiveDetails->product_id = $product->product_id;
                        $storeGoodReceiveDetails->generic_id = $request->generic_id[$i];
                        $storeGoodReceiveDetails->unit_id = $request->unit_id[$i];
                        $storeGoodReceiveDetails->store_id = $request->store_id;
                        $storeGoodReceiveDetails->vendor_id = $request->vendor_id[$i];
                        $storeGoodReceiveDetails->uom_id = $request->uom_id[$i];
                        $storeGoodReceiveDetails->batch_no = $hub_stock_entry_detail->batch_no;
                        $storeGoodReceiveDetails->expiry_date = $product->expiry_date;
                        $storeGoodReceiveDetails->warehouse_id = $request->warehouse_id;
                        $storeGoodReceiveDetails->request_quantity = $distributed_product_qty[$i];
                        $storeGoodReceiveDetails->distribution_quantity = $current_qty;
                        $storeGoodReceiveDetails->status = 'Pending';
                        $storeGoodReceiveDetails->save();
                    }
                }
            }
        }

        return redirect()->route('distribution.process')->with('success','Distribution has been processed successfully');
    }
}
