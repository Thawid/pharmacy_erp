<?php

namespace Modules\Inventory\Http\Controllers;

use App\Http\Middleware\Api\Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\HubStockEntry;
use Modules\Inventory\Entities\HubStockEntryDetail;
use Modules\Inventory\Entities\InventoryLocation;
use Modules\Product\Entities\ManageStoreProduct;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\UOM;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;
use Illuminate\Support\Facades\DB;

class IndependentStockController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $hub_user_store_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','hub')->value('store_id');
        $stores = Store::where('hub_id',$hub_user_store_id)->where('store_type','store')->get();
        $warehouse_id = Store::where('hub_id',$hub_user_store_id)->where('store_type','store')->value('warehouse_id');
        $store_product = [];
        $data = [];
        foreach ($stores as $store){
            $store_product[] = ManageStoreProduct::join('products','manage_store_products.product_id','=','products.id')
                ->select('products.id','products.product_name')
                ->where('store_id',$store->id)
                ->get();
        }
        foreach ($store_product as $temp_product){
            foreach ($temp_product as $product){
                $data[] = $product;
            }

        }
        $store_product = array_unique($data);
        $units = ProductUnit::where('status',1)->select('id','unit_name')->get();
        $uoms = UOM::where('status',1)->select('id','uom_name')->get();
        $inventory_location = InventoryLocation::select('id','state')->where('status',1)->get();
        return view('inventory::inventory-management.independent-stock.index',compact('store_product','units','uoms','inventory_location','warehouse_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('inventory::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $hub_id = StoreUser::where('user_id',\auth()->user()->id)->value('store_id');

        DB::beginTransaction();
        try {
            $hubstock_entry = new HubStockEntry();
            $hubstock_entry->grn_process_id = 0;
            $hubstock_entry->stock_id = rand(0,111).time();
            $hubstock_entry->lot_no = rand(0,222).time();
            $hubstock_entry->hub_id = $hub_id;
            $hubstock_entry->status = 0;
            $hubstock_entry->save();

            $hubstock_entry_details = [];

            foreach ($request->product_id as $key => $product_id) {

                $hubstock_entry_details[] = [
                    'hub_stock_entry_id' => $hubstock_entry->id,
                    'product_id' => $product_id,
                    'warehouse_id' => $request->warehouse_id,
                    'inventory_location_id' => $request->inventory_location_id[$key],
                    'unit_id' => $request->unit_id[$key],
                    'uom_id' => $request->uom_id[$key],
                    'batch_no' => $request->batch_no[$key],
                    'sku' => $product_id.'-'.$request->batch_no[$key].'-'.$request->inventory_location_id[$key],
                    'barcode' => $request->batch_no[$key],
                    'received_quantity' => $request->stock_quantity[$key],
                    'stock_quantity' => $request->stock_quantity[$key],
                    'shelf_no' => 0,
                    'expiry_date' => $request->expiry_date[$key],
                    'ysnWithoutShelf' => 0,
                    'ysnSpecialProduct' => 0,
                    'ysnGenerateBarcode' => 0,
                    'status' => 1,
                ];
            }

            HubStockEntryDetail::insert($hubstock_entry_details);

            $count = count($request->product_id);
            for ($i = 0; $i < $count; $i++) {
                $hub_available = HubAvailableProduct::where('hub_id', $hub_id)->first();
                $hubAvailableProductId = $hub_available->id;
                $hubavailableproductdetails = HubAvailableProductDetails::where('hub_available_product_id', $hubAvailableProductId)->where('product_id', $request->product_id[$i])->first();
                if (!$hubavailableproductdetails) {
                    $hub_available_product_details = new HubAvailableProductDetails();
                    $hub_available_product_details->hub_available_product_id = $hubAvailableProductId;
                    $hub_available_product_details->product_id = $request->product_id[$i];
                    $hub_available_product_details->available_quantity = $request->stock_quantity[$i];
                    $hub_available_product_details->unit_id = $request->unit_id[$i];
                    $hub_available_product_details->uom_id = $request->uom_id[$i];
                    $hub_available_product_details->status = 'Approved';
                    $hub_available_product_details->created_at = Carbon::now();
                    $hub_available_product_details->save();
                } else {
                    $update_qty = $hubavailableproductdetails->available_quantity + $request->stock_quantity[$i];
                    $hubavailableproductdetails->available_quantity = $update_qty;
                    $hubavailableproductdetails->save();
                }

            }
            DB::commit();

            return redirect('inventory/available-stock')->with('success', 'Independent stock entry has been created successfully');

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('inventory::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
