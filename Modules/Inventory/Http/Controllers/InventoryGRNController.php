<?php

namespace Modules\Inventory\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\TempGRN;
use Modules\Inventory\Entities\TempGrnDetails;
use Modules\Inventory\Repositories\InventoryGRNInterface;
use Modules\Inventory\Http\Requests\CreateInventoryGRNRequest;
use Modules\Inventory\Http\Requests\UpdateInventoryGRNRequest;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\ProductGeneric;
use Modules\Product\Entities\ProductUnit;
use Modules\Product\Entities\UOM;
use Modules\Procurement\Entities\PurchaseOrderDetails;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Inventory\Entities\InventoryGrn;
use Modules\Inventory\Entities\InventoryGrnDetails;
use DB;
use function GuzzleHttp\Promise\all;


class InventoryGRNController extends Controller
{
    private $purchaseOrders;

    public function __construct(InventoryGRNInterface $purchaseOrders)
    {
        $this->purchaseOrders= $purchaseOrders;
    }

    public function index()
    {
        $purchaseOrders = $this->purchaseOrders->index();
        return view('inventory::inventory-management.inventory.index', compact('purchaseOrders'));
    }

    public function create($id)
    {
        return "CREATE";
    }

    public function store(CreateInventoryGRNRequest $request)
    {
        return "STORE";
    }

    public function show(UpdateInventoryGRNRequest $request, $id)
    {

    }

    public function edit($id)
    {
        $purchaseOrders=$this->purchaseOrders->edit($id);
        $purchaseOrderDetails   = PurchaseOrderDetails::with('generic', 'unit', 'uom', 'product')
            ->where('purchase_order_id',$purchaseOrders->id)
            ->where('delivered_quantity','>',0)
            ->get();
        return view('inventory::inventory-management.inventory.edit', compact('purchaseOrders', 'purchaseOrderDetails'));
    }

    public function update(UpdateInventoryGRNRequest $request, $id)
    {
        //dd($request->all());
        $validate_date = $request->validated();
        if($validate_date && $request->change_status == 'Approved') {
            $purchaseOrder = PurchaseOrder::where('id', $id)->firstorFail();
            $purchaseOrder->grn_receive_status = "Received";
            $purchaseOrder->save();

            $storeInventoryGrn = new InventoryGrn();

            if ($request->hasFile('attach_file')) {
                $image = $request->file('attach_file');
                $file_name = time() . '.' . $image->extension();
                $location = public_path('uploads/grn-doc');
                if (!is_dir($location)) {
                    mkdir($location, 0755);
                }
                $request->attach_file->move(public_path('uploads/grn-doc'), $file_name);
            }

            $storeInventoryGrn->purchase_order_id = $id;
            $storeInventoryGrn->purchase_order_no = $request->purchase_order_no;
            $storeInventoryGrn->hub_requisition_no = $request->hub_requisition_no;
            $storeInventoryGrn->hub_id = $request->hub_id;
            $storeInventoryGrn->requisition_type = 'Pending';
            $storeInventoryGrn->lot_no = rand(1111, 9999) . time();
            $storeInventoryGrn->request_date = $request->request_date;
            $storeInventoryGrn->delivery_date = $request->delivery_date;
            $storeInventoryGrn->delivery_point = $request->delivery_point;
            $storeInventoryGrn->priority = $request->priority;
            $storeInventoryGrn->note = $request->note;
            $storeInventoryGrn->status = $request->change_status;
            $storeInventoryGrn->image = $file_name ?? NULL;

            $storeInventoryGrn->save();

            $storeGrnDetails = [];
            $i = 0;
            /*foreach ($request->product_id as $key => $value) {
                $storeGrnDetails[] = [
                    'grn_process_id' => $storeInventoryGrn->id,
                    'product_id' => $request->product_id[$key],
                    'generic_id' => $request->generic_id[$key],
                    'unit_id' => $request->unit_id[$key],
                    'uom_id' => $request->uom_id[$key],
                    'ordered_quantity' => $request->quantity[$key],
                    'received_quantity' => $request->received_quantity[$key],
                    'ysnReceived' => isset($request->ysnReceived[$key]),
                    'ysnQc' => isset($request->ysnQc[$key]),
                    'ysnStock' => isset($request->ysnStock[$key]),
                    'status' => $request->status[$key]
                ];
                $i++;
            }*/
            $temp_data = TempGrnDetails::where('purchase_order_no',$request->purchase_order_no)->get();
            $count = 0;
            $storeGrnDetails = [];

            foreach ($temp_data as $data){
                //dd($data);
                $storeGrnDetails []= [
                    'grn_process_id' =>$storeInventoryGrn->id,
                    'vendor_id'=>$data->vendor_id,
                    'product_id' =>$data->product_id,
                    'generic_id' =>$data->generic_id,
                    'uom_id' =>$data->uom_id,
                    'unit_id' =>$data->unit_id,
                    'ordered_quantity' =>$data->ordered_quantity,
                    'received_quantity' =>$data->received_quantity,
                    'batch_no' =>$data->batch_no,
                    'status' => 0,
                    'inventory_status' =>$data->inventory_status,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];

                $count++;
            }
            InventoryGrnDetails::insert($storeGrnDetails);

            DB::commit();
            return redirect()->route('inventory-GRN.index')->with('success', 'Inventory GRN Successfully Created');
        }else{
            return redirect()->back()->with('error', 'Inventory GRN change status before submit');
        }
    }

    public function destroy($id)
    {
        return "DESTROY";
    }


    public function grnProcess($purchase_order_id,$product_id){
    $grn_process = PurchaseOrder::join('purchase_order_details','purchase_orders.id','=','purchase_order_details.purchase_order_id')
            ->where('purchase_orders.id',$purchase_order_id)
            ->where('purchase_order_details.product_id',$product_id)
            ->first();

        return view('inventory::inventory-management.inventory.grn-process',compact('grn_process'));
    }

    public function process_batch_no(Request $request){
        //dd($request->all());
        $error = [
            'message' =>'Receive quantity can not be grater then order quantity'
        ];
        if(array_sum($request->received_quantity) > $request->order_quantity){
            return response()->json($error);
        }
        $temp_grn = new TempGRN();
        $temp_grn->purchase_order_no = $request->purchase_order_no;
        $temp_grn->save();
        $temp_grn_id = $temp_grn->id;
        $count = 0;
        $insert_data = [];

        DB::beginTransaction();

        try {
            foreach ($request->received_quantity as $receive_qty){
                $insert_data[] = [
                    'grn_process_id' => $temp_grn_id,
                    'vendor_id' => $temp_grn_id,
                    'product_id' => $request->product_id,
                    'purchase_order_no' => $request->purchase_order_no,
                    'unit_id' => $request->unit_id,
                    'generic_id' => $request->generic_id,
                    'uom_id' => $request->uom_id,
                    'ordered_quantity' => $request->order_quantity,
                    'received_quantity' => $receive_qty,
                    'batch_no' => $request->batch_no[$count],
                    'inventory_status' => $request->status[$count],
                ];
                $count++;
            }

            TempGrnDetails::insert($insert_data);
            DB::table('purchase_orders')
                ->join('purchase_order_details','purchase_orders.id','=','purchase_order_details.purchase_order_id')
                ->where('purchase_orders.purchase_order_no',$request->purchase_order_no)
                ->where('purchase_order_details.product_id',$request->product_id)
                ->update(['purchase_order_details.grn_status'=> 1]);

                DB::commit();


        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
        return  response()->json($request);
    }
}
