<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Modules\Inventory\Http\Requests\CreateWarehouseTypeRequest;
use Modules\Inventory\Http\Requests\UpdateWarehouseTypeRequest;
use Modules\Inventory\Repositories\WarehouseTypeRepositoryInterface;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;

class WarehouseTypeController extends Controller
{

    private $warehouseRepository;

    public function __construct(WarehouseTypeRepositoryInterface $warehouseRepository)
    {
        $this->warehouseRepository= $warehouseRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */


    public function index()
    {
        $warehouseTypes = $this->warehouseRepository->index();
        return view('inventory::inventory-management.warehouse-type.index',compact('warehouseTypes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');
        return view('inventory::inventory-management.warehouse-type.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateWarehouseTypeRequest $request)
    {
        $this->warehouseRepository->store($request);
        return redirect()->route('warehouse-type.index')->with('success','Warehouse type add successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $warehouseType = $this->warehouseRepository->edit($id);
        return view('inventory::inventory-management.warehouse-type.edit',compact('warehouseType'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateWarehouseTypeRequest $request, $id)
    {
        $this->warehouseRepository->update($request,$id);
        return redirect()->route('warehouse-type.index')->with('success','Update Warehouse type successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->warehouseRepository->destroy($id);
        return redirect()->route('warehouse-type.index')->with('success','Status update successfully');
    }
}
