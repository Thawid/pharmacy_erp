<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Modules\Inventory\Entities\Distributions;
use Modules\Inventory\Entities\HubStockEntryDetail;

class DashboardController extends Controller
{
    /**
     * Display a short data of the inventory.
     * @return Renderable
     */
    public function index()
    {
        $recent_expiry = HubStockEntryDetail::where('status',1)->latest()->take(5)->get(['expiry_date','sku','stock_quantity','created_at']);
        $recent_stockIn = HubStockEntryDetail::where('status',1)->latest()->take(5)->get(['sku','stock_quantity','created_at']);
        $type_wise_inventory_qty =  DB::table('inventory_grn_details')
            ->select('ordered_quantity','inventory_status')
            ->groupBy('inventory_status')
            ->get();
        $recent_distributions = Distributions::latest()->take(5)->get(['distribution_no','delivery_date','created_at']);
        return view('inventory::inventory-management.dashboard',
            compact('recent_expiry','recent_stockIn','type_wise_inventory_qty','recent_distributions')
        );
    }

}
