<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Repositories\WarehouseUserInterface;

class WarehouseUserController extends Controller
{

    private $warehouse_user;
    public function __construct(WarehouseUserInterface $warehouse_user){
        $this->warehouse_user = $warehouse_user;
    }
    
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
       $all_warehouse_user=  $this->warehouse_user->index();
       return view('inventory::inventory-management.warehouse-user.index',compact('all_warehouse_user'));
       
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $users_and_warehouses = $this->warehouse_user->create();
        return view('inventory::inventory-management.warehouse-user.create',compact('users_and_warehouses'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      $this->warehouse_user->store($request);
      return redirect()->route('warehouse-user.index')->with('success','Warehouse User Create Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $users_and_warehouses = $this->warehouse_user->edit($id);
        return view('inventory::inventory-management.warehouse-user.edit',compact('users_and_warehouses'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
       $this->warehouse_user->update($request,$id);
       return redirect()->route('warehouse-user.index')->with('success','Warehouse User Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $this->warehouse_user->destroy($id);
        return redirect()->back()->with('success','Warehouse User Status Change Successfully');
    }
}
