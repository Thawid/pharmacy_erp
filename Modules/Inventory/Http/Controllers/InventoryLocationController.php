<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\Warehouse;
use Modules\Inventory\Repositories\InventoryLocationRepositoryInterface;
use Modules\Inventory\Http\Requests\CreateInventoryLocationRequest;
use Modules\Inventory\Http\Requests\UpdateInventoryLocationRequest;
use Modules\Store\Entities\Store;

class InventoryLocationController extends Controller
{

    private $inventoryLocationRepository;

    public function __construct(InventoryLocationRepositoryInterface $inventoryLocationRepository)
    {
        $this->inventoryLocationRepository= $inventoryLocationRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $inventoryLocations = $this->inventoryLocationRepository->index();
        return view('inventory::inventory-management.inventory-location.index',compact('inventoryLocations'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');
        $inventoryLocationRepository = $this->inventoryLocationRepository->create();
//        $stores = Store::select('id', 'name')->where('store_type', 'hub')->where('status', 1)->get();
        return view('inventory::inventory-management.inventory-location.create', compact('inventoryLocationRepository'));
//        return view('inventory::inventory-management.inventory-location.create', compact('stores', 'inventoryLocationRepository'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateInventoryLocationRequest $request)
    {
//        return $request;
        $this->inventoryLocationRepository->store($request);
        return redirect()->route('inventory-location.index')->with('success','Inventory Location add successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */

    public function show(int $id)
    {
        $inventory_location = $this->inventoryLocationRepository->show($id);
        return view('inventory::inventory-management.inventory-location.show',compact('inventory_location'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(int $id)
    {
        Gate::authorize('hasEditPermission');
        $stores = Store::select('id', 'name')->where('store_type', 'hub')->where('status', 1)->get();
        
           
        
        
        $inventoryLocation=$this->inventoryLocationRepository->edit($id);
        $warehouse= Warehouse::select('id','name')->where('id', $inventoryLocation->warehouse_id)->first();
        return view('inventory::inventory-management.inventory-location.edit',compact('inventoryLocation', 'stores','warehouse'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateInventoryLocationRequest $request, int $id)
    {
        $this->inventoryLocationRepository->update($request,$id);
        return redirect()->route('inventory-location.index')->with('success','Update Inventory Location successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(int $id)
    {
        Gate::authorize('hasDeletePermission');
        $this->inventoryLocationRepository->destroy($id);
       return redirect()->route('inventory-location.index')->with('success','Status update successfully');
    }

    public function get_store(Request $request)
    {
        $store = Warehouse::where('store_id', $request->input('store'))
            ->where('status',1)->get();
        return response()->json(['data' => $store]);
    }
}
