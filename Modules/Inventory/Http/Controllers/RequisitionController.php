<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Entities\HoldDistributionRequestDetails;
use Modules\Inventory\Entities\HoldDistributionRequests;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\RegularDistributionRequestDetails;
use Modules\Inventory\Entities\RegularDistributionRequests;
use Modules\Inventory\Entities\WarehouseUser;
use Modules\Inventory\Repositories\RequisitionRepositoryInterface;
use Modules\Procurement\Entities\StoreRequisition;
use Modules\Procurement\Entities\StoreRequisitionDetails;
use Modules\Store\Entities\Store;

class RequisitionController extends Controller
{
    private $requisitionRepository;

    public function __construct(RequisitionRepositoryInterface $requisitionRepository)
    {
        $this->requisitionRepository= $requisitionRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

        $requisitions = $this->requisitionRepository->index();

        return view('inventory::inventory-management.requisition.index',compact('requisitions'));
    }

    public function requisition_list(){
        return view('inventory::inventory-management.requisition.requisition-list');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('inventory::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return null
     */
    public function store(Request $request)
    {
        $this->requisitionRepository->store($request);

        return redirect()->route('requisition.index')->with('success','Store requisition has been processed successfully');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $requisition = StoreRequisition::where('id',$id)->firstOrFail();
        $store_requisition_details = StoreRequisitionDetails::with(['product','generic','vendor','product_uom','unit'])
            ->where('store_requisition_id',$requisition->id)
            ->where('status', 'Pending')
            ->get();
        $available_products = HubAvailableProduct::where('hub_id',$requisition->store->hub_id)->first();

        /*$data = DB::table('store_requisition_details')
            ->join('products','store_requisition_details.product_id','=','products.id')
            ->join('product_generics','store_requisition_details.generic_id','=','product_generics.id')
            ->join('vendors','store_requisition_details.vendor_id','=','vendors.id')
            ->join('product_uoms','store_requisition_details.uom_id','=','product_uoms.id')
            ->join('product_unit','store_requisition_details.unit_id','=','product_unit.id')
            ->join('store_requisitions','store_requisitions.id','=','store_requisition_details.store_requisition_id')
            ->join('stores','store_requisitions.store_id','=','stores.id')
            ->join('hub_available_products','stores.hub_id','=','hub_available_products.hub_id')
            ->join('hub_available_product_details','hub_available_products.id','=','hub_available_product_details.hub_available_product_id')
            ->where('store_requisition_details.status','=','Pending')
            ->where('store_requisitions.id',$id)
            ->get();
        dd($data);*/



        return view('inventory::inventory-management.requisition.pen-details',compact('store_requisition_details','requisition','available_products'));
    }

    public  function process($id)
    {
        $store_requisition = StoreRequisition::where('id',$id)->firstOrFail();
        $store_requisition_details = StoreRequisitionDetails::with(['product','generic','vendor','product_uom','unit'])
            ->where('store_requisition_id',$store_requisition->id)
            ->where('status', 'Pending')
            ->get();
        $available_products = HubAvailableProduct::where('hub_id',$store_requisition->store->hub_id)->first();

        return view('inventory::inventory-management.requisition.pen-process',compact('store_requisition','store_requisition_details','available_products'));
    }

    public function distribution_process()
    {
        return view('inventory::inventory-management.distribution-process.distribution-process');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('inventory::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id, Request $request)
    {
        $this->requisitionRepository->destroy($request->input('id'));

        return response()->json($request->input('id'));

    }

}
