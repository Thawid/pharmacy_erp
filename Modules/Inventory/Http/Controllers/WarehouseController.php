<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Http\Requests\UpdateWarehouseRequest;
use Modules\Inventory\Http\Requests\CreateWarehouseRequest;
use Modules\Inventory\Entities\WarehouseType;
use Modules\Inventory\Repositories\WarehouseRepositoryInterface;
use Modules\Store\Entities\Store;

class WarehouseController extends Controller
{

    protected $warehouse;
    public function __construct(WarehouseRepositoryInterface $warehouse){
        $this->warehouse = $warehouse;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $warehouses = $this->warehouse->index();
        return view('inventory::inventory-management.warehouse.index', compact('warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        Gate::authorize('hasCreatePermission');
        $stores         = Store::select('id', 'name')->where('store_type', 'hub')->where('status', 1)->get();
        $warehouseTypes = WarehouseType::select('id', 'name')->where('status', 1)->get();
        return view('inventory::inventory-management.warehouse.create', compact('stores', 'warehouseTypes'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateWarehouseRequest $request)
    {
        $this->warehouse->store($request);
        return redirect()->route('warehouse.index')->with('success', 'Warehouse add Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        Gate::authorize('hasEditPermission');
        $warehouse      = $this->warehouse->edit($id);
        $stores         = Store::select('id', 'name')->where('status', 1)->get();
        $warehouseTypes = WarehouseType::select('id', 'name')->where('status', 1)->get();
        return view('inventory::inventory-management.warehouse.edit', compact('warehouse', 'stores', 'warehouseTypes'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateWarehouseRequest $request, $id)
    {
        $this->warehouse->update($request, $id);
        return redirect()->route('warehouse.index')->with('success', 'Warehouse Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Gate::authorize('hasDeletePermission');
        $this->warehouse->destroy($id);
        return redirect()->route('warehouse.index')->with('success', 'Status Successfully Updated');
    }
}
