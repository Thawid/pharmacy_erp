<?php

namespace Modules\Inventory\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Milon\Barcode\DNS1D;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\InventoryGrn;
use Modules\Inventory\Entities\InventoryGrnDetails;
use Modules\Inventory\Entities\HubStockEntry;
use Modules\Inventory\Entities\HubStockEntryDetail;
use Modules\Inventory\Entities\InventoryLocation;
use Modules\Inventory\Entities\StoreStockEntry;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\Inventory\Entities\WarehouseUser;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use Picqer\Barcode\BarcodeGeneratorHTML;


class HubStockEntryController extends Controller
{

    public function index()
    {
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $grn_processes = InventoryGrn::latest()->get();
        }else{
            $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
            $hub_id = Store::where('warehouse_id',$warehouse_id)->value('hub_id');
            $grn_processes = InventoryGrn::latest()->where('hub_id',$hub_id)->get();
        }
        return view('inventory::inventory-management.hub-stock-entry.index', compact('grn_processes'));
    }

    public function hubStockEntryCreate($id)
    {
        $inventory_locations = InventoryLocation::where('status', 1)->get(['id', 'state', 'warehouse_id']);
        $stock_id = rand(0, 999) . time();
        $grn_process = InventoryGrn::where('id', $id)->firstOrFail();
        $grn_process_details = InventoryGrnDetails::where('grn_process_id', $grn_process->id)
            ->where('inventory_status','=','Regular')
            ->get();
        return view('inventory::inventory-management.hub-stock-entry.create', compact('grn_process', 'grn_process_details', 'stock_id', 'inventory_locations'));
    }

    public function hubStockEntryStore(Request $request)
    {
        //dd($request->all());

        DB::beginTransaction();
        try {

            $inventory_grn_id = $request->inventory_grn_id;
            DB::table('inventory_grns')->where('id',$inventory_grn_id)->update([
                'requisition_type'=>'Approved'
            ]);

            $hubstock_entry = new HubStockEntry();
            $hubstock_entry->grn_process_id = $request->inventory_grn_id;
            $hubstock_entry->stock_id = $request->stock_id;
            $hubstock_entry->lot_no = $request->lot_no;
            $hubstock_entry->hub_id = $request->hub_id;
            $hubstock_entry->status = $request->status;
            $hubstock_entry->save();

            $hubstock_entry_details = [];

            foreach ($request->product_id as $key => $product_id) {

                $hubstock_entry_details[] = [
                    'hub_stock_entry_id' => $hubstock_entry->id,
                    'product_id' => $product_id,
                    'hub_id' => $request->hub_id,
                    'warehouse_id' => $request->warehouse_id,
                    'inventory_location_id' => $request->inventory_location_id[$key],
                    'unit_id' => $request->unit_id[$key],
                    'uom_id' => $request->uom_id[$key],
                    'batch_no' => $request->batch_no[$key],
                    'sku' => $request->sku[$key],
                    'barcode' => $request->sku[$key],
                    'received_quantity' => $request->received_quantity[$key],
                    'stock_quantity' => $request->stock_quantity[$key],
                    'shelf_no' => 0,
                    'expiry_date' => $request->expiry_date[$key],
                    'ysnWithoutShelf' => $request->ysnWithoutShelf,
                    'ysnSpecialProduct' => $request->ysnSpecialProduct,
                    'ysnGenerateBarcode' => $request->ysnGenerateBarcode,
                    'status' => 1,
                ];
            }

            HubStockEntryDetail::insert($hubstock_entry_details);

            $count = count($request->product_id);
            for ($i = 0; $i < $count; $i++) {
                $hub_available = HubAvailableProduct::where('hub_id', $request->hub_id)->first();
                $hubAvailableProductId = $hub_available->id;
                $hubavailableproductdetails = HubAvailableProductDetails::where('hub_available_product_id', $hubAvailableProductId)->where('product_id', $request->product_id[$i])->first();
                if (!$hubavailableproductdetails) {
                    $hub_available_product_details = new HubAvailableProductDetails();
                    $hub_available_product_details->hub_available_product_id = $hubAvailableProductId;
                    $hub_available_product_details->product_id = $request->product_id[$i];
                    $hub_available_product_details->available_quantity = $request->stock_quantity[$i];
                    $hub_available_product_details->unit_id = $request->unit_id[$i];
                    $hub_available_product_details->uom_id = $request->uom_id[$i];
                    $hub_available_product_details->status = 'Approved';
                    $hub_available_product_details->created_at = Carbon::now();
                    $hub_available_product_details->save();
                } else {
                    $update_qty = $hubavailableproductdetails->available_quantity + $request->stock_quantity[$i];
                    $hubavailableproductdetails->available_quantity = $update_qty;
                    $hubavailableproductdetails->save();
                }

            }
            DB::commit();

            return redirect('inventory/available-stock')->with('success', 'Hub Stock Entry and Details has been Created successfully');

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function availableStock()
    {
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $details = DB::table('hub_stock_entry_details')
                ->leftJoin('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->leftJoin('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->select(
                    'hub_stock_entry_details.id',
                    'hub_stock_entry_details.warehouse_id', 'hub_stock_entry_details.inventory_location_id',
                    'hub_stock_entry_details.sku', 'hub_stock_entry_details.received_quantity', 'hub_stock_entry_details.stock_quantity', 'hub_stock_entry_details.shelf_no',
                    'hub_stock_entry_details.expiry_date', 'products.product_name', 'uoms.uom_name', 'hub_stock_entry_details.product_id',
                    DB::raw('SUM(hub_stock_entry_details.stock_quantity) as stock_quantity')
                )
                ->groupBy('hub_stock_entry_details.product_id')
                ->get();
        }else if(auth()->user()->role == 'hub_user'){
            $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','=','hub')->value('store_id');
            $details = DB::table('hub_stock_entry_details')
                ->leftJoin('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->leftJoin('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->join('hub_stock_entries','hub_stock_entry_details.hub_stock_entry_id','=','hub_stock_entries.id')
                ->select(
                    'hub_stock_entry_details.id',
                    'hub_stock_entry_details.warehouse_id', 'hub_stock_entry_details.inventory_location_id',
                    'hub_stock_entry_details.sku', 'hub_stock_entry_details.received_quantity', 'hub_stock_entry_details.stock_quantity', 'hub_stock_entry_details.shelf_no',
                    'hub_stock_entry_details.expiry_date', 'products.product_name', 'uoms.uom_name', 'hub_stock_entry_details.product_id',
                    DB::raw('SUM(hub_stock_entry_details.stock_quantity) as stock_quantity')
                )->where('hub_stock_entries.hub_id',$hub_id)
                ->groupBy('hub_stock_entry_details.product_id')
                ->get();
        }else if(auth()->user()->role == 'warehouse_user'){
            $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
            $details = DB::table('hub_stock_entry_details')
                ->leftJoin('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->leftJoin('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->select(
                    'hub_stock_entry_details.id',
                    'hub_stock_entry_details.warehouse_id', 'hub_stock_entry_details.inventory_location_id',
                    'hub_stock_entry_details.sku', 'hub_stock_entry_details.received_quantity', 'hub_stock_entry_details.stock_quantity', 'hub_stock_entry_details.shelf_no',
                    'hub_stock_entry_details.expiry_date', 'products.product_name', 'uoms.uom_name', 'hub_stock_entry_details.product_id',
                    DB::raw('SUM(hub_stock_entry_details.stock_quantity) as stock_quantity')
                )->where('hub_stock_entry_details.warehouse_id',$warehouse_id)
                ->groupBy('hub_stock_entry_details.product_id')
                ->get();
        }else{
            return redirect()->back()->withErrors('You are now allow to access this page');
        }

        return view('inventory::inventory-management.hub-stock-entry.available-stock', compact('details'));
    }

    public function availableStockDetails($id)
    {
       $product = \Illuminate\Support\Facades\DB::table('products')
            ->join('hub_stock_entry_details','products.id','=','hub_stock_entry_details.product_id')
            ->join('product_generics','products.product_generic_id','=','product_generics.id')
            ->where('hub_stock_entry_details.product_id',$id)
            ->select('products.product_name','products.brand_name','product_generics.name as generic_name')
            ->first();
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $data = \Illuminate\Support\Facades\DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->leftJoin('inventory_grns', 'hub_stock_entries.grn_process_id', '=', 'inventory_grns.id')
                ->join('product_unit', 'hub_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->join('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->where('hub_stock_entry_details.product_id', $id)
                ->orderBy('hub_stock_entry_details.id','DESC')
                ->get();
        }else if(auth()->user()->role == 'hub_user'){
            $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','=','hub')->value('store_id');
            $data = \Illuminate\Support\Facades\DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->leftJoin('inventory_grns', 'hub_stock_entries.grn_process_id', '=', 'inventory_grns.id')
                ->join('product_unit', 'hub_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->join('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->where('hub_stock_entry_details.product_id', $id)
                ->where('hub_stock_entries.hub_id',$hub_id)
                ->orderBy('hub_stock_entry_details.id','DESC')
                ->get();
        }else if(auth()->user()->role == 'warehouse_user'){
            $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
            $data = \Illuminate\Support\Facades\DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->leftJoin('inventory_grns', 'hub_stock_entries.grn_process_id', '=', 'inventory_grns.id')
                ->join('product_unit', 'hub_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->join('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->where('hub_stock_entry_details.product_id', $id)
                ->where('hub_stock_entry_details.warehouse_id',$warehouse_id)
                ->orderBy('hub_stock_entry_details.id','DESC')
                ->get();
        }else{
            return redirect()->back()->withErrors('You are now allow to access this page');
        }
        return view('inventory::inventory-management.hub-stock-entry.available-stock-details', compact( 'data','product'));
    }


}
