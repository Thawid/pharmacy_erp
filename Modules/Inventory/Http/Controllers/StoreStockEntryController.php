<?php

namespace Modules\Inventory\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Milon\Barcode\DNS1D;
use Modules\Inventory\Entities\HubAvailableProduct;
use Modules\Inventory\Entities\HubAvailableProductDetails;
use Modules\Inventory\Entities\InventoryGrn;
use Modules\Inventory\Entities\InventoryGrnDetails;
use Modules\Inventory\Entities\HubStockEntry;
use Modules\Inventory\Entities\HubStockEntryDetail;
use Modules\Inventory\Entities\InventoryLocation;
use Modules\Inventory\Entities\StoreAvailableProduct;
use Modules\Inventory\Entities\StoreAvailableProductDetails;
use Modules\Inventory\Entities\StoreGoodReceive;
use Modules\Inventory\Entities\StoreGoodReceiveDetails;
use Modules\Inventory\Entities\StoreStockEntry;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Modules\Inventory\Entities\WarehouseUser;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;
use Picqer\Barcode\BarcodeGeneratorHTML;


class StoreStockEntryController extends Controller
{

    public function index()
    {
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $grn_processes = StoreGoodReceive::latest()->get();
        }else{      
          $store_user = StoreUser::select('store_id')->where('user_id',auth()->user()->id)->where('store_type','store')->first();
            $grn_processes = StoreGoodReceive::latest()->where('store_id',$store_user->store_id)->get();
        }
        return view('inventory::inventory-management.store-stock-entry.index', compact('grn_processes'));
    }

    public function storeStockEntryCreate($id)
    {
        $grn_process = StoreGoodReceive::where('id', $id)->firstOrFail();
        $grn_process_details = StoreGoodReceiveDetails::where('store_good_receive_id', $id)->get();
        return view('inventory::inventory-management.store-stock-entry.create', compact('grn_process', 'grn_process_details'));
    }

    public function storeStockEntryStore(Request $request)
    {
        //return $request->all();
        DB::beginTransaction();
        try {
            $store_stock_entry = new StoreStockEntry();
            $store_stock_entry->store_id = $request->store_id;
            $store_stock_entry->distribution_id = $request->distribution_id;
            $store_stock_entry->lot_no = mt_rand();
            $store_stock_entry->requisition_no = $request->requisition_no;
            $store_stock_entry->status = $request->s_status;
            $store_stock_entry->save();

            $store_stock_entry_details = [];

            foreach ($request->product_id as $key => $product_id) {
                $store_stock_entry_details[] = [
                    'store_stock_entry_id' => $store_stock_entry->id,
                    'sku' => mt_rand(),
                    'product_id' => $product_id,
                    'unit_id' => $request->unit_id[$key],
                    'uom_id' => $request->uom_id[$key],
                    'ordered_quantity' => $request->request_quantity[$key],
                    'received_quantity' => $request->stock_quantity[$key],
                    'store_id' => $request->store_id,
                    'batch_no' => $request->batch_no[$key],
                    'expire_date' => $request->expiry_date[$key],
                    'warehouse_id' => $request->warehouse_id[$key],
                    'status' => $request->status[$key],
                ];

                // Update Store Available Product Detail table
                $store_available = StoreAvailableProduct::where('store_id', $request->store_id)->first();
                $storeAvailableProductId = $store_available->id;
                $storeavailableproductdetails = StoreAvailableProductDetails::where('store_available_product_id', $storeAvailableProductId)->where('product_id', $product_id)->first();
                if (!$storeavailableproductdetails) {
                    $store_available_product_details = new StoreAvailableProductDetails(); 
                    $store_available_product_details->store_available_product_id = $storeAvailableProductId;
                    $store_available_product_details->product_id = $product_id;
                    $store_available_product_details->available_quantity = $request->stock_quantity[$key];
                    $store_available_product_details->unit_id = $request->unit_id[$key];
                    $store_available_product_details->uom_id = $request->uom_id[$key];
                    $store_available_product_details->status = 'Approved';
                    $store_available_product_details->created_at = Carbon::now();
                    $store_available_product_details->save();
                } else {
                    $update_qty = $storeavailableproductdetails->available_quantity + $request->stock_quantity[$key];
                    $storeavailableproductdetails->available_quantity = $update_qty;
                    $storeavailableproductdetails->save();
                }  
            }
           StoreStockEntryDetails::insert($store_stock_entry_details);
           $store_good_receive= StoreGoodReceive::find($request->store_grn_id);
           $store_good_receive->status = "Entered";
           $store_good_receive->save();

           
            DB::commit();

            return redirect('inventory/store-stocks/list')->with('success', 'Store Stock Entry and Details has been Created successfully');

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors("error " . $e->getMessage());
        }
    }

    public function availableStock()
    {
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $details = DB::table('hub_stock_entry_details')
                ->leftJoin('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->leftJoin('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->select(
                    'hub_stock_entry_details.id',
                    'hub_stock_entry_details.warehouse_id', 'hub_stock_entry_details.inventory_location_id',
                    'hub_stock_entry_details.sku', 'hub_stock_entry_details.received_quantity', 'hub_stock_entry_details.stock_quantity', 'hub_stock_entry_details.shelf_no',
                    'hub_stock_entry_details.expiry_date', 'products.product_name', 'uoms.uom_name', 'hub_stock_entry_details.product_id',
                    DB::raw('SUM(hub_stock_entry_details.stock_quantity) as stock_quantity')
                )
                ->groupBy('hub_stock_entry_details.product_id')
                ->get();
        }else if(auth()->user()->role == 'hub_user'){
            $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','=','hub')->value('store_id');
            $details = DB::table('hub_stock_entry_details')
                ->leftJoin('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->leftJoin('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->join('hub_stock_entries','hub_stock_entry_details.hub_stock_entry_id','=','hub_stock_entries.id')
                ->select(
                    'hub_stock_entry_details.id',
                    'hub_stock_entry_details.warehouse_id', 'hub_stock_entry_details.inventory_location_id',
                    'hub_stock_entry_details.sku', 'hub_stock_entry_details.received_quantity', 'hub_stock_entry_details.stock_quantity', 'hub_stock_entry_details.shelf_no',
                    'hub_stock_entry_details.expiry_date', 'products.product_name', 'uoms.uom_name', 'hub_stock_entry_details.product_id',
                    DB::raw('SUM(hub_stock_entry_details.stock_quantity) as stock_quantity')
                )->where('hub_stock_entries.hub_id',$hub_id)
                ->groupBy('hub_stock_entry_details.product_id')
                ->get();
        }else if(auth()->user()->role == 'warehouse_user'){
            $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
            $details = DB::table('hub_stock_entry_details')
                ->leftJoin('products', 'hub_stock_entry_details.product_id', '=', 'products.id')
                ->leftJoin('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->select(
                    'hub_stock_entry_details.id',
                    'hub_stock_entry_details.warehouse_id', 'hub_stock_entry_details.inventory_location_id',
                    'hub_stock_entry_details.sku', 'hub_stock_entry_details.received_quantity', 'hub_stock_entry_details.stock_quantity', 'hub_stock_entry_details.shelf_no',
                    'hub_stock_entry_details.expiry_date', 'products.product_name', 'uoms.uom_name', 'hub_stock_entry_details.product_id',
                    DB::raw('SUM(hub_stock_entry_details.stock_quantity) as stock_quantity')
                )->where('hub_stock_entry_details.warehouse_id',$warehouse_id)
                ->groupBy('hub_stock_entry_details.product_id')
                ->get();
        }else{
            return redirect()->back()->withErrors('You are now allow to access this page');
        }

        return view('inventory::inventory-management.hub-stock-entry.available-stock', compact('details'));
    }

    public function availableStockDetails($id)
    {
       $product = \Illuminate\Support\Facades\DB::table('products')
            ->join('hub_stock_entry_details','products.id','=','hub_stock_entry_details.product_id')
            ->join('product_generics','products.product_generic_id','=','product_generics.id')
            ->where('hub_stock_entry_details.product_id',$id)
            ->select('products.product_name','products.brand_name','product_generics.name as generic_name')
            ->first();
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'developer') {
            $data = \Illuminate\Support\Facades\DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->leftJoin('inventory_grns', 'hub_stock_entries.grn_process_id', '=', 'inventory_grns.id')
                ->join('product_unit', 'hub_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->join('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->where('hub_stock_entry_details.product_id', $id)
                ->orderBy('hub_stock_entry_details.id','DESC')
                ->get();
        }else if(auth()->user()->role == 'hub_user'){
            $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','=','hub')->value('store_id');
            $data = \Illuminate\Support\Facades\DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->leftJoin('inventory_grns', 'hub_stock_entries.grn_process_id', '=', 'inventory_grns.id')
                ->join('product_unit', 'hub_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->join('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->where('hub_stock_entry_details.product_id', $id)
                ->where('hub_stock_entries.hub_id',$hub_id)
                ->orderBy('hub_stock_entry_details.id','DESC')
                ->get();
        }else if(auth()->user()->role == 'warehouse_user'){
            $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
            $data = \Illuminate\Support\Facades\DB::table('hub_stock_entries')
                ->join('hub_stock_entry_details', 'hub_stock_entries.id', '=', 'hub_stock_entry_details.hub_stock_entry_id')
                ->leftJoin('inventory_grns', 'hub_stock_entries.grn_process_id', '=', 'inventory_grns.id')
                ->join('product_unit', 'hub_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->join('uoms', 'hub_stock_entry_details.uom_id', '=', 'uoms.id')
                ->where('hub_stock_entry_details.product_id', $id)
                ->where('hub_stock_entry_details.warehouse_id',$warehouse_id)
                ->orderBy('hub_stock_entry_details.id','DESC')
                ->get();
        }else{
            return redirect()->back()->withErrors('You are now allow to access this page');
        }
        return view('inventory::inventory-management.hub-stock-entry.available-stock-details', compact( 'data','product'));
    }


}
