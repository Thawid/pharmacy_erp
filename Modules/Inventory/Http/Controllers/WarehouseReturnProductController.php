<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Entities\InventoryGrn;
use Modules\Inventory\Entities\InventoryGrnDetails;
use Modules\Inventory\Entities\WarehouseUser;
use Modules\Store\Entities\Store;
use Modules\Store\Entities\StoreUser;

class WarehouseReturnProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        if(auth()->user()->role == 'warehouse_user') {
          $warehouse_id = WarehouseUser::where('user_id',auth()->user()->id)->where('status',1)->value('warehouse_id');
          $hub_id = Store::where('warehouse_id',$warehouse_id)->value('hub_id');
          $return_products = InventoryGrnDetails::join('inventory_grns', 'inventory_grns.id', '=', 'inventory_grn_details.grn_process_id')
              ->join('products','inventory_grn_details.product_id','=','products.id')
              ->join('uoms','inventory_grn_details.uom_id','=','uoms.id')
              ->Leftjoin('vendors','inventory_grn_details.vendor_id','=','vendors.id')
              ->select('products.product_name','vendors.name as vendor_name','inventory_grn_details.batch_no','inventory_grn_details.received_quantity','uoms.uom_name')
              ->where('inventory_grn_details.inventory_status', '=', 'Return')
              ->where('inventory_grns.hub_id',$hub_id)
              ->get();
        }else if(auth()->user()->role == 'hub_user'){
          $hub_id = StoreUser::where('user_id',auth()->user()->id)->where('store_type','=','hub')->value('store_id');
          $return_products = InventoryGrnDetails::join('inventory_grns', 'inventory_grns.id', '=', 'inventory_grn_details.grn_process_id')
                ->join('products','inventory_grn_details.product_id','=','products.id')
                ->join('uoms','inventory_grn_details.uom_id','=','uoms.id')
                ->Leftjoin('vendors','inventory_grn_details.vendor_id','=','vendors.id')
                ->select('products.product_name','vendors.name as vendor_name','inventory_grn_details.batch_no','inventory_grn_details.received_quantity','uoms.uom_name')
                ->where('inventory_grn_details.inventory_status', '=', 'Return')
                ->where('inventory_grns.hub_id',$hub_id)
                ->get();
        }else{
            return redirect()->back()->withErrors('You are not permitted to access this path');
        }
        return view('inventory::inventory-management.return.index',compact('return_products'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('procurement::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
