<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Modules\Inventory\Entities\StoreStockEntry;
use Modules\Inventory\Entities\StoreStockEntryDetails;
use Illuminate\Contracts\Support\Renderable;
use Modules\Store\Entities\StoreUser;

class StoreBarcodeController extends Controller
{
    /**
     * Display a list of the stock entries.
     *
     * @return Renderable
     */
    public function index()
    {
        $user_id = Auth::user()['id'];
        $get_products = [];
        $new_products = [];
        if(auth()->user()['role'] === 'store_user') {
            $storeUser = StoreUser::where('user_id', $user_id)->where('store_type', 'store')->where('status', 1)->first();
            $storeStockEntries = StoreStockEntry::where('store_id', $storeUser->store_id)->get();
            foreach ($storeStockEntries as $data){
                $get_products[] = StoreStockEntryDetails::join('products', 'store_stock_entry_details.product_id', '=', 'products.id')
                    ->join('product_unit', 'store_stock_entry_details.unit_id', '=', 'product_unit.id')
                    ->where('store_stock_entry_id', $data->id)->get();
            }
        }
        foreach ($get_products as $item){
            foreach ($item as $temp){
                $new_products[] = $temp;
            }
        }
        $products = $new_products;

        return view('inventory::inventory-management.store-barcode.index', compact('products'));
    }

    /**
     * Display a short data of the selected stock entry.
     *
     * @param $product_id
     * @return Renderable
     */
    public function show_store_product($store_stock_entry_id, $batch_no, $product_id)
    {
        $product = null;
        if(auth()->user()['role'] === 'store_user') {
            $product = StoreStockEntryDetails::join('products', 'store_stock_entry_details.product_id', '=', 'products.id')
                ->join('product_unit', 'store_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->where('store_stock_entry_id', $store_stock_entry_id)
                ->where('batch_no', $batch_no)
                ->where('product_id', $product_id)
                ->first();
        }
        return view('inventory::inventory-management.store-barcode.create', compact('product'));
    }

    /**
     * Process and generate Barcode.
     *
     * @param Request $request
     * @return Renderable
     */
    public function generate_barcode(Request $request)
    {
        $product = null;
        $barcode_qty = $request->barcode_qty ?? 0;
        if(auth()->user()['role'] === 'store_user') {
            $product = StoreStockEntryDetails::join('products', 'store_stock_entry_details.product_id', '=', 'products.id')
                ->join('product_unit', 'store_stock_entry_details.unit_id', '=', 'product_unit.id')
                ->where('store_stock_entry_id', $request['store_stock_entry_id'])
                ->where('batch_no', $request['batch_no'])
                ->where('product_id', $request['product_id'])
                ->first();
        }

        return view('inventory::inventory-management.store-barcode.barcodes', compact( 'product', 'barcode_qty'));
    }
}
