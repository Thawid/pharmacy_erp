<?php

namespace Modules\Inventory\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateWarehouseRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>'required|regex:/^[\pL\s\-]+$/u|min:5|max:20',
            'store_id'          =>'required',
            'warehouse_type_id' =>'required',
            'phone'             =>'required|regex:/(01)[0-9]{9}/|min:11|max:14',
            'email'             =>'string|email|unique:warehouses|regex:/(.+)@(.+)\.(.+)/i',
            'code'              => 'required',
            'address'           =>'string|min:5|max:255'
        ];
    }

    public function messages()
    {
        return [
            'name.required'             =>'Warehouse name field must be required',
            'name.regex'                =>'Warehouse name must only contain letters',
            'name.min'                  =>'Warehouse name must be at least 5 characters',
            'name.max'                  =>'Warehouse name must be maximum 20 characters',
            'name.unique'               =>'This Warehouse already exits',
            'store_id.required'         => 'Please select Store Name',
            'warehouse_type_id.required'=>'Please select Warehouse type',
            'phone.required'            =>'Phone number is required field',
            'email.unique'              =>'This email address already taken',
            'address.max'               =>'Maximum Length 255',
            'address.min'               =>'Minimum Length 05',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
