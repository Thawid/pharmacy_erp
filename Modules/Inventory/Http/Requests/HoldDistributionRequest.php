<?php

namespace Modules\Inventory\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HoldDistributionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if( $this->request->has('distribution_quantity') && count( $this->request->get('distribution_quantity'))>0){
            if($this->request->has('distribution_quantity') >= $this->request->has('available_quantity') ){
                return [

                ];
            }
        }
        return [
            'request_quantity'=>'required|array',
            'available_quantity'=>'required|array',
            'distribution_quantity'=>'required|array',
            'distribution_quantity.*' => 'bail|integer|gte:available_quantity',
        ];
    }


    public function messages()
    {
        return [
            'distribution_quantity.*.integer'=>'Invalid Distribute Quantity',
            'distribution_quantity.*.gte'=>'Available quantity must be grater then or equal distribute quantity',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
