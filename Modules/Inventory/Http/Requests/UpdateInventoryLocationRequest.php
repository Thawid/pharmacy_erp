<?php

namespace Modules\Inventory\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInventoryLocationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse' => 'required',
            'type' => 'required|min:5|regex:/^[a-zA-Z\s]*$/',
            'state' => 'required|min:3',
            'floor' => 'required|max:3|regex:/^[A-Za-z0-9]+$/',
            'room' => 'required|numeric',
            'room_point' => 'required|alpha',
            'self' => 'required',
        ];
    }
    public function messages()
    {
        return [

            'floor.'=>'Warehouse field is required',
            'floor.regex'=>'Floor must only contain letters and numbers or only numbers',
            'type.regex'=>'Inventory Type must only contain letters.',
            'state.regex'=>'Inventory State must only contain letters.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
