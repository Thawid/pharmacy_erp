@extends('dboard.index')

@section('title','Requisition Details | HQ')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-6 text-left">
                            <h2 class="title-heading">Purchase Request Details</h2>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a class="btn index-btn" href={{route('purchase.distribution')}}> Purchase Distribution
                            </a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- table -->
                    <div id="printDiv">
                        <!-- info area -->
                        <div class="row mb-4">
                            <div class="col-md-6">
                                <p><strong>Distribution ID: {{ $purchase_distribution_requests->id }}</strong></p>
                                <p><strong>Store : {{ $purchase_distribution_requests->store->name }} </strong></p>
                                <p><strong>Store ID : {{ $purchase_distribution_requests->store_id }} </strong></p>
                                <p><strong>Store Type : {{ $purchase_distribution_requests->store_type }}</strong></p>
                                <p><strong>Status: Hub Processing</strong></p>
                            </div>
                            <div class="col-md-4">
                                <p><strong>Request Date : {{ $purchase_distribution_requests->requisition_date }} </strong></p>
                                <p><strong>Request Delivery Date : {{ $purchase_distribution_requests->delivery_date }} </strong></p>
                                <p><strong>Priority: {{ $purchase_distribution_requests->priority }} </strong></p>
                            </div>
                        </div><!-- end.row -->

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table_field table-sm mb-0" id="table_field">
                                    <thead>
                                    <tr class="bg-white">
                                        <th>Product Name</th>
                                        <th>Generic Name</th>
                                        <th>Unit</th>
                                        <th>Available Vendor</th>
                                        <th>Purchase Quantity</th>
                                        <th>UOM</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($purchase_distribution_request_details as $purchase_distribution_request_detail)
                                            <tr>
                                                <td><input type="text" class="form-control"  value="{{ $purchase_distribution_request_detail->product->product_name }}"readonly></td>
                                                <td><input class="form-control" type="text" value="{{ $purchase_distribution_request_detail->generic->name }}" readonly></td>
                                                <td><input type="text" class="form-control" value="{{ $purchase_distribution_request_detail->unit->unit_name }}" readonly></td>
                                                <td><input class="form-control" type="text"  value="{{ $purchase_distribution_request_detail->vendor->name }}" readonly></td>
                                                <td><input class="form-control" type="number"  value="{{ $purchase_distribution_request_detail->request_quantity }}" readonly></td>
                                                <td><input class="form-control" type="text"  value="{{ $purchase_distribution_request_detail->product_uom->uom_name }}" readonly></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </div><!-- end#printDiv -->
                    <hr>

                    <div class="row text-right">
                        <div class="col-md-12 ">
                            <button type="button" class="btn index-btn" id="print">Print</button>
                        </div>
                    </div><!-- end.row -->
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
@endsection


@push('post_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js" integrity="sha512-d5Jr3NflEZmFDdFHZtxeJtBzk0eB+kkRXWFQqEc1EKmolXjHm2IKCA7kTvXBNjIYzjXfD5XzIjaaErpkZHCkBg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('#print').click( () => {
            $('#printDiv').printThis({
                pageTitle: 'Purchase Distribution Details',              // add title to print page
            })
        })
    </script>
@endpush

