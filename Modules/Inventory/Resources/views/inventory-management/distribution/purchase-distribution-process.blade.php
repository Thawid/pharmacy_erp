@extends('dboard.index')

@section('title','Requisition Details | HQ')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <h2 class="title-heading">Purchase Distribution Details</h2>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a class="btn index-btn" href={{route('purchase.distribution')}}> Purchase Distribution
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">


                        <div class="col-md-6 text-left">
                            <p><strong>Distribution ID: 112</strong></p>
                            <p><strong>Store : Banani Store </strong></p>
                            <p><strong>Store ID : S1120 </strong></p>
                            <p><strong>Store Type : Regular store</strong></p>
                            <p><strong>Status: Hub Processing</strong></p>
                        </div>
                        <div class="col-md-4 text-md-left">
                            <p><strong>Request Date : 29 Apr 2021 </strong></p>
                            <p><strong>Request Delivery Date : 05 May 2021 </strong></p>
                            <p><strong>Priority: High </strong></p>

                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

    <form  method="POST" action="" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="form-group col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <div class="row">
                            <table class="table table-striped table-bordered table_field table-sm mt-2" id="table_field">
                                <thead>
                                <tr class="bg-white">
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    <th>Unit</th>
                                    <th>Available Vendor</th>
                                    <th>Req Quantity</th>
                                    <th>Distribution Qty</th>
                                    <th>UOM</th>

                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td><input type="text" class="form-control"  value="Napa" readonly></td>
                                    <td><input class="form-control" type="text" value="Paracetamol" readonly></td>
                                    <td><input type="text" class="form-control" value="mg" readonly></td>
                                    <td><input class="form-control" type="text"  value="Baximco" readonly></td>
                                    <td><input class="form-control" type="number"  value="10" readonly></td>
                                    <td><input class="form-control" type="number"  value="50" readonly></td>
                                    <td><input class="form-control" type="text"  value="Box" readonly></td>

                                </tr>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 ">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary float-lg-right ">Submit</button>
                </div>
                 <div class="col-sm-0">
                     <button type="submit" class="btn btn-primary  float-lg-right mr-3 ">Print</button>
                 </div>

            </div>
        </div>
    </form>
@endsection


@push('post_scripts')

@endpush

