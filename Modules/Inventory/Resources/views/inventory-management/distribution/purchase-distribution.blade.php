@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')
    <style>
    </style>
@endpush
@section('dboard_content')

    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-9 text-left">
                    <h2 class="title-heading">Purchase Request List</h2>
                </div>
            </div><!-- end.row -->
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <table class="table table-bordered table-striped table-hover requisition-list-hq table-responsive-xl table-responsive-lg" id="requisitionListHq">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Requisition Info</th>
                                <th>Store Info</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Distribution Status</th>
                                <th width="5%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase_distributes as $k => $purchase_distribute)
                                    <tr>
                                        <td>{{ $k+1 }}</td>
                                        <td>
                                            <p>
                                                <strong>ID:{{ $purchase_distribute->store_requisition_no }}</strong><br>
                                                <strong>Created Date: {{ $purchase_distribute->requisition_date }}</strong><br>
                                                <strong>Request Delivery Date Date: {{ $purchase_distribute->delivery_date }}</strong>
                                            </p>
                                        </td>
                                        <td><strong>{{ $purchase_distribute->store->name }}</strong></td>
                                        <td><strong>{{ $purchase_distribute->priority }}</strong></td>
                                        <td>Distribution stage</td>
                                        <td>Partial</td>

                                        <td>
                                            <div class="d-flex justify-content-center align-items-center">
                                                <a href="{{route('purchase.distribution.show', $purchase_distribute->id )}}" class="btn details-btn text-center" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> <!-- end .col-md-12 -->
            </div> <!-- end .row -->
        </div> <!-- end .tile-body -->
    </div><!-- end .tile -->

@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
        $(document).ready( function () {
            $('#requisitionListHq').DataTable();
        } );
    </script>

@endpush



