
@extends('dboard.index')

@section('title','Create Warehouse Type Form')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Create Warehouse Type</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('warehouse-type.index')}}">Back</a>
                        </div>
                    </div><!-- end .row -->
                    <hr>

                    <form method="POST" action="{{ route('warehouse-type.store') }}" >
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Warehouse Type *</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" placeholder="Enter warehouse type name" value="{{ old('name') }}" required>
                                </div>
                            </div>
                
                            <div class="col-md-6">
                                <div class="form-group">
                                    @include('inventory::shares.status')
                                </div>
                            </div>
                        </div>
                        <hr>
                
                        <div class="row text-right">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Submit</button>
                            </div>
                        </div><!-- end .row -->
                    </form>
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->


@endsection
