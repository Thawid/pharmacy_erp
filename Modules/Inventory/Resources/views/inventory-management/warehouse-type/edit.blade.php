@extends('dboard.index')

@section('title','Edit Warehouse Type ')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Update Warehouse Type</h2>
                        </div>

                        <div class="col-md-6 text-right">
                            <a class="btn index-btn" href="{{route('warehouse-type.index')}}">Back</a>
                        </div>
                    </div><!-- end .row -->
                    <hr>

                    <!-- form -->
                    <form  action="{{ route('warehouse-type.update',$warehouseType->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row justify-content-center text-center">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Warehouse Type *</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" @if(isset($warehouseType))  value="{{ $warehouseType->name }} @endif" required>
            
                                </div>
                            </div>
                        </div><!-- end .row -->
                        <hr>

                        <div class="row text-center">
                            <div class="col-md-12">
                                <button class="btn create-btn" type="submit">Update</button>
                            </div>
                        </div><!-- end .row -->
                    </form>
                </div><!-- end .title-body -->
            </div><!-- end .tile -->
        </div><!-- col-md-12 -->
    </div><!-- end .row -->

@endsection






