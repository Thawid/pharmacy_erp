@extends('dboard.index')
@section('title', 'Create Stock Entry')
@push('styles')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<style>
  .btn .icon,
  .btn .fa {
    font-size: 14px;
    margin-right: 0px;
    vertical-align: middle;
    margin-top: -4px;
  }
</style>
@endpush
@section('dboard_content')
<!-- title-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-9 text-left">
            <h2 class="title-heading">Independent Stock Entry</h2>
          </div><!-- end .col-md-9 -->

          <div class="col-md-3 text-right">
            <a class="btn index-btn" href=" {{ url('inventory/hub-stocks/list') }}">Back</a>
          </div><!-- end .col-md-3 -->
        </div><!-- end .row -->
        <hr>

        <!-- form -->
        <!-- data-area -->
        <form method="POST" action="{{ route('store.independent.stock') }}">
          @csrf
          <div class="row mt-4">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped overflow-hidden table-bordered table-hover bg-white table-md">
                  <thead>
                    <tr>
                      <td>Product Name</td>
                      <td>Unit</td>
                      <td>UOM</td>
                      <td>Stock Qty</td>
                      <td>Batch</td>
                      <td>Inventory Location</td>
                      <td>Expiry Date</td>
                      <td width="2%">Action</td>
                    </tr>
                  </thead>
                  <tbody id="independent-stock-entry">
                    <tr>
                      <td>
                        <select class="form-control product select2" name="product_id[]" id="product-search">
                          <option value="0">Select Product</option>
                          @if(!empty($store_product))
                          @foreach($store_product as $product)
                          <option value="{{ $product->id }}"> {{ $product->product_name }}</option>
                          @endforeach
                          @endif
                        </select>
                      </td>
                      <td>
                        <select name="unit_id[]" id="" class="form-control ">
                          <option value="0">Select Unit</option>
                          @if(!empty($units))
                          @foreach($units as $unit)
                          <option value="{{ $unit->id }}"> {{ $unit->unit_name }}</option>
                          @endforeach
                          @endif
                        </select>
                      </td>
                      <td>
                        <select name="uom_id[]" id="" class="form-control ">
                          <option value="0">Select UOM</option>
                          @if(!empty($uoms))
                          @foreach($uoms as $uom)
                          <option value="{{ $uom->id }}"> {{ $uom->uom_name }}</option>
                          @endforeach
                          @endif
                        </select>
                      </td>
                      <td>
                        <input class="form-control" type="text" name="stock_quantity[]">
                      </td>
                      <td> <input class="form-control" type="text" name="batch_no[]" value=""> </td>
                      <td>
                        <select class="form-control" name="inventory_location_id[]" id="" class="form-control">
                          <option value="">Select Location</option>
                          @if(!empty($inventory_location))
                          @foreach($inventory_location as $location)
                          <option value="{{ $location->id }}"> {{$location->state}}</option>
                          @endforeach
                          @endif
                        </select>
                      </td>
                      <td><input type="date" class="form-control" name="expiry_date[]"></td>
                      <td>
                        <a href="javascript:void(0);" class="btn create-btn" type="button" onclick="addNewRow()"><i class="fa fa-plus"></i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div><!-- end .row -->
          <hr>
          <div class="row">
            <div class="col-md-12 text-right">
              <button type="submit" class="btn create-btn">Submit</button>
            </div>
          </div>
        </form>
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->

@endsection
@push('post_scripts')
<script>
  $('#select-all').click(function(event) {
    if (this.checked) {
      // Iterate each checkbox
      $('#tcheckbox:checkbox').each(function() {
        this.checked = true;
      });
    } else {
      $('#tcheckbox:checkbox').each(function() {
        this.checked = false;
      });
    }
  });
</script>

<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $(document).ready(function() {
    $('.select2').select2({
      width: '100%'
    });
  });
</script>

<script>
  function addNewRow() {
    $("#independent-stock-entry").append(
      `<tr>
        <td>
          <select class="form-control product select2" name="product_id[]">
            <option value="0">Select Product</option>
            @if(!empty($store_product))
            @foreach($store_product as $product)
            <option value="{{ $product->id }}"> {{ $product->product_name }}</option>
            @endforeach
            @endif
          </select>
        </td>
        <td>
          <select name="unit_id[]" id="" class="form-control ">
            <option value="0">Select Unit</option>
            @if(!empty($units))
            @foreach($units as $unit)
            <option value="{{ $unit->id }}"> {{ $unit->unit_name }}</option>
            @endforeach
            @endif
          </select>
        </td>
        <td>
          <select name="uom_id[]" id="" class="form-control ">
            <option value="0">Select UOM</option>
            @if(!empty($uoms))
            @foreach($uoms as $uom)
            <option value="{{ $uom->id }}"> {{ $uom->uom_name }}</option>
            @endforeach
            @endif
          </select>
        </td>
        <td>
          <input class="form-control" type="text" name="stock_quantity[]">
        </td>
        <td>
          <input class="form-control" type="text" name="batch_no[]" value="">
        </td>
        <td>
          <select class="form-control" name="inventory_location_id[]" id="" class="form-control">
            <option value="">Select Location</option>
            @if(!empty($inventory_location))
            @foreach($inventory_location as $location)
            <option value="{{ $location->id }}"> {{$location->state}}</option>
            @endforeach
            @endif
          </select>
        </td>
        <td><input type="date" class="form-control" name="expiry_date[]"></td>
        <td><a href="javascript:void(0);" class="btn btn-outline-danger remove-row" type="button"><i class="fa fa-close"></i></a></td>
      </tr>`
    );
    
    $('.select2').select2({
      width: '100%'
    });
  }

  $(document).on('click', '.remove-row', function() {
    $(this).parent().parent().remove();
  });
</script>

@endpush