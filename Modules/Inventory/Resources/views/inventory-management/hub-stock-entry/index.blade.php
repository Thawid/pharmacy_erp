@extends('dboard.index')

@section('title','Ready For Stock Entry')

@push('styles')
<style>
</style>
@endpush

@section('dboard_content')

<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-9 text-left">
            <h2 class="title-heading">Ready For Stock Entry</h2>
          </div><!-- end .col-md-9 -->
        </div><!-- end .row -->
        <hr>

        <div class="row mt-4">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="requisitionList">
                <thead>
                  <tr>
                    <th>SL</th>
                    <th>GRN Details</th>
                    <th>Attached Files</th>
                    <th>Entry Status</th>
                    @if(auth()->user()->role == 'warehouse_user')
                    <th>Action</th>
                    @endif
                  </tr>
                </thead><!-- end thead -->

                <tbody>
                  @foreach($grn_processes as $key=>$grn_process)
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>
                      <div class="">
                        <p><strong>Purchase Order
                            No: {{ $grn_process->purchase_order_no }}</strong></p>
                        <p><strong>Unload Date: </strong>{{ $grn_process->delivery_date }}
                        </p>
                        <p><strong>Lot / Branch: </strong>{{ $grn_process->lot_no }}</p>
                      </div>
                    </td>

                    <td><img src="{{ asset('uploads/grn-doc/'.$grn_process->image) }}" height="50" alt=""></td>
                    <td>
                      <p class="badge-info p-2 rounded d-inline">{{ $grn_process->requisition_type =='Approved' ? 'Entered' : 'Pending' }}</p>
                    </td>
                    @if(auth()->user()->role == 'warehouse_user')
                    <td>
                      <div class="d-md-flex justify-content-center align-items-center">
                        @if($grn_process->requisition_type == 'Approved')
                        <button href="javascript:void(0);" class="btn edit-btn" disabled="">
                          <i class="fa fa-plus-circle"></i></button>
                        @else
                        <a href="{{ route('hubStockEntry.create', $grn_process->id) }}" class="btn edit-btn"><i class="fa fa-plus-circle"></i></a>
                        @endif
                      </div>
                    </td>
                    @endif
                  </tr>
                  @endforeach
                </tbody>
              </table><!-- table -->
            </div><!-- end .table-responsive -->
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->

<!-- end data-area -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionList').DataTable();
  });
</script>

@endpush