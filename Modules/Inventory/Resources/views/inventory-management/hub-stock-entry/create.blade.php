@extends('dboard.index')
@section('title', 'Create Stock Entry')
@push('styles')
<style>
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
    font-weight: bolder;
  }
</style>
@endpush
@section('dboard_content')
<!-- title-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-9 text-left">
            <h2 class="title-heading">Stock Entry</h2>
          </div><!-- end .col-md-9 -->

          <div class="col-md-3 text-right">
            <a class="btn index-btn" href=" {{ url('inventory/hub-stocks/list') }}">Back</a>
          </div><!-- end .col-md-3 -->
        </div><!-- end .row -->
        <hr>

        <!-- form -->
        <!-- data-area -->
        <form method="POST" action="{{ route('hubStockEntry.store') }}">
          @csrf
          <div class="row">
            <div class="col-md-12">
              <h5>Purchase Order No # {{ $grn_process->purchase_order_no }}</h5>
              <div class="row mt-2">
                <!-- lot /batch -->
                <div class="col-xl-4 col-lg-3">
                  <div class="form-inline">
                    <label for="">Lot / Batch Number: </label>
                    <input type="hidden" name="inventory_grn_id" value="{{$grn_process->id}}">
                    <input type="hidden" name="hub_id" value="{{$grn_process->hub_id}}">
                    <input type="text" class="ml-xl-2 border-0 rounded form-control" name="lot_no" value="{{ $grn_process->lot_no }}" readonly>
                  </div>
                </div>

                <!-- stock id -->
                <div class="col-xl-4 col-lg-3">
                  <div class="form-inline">
                    <label for="">Stock ID: </label>
                    <input type="text" class="ml-xl-2 border-0 rounded form-control" name="stock_id" value="{{ $stock_id }}" readonly>
                  </div>
                </div>

                <!-- status -->
                <div class="col-xl-4 col-lg-3">
                  <div class="form-inline">
                    <label for="">Status: </label>
                    <input type="text" class="ml-xl-2 border-0 rounded form-control" name="status" value="{{ $grn_process->status }}" readonly>
                  </div>
                </div>
              </div><!-- end.row -->
            </div><!-- end .col-md-12 -->
          </div><!-- end .row -->

          <div class="row mt-2">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-responsive-md">
                  <thead>
                    <tr>
                      <th>#SL</th>
                      <th>SKU</th>
                      <th>Product Name</th>
                      <th>Unit</th>
                      <th>Received Qty</th>
                      <th>Stock Qty</th>
                      <th>Batch</th>
                      <th>Inventory Location</th>
                      <th>Expiry Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($grn_process_details as $key => $details)
                    @php
                    $product_name = Modules\Product\Entities\Product::where('id', $details->product_id)->value('product_name');
                    $unit_name = Modules\Product\Entities\ProductUnit::where('id', $details->unit_id)->value('unit_name');
                    $sku = "apl".rand(1,999900).date('s');
                    @endphp
                    <tr>

                      <td>{{ $key + 1 }}</td>
                      <td><input type="text" class="text-muted rounded border-light" name="sku[]" value="{{ $sku }}" readonly></td>
                      <td>
                        {{ $product_name }}
                        <input type="hidden" name="product_id[]" value="{{ $details->product_id }}" readonly>
                        <input type="hidden" name="all_product_id[]" value="{{ $details->product_id }}">
                      </td>
                      <td>{{ $unit_name }}<input type="hidden" name="unit_id[]" value="{{ $details->unit_id }}" readonly>
                        <input type="hidden" name="uom_id[]" value="{{ $details->uom_id }}" readonly>
                      </td>
                      <td>{{ $details->received_quantity }}<input type="hidden" name="received_quantity[]" value="{{ $details->received_quantity }}" readonly></td>
                      <td>{{ $details->received_quantity }}<input type="hidden" name="stock_quantity[]" value="{{ $details->received_quantity }}" readonly></td>
                      <td>
                        <input type="hidden" name="batch_no[]" value="{{ $details->batch_no }}">
                        {{ $details->batch_no }}
                      </td>
                      <td>
                        {{-- <label for=""> Select Inventory Location</label>--}}
                        <select name="inventory_location_id[]" id="" class="form-control">
                          <option value="" selected disabled>Select Location</option>
                          @foreach($inventory_locations as $inventory_location)
                          <option value="{{ $inventory_location->id }}">
                            {{$inventory_location->state}}
                          </option>
                          @endforeach
                        </select>
                        <input type="hidden" readonly name="warehouse_id" value="{{ $inventory_location->warehouse_id }}" />
                      </td>
                      <td><input type="date" class="form-control" name="expiry_date[]" value=""></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div><!-- end.col-md-12 -->
          </div><!-- end .row -->
          <hr>
          <div class="row">
            <div class="col-md-6">

              <div class="check-group">
                <input id="entry_all" name="ysnWithoutShelf" type="checkbox" value="1">
                <label for="entry_all" class="ml-2">Entry All without Location & Shelf</label>
              </div>

              <div class="check-group">
                <input type="checkbox" id="special_product" name="ysnSpecialProduct" value="1">
                <label for="special_product" class="ml-2">Special Product</label>
              </div>

              <div class="check-group">
                <input type="checkbox" name="ysnGenerateBarcode" id="generate_barcode" value="1">
                <label for="generate_barcode" class="ml-2">Generate Barcode</label>
              </div>
            </div><!-- end .col-md-6 -->

            <div class="col-md-6 text-right">
              {{--<a href="#" class="btn index-btn">Draft</a>--}}
              <button type="submit" class="btn index-btn">Submit</button>
            </div>
          </div><!-- end .row -->
        </form>
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->

@endsection
@push('post_scripts')
<script>
  $('#select-all').click(function(event) {
    if (this.checked) {
      // Iterate each checkbox
      $('#tcheckbox:checkbox').each(function() {
        this.checked = true;
      });
    } else {
      $('#tcheckbox:checkbox').each(function() {
        this.checked = false;
      });
    }
  });
</script>
@endpush