@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')

@endpush
@section('dboard_content')


<div class="tile">
  <div class="tile-body">
    <div class="row align-items-center">
      <div class="col-md-9 text-left">
        <h2 class="title-heading">Hold Distribution</h2>
      </div>
      <div class="col-md text-md-right">
      </div>
    </div><!-- end.row -->
    <hr>

    <!-- table -->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
            <thead>
              <tr>
                <th>SL</th>
                <th>Requisition Info</th>
                <th>Store Info</th>
                <th>Priority</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($hold_distributes as $k => $hold_distribute)
              <tr>
                <td>{{ $k+1 }}</td>
                <td>
                  <p>
                    <strong>ID:{{ $hold_distribute->store_requisition_no }}</strong><br>
                    <strong>Created Date: {{ $hold_distribute->requisition_date }}</strong><br>
                    <strong>Request Delivery Date Date: {{ $hold_distribute->delivery_date }}</strong>
                  </p>
                </td>
                <td><strong>{{ $hold_distribute->store->name }}</strong></td>
                <td><strong>{{ $hold_distribute->priority }}</strong></td>
                <td>
                  <div class="d-md-flex justify-content-around">
                    <a href="{{ route('hold.distribution.details', $hold_distribute->id )}}" class="btn details-btn" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                    <a href="{{ route('hold.distribution.process', $hold_distribute->id )}}" class="btn edit-btn" type="button"><i class="fa fa-spinner " aria-hidden="true" data-toggle="tooltip" title="process"> </i></a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!-- end.table-responsive -->
      </div><!-- end .col-md-12 -->
    </div><!-- end .row -->
  </div> <!-- end .tile-body -->
</div><!-- end .tile -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionList').DataTable();
  });

  $(document).ready(function() {
    $('#requisitionListHq').DataTable();
  });
</script>

@endpush