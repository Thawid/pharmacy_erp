@extends('dboard.index')

@section('title','Requisition Details | HQ')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-9">
                            <h2 class="title-heading">Hold Distribution Details</h2>
                        </div><!-- end.col-md-9 -->
                        <div class="col-md-3 text-md-right">
                            <a class="btn index-btn" href={{route('hold.distribution')}}>Hold Distribution</a>
                        </div><!-- end.col-md-3 -->
                    </div><!-- end.row -->
                    <hr>

                    <!-- info area -->
                    <div class="row mb-4">
                        <div class="col-md-6 text-left">
                            <p><strong>Distribution ID: {{ $hold_distribution_requests->id }}</strong></p>
                            <p><strong>Store : {{ $hold_distribution_requests->store->name }} </strong></p>
                            <p><strong>Store ID : {{ $hold_distribution_requests->store_id }} </strong></p>
                            <p><strong>Store Type : {{ $hold_distribution_requests->store_type }}</strong></p>
                            <p><strong>Status: Hub Processing</strong></p>
                        </div>
                        <div class="col-md-4 text-md-left">
                            <p><strong>Request Date : {{ $hold_distribution_requests->requisition_date }} </strong></p>
                            <p><strong>Request Delivery Date : {{ $hold_distribution_requests->delivery_date }} </strong></p>
                            <p><strong>Priority: {{ $hold_distribution_requests->priority }} </strong></p>
                        </div>
                    </div><!-- end.row -->

                    <!-- form -->
                    <form  method="POST" action="" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                    <thead>
                                    <tr class="bg-white">
                                        <th>Product Name</th>
                                        <th>Generic Name</th>
                                        <th>Unit</th>
                                        <th>Available Vendor</th>
                                        <th>Hold Quantity</th>
                                        <th>Available Quantity</th>
                                        <th>Distributed Quantity</th>
                                        <th>UOM</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($hold_distribution_request_details as $hold_distribution_request_detail)
                                        @for($i=0; $i<count($available_products->hubAvailableProductDetails); $i++)
                                            @if( $hold_distribution_request_detail->product_id == $available_products->hubAvailableProductDetails[$i]->product_id)
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control"  value="{{ $hold_distribution_request_detail->product->product_name }}"readonly>
                                                    </td>
                                                    <td>
                                                        <input class="form-control" type="text" value="{{ $hold_distribution_request_detail->generic->name }}" readonly>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="{{ $hold_distribution_request_detail->unit->unit_name }}" readonly>
                                                    </td>
                                                    <td>
                                                        <input class="form-control" type="text"  value="{{ $hold_distribution_request_detail->vendor->name }}" readonly>
                                                    </td>
                                                    <td><input class="form-control" type="number" name="request_quantity[]"  value="{{ $hold_distribution_request_detail->request_quantity }}" readonly></td>
                                                    <td><input class="form-control" type="number" name="available_quantity[]"  value="{{ $available_products->hubAvailableProductDetails[$i]->available_quantity ?? 0  }}" readonly></td>
                                                    <td><input class="form-control" type="number" name="distribution_quantity[]"  value="{{ $hold_distribution_request_detail->hold_quantity }}" readonly></td>
                                                    <td>
                                                        <input class="form-control" type="text"  value="{{ $hold_distribution_request_detail->product_uom->uom_name }}" readonly>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endfor
                                    @endforeach
                                    </tbody>

                                </table>
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </form>
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->


@endsection


@push('post_scripts')

@endpush

