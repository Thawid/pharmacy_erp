@extends('dboard.index')
@section('title', 'Create Stock Entry')
@push('styles')
    <style>

    </style>
@endpush
@section('dboard_content')
    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading">Stock Entry</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn index-btn" href=" {{ url('inventory/hub-stocks/list') }}">Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>

                    <!-- form -->
                    <!-- data-area -->
                    <form method="POST" action="{{ route('storeStockEntry.store') }}">
                        @csrf
                       
                        <div class="row">
                            <div class="col-md-12">

                                <div class="d-md-flex align-items-center form-inline mt-2">
                                    <!-- lot /batch -->
                                    <label for="">Lot / Batch Number: </label>
                                    <input type="hidden" name="store_grn_id" value="{{ $grn_process->id }}">
                                    <input type="hidden" name="store_id" value="{{ $grn_process->store_id }}">
                                    <input type="hidden" name="distribution_id" value="{{ $grn_process->distribution_id }}">
                                    <input type="hidden" name="requisition_no" value="{{ $grn_process->requisition_no }}">
                                    

                                    <!-- stock id -->
                                    <label for="">Stock ID: </label>
                                    <input type="text" class="ml-2 mr-3 border-0 rounded form-control" name="stock_id"
                                        value="" readonly>

                                    <!-- status -->
                                    <label for="">Status: </label>
                                    <input type="text" class="ml-2 mr-3 border-0 rounded form-control" name="s_status"
                                    value="{{$grn_process->status}}" readonly>
                                    
                                </div><!-- d-md-flex -->
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table
                                        class="table table-striped overflow-hidden table-bordered table-hover bg-white table-md">
                                        <thead>
                                            <tr>
                                                <td width="5%">#SL</td>
                                                <td width="15%">Product Name</td>
                                                <td width="15%">Generic Name</td>
                                                <td width="15%">Vendor Name</td>
                                                <td width="5%">Unit</td>
                                                <td width="10%">Request Qty</td>
                                                <td width="10%">Distribution Qty</td>
                                                <td width="15%">UOM</td>
                                                <td width="10%">Batch No</td>
                                                <td width="15%">status</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($grn_process_details as $key => $details)
                                                @php
                                                    $product_name = Modules\Product\Entities\Product::where('id', $details->product_id)->value('product_name');
                                                    $generic_name = Modules\Product\Entities\ProductGeneric::where('id', $details->generic_id)->value('name');
                                                    $vendor_name = Modules\Vendor\Entities\Vendor::where('id', $details->vendor_id)->value('name');
                                                    $unit_name = Modules\Product\Entities\ProductUnit::where('id', $details->unit_id)->value('unit_name');
                                                    $uom = Modules\Product\Entities\UOM::where('id', $details->uom_id)->value('uom_name');
                                                @endphp
                                                <tr>

                                                    <td>{{ $key + 1 }}</td>
                                                    {{-- <td>
                                                        <input type="text" name="sku[]" value=""
                                                            readonly>
                                                    </td> --}}
                                                    <td>
                                                        {{ $product_name }}
                                                        <input type="hidden" name="product_id[]"
                                                            value="{{ $details->product_id }}" readonly>
                                                    </td>
                                                    <td>
                                                        {{ $generic_name }}
                                                        <input type="hidden" name="generic_id[]"
                                                            value="{{ $details->generic_id }}" readonly>
                                                    </td>

                                                    <td>
                                                        {{$vendor_name}}
                                                    </td>
                                                    <td>
                                                        {{ $unit_name }}<input type="hidden" name="unit_id[]"
                                                            value="{{ $details->unit_id }}" readonly>
                                                    </td>
                                                    <td>
                                                        {{ $details->request_quantity }}<input type="hidden"
                                                            name="request_quantity[]"
                                                            value="{{ $details->request_quantity }}" readonly>
                                                    </td>
                                                    <td>
                                                        {{ $details->request_quantity }}
                                                        <input type="hidden"
                                                            name="stock_quantity[]"
                                                            value="{{ $details->request_quantity }}" readonly>
                                                            <input type="hidden"
                                                            name="warehouse_id[]"
                                                            value="{{ $details->warehouse_id }}" readonly>
                                                            <input type="hidden" name="expiry_date[]"
                                                        value="{{ $details->expiry_date }}" readonly>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="uom_id[]"
                                                        value="{{ $details->uom_id }}" readonly>
                                                        {{ $uom }}
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="batch_no[]"
                                                            value="{{ $details->batch_no }}">
                                                        {{ $details->batch_no }}
                                                    </td>

                                                    <td>
                                                        {{ $details->status }}
                                                        <input type="hidden" name="status[]"
                                                            value="{{ $details->status }}">
                                                    </td>

                                                    {{-- <td>
                                                        <label for=""> Select Inventory Location</label>
                                                        <select name="inventory_location_id[]" id="" class="form-control">
                                                            <option value="">Select Location</option>
                                                            @foreach ($inventory_locations as $inventory_location)
                                                                <option value="{{ $inventory_location->id }}">
                                                                    {{ $inventory_location->state }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <input type="hidden" readonly name="warehouse_id"
                                                            value="{{ $inventory_location->warehouse_id }}" />
                                                    </td> --}}
                                                    {{-- <td>
                                                        <input type="date" class="form-control" name="expiry_date[]"
                                                            value="">
                                                    </td> --}}
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- end .row -->
                        <hr>
                        <div class="row">
                            <div class="col-md-6">

                                <div class="check-group">
                                    <input id="entry_all" name="ysnWithoutShelf" type="checkbox" value="1">
                                    <label for="entry_all" class="ml-2">Entry All without Location & Shelf</label>
                                </div>

                                <div class="check-group">
                                    <input type="checkbox" id="special_product" name="ysnSpecialProduct" value="1">
                                    <label for="special_product" class="ml-2">Special Product</label>
                                </div>

                                <div class="check-group">
                                    <input type="checkbox" name="ysnGenerateBarcode" id="generate_barcode" value="1">
                                    <label for="generate_barcode" class="ml-2">Generate Barcode</label>
                                </div>
                            </div><!-- end .col-md-6 -->

                            <div class="col-md-6 text-right">
                                {{-- <a href="#" class="btn index-btn">Draft</a> --}}
                                <button type="submit" class="btn index-btn">Submit</button>
                            </div>
                        </div><!-- end .row -->
                    </form>
                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection
@push('post_scripts')
    <script>
        $('#select-all').click(function(event) {
            if (this.checked) {
                // Iterate each checkbox
                $('#tcheckbox:checkbox').each(function() {
                    this.checked = true;
                });
            } else {
                $('#tcheckbox:checkbox').each(function() {
                    this.checked = false;
                });
            }
        });
    </script>
@endpush
