@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')
<style>
  nav>.nav-tabs>a.nav-item {
    font-size: 18px;
  }
</style>
@endpush
@section('dboard_content')

<div class="tile">
  <div class="tile-body">
    <div class="row align-items-end">
      <div class="col-md-8">
        <h2 class="title-heading mb-4">Available Stock Details</h2>
        <p><strong>Brand Name : {{ $product->product_name }}</strong></p>
        <p><strong>Product Name : {{ $product->brand_name }}</strong></p>
        <strong> Generic Name : {{ $product->generic_name }} </strong>
      </div>
      <div class="col-md-4 text-right">
        <a href="{{ url('inventory/available-stock') }}" class="btn index-btn">Back</a>
      </div>
    </div><!-- end.row -->
    <hr>

    <div class="row">
      <div class="col-md-12 ">
        <div class="table-responsive bg-white">
          <table class="table table-bordered table-striped table-hover requisition-list-hq" id="requisitionListHq">
            <thead>
              <tr>
                <th>SL</th>
                <th>Lot Details</th>
                <th>Purchase Qty</th>
                <th>UOM</th>
                <th>Stock Alert</th>
                <th>Expiry Date</th>
              </tr>
            </thead>
            <tbody>
              @if(isset($data))

              @foreach($data as $row)
              <tr>
                <td> {{ $loop->iteration }}</td>
                <td>
                  <p>
                    <strong>Batch No : {{ $row->batch_no ?? null }}</strong><br>
                    <strong>SKU : {{ $row->sku ?? null }}</strong><br>
                    <strong>Purchase Order No : {{ $row->purchase_order_no ?? null }}</strong><br>
                  </p>
                </td>
                <td>
                  {{ $row->stock_quantity}}
                </td>
                <td><strong> {{ $row->uom_name }} </strong></td>
                <td>
                  <strong>
                    @php
                    $startDate = \Carbon\Carbon::today();

                    @endphp
                    @if(($startDate >= $row->expiry_date))
                    {{ 'Expired' }}
                    @else
                    {{ $startDate->diffInDays($row->expiry_date).' '.'Day Remain' }}
                    @endif
                  </strong>
                </td>
                <td>
                  <span> {{ date('d F Y', strtotime($row->expiry_date)) }}</span> <br>
                </td>

              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div><!-- end.table-responsive -->
      </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->
  </div><!-- end.tile-body -->
</div><!-- end.tile -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionListHq').DataTable();
  });
</script>

@endpush