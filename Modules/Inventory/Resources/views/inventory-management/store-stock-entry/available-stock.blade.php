@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')
<style>
  nav>.nav-tabs>a.nav-item {
    font-size: 18px;
  }
</style>
@endpush
@section('dboard_content')

<div class="tile">
  <div class="tile-body">
    <div class="row align-items-center">
      <div class="col-md-9 text-left">
        <h2 class="title-heading">Available Stock</h2>
      </div><!-- end.col-md-9 -->
    </div><!-- end.row -->
    <hr>

    <div class="row">
      <div class="col-md-12 ">
        <div class="row">
          <div class="col-md-12 table-responsive">
            <div class="table-responsive bg-white">
              <table class="table table-bordered table-striped table-hover requisition-list-hq" id="requisitionListHq">
                <thead>
                  <tr>
                    <th>SL</th>
                    <th>Product Details</th>
                    {{-- <th>Stock Location</th>--}}
                    <th>Available Stock</th>
                    <th>UOM</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                  @foreach($details as $key => $hubStockEntryDetail)
                  {{--{{ dd($hubStockEntryDetail) }}--}}
                  <tr>
                    <td>{{ $key + 1 }}</td>

                    <td>
                      <p>
                        <strong>{{ $hubStockEntryDetail->product_name }}</strong>
                      </p>
                    </td>
                    <td><strong> {{ $hubStockEntryDetail->stock_quantity }} </strong></td>
                    <td> {{ $hubStockEntryDetail->uom_name }} </td>

                    <td>
                      <a href="{{ route('hubStockEntry.availablestock.details', $hubStockEntryDetail->product_id) }}" class="btn details-btn" type="button">
                        <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"></i>
                      </a>

                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div> <!-- end .col-md-12 -->
        </div> <!-- end .row -->
      </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->
  </div><!-- end.tile-body -->
</div><!-- ene.tile -->



@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionListHq').DataTable();
  });
</script>

@endpush