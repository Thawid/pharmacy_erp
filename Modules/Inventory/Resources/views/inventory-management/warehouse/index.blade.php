@extends('dboard.index')

@section('title','Warehouse List')

@section('dboard_content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">

                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6 ">
                        <h2 class="title-heading">Warehouse</h2>
                    </div>
                    @can('hasCreatePermission')
                    <div class="col-md-6 text-right">
                        <a class="btn create-btn" href="{{route('warehouse.create')}}">Add Warehouse</a>
                    </div>
                    @endcan
                </div><!-- end .row -->
                <hr>

                <!-- data -->
                <div class="row" id="actionModal">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="WarehouseTable">
                                <thead class="thead">
                                    <tr>
                                        <th>SN</th>
                                        <th>Store Info</th>
                                        <th>Warehouse Type</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($warehouses as $warehouse)
                                    <tr>
                                        <td> {{ $warehouse->id }} </td>
                                        <td> {{ $warehouse->storeInfo[0]['name'] }} </td>
                                        <td> {{ $warehouse->warehouseType[0]['name'] }} </td>
                                        <td> {{ $warehouse->name }} </td>
                                        <td>
                                            @if($warehouse->status == 1)
                                            <span class="badge badge-success m-1 p-2">Active</span>
                                            @else
                                            <span class="badge badge-danger m-1 p-2">Inactive</span>
                                            @endif
                                        </td>

                                        <td>
                                            <div class="d-flex justify-content-around align-items-center">
                                                 @can('hasDeletePermission')
                                                    <form action="{{route('warehouse.destroy',$warehouse->id)}}" class="mr-3"
                                                        method="POST" onclick="return confirm('Are you sure?')">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn edit-btn" type="submit"><i class="fa fa-refresh"></i></button>
                                                    </form>
                                                @endcan
                                                @can('hasEditPermission')
                                                    <a class="btn edit-btn" href="{{route('warehouse.edit',$warehouse->id)}}">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- end .table-responsive -->
                    </div><!-- end .col-md-12 -->
                </div><!-- end .row -->
            </div><!-- end .tile-body -->
        </div><!-- end .title -->
    </div><!-- end .col-md-12 -->
</div><!-- end .row -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $('#WarehouseTable').DataTable();

</script>
@endpush
