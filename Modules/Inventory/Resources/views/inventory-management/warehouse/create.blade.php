@extends('dboard.index')

@section('title','Create Warehouse Form')

@section('dboard_content') 
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">

                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Create New Warehouse</h2>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('warehouse.index')}}">Back</a>
                    </div>
                </div><!-- end .row -->
                <hr>

                <!-- data -->
                <form method="POST" action="{{ route('warehouse.store') }}">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <!-- Store Name Start -->
                        <div class="form-group">
                            <label class="control-label">Hub Name <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="store_id" id="store_id">
                                <option value="" selected disabled>Select Hub Name</option>
                                @foreach($stores as $store)
                                <option value="{{ $store->id }}"> {{ $store->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- Store Name End -->

                        <!-- Warehouse Name Start -->
                        <div class="form-group">
                            <label class="control-label">Warehouse Name <span class="text-danger">*</span></label>
                            <input class="form-control @error('name') is-invalid @enderror" name="name" type="text"
                                placeholder="Enter warehouse name" value="{{ old('name') }}" required>
                        </div>
                        <!-- Warehouse Name End -->

                        <!-- Warehouse Phone Start -->
                        <div class="form-group">
                            <label class="control-label">Warehouse Phone </label>
                            <input class="form-control @error('phone') is-invalid @enderror" name="phone" type="number"
                                placeholder="Enter warehouse phone" value="{{ old('phone') }}">
                        </div>
                        <!-- Warehouse Phone End -->

                        <!-- Warehouse Address Start -->
                        <div class="form-group">
                            <label class="control-label">Warehouse Code <span class="text-danger">*</span></label>
                            <input class="form-control @error('code') is-invalid @enderror" name="code" type="text"
                                placeholder="Enter warehouse code" value="{{ old('code') }}" required>
                        </div>
                        <!-- Warehouse Address End -->
                    </div><!-- end .col-md-6 -->

                    <div class="col-md-6">
                        <!-- Warehouse Type Start -->
                        <div class="form-group">
                            <label class="control-label">Select Warehouse Type <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="warehouse_type_id" id="warehouse_type_id">
                                <option value="" selected disabled>Select Warehouse Type</option>
                                @foreach($warehouseTypes as $type)
                                <option value="{{ $type->id }}"> {{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- Warehouse Type End -->
                        
                        <!-- Warehouse Address Start -->
                        <div class="form-group">
                            <label class="control-label">Warehouse Address <span class="text-danger">*</span></label>
                            <input class="form-control @error('address') is-invalid @enderror" name="address" type="text"
                                placeholder="Enter warehouse address" value="{{ old('address') }}" required>
                        </div>
                        <!-- Warehouse Address End -->

                        <!-- Warehouse Email Start -->
                        <div class="form-group">
                            <label class="control-label">Warehouse Email</label>
                            <input class="form-control @error('email') is-invalid @enderror" name="email" type="email"
                            placeholder="Enter warehouse email" value="{{ old('email') }}">
                        </div>
                        <!-- Warehouse Email End -->

                        <div class="form-group">
                            @include('inventory::shares.status')
                        </div>
                    </div><!-- end .col-md-6 -->
                </div><!-- end .row -->
                <hr>

                <div class="row text-right">
                    <div class="col-md-12">
                        <button class="btn create-btn" type="submit">Submit</button>
                    </div>
                </div>
            </form>


            </div><!-- end .tile-body -->
        </div><!-- end .tile -->
    </div><!-- end .col-md-12 -->
</div><!-- end .row -->


@endsection

@push('post_scripts')
<script type="text/javascript">
    $('.select2').select2();

</script>
@endpush
