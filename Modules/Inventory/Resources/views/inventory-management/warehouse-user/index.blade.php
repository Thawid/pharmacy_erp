@extends('dboard.index')

@section('title','View Vendor User Interface')

@section('dboard_content')

    <div class="row" id="actionModal">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="title-heading">Warehouse User Details</h2>
                        </div>

                        <div class="col-md-6 text-right">
                            <a class="btn create-btn" href="{{route('warehouse-user.create')}}">Add New Warehouse User</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- data -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="StoreTypeTable">
                                    <thead class="thead">
                                    <tr>
                                        <th>SL</th>
                                        <th>Warehouse Name</th>
                                        <th>User  Name</th>
                                        <th>User  Role</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($all_warehouse_user))
                                        @foreach( $all_warehouse_user as $key => $warehouseUser)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{$warehouseUser->warehouse_name}}</td>
                                                <td>{{$warehouseUser->user_name}}</td>
                                                <td>{{$warehouseUser->user_role}}</td>
                                                <td>{{$warehouseUser->warehouse_user_status == 1 ? 'Active' : 'InActive' }}</td>
                                                <td>
                                                    <div class="d-md-flex justify-content-center align-items-center">
                                                        <form class="mr-3" id="delete_form{{$warehouseUser->id}}" method="POST" action="{{ route('warehouse-user.destroy',$warehouseUser->id) }}" onclick="return confirm('Are you sure?')">@csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn edit-btn" type="submit"><i class="fa fa-refresh"></i></button>
                                                    </form>
                                                  

                                                
                                                    <a class="btn edit-btn " href="{{route('warehouse-user.edit',$warehouseUser->id)}}">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    </div>
                                                    
                                               
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- end.table-responsive -->
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">$('#StoreTypeTable').DataTable();</script>
@endpush
