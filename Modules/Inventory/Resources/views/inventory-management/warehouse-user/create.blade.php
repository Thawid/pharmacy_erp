@extends('dboard.index')

@section('title', 'Create New Warehouse User Form')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Create New warehouse User</h2>
          </div>
          <div class="col-md-6 text-right">
            <a class="btn index-btn" href="{{ route('warehouse-user.index') }}">Back</a>
          </div>
        </div><!-- end.row -->
        <hr>

        <!-- data -->
        <form method="POST" action="{{ route('warehouse-user.store') }}">
          @csrf
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleSelect1">Warehouse</label>
                <select class="form-control " name="warehouse_id">
                  <option selected="true" disabled="disabled">Select Warehouse</option>
                  @foreach ($users_and_warehouses['warehouses'] as $warehouse )
                  <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                  @endforeach
                </select>
              </div><!-- end.form-group -->
            </div><!-- col-md-4 -->

            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleSelect1">User</label>
                <select class="form-control " name="user_id">
                  <option selected="true" disabled="disabled">Select User</option>
                  @foreach ($users_and_warehouses['users'] as $user )
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
                </select>
              </div>
            </div><!-- end.col-md-4 -->

            <div class="col-md-4">
              <div class="form-group">
                @include('store::shares.status')
              </div>
            </div><!-- end.col-md-4 -->
          </div><!-- end.row -->
          <hr>

          <div class="row text-right">
            <div class="col-md-12">
              <button class="btn create-btn" type="submit">Submit</button>
            </div>
          </div>
        </form>
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->

@endsection