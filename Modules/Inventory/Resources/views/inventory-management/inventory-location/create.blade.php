@extends('dboard.index')

@section('title','Create Inventory Location')

@section('dboard_content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">

                <!-- title -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="title-heading">Create Inventory Location</h2>
                    </div>

                    <div class="col-md-6 text-right">
                        <a class="btn index-btn" href="{{route('inventory-location.index')}}">Back</a>
                    </div>
                </div><!-- end .row -->
                <hr>

                <!-- data -->
                <form action="{{route('inventory-location.store')}}" method="POST">
                  @csrf
                    <div class="row">
                        <div class="col-md-6">
                            @if(isset($inventoryLocationRepository))
                            <div class="form-group" id="hub">
                                <label for="exampleSelect1">Hub</label>
                                <select class="form-control @error('hub_id') is-invalid @enderror" name="hub_id" onchange="getStore(this)">
                                    <option selected="true" disabled="disabled">Select Hub</option>
                                    @foreach($inventoryLocationRepository as $hub)
                                        <option value="{{$hub->id}}">{{$hub->name}}</option>
                                    @endforeach
                                </select>
                                @error('hub_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                {{--                @endif--}}

                {{--                @if(isset($warehouses))--}}
                            <div class="form-group" id="warehouse">
                                <label for="exampleSelect2">Warehouse</label>
                                <select class="form-control @error('warehouse_id') is-invalid @enderror" name="warehouse" id="selectedStore">
                                    <option selected="true" disabled="disabled">Select Warehouse</option>
                                </select>
                                @error('warehouse_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        @endif

                          <div class="form-group">
                                <label class="control-label">Inventory Type</label>
                                <input class="form-control @error('type') is-invalid @enderror" type="text" name="type" placeholder="Enter Inventory Type" value="{{ old('type') }}">
                          </div>

                          <div class="form-group">
                            <label class="control-label">Inventory State</label>
                            <input class="form-control @error('state') is-invalid @enderror" type="text" name="state" placeholder="Enter Inventory State" value="{{ old('state') }}">
                          </div>

                        <div class="form-group">
                              <label class="control-label">Floor No</label>
                              <input class="form-control @error('floor') is-invalid @enderror" type="text" name="floor" placeholder="Enter Floor No" value="{{ old('floor') }}">
                        </div>
                     </div>
                     
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Room No</label>
                                <input class="form-control @error('room') is-invalid @enderror" type="number" name="room" placeholder="Enter Room No" value="{{ old('room') }}">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Room Point</label>
                                <input class="form-control @error('room_point') is-invalid @enderror" type="text" name="room_point" placeholder="Enter Room Point" value="{{ old('room_point') }}">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Self No</label>
                                <input class="form-control @error('self') is-invalid @enderror" type="number" name="self" placeholder="Enter Self No" value="{{ old('self') }}">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Self Name</label>
                                <input class="form-control @error('self_name') is-invalid @enderror" type="text" name="self_name" placeholder="Enter Self Name" value="{{ old('self_name') }}">
                            </div>

                          <div class="form-group">
                              @include('inventory::shares.status')
                          </div>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>

                    <div class="row text-right">
                      <div class="col-md-12">
                          <button class="btn create-btn" type="submit">Submit</button>
                      </div>
                    </div><!-- end.row -->
                </form>
            </div><!-- end.tile-body -->
        </div><!-- end.tile -->
    </div><!-- end.col-md-12 -->
</div><!-- end.row -->

@endsection

@push('pre_scripts')

    <script>
        function getStore(store){
            var value = store.value;
            // alert(value);
            var selectStore = document.getElementById('selectedStore');
            selectStore.innerHTML = "";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('g_store') }}",
                method: 'get',
                data: {
                    store: value
                },
                success: function(result) {
                    console.log(result)
                    for (var i=0; i<result.data.length; i++){
                        opt = document.createElement('option');
                        opt.value = result.data[i].id;
                        opt.innerHTML = result.data[i].name;
                        selectStore.appendChild(opt);
                        /*html =  '<option value="'+result.data[i].id+'">'+result.data[i].name+'</option>'
                        selectType.add(html);*/
                    }
                }
            });
        }
    </script>
@endpush


