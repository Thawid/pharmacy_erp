@extends('dboard.index')

@section('title','Edit Warehouse Type ')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row align-items-center">
                        <div class="col-md-9">
                            <h2 class="title-heading">Update Inventory Location Type</h2>
                        </div>
                        <div class="col-md-3 text-right">
                            <a class="btn index-btn" href="{{route('inventory-location.index')}}">Back</a>
                        </div>
                    </div><!-- end.row -->
                    <hr>

                    <!-- data -->
                    <form  action="{{ route('inventory-location.update',$inventoryLocation->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-6">

                                @if(isset($stores))
                                    <div class="form-group" id="hub">
                                        <label for="exampleSelect1">Hub</label>
                                        <select class="form-control @error('hub_id') is-invalid @enderror" name="hub_id" onchange="getStore(this)">
                                            <option selected="true" disabled="disabled">Select Hub</option>
                                            @foreach($stores as $hub)
                                                <option value="{{$hub->id}}" {{$inventoryLocation->hub_id == $hub->id ? 'selected' : ''}}>{{$hub->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('hub_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    {{--                @endif--}}

                                    {{--                @if(isset($warehouses))--}}
                                    <div class="form-group" id="warehouse">
                                        <label for="exampleSelect2">Warehouse</label>
                                        <select class="form-control @error('warehouse_id') is-invalid @enderror" name="warehouse" id="selectedStore">
                                            <option value="{{$warehouse->id}}" {{$inventoryLocation->warehouse_id == $warehouse->id ? 'selected' : ''}}>{{$warehouse->name}}</option>
                                        </select>
                                        @error('warehouse_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @endif

                              <div class="form-group">
                                    <label class="control-label">Inventory Type</label>
                                    <input class="form-control @error('type') is-invalid @enderror" type="text" name="type" placeholder="Enter Inventory Type" value="{{ $inventoryLocation->type}}">
                              </div>

                              <div class="form-group">
                                <label class="control-label">Inventory State</label>
                                <input class="form-control @error('state') is-invalid @enderror" type="text" name="state" placeholder="Enter Inventory State" value="{{ $inventoryLocation->state }}">
                              </div>

                            <div class="form-group">
                                  <label class="control-label">Floor No</label>
                                  <input class="form-control @error('floor') is-invalid @enderror" type="text" name="floor" placeholder="Enter Floor No" value="{{ $inventoryLocation->floor_no}}">
                            </div>
                     </div><!-- end.col-md-6 -->



                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Room No</label>
                                    <input class="form-control @error('room') is-invalid @enderror" type="number" name="room" placeholder="Enter Room No" value="{{ $inventoryLocation->room_no }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Room Point</label>
                                    <input class="form-control @error('room_point') is-invalid @enderror" type="text" name="room_point" placeholder="Enter Room Point" value="{{ $inventoryLocation->room_point }}">
                               </div>

                               <div class="form-group">
                                <label class="control-label">Self No</label>
                                <input class="form-control @error('self') is-invalid @enderror" type="number" name="self" placeholder="Enter Self No" value="{{ $inventoryLocation->self_no }}">
                              </div>

                                <div class="form-group">
                                    <label class="control-label">Self Name</label>
                                    <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" placeholder="Enter Self Name" value="{{ $inventoryLocation->name }}">
                                </div>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>

                    <div class="row text-right">
                      <div class="col-md-12">
                          <button class="btn create-btn" type="submit">Update</button>
                      </div>
                    </div><!-- end.row -->
                </form>
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.ocl-md-12 -->
    </div><!-- end.row -->
@endsection


@push('pre_scripts')

    <script>
        function getStore(store){
            var value = store.value;
            // alert(value);
            var selectStore = document.getElementById('selectedStore');
            selectStore.innerHTML = "";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('g_store') }}",
                method: 'get',
                data: {
                    store: value
                },
                success: function(result) {
                    console.log(result)
                    for (var i=0; i<result.data.length; i++){
                        opt = document.createElement('option');
                        opt.value = result.data[i].id;
                        opt.innerHTML = result.data[i].name;
                        selectStore.appendChild(opt);
                        /*html =  '<option value="'+result.data[i].id+'">'+result.data[i].name+'</option>'
                        selectType.add(html);*/
                    }
                }
            });
        }
    </script>
@endpush





