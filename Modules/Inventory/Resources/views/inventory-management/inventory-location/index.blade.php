@extends('dboard.index')

@section('title','Inventory location list')

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">

        <!-- title -->
        <div class="row align-items-center">
          <div class="col-md-6 text-left">
            <h2 class="title-heading">Inventory Location</h2>
          </div>
          @can('hasCreatePermission')
          <div class="col-md-6 text-right">
            <a class="btn create-btn" href="{{route('inventory-location.create')}}">Add Inventory location</a>
          </div>
          @endcan
        </div><!-- end .row -->
        <hr>

        <!-- data -->
        <div class="row" id="actionModal">
          <div class="col-md-12">
            <table class="table table-hover table-bordered table-responsive-lg table-responsive-xl table-responsive-sm table-responsive-md" id="inventoryLocationTable">
              <thead>
                <tr>
                  <th>SL</th>
                  <th>HUB</th>
                  <th>Inventory Type</th>
                  <th>Inventory State</th>
                  <th>Floor No</th>
                  <th>Room No</th>
                  <th>Room point</th>
                  <th>Self No</th>
                  <th>Self Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {{--{{ dd($inventoryLocations) }}--}}
                @if(isset($inventoryLocations))
                @foreach($inventoryLocations as $key => $inventoryLocation)
                <tr>
                  <td>{{$key+=1 }}</td>
                  <td>{{$inventoryLocation->hub_name}}</td>
                  <td>{{$inventoryLocation->type}}</td>
                  <td>{{$inventoryLocation->state}}</td>
                  <td>{{$inventoryLocation->floor_no}}</td>
                  <td>{{$inventoryLocation->room_no}}</td>
                  <td>{{$inventoryLocation->room_point}}</td>
                  <td>{{$inventoryLocation->self_no}}</td>
                  <td>{{$inventoryLocation->invt_name}}</td>
                  <td>
                    @if( $inventoryLocation->status == 1 )
                    <span class="badge badge-success m-1 p-2">Active</span>
                    @else
                    <span class="badge badge-danger m-1 p-2">Inactive</span>
                    @endif
                  </td>

                  <td>
                    <div class="d-flex justify-content-around align-items-center">
                      @can('hasDeletePermission')
                      <form action="{{route('inventory-location.destroy',$inventoryLocation->id)}}" class="mr-3" method="POST" onclick="return confirm('Are you sure?')">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn edit-btn" type="submit" title="change status">
                          <i class="fa fa-refresh"></i></button>
                      </form>
                      @endcan
                      @can('hasEditPermission')
                      <a class="btn edit-btn" title="edit inventory location" href="{{route('inventory-location.edit',$inventoryLocation->id)}}"><i class="fa fa-pencil"></i></a>
                      @endcan
                    </div>

                  </td>
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
      </div><!-- end .tite-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->
@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $('#inventoryLocationTable').DataTable();
</script>
@endpush
