@extends('dboard.index')
@section('title', 'Product For Barcode')
@push('styles')
    <style>

    </style>
@endpush
@section('dboard_content')
    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading">Product For Barcode</h2>
                        </div><!-- end .col-md-9 -->

                        <div class="col-md-3 text-right">
                            <a class="btn index-btn" href=" {{ url('inventory/store-barcode') }}">Back</a>
                        </div><!-- end .col-md-3 -->
                    </div><!-- end .row -->
                    <hr>
                    <form method="POST" action="{{ route('store-barcode.generate', ['entry_id' => !empty($product) ? $product->store_stock_entry_id : 0, 'product_id' => !empty($product) ? $product->id : 0]) }}">
                        @csrf
                        <input name="product_id" hidden type="text" value="{{ !empty($product) ? $product->id : 0 }}">
                        <input name="store_stock_entry_id" hidden type="text" value="{{ !empty($product) ? $product->store_stock_entry_id : 0 }}">
                        <input name="batch_no" hidden type="text" value="{{ !empty($product) ? $product->batch_no : 0 }}">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="control-label">Please Input Quantity for Barcode <span class="text-danger">*</span></label>
                                <input class="form-control" name="barcode_qty" type="text" placeholder="Enter quantity for barcode" value="{{ old('code') }}" required>
                            </div><!-- end .col-md-12 -->
                        </div><!-- end .row -->

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped overflow-hidden table-bordered table-hover bg-white table-md">
                                        <thead>
                                        <tr>
                                            <td>Product Name</td>
                                            <td>SKU</td>
                                            <td>Unit</td>
                                            <td>Ordered Quantity</td>
                                            <td>Received Quantity</td>
                                            <td>Expiry Date</td>
                                            <td>Action</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($product))
                                            <tr>
                                                <td>
                                                    {{ $product->product_name ?? ''}}
                                                </td>
                                                <td>
                                                    {{ $product->sku ?? ''}}
                                                </td>
                                                <td>
                                                    {{ $product->unit_name ?? ''}}
                                                <td>
                                                    {{ $product->ordered_quantity ?? ''}}
                                                </td>
                                                <td>
                                                    {{ $product->received_quantity ?? ''}}
                                                </td>
                                                <td>
                                                    {{ $product->expiry_date ?? ''}}
                                                </td>
                                                <td>
                                                    <button type="submit" class="btn btn-success"><span class="mt-3 ml-3 mr-3 mb-3">Get Barcodes</span></button>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- end .row -->
                    </form>

                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection
@push('post_scripts')
    <script>
        $('#select-all').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $('#tcheckbox:checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $('#tcheckbox:checkbox').each(function () {
                    this.checked = false;
                });
            }
        });
    </script>
@endpush
