@extends('dboard.index')

@section('title', 'Barcodes')

@section('dboard_content')

    <!-- data-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-6 text-left">
                            <h2 class="title-heading mb-2">Barcodes For {{!empty($product) ? $product->product_name : 'Not Found'}}</h2>
                            <h5 >Brand Name: {{ !empty($product) ? $product->brand_name : 'Not Found' }}</h5>
                            <h5>Batch No : {{ !empty($product) ? $product->batch_no : 'Not Found' }}</h5>
                            <h5>Barcode Quantity : {{ $barcode_qty }}</h5>
                            <h5 >Expiry Date : {{ !empty($product) ? $product->expiry_date : 'Not Found'}}</h5>
                        </div>
                        <div class="col-md-6 text-right">
                            <div id="button-area" class="text-right">
                                <a onclick="window.print()" class="btn btn-outline-primary mr-3">Prints<i class="pl-2 fa fa-print"></i></a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @if(empty($product))
                        {{ $barcode = $product->id }}
                    @endif
                    <div class="row mt-4">
                        @for ($i = 0; $i < $barcode_qty; $i++)
                            <div class="col-md-4 mb-4">
                                @php
                                    $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
                                @endphp

                                {!! $generator->getBarcode(!empty($product) ? $product->batch_no :  $product->sku , $generator::TYPE_CODE_128) !!}
                                <p class="mb-3" style="margin-left: 30%">{{ !empty($product) ? $product->batch_no :  $product->sku }}</p>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
