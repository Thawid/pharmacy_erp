@extends('dboard.index')
@section('title', 'Products For Barcode')
@push('styles')
    <style>

    </style>
@endpush
@section('dboard_content')
    <!-- title-area -->
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-9 text-left">
                            <h2 class="title-heading">Products For Barcode</h2>
                        </div><!-- end .col-md-9 -->
                    </div><!-- end .row -->
                    <hr>
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped overflow-hidden table-bordered table-hover bg-white table-md">
                                    <thead>
                                    <tr>
                                        <td>SL</td>
                                        <td>Product Name</td>
                                        <td>SKU</td>
                                        <td>Batch No</td>
                                        <td>Unit</td>
                                        <td>Ordered Quantity</td>
                                        <td>Received Quantity</td>
                                        <td>Expiry Date</td>
                                        <td>Action</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $key => $details)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>
                                                {{ $details->product_name ?? ''}}
                                            </td>
                                            <td>
                                                {{ $details->sku ?? ''}}
                                            </td>
                                            <td>
                                                {{ $details->batch_no ?? ''}}
                                            </td>
                                            <td>
                                                {{ $details->unit_name ?? ''}}
                                            <td>
                                                {{ $details->ordered_quantity ?? ''}}
                                            </td>
                                            <td>
                                                {{ $details->received_quantity ?? ''}}
                                            </td>
                                            <td>
                                                {{ $details->expiry_date ?? '' }}
                                            </td>
                                            <td>
                                                <a href="{{route('store-barcode.show.product', ['entry_id' => !empty($details) ? $details->store_stock_entry_id : 0, 'batch' => !empty($details) ? $details->batch_no : 0, 'product_id' => !empty($details) ? $details->id : 0])}}"><span class="mt-3 ml-3 mr-3 mb-3"><i class="fa fa-eye"></i></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end .row -->

                </div><!-- end .tile-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection
@push('post_scripts')
    <script>
        $('#select-all').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $('#tcheckbox:checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $('#tcheckbox:checkbox').each(function () {
                    this.checked = false;
                });
            }
        });
    </script>
@endpush
