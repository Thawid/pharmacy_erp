@extends('dboard.index')

@section('title','Inventory GRN list')

@push('styles')
<style>
  .po-modal-table thead th {
    font-weight: 400;
  }

  address {
    margin-bottom: 0;
  }
</style>
  
@endpush

@section('dboard_content')

<div class="tile">
  <div class="tile-body">
    <div class="row align-items-center">
      <div class="col-md-8">
        <h2 class="title-heading">Purchase Order Receiving List</h2>
      </div>
      <div class="col-md-4 text-md-right">
        {{--<a class="btn btn-outline-primary icon-btn" href={{route('inventory-GRN.create')}}><i class="fa fa-arrow-right"></i> New Create</a>--}}
      </div>
    </div><!-- end.row -->
    <hr>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
          <thead>
            <tr>
              <th>SL</th>
              <th>Product Order Info</th>
              <th>Hub Info</th>
              {{-- <th>Requisition Type</th>--}}
              <th>Priority</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if(isset( $purchaseOrders))
            @foreach( $purchaseOrders as $key => $purchaseOrder)
            {{--{{ dd($purchaseOrder) }}--}}
            <tr>
              <td>{{ $key + 1 }}</td>
              <td>
                <address>
                  <strong>Purchase Order Id: {{$purchaseOrder->id}}</strong><br>
                  <strong>Requisition Id: {{$purchaseOrder->hq_approve_requisition_id}}</strong><br>
                  Created Date: {{$purchaseOrder->purchase_order_date}}<br>
                  Request Delivery Date: {{$purchaseOrder->requested_delivery_date}} <br>
                </address>
              </td>
              <td><strong> {{$purchaseOrder->hub_info->name}}</strong></td>
              <td><strong>{{($purchaseOrder->priority == '1') ? 'High' : 'Low'}}</strong></td>
              <td><strong>{{ $purchaseOrder->grn_receive_status }}</strong></td>

              <td>
                <div class="d-flex align-items-center justify-content-around">
                  <a href="#myModal" data-toggle="modal" data-target="#myModal_{{ $purchaseOrder->id}}" data-id="{{$purchaseOrder->id}}">
                    <button type="button" class="btn details-btn" name="" id=""><i class="fa fa-eye"></i></button>
                  </a>
                  <div class="modal fade" id="myModal_{{ $purchaseOrder->id}}" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Purchase Order Receiving List - Details</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <table class="table table-striped table-bordered table-hover bg-white table-sm po-modal-table">
                            <thead>
                              <tr>
                                <th>Product Order Id</th>
                                <th>Requisition Id</th>
                                <th>Hub Id</th>
                                <th>Created Date</th>
                                <th>Request Delivery Date</th>
                                <th>Priority</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr class="border mt-2">
                                <td>{{ $purchaseOrder->id }}</td>
                                <td>{{ $purchaseOrder->hq_approve_requisition_id }}</td>
                                <td>1</td>
                                <td>{{ $purchaseOrder->purchase_order_date }}</td>
                                <td>{{ $purchaseOrder->requested_delivery_date }}</td>
                                <td>{{( $purchaseOrder->priority == '1') ? 'High' : 'Low'}}</td>

                                <td>{{ $purchaseOrder->status }}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn index-btn" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  @if(auth()->user()->role == 'warehouse_user')
                  @if($purchaseOrder->grn_receive_status == 'Pending')
                  @can('hasEditPermission')
                  <a href="{{route('inventory-GRN.edit',$purchaseOrder->id)}}" class="btn details-btn" type="button" name="" id=""><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"></i></a>
                  @endcan
                  @endif
                  @endif

                </div>
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div><!-- end.col-md-12 -->
    </div><!-- end.row -->
  </div><!-- end.tile-body -->
</div><!-- end.tile -->



@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionList').DataTable();
  });
</script>

@endpush