@extends('dboard.index')

@section('title','Inventory GRN list')

@push('styles')
<style>
  .btn .icon,
  .btn .fa {
    font-size: 14px;
    margin-right: 0px;
    vertical-align: middle;
  }

  .delete-btn {
    background: rgba(255, 35, 35, 0.08);
    box-sizing: border-box;
    border-radius: 8px;
    border: 1px solid #ff2323;
    color: #ff2323;
    font-size: 14px;
  }
</style>
@endpush

@section('dboard_content')
<form action="{{ route('inventory-GRN.update', $purchaseOrders->id) }}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
  <div class="tile">
    <div class="tile-body">
      <div class="row align-items-start">
        <div class="col-md-12">
          <h2 class="title-heading">Process Goods Receive Note</h2>
        </div><!-- end.col-md-12 -->
      </div><!-- end.row -->
      <hr>

      <div class="row">
        <div class="col-md-4 text-left">
          <h5>Requisitions Number# {{ $purchaseOrders->hq_approve_requisition_id }} Purchase Order No# {{ $purchaseOrders->purchase_order_no }}</h5>
          <input type="hidden" name="vendor" value="{{ $purchaseOrders->vendor_id }}">
          <input type="hidden" name="hub_requisition_no" value="{{ $purchaseOrders->hq_approve_requisition_id }}"><input type="hidden" name="purchase_order_no" value="{{ $purchaseOrders->purchase_order_no }}">
          <p>Requisition Id: {{ $purchaseOrders->hq_approve_requisition_id }}</p><input type="hidden" name="requisition_id" value="{{ $purchaseOrders->hq_approve_requisition_id }}">
          <p>Hub Id: {{ $purchaseOrders->hub_id }}</p><input type="hidden" name="hub_id" value="{{ $purchaseOrders->hub_id }}">
        </div>

        <div class="col-md-4 text-left">
          <p>Request Date: {{ $purchaseOrders->purchase_order_date }}</p><input type="hidden" name="request_date" value="{{ $purchaseOrders->purchase_order_date }}">
          <p>Request Delivery Date: {{ $purchaseOrders->requested_delivery_date }}</p><input type="hidden" name="delivery_date" value="{{ $purchaseOrders->requested_delivery_date }}">
          <p>Priority: {{($purchaseOrders->priority == '1') ? 'High' : 'Low'}}</p><input type="hidden" name="priority" value="{{ $purchaseOrders->priority }}">
          <p>Delivery Point: {{ $purchaseOrders->purchase_order_no }}</p><input type="hidden" name="delivery_point" value="{{ $purchaseOrders->purchase_order_no }}">
        </div>

        <div class="col-md-4 text-left">
          {{--<p>Lot/Batch Number: <input type="text" name="lot_no" placeholder="Label"></p>--}}
          {{--<p>Status: <input type="text" name="change_status" placeholder="Label" required></p>--}}
          <div class="form-group">
            <label for="status" class="forget-form">Select Status</label>
            <select class="js-example-basic-single form-control" name="change_status">
              <option value="Pending" {{ $purchaseOrders->status == 'Pending' ? 'selected' : '' }}>Pending</option>
              <option value="Approved" {{ $purchaseOrders->status == 'Approved' ? 'selected' : '' }}>Approved</option>
            </select>
          </div>
        </div>
      </div><!-- end.row -->
      <hr>

      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered table-striped table-hover requisition-list" id="grn-product-list">
            <thead>
              <tr>
                <th>SN</th>
                <th>Product</th>
                <th>Generic</th>
                <th>Unit</th>
                <th>UOM</th>
                <th>Order Quantity</th>
                <th>Status</th>
                <th>Action</th>
                <th>Process</th>
              </tr>
            </thead>
            <tbody>
              @if(isset( $purchaseOrderDetails))
              @foreach( $purchaseOrderDetails as $key => $purchaseOrderDetail)

              <tr>
                <td>{{ $key + 1 }}</td>
                <td>
                  {{ $purchaseOrderDetail->product->product_name }}
                </td><input type="hidden" name="product_id[]" value="{{ $purchaseOrderDetail->product_id }}">
                <td>
                  {{ $purchaseOrderDetail->generic->name }}
                </td><input type="hidden" name="generic_id[]" value="{{ $purchaseOrderDetail->generic_id }}">
                <td>
                  {{ $purchaseOrderDetail->unit->unit_name }}
                </td><input type="hidden" name="unit_id[]" value="{{ $purchaseOrderDetail->unit_id }}">
                <td>
                  {{ $purchaseOrderDetail->uom->uom_name }}
                </td><input type="hidden" name="uom_id[]" value="{{ $purchaseOrderDetail->uom_id }}">
                <td>{{ $purchaseOrderDetail->order_quantity }}</td><input type="hidden" name="quantity[]" value="{{ $purchaseOrderDetail->order_quantity }}">
                {{--<td><input type="number" name="received_quantity[]" placeholder="qty" value="{{ $purchaseOrderDetail->delivered_quantity ?? 0 }}" required readonly></td>--}}
                <td>{{ $purchaseOrders->status }}</td><input type="hidden" name="status[]" value="{{ $purchaseOrders->status }}">
                <td>
                  <input type="checkbox" id="" name="ysnReceived[]" value="1"> Received All <br>
                  <input type="checkbox" id="" name="ysnQc[]" value="1"> Send To QC <br>
                  <input type="checkbox" id="" name="ysnStock[]" value="1"> Release for Stock Entry
                </td>
                <td>
                  <div class="d-flex justify-content-center">
                    @if($purchaseOrderDetail->grn_status == 1)
                    <a href="javascript:void(0);" class="btn details-btn grn-process disabled">
                      <i class="fa fa-plus-circle"></i>
                    </a>
                    @else
                    <a href="javascript:void(0);" class="btn details-btn grn-process" data-toggle="modal" data-target="#grnDetails" data-route="{{route('grn.process',[$purchaseOrderDetail->purchase_order_id,$purchaseOrderDetail->product_id])}}">
                      <i class="fa fa-plus-circle"></i>
                    </a>
                    @endif
                  </div>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div><!-- end.col-md-12 -->
      </div><!-- end.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <textarea class="form-control" name="note" value="" placeholder="Some note here"></textarea>
          </div>
        </div><!-- end.col-md-12 -->
      </div><!-- end.row -->
      <hr>

      <div class="row">
        <div class="col-md-6 text-left">
          <a href="javascript:void (0);" class="btn index-btn text-left">Attached File <input type="file" name="attach_file"></a>
        </div><!-- end.col-md-6 -->

        <div class="col-md-6 text-right">
          <button class="btn index-btn text-right" type="submit">Submit</button>
        </div><!-- end.col-md-6 -->
      </div><!-- end.row -->
    </div><!-- end.tile-body -->
  </div><!-- end.tile -->
</form>

<div class="modal fade" id="grnDetails">
  <div class="modal-dialog modal-lg">
    <div id="grn-data"> </div>
  </div>
</div>
@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionList').DataTable();
  });
</script>

<script>
  $('#select-all').click(function(event) {
    if (this.checked) {
      // Iterate each checkbox
      $(':checkbox').each(function() {
        this.checked = true;
      });
    } else {
      $(':checkbox').each(function() {
        this.checked = false;
      });
    }
  });
</script>

<script>
  $(document).ready(function() {

    $(".grn-process").click(function(e) {
      var url = $(this).attr("data-route");
      //alert(url);
      $.get(url, function(data) {
        console.log(data);
        $('#grn-data').html(data);
      });
    });
  });

  $("#grnDetails").submit(function(e) {

    e.preventDefault();

    let purchase_order_no = $("#purchase_order_no").val();
    let product_id = $("#product_id").val();
    let generic_id = $("#generic_id").val();
    let unit_id = $("#unit_id").val();
    let uom_id = $("#uom_id").val();

    let vendor_id = $("#vendor_id").val();
    let _token = $("input[name= _token]").val();
    let order_quantity = $("#order_quantity").val();

    let received_quantity = [];
    document.querySelectorAll('.received_quantity').forEach((item) => {
      received_quantity.push(item.value);
    })
    let batch_no = [];
    document.querySelectorAll('.batch_no').forEach((item) => {
      batch_no.push(item.value);
    })
    let status = [];
    document.querySelectorAll('.status').forEach((item) => {
      status.push(item.value);
    })

    let totalReceive = 0;
    for (let i = 0; i < received_quantity.length; i++) {
      totalReceive += parseInt(received_quantity[i]);
    }
    if (parseInt(totalReceive) > order_quantity) {
      alert('Receive quantity can not be grater then order quantity');
    }
    $.ajax({
      url: "{{route('batch.process')}}",
      type: "POST",
      data: {
        received_quantity: received_quantity,
        batch_no: batch_no,
        status: status,
        purchase_order_no: purchase_order_no,
        product_id: product_id,
        generic_id: generic_id,
        unit_id: unit_id,
        uom_id: uom_id,
        order_quantity: order_quantity,
        vendor_id: vendor_id,
        _token: _token,
      },

      success: function(response) {
        console.log(response)
        if (response) {
          $("#batch_process")[0].reset();
          $("#grnDetails").modal('hide');
          location.reload();
        }
      }
    });
  });
</script>

<script>
  function addNewGrnProcess() {
    $('#generated-table-body').append(
      `<tr>
        <td>
          <input type="number" class="form-control received_quantity" name="received_quantity[]">
        </td>

        <td>
          <input type="text" class="form-control batch_no" name="batch_no[]">
        </td>

        <td>
          <select class="form-control status" name="status[]">
            <option value="Regular">Regular</option>
            <option value="Return">Return</option>
            <option value="Damage">Damage</option>
          </select>
        </td>

        <td>
          <a href="javascript:void(0);" type="button" class="btn btn-outline-danger w-100 text-center item_remove" id="add-new-grn-fields">X</a>
        </td>
      </tr>`
    );
  }

  // Remove Item
  $(document).on('click', '.item_remove', function() {
    $(this).parent().parent().remove();
  });
</script>

@endpush
