<div class="modal-content">
  <div class="modal-header">
    <h4 class="title-heading">GRN Process </h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <!-- <div class="card-body"> -->
    <form id="batch_process" method="POST">
      @csrf

      <input type="hidden" id="purchase_order_no" name="purchase_order_no" value="{{ $grn_process->purchase_order_no }}">
      <input type="hidden" id="product_id" name="product_id" value="{{ $grn_process->product_id }}">
      <input type="hidden" id="generic_id" name="generic_id" value="{{ $grn_process->generic_id }}">
      <input type="hidden" id="unit_id" name="unit_id" value="{{ $grn_process->unit_id }}">
      <input type="hidden" id="uom_id" name="uom_id" value="{{ $grn_process->uom_id }}">
      <input type="hidden" id="order_quantity" name="ordered_quantity" value="{{ $grn_process->order_quantity }}">
      <input type="hidden" id="vendor_id" name="vendor_id" value="{{ $grn_process->vendor_id }}">

      <p class="mb-2"><strong>Order Qty : {{ $grn_process->order_quantity }} </strong></p>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <th>Receive Qty</th>
              <th>Batch No</th>
              <th>Status</th>
              <th width="3%">Action</th>
            </thead>
            <tbody id="generated-table-body">
              <tr>
                <td><input type="number" class="form-control received_quantity" name="received_quantity[]"></td>
                <td><input type="text" class="form-control batch_no" name="batch_no[]"></td>
                <td>
                  <select class="form-control status" name="status[]">
                    <option value="Regular">Regular</option>
                    <option value="Return">Return</option>
                    <option value="Damage">Damage</option>
                  </select>
                </td>
                <td>
                  <a href="javascript:void(0);" type="button" class="btn create-btn w-100 text-center" id="add-new-grn-fields" onclick="addNewGrnProcess()"><i class="fa fa-plus"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
        </div><!-- end.col-md-12 -->
      </div><!-- end.row -->
      <hr>

      <div class="row">
        <div class="col-md-12 text-right">
          <button type="button" class="btn index-btn mr-2" data-dismiss="modal" aria-label="Close">Close</button>

          <button type="submit" class="btn index-btn">Submit</button>
        </div><!-- end.col-md-12 -->
      </div><!-- end.row -->
    </form>
    <!-- </div> -->
  </div>
</div>
