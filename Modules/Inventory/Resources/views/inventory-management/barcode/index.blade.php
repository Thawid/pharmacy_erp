@extends('dboard.index')

@section('title','Barcode For Stock Entry')

@push('styles')
<style>

</style>
@endpush

@section('dboard_content')

<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-9 text-left">
            <h2 class="title-heading">Barcode For Stock Entry</h2>
          </div><!-- end .col-md-9 -->
        </div><!-- end .row -->
        <hr>

        <div class="row mt-4">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover bg-white" id="requisitionList">
                <thead>
                  <tr>
                    <th width="5%">#SL</th>
                    <th width="30%">GRN Details</th>
                    <th width="15%">Attached Files</th>
                    <th width="10%">Entry Status</th>
                    <th width="10%">Action</th>
                  </tr>
                </thead><!-- end thead -->

                <tbody>
                  @foreach($grn_processes as $key=>$grn_process)
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>
                      <div class="">
                        <p>
                          <strong>Purchase Order
                            No: {{ $grn_process->purchase_order_no }}
                          </strong>
                        </p>
                        <p>
                          <strong>
                            Unload Date:
                          </strong>
                          {{ $grn_process->delivery_date }}
                        </p>
                        <p>
                          <strong>
                            Lot / Branch:
                          </strong>
                          {{ $grn_process->lot_no }}
                        </p>
                      </div>
                    </td>

                    <td><img src="{{ asset('uploads/grn-doc/'.$grn_process->image) }}" height="50" alt=""></td>
                    <td>
                      <p class="badge-info p-2 rounded">{{ $grn_process->requisition_type =='Approved' ? 'Entered' : 'Pending' }}</p>
                    </td>

                    <td>
                      <div class="d-md-flex justify-content-center align-items-center">
                        <a href="{{ route('barcode.show', $grn_process->id) }}" class="btn details-btn" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"></i></a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table><!-- table -->
            </div><!-- end .table-responsive -->
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->

<!-- end data-area -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionList').DataTable();
  });
</script>

@endpush