@extends('dboard.index')

@section('title', 'Barcodes')

@section('dboard_content')

<!-- data-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-6 text-left">
            <h2 class="title-heading mb-2">Barcode For {{!empty($stock_entry_details->product) ? $stock_entry_details->product->product_name : ' This Product'}}</h2>
            <p><strong>Stock ID: {{!empty($stock_entry) ? $stock_entry->stock_id : ' Not Found'}}</strong></p>
            <p><strong>Stock Quantity : {{!empty($stock_entry_details) ? $stock_entry_details->stock_quantity : ' Not Found'}}</strong></p>
            @foreach($inventory_locations as $inventory_location)
            @if($inventory_location->id === $stock_entry_details->inventory_location_id)
            <p><strong>Inventory Location : {{!empty($inventory_location) ? $inventory_location->state : ' Not Found'}}</strong></p>
            @endif
            @endforeach
            <p><strong>Expiry Date : {{!empty($stock_entry_details) ? $stock_entry_details->expiry_date : ' Not Found'}}</strong></p>
          </div>
          <div class="col-md-6 text-right">
            <div id="button-area" class="text-right">
              <a onclick="window.print()" class="btn index-btn">Prints<i class="pl-2 fa fa-print"></i></a>
            </div>
          </div>
        </div>
        <hr>
        @if(empty($stock_entry_details->barcode))
        {{ $barcode = $stock_entry_details->barcode }}
        @endif
        <div class="row mt-4">
          @for ($i = 0; $i < $stock_entry_details->stock_quantity; $i++)
            <div class="col-md-4 mb-4">
              @php
              $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
              @endphp

              {!! $generator->getBarcode(!empty($grn_process_details->batch_no) ? $grn_process_details->batch_no : $stock_entry_details->sku , $generator::TYPE_CODE_128) !!}
              <p class="mb-3" style="margin-left: 30%">{{ !empty($grn_process_details->batch_no) ? $grn_process_details->batch_no :  $stock_entry_details->sku }}</p>
            </div>
            @endfor
        </div>
      </div>
    </div>
  </div>
</div>

@endsection