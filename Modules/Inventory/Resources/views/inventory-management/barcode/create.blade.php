@extends('dboard.index')
@section('title', 'Stock Entry For Barcode')
@push('styles')
<style>
  .table thead th {
    font-weight: bolder;
  }
</style>
@endpush
@section('dboard_content')
<!-- title-area -->
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-9 text-left">
            <h2 class="title-heading">Stock Entry For Barcode</h2>
          </div><!-- end .col-md-9 -->

          <div class="col-md-3 text-right">
            <a class="btn index-btn" href=" {{ url('inventory/barcode') }}">Back</a>
          </div><!-- end .col-md-3 -->
        </div><!-- end .row -->
        <hr>
        <div class="row">
          <div class="col-md-12">
            <p><strong>Purchase Order No # {{ $grn_process->purchase_order_no }}</strong></p>
            <div class="row mt-2">
              <div class="col-xl-4 col-lg-3">
                <!-- lot /batch -->
                <label for="">Lot / Batch Number: </label>
                <input type="hidden" name="inventory_grn_id" value="{{$grn_process->id ?? 0}}">
                <input type="hidden" name="hub_id" value="{{$grn_process->hub_id ?? 0}}">
                <input type="text" class="border-0 rounded form-control" name="lot_no" value="{{ $grn_process->lot_no ?? 0}}" readonly>

              </div>
              <div class="col-xl-4 col-lg-3">
                <!-- stock id -->
                <label for="">Stock ID: </label>
                <input type="text" class="border-0 rounded form-control" name="stock_id" value="{{ $stock_entry->stock_id ?? 0}}" readonly>


              </div>
              <div class="col-xl-4 col-lg-3">
                <!-- status -->
                <label for="">Status: </label>
                <input type="text" class="border-0 rounded form-control" name="status" value="{{ $grn_process->status ?? ''}}" readonly>
              </div>
            </div>
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->

        <div class="row mt-4">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-striped overflow-hidden table-bordered table-hover bg-white table-md">
                <thead>
                  <tr>
                    <th>SL</th>
                    <th>Product Name</th>
                    <th>SKU</th>
                    <th>Unit</th>
                    <th>Received Quantity</th>
                    <th>Stock Quantity</th>
                    <th>Inventory Location</th>
                    <th>Expiry Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($grn_process_details as $key => $details)
                  @php
                  $product_name = Modules\Product\Entities\Product::where('id', $details->product_id)->value('product_name');
                  $unit_name = Modules\Product\Entities\ProductUnit::where('id', $details->unit_id)->value('unit_name');
                  @endphp
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>
                      {{ $product_name ?? ''}}
                    </td>
                    <td>
                      {{ $stock_entry->hubStockEntryDetails[$key]->sku ?? ''}}
                    </td>
                    <td>
                      {{ $unit_name ?? ''}}
                    <td>
                      {{ $stock_entry->hubStockEntryDetails[$key]->received_quantity ?? ''}}
                    </td>
                    <td>
                      {{ $stock_entry->hubStockEntryDetails[$key]->stock_quantity ?? ''}}
                    </td>
                    <td>
                      @foreach($inventory_locations as $inventory_location)
                      @if($inventory_location->id === $stock_entry->hubStockEntryDetails[$key]->inventory_location_id)
                      {{ $inventory_location->state ?? ''}}
                      @endif
                      @endforeach
                    </td>
                    <td>
                      {{ $stock_entry->hubStockEntryDetails[$key]->expiry_date ?? ''}}
                    </td>
                    <td>
                      <a href="{{route('barcode.generate', ['grn_id' => $stock_entry->grn_process_id, 'stock_id' => $stock_entry->hubStockEntryDetails[$key]->id])}}">
                        <button type="button" class="btn btn-success">Get Barcodes</button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- end .row -->

      </div><!-- end .tile-body -->
    </div><!-- end .tile -->
  </div><!-- end .col-md-12 -->
</div><!-- end .row -->

@endsection
@push('post_scripts')
<script>
  $('#select-all').click(function(event) {
    if (this.checked) {
      // Iterate each checkbox
      $('#tcheckbox:checkbox').each(function() {
        this.checked = true;
      });
    } else {
      $('#tcheckbox:checkbox').each(function() {
        this.checked = false;
      });
    }
  });
</script>
@endpush