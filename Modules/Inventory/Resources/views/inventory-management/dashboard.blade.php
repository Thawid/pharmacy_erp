@extends('dboard.index')
@section('title','Vendor Dashboard')

@push('styles')
<style>
  .custom-tile-height {
    height: 450px;
    overflow-y: scroll;
  }

@media (min-width: 992px) and (max-width: 1199px) {
  .title-heading {
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 20px;
    color: #294a65;
}
}

</style>
@endpush
@section('dboard_content')
    <div class="row">
      <div class="col-md-6">
        <div class="tile custom-tile-height">
          <h3 class="title-heading">Type Wise Inventory Quantity</h3>
          <hr>
          <div id="pi-chart" class="d-flex justify-content-center"></div>
        </div><!-- end.tile -->
      </div><!-- end.col-md-6 -->

        <div class="col-md-6">
            {{--latest 5 Recent Expiry--}}
            @if(isset($recent_expiry))
                <div class="tile custom-tile-height" id="recent-expiry-tile-id">
                    <div class="tile-body">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <h2 class="title-heading">Recent Expiry</h2>
                            </div>
                        </div><!-- end.row -->
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover requisition-list"
                                           id="requisitionList">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Created Date</th>
                                            <th>Expiry Date</th>
                                            <th>Stock Quantity</th>
                                            <th>SKU</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($recent_expiry as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>{{$row->expiry_date}}</td>
                                                <td>{{$row->stock_quantity}}</td>
                                                <td>{{ $row->sku }}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div><!-- end.table-responsive -->
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </div>
                </div>
            @endif
            {{--end latest 5 store requisitions--}}
        </div><!-- end.col-md-6 -->
    </div><!-- end.row -->

    <div class="row">
        <div class="col-md-6">
            {{--latest 5 purchase order--}}
            @if(isset($recent_stockIn))
                <div class="tile custom-tile-height">
                    <div class="tile-body">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <h2 class="title-heading">Recent Stock In</h2>
                            </div>
                        </div><!-- end.row -->
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover requisition-list"
                                           id="requisitionList">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>SKU</th>
                                            <th>Stock Quantity</th>
                                            <th>Created Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($recent_stockIn as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{$row->sku}}</td>
                                                <td>{{$row->stock_quantity}}</td>
                                                <td>{{$row->created_at}}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div><!-- end.table-responsive -->
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </div>
                </div>
            @endif
            {{--end latest 5 purchase order--}}
        </div>
        <div class="col-md-6">
            {{--latest 5 purchase order--}}
            @if(isset($recent_distributions))
                <div class="tile custom-tile-height">
                    <div class="tile-body">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <h2 class="title-heading">Recent Distributions</h2>
                            </div>
                        </div><!-- end.row -->
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover requisition-list"
                                           id="requisitionList">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Distribution Number</th>
                                            <th>Delivery Date</th>
                                            <th>Created Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($recent_distributions as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{$row->distribution_no}}</td>
                                                <td>{{$row->delivery_date}}</td>
                                                <td>{{$row->created_at}}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div><!-- end.table-responsive -->
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                    </div>
                </div>
            @endif
            {{--end latest 5 purchase order--}}
        </div>
    </div><!-- end.row -->

@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{ asset('js/apex_chart.js') }}"></script>
    <script>
        //  pi chart
        var series = [
            @php
                if(count($type_wise_inventory_qty) > 0){
                    foreach ($type_wise_inventory_qty as $row){
                        echo $row->ordered_quantity . ",";
                    }
                }
            @endphp
        ];
        let labels = [
            @php
                if(count($type_wise_inventory_qty) > 0){
                      foreach ($type_wise_inventory_qty as $row){
                          echo '"'.$row->inventory_status .'",';
                      }
                }
            @endphp
        ];

        let newSeries = [];
        let newLabels = [];
        let grouped = 0;
        series.forEach((s, i) => {
            if (s < 10) {
                grouped += s;
            }
            if (s >= 0) {
                newSeries.push(s);
                newLabels.push(labels[i]);
            }
        });

        if (grouped > 0) {
            newSeries.push(grouped);
            newLabels.push("Others");
        }

        var options = {
            legend: {
                show: false,
            },
            series: newSeries,
            chart: {
                width: 380,
                type: "pie",
            },
            colors: ["#008FFB", "#00E396", "#FEB019", "#ccc"],
            labels: newLabels,
        };

        var chart = new ApexCharts(document.querySelector("#pi-chart"), options);
        chart.render();
    </script>
@endpush
