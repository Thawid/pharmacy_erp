@extends('dboard.index')

@section('title','Damage Products')
@push('styles')
    <style>
        nav>.nav-tabs>a.nav-item {
            font-size: 18px;
        }

    </style>
@endpush
@section('dboard_content')

    <div class="tile">
        <div class="tile-body">
            <div class="row align-items-center">
                <div class="col-md-9 text-left">
                    <h2 class="title-heading">Damage Product List</h2>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <div class="table-responsive bg-white pb-3 pt-5">
                        <table class="table table-bordered table-striped table-hover requisition-list-hq" id="requisitionListHq">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Product Name</th>
                                <th>Vendor Name</th>
                                <th>Batch</th>
                                <th> QTY</th>
                                <th>UOM</th>

                            </tr>
                            </thead>
                            <tbody>
                            {{--{{ dd($return_products) }}--}}
                            @foreach($return_products as $key => $return)
                                {{--{{ dd($return) }}--}}
                                <tr>
                                    <td>{{ $key + 1 }}</td>

                                    <td> <strong>{{ $return->product_name }}</strong> </td>
                                    <td> {{ $return->vendor_name ?? null }}</td>
                                    <td> {{ $return->batch_no }}</td>
                                    <td><strong> {{ $return->received_quantity }} </strong></td>
                                    <td> {{ $return->uom_name }} </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> <!-- end .col-md-12 -->
            </div> <!-- end .row -->
        </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();

    </script>
    <script>
        $(document).ready(function () {
            $('#requisitionListHq').DataTable();
        });

    </script>

@endpush
