@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')
@endpush
@section('dboard_content')

<div class="tile">
  <div class="tile-body">
    <div class="row align-items-center">
      <div class="col-md-6">
        <h2 class="title-heading">Regular Distribution</h2>
      </div><!-- end.col-md-6 -->
    </div><!-- end.row -->
    <hr>

    <div class="row">
      <div class="col-md-12">
        <div class="">
          <table class="table table-bordered table-striped table-hover requisition-list-hq table-responsive-xl table-responsive-lg" id="requisitionListHq">
            <thead>
              <tr>
                <th>SL</th>
                <th>Requisition Info</th>
                <th>Store Info</th>
                <th>Priority</th>
                <th>Status</th>
                <th>Distribution Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($regular_distributes as $k => $regular_distribute)
              <tr>
                <td>{{ $k+1 }}</td>
                <td>
                  <p>
                    <strong>ID:{{ $regular_distribute->store_requisition_no }}</strong><br>
                    <strong>Created Date: {{ $regular_distribute->requisition_date }}</strong><br>
                    <strong>Request Delivery Date Date: {{ $regular_distribute->delivery_date }}</strong>
                  </p>
                </td>
                <td><strong>{{ $regular_distribute->store->name }}</strong></td>
                <td><strong>{{ $regular_distribute->priority }}</strong></td>
                <td>{{ $regular_distribute->status }}</td>
                <td>Partial</td>

                <td>
                  <div class="d-md-flex justify-content-around">
                  <a href="{{route('distribution.process.show', $regular_distribute->id )}}" class="btn details-btn" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                  <a href="{{route('distribution.process.process', $regular_distribute->id )}}" class="btn edit-btn" type="button"><i class="fa fa-spinner" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!-- end.table-responsive -->
      </div> <!-- end .col-md-12 -->
    </div> <!-- end .row -->
  </div> <!-- end .tile-body -->
</div><!-- end .tile -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionListHq').DataTable();
  });
</script>

@endpush