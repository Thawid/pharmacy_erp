@extends('dboard.index')

@section('title','Requisition Details | HQ')

@section('dboard_content')
    @push('style')
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    @endpush

    <style>
        .input_text{
            border: none;
        }
        .input_text:focus{
            outline: none;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row align-items-center">
                        <div class="col-md-6 text-left">
                            <h2 class="title-heading">Distribution Process</h2>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a class="btn index-btn" href={{route('distribution.process')}}>Distribution Process
                            </a>
                        </div><!-- end.col-md-6 -->
                    </div><!-- end.row -->
                    <hr>

                    <form  method="POST" action="{{ route('distribution.process.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <p><strong>Distribution ID: <input name="distribute_id" class="input_text" value="{{ $regular_distribute_requests->id }}"></strong></p>
                                <p><strong>Store : <input name="" class="input_text" value="{{ $regular_distribute_requests->store->name }}"> </strong></p>
                                <p><strong>Store ID : <input name="store_id" class="input_text" value="{{ $regular_distribute_requests->store_id }}"></strong></p>
                                <p><strong>Store Type : <input name="store_type" class="input_text" value="{{ $regular_distribute_requests->store_type }}"></strong></p>
                                <p><strong>Status: Hub Processing</strong></p><input name="store_requisition_no" type="hidden" value="{{ $regular_distribute_requests->store_requisition_no }}">
                                <input name="status" type="hidden" value="{{ $regular_distribute_requests->distribution_status }}">
                                <input name="warehouse_id" type="hidden" value="{{ $regular_distribute_requests->warehouse_id }}">
                            </div>

                            <div class="col-md-6 text-md-left">
                                <p><strong>Request Date : <input name="requisition_date" class="input_text" value="{{ $regular_distribute_requests->requisition_date }}"></strong></p>
                                <p><strong>Request Delivery Date : <input name="delivery_date" class="input_text" value="{{ $regular_distribute_requests->delivery_date }}"></strong></p>
                                <p><strong>Priority: <input name="priority" class="input_text" value="{{ $regular_distribute_requests->priority }}"></strong></p>
                            </div>
                        </div><!-- end.row -->

                        <div class="row mt-4 mb-0">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                                <thead>
                                <tr class="bg-white">
                                    <th>Product Name</th>
                                    <th>Generic Name</th>
                                    <th>Unit</th>
                                    <th>Available Vendor</th>
                                    <th>Req Quantity</th>
                                    <th>Distribution Qty</th>
                                    <th>UOM</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($regular_distribution_request_details as $regular_distribution_request_detail)
                                        <tr>

                                            <td>
                                                <input type="text" class="form-control" hidden name="product_id[]"  value="{{ $regular_distribution_request_detail->product->id }}"readonly>
                                                <input type="text" class="form-control" name="product_name[]" value="{{ $regular_distribution_request_detail->product->product_name }}"readonly>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" hidden name="generic_id[]" value="{{ $regular_distribution_request_detail->generic->id }}"readonly>
                                                <input class="form-control" type="text" name="generic_name[]" value="{{ $regular_distribution_request_detail->generic->name }}" readonly>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" hidden name="unit_id[]" value="{{ $regular_distribution_request_detail->unit->id }}"readonly>
                                                <input type="text" class="form-control" name="unit_name[]" value="{{ $regular_distribution_request_detail->unit->unit_name }}" readonly>
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" hidden name="vendor_id[]" value="{{ $regular_distribution_request_detail->vendor->id }}"readonly>
                                                <input class="form-control" type="text" name="vendor_name[]" value="{{ $regular_distribution_request_detail->vendor->name }}" readonly>
                                            </td>

                                            <td>
                                                <input class="form-control" type="number" name="request_quantity[]" value="{{ $regular_distribution_request_detail->request_quantity }}" readonly>
                                            </td>

                                            <td><input class="form-control" type="number" name="distribution_quantity[]" value="{{ $regular_distribution_request_detail->distribution_quantity }}" readonly></td>
                                            <td>
                                                <input class="form-control" type="text" hidden name="uom_id[]" value="{{ $regular_distribution_request_detail->product_uom->id }}" readonly>
                                                <input class="form-control" type="text" name="uom_name[]" value="{{ $regular_distribution_request_detail->product_uom->uom_name }}" readonly>
                                            </td>

                                            <input type="hidden" name="price[]" value="{{ $regular_distribution_request_detail->uomPrice->uom_price ?? '' }}">
                                            <input type="hidden" name="currency[]" value="{{ $regular_distribution_request_detail->uomPrice->currency ?? '' }}">
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- end.col-md-12 -->
                        </div><!-- end.row -->
                        <hr>

                        <div class="row text-right">
                            <div class="col-md-12">
                                <button type="submit" class="btn index-btn">Submit</button>
                            </div>
                        </div><!-- end.row -->
                    </form>
                </div><!-- end.tile-body -->
            </div><!-- end.tile -->
        </div><!-- end.col-md-12 -->
    </div><!-- end.row -->


@endsection


@push('post_scripts')

@endpush

