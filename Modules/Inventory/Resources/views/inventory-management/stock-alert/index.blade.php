@extends('dboard.index')

@section('title','Inventory location list')

@section('dboard_content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <!-- title -->
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <h2 class="title-heading">Stock Alert Product List</h2>
                        </div>
                    </div><!-- end .row -->
                    <hr>

                    <!-- data -->
                    <div class="row" id="actionModal">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="stock-alert">
                                    <thead class="thead">
                                    <tr>
                                        <th>SN</th>
                                        <th>Product Name</th>
                                        <th>Available Quantity</th>
                                        <th style="color: red;">Stock Alert Quantity</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($alert_qty_lists))
                                        @foreach($alert_qty_lists as $key => $row)
                                            <tr>
                                                <td>{{$key+=1 }}</td>
                                                <td>{{$row->product_name}}</td>
                                                <td>{{$row->available_quantity}}</td>
                                                <td style="color: red;">{{$row->stock_alert}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- end.col-md-12 -->
                    </div><!-- end.row -->
                </div><!-- end .tite-body -->
            </div><!-- end .tile -->
        </div><!-- end .col-md-12 -->
    </div><!-- end .row -->
@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">$('#stock-alert').DataTable();</script>
@endpush

