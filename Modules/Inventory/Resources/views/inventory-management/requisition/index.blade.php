@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')

@endpush
@section('dboard_content')

<div class="tile">
  <div class="tile-body">
    <div class="row">
      <div class="col-md-12 ">
        <div class="row align-items-center">
          <div class="col-md-12">
            <h1 class="title-heading">Pending Requisition</h1>
          </div><!-- end.col-md-12 -->
        </div><!-- end.row -->
        <hr>

        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover requisition-list" id="requisitionList">
                <thead>
                  <tr>
                    <th>Requisition Info</th>
                    <th>Store Info</th>
                    <th>Priority</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($requisitions as $k=>$requisition)
                  <tr>
                    <td>
                      <p>
                        <strong>ID: {{ $requisition->store_requisition_no }}</strong><br>
                        <strong>Created Date: {{ $requisition->requisition_date }}</strong><br>
                        <strong>Request Delivery Date Date: {{ $requisition->delivery_date }}</strong>
                      </p>
                    </td>
                    <td><strong>{{ $requisition->store->name }}</strong></td>
                    <td><strong>{{ $requisition->priority }}</strong></td>
                    <td><strong>{{ $requisition->status }}</strong></td>
                    <td>
                      <div class="d-flex justify-content-around align-items-center">
                        <a href="{{route('requisition.show',  $requisition->id )}}" class="btn details-btn" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                        <a href="{{route('process', $requisition->id)}}" class="btn edit-btn" type="button"><i class="fa fa-spinner " aria-hidden="true" data-toggle="tooltip" title="process"> </i></a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
      </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->
  </div><!-- end.tile-body -->
</div><!-- end.tile -->

@endsection

@push('post_scripts')
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

<script>
  $('.select2').select2();
</script>
<script>
  $(document).ready(function() {
    $('#requisitionList').DataTable();
  });

  $(document).ready(function() {
    $('#requisitionListHq').DataTable();
  });
</script>

@endpush