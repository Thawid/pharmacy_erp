@extends('dboard.index')

@section('title','Requisition Details | HQ')

@section('dboard_content')
@push('styles')

<style>
  .input_text {
    border: none;
  }

  .input_text:focus {
    outline: none;
  }
</style>
@endpush

<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Pending Requisition Details</h2>
          </div><!-- end.col-md-6 -->
          <div class="col-md-6 text-md-right">
            <a class="btn index-btn" href={{route('requisition.index')}}>Pending list</a>
          </div><!-- end.col-md-6 -->
        </div><!-- end.row -->
        <hr>

        <!-- form -->
        <form method="POST" action="{{ route('requisition.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-6">
              <input class="input_text" name="requisition_id" hidden value="{{ $store_requisition->id }}" readonly>
              <input class="input_text" name="store_requisition_no" hidden value="{{ $store_requisition->store_requisition_no }}" readonly>
              <p><strong>Store : <input class="input_text text-muted" value="{{ $store_requisition->store->name }}" readonly></strong></p>
              <p><strong>Store ID : <input class="input_text text-muted" name="store_id" value="{{ $store_requisition->store_id }}" readonly></strong></p>
              <p><strong>Store Type : <input class="input_text text-muted" name="store_type" value="{{ $store_requisition->store_type }}" readonly></strong></p>
              <p><strong>Status: Hub Processing</strong></p>
            </div>
            <div class="col-md-6">
              <p><strong>Request Date : <input class="input_text text-muted" name="requisition_date" value="{{ $store_requisition->requisition_date }}" readonly></strong></p>
              <p><strong>Request Delivery Date : <input class="input_text text-muted" name="requisition_delivery_date" value="{{ $store_requisition->delivery_date }}" readonly></strong></p>
              <p><strong>Priority: <input class="input_text text-muted" name="requisition_priority" value="{{ $store_requisition->priority }}" readonly></strong></p>
              <input class="input_text" hidden name="warehouse_id" value="{{ $store_requisition->store->warehouse->id }}" readonly>
              <p><strong>Warehouse : <input class="input_text text-muted" value="{{ $store_requisition->store->warehouse->name }}" readonly></strong></p>
              <p><strong>Delivery Date : <input name="approximate_delivery_date" type="date" class="text-muted rounded" required></strong></p>
            </div><!-- end.col-md-4 -->
          </div><!-- end.row -->

          <div class="row mt-4">
            <div class="col-md-12">
              <table class="table table-striped table-bordered table_field table-sm table-responsive" id="table_field">
                <thead>
                  <tr>
                    <th>Product Name</th>
                    <th>Generic Name</th>
                    <th>Unit</th>
                    <th>Available Vendor</th>
                    <th>Req Quantity</th>
                    <th>Available Quantity</th>
                    <th>UOM</th>
                    <th>Distribute Qty</th>
                    <th>Purchase Qty</th>
                    <th>Hold Qty</th>
                    <th class="text-center text-danger">R</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($store_requisition_details as $k => $product)

                  @for($i=0; $i<count($available_products->hubAvailableProductDetails); $i++)
                    @if( $product->product->id == $available_products->hubAvailableProductDetails[$i]->product_id )
                    <tr>
                      <input class="form-control product" hidden type="text" name="product_id[]" value="{{ $product->product->id }}" readonly>
                      <td><input class="form-control" type="text" value="{{ $product->product->product_name }}" readonly></td>

                      <input class="form-control" hidden type="text" name="generic_id[]" value="{{ $product->generic->id }}" readonly>
                      <td><input class="form-control" type="text" value="{{ $product->generic->name }}" readonly></td>

                      <input type="text" hidden class="form-control" name="unit_id[]" value="{{ $product->unit->id }}" readonly>
                      <td><input type="text" class="form-control" value="{{ $product->unit->unit_name }}" readonly></td>

                      <input hidden class="form-control" type="text" name="vendor_id[]" value="{{ $product->vendor->id }}" readonly>
                      <td><input class="form-control" type="text" value="{{ $product->vendor->name }}" readonly></td>

                      <td><input class="form-control" type="number" id="request_qty{{ $k }}" name="request_qty[]" value="{{ $product->request_quantity }}" readonly></td>

                      <td><input class="form-control" type="number" id="available_qty{{ $k }}" name="available_qty[]" value="{{ $available_products->hubAvailableProductDetails[$i]->available_quantity ?? 0 }}" readonly></td>

                      <input hidden class="form-control" type="text" name="uom_id[]" value="{{ $product->product_uom->id }}" readonly>
                      <td><input class="form-control" type="text" value="{{ $product->product_uom->uom_name }}" readonly></td>

                      <td><input class="form-control distribute_qty" type="number" id="distribute_qty{{ $k }}" name="distribute_qty[]"></td>

                      <td><input class="form-control" type="number" id="purchase_qty{{ $k }}" name="purchase_qty[]" value=""></td>

                      <td><input class="form-control" type="number" id="hold_qty{{ $k }}" name="hold_qty[]" value=""></td>

                      <td>
                        <a class="btn btn-outline-danger btn-sm" type="button" data-id="{{ $product->id }}" id="deleteProduct">X</a>
                      </td>
                    </tr>
                    @endif
                    @endfor
                    @endforeach
                </tbody>
              </table>
            </div><!-- end.col-md-12 -->
          </div><!-- end.row -->
          <hr>

          <div class="row text-right">
            <div class="col-md-12">
              <button type="submit" class="btn index-btn">Process</button>
            </div><!-- end.col-md-12 -->
          </div><!-- end.row -->
        </form>
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->
@endsection


@push('post_scripts')
<script>
  $(document).on('click', '#deleteProduct', function() {
    // $(this).parent().parent().remove();
    var id = $(this).data('id');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      type: "DELETE",
      url: "{{ route('requisition.destroy'," + id + ") }}",
      data: {
        id: id
      },
      success: function(result) {
        location.reload();
      }
    });
  });

  var productId = $('.product');

  $(document).ready(function() {
    for (var i = 0; i < productId.length; i++) {
      var requestQty = $('#request_qty' + i).val();
      var availableQty = $('#available_qty' + i).val();
      var distributeQty = $('#distribute_qty' + i).val();
      var result = availableQty - requestQty;
      if (result < 0) {
        $('#distribute_qty' + i).val(availableQty);
        $('#purchase_qty' + i).val(-(result));
        $('#hold_qty' + i).val(-(result));
      } else {
        $('#purchase_qty' + i).val(0);
        $('#distribute_qty' + i).val(requestQty);
        $('#hold_qty' + i).val(0);
      }
    }
  })

  $('.distribute_qty').on('blur', function() {
    for (var i = 0; i < productId.length; i++) {
      if ($('#distribute_qty' + i).val() == $(this).val()) {
        var requestQty = $('#request_qty' + i).val();
        var availableQty = $('#available_qty' + i).val();
        var distributeQty = $(this).val();
        var hold = requestQty - distributeQty
        var purchase = availableQty - distributeQty

        if (purchase <= 0) {
          $('#purchase_qty' + i).val(-(purchase));
        } else {
          $('#purchase_qty' + i).val(0);
        }

        if (hold >= 0) {
          $('#hold_qty' + i).val(hold);
        } else {
          $('#hold_qty' + i).val(0);
        }
      }
    }
  })
</script>
@endpush