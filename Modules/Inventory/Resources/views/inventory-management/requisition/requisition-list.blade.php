@extends('dboard.index')

@section('title','Requisition List | HQ')
@push('styles')
    <style>
        nav > .nav-tabs > a.nav-item {
            font-size: 18px;
        }
    </style>
@endpush
@section('dboard_content')

    <div class="tile">
        <div class="tile-body">
            <div class="row">
                <div class="col-md-9 text-left">
                    <h2>Requisition List </h2>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('inventory/requisition') ? 'active' : '' }} " aria-current="page" href="{{route('requisition.index')}}">
                        <h4>Pending Requisition </h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('inventory/requisitionlist') ? 'active' : '' }} " href="{{route('requisitionlist')}}"><h4>Requisition List</h4></a>
                </li>
            </ul>
            <div class="row">
                <div class="col-md-12 table-responsive" style="padding-bottom: 30px">
                    <div class="table-responsive bg-white p-3 ">
                        <table class="table table-striped table-hover requisition-list-hq" id="requisitionListHq">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Requisition Info</th>
                                <th>Store Info</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><strong>ID:C1001</strong></td>
                                    <td><strong>Banani</strong></td>
                                    <td><strong>High</strong></td>
                                    <td>Ready to Process</td>
                                    <td>
                                        <a href="" class="btn btn-outline-primary btn-sm" type="button"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Details"> </i></a>
                                        <a href="" class="btn btn-outline-primary btn-sm" type="button"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"> </i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> <!-- end .col-md-12 -->
            </div> <!-- end .row -->
        </div> <!-- end .col-md-12 -->
    </div><!-- end .row -->

@endsection

@push('post_scripts')
    <script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
        $(document).ready( function () {
            $('#requisitionListHq').DataTable();
        } );
    </script>

@endpush


