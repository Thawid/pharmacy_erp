@extends('dboard.index')

@section('title','Requisition Details | HQ')


@push('styles')
<style>
  @media (min-width: 992px) and (max-width: 1199px) {
    .form-control {
      padding: 5px;
    }
  }
</style>
@endpush

@section('dboard_content')
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="row align-items-center">
          <div class="col-md-6">
            <h2 class="title-heading">Pending Requisition Details</h2>
          </div><!-- end.col-md-6 -->
          <div class="col-md-6 text-md-right">
            <a class="btn index-btn" href={{route('requisition.index')}}>Pending Requisition</a>
          </div><!-- end.col-md-6 -->
        </div><!-- end.row -->
        <hr>

        <!-- info-area -->
        <div class="row">
          <div class="col-md-6 text-left">
            <p><strong>Store : {{ $requisition->store->name }} </strong></p>
            <p><strong>Store ID : {{ $requisition->store->id }} </strong></p>
            <p><strong>Store Type : {{ $requisition->store_type }}</strong></p>
            <p><strong>Status: Hub Processing</strong></p>
          </div>
          <div class="col-md-4 text-md-left">
            <p><strong>Request Date : {{ $requisition->requisition_date }}</strong></p>
            <p><strong>Request Delivery Date : {{ $requisition->delivery_date }} </strong></p>
            <p><strong>Priority: {{ $requisition->priority }} </strong></p>
            <p><strong>Warehouse Availability: {{ $requisition->store->warehouse->name }} </strong></p>
          </div>
        </div><!-- end.row -->
        <hr>

        <!-- form -->
        <form method="POST" action="" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped table-bordered table_field table-sm" id="table_field">
                <thead>
                  <tr>
                    <th>Product Name</th>
                    <th>Generic Name</th>
                    <th>Unit</th>
                    <th>Available Vendor</th>
                    <th>Req Quantity</th>
                    <th>Available Quantity</th>
                    <th>UOM</th>
                    <th>Status</th>

                  </tr>
                </thead>
                <tbody>
                  @foreach($store_requisition_details as $product)
                  @for($i=0; $i<count($available_products->hubAvailableProductDetails); $i++)
                    @if( $product->product->id == $available_products->hubAvailableProductDetails[$i]->product_id )
                    <tr>
                      <td><input type="text" class="form-control" value="{{ $product->product->product_name }}" readonly></td>
                      <td><input class="form-control" type="text" value="{{ $product->generic->name }}" readonly></td>
                      <td><input type="text" class="form-control" value="{{ $product->unit->unit_name }}" readonly></td>
                      <td><input class="form-control" type="text" value="{{ $product->vendor->name }}" readonly></td>
                      <td><input class="form-control" type="number" value="{{ $product->request_quantity }}" readonly></td>
                      <td><input class="form-control" type="number" value="{{ $available_products->hubAvailableProductDetails[$i]->available_quantity ?? 0 }}" readonly></td>
                      <td><input class="form-control" type="text" value="{{ $product->product_uom->uom_name }}" readonly></td>
                      <td><input class="form-control {{ $product->status == 'cancel' ? 'text-danger' : 'text-success' }}" type="text" value="{{ $product->status }}" readonly></td>
                    </tr>
                    @endif
                    @endfor
                    @endforeach

                </tbody>

              </table>
            </div><!-- end.col-md-12 -->
          </div><!-- end.row -->
        </form>
      </div><!-- end.tile-body -->
    </div><!-- end.tile -->
  </div><!-- end.col-md-12 -->
</div><!-- end.row -->
@endsection


@push('post_scripts')

@endpush