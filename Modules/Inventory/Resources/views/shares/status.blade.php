<label for="status" class="control-label">Status <span class="text-danger">*</span></label>
<select class="form-control select2" name="status" required>
    <option value="" selected disabled>Select Status</option>
    <option value="1">Active</option>
    <option value="0">Inactive</option>
</select>
