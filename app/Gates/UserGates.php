<?php 

namespace App\Gates;

class UserGates{

   
    public function user_access_create($user)
    {
        $userAceess = explode('_',$user->access);
        if($userAceess[0]==='C'){
            return true;
        }else{
            return false;
        }
    }


    public function user_access_read($user)
    {
        $userAceess = explode('_',$user->access);
        if($userAceess[1]==='R'){
            return true;
        }else{
            return false;
        }
    }


    public function user_access_edit($user)
    {
        $userAceess = explode('_',$user->access); 
        if($userAceess[2]==='E'){
            return true;
        }else{
            return false;
        }
    }

    
    public function user_access_delete($user)
    {
        $userAceess = explode('_',$user->access); 
        if($userAceess[3]==='D'){
            return true;
        }else{
            return false;
        }
    }
}