<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProductsExport implements FromCollection
{
    use Exportable;

    private $collection;

    public function __construct($arrays)
    {
        $this->collection = collect($arrays);
    }

    public function collection()
    {
        return $this->collection;
    }
}
