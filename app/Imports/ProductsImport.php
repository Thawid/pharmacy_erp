<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductDetails;
use Modules\Product\Entities\ProductUom;
use Modules\Product\Entities\ProductVariation;

class ProductsImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
     * Process imported excel data.
     *
     * @param array $row
     * @return null
     */

    public function model(array $row)
    {
        $existing_product = Product::where('product_name', $row['product_name'])->first();
        if (empty($existing_product)) {
            if (!empty($row['product_name'])) {
                $product = [
                    'brand_name' => $row['brand_name'],
                    'product_name' => $row['product_name'],
                    'product_type_id' => $row['product_type_id'],
                    'product_category_id' => $row['product_category_id'],
                    'sub_category_id' => $row['sub_category_id'],
                    'product_generic_id' => $row['product_generic_id'],
                    'manufacturer_id' => $row['manufacturer_id'],
                    'unit_id' => $row['unit_id'],
                    'purchase_uom_id' => $row['purchase_uom_id'],
                    'purchase_price' => $row['purchase_price'],
                    'slug' => rand(1000, 3000) . '_' . $row['brand_name'] . '_' . $row['product_name'],
                    'status' => 1,
                ];

                $new_product_id = Product::insertGetId($product);

                if (!empty($new_product_id)) {
                    $product_details = [
                        'product_id' => $new_product_id,
                        'status' => 1,
                    ];
                    ProductDetails::insert($product_details);

                    $product_variation = [
                        'product_id' => $new_product_id,
                        'status' => 1,
                    ];
                    ProductVariation::insert($product_variation);

                    $uom_ids = explode(",", $row['uom_id']);
                    $quantity_per_uom = explode(",", $row['quantity_per_uom']);

                    foreach ($uom_ids as $key => $each_uom_id) {
                        $product_uoms['product_id'] = $new_product_id;
                        $product_uoms['uom_id'] = (int)$each_uom_id;
                        $product_uoms['quantity_per_uom'] = (int)$quantity_per_uom[$key];
                        $product_uoms['status'] = 1;
                        ProductUom::insert($product_uoms);
                    }
                }
            }
        }
    }
}
