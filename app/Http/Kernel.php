<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,

    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

        ],

        'api' => [
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],


    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,

        // Custom middleware
        'dboard' => \App\Http\Middleware\DboardMiddleware::class,
        'system' => \Modules\User\Http\Middleware\CheckUserMiddleware::class,
        'store' => \Modules\Store\Http\Middleware\CheckStoreMiddleware::class,
        'warehouse' => \Modules\Inventory\Http\Middleware\CheckInventoryMiddleware::class,
        'hub' => \Modules\Store\Http\Middleware\CheckHubMiddleware::class,
        'hq' => \Modules\Store\Http\Middleware\CheckHqMiddleware::class,
        'vendor' => \Modules\Vendor\Http\Middleware\CheckVendorMiddleware::class,
        'product' => \Modules\Product\Http\Middleware\CheckProductMiddleware::class,
        'price' => \Modules\Price\Http\Middleware\CheckPriceMiddleware::class,
        'accounts' => \Modules\Accounts\Http\Middleware\CheckAccountsMiddleware::class,
        'warhouseOrHub' => \Modules\Inventory\Http\Middleware\CheckWarehouseOrHubMiddleware::class,
        'storeOrHub' => \Modules\POS\Http\Middleware\CheckStoreOrHubMiddleware::class,
        'hqoraccount' => \App\Http\Middleware\HqOrAccountAccess::class,

        /*** API ***/
        'api.auth' => \App\Http\Middleware\Api\Auth::class,
        'api.guest' => \App\Http\Middleware\Api\Guest::class,
        /*** API END ***/
    ];
}
