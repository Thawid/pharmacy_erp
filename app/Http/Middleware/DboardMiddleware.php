<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class DboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
        
        // if (Auth::user()->weight >= 39.99 && Auth::user()->status == true)
        // {
        //     return $next($request);
        // }
        // return redirect('/')->with('message_warning','Sorry, you are not permitted to enter here. Please contact to your system admin.');
    }
}
