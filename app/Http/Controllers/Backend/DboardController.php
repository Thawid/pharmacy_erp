<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\POS\Entities\PointOfSale;
use DB;

class DboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function dboard()
    {
        $total_products = DB::table('products')->where('status', 1)->count();
        $total_users = DB::table('users')->where('status', 1)->count();
        $total_stores = DB::table('stores')->where('status', 1)->where('store_type', '=', "store")->count();
        $total_sales = DB::table('point_of_sales')->select(DB::raw('sum(grand_total) as total'))
            ->where('invoice_no', '!=', null)
            ->get();
        $monthly_sales = PointOfSale::select(
            DB::raw('LEFT(MONTHNAME(created_at),3) as month'),
            DB::raw('sum(grand_total) as total'))
            ->groupBy('month')
            ->get();

        $monthly_invoices = DB::table('store_invoices')->select(
            DB::raw('LEFT(MONTHNAME(created_at),3) as month'),
            DB::raw('sum(amount) as amounts'))
            ->groupBy('month')
            ->get();
        return view('dboard.dboard',
            compact('monthly_sales','total_products',
                'total_users','total_stores','total_sales','monthly_invoices'));
    }

    public function bootstrap_components()
    {
        return view('dboard.bootstrap_components');
    }

    public function cards()
    {
        return view('dboard.cards');
    }

    public function form_components()
    {
        return view('dboard.form_components');
    }

    public function custom_form_components()
    {
        return view('dboard.custom_form_components');
    }

    public function form_sample()
    {
        return view('dboard.form_sample');
    }
}
