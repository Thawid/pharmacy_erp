<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\RefreshToken;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth');
    }

    public function logout()
    {
        $accessToken = Auth::guard('api')->user()->token();
        $refreshToken = RefreshToken::where('access_token_id', $accessToken->id)->first();

        $refreshToken->revoke();
        $accessToken->revoke();

        return response()->json([
            'status' => 'success',
            'message' => 'You are logged out.',
        ], 200);
    }

    public function me()
    {
        return response()->json([
            'status' => 'success',
            'message' => 'User Information Get Successfully!.',
            'data' => [
                'user' => Auth::guard('api')->user()
            ]
        ], 200);
    }
}
