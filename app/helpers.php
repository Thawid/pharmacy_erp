<?php

use Illuminate\Support\Facades\Auth;


function userDomain(){
    $userDomainJsonString = file_get_contents(config_path('userDomain.json'));
    return json_decode($userDomainJsonString,true);
}
function userRole(){
    $userRoleJsonString = file_get_contents(config_path('userRole.json'));
    return json_decode($userRoleJsonString,true);
}
function userWeight(){
    $userWeightJsonString = file_get_contents(config_path('userWeight.json'));
    return json_decode($userWeightJsonString,true);
}


function systemModuleAuth(){

    $roles =userRole();
    $weight=userWeight();

   return  (Auth::user()->status == true && Auth::user()->role == $roles['admin'] && Auth::user()->weight == $weight['99.00']) || (Auth::user()->status == true && Auth::user()->role == $roles['developer'] && Auth::user()->weight == $weight['88.00']);
}


function storeModuleAuth(){

    $roles =userRole();
    $weight=userWeight();


   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['store_user'] && Auth::user()->weight == $weight['77.00']));
}


function hubModuleAuth(){

    $roles =userRole();
    $weight=userWeight();


   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['hub_user'] && Auth::user()->weight == $weight['77.00']));
}


function hqModuleAuth(){

    $roles =userRole();
    $weight=userWeight();
    
   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['hq_user'] && Auth::user()->weight == $weight['77.00']));
}


function productModuleAuth(){
    $roles =userRole();
    $weight=userWeight();

   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['product_operator'] && Auth::user()->weight == $weight['77.00']));
}

function vendorModuleAuth(){
    $roles =userRole();
    $weight=userWeight();

   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['vendor_user'] && Auth::user()->weight == $weight['77.00']));
}

function warehouseModuleAuth(){
    $roles =userRole();
    $weight=userWeight();

   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['warehouse_user'] && Auth::user()->weight == $weight['77.00']));
}



function priceModuleAuth(){
    $roles =userRole();
    $weight=userWeight();

   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['price_operator'] && Auth::user()->weight == $weight['77.00']));
}



function accountsModuleAuth(){
    $roles =userRole();
    $weight=userWeight();

   return  (Auth::user()->status == true && Auth::user()->role ==$roles['admin'] && Auth::user()->weight == $weight['99.00'])||((Auth::user()->status == true && Auth::user()->role ==$roles['developer'] && Auth::user()->weight == $weight['88.00'])) ||((Auth::user()->status == true && Auth::user()->role ==$roles['account_user'] && Auth::user()->weight == $weight['77.00']));
}