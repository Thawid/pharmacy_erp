<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Gates\UserGates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Laravel\Passport\Passport;

use Modules\User\Repositories\UserRepositoryInterface;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // API Auth Routes
        if (! $this->app->routesAreCached()) {
            Passport::routes();
        }

        // User access Gate
        Gate::define('hasCreatePermission',[UserGates::class, 'user_access_create']);
        Gate::define('hasReadPermission',[UserGates::class, 'user_access_read']);
        Gate::define('hasEditPermission',[UserGates::class, 'user_access_edit']);
        Gate::define('hasDeletePermission',[UserGates::class, 'user_access_delete']);
         
       
    }
}
