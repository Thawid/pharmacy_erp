//  pi chart
/*var series = [23, 44, 3, 21, 35, 1, 2];
let labels = ["A", "Team B", "Team C", "Team D", "Team E", "Team F", "Team G"];

let newSeries = [];
let newLabels = [];
let grouped = 0;
series.forEach((s, i) => {
    if (s < 10) {
        grouped += s;
    }
    if (s >= 10) {
        newSeries.push(s);
        newLabels.push(labels[i]);
    }
});

if (grouped > 0) {
    newSeries.push(grouped);
    newLabels.push("Others");
}

var options = {
    legend: {
        show: false,
    },
    series: newSeries,
    chart: {
        width: 380,
        type: "pie",
    },
    colors: ["#008FFB", "#00E396", "#FEB019", "#ccc"],
    labels: newLabels,
};

var chart = new ApexCharts(document.querySelector("#pi-chart"), options);
chart.render();*/

/*// bar chart

	var options = {
        chart: {
        type: 'bar',
        height: 280,
        toolbar: {
          show: false
      }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '48%',
        },
      },
      colors:['#0062FF','#D5D7E3'],
      series: [{
        name: 'Order',
        data: [20,50,23,35,15,18]
      }, {
        name: 'Sale in thousand',
        data: [50,92,49,70,28,66]
      }],
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      xaxis: {
        categories: ["Apr","May","Jun","Jul","Aug","Sep"],
        axisBorder: {
          show: true,
          color: 'rgba(0,0,0,0.05)'
      },
      },
      grid: {
        row: {
            colors: ['transparent', 'transparent'], opacity: .2
        },
        borderColor: 'rgba(0,0,0,0.05)'
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return  val ;
          }
        }
      },
      legend: {
        show: false
        }
      };
      var chart = new ApexCharts(document.querySelector("#bar_chart"), options);
      chart.render();*/

// gradient area chart
var options = {
    chart: {
        width: "100%",
        height: 280,
        type: "area",
        toolbar: {
            show: false,
        },
    },
    dataLabels: {
        enabled: false,
    },
    stroke: {
        curve: "smooth",
    },
    toolbar: {
        show: false,
    },
    legend: {
        show: false,
    },
    colors: ["#0080ff", "#00CDA2"],
    series: [
        {
            name: "lorem",
            data: [120, 59, 64, 131, 74, 128, 78],
        },
        {
            name: "Area chart",
            data: [56, 33, 45, 57, 39, 54, 45],
        },
    ],
    grid: {
        row: {
            colors: ["transparent", "transparent"],
            opacity: 0.2,
        },
        borderColor: "rgba(0,0,0,0.05)",
    },
    xaxis: {
        categories: ["Wed", "Thu", "Fri", "Sat", "Sun", "Mon", "Tue"],
        axisBorder: {
            show: true,
            color: "rgba(0,0,0,0.05)",
        },
    },
};

var chart = new ApexCharts(document.querySelector("#area_chart"), options);
chart.render();

//   radial chart
var options = {
    chart: {
        height: 280,
        type: "radialBar",
    },

    series: [67],

    plotOptions: {
        radialBar: {
            hollow: {
                margin: 15,
                size: "70%",
            },

            dataLabels: {
                showOn: "always",
                name: {
                    offsetY: -10,
                    show: true,
                    color: "#888",
                    fontSize: "13px",
                },
                value: {
                    color: "#111",
                    fontSize: "30px",
                    show: true,
                },
            },
        },
    },

    stroke: {
        lineCap: "round",
    },
    labels: ["Progress"],
};

var chart = new ApexCharts(document.querySelector("#radial_chart"), options);

chart.render();

// single bar chart
var options = {
    chart: {
        height: 350,
        type: "bar",
        stacked: true,
        toolbar: {
            show: false,
        },
        zoom: {
            enabled: false,
        },
    },
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: "20%",
        },
    },
    dataLabels: {
        enabled: false,
    },
    colors: ["#0062FF", "#57B8FF", "#D5D7E3"],
    series: [
        {
            name: "Direct",
            data: [40, 61, 70, 80, 51, 81],
        },
        {
            name: "Coupon",
            data: [55, 58, 30, 40, 60, 30],
        },
        {
            name: "Affiliate",
            data: [40, 20, 30, 12, 57, 42],
        },
    ],
    xaxis: {
        categories: ["Apr", "May", "Jun", "Jul", "Aug", "Sep"],
        axisBorder: {
            show: true,
            color: "rgba(0,0,0,0.05)",
        },
        axisTicks: {
            show: true,
            color: "rgba(0,0,0,0.05)",
        },
    },
    grid: {
        row: {
            colors: ["transparent", "transparent"],
            opacity: 0.2,
        },
        borderColor: "rgba(0,0,0,0.05)",
    },
    legend: {
        show: false,
    },
    fill: {
        opacity: 1,
    },
};

var chart = new ApexCharts(document.querySelector("#singleBar"), options);
chart.render();

// barMarker
var options = {
    series: [
        {
            name: "Actual",
            data: [
                {
                    x: "2011",
                    y: 12,
                    goals: [
                        {
                            name: "Expected",
                            value: 14,
                            strokeWidth: 5,
                            strokeColor: "#775DD0",
                        },
                    ],
                },
                {
                    x: "2012",
                    y: 44,
                    goals: [
                        {
                            name: "Expected",
                            value: 54,
                            strokeWidth: 5,
                            strokeColor: "#775DD0",
                        },
                    ],
                },
                {
                    x: "2013",
                    y: 54,
                    goals: [
                        {
                            name: "Expected",
                            value: 52,
                            strokeWidth: 5,
                            strokeColor: "#775DD0",
                        },
                    ],
                },
                {
                    x: "2014",
                    y: 66,
                    goals: [
                        {
                            name: "Expected",
                            value: 65,
                            strokeWidth: 5,
                            strokeColor: "#775DD0",
                        },
                    ],
                },
                {
                    x: "2015",
                    y: 81,
                    goals: [
                        {
                            name: "Expected",
                            value: 66,
                            strokeWidth: 5,
                            strokeColor: "#775DD0",
                        },
                    ],
                },
                {
                    x: "2016",
                    y: 67,
                    goals: [
                        {
                            name: "Expected",
                            value: 70,
                            strokeWidth: 5,
                            strokeColor: "#775DD0",
                        },
                    ],
                },
            ],
        },
    ],
    chart: {
        height: 350,
        type: "bar",
    },
    plotOptions: {
        bar: {
            horizontal: true,
        },
    },
    colors: ["#00E396"],
    dataLabels: {
        formatter: function (val, opt) {
            const goals =
                opt.w.config.series[opt.seriesIndex].data[opt.dataPointIndex]
                    .goals;

            if (goals && goals.length) {
                return `${val} / ${goals[0].value}`;
            }
            return val;
        },
    },
    legend: {
        show: true,
        showForSingleSeries: true,
        customLegendItems: ["Actual", "Expected"],
        markers: {
            fillColors: ["#00E396", "#775DD0"],
        },
    },
};

var chart = new ApexCharts(document.querySelector("#barMarker"), options);
chart.render();
