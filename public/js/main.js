(function () {
	"use strict";

	var treeviewMenu = $('.app-menu');

	// Toggle Sidebar
	$('[data-toggle="sidebar"]').click(function(event) {
		event.preventDefault();
		$('.app').toggleClass('sidenav-toggled');
	});

	// Activate sidebar treeview toggle
	$("[data-toggle='treeview']").click(function(event) {
		event.preventDefault();
		if(!$(this).parent().hasClass('is-expanded')) {
			treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
		}
		$(this).parent().toggleClass('is-expanded');
	});

	// Activate sidebar treeview toggle
	$(".child-treeview").click(function(event) {
		event.preventDefault();
		if(!$(this).parent().hasClass('expanded_child_item')) {
			treeviewMenu.find("[data-toggle='child']").parent().removeClass('expanded_child_item');
		}
		$(this).parent().toggleClass('expanded_child_item');
	});


	// Set initial active toggle
	$("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');
	$("[data-toggle='child.'].expanded_child_item").parent().toggleClass('expanded_child_item');

	//Activate bootstrip tooltips
	//$("[data-toggle='tooltip']").tooltip();

})();
